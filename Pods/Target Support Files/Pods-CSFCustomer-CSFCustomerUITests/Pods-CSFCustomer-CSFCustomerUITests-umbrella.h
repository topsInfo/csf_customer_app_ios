#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double Pods_CSFCustomer_CSFCustomerUITestsVersionNumber;
FOUNDATION_EXPORT const unsigned char Pods_CSFCustomer_CSFCustomerUITestsVersionString[];

