//
//  SwitchUser+CoreDataProperties.swift
//  CSFCustomer
//
//  Created by Tops on 15/04/21.
//
import Foundation
import CoreData

extension SwitchUser {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<SwitchUser> {
        return NSFetchRequest<SwitchUser>(entityName: "SwitchUser")
    }
    
    @NSManaged public var authToken: String?
    @NSManaged public var box_id: String?
    @NSManaged public var firstName: String?
    @NSManaged public var imgUser: String?
    @NSManaged public var is_default: Bool
    @NSManaged public var isActive: Bool
    @NSManaged public var lastName: String?
    @NSManaged public var loginType: String?
    @NSManaged public var unique_id: String?
    @NSManaged public var user_id: String?
    @NSManaged public var isBiometricEnabled: Bool
}
