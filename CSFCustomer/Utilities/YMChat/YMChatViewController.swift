//
//  YMChatViewController.swift
//  YMChat
//
//  Created by Kauntey Suryawanshi on 12/02/21.
//

import Foundation
import UIKit
import WebKit

protocol YMChatViewControllerDelegate: AnyObject {
    func eventReceivedFromBot(code: String, data: String?)
    func botCloseButtonTapped()
}

//@objc(YMChatViewController)
class YMChatViewController: UIViewController {
    
    @IBOutlet weak var viewChatBot: UIView!

    weak var delegate: YMChatViewControllerDelegate?

     var webView: WKWebView?
     var config: YMConfig?
     var objUserInfo = UserInfo()

//    init(config: YMConfig) {
//        self.config = config
//        super.init(nibName: nil, bundle: nil)
//        modalPresentationStyle = .fullScreen
//    }
//
//    required public init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented. Use init(config:) instead")
//    }

//    deinit {
//        webView?.stopLoading()
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let objUserInfo = getUserInfo()
        
        let firstName = objUserInfo.firstName ?? ""
        let lastName = objUserInfo.lastName ?? ""
        
        var fullName : String = firstName
        
        if firstName.count > 0 && lastName.count > 0 {
            fullName = "\(firstName) \(lastName)"
        }
        print("Full name \(fullName)")
        
        let boxId = objUserInfo.boxId ?? ""
        print("Box ID \(boxId)")

        self.config = YMConfig(botId: kBotID)
        config?.payload = ["name": "\(fullName)", "boxId": "\(boxId)"]
        print("payload is \(String(describing: config?.payload))")
        
//      super.init(nibName: nil, bundle: nil)
//      modalPresentationStyle = .fullScreen
        addWebView()
        if config!.showCloseButton {
//            addCloseButton(tintColor: config!.closeButtonColor)
        }
//        log("Loading URL: \(config.url)")
        webView?.load(URLRequest(url: config!.url))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    private func addWebView() {
        let configuration = WKWebViewConfiguration()
        let contentController = WKUserContentController()
        let js = "function sendEventFromiOS(s){document.getElementById('ymIframe').contentWindow.postMessage(JSON.stringify({ event_code: 'send-voice-text', data: s }), '*');}"
        let userScript = WKUserScript(source: js, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        contentController.addUserScript(userScript)

        contentController.add(LeakAvoider(delegate:self), name: "ymHandler")
        configuration.userContentController = contentController
        
        self.webView = WKWebView(frame: .zero, configuration: configuration)

        webView!.navigationDelegate = self
        webView!.uiDelegate = self
        viewChatBot.addSubview(webView!)
        webView!.translatesAutoresizingMaskIntoConstraints = false
//        let margins = viewChatBot.layoutMarginsGuide
        webView!.leadingAnchor.constraint(equalTo: viewChatBot.leadingAnchor).isActive = true
        webView!.topAnchor.constraint(equalTo: viewChatBot.topAnchor).isActive = true
//        webView!.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        
        webView!.trailingAnchor.constraint(equalTo: viewChatBot.trailingAnchor).isActive = true
        webView!.bottomAnchor.constraint(equalTo: viewChatBot.bottomAnchor).isActive = true
    }

    private let closeButton = UIButton()
    private func addCloseButton(tintColor: UIColor) {
        let closeImage = UIImage(named: "closeChat", in: Bundle.assetBundle, compatibleWith: nil) ?? UIImage()
        closeButton.setImage(closeImage, for: .normal)
        closeButton.tintColor = tintColor
        viewChatBot.addSubview(closeButton)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
//        let margins = viewChatBot.layoutMarginsGuide
//        closeButton.topAnchor.constraint(equalTo: margins.topAnchor, constant: 10).isActive = true
        closeButton.topAnchor.constraint(equalTo: viewChatBot.topAnchor, constant: 10).isActive = true
        closeButton.rightAnchor.constraint(equalTo: viewChatBot.rightAnchor, constant: -10).isActive = true
        
        closeButton.addTarget(self, action: #selector(botCloseButtonTapped), for: .touchUpInside)
    }

    @objc func botCloseButtonTapped() {
//        log(#function)
        self.dismiss(animated: true) { [weak self] in
            self?.delegate?.botCloseButtonTapped()
        }
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.backgroundColor = config?.statusBarColor
    }

//    open override var preferredStatusBarStyle: UIStatusBarStyle {
//        config!.statusBarStyle
//    }

    func sendMessageInWebView(text: String) {
//        log(#function, text)
        webView?.evaluateJavaScript("sendEventFromiOS('\(text)');", completionHandler: nil)
    }
    
    // MARK: - IBAction methods
    @IBAction func btnMenuClicked()
    {
        self.revealViewController().revealToggle(animated: true)
    }
    
//    @IBAction func unlinkDeviceToken(_ sender: Any) {
//        Messaging.messaging().token { token, error in
//            if let error = error {
//                print("########## Error fetching FCM registration token: \(error)")
//            } else if let token = token {
//                //Get api key from Account setting section of app.yellow.ai or My Profile Section of cloud.yellow.ai
//                let apiKey: String = ""
//                YMChat.shared.unlinkDeviceToken(botId: self.botID, apiKey: apiKey, deviceToken: token) {
//                    print("Token removed successfully")
//                } failure: { errorString in
//                    print("ERROR: \(errorString)")
//                }
//            }
//        }
//    }
}

//extension YMChatViewController: SpeechDelegate {
extension YMChatViewController{
    func handleInternalEvent(code: String) {
        switch code {
        case "image-opened":
            closeButton.isHidden = true
        case "image-closed":
            closeButton.isHidden = false
        default: break
        }
    }
}

extension YMChatViewController: WKNavigationDelegate, WKScriptMessageHandler {
    open func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "ymHandler" {
            guard let dict = message.body as? [String: Any],
                  let code = dict["code"] as? String else {
                return
            }
            let isInternal = dict["internal"] as? Bool ?? false
            if isInternal {
                handleInternalEvent(code: code)
            } else {
                let data = dict["data"] as? String
                delegate?.eventReceivedFromBot(code: code, data: data)
            }
        }
    }

    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated  {
            if let url = navigationAction.request.url,
               UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
                decisionHandler(.cancel)
            } else {
                decisionHandler(.allow)
            }
        } else {
            decisionHandler(.allow)
        }
    }
}

extension YMChatViewController: WKUIDelegate {
    // for <buttons> in html that have window.open
    // https://stackoverflow.com/questions/33190234/wkwebview-and-window-open
    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if let url = navigationAction.request.url, UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
        return nil
    }
}

/// WKUserContentController retains its message handler.
///
/// https://stackoverflow.com/a/26383032/1311902
fileprivate class LeakAvoider : NSObject, WKScriptMessageHandler {
    weak var delegate : WKScriptMessageHandler?

    init(delegate:WKScriptMessageHandler) {
        self.delegate = delegate
        super.init()
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        self.delegate?.userContentController(userContentController, didReceive: message)
    }
}
