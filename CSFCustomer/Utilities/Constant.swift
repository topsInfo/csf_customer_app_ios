//
//  Constant.swift
//  CSFCustomer
//
//  Created by Tops on 23/11/20.
//

import UIKit
import MBProgressHUD
import SDWebImage
import LoginWithAmazon
import SwiftyJSON
import CoreData
import SystemConfiguration
import AVFoundation

// MARK: - Global Variables
let appDelegate                         = UIApplication.shared.delegate as! AppDelegate
var kAuthToken : String                 = "AuthToken"
var kDeviceToken : String               = "DeviceToken"
var kCurrentAppStoreVersion : String    = "AppstoreVersion"
var kAppVersionPopupTimeStamp : String  = "AppVersionPopupTimeStamp"
let kDeviceType : Int                   = 2
let kDeviceBrand : String               = "Apple"
let kDeviceName : String                = UIDevice.modelName
let kOSVersion : String                 = UIDevice.current.systemVersion
let kVersion                            = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
let kLoggedIn : String                  = "LoggedIn"
let kCurrency : String                  = "USD"
let kCurrencySymbol : String            = "$"
let kTTCurrency : String                = "TTD"
let kUserInfo : String                  = "UserInfo"
let kAppName : String                   = ""
let kNotificationTitle : String         = "CSF Couriers"
let kTypeOfLogin : String               = ""
let kRememberMe : String                = "RememberMe"
let kUsername : String                  = "UserName"
let kAcNo : String                      = "Card No : " //new change
let kCartItemAvailable                  = "Cart"
let kProfileInfoUpdated                 = "ProfileInfo"
let kWeightUnit                         = "lbs"
let kRecentTitles                       = "RecentTitle"
let kShowBiometricPopup                 = "ShowBiometric"
let kIsBiometricEnabled                 = "isBiometricEnabled"
let kIsDarkModeEnabled                  = "isDarkModeEnabled"
let kIsLightModeEnabled                 = "isLightModeEnabled"
let kIsSystemModeModeEnabled            = "isSystemModeModeEnabled"

let kUserID                             = "UserID"
let kBoxID                              = "BoxID"
let kIsAddresAdded                      = "AddressAdded"
let kDeliveryOption                     = "DeliveryOption"
let kAppstoreID                         = "1547257972"
let kMiniumAppVersion                   = "1.0.1"
let kSwitchUserEntity : String          = "SwitchUser"
var kSwitchProfileUserToken : String    = ""
var uniqueID : String                   = ""
var loginType : String                  = ""
var kBotName : String                   = "CSF Couriers"
//side menu updated using following vars
var cartCounter : Int                   = -1
var referralAmount : Int                = 0
var arrFeedbackReason :[FeedbackReason] = [FeedbackReason]()
var openTicketURL : String              = ""
var openTicketLogoutURL : String        = ""
let kArrSegmentOptons                 = "arrSegmentOptons"
var kVersionPopupCounter : String    = "VersionPopupCounter"
var kApiIOSVersion: String    = "ApiIosVersion"
var kUpdateLater : String    = "UpdateLater"
var kDontAskMeAgain : String    = "DontAskMeAgain"
var kIsAccountClosureShown : String = "isAccountClosureShown"
var kIsVideoTutorialShown : String = "isVideoTutorialShown"
//Nand : New Feature Updates Code
let kShowFeatureMsg : String = "ShowFeatureMsg"

var documentSizeLimit : Int = 2
var documentLimit : Int = 5

let WALLET = "Wallet"
let PAYPAL = "PayPal"
let CARD_PAYMENT = "CardPayment"
let APPLE_PAY = "ApplePay"

let SUFFIX_TOTALCOUNT_INVOICE = "Invoices"
let SUFFIX_TOTALCOUNT_NOTIFICATION = "Notifications"
let SUFFIX_TOTALCOUNT_PREALERT = "Pre-Alerts"
let SUFFIX_TOTALCOUNT_PACKAGES = "Packages"
let SUFFIX_TOTALCOUNT_VIDEOS = "Videos"
let SUFFIX_TOTALCOUNT_REQUESTS = "Requests"

// MARK: - Enums
// MARK: - RecentMenuTitle Enum
enum RecentMenuTitle : String
{
   case PreAlert = "PreAlert"
   case Invoice = "Invoices"
   case ViewCart = "ViewCart"
   case Refund = "Refund"
   case Feedback = "Feedback"
   func dispValue() -> String
   {
        return self.rawValue
   }
}

// MARK: - LoginType Enum
enum LoginType :String
{
    case AppLogin = "AppLogin"
    case AmazonLogin = "Amazon"
    case AppleLogin = "Apple"
}

// MARK: - Segments Enum
enum Segments : String
{
    case MyInvoice = "My Invoices"
    case UnpaidInvoice  = "Unpaid"
    case PackageRequest  = "Package Request"
    case PaidInvoice  = "Paid"
    case Archived  = "Archived"
    case InTransit = "In Transit"
    case Pickup = "Pickup"
    case OnDelivery = "On Delivery"
    case Refunded = "Refunded"
    case UpcomingInvoice = "Upcoming"
}

// MARK: - SegmentsIndex Enum
enum SegmentsIndex : String
{
    case MyInvoice = "0"
    case UnpaidInvoice  = "1"
    case PackageRequest  = "2"
    case PaidInvoice  = "3"
    case Archived  = "4"
    case InTransit = "5"
    case Pickup = "6"
    case OnDelivery = "7"
    case Refunded = "8"
    case UpcomingInvoice = "9"
}

// MARK: - InvoiceActionsName Enum
enum InvoiceActionsName : String
{
    case GenerateTicket  = "Generate Ticket"
    case InvoiceDetails  = "Invoice Details"
    case DownloadInvoice  = "Download Invoice"
    case ViewYourCart  = "Add To Cart"
    //case ViewYourCart  = "View Your Cart"
    case PackageRequest  = "Package Request"
}

// MARK: - InvoiceActions Enum
class InvoiceActions : NSObject{
    var actionName : String!
    var actionImage : String!
    var imageBackColor : String!
    override init() {
    }
}

// MARK: - InvoiceActionsImage Enum
enum InvoiceActionsImage : String
{
    case Support  = "support-1"
    case View  = "view"
    case Download  = "download"
    case AddToCart  = "add_to_cart"
    case PackageRequest  = "package request"
}

// MARK: - InvoiceActionsColor Enum
enum InvoiceActionsColor : String
{
    case Support  = "DC4100"
    case View  = "FFD94A"
    case Download  = "38A900"
    case AddToCart  = "055B94"
    case PackageRequest  = "2973F7"
}

// MARK: - ToastPosition Enum
enum ToastPosition : Int
{
   case bottom = 0
   case top = 1
   func dispValue() -> Int
   {
        return self.rawValue
   }
}

// MARK: - Font Struct
struct fontname
{
    static let openSansRegular = "OpenSans-Regular"
    static let openSansBold = "OpenSans-Bold"
    static let openSansSemiBold = "OpenSans-SemiBold"
    static let openSansItalic = "OpenSans-Italic"    
}

// MARK: - Colors Struct
struct Colors
{
    static let clearColor               = UIColor.clear
    static let theme_blue_color         = UIColor.init(red: 5/255, green: 91/255, blue: 148/255, alpha: 1.0)
    static let theme_orange_color       = UIColor.init(red: 212/255, green: 85/255, blue: 0, alpha: 1.0)
    static let theme_gray_color         = UIColor.init(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)//989898
    
    static let theme_black_color        = UIColor.init(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
    static let theme_segment_bg_color   = UIColor.init(red: 63/255, green: 63/255, blue: 63/255, alpha: 1.0)
    static let theme_white_color        = UIColor.white
    static let theme_lightgray_color    = UIColor.init(red: 198/255, green: 198/255, blue: 198/255,alpha: 1.0) //c6c6c6
    static let theme_border_gray_color  = UIColor.init(red: 246/255, green: 246/255, blue: 246/255,alpha: 1.0)
    static let theme_green_color        = UIColor.init(red: 56/255, green: 169/255, blue: 0,alpha: 1.0)
    //notification
    static let theme_lightorange_color  = UIColor.init(red: 253/255, green: 246/255, blue: 242/255,alpha: 1.0)
    //invoice
    static let theme_yellow_color       = UIColor.init(red: 255/255, green: 193/255, blue: 7/255,alpha: 1.0)
    static let theme_red_color          = UIColor(named: "theme_red_color")!
    //switch account
    static let theme_light_yellow_color = UIColor.init(red: 255/255, green: 219/255, blue: 149/255,alpha: 1.0)
    static let theme_light_skyblue_color = UIColor.init(red: 45/255, green:140/255, blue: 179/255,alpha: 1.0)
    static let theme_orange_bg_color    = UIColor.init(red: 254/255, green:231/255, blue: 218/255,alpha:0.9)
    
    static let support_card_shadow_color = UIColor.init(red: 198/255, green: 198/255, blue: 198/255, alpha: 1.0)
    
    static let theme_tracker_stage_seperator_color = UIColor(named: "theme_tracker_stage_seperator_color")!
    static let theme_bgColor            = UIColor(named: "theme_bg_color")!
    static let theme_lightblue_color    = UIColor(named: "theme_lightblue_color")!
    static let theme_label_black_color  = UIColor(named: "theme_label_black_color")!
    static let theme_calendar_dates_disable_color    = UIColor(named: "theme_calendar_dates_disable_color")!
    static let theme_calendar_dates_enable_color    = UIColor(named: "theme_calendar_dates_enable_color")!
    static let theme_dele_ac_border_color    = UIColor(named: "theme_dele_ac_border_color")!
    static let theme_dele_ac_disable_button_back_color    = UIColor(named: "theme_dele_ac_disable_button_back_color")!
    
    static let theme_dele_ac_disable_button_text_color    = UIColor(named: "theme_dele_ac_disable_button_text_color")!
    static let theme_dropdown_selection_back_color = UIColor(named: "theme_dropdown_selection_back_color")!
    static let theme_dropdown_selection_text_color = UIColor(named: "theme_dropdown_selection_text_color")!
    
    static let theme_label_blue_color = UIColor(named: "theme_label_blue_color")!
    
    static let credit_invoice_bar_color = UIColor(named: "credit_invoice_bar_color")!
}

// MARK: - Messsages Struct
struct Messsages
{
    static let msg_no_internet_connection       = "The internet connection appears to be offline"
    static let msg_no_osticket_url_found        = "No Open Ticket URL Found"
    static let msg_enter_username               = "Please enter your BoxID"
    static let msg_enter_password               = "Please enter password"
    static let msg_not_supported_apple_signin   = "Apple sign in is supported with iOS 13 or higher version"
    //add prealerts
    static let msg_shipping_method              = "Please select Shipping method"
    static let msg_merchant_store               = "Please enter Merchant/store"
    static let msg_other_shipper_name           = "Please enter Other shipper name"
    
    static let msg_goods_description            = "Please enter Description of goods"
    static let msg_tracking_no                  = "Please enter Tracking number"
    static let msg_length_of_tracking_no        = "Length of Tracking number must be between 7 to 50."
    static let msg_usdvalue                     = "Please enter USD value"
    static let msg_usdvalue_exceeds             = "USD amount can't exceed 99999.99"
    static let msg_invalid_USDAmount            = "USD Amount should be greater than or equal to 0.5"
    static let msg_choose_file                  = "Please select document"
    static let msg_add_item                     = "Please add an item description"
    static let msg_agree_terms                  = "Please agree with terms and conditions"
    static let msg_alert_success                = "Pre-alert has been saved successfully"
    static let msg_pls_try_again                = "Pleaes try again!"
    static let msg_cancle_action                = "User has cancelled this action"
    static let msg_prealert_exist               = "Pre alert already exist"
    //logout
    static let msg_logout                       = "Are you sure you want to logout?"
    static let msg_logout_all                   = "Are you sure you want to logout all users?"
    static let msg_not_logout                   = "You have not logged out of Amazon login.Please try again!"
    //forgotpassword
    static let msg_email                        = "Please enter email"
    static let msg_invalid_email                = "Invalid email address"
    //invoice
    static let msg_no_invoice                   = "No any latest Invoice"
    static let msg_invoice_downloaded           = "Downloaded Successfully"
    static let msg_invoice_exists               = "Invoice already downloaded"
    static let msg_select_invoice_pickup_request = "Please select invoice(s) to pickup request"
    //add alert screen notes
    static let msg_goods_desc                   = "Please enter the description of your order (e.g. 1 CASIO WATCH, 1 KINDLE TABLET etc). Please note that you can also enter special instructions/notes for our miami team (e.g. PLEASE HOLD PACKAGE REQUEST PHOTO OF PACKAGE CONTENTS etc.)"
    static let msg_merchant_desc                = "Please select the merchant/store you purchased your item from (Eg. Amazon.com,Walmart,Ebay etc.)"
    static let msg_tracking_desc                = "If available, please enter only the tracking number for the US shipping company in the field above (FedEx, UPS, DHL, USPS etc.). Please note, not the Supplier/Vendor order number."
    
    static let msg_terms_condition              = "I agree to understanding the Pre Alert terms.*"
    static let msg_image_type                   = "Selected image type should be .jpg/.png"
    //generate ticket
    static let msg_subject                      = "Please enter subject"
    static let msg_report                       = "Please report an issue"
    //viewcart
    static let msg_select_invoice               = "Please select at least one invoice"
    //checkout new change
    static let msg_delete_card                  = "Are you sure you want to delete selected card?"
    static let msg_cardholder_name              = "Please enter card holder name"
    static let msg_card_name                    = "Please enter business or personal card name"
    static let msg_add_card                     = "Please add new card detail"
    static let msg_postal_code                  = "Please add new card with Postal code"
    static let msg_add_card_title               = "Add your credit card and pay"
    static let msg_select_card_title            = "Select your credit card and pay"
    //Add new card
    static let msg_card_added                   = "  New card added successfully"
    //edit profile
    static let msg_enter_firstname              = "Please enter first name"
    static let msg_enter_lastname               = "Please enter last name"
    static let msg_enter_companyname            = "Please enter company name"
    static let msg_enter_email                  = "Please enter email"
    static let msg_phone_length                 = "Invalid phone number"
    static let msg_enter_mobile                 = "Please enter mobile no"
    static let msg_mobile_length                = "Invalid mobile number"
    static let msg_select_dob                   = "Please select Date of Birth"
    static let msg_select_nationalId            = "Please select National id type"
    static let msg_enter_permitno               = "Please enter"
    static let msg_select_gender                = "Please select gender"
    static let msg_address_already_added        = "Address has already been added by the user."
    //address
    static let msg_select_address_title         = "Please select address title"
    static let msg_enter_address                = "Please enter address line 1"
    static let msg_select_city                  = "Please select your city"
    static let msg_select_valid_city            = "Please select valid city"
    static let msg_other_title                  = "Please enter other title"
    static let msg_delete_address               = "Are you sure you want to delete this address?"
    static let msg_enter_addresses              = "Please enter address"
    //Change password
    static let msg_enter_old_pwd                = "Please enter old password"
    static let msg_enter_new_pwd                = "Please enter new password"
    static let msg_enter_confirm_pwd            = "Please enter confirm password"
    static let msg_pwd_mismatch                 = "Confirm password must match with new password"
    static let msg_old_pwd_length               = "Old password length should not exceed 12 characters"
    static let msg_pwd_rules                    = "New password do not satisfy given rules"
    static let msg_pwd_note                     = "Note : New password must be 8 to 12 characters long and must contain an uppercase character,lowercase character,a number and a special character."
    static let msg_pwd_length                   = "Password must be 6 to 12 characters long"
    //feedback
    static let msg_select_reason                = "Please select reason"
    static let msg_enter_desc                   = "Please enter description"
    //florida address
    static let msg_address_copied               = "The address is copied to the clipboard"
    //calculator
    static let msg_enter_itemdesc               = "Please select item description"
    static let msg_item_not_exist               = "Item does not exist in the list"
    static let msg_enter_totalamt               = "Please enter invoice total"
    static let msg_invalid_totalamt             = "Invalid invoice total"
    static let msg_enter_weight                 = "Please enter shipping weight"
    static let msg_invalid_weight               = "Invalid shipping weight of item"
    static let msg_enter_shipping_dimension     = "Please enter shipping dimensions"
    static let msg_invalid_shipping_dimension   = "Invalid shipping dimensions"
    //Track order
    static let msg_enter_trackorder             = "Please enter Tracking Number"
    static let msg_claim                        = "Are you sure you want to claim for this order?"
    static let msg_invalid_amount               = "Please enter valid amount"
    //biometric
    static let msg_no_biometric                 = "No Touch ID or Face ID is supported in this device"
    static let msg_biometric_error              = "Touch ID or Face ID may be disabled or not supported in the device."
    //apple login message
    static let msg_fillup_profile               = "Please fill up all mendatory details.This information is required for CSF Couriers dispatching process."
    //paypal messages
    static let msg_paypal_cancel                = "Paypal payment cancelled by user"
    //Invite messages
    static let msg_no_contact_list              =  "Unable to fetch contacts"
    static let msg_code_copied                  = "Referral code copied to clipboard"
    //stripe card messages
    static let msg_no_valid_card                =  "Please enter valid card details"
    //Add Money
    static let msg_enter_amount                 =  "Please enter amount"
//    static let msg_invalid_ttd_amount           =  "Amount should be between 100 TTD and 1000 TTD"
    
    static let msg_add_min_max_ttd_amount_into_wallet           =  "The minimum amount you can add into your wallet for each transaction is 100 TTD and the maximum amount you can add is 1000 TTD."
    static let msg_max_ttd_amount_for_wallet           =  "Wallet amount allowed up to TTD $1000.00"

    //Pay checkout
    static let msg_select_payment_method        =  "Please select Payment Method"
    static let msg_delete_card_confirmation     = "Are you sure you want to remove this card?"
    static let msg_invalid_usdamount            = "Invalid Amount."
    //service guide
    static let msg_service_guide_downloaded     = "Service Guide Downloaded Successfully"
    static let msg_service_guide_download_failure = "Service Guide Download failure"
    static let msg_service_guide_download       = "Are you sure you want to download Service Guide?"
    
    static let msg_online_shopper_guide_downloaded     = "Online Shopper Guide Downloaded Successfully"
    static let msg_online_shopper_guide_download_failure = "Online Shopper Guide Download failure"

    //pickup request
    static let msg_pickup_request_confirmation  = "Are you sure you want to add pickup request for selected invoices?"
    static let msg_select_date                  = "Please select Pickup Date"
    static let msg_select_time                  = "Please select Pickup Time"
    static let msg_vehicle_description          = "Please enter vehicle description and number"
    static let msg_additional_notes             = "Please enter additoinal information"
    static let msg_select_delivery_address      = "Please select your delivery address"
    //Main login
    static let msg_switch_user                  = "Only five active accounts are supported in CSF Couriers. To enter into app, user can switch to active account or remove one of the active account and add a new one."
    static let msg_max_active_user_limit        = "You have reached the limit of maximum active account"
    //filter
    static let msg_enter_search_text            = "Please enter search text"
    
    //ManageTab - Invoice
    static let msg_select_3_to_5_tabs = "You can select tabs between 3 to 5."
    static let msg_remove_image_confirm = "Are you sure want to remove this image?"
    static let msg_enter_desc_report = "Please enter the description."
    static let msg_enter_add_some_attachment = "Please add some attachments."
    static let msg_report_sent_success = "Report Sent Successfully."
    static let msg_report_desc_limit = "Description should be 200 characters only."
    static let msg_no_camera_access = "Camera access is denied"
    static let msg_attachment_should_less_than_2_mb = "Selected attachment should be less than 2 mb."

    static let msg_enter_partial_amount = "Please enter amount"
    
    static let msg_select_payment_method_new = "Please select payment mehtod"
    static let msg_select_partial_payment_method = "Please select partial payment mehtod"
    static let msg_select_partial_wallet_amount_is_not_sufficient = "Wallet balance is not sufficient. Use partial payment option or change the payment method."
    
    static let partial_amount_same_as_total_amount = "Your partial payment amount is same as total amount. So you can not select other payment option or remove the partial payment."
    
    static let unselect_wallet_option_first = "Please unselect the wallet options first."
    
    static let select_another_payment_method_to_complete_partial_payment = "Please select another payment mehtod to complete the partial payment."

    //Partial payment messages
    static let msg_enter_amount_should_not_be_greater_than_wallet_amount = "Enter amount should not be greater than wallet amount"
    static let msg_wallet_amount_should_not_be_greater_than_total_amount = "Wallet amount should not be greater than total amount"

    static let msg_wallet_balance_not_sufficient = "Wallet balance is not sufficient. Choose partial payment option or change to full payment method."
    static let msg_wallet_balance_sufficient = "Your wallet has sufficient balance to pay the invoice."
    
    static let msg_please_select_payment_method_checkout = "Please select payment method."
    
    static let msg_please_select_partial_payment_method_checkout = "Please select partial payment method."
    
    static let msg_copied_to_clipboard = "Copied to clipboard"
    
    static let msg_service_guide_note = "Please note the change in our updated Service Guide in the following sections :"
    static let msg_online_shopper_guide_note = "Please note the changes in our updated Online Shopper Guide in the following sections :"
    
    static let msg_select_month_and_year = "Please select month & year"

    static let msg_doc_already_uploaded = "You have already uploaded this document."
    
    static let msg_delete_document = "Are you sure you want to delete this document?"
    
    static let msg_delete_account = "Are you sure you want to delete this account?"

    static let msg_approve_request = "Are you sure you want to Continue the request?"
    static let msg_reject_request = "Are you sure you want to Cancel the request?"
    static let msg_delete_reason = "Please enter additional notes"
    
    static let apple_login_info_require_msg = "Name and email address required for new apple login.\nPlease remove CSF Courier from Apps Using Apple ID from device’s settings.\nOpen the Settings app -> Tap your name -> Tap Password & Security -> Sign in with Apple -> Tap Apps Using Apple ID."

    static let msg_disalbeContinueButton_msg = "Your account might have pending packages or outstanding balance. Please check details again."
    static let msg_agree_terms_to_delete_ac = "I understand and agree that my payment information will be removed upon account deletion."
    
    static let msg_cancel_delete_account = "Are you sure you want to cancel the account closure process?"
    static let msg_textview_additionalNotes_limit = "Additional notes should be 200 characters only."
    static let msg_deleteAccount_final_confirmation_text = "This is your final confirmation for deleting your CSF Account.\n\nIf you proceed, you will permanently lose all Account information including:"
    static let msg_enter_only_alpha_numeric_character = "Please enter only alpha or numeric character"
    
    static let msg_select_delivery_preference = "Please select Delivery Prefence Schedule"
    
    //DriveThru
    static let msg_enter_note = "Please enter notes."

    static let msg_drive_thru_success_desc = "Your request has been generated. Your request is being processed, you receive confirmation soon."
    static let msg_select_reason_cancel_driveThruRequest = "Please select reason."
    
    static let msg_pickup_dates_not_available = "Pickup dates not available, contact to CSF."
    
    //Rate & Review
    static let msg_please_select_ratting_category = "Please select the rating category."
    
    static let msg_cancel_process = "Are you sure you want to cancel this process?"

}

// MARK: - Geometry Struct
struct Geometry {
    static let screen = UIScreen.main
    static let frame = screen.bounds
    static let width = frame.width
    static let height = frame.height
    
//    static let safeArea: UIEdgeInsets = UIApplication.shared.keyWindow?.safeAreaInsets ?? UIEdgeInsets.zero
    
    static let safeArea: UIEdgeInsets = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?
.safeAreaInsets ?? UIEdgeInsets.zero

    static let bottomSafeAreaHeight = safeArea.bottom
    static let topSafeAreaHeight = safeArea.top
    static var hasNotch: Bool {
        return topSafeAreaHeight >= 44
    }
    static var width_ratio: CGFloat {
        return width / 375
    }
    static var height_ratio: CGFloat {
        return height / 833
    }
}

// MARK: - configureLabel
func configureLabel (label:UILabel,title:String,bgColor:UIColor,textColor:UIColor,font:String,fontSize:Float)
{
      let attributes = [NSAttributedString.Key.font : UIFont(name: font, size: CGFloat(fontSize)),
                        NSAttributedString.Key.foregroundColor : textColor
          
      ]
      label.attributedText = NSAttributedString(string: title, attributes: attributes as [NSAttributedString.Key : Any])
      label.backgroundColor = bgColor
      label.textColor = textColor
}

// MARK: - getSafeAreaValue
func getSafeAreaValue() -> Int
{
    let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
       if #available(iOS 11.0, *)
       {
            let safeareaBottom = UIApplication.shared.windows.first?.safeAreaInsets.bottom
            if safeareaBottom == 34
            {
                return Int((safeareaBottom ?? 0) + 150)
            }
            else if safeareaBottom == 0
            {
                return Int((bottomPadding ?? 0) + 120) //160
            }
       }
       else
        {
            return Int((bottomPadding ?? 0) + 120)//160
        }
   return 100
}

// MARK: - getSafeAreaTopValue
func getSafeAreaTopValue() -> Int {
    let topPadding = UIApplication.shared.windows.first?.safeAreaInsets.top as? CGFloat
    if #available(iOS 11.0, *)
    {
        let safeareaTop = UIApplication.shared.windows.first?.safeAreaInsets.top
        if safeareaTop == 48
        {
            return Int((safeareaTop ?? 0) + 100)
        }
        else if safeareaTop == 0
        {
            return Int((safeareaTop ?? 0) + 100) //160
        }
    }
    else
    {
        return Int((topPadding ?? 0) + 120)//160
    }
    return 100
}

// MARK: - getUserInfo
func getUserInfo() -> UserInfo
{
    if  let decoded = UserDefaults.standard.object(forKey: kUserInfo) as? Data
    {
//       This old code is now replaced by below new code
//       return NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserInfo
        
        do {
            if let unarchivedObject = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserInfo.self, from: decoded) {
                // Handle the unarchived object
                return unarchivedObject
            } else {
                // Handle the case where unarchiving failed
            }
        } catch {
            // Handle the error
        }
    }
    return UserInfo()
}

// MARK: - Extension
// MARK: - UserDefaults Extension
extension UserDefaults {
    func decode<T : Codable>(for type : T.Type, using key : String) -> T? {
        let defaults = UserDefaults.standard
        guard let data = defaults.object(forKey: key) as? Data else {return nil}
        let decodedObject = try? PropertyListDecoder().decode(type, from: data)
        return decodedObject
    }
    
    func encode<T : Codable>(for type : T, using key : String) {
        let defaults = UserDefaults.standard
        let encodedData = try? PropertyListEncoder().encode(type)
        defaults.set(encodedData, forKey: key)
    }
}

// MARK: - UIButton Extension
extension UIButton
{
    func setDottedBorder()
    {
        let color = Colors.theme_lightgray_color.cgColor
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.bounds.size
        let width = UIScreen.main.bounds.size.width - 40
        let shapeRect = CGRect(x: 0, y: 0, width: width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}

// MARK: - UIImageView Extension
extension UIImageView{
    func setImage(_ url: String?, placeHolder: UIImage? = nil, isShowLoader : Bool? = true){
        if isShowLoader! {
            self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        }
        self.sd_setImage(with: URL(string: url ?? ""), placeholderImage: placeHolder, completed: nil)
    }
}

// MARK: - UIImage Extension
extension UIImage {
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image { _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    
    public enum DataUnits: String {
        case byte, kilobyte, megabyte, gigabyte
    }

    func getSizeIn(mimeType : String, type: DataUnits) -> Double {

        var data = Data()
        
        if mimeType == "image/jpeg" {
            data = Data(self.jpegData(compressionQuality: 1)!)
        }else if mimeType == "" || mimeType == "image/png" {
            if self.pngData() != nil {
                data = self.pngData()! as Data
            }
        }

        var size: Double = 0.0
        print("*** byte size is \(Double(data.count))")
        switch type {
        case .byte:
            size = Double(data.count)
        case .kilobyte:
            size = Double(data.count) / 1024
        case .megabyte:
            size = Double(data.count) / 1024 / 1024
            print("***  byte size In MB \(size)")
        case .gigabyte:
            size = Double(data.count) / 1024 / 1024 / 1024
        }
        
        return size
    }
    
    func isEqualToImage(_ image: UIImage) -> Bool {
        let data1 = self.pngData()
        let data2 = image.pngData()
        return data1 == data2
    }
}

// MARK: - Data Extension
extension Data {
    
    public enum DataUnits: String {
        case byte, kilobyte, megabyte, gigabyte
    }
    
    func getSizeInFromData(data : Data, type: DataUnits) -> String {
        
        var size: Double = 0.0

        switch type {
        case .byte:
            size = Double(data.count)
        case .kilobyte:
            size = Double(data.count) / 1024
        case .megabyte:
            size = Double(data.count) / 1024 / 1024
        case .gigabyte:
            size = Double(data.count) / 1024 / 1024 / 1024
        }
        //print("image size is %.2f \(size)")
        return String(format: "%.2f", size)
    }
    
    var attributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options:[.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print(error)
        }
        return nil
    }
}

// MARK: - UITextField Extension
extension UITextField
{
    func paddingView(xvalue:CGFloat)
    {
            let paddingView = UIView(frame: CGRect(x:xvalue, y: 10, width: 10, height: 20))
            self.leftView = paddingView
            self.leftViewMode = .always
    }
    func addInputViewDatePicker(target: Any, selector: Selector,selectorChange:Selector,formatType:String,from:String) {
        
        let screenWidth = UIScreen.main.bounds.width
        //Add DatePicker as inputView
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 200))
        datePicker.datePickerMode = .date
        if from == "AddUser" || from == "EditProfile"
        {
            datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -16, to: Date())
            datePicker.setDate(datePicker.maximumDate!, animated: false)
        }
        else if from == "PickupRequest"
        {
            datePicker.minimumDate = Date()
            datePicker.setDate(Date(), animated: false)
        }
        else
        {
            datePicker.maximumDate = Date()
            datePicker.setDate(Date(), animated: false)
        }
        if #available(iOS 14.0, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        
        self.inputView = datePicker
        
        //Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        datePicker.addTarget(target, action: selectorChange, for: .valueChanged)
        toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)
        
        self.inputAccessoryView = toolBar
    }
    
    func addCustomPicker(target: Any, selector: Selector) -> UIPickerView {
        
        let screenWidth = UIScreen.main.bounds.width
        //Add DatePicker as inputView
        let monthPicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 200))
        self.inputView = monthPicker
       
        //Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)
        
        self.inputAccessoryView = toolBar
        return monthPicker
    }
    @objc func cancelPressed() {
        self.resignFirstResponder()
    }
    func activeNextTextField(_ view: UIView) {
        let nextTag = self.tag + 1
        guard let nextResponder = view.viewWithTag(nextTag) as? UITextField? else {
            return
        }
        if nextResponder != nil {
            nextResponder?.becomeFirstResponder()
        } else {
            self.resignFirstResponder()
        }
    }
}

// MARK: - UIView Extension
extension UIView
{
    func topBarBGColor()
    {
        self.backgroundColor = Colors.theme_black_color
    }
    func dropShadow(cornerRadius : CGFloat = 10.0) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.init(red: 198/255, green: 198/255, blue: 198/255, alpha: 1.0).cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = cornerRadius
        //        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        //        self.layer.shouldRasterize = true
        //        self.layer.rasterizationScale = UIScreen.main.scale
    }
    func dropShadowForNotifyView() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.init(red: 198/255, green: 198/255, blue: 198/255, alpha: 1.0).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 5, height: 5)
        self.layer.shadowRadius = 3
    }
    func dropShadowForNewFeature() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.init(red: 198/255, green: 198/255, blue: 198/255, alpha: 0.5).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowRadius = 5
    }
    func complexShape() {
        let path = UIBezierPath()
        path.addCurve(to:CGPoint(x: self.frame.size.width, y: 50.0),
                      controlPoint1: CGPoint(x: self.frame.size.width + 50.0, y: 25.0),
                      controlPoint2: CGPoint(x: self.frame.size.width - 150.0, y: 50.0))
       
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        
//        self.backgroundColor = UIColor.orange
//        self.layer.mask = shapeLayer
        self.layer.addSublayer(shapeLayer)
    }
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    @IBInspectable var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    @IBInspectable var borderWidth : CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    func setDotBorder(newwidth:Int)
    {
        let color = Colors.theme_lightgray_color.cgColor
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.bounds.size
        let width = newwidth //UIScreen.main.bounds.size.width - 40
        let shapeRect = CGRect(x: 0, y: 0, width: width, height: Int(frameSize.height))
               
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: width/2, y: Int(frameSize.height)/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.bevel
        shapeLayer.lineDashPattern = [6,3]
        
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, byRoundingCorners: [.topLeft,.bottomLeft], cornerRadii: CGSize(width: 20, height: 20)).cgPath
       
        self.layer.addSublayer(shapeLayer)
    }
    
    func addShadowToCard(offset: CGSize, color: UIColor, radius: CGFloat, cornerRadius: CGFloat, isTopLeftTopRight : Bool, isBottomLeftBottomRight : Bool) {
        
        if isTopLeftTopRight {
            self.layer.cornerRadius = cornerRadius
            self.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        }
        
        if isBottomLeftBottomRight {
            self.layer.cornerRadius = cornerRadius
            self.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        }
        
        if !isTopLeftTopRight && !isBottomLeftBottomRight {
            self.layer.cornerRadius = 0
//            self.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner, .layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        }
        
        if isTopLeftTopRight && isBottomLeftBottomRight {
            self.layer.cornerRadius = cornerRadius
            self.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner, .layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        }
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
//        self.layer.masksToBounds = false
    }
    
    func setCornerRadiusTopLeftTopRight(cornerRadius: CGFloat){
        self.layer.cornerRadius = 10
        self.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
    }
    
    func setCornerRadiusBottomLeftBottomRight(cornerRadius: CGFloat){
        self.layer.cornerRadius = 10
        self.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }

    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }

    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }

    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBorderWithColor(color: UIColor, width: CGFloat) {
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
    }
    
    func addTopLeftRightBorderWithColor(color: UIColor, width: CGFloat){
        let topBorder = CALayer()
        topBorder.backgroundColor = color.cgColor
        topBorder.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        
        let leftBorder = CALayer()
        leftBorder.backgroundColor = color.cgColor
        leftBorder.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)

        let rightBorder = CALayer()
        rightBorder.backgroundColor = color.cgColor
        rightBorder.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)

        self.layer.addSublayer(topBorder)
        self.layer.addSublayer(leftBorder)
        self.layer.addSublayer(rightBorder)
    }
    
    func addBottomLeftRightBorderWithColor(color: UIColor, width: CGFloat){
        
        let bottomBorder = CALayer()
        bottomBorder.backgroundColor = color.cgColor
        bottomBorder.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        
        let leftBorder = CALayer()
        leftBorder.backgroundColor = color.cgColor
        leftBorder.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)

        let rightBorder = CALayer()
        rightBorder.backgroundColor = color.cgColor
        rightBorder.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        
        self.layer.addSublayer(bottomBorder)
        self.layer.addSublayer(leftBorder)
        self.layer.addSublayer(rightBorder)
    }
    
    func addLeftRightBorderWithColor(color: UIColor, width: CGFloat){
        let leftBorder = CALayer()
        leftBorder.backgroundColor = color.cgColor
        leftBorder.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)

        let rightBorder = CALayer()
        rightBorder.backgroundColor = color.cgColor
        rightBorder.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(leftBorder)
        self.layer.addSublayer(rightBorder)
    }
}

// MARK: - Sequence Extension
extension Sequence where Element: AdditiveArithmetic {
    func sum() -> Element { reduce(.zero, +) }
}

// MARK: - UIViewController Extension
extension UIViewController
{
    // MARK: - goToHomeVC
    func goToHomeVC()
    {
        UIApplication.shared.windows.first?.rootViewController = appDelegate.setUpSWRevealViewController()
    }
    
    // MARK: - gotoWalletVC
    func gotoWalletVC()
    {
        if self.navigationController != nil
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WalletVC.self)
                {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    // MARK: - isShowBiometricPopup
    func isShowBiometricPopup()
    {
        if UserDefaults.standard.value(forKey:kShowBiometricPopup) == nil
        {
            showBiometricPopup()
        }
    }
    
    // MARK: - goToEditProfileVC
    func goToEditProfileVC(userID:String)
    {
        if let vc = appDelegate.getViewController("EditProfileVC", onStoryboard: "Profile") as? EditProfileVC {
            vc.userID = userID
            vc.isFromVC = "Register"
            vc.isProfileRequired = 1
            if let tabBarVC = self.revealViewController()?.frontViewController as? UITabBarController
            {
                tabBarVC.selectedIndex = 3
            }
            if let profileNavVC = appDelegate.topNavigation()
            {
                profileNavVC.pushViewController(vc, animated: false)
            }
        }
    }
    
    // MARK: - goToEditProfileVCFromLogin
    func goToEditProfileVCFromLogin(userID:String)
    {
        //isProfileRequired - Flow Change -by Nand
        if let vc = appDelegate.getViewController("EditProfileVC", onStoryboard: "Profile") as? EditProfileVC {
            vc.userID = userID
            vc.isFromVC = "Register"
            vc.isProfileRequired = 1
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    // MARK: - showBiometricPopup
    func showBiometricPopup()
    {
        UserDefaults.standard.setValue(true, forKey: kShowBiometricPopup)
        if let popupVC = appDelegate.getViewController("BiometricPopupVC", onStoryboard: "Home") as? BiometricPopupVC {
            popupVC.modalPresentationStyle = .overFullScreen
            if let objNavigationController  = appDelegate.topNavigation()
            {
                objNavigationController.present(popupVC, animated: true)
            }
        }
    }
    
    // MARK: - configureTextField
    func configureTextField(textField: UITextField,placeHolder:String,font:String,fontSize:Float,textColor:UIColor,cornerRadius:Int,borderColor:UIColor,borderWidth:CGFloat,bgColor:UIColor)
    {
        if font != ""
        {
            textField.attributedPlaceholder = NSAttributedString(string: placeHolder,
                                                                 attributes: [NSAttributedString.Key.foregroundColor: Colors.theme_lightgray_color,
                                                                              NSAttributedString.Key.font : UIFont(name: font, size: CGFloat(fontSize))])
            textField.font =  UIFont.init(name: font, size: CGFloat(fontSize))
        }
        textField.layer.cornerRadius = CGFloat(cornerRadius)
        textField.layer.borderColor = borderColor.cgColor
        textField.layer.borderWidth = borderWidth
        textField.textColor = textColor
        textField.backgroundColor = bgColor
    }
    
    // MARK: - configureTextView
    func configureTextView(textField: UITextView,placeHolder:String,font:String,fontSize:Float,textColor:UIColor,cornerRadius:Int,borderColor:UIColor,borderWidth:CGFloat,bgColor:UIColor)
    {
        if font != ""
        {
            textField.attributedText = NSAttributedString(string: placeHolder,
                                                          attributes: [NSAttributedString.Key.foregroundColor: Colors.theme_lightgray_color,
                                                                       NSAttributedString.Key.font : UIFont(name: font, size: CGFloat(fontSize))])
            textField.font =  UIFont.init(name: font, size: CGFloat(fontSize))
        }
        textField.layer.cornerRadius = CGFloat(cornerRadius)
        textField.layer.borderColor = borderColor.cgColor
        textField.layer.borderWidth = borderWidth
        //textField.textColor = textColor
        textField.backgroundColor = bgColor
    }
    
    // MARK: - isCheckNull
    func isCheckNull(strText : String) -> Bool
    {
        if strText.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            return true
        }
        return false
    }
    
    // MARK: - isValidEmail
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK: - showAlert
    func showAlert(title:String,msg:String,vc:UIViewController)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            
        }
        action1.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(action1)
        vc.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - showAlertWithOk
    func showAlertWithOk(title:String,msg:String,vc:UIViewController,  completion: @escaping (_ status:Bool) -> Void)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            completion(true)
        }
        action1.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(action1)
        vc.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - showAlert
    func showAlert(title:String,msg:String, viewController : UIViewController, completion: @escaping (_ status:Bool) -> Void)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            completion(true)
        }
        let noAction = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
            completion(false)
        }
        yesAction.setValue(UIColor(named:"theme_black_color"), forKey: "titleTextColor")
        noAction.setValue(UIColor(named:"theme_black_color"), forKey: "titleTextColor")
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - popupAlert
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            action.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - showHUD
    func showHUD()
    {
        let loading = MBProgressHUD.showAdded(to:self.view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
        loading.bezelView.color = Colors.clearColor
    }
    
    // MARK: - hideHUD
    func hideHUD()
    {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // MARK: - showHUDOnView
    func showHUDOnView(view : UIView)
    {
        let loading = MBProgressHUD.showAdded(to:view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
        loading.bezelView.color = Colors.clearColor
    }
    
    // MARK: - hideHUDFromView
    func hideHUDFromView(view : UIView)
    {
        MBProgressHUD.hide(for: view, animated: true)
    }
    
    // MARK: - sourceFormatter
    func sourceFormatter() -> DateFormatter
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter
    }
    
    // MARK: - destinationFormatter
    func destinationFormatter() -> DateFormatter
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }
    
    // MARK: - isValidNewPassword
    func isValidNewPassword(strPwd:String) -> Bool {
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,12}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: strPwd)
    }
    
    // MARK: - showAlertForLogout
    func showAlertForLogout(title:String,msg:String,userID:String,type:String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            
            if type == "LogoutAll"
            {
                self.logoutAll()
            }
            else
            {
                let objUserInfo = getUserInfo()
                self.callAPIForLogout(userID:userID,isAction:"",boxID:objUserInfo.boxId ?? "",token:objUserInfo.token ?? "")
            }
        }
        let noAction = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        yesAction.setValue(UIColor(named:"theme_black_color"), forKey: "titleTextColor")
        noAction.setValue(UIColor(named:"theme_black_color"), forKey: "titleTextColor")
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - logoutAll
    func logoutAll()
    {
        let objManageCoreData = ManageCoreData()
        let results = objManageCoreData.fetchDataOfLoginTypewise(entityName:kSwitchUserEntity,LoginType:LoginType.AmazonLogin.rawValue)
        if results.count > 0
        {
            self.showHUD()
            AMZNAuthorizationManager.shared().signOut({ error in
                if error == nil {
                    self.callAPIForLogoutAll()
                }
                else
                {   self.hideHUD()
                    self.showAlert(title: kAppName, msg: Messsages.msg_pls_try_again, vc: self)
                }
            })
        }
        else
        {
            self.callAPIForLogoutAll()
        }
    }
    
    // MARK: - callAPIForLogout
    func callAPIForLogout(userID:String,isAction:String,boxID:String,token:String)
    {
        
        let objUserInfo = getUserInfo()
        if checkLoginType(boxID:boxID)
        {
            var params : [String:Any] = [:]
            params["user_id"] = userID
            params["unique_id"] = uniqueID
            params["token"] = token
            params["device_type"] = kDeviceType
            params["device_name"] = kDeviceName
            params["device_brand"] = kDeviceBrand
            params["os_version"] = kOSVersion
            params["app_version"] = kVersion
            params["api_version"] = apiVersion
            self.showHUDOnView(view: self.view)
            URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.Logout, showLoader: false) { (resultDict, status, message) in
                
                self.hideHUDFromView(view: self.view)

                if status == true
                {
                    //new change for core data apr- 2021
                    let objManageCoreData = ManageCoreData()
                    objManageCoreData.updateActiveStatus(entityName: kSwitchUserEntity, BoxId: boxID ,status: false)
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    //over
                    
                    if loginType == LoginType.AppLogin.rawValue
                    {
                        if isAction == "Remove"
                        {
                            self.removeRecordFromDB(boxID:boxID)
                        }
                        else
                        {
                            self.PostLogoutAction()
                        }
                    }
                    else if loginType == LoginType.AppleLogin.rawValue
                    {
                        //new change in apr-2021
                        
                        if isAction == "Remove"
                        {
                            self.removeRecordFromDB(boxID:boxID)
                        }
                        else
                        {
                            self.PostLogoutAction()
                        }
                    }
                    else if loginType == LoginType.AmazonLogin.rawValue
                    {
                        AMZNAuthorizationManager.shared().signOut({ error in
                            if error == nil {
                                
                                if isAction == "Remove"
                                {
                                    self.hideHUD()
                                    self.removeRecordFromDB(boxID:boxID)
                                }
                                else
                                {
                                    self.PostLogoutAction()
                                }
                            }
                            else
                            {
                                self.hideHUD()
                                self.showAlert(title: kAppName, msg: Messsages.msg_pls_try_again, vc: self)
                            }
                        })
                    }
                    
                }
                else
                {
                    if isAction == "Remove"
                    {
                        DispatchQueue.main.async {
                            self.hideHUD()
                        }
                    }
                    appDelegate.showToast(message: Messsages.msg_pls_try_again, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    
    // MARK: - showConfirmationPopup
    func showConfirmationPopup(title:String,msg:String, completion: @escaping (_ status:Bool) -> Void)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
            completion(true)
            alert.dismiss(animated: true, completion: nil)
        }
        let noAction = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
            completion(false)
            alert.dismiss(animated: true, completion: nil)
        }
        yesAction.setValue(UIColor(named:"theme_black_color"), forKey: "titleTextColor")
        noAction.setValue(UIColor(named:"theme_black_color"), forKey: "titleTextColor")
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - checkLoginType
    func checkLoginType(boxID:String) -> Bool
    {
        let objManageCoreData = ManageCoreData()
        let results = objManageCoreData.recordExists(BoxId:boxID , entityName: kSwitchUserEntity)
        if results.count > 0
        {
            if let records = results as? [NSManagedObject]
            {
                let item = records[0]
                
//                for key in item.entity.propertiesByName.keys{
//                    let value: Any? = item.value(forKey: key)
//                    print("KEY = \(key): Value = \(value)")
//                }
                
                if let login_type =  item.value(forKey: SwitchUserInfo.loginType.rawValue) as? String
                {
                    uniqueID = item.value(forKey: SwitchUserInfo.uniqueID.rawValue) as? String ?? ""
                    loginType = login_type
                    UserDefaults.standard.setValue(login_type, forKey: kTypeOfLogin)
                    return true
                }
            }
        }
        return false
    }
    
    // MARK: - PostLogoutAction
    func PostLogoutAction(fromVC : UIViewController = UIViewController())
    {
//        let updateLater = UserDefaults.standard.value(forKey: kUpdateLater)
//        let dontAskMeAgain = UserDefaults.standard.value(forKey: kDontAskMeAgain)
        let versionPopupCounter : Int = UserDefaults.standard.integer(forKey: kVersionPopupCounter)
        let prefIosVersion = UserDefaults.standard.value(forKey: kApiIOSVersion) as? String
        let isDarkModeEnabled = UserDefaults.standard.value(forKey: kIsDarkModeEnabled)
        let isLightModeEnabled = UserDefaults.standard.value(forKey: kIsLightModeEnabled)
        let isSystemModeEnabled = UserDefaults.standard.value(forKey: kIsSystemModeModeEnabled)
        let isAccountClosureShown = UserDefaults.standard.value(forKey: kIsAccountClosureShown)
        let isVideoPalyerShown = UserDefaults.standard.value(forKey: kIsVideoTutorialShown)
        
        let decodedSegments  = UserDefaults.standard.data(forKey: kArrSegmentOptons)
        
        let fcmtoken = UserDefaults.standard.value(forKey: kDeviceToken) ?? ""
        let username = UserDefaults.standard.value(forKey: kUsername) ?? ""
        let currentAppstoreVersion = UserDefaults.standard.value(forKey: kCurrentAppStoreVersion) ?? ""
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(fcmtoken, forKey: kDeviceToken)
        UserDefaults.standard.setValue(username, forKey: kUsername)
        UserDefaults.standard.setValue(true, forKey: kShowBiometricPopup)
        UserDefaults.standard.setValue(currentAppstoreVersion, forKey: kCurrentAppStoreVersion)
        UserDefaults.standard.setValue(versionPopupCounter, forKey: kVersionPopupCounter)
//        UserDefaults.standard.setValue(updateLater, forKey: kUpdateLater)
//        UserDefaults.standard.setValue(dontAskMeAgain, forKey: kDontAskMeAgain)
        UserDefaults.standard.setValue(prefIosVersion, forKey: kApiIOSVersion)
        UserDefaults.standard.setValue(isDarkModeEnabled, forKey: kIsDarkModeEnabled)
        UserDefaults.standard.setValue(isLightModeEnabled, forKey: kIsLightModeEnabled)
        UserDefaults.standard.setValue(isSystemModeEnabled, forKey: kIsSystemModeModeEnabled)
        UserDefaults.standard.setValue(isAccountClosureShown, forKey: kIsAccountClosureShown)
        UserDefaults.standard.setValue(isVideoPalyerShown, forKey: kIsVideoTutorialShown)
        
        //print("prefIosVersion on logout \(prefIosVersion)")

//        This old code is now replaced by below new code - SegmentOptions
//        if decodedSegments != nil {
//            if let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decodedSegments!) as? [SegmentOptions] {
//                appDelegate.arrOptions = decodedTeams
//            }
//        }
        
        if let arrData = UserDefaults.standard.decode(for: [SegmentOptions].self, using: String(describing: SegmentOptions.self)){
            appDelegate.arrOptions = arrData
        }
        
        getSegmentOptons()
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let navvc = storyboard.instantiateViewController(withIdentifier: "navvc") as? CustomNavigationVC {
            print("navvc is \(navvc)")
            print("navvc.viewControllers is \(navvc.viewControllers)")
//            appDelegate.appNav = navvc
//            if self.revealViewController() == nil {
//                if fromVC.revealViewController() != nil {
//                    fromVC.revealViewController().pushFrontViewController(navvc, animated: true)
//                }
//            }else{
//                self.revealViewController().pushFrontViewController(navvc, animated: true)
//            }
            if self.revealViewController() == nil {
                if fromVC.revealViewController() != nil {
                    fromVC.revealViewController().pushFrontViewController(navvc, animated: true)
                }else{
                    UIApplication.shared.windows.first?.rootViewController = appDelegate.setUpSWRevealViewController()
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    if appDelegate.objSWRevealViewController != nil
                    {
                        appDelegate.objSWRevealViewController?.pushFrontViewController(navvc, animated: true)
                    }
                }
            }else{
                self.revealViewController().pushFrontViewController(navvc, animated: true)
            }
        }
    }
    
    // MARK: - callAPIForLogoutAll
    func callAPIForLogoutAll()
    {
        let objUserInfo = getUserInfo()
        var params : [String:Any] = [:]
        params["unique_id"] =  objUserInfo.uniqueId ?? ""
        params["device_type"] = kDeviceType
        params["device_name"] = kDeviceName
        params["device_brand"] = kDeviceBrand
        params["os_version"] = kOSVersion
        params["app_version"] = kVersion
        params["api_version"] = apiVersion
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.LogoutAll, showLoader: true) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()
                let objManageCoreData = ManageCoreData()
                objManageCoreData.updateStatus(entityName: kSwitchUserEntity, fieldName: "isActive", value: false) { (status) in
                    if status == true
                    {
                        UIApplication.shared.applicationIconBadgeNumber = 0
                        self.postLogoutActionForLogoutAll()
                    }
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - postLogoutActionForLogoutAll
    func postLogoutActionForLogoutAll()
    {
        
//        let updateLater = UserDefaults.standard.value(forKey: kUpdateLater)
//        let dontAskMeAgain = UserDefaults.standard.value(forKey: kDontAskMeAgain)
        let versionPopupCounter : Int = UserDefaults.standard.integer(forKey: kVersionPopupCounter)
        let prefIosVersion = UserDefaults.standard.value(forKey: kApiIOSVersion) as? String
        let isDarkModeEnabled = UserDefaults.standard.value(forKey: kIsDarkModeEnabled)
        let isLightModeEnabled = UserDefaults.standard.value(forKey: kIsLightModeEnabled)
        let isSystemModeEnabled = UserDefaults.standard.value(forKey: kIsSystemModeModeEnabled)
        let isAccountClosureShown = UserDefaults.standard.value(forKey: kIsAccountClosureShown)
        let isVideoPalyerShown = UserDefaults.standard.value(forKey: kIsVideoTutorialShown)
        
        let decodedSegments  = UserDefaults.standard.data(forKey: kArrSegmentOptons)
        
        let fcmtoken = UserDefaults.standard.value(forKey: kDeviceToken) ?? ""
        let isBioMetricEnabled = UserDefaults.standard.value(forKey: kIsBiometricEnabled) ?? false
        let currentAppstoreVersion = UserDefaults.standard.value(forKey: kCurrentAppStoreVersion) ?? ""
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(fcmtoken, forKey: kDeviceToken)
        UserDefaults.standard.setValue(true, forKey: kShowBiometricPopup)
        UserDefaults.standard.setValue(currentAppstoreVersion, forKey: kCurrentAppStoreVersion)
        UserDefaults.standard.setValue(versionPopupCounter, forKey: kVersionPopupCounter)
//        UserDefaults.standard.setValue(updateLater, forKey: kUpdateLater)
//        UserDefaults.standard.setValue(dontAskMeAgain, forKey: kDontAskMeAgain)
        UserDefaults.standard.setValue(prefIosVersion, forKey: kApiIOSVersion)
        UserDefaults.standard.setValue(isDarkModeEnabled, forKey: kIsDarkModeEnabled)
        UserDefaults.standard.setValue(isLightModeEnabled, forKey: kIsLightModeEnabled)
        UserDefaults.standard.setValue(isSystemModeEnabled, forKey: kIsSystemModeModeEnabled)
        UserDefaults.standard.setValue(isAccountClosureShown, forKey: kIsAccountClosureShown)
        UserDefaults.standard.setValue(isVideoPalyerShown, forKey: kIsVideoTutorialShown)
        
        //print("prefIosVersion on logout all \(prefIosVersion)")
        //  UserDefaults.standard.setValue(isBioMetricEnabled, forKey: kIsBiometricEnabled) //apr-2021

//        This old code is now replaced by below new code - SegmentOptions
//        if decodedSegments != nil {
//            if let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decodedSegments!) as? [SegmentOptions] {
//                appDelegate.arrOptions = decodedTeams
//            }
//        }
        
        if let arrData = UserDefaults.standard.decode(for: [SegmentOptions].self, using: String(describing: SegmentOptions.self)){
            appDelegate.arrOptions = arrData
        }
        
        getSegmentOptons()
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let navvc = storyboard.instantiateViewController(withIdentifier: "navvc") as? CustomNavigationVC {
            appDelegate.appNav = navvc
            self.revealViewController().pushFrontViewController(navvc, animated: true)
        }
    }
    
    // MARK: - callAPIForDeleteAccount
    func callAPIForDeleteAccount(userID:String,boxID:String,token:String, completion: @escaping (_ status:Bool) -> Void)
    {
        completion(true)
    }
    
    // MARK: - deleteUserIdentifierFromKeychain
    static func deleteUserIdentifierFromKeychain() {
        do {
            try KeychainItem(service: Bundle.main.bundleIdentifier ?? "", account: "userIdentifier").deleteItem()
            try KeychainItem(service:Bundle.main.bundleIdentifier ?? "", account: "userEmail").deleteItem()
            try KeychainItem(service: Bundle.main.bundleIdentifier ?? "", account: "userFirstName").deleteItem()
            try KeychainItem(service: Bundle.main.bundleIdentifier ?? "", account: "userLastName").deleteItem()
            
        } catch {
            print("Unable to delete userIdentifier from keychain")
        }
    }
    
    // MARK: - saveCoreData
    func saveCoreData(objUserInfo:UserInfo,TypeOfLogin:String)
    {
        //new change in apr-2021 core data
        let objManageCoreData = ManageCoreData()
        objManageCoreData.saveData(entityName: kSwitchUserEntity , userData: objUserInfo,isDefault: true,LoginType:TypeOfLogin)
    }
    
    // MARK: - removeRecordFromDB
    func removeRecordFromDB(boxID:String)
    {
        let objManageCoreData = ManageCoreData()
        objManageCoreData.deleteRecord(entityName: kSwitchUserEntity, BoxId: boxID) { (status) in
            if status == true
            {
                NotificationCenter.default.post(name: Notification.Name("DBRecordDeleted"), object: nil,userInfo: nil)
            }
        }
    }
    
    // MARK: - callAPIForSwitchProfile
    func callAPIForSwitchProfile(objSwitchAccount:SwitchAccount)
    {
        var params : [String:Any] = [:]
        params["user_id"] = objSwitchAccount.userID
        params["device_token"] = UserDefaults.standard.value(forKey: kDeviceToken) ?? ""
        params["device_type"] = kDeviceType
        params["unique_id"]  = objSwitchAccount.uniqueID
        kSwitchProfileUserToken = objSwitchAccount.authToken
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.SwitchProfile, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                let objUserInfo = UserInfo(fromJson: JSON(resultDict))
                //UserInfo.shared.setData(fromJson: JSON(resultDict))
                UserDefaults.standard.setValue((resultDict as NSDictionary).value(forKey: "token"), forKey: kAuthToken)
                UserDefaults.standard.setValue(true, forKey: kLoggedIn)
                UserDefaults.standard.setValue(objSwitchAccount.LoginType,forKey: kTypeOfLogin)
                
//                This old code is now replaced by below new code
//                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: objUserInfo)
//                UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                
//                self.saveCoreData(objUserInfo:objUserInfo, TypeOfLogin: objSwitchAccount.LoginType ?? "") //need to change,new change core data
//                self.goToHomeVC()
                
                do {
                    let encodedData = try NSKeyedArchiver.archivedData(withRootObject: objUserInfo, requiringSecureCoding: true)
                    UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                    
                    self.saveCoreData(objUserInfo:objUserInfo, TypeOfLogin: objSwitchAccount.LoginType) //need to change,new change core data
                    self.goToHomeVC()
                } catch {
                    print("NSKeyedArchiver Error is : \(error.localizedDescription)")
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - createDriveThruRequest - DriveThru
    func createDriveThruRequest(userId : String, invoiceId : String, pickupDate : String, pickupTime : String,selectedDateToDisplay : String, selectedTimeToDisplay : String,type : Int, resultDate : [String : Any]?,fromVC : UIViewController)
    {
        var params : [String:Any] = [:]
        params["user_id"] = userId
        params["invoice_id"] = invoiceId
        params["pickup_date"] = pickupDate
        params["pickup_time"] = pickupTime
        
        print("drive thru request params are \(params)")
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driveThruRequest, showLoader: true) { (resultDict, status, message) in
            if status == true
            {
                print("drive thru request response is \(resultDict)")
                if type == 1 {
                    if let vc = appDelegate.getViewController("DriveThruRequestSuccessVC", onStoryboard: "DriveThru") as? DriveThruRequestSuccessVC{
                        vc.successPopupType = "1"
                        vc.resultDate = resultDate
                        vc.strSelectedDate = selectedDateToDisplay
                        vc.strSelectedTime = selectedTimeToDisplay
                        vc.totalPackages = invoiceId.components(separatedBy: ",").count
                        vc.strSuccessMsg = message
                        NotificationCenter.default.post(name: Notification.Name("refreshDriveThruRequestList"), object: nil)
                        fromVC.navigationController?.pushViewController(vc, animated: true)
                    }
                }else if type == 2 {
                    if let vc = appDelegate.getViewController("DriveThruRequestSuccessVC", onStoryboard: "DriveThru") as? DriveThruRequestSuccessVC{
                        vc.successPopupType = "2"
                        vc.resultDate = resultDate
                        vc.strSelectedDate = selectedDateToDisplay
                        vc.strSelectedTime = selectedTimeToDisplay
                        vc.totalPackages = invoiceId.components(separatedBy: ",").count
                        vc.strSuccessMsg = message
                        NotificationCenter.default.post(name: Notification.Name("refreshDriveThruRequestList"), object: nil)
                        
                        fromVC.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else
            {
                NotificationCenter.default.post(name: Notification.Name("refreshAddDriveThruRequestList"), object: nil)
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                fromVC.navigationController?.popViewController(animated: true)
            }
        }
    }
}

// MARK: - UIStackView Extension
extension UIStackView {
    
    // MARK: - addBorder
    func addBorder(color: UIColor, backgroundColor: UIColor, thickness: CGFloat) {
        let insetView = UIView(frame: bounds)
        insetView.backgroundColor = backgroundColor
        insetView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(insetView, at: 0)

        let borderBounds = CGRect(
            x: thickness,
            y: thickness,
            width: frame.size.width - thickness * 2,
            height: frame.size.height - thickness * 2)

        let borderView = UIView(frame: borderBounds)
        borderView.backgroundColor = color
        borderView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(borderView, at: 0)
    }
}

// MARK: - Bundle Extension
extension Bundle {
    
    // MARK: - versionNumber
    var versionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    // MARK: - buildNumber
    var buildNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}

// MARK: - UILabel Extension
extension UILabel {
    
    // MARK: - addTrailing
    func addTrailing(str:String,trailingText:String) -> NSMutableAttributedString
    {
        let underlineAttriString = NSMutableAttributedString(string: str)
        let range1 = (str as NSString).range(of: trailingText)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: fontname.openSansRegular, size: 14)!, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.theme_black_color, range: range1)
        return underlineAttriString
    }
    
    // MARK: - startBlink
    func startBlink() {
        UIView.animate(withDuration: 0.5,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0 },
                       completion: nil)
    }
    
    // MARK: - stopBlink
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}

// MARK: - String Extension
extension String {
    
    // MARK: - htmlToAttributedString
    func htmlToAttributedString(size: CGFloat, hexStringColor: String, fontFamily : String) -> NSAttributedString? {
        let htmlTemplate = """
        <!doctype html>
        <html>
          <head>
            <style>
              body {
                color: \(hexStringColor);
                font-family: \(fontFamily);
                font-size: \(size)px;
              }
            </style>
          </head>
          <body>
            \(self)
          </body>
        </html>
        """

        guard let data = htmlTemplate.data(using: .utf8) else {
            return nil
        }

        guard let attributedString = try? NSAttributedString(
            data: data,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
            documentAttributes: nil
            ) else {
            return nil
        }

        return attributedString
    }
    
    // MARK: - initials
    var initials: String {
        return self.components(separatedBy: " ")
            .reduce("") {
                ($0.isEmpty ? "" : "\($0.first?.uppercased() ?? "")") +
                ($1.isEmpty ? "" : "\($1.first?.uppercased() ?? "")")
            }
    }
    
    // MARK: - setMarquee
    func setMarquee(size: CGFloat, hexStringColor: String, fontFamily : String) -> NSAttributedString? {
        let htmlTemplate = """
        <!doctype html>
        <html>
          <head>
            <style>
              body {
                color: \(hexStringColor);
                font-family: \(fontFamily);
                font-size: \(size)px;
              }
            </style>
          </head>
          <body>
            <marquee>
                \(self)
            </marquee>
          </body>
        </html>
        """

        guard let data = htmlTemplate.data(using: .utf8) else {
            return nil
        }

        guard let attributedString = try? NSAttributedString(
            data: data,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
            documentAttributes: nil
            ) else {
            return nil
        }

        return attributedString
    }
    
    // MARK: - isNumber
    var isNumber: Bool {
        let digitsCharacters = CharacterSet(charactersIn: "0123456789")
        return CharacterSet(charactersIn: self).isSubset(of: digitsCharacters)
    }
    
    // MARK: - data
    var data: Data {
        return Data(utf8)
    }
    
    // MARK: - getTimestampFromDateString
    func getTimestampFromDateString(formate : String) -> TimeInterval{
        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = formate
        let date = dateFormatter.date(from:self)!
        
        return date.timeIntervalSince1970
    }
    
    // MARK: - getDateFromTimeStamp
    func getDateFromTimeStamp(formate : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = formate
        let date = dateFormatter.date(from:self)!
        return date
    }
    
    // MARK: - getFileName
    var getFileName: String{
        URL(string: self)!.deletingPathExtension().lastPathComponent
    }
    
    // MARK: - getFileExtension
    var getFileExtension: String{
        URL(string: self)!.pathExtension
    }
    
    // MARK: - getFileNameWithExtension
    var getFileNameWithExtension: String{
        URL(string: self)!.lastPathComponent
    }
    
    // MARK: - getHeightForLable
    func getHeightForLable(width: CGFloat,
                           numberOfLines: Int = 0,
                           font: UIFont) -> CGFloat {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = self
        label.sizeToFit()
        return label.frame.height
    }
    
    // MARK: - setHTMLFromString
    func setHTMLFromString(text: String) -> NSAttributedString
    {
        var modifiedFont : NSString?
        if isDarkTheme()
        {
            modifiedFont = NSString(format:"<span style=\"font-family: \(fontname.openSansRegular);color:white; font-size: \(15)\">%@</span>" as NSString, text)
        }
        else
        {
            modifiedFont = NSString(format:"<span style=\"font-family: \(fontname.openSansRegular);color:black; font-size: \(15)\">%@</span>" as NSString, text)
        }
        let attrStr = try! NSAttributedString(
            data: modifiedFont!.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        return attrStr
        //self.attributedText = attrStr
    }
    
    // MARK: - isDarkTheme
    func isDarkTheme() -> Bool
    {
        if #available(iOS 13, *)
        {
            if UITraitCollection.current.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return true
            } else {
                /// Return the color for Light Mode
                return false
            }
            
        } else {
            /// Return a fallback color for iOS 12 and lower.
            return false
        }
    }
    
    // MARK: - stringByRemovingAll
    func stringByRemovingAll(subStrings: [String]) -> String {
        var resultString = self
        subStrings.map { resultString = resultString.replacingOccurrences(of: $0, with: "") }
        return resultString
    }
}

// MARK: - UITextView Extension
extension UITextView
{
    // MARK: - setHTMLFromString
    func setHTMLFromString(text: String) -> NSAttributedString
    {
        var modifiedFont : NSString?
        if isDarkTheme()
        {
            modifiedFont = NSString(format:"<span style=\"font-family: \(fontname.openSansRegular);color:white; font-size: \(15)\">%@</span>" as NSString, text)
        }
        else
        {
            modifiedFont = NSString(format:"<span style=\"font-family: \(fontname.openSansRegular);color:black; font-size: \(15)\">%@</span>" as NSString, text)
        }
        let attrStr = try! NSAttributedString(
            data: modifiedFont!.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        return attrStr
        //self.attributedText = attrStr
    }
    
    // MARK: - isDarkTheme
    func isDarkTheme() -> Bool
    {
        if #available(iOS 13, *)
        {
            if UITraitCollection.current.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return true
            } else {
                /// Return the color for Light Mode
                return false
            }
            
        } else {
            /// Return a fallback color for iOS 12 and lower.
            return false
        }
    }
    func adjustUITextViewHeight() {
        self.sizeToFit()
        self.isScrollEnabled = false
    }
}

// MARK: - Date Extension
extension Date {
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }

    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
    
    // MARK: - minutes
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    // MARK: - getDates
    func getDates(dateFormate : String, fromDate : Date ,forNDays nDays: Int, isNext : Bool) -> ([String]){
        let cal = NSCalendar.current
        // start with today
//        var date : Date = Date()
        var date : Date = fromDate

        if !isNext {
            date = Calendar.current.date(byAdding: .day, value: 1, to: fromDate)!
        }else{
            date = cal.startOfDay(for: fromDate)
        }

        var arrDates = [String]()

        for _ in 1 ... nDays {
            // move back in time by one day:
            if isNext {
                date = cal.date(byAdding: Calendar.Component.day, value: 1, to: date)!
            }else {
                date = cal.date(byAdding: Calendar.Component.day, value: -1, to: date)!
            }
            arrDates.append(getDateToExpectedFormate(dateVal: date, formate: dateFormate))
        }
        return (arrDates)
    }
    
    // MARK: - getDateToExpectedFormate
    func getDateToExpectedFormate(dateVal : Date, formate : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        let dateString = dateFormatter.string(from: dateVal)
        return dateString
    }
    
    // MARK: - adding
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}

// MARK: - UIDevice Extension
public extension UIDevice {
    //https://medium.com/ios-os-x-development/get-model-info-of-ios-devices-18bc8f32c254
    
    static let deviceIdentifier : String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        func mapToDeviceIdentifier(identifier: String) -> String {
            return identifier
        }
        
        return mapToDeviceIdentifier(identifier: identifier)
    }()
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String {
#if os(iOS)
            switch identifier {
            case "iPod5,1":                                           return "iPod Touch 5"
            case "iPod7,1":                                           return "iPod Touch 6"
            case "iPod9,1":                                           return "iPod Touch 7"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":               return "iPhone 4"
            case "iPhone4,1":                                         return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                            return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                            return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                            return "iPhone 5s"
            case "iPhone7,2":                                         return "iPhone 6"
            case "iPhone7,1":                                         return "iPhone 6 Plus"
            case "iPhone8,1":                                         return "iPhone 6s"
            case "iPhone8,2":                                         return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                            return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                            return "iPhone 7 Plus"
            case "iPhone8,4":                                         return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                          return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                          return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                          return "iPhone X"
            case "iPhone11,2":                                        return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                          return "iPhone XS Max"
            case "iPhone11,8":                                        return "iPhone XR"
            case "iPhone12,1":                                        return "iPhone 11"
            case "iPhone12,3":                                        return "iPhone 11 Pro"
            case "iPhone12,5":                                        return "iPhone 11 Pro Max"
            case "iPhone12,8":                                        return "iPhone SE (2nd generation)"
            case "iPhone13,1":                                        return "iPhone 12 mini"
            case "iPhone13,2":                                        return "iPhone 12"
            case "iPhone13,3":                                        return "iPhone 12 Pro"
            case "iPhone13,4":                                        return "iPhone 12 Pro Max"
            case "iPhone14,4":                                        return "iPhone 13 mini"
            case "iPhone14,5":                                        return "iPhone 13"
            case "iPhone14,2":                                        return "iPhone 13 Pro"
            case "iPhone14,3":                                        return "iPhone 13 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":          return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":                     return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":                     return "iPad 4"
            case "iPad6,11", "iPad6,12":                              return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                                return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                              return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                              return "iPad (8th generation)"
            case "iPad12,1", "iPad12,2":                              return "iPad (9th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":                     return "iPad Air"
            case "iPad5,3", "iPad5,4":                                return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                              return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                              return "iPad Air (4th generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":                     return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":                     return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":                     return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                                return "iPad Mini 4"
            case "iPad11,1", "iPad11,2":                              return "iPad mini (5th generation)"
            case "iPad14,1", "iPad14,2":                              return "iPad mini (6th generation)"
            case "iPad6,3", "iPad6,4":                                return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                                return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":          return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                               return "iPad Pro (11-inch) (2nd generation)"
            case "iPad13,4", "iPad13,5", "iPad13,6", "iPad13,7":      return "iPad Pro (11-inch) (3rd generation)"
            case "iPad6,7", "iPad6,8":                                return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                                return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":          return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                              return "iPad Pro (12.9-inch) (4th generation)"
            case "iPad13,8", "iPad13,9", "iPad13,10", "iPad13,11":    return "iPad Pro (12.9-inch) (5th generation)"
            case "AppleTV5,3":                                        return "Apple TV"
            case "AppleTV6,2":                                        return "Apple TV 4K"
            case "AudioAccessory1,1":                                 return "HomePod"
            case "AudioAccessory5,1":                                 return "HomePod mini"
            case "i386", "x86_64":                                    return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
#elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
#endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
}

// MARK: - Int Extension
extension Int {
    var byteSize: String {
        return ByteCountFormatter().string(fromByteCount: Int64(self))
    }
}

// MARK: - isDarkTheme
func isDarkTheme() -> Bool
{
    if #available(iOS 13, *)
    {
        if UITraitCollection.current.userInterfaceStyle == .dark {
            /// Return the color for Dark Mode
            return true
        } else {
            /// Return the color for Light Mode
            return false
        }
        
    } else {
        /// Return a fallback color for iOS 12 and lower.
        return false
    }
}

// MARK: - getSegmentOptons
func getSegmentOptons(){
   
    if appDelegate.arrOptions.count > 0 {
        //print("tabs are exist")

        if appDelegate.arrOptions.contains(where: { $0.optionName == Segments.UpcomingInvoice.rawValue }) {
             // found
            return
        } else {
             // not
            let obj10 : SegmentOptions = SegmentOptions()
            obj10.optionName = Segments.UpcomingInvoice.rawValue
            obj10.refNo = InvoiceType.Upcoming.dispValue()
            obj10.isChecked = "0"
            obj10.index = SegmentsIndex.UpcomingInvoice.rawValue
            appDelegate.arrOptions.append(obj10)
            appDelegate.saveMangeTabsData()
        }
        return
    }
    
    let obj1 : SegmentOptions = SegmentOptions()
    obj1.optionName = Segments.MyInvoice.rawValue
    obj1.refNo = InvoiceType.MyInvoice.dispValue()
    obj1.isChecked = "1"
    obj1.index = SegmentsIndex.MyInvoice.rawValue
    appDelegate.arrOptions.append(obj1)
    
    let obj2 : SegmentOptions = SegmentOptions()
    obj2.optionName = Segments.UnpaidInvoice.rawValue
    obj2.refNo = InvoiceType.UnpaidInvoice.dispValue()
    obj2.isChecked = "1"
    obj2.index = SegmentsIndex.UnpaidInvoice.rawValue
    appDelegate.arrOptions.append(obj2)
    
    let obj3 : SegmentOptions = SegmentOptions()
    obj3.optionName = Segments.PackageRequest.rawValue
    obj3.refNo = InvoiceType.PackageRequest.dispValue()
    obj3.isChecked = "1"
    obj3.index = SegmentsIndex.PackageRequest.rawValue
    appDelegate.arrOptions.append(obj3)
    
    let obj4 : SegmentOptions = SegmentOptions()
    obj4.optionName = Segments.PaidInvoice.rawValue
    obj4.refNo = InvoiceType.PaidInvoice.dispValue()
    obj4.isChecked = "0"
    obj4.index = SegmentsIndex.PaidInvoice.rawValue
    appDelegate.arrOptions.append(obj4)
    
    let obj5 : SegmentOptions = SegmentOptions()
    obj5.optionName = Segments.Archived.rawValue
    obj5.refNo = InvoiceType.Archived.dispValue()
    obj5.isChecked = "0"
    obj5.index = SegmentsIndex.Archived.rawValue
    appDelegate.arrOptions.append(obj5)
    
    let obj6 : SegmentOptions = SegmentOptions()
    obj6.optionName = Segments.InTransit.rawValue
    obj6.refNo = InvoiceType.InTransit.dispValue()
    obj6.isChecked = "0"
    obj6.index = SegmentsIndex.InTransit.rawValue
    appDelegate.arrOptions.append(obj6)
    
    let obj7 : SegmentOptions = SegmentOptions()
    obj7.optionName = Segments.Pickup.rawValue
    obj7.refNo = InvoiceType.Pickup.dispValue()
    obj7.isChecked = "0"
    obj7.index = SegmentsIndex.Pickup.rawValue
    appDelegate.arrOptions.append(obj7)
    
    let obj8 : SegmentOptions = SegmentOptions()
    obj8.optionName = Segments.OnDelivery.rawValue
    obj8.refNo = InvoiceType.OnDelivery.dispValue()
    obj8.isChecked = "0"
    obj8.index = SegmentsIndex.OnDelivery.rawValue
    appDelegate.arrOptions.append(obj8)
    
    let obj9 : SegmentOptions = SegmentOptions()
    obj9.optionName = Segments.Refunded.rawValue
    obj9.refNo = InvoiceType.Refunded.dispValue()
    obj9.isChecked = "0"
    obj9.index = SegmentsIndex.Refunded.rawValue
    appDelegate.arrOptions.append(obj9)
    
    let obj10 : SegmentOptions = SegmentOptions()
    obj10.optionName = Segments.UpcomingInvoice.rawValue
    obj10.refNo = InvoiceType.Upcoming.dispValue()
    obj10.isChecked = "0"
    obj10.index = SegmentsIndex.UpcomingInvoice.rawValue
    appDelegate.arrOptions.append(obj10)
    
    appDelegate.saveMangeTabsData()    
}

// MARK: - getInvoiceActions
func getInvoiceActions(isShowCartOption : Bool, isShowOpenTicketOption : Bool) -> [InvoiceActions]{

    var arrInvoiceActions = [InvoiceActions]()
    
    let obj1 : InvoiceActions = InvoiceActions()
    obj1.actionName = InvoiceActionsName.GenerateTicket.rawValue
    obj1.actionImage = InvoiceActionsImage.Support.rawValue
    obj1.imageBackColor = InvoiceActionsColor.Support.rawValue
    arrInvoiceActions.append(obj1)

    let obj2 : InvoiceActions = InvoiceActions()
    obj2.actionName = InvoiceActionsName.InvoiceDetails.rawValue
    obj2.actionImage = InvoiceActionsImage.View.rawValue
    obj2.imageBackColor = InvoiceActionsColor.View.rawValue
    arrInvoiceActions.append(obj2)

    let obj3 : InvoiceActions = InvoiceActions()
    obj3.actionName = InvoiceActionsName.DownloadInvoice.rawValue
    obj3.actionImage = InvoiceActionsImage.Download.rawValue
    obj3.imageBackColor = InvoiceActionsColor.Download.rawValue
    arrInvoiceActions.append(obj3)

    if isShowCartOption {
        let obj4 : InvoiceActions = InvoiceActions()
        obj4.actionName = InvoiceActionsName.ViewYourCart.rawValue
        obj4.actionImage = InvoiceActionsImage.AddToCart.rawValue
        obj4.imageBackColor = InvoiceActionsColor.AddToCart.rawValue
        arrInvoiceActions.append(obj4)
    }
    
    if isShowOpenTicketOption {
        let obj5 : InvoiceActions = InvoiceActions()
        obj5.actionName = InvoiceActionsName.PackageRequest.rawValue
        obj5.actionImage = InvoiceActionsImage.PackageRequest.rawValue
        obj5.imageBackColor = InvoiceActionsColor.PackageRequest.rawValue
        arrInvoiceActions.append(obj5)
    }

    return arrInvoiceActions
}

// MARK: - hexStringToUIColor
func hexStringToUIColor (hex:String, alpha : CGFloat) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }

    if ((cString.count) != 6) {
        return UIColor.gray
    }

    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(alpha)
    )
}

// MARK: - IBDesignable
@IBDesignable
class TapAndCopyLabel: UILabel {

    override func awakeFromNib() {
        super.awakeFromNib()
        //1.Here i am Adding UILongPressGestureRecognizer by which copy popup will Appears
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGesture(_:)))
        self.addGestureRecognizer(gestureRecognizer)
        self.isUserInteractionEnabled = true
    }

    // MARK: - UIGestureRecognizer
    //https://stackoverflow.com/questions/1246198/show-iphone-cut-copy-paste-menu-on-uilabel
    @objc func handleLongPressGesture(_ recognizer: UIGestureRecognizer) {
        guard recognizer.state == .recognized else { return }
        
        let copiedText = text?.stringByRemovingAll(subStrings: ["BOX ID : ","Hi "]) ?? ""
        
        if copiedText.count > 0 {
            //print("Copied to clipboard")
            UIPasteboard.general.string = copiedText
            appDelegate.showToast(message: Messsages.msg_copied_to_clipboard, bottomValue: getSafeAreaValue(), isForSuccess: true)
        }
    }
}

// MARK: - showTotalRecordsTitle
func showTotalRecordsTitle(selectedRecords : Int,totalRecords :  Int) -> String{
    
    if selectedRecords > 0 {
        //return "Selected"
        return ""
    }else {
        return ""
        //return "Total"
    }
}

// MARK: - showTotalRecordsValue
func showTotalRecordsValue(selectedRecords : Int,totalRecords :  Int, suffix : String) -> String{
    
    if selectedRecords > 0 {
        return "Selected - \(selectedRecords) / \(totalRecords) \(suffix)"
    }else {
        return "Total - \(totalRecords) \(suffix)"
    }
}

// MARK: - isSystemDarkModeOn
func isSystemDarkModeOn() -> Bool {
    
    if #available(iOS 13.0, *) {
        if UITraitCollection.current.userInterfaceStyle == .dark {
            print("Dark mode")
            return true
        }
        else {
            print("Light mode")
            return false
        }
    }
    return false
}

// MARK: - enableAppAppearanceMode
func enableAppAppearanceMode(){
    DispatchQueue.main.async {
        
        if #available(iOS 13.0, *) {
            let appDel = UIApplication.shared.windows.first
            
            if (UserDefaults.standard.value(forKey: kIsSystemModeModeEnabled)) == nil{
                UserDefaults.standard.set(true, forKey: kIsSystemModeModeEnabled)
                UserDefaults.standard.set(false, forKey: kIsLightModeEnabled)
                UserDefaults.standard.set(false, forKey: kIsDarkModeEnabled)
                appDel?.overrideUserInterfaceStyle = .unspecified
            }else {
                if (UserDefaults.standard.value(forKey: kIsSystemModeModeEnabled) as? Bool == true){
                    if isSystemDarkModeOn() {
                        appDel?.overrideUserInterfaceStyle = .dark
                    }else{
                        appDel?.overrideUserInterfaceStyle = .light
                    }
                }else{
                    if let isDarkModeEnabled = UserDefaults.standard.value(forKey: kIsDarkModeEnabled) as? Bool, isDarkModeEnabled == true
                    {
                        appDel?.overrideUserInterfaceStyle = .dark
                    }
                    else
                    {
                        appDel?.overrideUserInterfaceStyle = .light
                    }
                }
            }
        }
    }
}

// MARK: - checkCameraAccess
func checkCameraAccess(completion: @escaping (_ status:Bool) -> Void) {
    switch AVCaptureDevice.authorizationStatus(for: .video) {
    case .denied:
        print("Denied, request permission from settings")
        completion(false)
    case .restricted:
        print("Restricted, device owner must approve")
        completion(false)
    case .authorized:
        print("Authorized, proceed")
        completion(true)
    case .notDetermined:
        AVCaptureDevice.requestAccess(for: .video) { success in
            if success {
                print("Permission granted, proceed")
                completion(true)
            } else {
                print("Permission denied")
                completion(false)
            }
        }
    default : break
    }
}
    
// MARK: - openCameraAccessPopup
func openCameraAccessPopup(title:String,msg:String, fromVC : UIViewController, completion: @escaping (_ status:Bool) -> Void)
{
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    let yesAction = UIAlertAction(title: "Settings", style: .default) { (action:UIAlertAction) in
        completion(true)
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                // Handle
            })
        }
        fromVC.dismiss(animated: true, completion: nil)
    }
    let noAction = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
        completion(false)
        fromVC.dismiss(animated: true, completion: nil)
    }
    yesAction.setValue(UIColor(named:"theme_black_color"), forKey: "titleTextColor")
    noAction.setValue(UIColor(named:"theme_black_color"), forKey: "titleTextColor")
    alert.addAction(yesAction)
    alert.addAction(noAction)
    fromVC.present(alert, animated: true, completion: nil)
}

// MARK: - isValidatePromotionalCode
func isValidatePromotionalCode(promotionalCode : String) -> Bool{
    if promotionalCode.count < 5 {
        print("Please enter Promotional Code lenght > 5 ")
        appDelegate.showToast(message: "Promotional Code should be at least 5 character long", bottomValue: getSafeAreaValue())
        return false
    }
    return true
}

// MARK: - getDateToExpectedFormate - DriveThru
func getDateToExpectedFormate(dateVal : Date, formate : String) -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = formate
    let dateString = dateFormatter.string(from: dateVal)
    return dateString
}

// MARK: - removeCurrentDateFromDateArr
func removeCurrentDateFromDateArr(arrPickupSettingList : [PickupSettingModel]) -> [PickupSettingModel]{
    var pickupSettingList : [PickupSettingModel] = arrPickupSettingList
    
    let tmpCurrentDateString = getDateToExpectedFormate(dateVal: Date(), formate: "yyyy-MM-dd")
    print("tmpCurrentDateString is \(tmpCurrentDateString)")

    if arrPickupSettingList.contains(where: {$0.date == tmpCurrentDateString}) {
        let objTemp : PickupSettingModel = arrPickupSettingList.filter({ $0.date == tmpCurrentDateString })[0]

        let removedIndex = arrPickupSettingList.firstIndex(of: objTemp)

        print("objTemp is \(objTemp)")
        print("removedIndex data is \(String(describing: removedIndex))")
        pickupSettingList.remove(at: removedIndex!)
        
    }else{
        print("Given date obj not found")
    }
    return pickupSettingList
}
