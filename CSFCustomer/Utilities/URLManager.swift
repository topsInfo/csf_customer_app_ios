//
//  URLManager.swift
//  FMContractorCustomer
//
//  Created by WedoWebApps on 05/02/18.
//  Copyright © 2018 WedoWebApps. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
import MBProgressHUD
import LoginWithAmazon

// MARK: - URLManager
class URLManager : NSObject
{
    static let shared =  URLManager()
    let retryLimit = 3
    var mainSession: Session!
    let tokenSession = Alamofire.Session()
    var authInterceptor: OAuthInterceptor!
    private override init(){
        let configuration = URLSessionConfiguration.default
        self.authInterceptor = OAuthInterceptor(baseURLString: baseURL)
        mainSession = Session(configuration: configuration, interceptor: self.authInterceptor)
    }
    
    // MARK: - URLCall
    func URLCall(method: Alamofire.HTTPMethod, parameters:[String:Any],header : Bool, url:String,showLoader:Bool,interceptor: RequestInterceptor? = nil,completion: @escaping (_ resultDict: [String:Any],_ status:Bool,_ message:String) -> Void)
    {
        let resultDict : [String:Any] = [String:Any]()
        var _ : String = ""
        var headers : HTTPHeaders?
        var urlFinal : String = ""
        
        if(!Reachability.isConnectedToNetwork())
        {
            if showLoader
            {
                appDelegate.hideLoader()
            }
            appDelegate.showToast(message: Messsages.msg_no_internet_connection, bottomValue: getSafeAreaValue())
            completion(resultDict,false,"")
            return
        }
        if showLoader
        {
            appDelegate.showLoader()
        }
        if header == true
        {
            var authToken : String = ""
            if url == APICall.SwitchProfile
            {
                authToken = kSwitchProfileUserToken
            }
            else
            {
                authToken = UserDefaults.standard.value(forKey: kAuthToken) as? String ?? ""
            }
            headers = [
                "Authorization" : authToken != "" ? "Bearer \(authToken)" : ""
            ]
        }
        else
        {
            headers = [:]
        }
        //url
        urlFinal = baseURL + url
        print("url is \(urlFinal)")
        print("params are \(parameters)")
        print("headers is \(String(describing: headers))")

        AF.request(urlFinal, method: method, parameters: parameters, encoding:URLEncoding.default,headers:headers).responseJSON{ (response) in
            
            if showLoader
            {
                appDelegate.hideLoader()
            }
            print("url final is \(urlFinal)")
            
            if response.response?.statusCode == 200
            {
                if showLoader
                {
                    appDelegate.hideLoader()
                    kSwitchProfileUserToken = ""
                }
                switch response.result
                {
                case let .success(result):
                    print("Result is: \(result)")
                    if let dict = result as? [String:Any]
                    {
                        if let status = dict["status"] as? Bool, status == true
                        {
                            let resultDict = dict["data"] as? [String:Any] ?? [:]
                            completion(resultDict,true,dict["message"] as? String ?? "")
                        }
                        else
                        {
                            completion(resultDict,false,dict["message"] as? String ?? "")
                        }
                    }
                    break
                case let .failure(error):
                    // Handle the error.
                    print("Error description is: \(error.localizedDescription)")
                    let res = response.result
                    if let dict = res as? [String:Any]
                    {
                        completion(resultDict,false,dict["message"] as? String ?? "")
                    }
                    else
                    {
                        completion(resultDict,false,"")
                    }
                }
            }
            else if response.response?.statusCode == 401
            {
                if showLoader
                {
                    appDelegate.hideLoader()
                    kSwitchProfileUserToken = ""
                }
                switch response.result
                {
                case let .success(result):
                    //print("Result is: \(result)")
                    if let dict = result as? [String:Any]
                    {
                        if let status = dict["status"] as? Bool, status == true
                        {
                            completion(resultDict,false,dict["message"] as? String ?? "")
                        }
                        else
                        {
                            if let tokenStatus = dict["token_expired"] as? Bool, tokenStatus == true
                            {
                                //new change in apr-2021
                                if let msg = dict["message"] as? String, msg != ""
                                {
                                    appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                                }
                                self.logoutAllFromURL() //need to uncomment
                            }
                            else
                            {
                                completion(resultDict,false,dict["message"] as? String ?? "")
                            }
                        }
                    }
                    break
                case let .failure(error):
                    // Handle the error.
                    print("Error description is: \(error.localizedDescription)")
                    let result = response.result
                    if let dict = result as? [String:Any]
                    {
                        completion(resultDict,false,dict["message"] as? String ?? "")
                    }
                }
            }//else
            else
            {
                if showLoader
                {
                    appDelegate.hideLoader()
                    kSwitchProfileUserToken = ""
                }
                completion(resultDict,false,"")
            }
        }
    }
    
    // MARK: - URLCallMultipartData - EditProfileVC
    func URLCallMultipartData(method: Alamofire.HTTPMethod, parameters:[String:Any],header : Bool, url:String,showLoader:Bool,WithName:String,docFileName:String,uploadData:Data,interceptor: RequestInterceptor? = nil,mimeType:String,completion: @escaping (_ resultDict: [String:Any],_ status:Bool,_ message:String) -> Void) {
        var resultDict : [String:Any] = [String:Any]()
        var _ : String = ""
        var headers : HTTPHeaders?
        var urlFinal : String = ""
        
        if(!Reachability.isConnectedToNetwork())
        {
            if showLoader
            {
                appDelegate.hideLoader()
            }
            appDelegate.showToast(message: Messsages.msg_no_internet_connection, bottomValue: getSafeAreaValue())
            completion(resultDict,false,"")
            return
        }
        if showLoader
        {
            appDelegate.showLoader()
        }
        if header == true
        {
            let authToken = UserDefaults.standard.value(forKey: kAuthToken) as? String ?? ""
            headers = [
                "Authorization" : authToken != "" ? "Bearer \(authToken)" : "",
                "Content-type": "multipart/form-data",
                "Content-Disposition" : "form-data"
            ]
        }
        else
        {
            headers = [:]
        }
        //url
        urlFinal = baseURL + url
        print("url \(urlFinal)")
        print("params are \(parameters)")
        guard let url = URL(string: urlFinal) else {
            return
        }
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "POST"
        urlRequest.headers = headers!
        
        AF.upload(multipartFormData: {  MultipartFormData in
            for (key, value) in parameters {
                if let temp = value as? String {
                    MultipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    MultipartFormData.append("\(temp)".data(using: .utf8)!, withName: key )
                }
            }
            if uploadData != nil{
                MultipartFormData.append(uploadData, withName: "\(WithName)", fileName: docFileName, mimeType: mimeType)
            }
        }, with: urlRequest)
        .uploadProgress(closure: { (Progress) in
            print("Upload Progress: \(Progress.fractionCompleted)")
        })
        .responseJSON(completionHandler: { data in
            switch data.result {
            case .success:
                appDelegate.hideLoader()
                
                if let dictionary = data.value as? NSDictionary,
                   let status = dictionary.value(forKey: "status") as? Bool,
                   status == true {
                    let msg = dictionary.value(forKey: "message") as? String ?? ""
                    
                    if let dictData = dictionary.value(forKey: "data") as? [String : Any]{
                        resultDict = dictData
                    }
                    completion(resultDict,status,msg)
                } else {
                    if let dictionary = data.value as? NSDictionary,
                       let tokenStatus = dictionary.value(forKey: "token_expired") as? Bool,
                       tokenStatus == true {
                        if let msg = dictionary.value(forKey:"message") as? String, msg != ""{
                            appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                        }
                        self.logoutAllFromURL()
                    }else{
                        if let dictionary = data.value as? NSDictionary{
                            let msg = dictionary.value(forKey:"message") as? String ?? ""
                            completion(resultDict,false,msg)
                        }
                    }
                }
                break
            case .failure:
                print("failure")
                appDelegate.hideLoader()
                
                if let dictionary = data.value as? NSDictionary{
                    if let msg = dictionary.value(forKey:"message") as? String {
                        completion(resultDict,false,msg)
                    }else{
                        completion(resultDict,false,Messsages.msg_pls_try_again)
                    }
                }
                break
            }
        })
    }

    // MARK: - URLCallMultipleMultipartData - ReportVC
    func URLCallMultipleMultipartData(method: Alamofire.HTTPMethod, parameters:[String:Any],header : Bool, url:String,showLoader:Bool,WithName:[String],docFileName:[String],uploadData:[Data],interceptor: RequestInterceptor? = nil,mimeType:[String],completion: @escaping (_ resultDict: [String:Any],_ status:Bool,_ message:String) -> Void) {
        var resultDict : [String:Any] = [String:Any]()
        var _ : String = ""
        var headers : HTTPHeaders?
        var urlFinal : String = ""
        
        if(!Reachability.isConnectedToNetwork())
        {
            if showLoader
            {
                appDelegate.hideLoader()
            }
            appDelegate.showToast(message: Messsages.msg_no_internet_connection, bottomValue: getSafeAreaValue())
            completion(resultDict,false,"")
            return
        }
        if showLoader
        {
            appDelegate.showLoader()
        }
        if header == true
        {
            let authToken = UserDefaults.standard.value(forKey: kAuthToken) as? String ?? ""
            headers = [
                "Authorization" : authToken != "" ? "Bearer \(authToken)" : "",
                "Content-type": "multipart/form-data",
                "Content-Disposition" : "form-data"
            ]
        }
        else
        {
            headers = [:]
        }
        print("headers \(String(describing: headers))")
        //url
        urlFinal = baseURL + url
        guard let url = URL(string: urlFinal) else {
            return
        }
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "POST"
        urlRequest.headers = headers!
        
        AF.upload(multipartFormData: {  MultipartFormData in
            
            for (key, value) in parameters {
                if let temp = value as? String {
                    MultipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    MultipartFormData.append("\(temp)".data(using: .utf8)!, withName: key )
                }
                
                if let temp = value as? [Data] {
                    var index : Int = 0
                    for imgData in temp {
                        MultipartFormData.append(imgData, withName: String(format: "documents[\(index)]"), fileName: "documents[\(index)]", mimeType: mimeType[index])
                        index += 1
                    }
                }
                
            }
        }, with: urlRequest)
        .uploadProgress(closure: { (Progress) in
            print("Upload Progress: \(Progress.fractionCompleted)")
        })
        .responseJSON(completionHandler: { data in
            switch data.result {
            case .success:
                appDelegate.hideLoader()

                if let dictionary = data.value as? NSDictionary,
                   let status = dictionary.value(forKey: "status") as? Bool,
                   status == true {
                    let msg = dictionary.value(forKey: "message") as? String ?? ""
                    
                    if let dictData = dictionary.value(forKey: "data") as? [String : Any]{
                        resultDict = dictData
                    }
                    completion(resultDict,status,msg)
                } else {
                    if let dictionary = data.value as? NSDictionary,
                       let tokenStatus = dictionary.value(forKey: "token_expired") as? Bool,
                       tokenStatus == true {
                        if let msg = dictionary.value(forKey:"message") as? String, msg != ""{
                            appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                        }
                        self.logoutAllFromURL()
                    }else{
                        if let dictionary = data.value as? NSDictionary{
                            let msg = dictionary.value(forKey:"message") as? String ?? ""
                            completion(resultDict,false,msg)
                        }
                    }
                }
                break
            case .failure:
                print("failure")
                appDelegate.hideLoader()

                if let dictionary = data.value as? NSDictionary{
                    if let msg = dictionary.value(forKey:"message") as? String {
                        completion(resultDict,false,msg)
                    }else{
                        completion(resultDict,false,Messsages.msg_pls_try_again)
                    }
                }
                break
            }
        })
    }
    
    // MARK: - URLCallMultipleMultipartTicketData - GenerateTicketVC
    func URLCallMultipleMultipartTicketData(method: Alamofire.HTTPMethod, parameters:[String:Any],header : Bool, url:String,showLoader:Bool,WithName:[String],docFileName:[String],uploadData:[Data],interceptor: RequestInterceptor? = nil,mimeType:[String],completion: @escaping (_ resultDict: [String:Any],_ status:Bool,_ message:String) -> Void) {
        var resultDict : [String:Any] = [String:Any]()
        var _ : String = ""
        var headers : HTTPHeaders?
        var urlFinal : String = ""
        
        if(!Reachability.isConnectedToNetwork())
        {
            if showLoader
            {
                appDelegate.hideLoader()
            }
            appDelegate.showToast(message: Messsages.msg_no_internet_connection, bottomValue: getSafeAreaValue())
            completion(resultDict,false,"")
            return
        }
        if showLoader
        {
            appDelegate.showLoader()
        }
        if header == true
        {
            let authToken = UserDefaults.standard.value(forKey: kAuthToken) as? String ?? ""
            headers = [
                "Authorization" : authToken != "" ? "Bearer \(authToken)" : "",
                "Content-type": "multipart/form-data",
                "Content-Disposition" : "form-data"
            ]
        }
        else
        {
            headers = [:]
        }
        print("headers \(headers)")
        //url
        urlFinal = baseURL + url
        guard let url = URL(string: urlFinal) else {
            return
        }
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "POST"
        urlRequest.headers = headers!
        
        AF.upload(multipartFormData: {  MultipartFormData in
            
            for (key, value) in parameters {
                if let temp = value as? String {
                    MultipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    MultipartFormData.append("\(temp)".data(using: .utf8)!, withName: key )
                }
                
                if let temp = value as? [Data] {
                    var index : Int = 0
                    for imgData in temp {
                        MultipartFormData.append(imgData, withName: String(format: "attachment[\(index)]"), fileName: "attachment[\(index)]", mimeType: mimeType[index])
                        index += 1
                    }
                }
            }
        }, with: urlRequest)
        .uploadProgress(closure: { (Progress) in
            print("Upload Progress: \(Progress.fractionCompleted)")
        })
        .responseJSON(completionHandler: { data in
            switch data.result {
            case .success:
                appDelegate.hideLoader()
                
                if let dictionary = data.value as? NSDictionary,
                   let status = dictionary.value(forKey: "status") as? Bool,
                   status == true {
                    let msg = dictionary.value(forKey: "message") as? String ?? ""
                    
                    if let dictData = dictionary.value(forKey: "data") as? [String : Any]{
                        resultDict = dictData
                    }
                    completion(resultDict,status,msg)
                } else {
                    if let dictionary = data.value as? NSDictionary,
                       let tokenStatus = dictionary.value(forKey: "token_expired") as? Bool,
                       tokenStatus == true {
                        if let msg = dictionary.value(forKey:"message") as? String, msg != ""{
                            appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                        }
                        self.logoutAllFromURL()
                    }else{
                        if let dictionary = data.value as? NSDictionary{
                            let msg = dictionary.value(forKey:"message") as? String ?? ""
                            completion(resultDict,false,msg)
                        }
                    }
                }
                break
            case .failure:
                print("failure")
                appDelegate.hideLoader()
                
                if let dictionary = data.value as? NSDictionary{
                    if let msg = dictionary.value(forKey:"message") as? String {
                        completion(resultDict,false,msg)
                    }else{
                        completion(resultDict,false,Messsages.msg_pls_try_again)
                    }
                }
                break
            }
        })
    }
    
    // MARK: - URLCallMultipleMultipartDataForPreAlert - PreAlertVC
    func URLCallMultipleMultipartDataForPreAlert(method: Alamofire.HTTPMethod, parameters:[String:Any],header : Bool, url:String,showLoader:Bool,WithName:[String],docFileName:[String],uploadData:[Data],interceptor: RequestInterceptor? = nil,mimeType:[String],completion: @escaping (_ resultDict: [String:Any],_ status:Bool,_ message:String) -> Void) {
        var resultDict : [String:Any] = [String:Any]()
        var _ : String = ""
        var headers : HTTPHeaders?
        var urlFinal : String = ""
        
        if(!Reachability.isConnectedToNetwork())
        {
            if showLoader
            {
                appDelegate.hideLoader()
            }
            appDelegate.showToast(message: Messsages.msg_no_internet_connection, bottomValue: getSafeAreaValue())
            completion(resultDict,false,"")
            return
        }
        if showLoader
        {
            appDelegate.showLoader()
        }
        if header == true
        {
            let authToken = UserDefaults.standard.value(forKey: kAuthToken) as? String ?? ""
            headers = [
                "Authorization" : authToken != "" ? "Bearer \(authToken)" : "",
                "Content-type": "multipart/form-data",
                "Content-Disposition" : "form-data"
            ]
        }
        else
        {
            headers = [:]
        }
        print("headers \(headers)")
        //url
        urlFinal = baseURL + url
        guard let url = URL(string: urlFinal) else {
            return
        }
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "POST"
        urlRequest.headers = headers!
        
        AF.upload(multipartFormData: {  MultipartFormData in
            
            for (key, value) in parameters {
                if let temp = value as? String {
                    MultipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    MultipartFormData.append("\(temp)".data(using: .utf8)!, withName: key )
                }
                
                if let temp = value as? [Data] {
                    
                    var index : Int = 0
                    for imgData in temp {
                        MultipartFormData.append(imgData, withName: String(format: "document[]"), fileName: WithName[index], mimeType: mimeType[index])
                        index += 1
                    }
                }
            }
        }, with: urlRequest)
        .uploadProgress(closure: { (Progress) in
            print("Upload Progress: \(Progress.fractionCompleted)")
        })
        .responseJSON(completionHandler: { data in
            switch data.result {
            case .success:
                appDelegate.hideLoader()

                if let dictionary = data.value as? NSDictionary,
                   let status = dictionary.value(forKey: "status") as? Bool,
                   status == true {
                    let msg = dictionary.value(forKey: "message") as? String ?? ""
                    
                    if let dictData = dictionary.value(forKey: "data") as? [String : Any]{
                        resultDict = dictData
                    }
                    completion(resultDict,status,msg)
                } else {
                    if let dictionary = data.value as? NSDictionary,
                       let tokenStatus = dictionary.value(forKey: "token_expired") as? Bool,
                       tokenStatus == true {
                        if let msg = dictionary.value(forKey:"message") as? String, msg != ""{
                            appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                        }
                        self.logoutAllFromURL()
                    }else{
                        if let dictionary = data.value as? NSDictionary{
                            let msg = dictionary.value(forKey:"message") as? String ?? ""
                            completion(resultDict,false,msg)
                        }
                    }
                }
                break
            case .failure:
                print("failure")
                appDelegate.hideLoader()

                if let dictionary = data.value as? NSDictionary{
                    if let msg = dictionary.value(forKey:"message") as? String {
                        completion(resultDict,false,msg)
                    }else{
                        completion(resultDict,false,Messsages.msg_pls_try_again)
                    }
                }
                break
            }
        })
    }
    
    // MARK: - URLCallMultipleMultipartDataForFeedback - FeedbackVC
    func URLCallMultipleMultipartDataForFeedback(method: Alamofire.HTTPMethod, parameters:[String:Any],header : Bool, url:String,showLoader:Bool,WithName:[String],docFileName:[String],uploadData:[Data],interceptor: RequestInterceptor? = nil,mimeType:[String],completion: @escaping (_ resultDict: [String:Any],_ status:Bool,_ message:String) -> Void) {
        var resultDict : [String:Any] = [String:Any]()
        var _ : String = ""
        var headers : HTTPHeaders?
        var urlFinal : String = ""
        
        if(!Reachability.isConnectedToNetwork())
        {
            if showLoader
            {
                appDelegate.hideLoader()
            }
            appDelegate.showToast(message: Messsages.msg_no_internet_connection, bottomValue: getSafeAreaValue())
            completion(resultDict,false,"")
            return
        }
        if showLoader
        {
            appDelegate.showLoader()
        }
        if header == true
        {
            let authToken = UserDefaults.standard.value(forKey: kAuthToken) as? String ?? ""
            headers = [
                "Authorization" : authToken != "" ? "Bearer \(authToken)" : "",
                "Content-type": "multipart/form-data",
                "Content-Disposition" : "form-data"
            ]
        }
        else
        {
            headers = [:]
        }
        print("headers \(String(describing: headers))")
        //url
        urlFinal = baseURL + url
        guard let url = URL(string: urlFinal) else {
            return
        }
        var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
        urlRequest.httpMethod = "POST"
        urlRequest.headers = headers!
        
        AF.upload(multipartFormData: {  MultipartFormData in
            
            for (key, value) in parameters {
                if let temp = value as? String {
                    MultipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    MultipartFormData.append("\(temp)".data(using: .utf8)!, withName: key )
                }
                
                if let temp = value as? [Data] {
                    
                    var index : Int = 0
                    for imgData in temp {
                        MultipartFormData.append(imgData, withName: String(format: "document[]"), fileName: WithName[index], mimeType: mimeType[index])
                        index += 1
                    }
                }
                
            }
        }, with: urlRequest)
        .uploadProgress(closure: { (Progress) in
            print("Upload Progress: \(Progress.fractionCompleted)")
        })
        .responseJSON(completionHandler: { data in
            switch data.result {
            case .success:
                appDelegate.hideLoader()
                
                if let dictionary = data.value as? NSDictionary,
                   let status = dictionary.value(forKey: "status") as? Bool,
                   status == true {
                    let msg = dictionary.value(forKey: "message") as? String ?? ""
                    
                    if let dictData = dictionary.value(forKey: "data") as? [String : Any]{
                        resultDict = dictData
                    }
                    completion(resultDict,status,msg)
                } else {
                    if let dictionary = data.value as? NSDictionary,
                       let tokenStatus = dictionary.value(forKey: "token_expired") as? Bool,
                       tokenStatus == true {
                        if let msg = dictionary.value(forKey:"message") as? String, msg != ""{
                            appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                        }
                        self.logoutAllFromURL()
                    }else{
                        if let dictionary = data.value as? NSDictionary{
                            let msg = dictionary.value(forKey:"message") as? String ?? ""
                            completion(resultDict,false,msg)
                        }
                    }
                }
                break
            case .failure:
                print("failure")
                appDelegate.hideLoader()
                
                if let dictionary = data.value as? NSDictionary{
                    if let msg = dictionary.value(forKey:"message") as? String {
                        completion(resultDict,false,msg)
                    }else{
                        completion(resultDict,false,Messsages.msg_pls_try_again)
                    }
                }
                break
            }
        })
    }

    // MARK: - callAPIForLogoutAllFromURL
    func callAPIForLogoutAllFromURL()
    {
        let objUserInfo = getUserInfo()
        var params : [String:Any] = [:]
        params["unique_id"] =  objUserInfo.uniqueId ?? ""
        params["device_type"] = kDeviceType
        params["device_name"] = kDeviceName
        params["device_brand"] = kDeviceBrand
        params["os_version"] = kOSVersion
        params["app_version"] = kVersion
        params["api_version"] = apiVersion
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.LogoutAll, showLoader: true) { (resultDict, status, message) in
            if status == true
            {
                appDelegate.hideLoader()
                let objManageCoreData = ManageCoreData()
                objManageCoreData.updateStatus(entityName: kSwitchUserEntity, fieldName: "isActive", value: false) { (status) in
                    if status == true
                    {
                        UIApplication.shared.applicationIconBadgeNumber = 0
                        self.postLogoutActionForLogoutAllFromURL()
                    }
                }
            }
            else
            {
                appDelegate.hideLoader()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - showAlert
    func showAlert(message:String)
    {
        let alertController = UIAlertController(title: kAppName, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            alertController.dismiss(animated: true, completion: nil)
        }
        action1.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alertController.addAction(action1)
        if UIApplication.shared.windows.first?.rootViewController != nil
        {
            DispatchQueue.main.async {
                UIApplication.shared.windows.first?.rootViewController?.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - logoutAllFromURL
    func logoutAllFromURL()
    {
        let objManageCoreData = ManageCoreData()
        let results = objManageCoreData.fetchDataOfLoginTypewise(entityName:kSwitchUserEntity,LoginType:LoginType.AmazonLogin.rawValue)
        if results.count > 0
        {
            appDelegate.showLoader()
            AMZNAuthorizationManager.shared().signOut({ error in
                if error == nil {
                    self.callAPIForLogoutAllFromURL()
                }
                else
                {
                    appDelegate.hideLoader()
                    appDelegate.showToast(message: Messsages.msg_pls_try_again, bottomValue: getSafeAreaValue())
                }
            })
        }
        else
        {
            self.callAPIForLogoutAllFromURL()
        }
    }
    
    // MARK: - postLogoutActionForLogoutAllFromURL
    func postLogoutActionForLogoutAllFromURL()
    {
        let decodedSegments  = UserDefaults.standard.data(forKey: kArrSegmentOptons)
        
        let fcmtoken = UserDefaults.standard.value(forKey: kDeviceToken) ?? ""
        let isBioMetricEnabled = UserDefaults.standard.value(forKey: kIsBiometricEnabled) ?? false
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(fcmtoken, forKey: kDeviceToken)
        UserDefaults.standard.setValue(true, forKey: kShowBiometricPopup)
        //  UserDefaults.standard.setValue(isBioMetricEnabled, forKey: kIsBiometricEnabled) //apr-2021
        
        //        This old code is now replaced by below new code - SegmentOptions
        //        if decodedSegments != nil {
        //            if let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decodedSegments!) as? [SegmentOptions] {
        //                appDelegate.arrOptions = decodedTeams
        //            }
        //        }
        
        if let arrData = UserDefaults.standard.decode(for: [SegmentOptions].self, using: String(describing: SegmentOptions.self)){
            appDelegate.arrOptions = arrData
        }
        
        getSegmentOptons()
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let navvc = storyboard.instantiateViewController(withIdentifier: "navvc") as? CustomNavigationVC {
            appDelegate.appNav = navvc
            if appDelegate.objSWRevealViewController != nil
            {
                appDelegate.objSWRevealViewController?.pushFrontViewController(navvc, animated: true)
            }
        }
    }

    // MARK: - appleAuthTokenRevoke
    func appleAuthTokenRevoke(completion: @escaping (_ status:Bool, _ errorMsg : String) -> Void) {
        //https://stackoverflow.com/questions/72399534/how-to-make-apple-sign-in-revoke-token-post-request
        let clientId : String = Bundle.main.bundleIdentifier ?? ""
        let clientSecrete : String = KeychainItem.clientSecrete ?? ""
        let token : String = KeychainItem.currentUserToken ?? ""
        
        let jsonString = "client_id=\(clientId)&client_secret=\(clientSecrete)&token=\(token)"
        let jsonData = jsonString.data(using: .utf8)
        
        let url = URL(string: APICall.revokeAppleToken)!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        request.httpBody = jsonData
        
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            
            var errMsg : String = ""
            
            guard let response = response as? HTTPURLResponse, error == nil else {
                print("error", error ?? URLError(.badServerResponse))
                if let errorMsg : String = error?.localizedDescription {
                    errMsg = errorMsg
                }
                completion(false, errMsg)
                return
            }
            print("response = \(response)")
            
            if response.statusCode == 200 {
                print("Success")
                completion(true, errMsg)
                return
            }else {
                completion(false, errMsg)
                print("Failure")
                return
            }
        }
        task.resume()
    }
    
    // MARK: - appleAuth
    func appleAuth(completion: (([String: Any]?, Error?) -> Void)? = nil) {
        //https://stackoverflow.com/questions/72399534/how-to-make-apple-sign-in-revoke-token-post-request
        let paramString: [String : Any] = [
            "client_id": Bundle.main.bundleIdentifier!, //"com.MyCompany.Name",
            "client_secret": KeychainItem.clientSecrete ?? "",//self.clientSecret,
            "grant_type": "refresh_token",//refresh_token or  authorization_code
        ]
        print("appleAuth paramas are \(paramString)")
        
        let url = URL(string: "https://appleid.apple.com/auth/token")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: paramString, options: .prettyPrinted)
        }
        catch let error {
            print(error.localizedDescription)
            completion?(nil, error)
        }
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            guard let response = response as? HTTPURLResponse, error == nil else {
                print("error", error ?? URLError(.badServerResponse))
                return
            }
            
            guard (200 ... 299) ~= response.statusCode else {
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            if let error = error {
                print(error)
            }
            else {
                print("deleted accont")
            }
        }
        task.resume()
    }
}
extension Data {
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}
