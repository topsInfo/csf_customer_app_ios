//
//  URLManager+RequestInterCeptor.swift
//  CSFCustomer
//
//  Created by Tops on 30/11/20.
//

import UIKit
import Alamofire
//class OAuthInterceptor : RequestInterceptor
//{
//
//    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
//        var request = urlRequest
//        guard let token = UserDefaults.standard.value(forKey: kAuthToken) else {
//            completion(.success(urlRequest))
//            return
//        }
//        let bearerToken = "Bearer \(token)"
//        request.setValue(bearerToken, forHTTPHeaderField: "Authorization")
//        print("\nadapted; token added to the header field is: \(bearerToken)\n")
//        completion(.success(request))
//    }
//
//    func retry(_ request: Request, for session: Session, dueTo error: Error,
//               completion: @escaping (RetryResult) -> Void) {
//        guard request.retryCount < retryLimit else {
//            completion(.doNotRetry)
//            return
//        }
//        print("\nretried; retry count: \(request.retryCount)\n")
//        refreshToken { isSuccess in
//            isSuccess ? completion(.retry) : completion(.doNotRetry)
//        }
//    }
//
//    func refreshToken(completion: @escaping (_ isSuccess: Bool) -> Void) {
//        let params = ["token": UserDefaults.standard.value(forKey: "token") as? String ?? ""] as [String:Any]
//        URLManager.URLCall(method: .post, parameters: params, header: false, url: APICall.RefreshToken, hideLoader: true) { (resultDict, status, message) in
//            if status == true
//            {
//                print("completion")
//
//            }
//        }
//    }
//}
class OAuthInterceptor: RequestInterceptor {
    private var baseURLString: String
    public var accessToken: String?
    
    // MARK: - Initialization
    
    public init(baseURLString: String) {
        self.baseURLString = baseURLString
    }
    
    // MARK: - RequestAdapter
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (AFResult<URLRequest>) -> Void) {
        //completion(.success(urlRequest))
//        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(baseURLString) {
//            var adaptedRequest = urlRequest
//
//            if let sToken = accessToken {
//                adaptedRequest.setValue("Bearer \(sToken)", forHTTPHeaderField: "Authorization")
//                print("🔑 >> AuthToken appended: \(urlString)")
//            }else{
//                print("🔑❌ >> No Token appended: \(urlString)")
//            }
//            completion(.success(adaptedRequest))
//        }
//        completion(.success(urlRequest))
        //===
//        guard let token = UserDefaults.standard.value(forKey: kAuthToken) else {
//                  completion(.success(urlRequest))
//                  return
//              }
//              let bearerToken = "Bearer \(token)"
//              request.setValue(bearerToken, forHTTPHeaderField: "Authorization")
//              print("\nadapted; token added to the header field is: \(bearerToken)\n")
//              completion(.success(request))
    }
    
    // MARK: - RequestRetrier
    
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
//        dump(request.request?.allHTTPHeaderFields!) //Prints twice because for some reason, I read that 401 causes to repeat the request automatically no matter what. But still no Authorization Header that I set in adapt()
//        completion(.doNotRetry)
        
//        guard request.retryCount < 2 else {
//                   completion(.doNotRetry)
//                   return
//               }
//               print("\nretried; retry count: \(request.retryCount)\n")
////                self.refreshToken1() { isSuccess in
////                   isSuccess ? completion(.retry) : completion(.doNotRetry)
////               }
//        refreshToken1 {
//            isSuccess in
//
//            isSuccess ? completion(.doNotRetry) : completion(.retry)
//
//        }
    }
    
    func refreshToken1(completion: @escaping (_ isSuccess: Bool) -> Void) {
           let params = ["token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjM2MDg2IiwiZW1haWwiOiJpbmZvQGprZW50ZXJ0YWlubWVudC5uZXQiLCJpYXQiOjE2MDY3OTc5MzQsImV4cCI6MTYwNjc5Nzk5NH0.gaFcoOf4cD3ROL4Qg5W7DB4o6n8tA3QA2E-41YH59BM"]//UserDefaults.standard.value(forKey: "token") as? String ?? ""] as [String:Any]
        URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.RefreshToken, showLoader:false) { (resultDict, status, message) in
               if status == true
               {
                if let token = (resultDict as NSDictionary).value(forKey: "token") as? String
                {
                    UserDefaults.standard.setValue(token, forKey: kAuthToken)
                    completion(true)
                }
               }
           }
       }
   
}
