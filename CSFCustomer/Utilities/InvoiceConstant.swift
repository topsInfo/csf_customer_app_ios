//
//  InvoiceConstant.swift
//  CSFCustomer
//
//  Created by Tops on 23/12/20.
//

import Foundation
import UIKit
let group = DispatchGroup()
var activityViewController : UIActivityViewController?
enum gender : String
{
    case Male = "1"
    case Female = "2"
    func dispValue() -> String
    {
         return self.rawValue
    }
}
enum InvoiceType : String
{
   case MyInvoice = "1"
   case UnpaidInvoice = "2"
   case PaidInvoice = "3"
   case Archived = "4"
   case InTransit = "5"
   case Pickup = "6"
   case OnDelivery = "7"
   case Refunded = "8"
   case PackageRequest = "9"
   case Upcoming = "10"
   func dispValue() -> String
   {
        return self.rawValue
   }
}

enum DateFormatType : String
{
    case Day = "1"
    case Month = "2"
    case Year = "3"
    func dispValue() -> String
    {
         return self.rawValue
    }
}
enum PaymentStatus : Int
{
    case Paid = 1
    case UnPaid = 2
    case InTransit = 3
    case PaidBtNotCollected = 4
    func dispValue() -> Int
    {
         return self.rawValue
    }
}

// MARK: - DriveThru Enum
enum DriveThruPackageStatusId : Int {
    
    case Pending = 1
    case Processed = 2
    case Query = 3
    case Delivered = 4
    case Cancelled = 5
    
    func dispValue() -> Int
    {
         return self.rawValue
    }
}
enum DriveThruRequestStatusId : Int {
    case Pending = 1
    case CustomerConfirmation = 2
    case Cancelled = 3
    case Approved = 4
    case Received = 5
    case Verified = 6
    case Delivered = 7
    
    func dispValue() -> Int
    {
         return self.rawValue
    }
}

enum DriveThruPaymentStatus : Int
{
    case Paid = 1
    case UnPaid = 0
    case fullDiscount = 2
    func dispValue() -> Int
    {
         return self.rawValue
    }
}

enum DriveThruRequestListType : String
{
    case RequestList  = "1"
    case History  = "2"
}

extension UIViewController
{
    func downloadFile(InvoiceID:String,pdfFileURL:String, downloadedFileType : String = "Invoice")
    {
        var filename : String = ""
        var actualFilename : String = ""

        if pdfFileURL != ""
        {
            let arrURLComponets = pdfFileURL.components(separatedBy: "/")
            if arrURLComponets.count > 0
            {
                filename = (arrURLComponets.last!).lowercased()
            }
            
            if downloadedFileType == "QRCode" {
                if filename.contains(".png") {
                    if filename.components(separatedBy: ".png").count > 0 {
                        actualFilename = filename.components(separatedBy: ".png").first!
                    }
                }
            }else if downloadedFileType == "Document"{
                
                let strImageFileExtension : String = (filename.contains(".png") ? ".png" : (filename.contains(".jpg") ? ".jpg" : (filename.contains("jpeg") ? ".jpeg" : ".png")))
                
                if filename.contains(strImageFileExtension) {
                    if filename.components(separatedBy: strImageFileExtension).count > 0 {
                        actualFilename = filename.components(separatedBy: strImageFileExtension).first!
                    }
                }else if filename.contains(".pdf") {
                    if filename.components(separatedBy: ".pdf").count > 0 {
                        actualFilename = filename.components(separatedBy: ".pdf").first!
                    }
                }
            }
            else{
                if filename.contains(".pdf") {
                    if filename.components(separatedBy: ".pdf").count > 0 {
                        actualFilename = filename.components(separatedBy: ".pdf").first!
                    }
                }
            }
            
            let time = Date().timeIntervalSince1970
            
            if downloadedFileType == "Invoice" {
                actualFilename = "INVOICE_\(time).pdf"
                print("actualFilename name is \(actualFilename)")
            }else if downloadedFileType == "Report" {
                actualFilename = "REPORT_\(time).pdf"
                print("actualFilename name is \(actualFilename)")
            }else if downloadedFileType == "QRCode" {
                actualFilename = "QRCODE_\(time).png"
                print("actualFilename name is \(actualFilename)")
            }else if downloadedFileType == "Document" {
                
                let strImageFileExtension : String = (filename.contains(".png") ? ".png" : (filename.contains(".jpg") ? ".jpg" : (filename.contains("jpeg") ? ".jpeg" : ".png")))
                
                if filename.contains(strImageFileExtension) {
                    actualFilename = "Document_\(time)\(strImageFileExtension)"
                }else if filename.contains(".pdf") {
                    actualFilename = "Document_\(time).pdf"
                }
            }

            let queueSavePDF = DispatchQueue(label: "com.savePDF")
            group.enter()
            queueSavePDF.async(group: group) {
                sleep(2)
                self.savePdf(InvoiceID:InvoiceID,urlString: pdfFileURL, fileName: actualFilename, downloadedFileType: downloadedFileType)
                
            }

            group.notify(queue: .main) {
                print("all finished.")
                if activityViewController != nil {
                    activityViewController!.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController!, animated: true, completion: nil)
                }
            }

        }
        else
        {
            self.hideHUD()
        }
    }

    func savePdf(InvoiceID:String,urlString:String, fileName:String, downloadedFileType : String = "Invoice")
    {
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let fileURL = URL(string: urlString)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL!)
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
        if let tempLocalUrl = tempLocalUrl, error == nil
        {
          // Success
          do {
             try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
            do
            {
                let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                for indexx in 0..<contents.count
                {
                    if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent
                    {
                        activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                        DispatchQueue.main.async {
                            self.hideHUD()
                            group.leave()
                            
                            var msgBody : String = ""
                            print("downloadedFileType is \(downloadedFileType)")
                            if downloadedFileType == "Invoice" {
                                msgBody = "\(downloadedFileType) \(InvoiceID) Downloaded Successfully"
                            }
                            else if downloadedFileType == "Document" {
                                msgBody = "\(downloadedFileType) \(InvoiceID) Downloaded Successfully"
                                
                                let strFileExtension : String = destinationFileUrl.lastPathComponent.lowercased()
                                if !strFileExtension.contains(".pdf"){
                                    return
                                }
                            }
                            else if downloadedFileType == "QRCode" {
                                return
                            }else{
                                msgBody = "\(downloadedFileType) Downloaded Successfully"
                            }
                            self.setNotification(msgBody:msgBody,pdffileURL:destinationFileUrl.absoluteString,invoiceID:fileName, downloadedFileType: downloadedFileType)
                        }
                    }
                 }
             }
             catch (let err) {
                 print("error: \(err.localizedDescription)")
                DispatchQueue.main.async {
                    self.hideHUD()
                    self.showAlert(title: "", msg: "Please try again!!", vc: self)
                    }
                }
            }
            catch (let writeError)
            {
                print("error: \(writeError.localizedDescription)")
                    // print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    DispatchQueue.main.async {
                        self.hideHUD()
                        self.showAlert(title: "", msg: "Document already exists", vc: self)
                    }
                }
            }
         else
         {
                DispatchQueue.main.async {
                    self.hideHUD()
                    self.showAlert(title: "", msg: "Please try again!!", vc: self)
                }
         }
        }
        task.resume()
        //over
    }
    func PDFFileExists(url:String, fileName:String) -> Bool {
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for url in contents {
                    if url.description.contains(fileName) {
                        // its your file! do what you want with it!
                        return true
                    }
                }
            } catch
            {
                return false
            }
        }
        return false
    }
    func setNotification(msgBody:String,pdffileURL:String,invoiceID:String, downloadedFileType : String = "Invoice")
    {
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: kNotificationTitle, arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey:msgBody, arguments: nil)
        content.sound = UNNotificationSound.default
        
        if downloadedFileType == "ServiceGuide"{
            content.userInfo = ["link":pdffileURL,"title":"Service Guide"]
        }else if downloadedFileType == "OnlineShopperGuide"{
            content.userInfo = ["link":pdffileURL,"title":"Online Shopper Guide"]
        }else if downloadedFileType == "Invoice" {
            content.userInfo = ["link":pdffileURL,"title":"Invoice"]
        }else if downloadedFileType == "Report"{
            content.userInfo = ["link":pdffileURL,"title":"Report"]
        }else if downloadedFileType == "QRCode"{
            content.userInfo = ["link":pdffileURL,"title":"QRCode"]
        }else if downloadedFileType == "Document"{
            content.userInfo = ["link":pdffileURL,"title":"Document"]
        }
        content.categoryIdentifier = invoiceID

        let request = UNNotificationRequest.init(identifier: content.categoryIdentifier, content: content, trigger: nil)

        let center = UNUserNotificationCenter.current()
        center.add(request)
    }
}
