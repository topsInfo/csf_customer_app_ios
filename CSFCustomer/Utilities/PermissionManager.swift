//
//  PermissionManager.swift
//  CSFCustomer
//
//  Created by Tops on 24/02/21.
//

import UIKit
import AVKit
import PhotosUI

// MARK: - PermissionMessage
struct PermissionMessage {
    static var noCameraTitle: String {
        "No camera access"
    }
    static var noCameraMessage: String {
        "\("Please go to settings and switch on your Camera.") \n\("Settings") -> CSF Couriers -> \("switch on camera")"
    }
    static var noPhotoTitle: String {
        "No photo library access"
    }
    static var noPhotoMessage: String {
        "\("Please go to settings and allow Photo library access.") \n\("Settings") -> CSF Couriers -> \("Photos") -> \("select") \"\("Limited Access")\" \("or") \"\("Full Access")\""
    }
}

typealias PermissionStatus = (_ status: Int, _ isGranted: Bool) -> Void

// MARK: - Manager
class PermissionManager {

    static var shared = PermissionManager()

    var cameraPermission: AVAuthorizationStatus {
        return AVCaptureDevice.authorizationStatus(for: .video)
    }
    var photoPermission: PHAuthorizationStatus {
        return PHPhotoLibrary.authorizationStatus()
    }

    private init() {}
}

// MARK: - UI method(s)
extension PermissionManager {

    func showPopup(title: String, message: String, vc : UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        })
        
        DispatchQueue.main.async {
            vc.present(alert, animated: true)
        }
    }
}
extension UIViewController {
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
}

// MARK: - Permsission requests
extension PermissionManager {

    func requestCameraPermission(vc : UIViewController,completion: PermissionStatus?) {
        switch cameraPermission {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) {[weak self] (isGranted) in
                guard let weakSelf = self else { return }
                if isGranted {
                    completion?(AVAuthorizationStatus.authorized.rawValue, true)
                } else {
                    completion?(AVAuthorizationStatus.denied.rawValue, false)
                    weakSelf.showPopup(
                        title: PermissionMessage.noCameraTitle,
                        message: PermissionMessage.noCameraMessage, vc: vc)
                }
            }
        case .authorized:
            completion?(AVAuthorizationStatus.authorized.rawValue, true)
        case .denied:
            completion?(AVAuthorizationStatus.denied.rawValue, false)
            showPopup(
                title: PermissionMessage.noCameraTitle,
                message: PermissionMessage.noCameraMessage, vc: vc)
        case .restricted:
            completion?(AVAuthorizationStatus.restricted.rawValue, false)
        @unknown default:
            completion?(-1, false)
        }
    }

    func requestPhotoPermission(vc : UIViewController, completion: PermissionStatus?) {
        switch photoPermission {
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization {[weak self] (status) in
                //guard let weakSelf = self else { return }
                DispatchQueue.main.async {[weak self] in
                    guard let weakSelf = self else { return }
                    if status == .authorized {
                        completion?(PHAuthorizationStatus.authorized.rawValue, true)
                    } else {
                        completion?(PHAuthorizationStatus.denied.rawValue, false)
                        weakSelf.showPopup(
                            title: PermissionMessage.noPhotoTitle,
                            message: PermissionMessage.noPhotoMessage, vc: vc
                        )
                    }
                }
            }
        case .authorized:
            completion?(PHAuthorizationStatus.authorized.rawValue, true)
        case .denied:
            completion?(PHAuthorizationStatus.denied.rawValue, false)
            showPopup(
                title: PermissionMessage.noPhotoTitle,
                message: PermissionMessage.noPhotoMessage, vc: vc
            )
        case .restricted:
            completion?(PHAuthorizationStatus.restricted.rawValue, false)
        case .limited:
            if #available(iOS 14, *) {
                completion?(PHAuthorizationStatus.limited.rawValue, true)
            } else {
                // Fallback on earlier versions
            }
        @unknown default:
            completion?(-1, false)
        }
    }
}
