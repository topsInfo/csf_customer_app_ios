//
//  APIConstant.swift
//  CSFCustomer
//
//  Created by Tops on 03/09/21.
//
import Foundation

// MARK: - Global Variables
var apiVersion : String
{
    return NPConfigurations.getValueFor(.apiVersion)!
}

var baseURL : String
{
    return NPConfigurations.getValueFor(.baseUrl)!
}

var stripePublishKey : String
{
    return NPConfigurations.getValueFor(.stripePublickKey)!
}

var kBotID : String
{
    return NPConfigurations.getValueFor(.botId)!
}

var yellowMessangerWhatsAppURL : String
{
    return NPConfigurations.getValueFor(.yellowMsgWhatsAppUrl)!
}

// MARK: - Drive Thru Flag
let isShowDriveThruOption : Bool = true

// MARK: - APICall Struct
struct APICall
{
    static let Login                    = "login"
    static let BioMetricLogin           = "biomatric_login" //new
    static let RefreshToken             = "refresh_token"
    static let Dashboard                = "dashboard"
    static let PreAlertList             = "prealerts"
    static let PreAlertDelete           = "delete_prealert"
    static let DescriptionList          = "description_list"
    static let ShipperList              = "shipper_list"
    static let AddEditPreAlert          = "add_prealert"
    static let Logout                   = "logout"
    static let LogoutAll                = "logout_all"
    static let LoginWithAmazon          = "amazon_login"
    static let ForgotPassword           = "forgot_password"
    static let getNotification          = "notification"
    static let LoginWithApple           = "apple_login"
    static let Register                 = "registration"
    //invoice
    static let MyInvoiceList            = "invoices"
    static let CreateTicket             = "create_ticket"
    static let DownloadInvoice          = "download_invoice"
    static let ViewInvoice              = "view_invoice"
    static let InvoiceHistory           = "view_history"
    static let PickupRequest            = "add_request"
    static let UndeliveredInvoiceList   = "undelivered_invoices"
    //cart
    static let addToCart                = "add_to_cart"
    static let CartList                 = "cart_list"
    static let RemoveCart               = "remove_cart"
    //chceckout //new change
    static let MyCardList               = "my_cards"
    static let DeleteCard               = "delete_card"
    static let SquareupPayment          = "square_pay"
    //Profile
    static let ProfieInfo               = "my_profile"
    static let SaveProfile              = "save_profile"
    static let AddCard                  = "add_card"
    static let MyAddress                = "my_address"
    static let SaveAddress              = "save_address"
    static let DefaultAddress           = "default_address"
    static let viewAddress              = "view_address"
    static let deleteAddress            = "delete_address"
    static let MyUsers                  = "my_users"
    static let SaveUser                 = "save_user"
    static let DeleteUser               = "delete_user"
    static let ChangePassword           = "change_password"
    static let UserChangePassword       = "change_password_user"
    static let GetAddressData           = "address_data"
    //payment refund
    static let PaymentRefund            = "refund_invoices"
    static let Feedback                 = "feedback"
    //florid address
    static let FloridaAddress           = "florida_address"
    //calculator
    static let GetCalculator            = "calculator_view"
    static let SubmitCalculatorData     = "calculator"
    //Track order
    static let TrackOrder               = "track_order"
    static let ClaimOrder               = "claim_order"
    //paypal
    static let GetPaypalURL             = "paypal_create_order"
    static let PostPaypalDataURL        = "paypal_charge_order"
    static let GetWalletPaypalURL       = "wallet_paypal_create_order"
    static let PostWalletPaypalURL      = "wallet_paypal_charge_order"
    //wallet
    static let GetWalletHistory         = "wallet"
    //Stripe payment
    static let StripePayment            = "stripe_pay"
    static let StripeToWalletTransfer   = "wallet_stripe_pay"
    static let TTDTOUSDConversion       = "ttd_to_usd"
    //notification list
    static let NotificationList         = "notification_list"
    static let NotificationSetting      = "notification_setting"
    //wallet payment checkout
    static let WalletCheckout           = "wallet_pay"
    //stripe card
    static let StripeCardList           = "stripe_cards"
    static let DeleteStripeCard         = "remove_stripe_card"
    static let UpdateStripeCard         = "update_stripe_card"
    
    //service guide
    static let ServiceGuideList         = "service_guide"
    static let ServiceGuideAgree        = "service_guide_agree"
    //Online Shopper Guide
    static let OnlineShopperGuideList   = "online_shopper_guide"
    //Pickup Request
    static let FetchPickupTime          = "check_working_hours"
    //switch account
    static let SwitchProfile            = "switch_profile"
    //Reward list
    static let RewardList               = "referral_users"
    //Maintenance
    static let maintenanceData          = "splash_data"
    //Receive at Miami
    static let ReceiptList              = "receipt_list"
    
    static let ReportIssue              = "report_issue"

    //How to use
    static let HowToUse                = "how_to_use"
    
    //Driver contact detail
    static let driverDetail            = "driver_details"
    
    //Export Reports
    static let exportPrealert          = "export_prealert"
    static let exportInvoices          = "export_invoices"
    static let exportRefundInvoices    = "export_refund_invoices"
    static let exportReceipt           = "export_receipt"
    
    //Wallet Freeze
    static let checkWalletFreez           = "check_wallet_freeze"
    
    static let revokeAppleToken           = "https://appleid.apple.com/auth/revoke"
    static let deleteAccount              = "delete-account"
    
    //Delete Account
    static let deletePreview              = "delete_preview"
    static let sendDeleteOtp              = "send_delete_otp"
    static let verifyDeleteOtp            = "verify_delete_otp"

    //Video Tutorial
    static let videoTutorialList          = "tutorials"
    
    //Promotional Code
    static let checkPromotionalCode       = "check_promotional_code"
    
    //Drive Thru
    static let driveThruList              = "drive_thru_list"
    static let driveThruRequest           = "drive_thru_request"
    static let driveThruDetails           = "drive_thru_details"
    static let driveThruLogs              = "drive_thru_logs"
    static let driveThruApproval          = "drive_thru_approval"
    static let driveThruCancel            = "drive_thru_cancel"
    static let driveThruAuth              = "drive_thru_auth"
    static let driveThruReschedule        = "drive_thru_reschedule"
    
    //Rate Delivery
    static let rateDelivery               = "rate_delivery"
}
