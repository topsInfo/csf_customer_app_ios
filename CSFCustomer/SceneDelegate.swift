//
//  SceneDelegate.swift
//  Temp
//
//  Created by Tops on 25/02/21.
//

import UIKit
import Firebase
import FirebaseDynamicLinks

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        if #available(iOS 13.0, *){
            if let scene = UIApplication.shared.connectedScenes.first{
                guard let windowScene = (scene as? UIWindowScene) else { return }
                //  print(">>> windowScene: \(windowScene)")
                let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                window.windowScene = windowScene //Make sure to do this
                window.backgroundColor = Colors.theme_white_color
                window.makeKeyAndVisible()
                appDelegate.window = window
            }
        } else {
            appDelegate.window?.makeKeyAndVisible()
        }
        //     print("=======\(connectionOptions.userActivities)")
        if let userActivity = connectionOptions.userActivities.first {
            let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
                if let deepLink = dynamiclink?.url
                {
                    let queryItems = URLComponents(url: deepLink, resolvingAgainstBaseURL: true)?.queryItems
                    if appDelegate.checkIfAnyUserActive() == false{
                        let invitedBy = queryItems?.filter({(item) in item.name == "referralCode"}).first?.value
                        //   print("code=========,\(String(describing: invitedBy))")
                        if (invitedBy != "" && invitedBy != nil)
                        {
                            //       print("==============\(String(describing: invitedBy))")
                            UserDefaults.standard.setValue(invitedBy!, forKey: "invitedby")
                            NotificationCenter.default.post(name: Notification.Name("dynamicLinkClicked"), object: nil)
                        }
                    }
                }
            }
        }
    }
    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        enableAppAppearanceMode()
    }
    
    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        appDelegate.openApp()
        if let loggedIn = UserDefaults.standard.value(forKey: kLoggedIn) as? Bool,loggedIn == true
        {
            NotificationCenter.default.post(name: Notification.Name("NotificationRead1"), object: nil)
        }
    }
    
    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        if let incomingURL = userActivity.webpageURL
        {
            //   print("url: \(incomingURL)")
            let handled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL)
            { (dynamiclink, error) in
                if let deepLink = dynamiclink?.url
                {
                    let queryItems = URLComponents(url: deepLink, resolvingAgainstBaseURL: true)?.queryItems
                    if appDelegate.checkIfAnyUserActive() == false{
                        let invitedBy = queryItems?.filter({(item) in item.name == "referralCode"}).first?.value
                        if (invitedBy != "" && invitedBy != nil)
                        {
                            //    print("code==============\(String(describing: invitedBy))")
                            UserDefaults.standard.setValue(invitedBy!, forKey: "invitedby")
                            NotificationCenter.default.post(name: Notification.Name("dynamicLinkClicked"), object: nil)
                        }
                    }
                }
            }
        }
    }
}