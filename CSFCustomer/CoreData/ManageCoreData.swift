//
//  ManageCoreData.swift
//  CSFCustomer
//
//  Created by Tops on 06/04/21.
//

import Foundation
import CoreData
enum SwitchUserInfo : String
{
    case userID  = "user_id"
    case uniqueID  = "unique_id"
    case boxID  = "box_id"
    case imgUser  = "imgUser"
    case isDefault = "is_default"
    case firstName = "firstName"
    case lastName = "lastName"
    case isActive = "isActive"    
    //Nand : crash solution
    case authToken = "authToken"
    case loginType = "loginType"
    case isBiometricEnabled = "isBiometricEnabled"
}
class ManageCoreData : NSObject
{
    //  static let sharedInstance = ManageCoreData()
    var manageObjectContext:NSManagedObjectContext?
    var LoginType:String = ""
    class func getContext() -> NSManagedObjectContext {
        return appDelegate.persistentContainer.viewContext
    }
    override init()
    {
        
    }
    func saveData(entityName:String,userData:UserInfo,isDefault:Bool,LoginType:String)
    {
        manageObjectContext = ManageCoreData.getContext()
        self.LoginType = LoginType
        let results = recordExists(BoxId:userData.boxId ?? "0",entityName:entityName)
        if results.count == 0
        {
            //isDefault = false for all records and then save data
            updateStatus(entityName:entityName,fieldName: "isDefault",value:false){ (status) in
                if status == true
                {
                    save(entityName:entityName,userData:userData,isDefault:isDefault)
                }
            }
        }
        else
        {
            //if records are available in core data then update isdefault == false for all records and update isDefault= true for current login
            updateStatus(entityName:entityName,fieldName: "isDefault",value:false){ (status) in
                if status == true
                {
                    updateRecord(entityName:entityName,BoxId:userData.boxId ?? "0",userData:userData)
                }
            }
        }
    }
    func save(entityName:String,userData:UserInfo,isDefault:Bool)
    {
        if let switchUser : SwitchUser = NSEntityDescription.insertNewObject(forEntityName: entityName, into: manageObjectContext!) as? SwitchUser {
            
            switchUser.user_id = userData.id ?? ""
            switchUser.unique_id = userData.uniqueId ?? ""
            switchUser.box_id = userData.boxId ?? ""
            switchUser.imgUser = userData.profileImagePath ?? ""
            switchUser.is_default = isDefault
            switchUser.firstName = userData.firstName ?? ""
            switchUser.lastName = userData.lastName ?? ""
            switchUser.isActive = true
            switchUser.authToken = userData.token ?? ""
            switchUser.loginType = self.LoginType
            switchUser.isBiometricEnabled = false
            UserDefaults.standard.setValue(false, forKey:kIsBiometricEnabled)
            appDelegate.saveContext()
        }
    }
    func updateStatus(entityName:String,fieldName:String,value:Bool,completion:(Bool) -> Void)
    {
        // Create Fetch Request
        manageObjectContext = ManageCoreData.getContext()
        let request:NSFetchRequest<SwitchUser> = SwitchUser.fetchRequest()
        do {
            // Execute Fetch Request
            let searchRecords = try manageObjectContext!.fetch(request)
            if let record = searchRecords as? [SwitchUser]
            {
                if record.count > 0
                {
                    for item in record
                    {
                        //  item.setValue(false,forKey: SwitchUserInfo.isDefault.rawValue)
                        if item.user_id != nil
                        {
                            if fieldName == "isDefault"
                            {
                                item.is_default = value
                            }
                            else if fieldName == "isActive"
                            {
                                item.isActive = value
                            }
                        }
                    }
                    appDelegate.saveContext()
                }
                completion(true)
            }
            else
            {
                completion(true)
            }
            
        } catch {
          //  print("Unable to fetch managed objects for entity \(entityName).")
            completion(true)
        }
    }
    func updateStatusofBiometric(entityName:String,boxID:String,fieldName:String,value:Bool,completion:(Bool) -> Void)
    {
        manageObjectContext = ManageCoreData.getContext()
        let request = NSFetchRequest<NSManagedObject>(entityName: entityName)
        //For case insensitive box id check we are using ==[c] here
        request.predicate = NSPredicate(format: "box_id ==[c] %@", boxID)
        do {
            // Execute Fetch Request
            let searchRecords = try manageObjectContext!.fetch(request)
            if let record = searchRecords as? [SwitchUser]
            {
                if record.count > 0
                {
                    for item in record
                    {
                        //  item.setValue(false,forKey: SwitchUserInfo.isDefault.rawValue)
                        if item.user_id != nil
                        {
                            if fieldName == "isBiometricEnabled"
                            {
                                item.isBiometricEnabled = value
                            }
                        }
                    }
                    appDelegate.saveContext()
                    completion(true)
                }
                else
                {
                    completion(false)
                }
            }
            else
            {
                completion(false)
            }
            
        } catch {
           // print("Unable to fetch managed objects for entity \(entityName).")
            completion(false)
        }
    }
    func updateRecord(entityName:String,BoxId:String,userData:UserInfo)
    {
        manageObjectContext = ManageCoreData.getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        //For case insensitive box id check we are using ==[c] here
        fetchRequest.predicate = NSPredicate(format: "box_id ==[c] %@", BoxId)
        do {
            let records = try manageObjectContext!.fetch(fetchRequest)
            if let records = records as? [NSManagedObject] {
                for item in records
                {
                    item.setValue(true,forKey: SwitchUserInfo.isDefault.rawValue)
                    item.setValue(userData.id ?? "" , forKey: SwitchUserInfo.userID.rawValue)
                    item.setValue(userData.uniqueId ?? "" , forKey: SwitchUserInfo.uniqueID.rawValue)
                    item.setValue(userData.boxId ?? "" , forKey: SwitchUserInfo.boxID.rawValue)
                    item.setValue(userData.profileImagePath ?? "" , forKey: SwitchUserInfo.imgUser.rawValue)
                    item.setValue(userData.firstName ?? "" , forKey: SwitchUserInfo.firstName.rawValue)
                    item.setValue(userData.lastName ?? "" , forKey: SwitchUserInfo.lastName.rawValue)
                    item.setValue(true , forKey: SwitchUserInfo.isActive.rawValue)
                    item.setValue(userData.token ?? "" , forKey: SwitchUserInfo.authToken.rawValue)
                    item.setValue(self.LoginType, forKey: SwitchUserInfo.loginType.rawValue)
                    if let isBiometricenabled = item.value(forKey: SwitchUserInfo.isBiometricEnabled.rawValue) as? Bool
                    {
                        item.setValue(isBiometricenabled, forKey: SwitchUserInfo.isBiometricEnabled.rawValue)
                        UserDefaults.standard.setValue(isBiometricenabled, forKey:kIsBiometricEnabled)
                    }
                    
                    do {
                        try manageObjectContext!.save()
                    }catch  let error as NSError {
                       // print("\(error)")
                    }
                }
            }
        }
        catch {
           // print("error executing fetch request: \(error)")
        }
        
    }
    func updateActiveStatus(entityName:String,BoxId:String,status:Bool)
    {
        manageObjectContext = ManageCoreData.getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        //For case insensitive box id check we are using ==[c] here
        fetchRequest.predicate = NSPredicate(format: "box_id ==[c] %@", BoxId)
        do {
            let records = try manageObjectContext!.fetch(fetchRequest)
            if let records = records as? [NSManagedObject] {
                for item in records
                {
                    item.setValue(status,forKey: SwitchUserInfo.isActive.rawValue)
                    do {
                        try manageObjectContext!.save()
                    }catch  let error as NSError {
                      //  print("\(error)")
                    }
                }
            }
        }
        catch {
          //  print("error executing fetch request: \(error)")
        }
        
    }
    func recordExists(BoxId: String,entityName:String) -> [NSManagedObject] {
        
        manageObjectContext = ManageCoreData.getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        //For case insensitive box id check we are using ==[c] here
        fetchRequest.predicate = NSPredicate(format: "box_id ==[c] %@", BoxId)
        var results: [NSManagedObject] = []
        do {
            results = try manageObjectContext!.fetch(fetchRequest)
        }
        catch {
           // print("error executing fetch request: \(error)")
        }
        return results
    }
    
    func fetchData(entityName:String) -> [NSManagedObject]
    {
       // print("Fetching Data..")
        var results: [NSManagedObject] = []
        manageObjectContext = ManageCoreData.getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        // request.returnsObjectsAsFaults = false
        do {
            results = try manageObjectContext!.fetch(fetchRequest)
            
        } catch {
            //print("Fetching data Failed")
        }
        return results
    }
    func fetchDataOfLoginTypewise(entityName:String,LoginType:String) -> [NSManagedObject]
    {
        manageObjectContext = ManageCoreData.getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: "loginType == %@ && isActive == %d", LoginType,1)
        var results: [NSManagedObject] = []
        do {
            results = try manageObjectContext!.fetch(fetchRequest)
        }
        catch {
            //print("error executing fetch request: \(error)")
        }
        return results
    }
    func deleteRecord(entityName:String,BoxId:String,completion:(Bool) -> Void)
    {
        manageObjectContext = ManageCoreData.getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        //For case insensitive box id check we are using ==[c] here
        fetchRequest.predicate = NSPredicate(format: "box_id ==[c] %@", BoxId)
        fetchRequest.includesPropertyValues = false
        do {
            let records = try manageObjectContext!.fetch(fetchRequest)
            if let records = records as? [NSManagedObject] {
                for item in records {
                    manageObjectContext!.delete(item)
                }
            }
            appDelegate.saveContext()
            completion(true)
            
        } catch {
            completion(false)
        }
    }
    func countActiveUsers(entityName:String) -> Int
    {
        var counter : Int = 0
        let results = fetchData(entityName: entityName)
        if results.count > 0
        {
            for data in results as [NSManagedObject]
            {
                if let activeStatus = data.value(forKey: SwitchUserInfo.isActive.rawValue) as? Bool,activeStatus == true
                {
                    counter += 1
                }
            }
        }
        return counter
    }
    
}
