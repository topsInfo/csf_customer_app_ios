//
//  SwitchUser+CoreDataProperties.swift
//  
//
//  Created by Tops on 07/04/21.
//
//

import Foundation
import CoreData


extension SwitchUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SwitchUser> {
        return NSFetchRequest<SwitchUser>(entityName: "SwitchUser")
    }

    @NSManaged public var box_id: String?
    @NSManaged public var firstName: String?
    @NSManaged public var imgUser: String?
    @NSManaged public var is_default: Bool
    @NSManaged public var lastName: String?
    @NSManaged public var unique_id: String?
    @NSManaged public var user_id: String?

}
