import Foundation
import SwiftyJSON
//This old code is now replaced by below new code
//class SegmentOptions : NSObject, NSCoding, NSSecureCoding, Decodable, Encodable{
//    static var supportsSecureCoding: Bool {
//        return true
//    }
    
class SegmentOptions : NSObject, Decodable, Encodable{
    var optionName : String!
    var isChecked : String!
    var index : String!
    var refNo : String!

    override init() {

    }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        optionName = json["optionName"].stringValue
        isChecked = json["isChecked"].stringValue
        index = json["index"].stringValue
        refNo = json["refNo"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if optionName != nil{
            dictionary["optionName"] = optionName
        }
        if isChecked != nil{
            dictionary["isChecked"] = isChecked
        }
        if index != nil{
            dictionary["index"] = index
        }
        if refNo != nil{
            dictionary["refNo"] = refNo
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        optionName = aDecoder.decodeObject(forKey: "optionName") as? String
        isChecked = aDecoder.decodeObject(forKey: "isChecked") as? String
        index = aDecoder.decodeObject(forKey: "index") as? String
        refNo = aDecoder.decodeObject(forKey: "refNo") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if optionName != nil{
            aCoder.encode(optionName, forKey: "optionName")
        }
        if isChecked != nil{
            aCoder.encode(isChecked, forKey: "isChecked")
        }
        if index != nil{
            aCoder.encode(index, forKey: "index")
        }
        if refNo != nil{
            aCoder.encode(refNo, forKey: "refNo")
        }
    }
}
