//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Invoice : NSObject, NSCoding{

    var cartCount : Int!
	var list : [InvoiceList]!
	var memberList : [MemberList]!
	var totalPage : Int!
    var totalCount : Int!
    
    //DriveThru
    var restrictedItemsAlert : String!

    //Curbside Pickup Push Link
    var driveThruReqUrl : String!
    
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init() {
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        cartCount = json["cart_count"].intValue
		list = [InvoiceList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = InvoiceList(fromJson: listJson)
			list.append(value)
		}
		memberList = [MemberList]()
		let memberListArray = json["member_list"].arrayValue
		for memberListJson in memberListArray{
			let value = MemberList(fromJson: memberListJson)
			memberList.append(value)
		}
		totalPage = json["total_page"].intValue
        totalCount = json["total_count"].intValue
        
        //DriveThru
        restrictedItemsAlert = json["restricted_items_alert"].stringValue
        
        //Curbside Pickup Push Link
        driveThruReqUrl = json["drive_thru_req_url"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if cartCount != nil{
            dictionary["cart_count"] = cartCount
        }
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		if memberList != nil{
			var dictionaryElements = [[String:Any]]()
			for memberListElement in memberList {
				dictionaryElements.append(memberListElement.toDictionary())
			}
			dictionary["member_list"] = dictionaryElements
		}
		if totalPage != nil{
			dictionary["total_page"] = totalPage
		}
        if totalCount != nil{
            dictionary["total_count"] = totalCount
        }

        //DriveThru
        if restrictedItemsAlert != nil{
            dictionary["restricted_items_alert"] = restrictedItemsAlert
        }
        
        //Curbside Pickup Push Link
        if driveThruReqUrl != nil{
            dictionary["drive_thru_req_url"] = driveThruReqUrl
        }
        
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cartCount = aDecoder.decodeObject(forKey: "cart_count") as? Int
         list = aDecoder.decodeObject(forKey: "list") as? [InvoiceList]
         memberList = aDecoder.decodeObject(forKey: "member_list") as? [MemberList]
         totalPage = aDecoder.decodeObject(forKey: "total_page") as? Int
         totalCount = aDecoder.decodeObject(forKey: "total_count") as? Int
        
         //DriveThru
         restrictedItemsAlert = aDecoder.decodeObject(forKey: "restricted_items_alert") as? String
        
        //Curbside Pickup Push Link
        driveThruReqUrl = aDecoder.decodeObject(forKey: "drive_thru_req_url") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
        if cartCount != nil{
            aCoder.encode(cartCount, forKey: "cart_count")
        }
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
		if memberList != nil{
			aCoder.encode(memberList, forKey: "member_list")
		}
		if totalPage != nil{
			aCoder.encode(totalPage, forKey: "total_page")
		}
        if totalCount != nil{
            aCoder.encode(totalCount, forKey: "total_count")
        }
        
        //DriveThru
        if restrictedItemsAlert != nil{
            aCoder.encode(restrictedItemsAlert, forKey: "restricted_items_alert")
        }
        
        //Curbside Pickup Push Link
        if driveThruReqUrl != nil{
            aCoder.encode(driveThruReqUrl, forKey: "drive_thru_req_url")
        }
	}
}
class InvoiceList : NSObject, NSCoding{

    var allowRequest : Int!
    var customerName : String!
    var dateOfInvoice : String!
    var descriptionName : String!
    var driverName : String!
    var hawbNumber : String!
    var id : String!
    var packagesWeight : String!
    var shipper : String!
    var status : Int!
    var totalTtd : String!
    var totalUsd : String!
    var trackingNumber : String!
    var paidStatus : Int!
    
    //DriveThru
    var allowed : Int!
    var isAdded : Int!
    var markAsQuery : String!
    var onDelivery : Int!

    //Receipt Docs
    var documentsArr : [String]!
    
    //Credit Payment
    var isCreditPayment : Int!
    
    override init()
    {
        
    }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        allowRequest = json["allow_request"].intValue
        customerName = json["customer_name"].stringValue
        dateOfInvoice = json["date_of_invoice"].stringValue
        descriptionName = json["description_name"].stringValue
        driverName = json["driver_name"].stringValue
        hawbNumber = json["hawb_number"].stringValue
        id = json["id"].stringValue
        packagesWeight = json["packages_weight"].stringValue
        shipper = json["shipper"].stringValue
        status = json["status"].intValue
        totalTtd = json["total_ttd"].stringValue
        totalUsd = json["total_usd"].stringValue
        trackingNumber = json["tracking_number"].stringValue
        paidStatus = json["paid_status"].intValue
        
        //DriveThru
        allowed = json["allowed"].intValue
        isAdded = json["is_added"].intValue
        markAsQuery = json["mark_as_query"].stringValue
        onDelivery = json["on_delivery"].intValue
        
        //Receipt Docs
        documentsArr = [String]()
        let documentsArray = json["documents"].arrayValue
        for documentsReasonListJson in documentsArray{
            if let documentsReason : String = documentsReasonListJson.rawString() {
                documentsArr.append(documentsReason)
            }
        }
        
        //Credit Payment
        isCreditPayment = json["is_credit_payment"].intValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if allowRequest != nil{
            dictionary["allow_request"] = allowRequest
        }
        if customerName != nil{
            dictionary["customer_name"] = customerName
        }
        if dateOfInvoice != nil{
            dictionary["date_of_invoice"] = dateOfInvoice
        }
        if descriptionName != nil{
            dictionary["description_name"] = descriptionName
        }
        if driverName != nil{
            dictionary["driver_name"] = driverName
        }
        if hawbNumber != nil{
            dictionary["hawb_number"] = hawbNumber
        }
        if id != nil{
            dictionary["id"] = id
        }
        if packagesWeight != nil{
            dictionary["packages_weight"] = packagesWeight
        }
        if shipper != nil{
            dictionary["shipper"] = shipper
        }
        if status != nil{
            dictionary["status"] = status
        }
        if totalTtd != nil{
            dictionary["total_ttd"] = totalTtd
        }
        if totalUsd != nil{
            dictionary["total_usd"] = totalUsd
        }
        if trackingNumber != nil{
            dictionary["tracking_number"] = trackingNumber
        }
        if paidStatus != nil{
            dictionary["paid_status"] = paidStatus
        }
        
        //DriveThru
        if allowed != nil{
            dictionary["allowed"] = allowed
        }
        if isAdded != nil{
            dictionary["is_added"] = isAdded
        }
        if markAsQuery != nil{
            dictionary["mark_as_query"] = markAsQuery
        }
        if onDelivery != nil{
            dictionary["on_delivery"] = onDelivery
        }
        
        //Receipt Docs
        if documentsArr != nil{
            dictionary["documents"] = documentsArr
        }
        
        //Credit Payment
        if isCreditPayment != nil{
            dictionary["is_credit_payment"] = isCreditPayment
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         allowRequest = aDecoder.decodeObject(forKey: "allow_request") as? Int
         customerName = aDecoder.decodeObject(forKey: "customer_name") as? String
         dateOfInvoice = aDecoder.decodeObject(forKey: "date_of_invoice") as? String
         descriptionName = aDecoder.decodeObject(forKey: "description_name") as? String
         driverName = aDecoder.decodeObject(forKey: "driver_name") as? String
         hawbNumber = aDecoder.decodeObject(forKey: "hawb_number") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         packagesWeight = aDecoder.decodeObject(forKey: "packages_weight") as? String
         shipper = aDecoder.decodeObject(forKey: "shipper") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int
         totalTtd = aDecoder.decodeObject(forKey: "total_ttd") as? String
         totalUsd = aDecoder.decodeObject(forKey: "total_usd") as? String
         trackingNumber = aDecoder.decodeObject(forKey: "tracking_number") as? String
         paidStatus = aDecoder.decodeObject(forKey: "paid_status") as? Int
        
         //DriveThru
         allowed = aDecoder.decodeObject(forKey: "allowed") as? Int
         isAdded = aDecoder.decodeObject(forKey: "is_added") as? Int
         markAsQuery = aDecoder.decodeObject(forKey: "mark_as_query") as? String
         onDelivery = aDecoder.decodeObject(forKey: "on_delivery") as? Int
        
         //Receipt Docs
         documentsArr = aDecoder.decodeObject(forKey: "documents") as? [String]
        
        //Credit Payment
        isCreditPayment = aDecoder.decodeObject(forKey: "is_credit_payment") as? Int
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if allowRequest != nil{
            aCoder.encode(allowRequest, forKey: "allow_request")
        }
        if customerName != nil{
            aCoder.encode(customerName, forKey: "customer_name")
        }
        if dateOfInvoice != nil{
            aCoder.encode(dateOfInvoice, forKey: "date_of_invoice")
        }
        if descriptionName != nil{
            aCoder.encode(descriptionName, forKey: "description_name")
        }
        if driverName != nil{
            aCoder.encode(driverName, forKey: "driver_name")
        }
        if hawbNumber != nil{
            aCoder.encode(hawbNumber, forKey: "hawb_number")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if packagesWeight != nil{
            aCoder.encode(packagesWeight, forKey: "packages_weight")
        }
        if shipper != nil{
            aCoder.encode(shipper, forKey: "shipper")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if totalTtd != nil{
            aCoder.encode(totalTtd, forKey: "total_ttd")
        }
        if totalUsd != nil{
            aCoder.encode(totalUsd, forKey: "total_usd")
        }
        if trackingNumber != nil{
            aCoder.encode(trackingNumber, forKey: "tracking_number")
        }
        if paidStatus != nil{
            aCoder.encode(paidStatus, forKey: "paid_status")
        }
        
        //DriveThru
        if allowed != nil{
            aCoder.encode(allowed, forKey: "allowed")
        }
        if isAdded != nil{
            aCoder.encode(isAdded, forKey: "is_added")
        }
        if markAsQuery != nil{
            aCoder.encode(markAsQuery, forKey: "mark_as_query")
        }
        if onDelivery != nil{
            aCoder.encode(onDelivery, forKey: "on_delivery")
        }
        
        //Receipt Docs
        if documentsArr != nil{
            aCoder.encode(documentsArr, forKey: "documents")
        }
        
        //Credit Payment
        if isCreditPayment != nil{
            aCoder.encode(isCreditPayment, forKey: "is_credit_payment")
        }
    }

}

class MemberList : NSObject, NSCoding{

    var id : String!
    var name : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        name = json["name"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if name != nil{
            dictionary["name"] = name
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
    }
}
