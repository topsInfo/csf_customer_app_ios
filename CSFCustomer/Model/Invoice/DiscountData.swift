//
//  DiscountData.swift
//  CSFCustomer
//
//  Created by iMac on 16/08/23.
//

import Foundation
import SwiftyJSON

class DiscountData : NSObject, NSCoding{

    var discountNote : String!
    var discountTtd : String!
    var discountUsd : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    
    override init() {
        
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        discountNote = json["discount_note"].stringValue
        discountTtd = json["discount_ttd"].stringValue
        discountUsd = json["discount_usd"].stringValue
    }
    
    var height: CGFloat {
        let padding: CGFloat = (20 * 2)
        let tddWidth: CGFloat = 80
        let usdWidth: CGFloat = 80
        let totalOtherWidth: CGFloat = padding + tddWidth + usdWidth
        let noteWidth: CGFloat = UIScreen.main.bounds.width - totalOtherWidth
        //print("--- Otherwidth: \(totalOtherWidth), screenWidth: \(UIScreen.main.bounds.width) NoteWidth: \(noteWidth) ---")
        let font = UIFont(name: fontname.openSansBold, size: 14)!
        let height = discountNote.getHeightForLable(width: noteWidth, font: font) + 8
        //print("--- \(discountNote), \(height) ---")
        return height
//        return 76.5
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if discountNote != nil{
            dictionary["discount_note"] = discountNote
        }
        if discountTtd != nil{
            dictionary["discount_ttd"] = discountTtd
        }
        if discountUsd != nil{
            dictionary["discount_usd"] = discountUsd
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         discountNote = aDecoder.decodeObject(forKey: "discount_note") as? String
         discountTtd = aDecoder.decodeObject(forKey: "discount_ttd") as? String
         discountUsd = aDecoder.decodeObject(forKey: "discount_usd") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if discountNote != nil{
            aCoder.encode(discountNote, forKey: "discount_note")
        }
        if discountTtd != nil{
            aCoder.encode(discountTtd, forKey: "discount_ttd")
        }
        if discountUsd != nil{
            aCoder.encode(discountUsd, forKey: "discount_usd")
        }

    }

}
