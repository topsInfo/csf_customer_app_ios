//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class InvoiceHistory : NSObject, NSCoding{

	var list : [InvoiceHistoryList]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		list = [InvoiceHistoryList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = InvoiceHistoryList(fromJson: listJson)
			list.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey: "list") as? [InvoiceHistoryList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}

	}

}
class InvoiceHistoryList : NSObject, NSCoding{

    var actionBy : String!
    var date : String!
    var descriptionField : String!
    var time : String!
    var title : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        actionBy = json["action_by"].stringValue
        date = json["date"].stringValue
        descriptionField = json["description"].stringValue
        time = json["time"].stringValue
        title = json["title"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if actionBy != nil{
            dictionary["action_by"] = actionBy
        }
        if date != nil{
            dictionary["date"] = date
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if time != nil{
            dictionary["time"] = time
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         actionBy = aDecoder.decodeObject(forKey: "action_by") as? String
         date = aDecoder.decodeObject(forKey: "date") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         time = aDecoder.decodeObject(forKey: "time") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if actionBy != nil{
            aCoder.encode(actionBy, forKey: "action_by")
        }
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if time != nil{
            aCoder.encode(time, forKey: "time")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }

    }

}
