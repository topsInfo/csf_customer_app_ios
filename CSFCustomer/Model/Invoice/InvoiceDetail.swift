//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class InvoiceDetail : NSObject, NSCoding{

    var address : String!
    var boxId : String!
    var conversionRate : String!
    var country : String!
    var dateOfInvoice : String!
    var deliveryAgent : String!
    var descriptionList : [String]!
    var disAtTtd : String!
    var disAtUsd : String!
//    var discountNote : String!
//    var discountTtd : String!
//    var discountUsd : String!
    var driverName : String!
    var estimationList : [EstimationList]!
    var fullRefund : String!
    var hawbNumber : String!
    var invoiceId : String!
    var invoiceNumber : String!
    var isDiscount : String!
    var isRefund : String!
    var memberName : String!
    var packageStatus : [PackageStatus]!
    //Nand : Payment detail changes
    var paymentDetails : [PaymentDetails]!
    var paidDate : String!
    var paidStatus : String!
    var paymentType : String!
    var refundAmount : AnyObject!
    var shipper : String!
    var weight : String!
    var totalTtd : String!
    var totalUsd : String!
    var trackingNumber : String!
    var transactionId : String!
    
    var batchQR : String!
    var packageQR : String!
    
    //Nand : Discount Data Changes
    var discountData : [DiscountData]!
    
    //Credit Payment
    var isCreditPayment : Int!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    override init() {
        
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address = json["address"].stringValue
        boxId = json["box_id"].stringValue
        conversionRate = json["conversion_rate"].stringValue
        country = json["country"].stringValue
        dateOfInvoice = json["date_of_invoice"].stringValue
        deliveryAgent = json["delivery_agent"].stringValue
        descriptionList = [String]()
        let descriptionListArray = json["description_list"].arrayValue
        for descriptionListJson in descriptionListArray{
            descriptionList.append(descriptionListJson.stringValue)
        }
        disAtTtd = json["dis_at_ttd"].stringValue
        disAtUsd = json["dis_at_usd"].stringValue
//        discountNote = json["discount_note"].stringValue
//        discountTtd = json["discount_ttd"].stringValue
//        discountUsd = json["discount_usd"].stringValue
        driverName = json["driver_name"].stringValue
        estimationList = [EstimationList]()
        let estimationListArray = json["estimation_list"].arrayValue
        for estimationListJson in estimationListArray{
            let value = EstimationList(fromJson: estimationListJson)
            estimationList.append(value)
        }
        fullRefund = json["full_refund"].stringValue
        hawbNumber = json["hawb_number"].stringValue
        invoiceId = json["invoice_id"].stringValue
        invoiceNumber = json["invoice_number"].stringValue
        isDiscount = json["is_discount"].stringValue
        isRefund = json["is_refund"].stringValue
        memberName = json["member_name"].stringValue
        packageStatus = [PackageStatus]()
        let packageStatusArray = json["package_status"].arrayValue
        for packageStatusJson in packageStatusArray{
            let value = PackageStatus(fromJson: packageStatusJson)
            packageStatus.append(value)
        }
        
        //Nand : Payment detail changes
        paymentDetails = [PaymentDetails]()
        let paymentDetailsArray = json["payment_details"].arrayValue
        for paymentDetailsJson in paymentDetailsArray{
            let value = PaymentDetails(fromJson: paymentDetailsJson)
            paymentDetails.append(value)
        }

        paidDate = json["paid_date"].stringValue
        paidStatus = json["paid_status"].stringValue
        paymentType = json["payment_type"].stringValue
        refundAmount = json["refund_amount"].stringValue as AnyObject
        shipper = json["shipper"].stringValue
        weight = json["weight"].stringValue
        totalTtd = json["total_ttd"].stringValue
        totalUsd = json["total_usd"].stringValue
        trackingNumber = json["tracking_number"].stringValue
        transactionId = json["transaction_id"].stringValue
        
        batchQR = json["batch_qr"].stringValue
        packageQR = json["package_qr"].stringValue
        
        //Nand : Discount Data Changes
        discountData = [DiscountData]()
        let discountDataArray = json["discount_data"].arrayValue
        for discountDataJson in discountDataArray{
            let value = DiscountData(fromJson: discountDataJson)
            discountData.append(value)
        }
        
        //Credit Payment
        isCreditPayment = json["is_credit_payment"].intValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if boxId != nil{
            dictionary["box_id"] = boxId
        }
        if conversionRate != nil{
            dictionary["conversion_rate"] = conversionRate
        }
        if country != nil{
            dictionary["country"] = country
        }
        if dateOfInvoice != nil{
            dictionary["date_of_invoice"] = dateOfInvoice
        }
        if deliveryAgent != nil{
            dictionary["delivery_agent"] = deliveryAgent
        }
        if descriptionList != nil{
            dictionary["description_list"] = descriptionList
        }
        if disAtTtd != nil{
            dictionary["dis_at_ttd"] = disAtTtd
        }
        if disAtUsd != nil{
            dictionary["dis_at_usd"] = disAtUsd
        }
//        if discountNote != nil{
//            dictionary["discount_note"] = discountNote
//        }
//        if discountTtd != nil{
//            dictionary["discount_ttd"] = discountTtd
//        }
//        if discountUsd != nil{
//            dictionary["discount_usd"] = discountUsd
//        }
        if driverName != nil{
            dictionary["driver_name"] = driverName
        }
        if estimationList != nil{
            var dictionaryElements = [[String:Any]]()
            for estimationListElement in estimationList {
                dictionaryElements.append(estimationListElement.toDictionary())
            }
            dictionary["estimation_list"] = dictionaryElements
        }
        if fullRefund != nil{
            dictionary["full_refund"] = fullRefund
        }
        if hawbNumber != nil{
            dictionary["hawb_number"] = hawbNumber
        }
        if invoiceId != nil{
            dictionary["invoice_id"] = invoiceId
        }
        if invoiceNumber != nil{
            dictionary["invoice_number"] = invoiceNumber
        }
        if isDiscount != nil{
            dictionary["is_discount"] = isDiscount
        }
        if isRefund != nil{
            dictionary["is_refund"] = isRefund
        }
        if memberName != nil{
            dictionary["member_name"] = memberName
        }
        if packageStatus != nil{
            var dictionaryElements = [[String:Any]]()
            for packageStatusElement in packageStatus {
                dictionaryElements.append(packageStatusElement.toDictionary())
            }
            dictionary["package_status"] = dictionaryElements
        }
        //Nand : Payment detail changes
        if paymentDetails != nil{
            var dictionaryElements = [[String:Any]]()
            for paymentDetailsElement in paymentDetails {
                dictionaryElements.append(paymentDetailsElement.toDictionary())
            }
            dictionary["payment_details"] = dictionaryElements
        }
        if paidDate != nil{
            dictionary["paid_date"] = paidDate
        }
        if paidStatus != nil{
            dictionary["paid_status"] = paidStatus
        }
        if paymentType != nil{
            dictionary["payment_type"] = paymentType
        }
        if refundAmount != nil{
            dictionary["refund_amount"] = refundAmount
        }
        if shipper != nil{
            dictionary["shipper"] = shipper
        }
        if weight != nil{
            dictionary["weight"] = weight
        }
        if totalTtd != nil{
            dictionary["total_ttd"] = totalTtd
        }
        if totalUsd != nil{
            dictionary["total_usd"] = totalUsd
        }
        if trackingNumber != nil{
            dictionary["tracking_number"] = trackingNumber
        }
        if transactionId != nil{
            dictionary["transaction_id"] = transactionId
        }
        if batchQR != nil{
            dictionary["batch_qr"] = batchQR
        }
        if packageQR != nil{
            dictionary["package_qr"] = packageQR
        }
        
        //Nand : Discount Data changes
        if discountData != nil{
            var dictionaryElements = [[String:Any]]()
            for discountDataElement in discountData {
                dictionaryElements.append(discountDataElement.toDictionary())
            }
            dictionary["discount_data"] = dictionaryElements
        }
        
        //Credit Payment
        if isCreditPayment != nil{
            dictionary["is_credit_payment"] = isCreditPayment
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         address = aDecoder.decodeObject(forKey: "address") as? String
         boxId = aDecoder.decodeObject(forKey: "box_id") as? String
         conversionRate = aDecoder.decodeObject(forKey: "conversion_rate") as? String
         country = aDecoder.decodeObject(forKey: "country") as? String
         dateOfInvoice = aDecoder.decodeObject(forKey: "date_of_invoice") as? String
         deliveryAgent = aDecoder.decodeObject(forKey: "delivery_agent") as? String
         descriptionList = aDecoder.decodeObject(forKey: "description_list") as? [String]
         disAtTtd = aDecoder.decodeObject(forKey: "dis_at_ttd") as? String
         disAtUsd = aDecoder.decodeObject(forKey: "dis_at_usd") as? String
//         discountNote = aDecoder.decodeObject(forKey: "discount_note") as? String
//         discountTtd = aDecoder.decodeObject(forKey: "discount_ttd") as? String
//         discountUsd = aDecoder.decodeObject(forKey: "discount_usd") as? String
         driverName = aDecoder.decodeObject(forKey: "driver_name") as? String
         estimationList = aDecoder.decodeObject(forKey: "estimation_list") as? [EstimationList]
         fullRefund = aDecoder.decodeObject(forKey: "full_refund") as? String
         hawbNumber = aDecoder.decodeObject(forKey: "hawb_number") as? String
         invoiceId = aDecoder.decodeObject(forKey: "invoice_id") as? String
         invoiceNumber = aDecoder.decodeObject(forKey: "invoice_number") as? String
         isDiscount = aDecoder.decodeObject(forKey: "is_discount") as? String
         isRefund = aDecoder.decodeObject(forKey: "is_refund") as? String
         memberName = aDecoder.decodeObject(forKey: "member_name") as? String
         packageStatus = aDecoder.decodeObject(forKey: "package_status") as? [PackageStatus]
        //Nand : Payment detail changes
         paymentDetails = aDecoder.decodeObject(forKey: "payment_details") as? [PaymentDetails]
         paidDate = aDecoder.decodeObject(forKey: "paid_date") as? String
         paidStatus = aDecoder.decodeObject(forKey: "paid_status") as? String
         paymentType = aDecoder.decodeObject(forKey: "payment_type") as? String
         refundAmount = aDecoder.decodeObject(forKey: "refund_amount") as? AnyObject
         shipper = aDecoder.decodeObject(forKey: "shipper") as? String
         weight = aDecoder.decodeObject(forKey: "weight") as? String
         totalTtd = aDecoder.decodeObject(forKey: "total_ttd") as? String
         totalUsd = aDecoder.decodeObject(forKey: "total_usd") as? String
         trackingNumber = aDecoder.decodeObject(forKey: "tracking_number") as? String
         transactionId = aDecoder.decodeObject(forKey: "transaction_id") as? String
         batchQR = aDecoder.decodeObject(forKey: "batch_qr") as? String
         packageQR = aDecoder.decodeObject(forKey: "package_qr") as? String
        
        //Nand : Discount Data changes
        discountData = aDecoder.decodeObject(forKey: "discount_data") as? [DiscountData]
        
        //Credit Payment
        isCreditPayment = aDecoder.decodeObject(forKey: "is_credit_payment") as? Int
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if boxId != nil{
            aCoder.encode(boxId, forKey: "box_id")
        }
        if conversionRate != nil{
            aCoder.encode(conversionRate, forKey: "conversion_rate")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if dateOfInvoice != nil{
            aCoder.encode(dateOfInvoice, forKey: "date_of_invoice")
        }
        if deliveryAgent != nil{
            aCoder.encode(deliveryAgent, forKey: "delivery_agent")
        }
        if descriptionList != nil{
            aCoder.encode(descriptionList, forKey: "description_list")
        }
        if disAtTtd != nil{
            aCoder.encode(disAtTtd, forKey: "dis_at_ttd")
        }
        if disAtUsd != nil{
            aCoder.encode(disAtUsd, forKey: "dis_at_usd")
        }
//        if discountNote != nil{
//            aCoder.encode(discountNote, forKey: "discount_note")
//        }
//        if discountTtd != nil{
//            aCoder.encode(discountTtd, forKey: "discount_ttd")
//        }
//        if discountUsd != nil{
//            aCoder.encode(discountUsd, forKey: "discount_usd")
//        }
        if driverName != nil{
            aCoder.encode(driverName, forKey: "driver_name")
        }
        if estimationList != nil{
            aCoder.encode(estimationList, forKey: "estimation_list")
        }
        if fullRefund != nil{
            aCoder.encode(fullRefund, forKey: "full_refund")
        }
        if hawbNumber != nil{
            aCoder.encode(hawbNumber, forKey: "hawb_number")
        }
        if invoiceId != nil{
            aCoder.encode(invoiceId, forKey: "invoice_id")
        }
        if invoiceNumber != nil{
            aCoder.encode(invoiceNumber, forKey: "invoice_number")
        }
        if isDiscount != nil{
            aCoder.encode(isDiscount, forKey: "is_discount")
        }
        if isRefund != nil{
            aCoder.encode(isRefund, forKey: "is_refund")
        }
        if memberName != nil{
            aCoder.encode(memberName, forKey: "member_name")
        }
        if packageStatus != nil{
            aCoder.encode(packageStatus, forKey: "package_status")
        }
        //Nand : Payment detail changes
        if paymentDetails != nil{
            aCoder.encode(paymentDetails, forKey: "payment_details")
        }
        if paidDate != nil{
            aCoder.encode(paidDate, forKey: "paid_date")
        }
        if paidStatus != nil{
            aCoder.encode(paidStatus, forKey: "paid_status")
        }
        if paymentType != nil{
            aCoder.encode(paymentType, forKey: "payment_type")
        }
        if refundAmount != nil{
            aCoder.encode(refundAmount, forKey: "refund_amount")
        }
        if shipper != nil{
            aCoder.encode(shipper, forKey: "shipper")
        }
        if weight != nil{
            aCoder.encode(weight, forKey: "weight")
        }
        if totalTtd != nil{
            aCoder.encode(totalTtd, forKey: "total_ttd")
        }
        if totalUsd != nil{
            aCoder.encode(totalUsd, forKey: "total_usd")
        }
        if trackingNumber != nil{
            aCoder.encode(trackingNumber, forKey: "tracking_number")
        }
        if transactionId != nil{
            aCoder.encode(transactionId, forKey: "transaction_id")
        }
        if batchQR != nil{
            aCoder.encode(batchQR, forKey: "batch_qr")
        }
        if packageQR != nil{
            aCoder.encode(packageQR, forKey: "package_qr")
        }
        
        //Nand : Discount Data changes
        if discountData != nil{
            aCoder.encode(discountData, forKey: "discount_data")
        }
        
        //Credit Payment
        if isCreditPayment != nil{
            aCoder.encode(isCreditPayment, forKey: "is_credit_payment")
        }
    }

}
class PackageStatus : NSObject, NSCoding{

    var date : String!
    var title : String!
    var status : Int!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        date = json["Date"].stringValue
        title = json["Title"].stringValue
        status = json["status"].intValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if date != nil{
            dictionary["Date"] = date
        }
        if title != nil{
            dictionary["Title"] = title
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         date = aDecoder.decodeObject(forKey: "Date") as? String
         title = aDecoder.decodeObject(forKey: "Title") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if date != nil{
            aCoder.encode(date, forKey: "Date")
        }
        if title != nil{
            aCoder.encode(title, forKey: "Title")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }

    }

}
class EstimationList : NSObject, NSCoding{

    var amountTtd : String!
    var amountUsd : String!
    var descriptionField : String!
    var quantity : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        amountTtd = json["amount_ttd"].stringValue
        amountUsd = json["amount_usd"].stringValue
        descriptionField = json["description"].stringValue
        quantity = json["quantity"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amountTtd != nil{
            dictionary["amount_ttd"] = amountTtd
        }
        if amountUsd != nil{
            dictionary["amount_usd"] = amountUsd
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if quantity != nil{
            dictionary["quantity"] = quantity
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         amountTtd = aDecoder.decodeObject(forKey: "amount_ttd") as? String
         amountUsd = aDecoder.decodeObject(forKey: "amount_usd") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         quantity = aDecoder.decodeObject(forKey: "quantity") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if amountTtd != nil{
            aCoder.encode(amountTtd, forKey: "amount_ttd")
        }
        if amountUsd != nil{
            aCoder.encode(amountUsd, forKey: "amount_usd")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if quantity != nil{
            aCoder.encode(quantity, forKey: "quantity")
        }

    }

}
//Nand : Payment detail changes
class PaymentDetails : NSObject, NSCoding{

    var transactionId : String!
    var paymentType : String!
    var totalTtd : String!
    var totalUsd : String!
    var paymentDate : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        transactionId = json["transaction_id"].stringValue
        paymentType = json["payment_type"].stringValue
        totalTtd = json["total_ttd"].stringValue
        totalUsd = json["total_usd"].stringValue
        paymentDate = json["date"].stringValue
        
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if transactionId != nil{
            dictionary["transaction_id"] = transactionId
        }
        if paymentType != nil{
            dictionary["payment_type"] = paymentType
        }
        if totalTtd != nil{
            dictionary["total_ttd"] = totalTtd
        }
        if totalUsd != nil{
            dictionary["total_usd"] = totalUsd
        }
        if paymentDate != nil{
            dictionary["date"] = paymentDate
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        transactionId = aDecoder.decodeObject(forKey: "transaction_id") as? String
        paymentType = aDecoder.decodeObject(forKey: "payment_type") as? String
        totalTtd = aDecoder.decodeObject(forKey: "total_ttd") as? String
        totalUsd = aDecoder.decodeObject(forKey: "total_usd") as? String
        paymentDate = aDecoder.decodeObject(forKey: "date") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if transactionId != nil{
            aCoder.encode(transactionId, forKey: "transaction_id")
        }
        if paymentType != nil{
            aCoder.encode(paymentType, forKey: "payment_type")
        }
        if totalTtd != nil{
            aCoder.encode(totalTtd, forKey: "total_ttd")
        }
        if totalUsd != nil{
            aCoder.encode(totalUsd, forKey: "total_usd")
        }
        if paymentDate != nil{
            aCoder.encode(paymentDate, forKey: "date")
        }
    }

}
