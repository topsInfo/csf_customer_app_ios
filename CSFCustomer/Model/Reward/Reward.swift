//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Reward : NSObject, NSCoding{

	var list : [RewardList]!
	var totalCreditedTtd : String!
	var totalCreditedUsd : String!
	var totalPendingTtd : String!
	var totalPendingUsd : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		list = [RewardList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = RewardList(fromJson: listJson)
			list.append(value)
		}
		totalCreditedTtd = json["total_credited_ttd"].stringValue
		totalCreditedUsd = json["total_credited_usd"].stringValue
		totalPendingTtd = json["total_pending_ttd"].stringValue
		totalPendingUsd = json["total_pending_usd"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		if totalCreditedTtd != nil{
			dictionary["total_credited_ttd"] = totalCreditedTtd
		}
		if totalCreditedUsd != nil{
			dictionary["total_credited_usd"] = totalCreditedUsd
		}
		if totalPendingTtd != nil{
			dictionary["total_pending_ttd"] = totalPendingTtd
		}
		if totalPendingUsd != nil{
			dictionary["total_pending_usd"] = totalPendingUsd
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey: "list") as? [RewardList]
         totalCreditedTtd = aDecoder.decodeObject(forKey: "total_credited_ttd") as? String
         totalCreditedUsd = aDecoder.decodeObject(forKey: "total_credited_usd") as? String
         totalPendingTtd = aDecoder.decodeObject(forKey: "total_pending_ttd") as? String
         totalPendingUsd = aDecoder.decodeObject(forKey: "total_pending_usd") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
		if totalCreditedTtd != nil{
			aCoder.encode(totalCreditedTtd, forKey: "total_credited_ttd")
		}
		if totalCreditedUsd != nil{
			aCoder.encode(totalCreditedUsd, forKey: "total_credited_usd")
		}
		if totalPendingTtd != nil{
			aCoder.encode(totalPendingTtd, forKey: "total_pending_ttd")
		}
		if totalPendingUsd != nil{
			aCoder.encode(totalPendingUsd, forKey: "total_pending_usd")
		}

	}

}
class RewardList : NSObject, NSCoding{

    var amountTtd : String!
    var amountUsd : String!
    var boxId : String!
    var createdAt : String!
    var name : String!
    var profileImage : String!
    var status : Int!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        amountTtd = json["amount_ttd"].stringValue
        amountUsd = json["amount_usd"].stringValue
        boxId = json["box_id"].stringValue
        createdAt = json["created_at"].stringValue
        name = json["name"].stringValue
        profileImage = json["profile_image"].stringValue
        status = json["status"].intValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amountTtd != nil{
            dictionary["amount_ttd"] = amountTtd
        }
        if amountUsd != nil{
            dictionary["amount_usd"] = amountUsd
        }
        if boxId != nil{
            dictionary["box_id"] = boxId
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if name != nil{
            dictionary["name"] = name
        }
        if profileImage != nil{
            dictionary["profile_image"] = profileImage
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         amountTtd = aDecoder.decodeObject(forKey: "amount_ttd") as? String
         amountUsd = aDecoder.decodeObject(forKey: "amount_usd") as? String
         boxId = aDecoder.decodeObject(forKey: "box_id") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         profileImage = aDecoder.decodeObject(forKey: "profile_image") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if amountTtd != nil{
            aCoder.encode(amountTtd, forKey: "amount_ttd")
        }
        if amountUsd != nil{
            aCoder.encode(amountUsd, forKey: "amount_usd")
        }
        if boxId != nil{
            aCoder.encode(boxId, forKey: "box_id")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if profileImage != nil{
            aCoder.encode(profileImage, forKey: "profile_image")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }

    }

}
