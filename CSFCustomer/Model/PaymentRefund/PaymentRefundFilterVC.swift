//
//  PaymentRefundFilterVC.swift
//  CSFCustomer
//
//  Created by Tops on 25/01/20.
//

import UIKit
import DropDown
class PaymentRefundFilterVC: UIViewController,UITextFieldDelegate
{
    @IBOutlet weak var tblFilter : UITableView!
    @IBOutlet weak var startDateContainer : UIView!
    @IBOutlet weak var endDateContainer : UIView!
    @IBOutlet weak var searchContainer : UIView!
    @IBOutlet weak var txtStartDate : UITextField!
    @IBOutlet weak var txtEndDate : UITextField!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var dimmerView: UIView!
    @IBOutlet weak var btnApply : UIButton!
    @IBOutlet weak var btnReset : UIButton!
    let dropDown = DropDown()
    var arrContainers : [UIView] = [UIView]()
    var monthComponents = [String]()
    var currentControl = UITextField()
    
    lazy var monthFormatter : DateFormatter =
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM"
            return formatter
        }()
    //vars
    
    var strStartDt : String  = ""
    var strEndDt :String = ""
    var strSearch : String  = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //Mark :-Custom methods
    func configureControls()
    {
        showCard()
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black") //UIColor.black.withAlphaComponent(0.5)
        arrContainers = [startDateContainer,endDateContainer]
        for item in arrContainers
        {
            item.layer.borderWidth = 1
            item.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
        }
        tblFilter.tableFooterView = UIView()
        txtSearch.paddingView(xvalue: 10)
        configureTextFields()
        //==button
        btnApply.layer.cornerRadius = 2.0
        btnApply.layer.masksToBounds = true
        btnReset.layer.cornerRadius = 2.0
        btnReset.layer.masksToBounds = true
        //==set monthnames
        let formatter = DateFormatter()
        monthComponents = formatter.shortMonthSymbols
        txtStartDate.delegate = self
        txtEndDate.delegate = self
        //==set year picker
        showData()
    }
    func configureTextFields()
    {
        //Colors.theme_black_color , Colors.theme_white_color : bgcolor
        configureTextField(textField: txtStartDate, placeHolder: "Start Date", font: fontname.openSansRegular, fontSize: 14, textColor:UIColor(named: "theme_text_color")! , cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor:UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtEndDate, placeHolder: "End Date", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtSearch, placeHolder: "Search", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        txtStartDate.paddingView(xvalue: 10)
        txtEndDate.paddingView(xvalue: 10)
        setDayPicker()
    }
    func setDayPicker()
    {
        txtStartDate.addInputViewDatePicker(target: self,selector: #selector(doneButtonPressed),selectorChange:#selector(dateValueChanged),formatType:"",from:"RefundFilter")
        txtEndDate.addInputViewDatePicker(target: self,selector: #selector(doneButtonPressed),selectorChange:#selector(dateValueChanged),formatType:"",from:"RefundFilter")
    }
    func showData()
    {
        txtSearch.text = strSearch
        if let dt = destinationFormatter().date(from: strStartDt)
        {
            let startDt = sourceFormatter().string(from: dt)
            txtStartDate.text = startDt
        }
        if let dt1 = destinationFormatter().date(from: strEndDt)
        {
            let endDt = sourceFormatter().string(from: dt1)
            txtEndDate.text = endDt
        }
    }
    func showCard()
    {
        self.view.layoutIfNeeded()
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - 370
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
    }
    
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func doneButtonPressed()
    {
        if let  datePicker = currentControl.inputView as? UIDatePicker {
            setDate(datePicker: datePicker)
        }
        currentControl.resignFirstResponder()
        self.view.endEditing(true)
    }
    @objc func dateValueChanged(_ datePicker: UIDatePicker) {
        // setDate(datePicker: datePicker)
    }
    func setData(name:String)
    {
        currentControl.text = name
        if (currentControl == txtStartDate && txtEndDate.text == "") || (currentControl == txtEndDate && txtStartDate.text == "")
        {
            txtStartDate.text = name
            txtEndDate.text = name
        }
        else
        {
            let sDate = monthFormatter.date(from: txtStartDate.text!)!
            let dDate = monthFormatter.date(from: txtEndDate.text!)!
            compareTwoDates(date1:sDate,date2:dDate)
        }
    }
    func setDate(datePicker:UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        currentControl.text = dateFormatter.string(from: datePicker.date)
        if (currentControl == txtStartDate && txtEndDate.text == "") || (currentControl == txtEndDate && txtStartDate.text == "")
        {
            if txtEndDate.text == ""
            {
                txtEndDate.text = dateFormatter.string(from: datePicker.date)
            }
            else if txtStartDate.text == ""
            {
                txtStartDate.text = dateFormatter.string(from: datePicker.date)
            }
        }
        else
        {
            let date1 = dateFormatter.date(from: txtStartDate.text!)!
            let date2 = dateFormatter.date(from: txtEndDate.text!)!
            compareTwoDates(date1:date1,date2:date2)
        }
        
    }
    func compareTwoDates(date1:Date,date2:Date)
    {
        switch date1.compare(date2) {
        case .orderedAscending:
            //   print("asc")
            break
        case .orderedSame:
            //  print("same")
            if currentControl == txtStartDate
            {
                txtEndDate.text = txtStartDate.text
            }
            else
            {
                txtStartDate.text = txtEndDate.text
            }
            break
        case .orderedDescending:
            //  print("des")
            if currentControl == txtStartDate
            {
                txtEndDate.text = txtStartDate.text
            }
            else
            {
                txtStartDate.text = txtEndDate.text
            }
            break
            
        }
    }
    @IBAction func btnApplyClicked()
    {
        var startDt : String = ""
        var endDt : String = ""
        if let dt = sourceFormatter().date(from: txtStartDate.text ?? "")
        {
            startDt = destinationFormatter().string(from: dt)
        }
        if let dt1 = sourceFormatter().date(from: txtEndDate.text ?? "")
        {
            endDt = destinationFormatter().string(from: dt1)
        }
        let filterDict :[String: Any] = [
            "start_date" : startDt ,
            "end_date": endDt,
            "search_text" : txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
        ]
        NotificationCenter.default.post(name: Notification.Name("PaymentFilterApplied"), object: nil,userInfo: filterDict)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnResetClicked()
    {
        txtStartDate.text = ""
        txtEndDate.text = ""
        txtSearch.text = ""
    }
    //    func isValidData() -> Bool
    //    {
    //        if  isCheckNull(strText: txtStartDate.text!) == true && isCheckNull(strText: txtEndDate.text!) == true && isCheckNull(strText: txtSearch.text!) == true
    //        {
    //            return true
    //        }
    //       return true
    //    }
    //Mark :- Textfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentControl = textField
        if textField.text != ""
        {
            if let dt = sourceFormatter().date(from: textField.text ?? "")
            {
                if let  datePicker = currentControl.inputView as? UIDatePicker {
                    datePicker.setDate(dt, animated: false)
                }
            }
        }
    }
}
