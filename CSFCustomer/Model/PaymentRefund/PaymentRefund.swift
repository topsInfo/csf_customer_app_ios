//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PaymentRefund : NSObject, NSCoding{

	var list : [RefundList]!
	var totalPage : Int!
    var totalCount : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init()
    {
        
    }
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		list = [RefundList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = RefundList(fromJson: listJson)
			list.append(value)
		}
		totalPage = json["total_page"].intValue
        totalCount = json["total_count"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		if totalPage != nil{
			dictionary["total_page"] = totalPage
		}
        if totalCount != nil{
            dictionary["total_count"] = totalCount
        }

		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey: "list") as? [RefundList]
         totalPage = aDecoder.decodeObject(forKey: "total_page") as? Int
         totalCount = aDecoder.decodeObject(forKey: "total_count") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
		if totalPage != nil{
			aCoder.encode(totalPage, forKey: "total_page")
		}
        if totalCount != nil{
            aCoder.encode(totalCount, forKey: "total_count")
        }

	}

}

class RefundList : NSObject, NSCoding{

    var customerName : String!
    var dateOfInvoice : String!
    var hawbNumber : String!
    var id : String!
    var refundAmount : String!
    var refundDate : String!
    var refundMethod : String!
    var refundType : String!
    var totalUsd : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        customerName = json["customer_name"].stringValue
        dateOfInvoice = json["date_of_invoice"].stringValue
        hawbNumber = json["hawb_number"].stringValue
        id = json["id"].stringValue
        refundAmount = json["refund_amount"].stringValue
        refundDate = json["refund_date"].stringValue
        refundMethod = json["refund_method"].stringValue
        refundType = json["refund_type"].stringValue
        totalUsd = json["total_usd"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if customerName != nil{
            dictionary["customer_name"] = customerName
        }
        if dateOfInvoice != nil{
            dictionary["date_of_invoice"] = dateOfInvoice
        }
        if hawbNumber != nil{
            dictionary["hawb_number"] = hawbNumber
        }
        if id != nil{
            dictionary["id"] = id
        }
        if refundAmount != nil{
            dictionary["refund_amount"] = refundAmount
        }
        if refundDate != nil{
            dictionary["refund_date"] = refundDate
        }
        if refundMethod != nil{
            dictionary["refund_method"] = refundMethod
        }
        if refundType != nil{
            dictionary["refund_type"] = refundType
        }
        if totalUsd != nil{
            dictionary["total_usd"] = totalUsd
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         customerName = aDecoder.decodeObject(forKey: "customer_name") as? String
         dateOfInvoice = aDecoder.decodeObject(forKey: "date_of_invoice") as? String
         hawbNumber = aDecoder.decodeObject(forKey: "hawb_number") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         refundAmount = aDecoder.decodeObject(forKey: "refund_amount") as? String
         refundDate = aDecoder.decodeObject(forKey: "refund_date") as? String
         refundMethod = aDecoder.decodeObject(forKey: "refund_method") as? String
         refundType = aDecoder.decodeObject(forKey: "refund_type") as? String
         totalUsd = aDecoder.decodeObject(forKey: "total_usd") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if customerName != nil{
            aCoder.encode(customerName, forKey: "customer_name")
        }
        if dateOfInvoice != nil{
            aCoder.encode(dateOfInvoice, forKey: "date_of_invoice")
        }
        if hawbNumber != nil{
            aCoder.encode(hawbNumber, forKey: "hawb_number")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if refundAmount != nil{
            aCoder.encode(refundAmount, forKey: "refund_amount")
        }
        if refundDate != nil{
            aCoder.encode(refundDate, forKey: "refund_date")
        }
        if refundMethod != nil{
            aCoder.encode(refundMethod, forKey: "refund_method")
        }
        if refundType != nil{
            aCoder.encode(refundType, forKey: "refund_type")
        }
        if totalUsd != nil{
            aCoder.encode(totalUsd, forKey: "total_usd")
        }

    }

}
