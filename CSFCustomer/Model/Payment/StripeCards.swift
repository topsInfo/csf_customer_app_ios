//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class StripeCards : NSObject, NSCoding{

    var cardExpiredMsg : String!
    var cardExpiryMsg : String!
	var list : [StripeCardList]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init() {
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        
        cardExpiredMsg = json["card_expired_msg"].stringValue
        cardExpiryMsg = json["card_expiry_msg"].stringValue
        
		list = [StripeCardList]()
		let listArray = json["list"].arrayValue
        for listJson in listArray{
            let value = StripeCardList(fromJson: listJson)
            list.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if cardExpiredMsg != nil{
            dictionary["card_expired_msg"] = cardExpiredMsg
        }
        
        if cardExpiryMsg != nil{
            dictionary["card_expiry_msg"] = cardExpiryMsg
        }
        
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cardExpiredMsg = aDecoder.decodeObject(forKey: "card_expired_msg") as? String
         cardExpiryMsg = aDecoder.decodeObject(forKey: "card_expiry_msg") as? String
         list = aDecoder.decodeObject(forKey: "list") as? [StripeCardList]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
        if cardExpiredMsg != nil{
            aCoder.encode(cardExpiredMsg, forKey: "card_expired_msg")
        }
        
        if cardExpiryMsg != nil{
            aCoder.encode(cardExpiryMsg, forKey: "card_expiry_msg")
        }
        
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
	}

}
class StripeCardList : NSObject, NSCoding{
    
    var brand : String!
    var cardId : String!
    var last4 : String!
    var postalCode : String!
    var expiry : String!
    var isExpired : Bool!
    var isExpiry : Bool!
    
    override init()
    {
        
    }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        brand = json["brand"].stringValue
        cardId = json["card_id"].stringValue
        last4 = json["last4"].stringValue
        postalCode = json["postal_code"].stringValue

        expiry = json["expiry"].stringValue
        isExpired = json["is_expired"].boolValue
        isExpiry = json["is_expiry"].boolValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if brand != nil{
            dictionary["brand"] = brand
        }
        if cardId != nil{
            dictionary["card_id"] = cardId
        }
        if last4 != nil{
            dictionary["last4"] = last4
        }
        if postalCode != nil{
            dictionary["postal_code"] = postalCode
        }
        
        if expiry != nil{
            dictionary["expiry"] = expiry
        }
        
        if isExpired != nil{
            dictionary["is_expired"] = isExpired
        }
        
        if isExpiry != nil{
            dictionary["is_expiry"] = isExpiry
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         brand = aDecoder.decodeObject(forKey: "brand") as? String
         cardId = aDecoder.decodeObject(forKey: "card_id") as? String
         last4 = aDecoder.decodeObject(forKey: "last4") as? String
         postalCode = aDecoder.decodeObject(forKey: "postal_code") as? String
        
         expiry = aDecoder.decodeObject(forKey: "expiry") as? String
         isExpired = aDecoder.decodeObject(forKey: "is_expired") as? Bool
         isExpiry = aDecoder.decodeObject(forKey: "is_expiry") as? Bool
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if brand != nil{
            aCoder.encode(brand, forKey: "brand")
        }
        if cardId != nil{
            aCoder.encode(cardId, forKey: "card_id")
        }
        if last4 != nil{
            aCoder.encode(last4, forKey: "last4")
        }
        if postalCode != nil{
            aCoder.encode(postalCode, forKey: "postal_code")
        }
        
        if expiry != nil{
            aCoder.encode(expiry, forKey: "expiry")
        }
        if isExpired != nil{
            aCoder.encode(isExpired, forKey: "is_expired")
        }
        if isExpiry != nil{
            aCoder.encode(isExpiry, forKey: "is_expiry")
        }
    }
}
