//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class ViewCart : NSObject, NSCoding{

    var list : [CartList]!
    var totalTtdAmount : String!
    var totalUsdAmount : String!
    var walletUsd : String!
    var walletTtd : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        list = [CartList]()
        let listArray = json["list"].arrayValue
        for listJson in listArray{
            let value = CartList(fromJson: listJson)
            list.append(value)
        }
        totalTtdAmount = json["total_ttd_amount"].stringValue
        totalUsdAmount = json["total_usd_amount"].stringValue
        walletUsd = json["wallet_usd"].stringValue
        walletTtd = json["wallet_ttd"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if list != nil{
            var dictionaryElements = [[String:Any]]()
            for listElement in list {
                dictionaryElements.append(listElement.toDictionary())
            }
            dictionary["list"] = dictionaryElements
        }
        if totalTtdAmount != nil{
            dictionary["total_ttd_amount"] = totalTtdAmount
        }
        if totalUsdAmount != nil{
            dictionary["total_usd_amount"] = totalUsdAmount
        }
        if walletUsd != nil{
            dictionary["wallet_usd"] = walletUsd
        }
        if walletTtd != nil{
            dictionary["wallet_ttd"] = walletTtd
        }

        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         list = aDecoder.decodeObject(forKey: "list") as? [CartList]
         totalTtdAmount = aDecoder.decodeObject(forKey: "total_ttd_amount") as? String
         totalUsdAmount = aDecoder.decodeObject(forKey: "total_usd_amount") as? String
         walletUsd = aDecoder.decodeObject(forKey: "wallet_usd") as? String
         walletTtd = aDecoder.decodeObject(forKey: "wallet_ttd") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if list != nil{
            aCoder.encode(list, forKey: "list")
        }
        if totalTtdAmount != nil{
            aCoder.encode(totalTtdAmount, forKey: "total_ttd_amount")
        }
        if totalUsdAmount != nil{
            aCoder.encode(totalUsdAmount, forKey: "total_usd_amount")
        }
        if walletUsd != nil{
            aCoder.encode(walletUsd, forKey: "wallet_usd")
        }
        if walletTtd != nil{
            aCoder.encode(walletTtd, forKey: "wallet_ttd")
        }

    }

}
class CartList : NSObject, NSCoding{

    var date : String!
    var descriptionField : String!
    var discountTtd : String!
    var discountUsd : String!
    var hawbNumber : String!
    var invoiceId : Int!
    var isDiscount : Int!
    var totalTtd : String!
    var totalUsd : String!
    var received : String!
    var beforeDiscountTtd : String!
    var beforeDiscountUsd : String!
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    override init(){
        
    }
    
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        date = json["date"].stringValue
        descriptionField = json["description"].stringValue
        discountTtd = json["discount_ttd"].stringValue
        discountUsd = json["discount_usd"].stringValue
        hawbNumber = json["hawb_number"].stringValue
        invoiceId = json["invoice_id"].intValue
        isDiscount = json["is_discount"].intValue
        totalTtd = json["total_ttd"].stringValue
        totalUsd = json["total_usd"].stringValue
        received = json["received"].stringValue
        beforeDiscountTtd = json["before_discount_ttd"].stringValue
        beforeDiscountUsd = json["before_discount_usd"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if date != nil{
            dictionary["date"] = date
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if discountTtd != nil{
            dictionary["discount_ttd"] = discountTtd
        }
        if discountUsd != nil{
            dictionary["discount_usd"] = discountUsd
        }
        if hawbNumber != nil{
            dictionary["hawb_number"] = hawbNumber
        }
        if invoiceId != nil{
            dictionary["invoice_id"] = invoiceId
        }
        if isDiscount != nil{
            dictionary["is_discount"] = isDiscount
        }
        if totalTtd != nil{
            dictionary["total_ttd"] = totalTtd
        }
        if totalUsd != nil{
            dictionary["total_usd"] = totalUsd
        }
        if received != nil{
            dictionary["received"] = received
        }
        if beforeDiscountTtd != nil{
            dictionary["before_discount_ttd"] = beforeDiscountTtd
        }
        if beforeDiscountUsd != nil{
            dictionary["before_discount_usd"] = beforeDiscountUsd
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         date = aDecoder.decodeObject(forKey: "date") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         discountTtd = aDecoder.decodeObject(forKey: "discount_ttd") as? String
         discountUsd = aDecoder.decodeObject(forKey: "discount_usd") as? String
         hawbNumber = aDecoder.decodeObject(forKey: "hawb_number") as? String
         invoiceId = aDecoder.decodeObject(forKey: "invoice_id") as? Int
         isDiscount = aDecoder.decodeObject(forKey: "is_discount") as? Int
         totalTtd = aDecoder.decodeObject(forKey: "total_ttd") as? String
         totalUsd = aDecoder.decodeObject(forKey: "total_usd") as? String
         received = aDecoder.decodeObject(forKey: "received") as? String
         beforeDiscountTtd = aDecoder.decodeObject(forKey: "before_discount_ttd") as? String
         beforeDiscountUsd = aDecoder.decodeObject(forKey: "before_discount_usd") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if discountTtd != nil{
            aCoder.encode(discountTtd, forKey: "discount_ttd")
        }
        if discountUsd != nil{
            aCoder.encode(discountUsd, forKey: "discount_usd")
        }
        if hawbNumber != nil{
            aCoder.encode(hawbNumber, forKey: "hawb_number")
        }
        if invoiceId != nil{
            aCoder.encode(invoiceId, forKey: "invoice_id")
        }
        if isDiscount != nil{
            aCoder.encode(isDiscount, forKey: "is_discount")
        }
        if totalTtd != nil{
            aCoder.encode(totalTtd, forKey: "total_ttd")
        }
        if totalUsd != nil{
            aCoder.encode(totalUsd, forKey: "total_usd")
        }
        if received != nil{
            aCoder.encode(received, forKey: "received")
        }
        if beforeDiscountTtd != nil{
            aCoder.encode(beforeDiscountTtd, forKey: "before_discount_ttd")
        }
        if beforeDiscountUsd != nil{
            aCoder.encode(beforeDiscountUsd, forKey: "before_discount_usd")
        }
    }

}
