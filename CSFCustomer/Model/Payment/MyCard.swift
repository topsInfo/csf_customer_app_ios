//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class MyCard : NSObject, NSCoding{

	var list : [CardList]!

    /**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		list = [CardList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = CardList(fromJson: listJson)
			list.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey: "list") as? [CardList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}

	}

}
class CardList : NSObject, NSCoding{

    var address1 : String!
    var address2 : String!
    var cardNumber : String!
    var city : String!
    var exipry : String!
    var exipryFullyear : Int!
    var exipryMonth : Int!
    var exipryYear : String!
    var id : String!
    var name : String!
    var nickname : String!
    var postalCode : String!
    var province : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address1 = json["address1"].stringValue
        address2 = json["address2"].stringValue
        cardNumber = json["card_number"].stringValue
        city = json["city"].stringValue
        exipry = json["exipry"].stringValue
        exipryFullyear = json["exipry_fullyear"].intValue
        exipryMonth = json["exipry_month"].intValue
        exipryYear = json["exipry_year"].stringValue
        id = json["id"].stringValue
        name = json["name"].stringValue
        nickname = json["nickname"].stringValue
        postalCode = json["postal_code"].stringValue
        province = json["province"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address1 != nil{
            dictionary["address1"] = address1
        }
        if address2 != nil{
            dictionary["address2"] = address2
        }
        if cardNumber != nil{
            dictionary["card_number"] = cardNumber
        }
        if city != nil{
            dictionary["city"] = city
        }
        if exipry != nil{
            dictionary["exipry"] = exipry
        }
        if exipryFullyear != nil{
            dictionary["exipry_fullyear"] = exipryFullyear
        }
        if exipryMonth != nil{
            dictionary["exipry_month"] = exipryMonth
        }
        if exipryYear != nil{
            dictionary["exipry_year"] = exipryYear
        }
        if id != nil{
            dictionary["id"] = id
        }
        if name != nil{
            dictionary["name"] = name
        }
        if nickname != nil{
            dictionary["nickname"] = nickname
        }
        if postalCode != nil{
            dictionary["postal_code"] = postalCode
        }
        if province != nil{
            dictionary["province"] = province
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         address1 = aDecoder.decodeObject(forKey: "address1") as? String
         address2 = aDecoder.decodeObject(forKey: "address2") as? String
         cardNumber = aDecoder.decodeObject(forKey: "card_number") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         exipry = aDecoder.decodeObject(forKey: "exipry") as? String
         exipryFullyear = aDecoder.decodeObject(forKey: "exipry_fullyear") as? Int
         exipryMonth = aDecoder.decodeObject(forKey: "exipry_month") as? Int
         exipryYear = aDecoder.decodeObject(forKey: "exipry_year") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         nickname = aDecoder.decodeObject(forKey: "nickname") as? String
         postalCode = aDecoder.decodeObject(forKey: "postal_code") as? String
         province = aDecoder.decodeObject(forKey: "province") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if address1 != nil{
            aCoder.encode(address1, forKey: "address1")
        }
        if address2 != nil{
            aCoder.encode(address2, forKey: "address2")
        }
        if cardNumber != nil{
            aCoder.encode(cardNumber, forKey: "card_number")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if exipry != nil{
            aCoder.encode(exipry, forKey: "exipry")
        }
        if exipryFullyear != nil{
            aCoder.encode(exipryFullyear, forKey: "exipry_fullyear")
        }
        if exipryMonth != nil{
            aCoder.encode(exipryMonth, forKey: "exipry_month")
        }
        if exipryYear != nil{
            aCoder.encode(exipryYear, forKey: "exipry_year")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if nickname != nil{
            aCoder.encode(nickname, forKey: "nickname")
        }
        if postalCode != nil{
            aCoder.encode(postalCode, forKey: "postal_code")
        }
        if province != nil{
            aCoder.encode(province, forKey: "province")
        }

    }

}
