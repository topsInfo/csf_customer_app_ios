//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class InvoiceAtMiami : NSObject, NSCoding{

	var list : [InvoiceAtMiamiList]!
	var totalCount : Int!
	var totalPage : Int!

    override init() {
        
    }
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		list = [InvoiceAtMiamiList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = InvoiceAtMiamiList(fromJson: listJson)
			list.append(value)
		}
		totalCount = json["total_count"].intValue
		totalPage = json["total_page"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		if totalCount != nil{
			dictionary["total_count"] = totalCount
		}
		if totalPage != nil{
			dictionary["total_page"] = totalPage
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey: "list") as? [InvoiceAtMiamiList]
         totalCount = aDecoder.decodeObject(forKey: "total_count") as? Int
         totalPage = aDecoder.decodeObject(forKey: "total_page") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
		if totalCount != nil{
			aCoder.encode(totalCount, forKey: "total_count")
		}
		if totalPage != nil{
			aCoder.encode(totalPage, forKey: "total_page")
		}

	}

}

class InvoiceAtMiamiList : NSObject, NSCoding{

    var createdAt : String!
    var descriptionField : String!
    var hawbNumber : String!
    var shipperName : String!
    var shippingType : String!
    var status : String!
    var trackingNumber : String!
    var weight : String!
    
    var documentsArr : [String]!

    override init()
     {
        
     }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        hawbNumber = json["hawb_number"].stringValue
        shipperName = json["shipper_name"].stringValue
        shippingType = json["shipping_type"].stringValue
        status = json["status"].stringValue
        trackingNumber = json["tracking_number"].stringValue
        weight = json["weight"].stringValue
        
        documentsArr = [String]()
        let documentsArray = json["documents"].arrayValue
        for documentsReasonListJson in documentsArray{
            if let documentsReason : String = documentsReasonListJson.rawString() {
                documentsArr.append(documentsReason)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if hawbNumber != nil{
            dictionary["hawb_number"] = hawbNumber
        }
        if shipperName != nil{
            dictionary["shipper_name"] = shipperName
        }
        if shippingType != nil{
            dictionary["shipping_type"] = shippingType
        }
        if status != nil{
            dictionary["status"] = status
        }
        if trackingNumber != nil{
            dictionary["tracking_number"] = trackingNumber
        }
        if weight != nil{
            dictionary["weight"] = weight
        }
        if documentsArr != nil{
            dictionary["documents"] = documentsArr
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         hawbNumber = aDecoder.decodeObject(forKey: "hawb_number") as? String
         shipperName = aDecoder.decodeObject(forKey: "shipper_name") as? String
         shippingType = aDecoder.decodeObject(forKey: "shipping_type") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         trackingNumber = aDecoder.decodeObject(forKey: "tracking_number") as? String
         weight = aDecoder.decodeObject(forKey: "weight") as? String
         documentsArr = aDecoder.decodeObject(forKey: "documents") as? [String]
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if hawbNumber != nil{
            aCoder.encode(hawbNumber, forKey: "hawb_number")
        }
        if shipperName != nil{
            aCoder.encode(shipperName, forKey: "shipper_name")
        }
        if shippingType != nil{
            aCoder.encode(shippingType, forKey: "shipping_type")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if trackingNumber != nil{
            aCoder.encode(trackingNumber, forKey: "tracking_number")
        }
        if weight != nil{
            aCoder.encode(weight, forKey: "weight")
        }
        if documentsArr != nil{
            aCoder.encode(documentsArr, forKey: "documents")
        }
    }
}
