//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class UserInfo : NSObject, NSCoding, NSSecureCoding{

    static var supportsSecureCoding: Bool {
        return true
    }
    var boxId : String!
    var companyName : String!
    var emailAddress : String!
    var firstName : String!
    var id : String!
    var isParent : String!
    var isVerification : String!
    var lastName : String!
    var profileImagePath : String!
    var profileRequired : Int!
    var status : String!
    var referralCode : String!
    var token : String!
    var uniqueId : String!
    var mobileNo : String!
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    override init()
    {
        
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        boxId = json["box_id"].stringValue
        companyName = json["company_name"].stringValue
        emailAddress = json["email_address"].stringValue
        firstName = json["first_name"].stringValue
        id = json["id"].stringValue
        isParent = json["is_parent"].stringValue
        isVerification = json["is_verification"].stringValue
        lastName = json["last_name"].stringValue
        profileImagePath = json["profile_image_path"].stringValue
        profileRequired = json["profile_required"].intValue
        status = json["status"].stringValue
        referralCode = json["referral_code"].stringValue
        token = json["token"].stringValue
        uniqueId = json["unique_id"].stringValue
        mobileNo = json["mobile_no"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if boxId != nil{
            dictionary["box_id"] = boxId
        }
        if companyName != nil{
            dictionary["company_name"] = companyName
        }
        if emailAddress != nil{
            dictionary["email_address"] = emailAddress
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if id != nil{
            dictionary["id"] = id
        }
        if isParent != nil{
            dictionary["is_parent"] = isParent
        }
        if isVerification != nil{
            dictionary["is_verification"] = isVerification
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if profileImagePath != nil{
            dictionary["profile_image_path"] = profileImagePath
        }
        if profileRequired != nil{
            dictionary["profile_required"] = profileRequired
        }
        if status != nil{
            dictionary["status"] = status
        }
        if referralCode != nil{
            dictionary["referral_code"] = referralCode
        }
        if token != nil{
            dictionary["token"] = token
        }
        if uniqueId != nil{
            dictionary["unique_id"] = uniqueId
        }
        if mobileNo != nil{
            dictionary["mobile_no"] = mobileNo
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         boxId = aDecoder.decodeObject(forKey: "box_id") as? String
         companyName = aDecoder.decodeObject(forKey: "company_name") as? String
         emailAddress = aDecoder.decodeObject(forKey: "email_address") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         isParent = aDecoder.decodeObject(forKey: "is_parent") as? String
         isVerification = aDecoder.decodeObject(forKey: "is_verification") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         profileImagePath = aDecoder.decodeObject(forKey: "profile_image_path") as? String
         profileRequired = aDecoder.decodeObject(forKey: "profile_required") as? Int
         status = aDecoder.decodeObject(forKey: "status") as? String
         referralCode = aDecoder.decodeObject(forKey: "referral_code") as? String
         token = aDecoder.decodeObject(forKey: "token") as? String
         uniqueId = aDecoder.decodeObject(forKey: "unique_id") as? String
         mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if boxId != nil{
            aCoder.encode(boxId, forKey: "box_id")
        }
        if companyName != nil{
            aCoder.encode(companyName, forKey: "company_name")
        }
        if emailAddress != nil{
            aCoder.encode(emailAddress, forKey: "email_address")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if isParent != nil{
            aCoder.encode(isParent, forKey: "is_parent")
        }
        if isVerification != nil{
            aCoder.encode(isVerification, forKey: "is_verification")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "last_name")
        }
        if profileImagePath != nil{
            aCoder.encode(profileImagePath, forKey: "profile_image_path")
        }
        if profileRequired != nil{
            aCoder.encode(profileRequired, forKey: "profile_required")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if referralCode != nil{
            aCoder.encode(referralCode, forKey: "referral_code")
        }
        if token != nil{
            aCoder.encode(token, forKey: "token")
        }
        if uniqueId != nil{
            aCoder.encode(uniqueId, forKey: "unique_id")
        }
        if mobileNo != nil{
            aCoder.encode(mobileNo, forKey: "mobile_no")
        }

    }

}
