//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class FloridaAddress : NSObject, NSCoding{

	var list : [FloridaAddList]!
    var officeList : [OfficeList]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		list = [FloridaAddList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = FloridaAddList(fromJson: listJson)
			list.append(value)
		}
        officeList = [OfficeList]()
        let officeListArray = json["office_list"].arrayValue
        for officeListJson in officeListArray{
            let value = OfficeList(fromJson: officeListJson)
            officeList.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
        if officeList != nil{
            var dictionaryElements = [[String:Any]]()
            for officeListElement in officeList {
                dictionaryElements.append(officeListElement.toDictionary())
            }
            dictionary["office_list"] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey: "list") as? [FloridaAddList]
         officeList = aDecoder.decodeObject(forKey: "office_list") as? [OfficeList]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
        if officeList != nil{
            aCoder.encode(officeList, forKey: "office_list")
        }
	}
}

class FloridaAddList : NSObject, NSCoding{

    var address1 : String!
    var address2 : String!
    var city : String!
    var mobile : String!
    var name : String!
    var notes : String!
    var state : String!
    var title : String!
    var zipcode : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address1 = json["address_1"].stringValue
        address2 = json["address_2"].stringValue
        city = json["city"].stringValue
        mobile = json["mobile"].stringValue
        name = json["name"].stringValue
        notes = json["notes"].stringValue
        state = json["state"].stringValue
        title = json["title"].stringValue
        zipcode = json["zipcode"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address1 != nil{
            dictionary["address_1"] = address1
        }
        if address2 != nil{
            dictionary["address_2"] = address2
        }
        if city != nil{
            dictionary["city"] = city
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if name != nil{
            dictionary["name"] = name
        }
        if notes != nil{
            dictionary["notes"] = notes
        }
        if state != nil{
            dictionary["state"] = state
        }
        if title != nil{
            dictionary["title"] = title
        }
        if zipcode != nil{
            dictionary["zipcode"] = zipcode
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         address1 = aDecoder.decodeObject(forKey: "address_1") as? String
         address2 = aDecoder.decodeObject(forKey: "address_2") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         mobile = aDecoder.decodeObject(forKey: "mobile") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         notes = aDecoder.decodeObject(forKey: "notes") as? String
         state = aDecoder.decodeObject(forKey: "state") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         zipcode = aDecoder.decodeObject(forKey: "zipcode") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if address1 != nil{
            aCoder.encode(address1, forKey: "address_1")
        }
        if address2 != nil{
            aCoder.encode(address2, forKey: "address_2")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if notes != nil{
            aCoder.encode(notes, forKey: "notes")
        }
        if state != nil{
            aCoder.encode(state, forKey: "state")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if zipcode != nil{
            aCoder.encode(zipcode, forKey: "zipcode")
        }

    }

}
