//
//	OfficeList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class OfficeList : NSObject, NSCoding{

	var address : String!
	var lat : String!
	var longField : String!
	var title : String!
    var iosLat : String!
    var iosLong : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		address = json["address"].stringValue
		lat = json["lat"].stringValue
		longField = json["long"].stringValue
		title = json["title"].stringValue
        iosLat = json["ios_lat"].stringValue
        iosLong = json["ios_long"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if address != nil{
			dictionary["address"] = address
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if longField != nil{
			dictionary["long"] = longField
		}
		if title != nil{
			dictionary["title"] = title
		}
        if iosLat != nil{
            dictionary["ios_lat"] = iosLat
        }
        if iosLong != nil{
            dictionary["ios_long"] = iosLong
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         lat = aDecoder.decodeObject(forKey: "lat") as? String
         longField = aDecoder.decodeObject(forKey: "long") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         iosLat = aDecoder.decodeObject(forKey: "ios_lat") as? String
         iosLong = aDecoder.decodeObject(forKey: "ios_long") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if longField != nil{
			aCoder.encode(longField, forKey: "long")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
        if iosLat != nil{
            aCoder.encode(iosLat, forKey: "ios_lat")
        }
        if iosLong != nil{
            aCoder.encode(iosLong, forKey: "ios_long")
        }
	}
}
