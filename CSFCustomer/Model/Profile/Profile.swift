//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Profile : NSObject, NSCoding{

	var companyName : String!
	var dateOfBirth : String!
	var dateOfBirthString : String!
	var deliveryOption : String!
	var emailAddress : String!
	var firstName : String!
	var gender : String!
	var genderString : String!
	var lastName : String!
    var maskMobileNo : String!
    var maskPhoneNo : String!
	var memberName : String!
	var middleName : String!
	var mobileNo : String!
	var nationalIdList : [NationalIdList]!
	var nationalIdNumber : String!
	var nationalIdString : String!
	var nationalIdType : String!
	var phoneNo : String!
	var profileImage : String!
    var referralCode : String!
    var isReferalApplied : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init() {
        
    }
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		companyName = json["company_name"].stringValue
		dateOfBirth = json["date_of_birth"].stringValue
		dateOfBirthString = json["date_of_birth_string"].stringValue
		deliveryOption = json["delivery_option"].stringValue
		emailAddress = json["email_address"].stringValue
		firstName = json["first_name"].stringValue
		gender = json["gender"].stringValue
		genderString = json["gender_string"].stringValue
		lastName = json["last_name"].stringValue
        maskMobileNo = json["mask_mobile_no"].stringValue
        maskPhoneNo = json["mask_phone_no"].stringValue
		memberName = json["member_name"].stringValue
		middleName = json["middle_name"].stringValue
		mobileNo = json["mobile_no"].stringValue
		nationalIdList = [NationalIdList]()
		let nationalIdListArray = json["national_id_list"].arrayValue
		for nationalIdListJson in nationalIdListArray{
			let value = NationalIdList(fromJson: nationalIdListJson)
			nationalIdList.append(value)
		}
		nationalIdNumber = json["national_id_number"].stringValue
		nationalIdString = json["national_id_string"].stringValue
		nationalIdType = json["national_id_type"].stringValue
		phoneNo = json["phone_no"].stringValue
		profileImage = json["profile_image"].stringValue
        referralCode = json["referral_code"].stringValue
        isReferalApplied = json["is_referal_applied"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if companyName != nil{
			dictionary["company_name"] = companyName
		}
		if dateOfBirth != nil{
			dictionary["date_of_birth"] = dateOfBirth
		}
		if dateOfBirthString != nil{
			dictionary["date_of_birth_string"] = dateOfBirthString
		}
		if deliveryOption != nil{
			dictionary["delivery_option"] = deliveryOption
		}
		if emailAddress != nil{
			dictionary["email_address"] = emailAddress
		}
		if firstName != nil{
			dictionary["first_name"] = firstName
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if genderString != nil{
			dictionary["gender_string"] = genderString
		}
		if lastName != nil{
			dictionary["last_name"] = lastName
		}
        if maskMobileNo != nil{
            dictionary["mask_mobile_no"] = maskMobileNo
        }
        if maskPhoneNo != nil{
            dictionary["mask_phone_no"] = maskPhoneNo
        }
		if memberName != nil{
			dictionary["member_name"] = memberName
		}
		if middleName != nil{
			dictionary["middle_name"] = middleName
		}
		if mobileNo != nil{
			dictionary["mobile_no"] = mobileNo
		}
		if nationalIdList != nil{
			var dictionaryElements = [[String:Any]]()
			for nationalIdListElement in nationalIdList {
				dictionaryElements.append(nationalIdListElement.toDictionary())
			}
			dictionary["national_id_list"] = dictionaryElements
		}
		if nationalIdNumber != nil{
			dictionary["national_id_number"] = nationalIdNumber
		}
		if nationalIdString != nil{
			dictionary["national_id_string"] = nationalIdString
		}
		if nationalIdType != nil{
			dictionary["national_id_type"] = nationalIdType
		}
		if phoneNo != nil{
			dictionary["phone_no"] = phoneNo
		}
		if profileImage != nil{
			dictionary["profile_image"] = profileImage
		}
        if referralCode != nil{
            dictionary["referral_code"] = referralCode
        }
        if isReferalApplied != nil{
            dictionary["is_referal_applied"] = isReferalApplied
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         companyName = aDecoder.decodeObject(forKey: "company_name") as? String
         dateOfBirth = aDecoder.decodeObject(forKey: "date_of_birth") as? String
         dateOfBirthString = aDecoder.decodeObject(forKey: "date_of_birth_string") as? String
         deliveryOption = aDecoder.decodeObject(forKey: "delivery_option") as? String
         emailAddress = aDecoder.decodeObject(forKey: "email_address") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         genderString = aDecoder.decodeObject(forKey: "gender_string") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         maskMobileNo = aDecoder.decodeObject(forKey: "mask_mobile_no") as? String
         maskPhoneNo = aDecoder.decodeObject(forKey: "mask_phone_no") as? String
         memberName = aDecoder.decodeObject(forKey: "member_name") as? String
         middleName = aDecoder.decodeObject(forKey: "middle_name") as? String
         mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String
         nationalIdList = aDecoder.decodeObject(forKey: "national_id_list") as? [NationalIdList]
         nationalIdNumber = aDecoder.decodeObject(forKey: "national_id_number") as? String
         nationalIdString = aDecoder.decodeObject(forKey: "national_id_string") as? String
         nationalIdType = aDecoder.decodeObject(forKey: "national_id_type") as? String
         phoneNo = aDecoder.decodeObject(forKey: "phone_no") as? String
         profileImage = aDecoder.decodeObject(forKey: "profile_image") as? String
         referralCode = aDecoder.decodeObject(forKey: "referral_code") as? String
         isReferalApplied = aDecoder.decodeObject(forKey: "is_referal_applied") as? Int
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if companyName != nil{
			aCoder.encode(companyName, forKey: "company_name")
		}
		if dateOfBirth != nil{
			aCoder.encode(dateOfBirth, forKey: "date_of_birth")
		}
		if dateOfBirthString != nil{
			aCoder.encode(dateOfBirthString, forKey: "date_of_birth_string")
		}
		if deliveryOption != nil{
			aCoder.encode(deliveryOption, forKey: "delivery_option")
		}
		if emailAddress != nil{
			aCoder.encode(emailAddress, forKey: "email_address")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "first_name")
		}
		if gender != nil{
			aCoder.encode(gender, forKey: "gender")
		}
		if genderString != nil{
			aCoder.encode(genderString, forKey: "gender_string")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "last_name")
		}
        if maskMobileNo != nil{
            aCoder.encode(maskMobileNo, forKey: "mask_mobile_no")
        }
        if maskPhoneNo != nil{
            aCoder.encode(maskPhoneNo, forKey: "mask_phone_no")
        }
		if memberName != nil{
			aCoder.encode(memberName, forKey: "member_name")
		}
		if middleName != nil{
			aCoder.encode(middleName, forKey: "middle_name")
		}
		if mobileNo != nil{
			aCoder.encode(mobileNo, forKey: "mobile_no")
		}
		if nationalIdList != nil{
			aCoder.encode(nationalIdList, forKey: "national_id_list")
		}
		if nationalIdNumber != nil{
			aCoder.encode(nationalIdNumber, forKey: "national_id_number")
		}
		if nationalIdString != nil{
			aCoder.encode(nationalIdString, forKey: "national_id_string")
		}
		if nationalIdType != nil{
			aCoder.encode(nationalIdType, forKey: "national_id_type")
		}
		if phoneNo != nil{
			aCoder.encode(phoneNo, forKey: "phone_no")
		}
		if profileImage != nil{
			aCoder.encode(profileImage, forKey: "profile_image")
		}
        if referralCode != nil{
            aCoder.encode(referralCode, forKey: "referral_code")
        }
        if isReferalApplied != nil{
            aCoder.encode(isReferalApplied, forKey: "is_referal_applied")
        }
	}

}
class NationalIdList : NSObject, NSCoding{

    var id : Int!
    var name : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        name = json["name"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if name != nil{
            dictionary["name"] = name
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? Int
         name = aDecoder.decodeObject(forKey: "name") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }

    }

}
