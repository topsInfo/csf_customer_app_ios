import Foundation
import SwiftyJSON

class CalculatorData : NSObject, NSCoding{

    var conversionRate : String!
    var estimationList : [EstimationListData]!
    var finalTtd : String!
    var finalUsd : String!
    var totalAmountTtd : String!
    var totalAmountUsd : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    override init()
    {
        
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        conversionRate = json["conversion_rate"].stringValue
        estimationList = [EstimationListData]()
        let estimationListArray = json["estimation_list"].arrayValue
        for estimationListJson in estimationListArray{
            let value = EstimationListData(fromJson: estimationListJson)
            estimationList.append(value)
        }
        finalTtd = json["final_ttd"].stringValue
        finalUsd = json["final_usd"].stringValue
        totalAmountTtd = json["total_amount_ttd"].stringValue
        totalAmountUsd = json["total_amount_usd"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if conversionRate != nil{
            dictionary["conversion_rate"] = conversionRate
        }
        if estimationList != nil{
            var dictionaryElements = [[String:Any]]()
            for estimationListElement in estimationList {
                dictionaryElements.append(estimationListElement.toDictionary())
            }
            dictionary["estimation_list"] = dictionaryElements
        }
        if finalTtd != nil{
            dictionary["final_ttd"] = finalTtd
        }
        if finalUsd != nil{
            dictionary["final_usd"] = finalUsd
        }
        if totalAmountTtd != nil{
            dictionary["total_amount_ttd"] = totalAmountTtd
        }
        if totalAmountUsd != nil{
            dictionary["total_amount_usd"] = totalAmountUsd
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         conversionRate = aDecoder.decodeObject(forKey: "conversion_rate") as? String
         estimationList = aDecoder.decodeObject(forKey: "estimation_list") as? [EstimationListData]
         finalTtd = aDecoder.decodeObject(forKey: "final_ttd") as? String
         finalUsd = aDecoder.decodeObject(forKey: "final_usd") as? String
         totalAmountTtd = aDecoder.decodeObject(forKey: "total_amount_ttd") as? String
         totalAmountUsd = aDecoder.decodeObject(forKey: "total_amount_usd") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if conversionRate != nil{
            aCoder.encode(conversionRate, forKey: "conversion_rate")
        }
        if estimationList != nil{
            aCoder.encode(estimationList, forKey: "estimation_list")
        }
        if finalTtd != nil{
            aCoder.encode(finalTtd, forKey: "final_ttd")
        }
        if finalUsd != nil{
            aCoder.encode(finalUsd, forKey: "final_usd")
        }
        if totalAmountTtd != nil{
            aCoder.encode(totalAmountTtd, forKey: "total_amount_ttd")
        }
        if totalAmountUsd != nil{
            aCoder.encode(totalAmountUsd, forKey: "total_amount_usd")
        }

    }

}
class EstimationListData : NSObject, NSCoding{

    var amountTtd : String!
    var amountUsd : String!
    var qty : String!
    var title : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        amountTtd = json["amount_ttd"].stringValue
        amountUsd = json["amount_usd"].stringValue
        qty = json["qty"].stringValue
        title = json["title"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amountTtd != nil{
            dictionary["amount_ttd"] = amountTtd
        }
        if amountUsd != nil{
            dictionary["amount_usd"] = amountUsd
        }
        if qty != nil{
            dictionary["qty"] = qty
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         amountTtd = aDecoder.decodeObject(forKey: "amount_ttd") as? String
         amountUsd = aDecoder.decodeObject(forKey: "amount_usd") as? String
         qty = aDecoder.decodeObject(forKey: "qty") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if amountTtd != nil{
            aCoder.encode(amountTtd, forKey: "amount_ttd")
        }
        if amountUsd != nil{
            aCoder.encode(amountUsd, forKey: "amount_usd")
        }
        if qty != nil{
            aCoder.encode(qty, forKey: "qty")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
    }
}
