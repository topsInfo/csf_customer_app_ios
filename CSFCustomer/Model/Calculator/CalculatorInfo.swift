//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class CalculatorInfo : NSObject, NSCoding{

    var conversionRate : String!
   // var descriptionList : [AnyObject]!
    var shippingMethod : [ShippingMethod]!
    var customsLink : String!
    var customsMsg : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    
    override init() {
        
    }
    
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        conversionRate = json["conversion_rate"].stringValue
    //    descriptionList = [AnyObject]()
//        let descriptionListArray = json["description_list"].arrayValue
//        for descriptionListJson in descriptionListArray{
//            descriptionList.append(descriptionListJson.stringValue)
//        }
        shippingMethod = [ShippingMethod]()
        let shippingMethodArray = json["shipping_method"].arrayValue
        for shippingMethodJson in shippingMethodArray{
            let value = ShippingMethod(fromJson: shippingMethodJson)
            shippingMethod.append(value)
        }
        customsLink = json["customs_link"].stringValue
        customsMsg = json["customs_msg"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if conversionRate != nil{
            dictionary["conversion_rate"] = conversionRate
        }
//        if descriptionList != nil{
//            dictionary["description_list"] = descriptionList
//        }
        if shippingMethod != nil{
            var dictionaryElements = [[String:Any]]()
            for shippingMethodElement in shippingMethod {
                dictionaryElements.append(shippingMethodElement.toDictionary())
            }
            dictionary["shipping_method"] = dictionaryElements
        }
        if customsLink != nil{
            dictionary["customs_link"] = conversionRate
        }

        if customsMsg != nil{
            dictionary["customs_msg"] = conversionRate
        }

        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         conversionRate = aDecoder.decodeObject(forKey: "conversion_rate") as? String
       //  descriptionList = aDecoder.decodeObject(forKey: "description_list") as? [AnyObject]
         shippingMethod = aDecoder.decodeObject(forKey: "shipping_method") as? [ShippingMethod]
        customsLink = aDecoder.decodeObject(forKey: "customs_link") as? String
        customsMsg = aDecoder.decodeObject(forKey: "customs_msg") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if conversionRate != nil{
            aCoder.encode(conversionRate, forKey: "conversion_rate")
        }
//        if descriptionList != nil{
//            aCoder.encode(descriptionList, forKey: "description_list")
//        }
        if shippingMethod != nil{
            aCoder.encode(shippingMethod, forKey: "shipping_method")
        }
        
        if customsLink != nil{
            aCoder.encode(customsLink, forKey: "customs_link")
        }
        
        if customsMsg != nil{
            aCoder.encode(customsMsg, forKey: "customs_msg")
        }

    }

}
class ShippingMethod : NSObject, NSCoding{

    var id : String!
    var list : [ShippingList]!
    var title : String!
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    
    override init()
    {
        
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        list = [ShippingList]()
        let listArray = json["list"].arrayValue
        for listJson in listArray{
            let value = ShippingList(fromJson: listJson)
            list.append(value)
        }
        title = json["title"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if list != nil{
            var dictionaryElements = [[String:Any]]()
            for listElement in list {
                dictionaryElements.append(listElement.toDictionary())
            }
            dictionary["list"] = dictionaryElements
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? String
         list = aDecoder.decodeObject(forKey: "list") as? [ShippingList]
         title = aDecoder.decodeObject(forKey: "title") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if list != nil{
            aCoder.encode(list, forKey: "list")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
    }
}
class ShippingList : NSObject, NSCoding{

    var id : String!
    var title : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        title = json["title"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }

    }
}
