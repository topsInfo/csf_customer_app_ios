//
//	CreditDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CreditDetail : NSObject, NSCoding{

	var instruction : String!
	var status : Int!
	var totalOutstandingTtd : String!
	var totalOutstandingUsd : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		instruction = json["instruction"].stringValue
		status = json["status"].intValue
		totalOutstandingTtd = json["total_outstanding_ttd"].stringValue
		totalOutstandingUsd = json["total_outstanding_usd"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if instruction != nil{
			dictionary["instruction"] = instruction
		}
		if status != nil{
			dictionary["status"] = status
		}
		if totalOutstandingTtd != nil{
			dictionary["total_outstanding_ttd"] = totalOutstandingTtd
		}
		if totalOutstandingUsd != nil{
			dictionary["total_outstanding_usd"] = totalOutstandingUsd
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         instruction = aDecoder.decodeObject(forKey: "instruction") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Int
         totalOutstandingTtd = aDecoder.decodeObject(forKey: "total_outstanding_ttd") as? String
         totalOutstandingUsd = aDecoder.decodeObject(forKey: "total_outstanding_usd") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if instruction != nil{
			aCoder.encode(instruction, forKey: "instruction")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if totalOutstandingTtd != nil{
			aCoder.encode(totalOutstandingTtd, forKey: "total_outstanding_ttd")
		}
		if totalOutstandingUsd != nil{
			aCoder.encode(totalOutstandingUsd, forKey: "total_outstanding_usd")
		}
	}
}
