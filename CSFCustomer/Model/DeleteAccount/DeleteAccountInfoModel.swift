//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class DeleteAccountInfoModel : NSObject, NSCoding{

	var cardDetails : [CardDetail]!
	var creditDetails : CreditDetail!
	var feedbackReason : [FeedbackReasonDelAc]!
	var packageDetails : PackageDetailDelAc!
	var termsDocument : String!
	var walletDetails : WalletDetail!
    var finalSummaryArr : [String]!
    var isAllowed : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init() {
        
    }
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		cardDetails = [CardDetail]()
		let cardDetailsArray = json["card_details"].arrayValue
		for cardDetailsJson in cardDetailsArray{
			let value = CardDetail(fromJson: cardDetailsJson)
			cardDetails.append(value)
		}
		let creditDetailsJson = json["credit_details"]
		if !creditDetailsJson.isEmpty{
			creditDetails = CreditDetail(fromJson: creditDetailsJson)
		}
		feedbackReason = [FeedbackReasonDelAc]()
		let feedbackReasonArray = json["feedback_reason"].arrayValue
		for feedbackReasonJson in feedbackReasonArray{
			let value = FeedbackReasonDelAc(fromJson: feedbackReasonJson)
			feedbackReason.append(value)
		}
		let packageDetailsJson = json["package_details"]
		if !packageDetailsJson.isEmpty{
			packageDetails = PackageDetailDelAc(fromJson: packageDetailsJson)
		}
		termsDocument = json["terms_document"].stringValue
		let walletDetailsJson = json["wallet_details"]
		if !walletDetailsJson.isEmpty{
			walletDetails = WalletDetail(fromJson: walletDetailsJson)
		}
        
        finalSummaryArr = [String]()
        let finalSummaryArray = json["final_summary"].arrayValue
        for documentListJson in finalSummaryArray{
            if let summary : String = documentListJson.rawString() {
                finalSummaryArr.append(summary)
            }
            
        }
        isAllowed = json["is_allowed"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if cardDetails != nil{
			var dictionaryElements = [[String:Any]]()
			for cardDetailsElement in cardDetails {
				dictionaryElements.append(cardDetailsElement.toDictionary())
			}
			dictionary["card_details"] = dictionaryElements
		}
		if creditDetails != nil{
			dictionary["credit_details"] = creditDetails.toDictionary()
		}
		if feedbackReason != nil{
			var dictionaryElements = [[String:Any]]()
			for feedbackReasonElement in feedbackReason {
				dictionaryElements.append(feedbackReasonElement.toDictionary())
			}
			dictionary["feedback_reason"] = dictionaryElements
		}
		if packageDetails != nil{
			dictionary["package_details"] = packageDetails.toDictionary()
		}
		if termsDocument != nil{
			dictionary["terms_document"] = termsDocument
		}
		if walletDetails != nil{
			dictionary["wallet_details"] = walletDetails.toDictionary()
		}
        if finalSummaryArr != nil{
            dictionary["final_summary"] = finalSummaryArr
        }
        if isAllowed != nil{
            dictionary["is_allowed"] = isAllowed
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cardDetails = aDecoder.decodeObject(forKey: "card_details") as? [CardDetail]
         creditDetails = aDecoder.decodeObject(forKey: "credit_details") as? CreditDetail
         feedbackReason = aDecoder.decodeObject(forKey: "feedback_reason") as? [FeedbackReasonDelAc]
         packageDetails = aDecoder.decodeObject(forKey: "package_details") as? PackageDetailDelAc
         termsDocument = aDecoder.decodeObject(forKey: "terms_document") as? String
         walletDetails = aDecoder.decodeObject(forKey: "wallet_details") as? WalletDetail
         finalSummaryArr = aDecoder.decodeObject(forKey: "final_summary") as? [String]
         isAllowed = aDecoder.decodeObject(forKey: "is_allowed") as? Int
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if cardDetails != nil{
			aCoder.encode(cardDetails, forKey: "card_details")
		}
		if creditDetails != nil{
			aCoder.encode(creditDetails, forKey: "credit_details")
		}
		if feedbackReason != nil{
			aCoder.encode(feedbackReason, forKey: "feedback_reason")
		}
		if packageDetails != nil{
			aCoder.encode(packageDetails, forKey: "package_details")
		}
		if termsDocument != nil{
			aCoder.encode(termsDocument, forKey: "terms_document")
		}
		if walletDetails != nil{
			aCoder.encode(walletDetails, forKey: "wallet_details")
		}
        if finalSummaryArr != nil{
            aCoder.encode(finalSummaryArr, forKey: "final_summary")
        }
        if isAllowed != nil{
            aCoder.encode(isAllowed, forKey: "is_allowed")
        }
	}
}
