//
//	CardDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CardDetail : NSObject, NSCoding{

	var brand : String!
	var expiry : String!
	var last4 : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		brand = json["brand"].stringValue
		expiry = json["expiry"].stringValue
		last4 = json["last4"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if brand != nil{
			dictionary["brand"] = brand
		}
		if expiry != nil{
			dictionary["expiry"] = expiry
		}
		if last4 != nil{
			dictionary["last4"] = last4
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         brand = aDecoder.decodeObject(forKey: "brand") as? String
         expiry = aDecoder.decodeObject(forKey: "expiry") as? String
         last4 = aDecoder.decodeObject(forKey: "last4") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if brand != nil{
			aCoder.encode(brand, forKey: "brand")
		}
		if expiry != nil{
			aCoder.encode(expiry, forKey: "expiry")
		}
		if last4 != nil{
			aCoder.encode(last4, forKey: "last4")
		}
	}
}
