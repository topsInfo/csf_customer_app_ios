//
//	PackageDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PackageDetailDelAc : NSObject, NSCoding{

	var instruction : String!
	var pendingReceipts : Int!
	var returnInvoices : Int!
	var undeliveredInvoices : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		instruction = json["instruction"].stringValue
		pendingReceipts = json["pending_receipts"].intValue
		returnInvoices = json["return_invoices"].intValue
		undeliveredInvoices = json["undelivered_invoices"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if instruction != nil{
			dictionary["instruction"] = instruction
		}
		if pendingReceipts != nil{
			dictionary["pending_receipts"] = pendingReceipts
		}
		if returnInvoices != nil{
			dictionary["return_invoices"] = returnInvoices
		}
		if undeliveredInvoices != nil{
			dictionary["undelivered_invoices"] = undeliveredInvoices
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         instruction = aDecoder.decodeObject(forKey: "instruction") as? String
         pendingReceipts = aDecoder.decodeObject(forKey: "pending_receipts") as? Int
         returnInvoices = aDecoder.decodeObject(forKey: "return_invoices") as? Int
         undeliveredInvoices = aDecoder.decodeObject(forKey: "undelivered_invoices") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if instruction != nil{
			aCoder.encode(instruction, forKey: "instruction")
		}
		if pendingReceipts != nil{
			aCoder.encode(pendingReceipts, forKey: "pending_receipts")
		}
		if returnInvoices != nil{
			aCoder.encode(returnInvoices, forKey: "return_invoices")
		}
		if undeliveredInvoices != nil{
			aCoder.encode(undeliveredInvoices, forKey: "undelivered_invoices")
		}

	}

}
