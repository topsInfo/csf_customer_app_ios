//
//	WalletDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class WalletDetail : NSObject, NSCoding{

	var instruction : String!
	var instructionTwo : String!
	var mainBalanceTtd : String!
	var mainBalanceUsd : String!
    
    var nonRefundedTtd : String!
    var nonRefundedUsd : String!
    
    var refundedTtd : String!
    var refundedUsd : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		instruction = json["instruction"].stringValue
		instructionTwo = json["instruction_two"].stringValue
		mainBalanceTtd = json["main_balance_ttd"].stringValue
		mainBalanceUsd = json["main_balance_usd"].stringValue
        
        nonRefundedTtd = json["non_refunded_ttd"].stringValue
        nonRefundedUsd = json["non_refunded_usd"].stringValue
        
        refundedTtd = json["refunded_ttd"].stringValue
        refundedUsd = json["refunded_usd"].stringValue
        
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if instruction != nil{
			dictionary["instruction"] = instruction
		}
		if instructionTwo != nil{
			dictionary["instruction_two"] = instructionTwo
		}
		if mainBalanceTtd != nil{
			dictionary["main_balance_ttd"] = mainBalanceTtd
		}
		if mainBalanceUsd != nil{
			dictionary["main_balance_usd"] = mainBalanceUsd
		}
        if nonRefundedTtd != nil{
            dictionary["non_refunded_ttd"] = nonRefundedTtd
        }
        if nonRefundedUsd != nil{
            dictionary["non_refunded_usd"] = nonRefundedUsd
        }
        if refundedTtd != nil{
            dictionary["refunded_ttd"] = refundedTtd
        }
        if refundedUsd != nil{
            dictionary["refunded_usd"] = refundedUsd
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         instruction = aDecoder.decodeObject(forKey: "instruction") as? String
         instructionTwo = aDecoder.decodeObject(forKey: "instruction_two") as? String
         mainBalanceTtd = aDecoder.decodeObject(forKey: "main_balance_ttd") as? String
         mainBalanceUsd = aDecoder.decodeObject(forKey: "main_balance_usd") as? String
         nonRefundedTtd = aDecoder.decodeObject(forKey: "non_refunded_ttd") as? String
         nonRefundedUsd = aDecoder.decodeObject(forKey: "non_refunded_usd") as? String
         refundedTtd = aDecoder.decodeObject(forKey: "refunded_ttd") as? String
         refundedUsd = aDecoder.decodeObject(forKey: "refunded_usd") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if instruction != nil{
			aCoder.encode(instruction, forKey: "instruction")
		}
		if instructionTwo != nil{
			aCoder.encode(instructionTwo, forKey: "instruction_two")
		}
		if mainBalanceTtd != nil{
			aCoder.encode(mainBalanceTtd, forKey: "main_balance_ttd")
		}
		if mainBalanceUsd != nil{
			aCoder.encode(mainBalanceUsd, forKey: "main_balance_usd")
		}
        if nonRefundedTtd != nil{
            aCoder.encode(nonRefundedTtd, forKey: "non_refunded_ttd")
        }
        if nonRefundedUsd != nil{
            aCoder.encode(nonRefundedUsd, forKey: "non_refunded_usd")
        }
        if refundedUsd != nil{
            aCoder.encode(refundedUsd, forKey: "refunded_usd")
        }
        if refundedTtd != nil{
            aCoder.encode(refundedTtd, forKey: "refunded_ttd")
        }

	}

}
