//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ServiceGuide : NSObject, NSCoding{

	var serviceGuideAlert : Int!
	var serviceGuideLink : String!
	var serviceGuideMessage : String!
	var serviceGuideVersion : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		serviceGuideAlert = json["service_guide_alert"].intValue
		serviceGuideLink = json["service_guide_link"].stringValue
		serviceGuideMessage = json["service_guide_message"].stringValue
		serviceGuideVersion = json["service_guide_version"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if serviceGuideAlert != nil{
			dictionary["service_guide_alert"] = serviceGuideAlert
		}
		if serviceGuideLink != nil{
			dictionary["service_guide_link"] = serviceGuideLink
		}
		if serviceGuideMessage != nil{
			dictionary["service_guide_message"] = serviceGuideMessage
		}
		if serviceGuideVersion != nil{
			dictionary["service_guide_version"] = serviceGuideVersion
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         serviceGuideAlert = aDecoder.decodeObject(forKey: "service_guide_alert") as? Int
         serviceGuideLink = aDecoder.decodeObject(forKey: "service_guide_link") as? String
         serviceGuideMessage = aDecoder.decodeObject(forKey: "service_guide_message") as? String
         serviceGuideVersion = aDecoder.decodeObject(forKey: "service_guide_version") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if serviceGuideAlert != nil{
			aCoder.encode(serviceGuideAlert, forKey: "service_guide_alert")
		}
		if serviceGuideLink != nil{
			aCoder.encode(serviceGuideLink, forKey: "service_guide_link")
		}
		if serviceGuideMessage != nil{
			aCoder.encode(serviceGuideMessage, forKey: "service_guide_message")
		}
		if serviceGuideVersion != nil{
			aCoder.encode(serviceGuideVersion, forKey: "service_guide_version")
		}

	}

}
