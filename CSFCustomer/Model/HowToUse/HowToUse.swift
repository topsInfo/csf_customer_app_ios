//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class HowToUse : NSObject, NSCoding{

	var modules : [Module]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    
    override init()
    {
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		modules = [Module]()
		let modulesArray = json["modules"].arrayValue
		for modulesJson in modulesArray{
			let value = Module(fromJson: modulesJson)
			modules.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if modules != nil{
			var dictionaryElements = [[String:Any]]()
			for modulesElement in modules {
				dictionaryElements.append(modulesElement.toDictionary())
			}
			dictionary["modules"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         modules = aDecoder.decodeObject(forKey: "modules") as? [Module]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if modules != nil{
			aCoder.encode(modules, forKey: "modules")
		}

	}

}
