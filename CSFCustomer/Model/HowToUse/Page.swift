//
//	Page.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Page : NSObject, NSCoding{

	var descriptionField : String!
	var forAndroid : String!
	var forIos : String!
	var title : String!

    /**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init()
    {
        
    }    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		descriptionField = json["description"].stringValue
		forAndroid = json["for_android"].stringValue
		forIos = json["for_ios"].stringValue
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if forAndroid != nil{
			dictionary["for_android"] = forAndroid
		}
		if forIos != nil{
			dictionary["for_ios"] = forIos
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         forAndroid = aDecoder.decodeObject(forKey: "for_android") as? String
         forIos = aDecoder.decodeObject(forKey: "for_ios") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if forAndroid != nil{
			aCoder.encode(forAndroid, forKey: "for_android")
		}
		if forIos != nil{
			aCoder.encode(forIos, forKey: "for_ios")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
	}
}
