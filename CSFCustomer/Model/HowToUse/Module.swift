//
//	Module.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Module : NSObject, NSCoding{

	var descriptionField : String!
	var pages : [Page]!
	var title : String!

    /**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    
    override init()
    {
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		descriptionField = json["description"].stringValue
		pages = [Page]()
		let pagesArray = json["pages"].arrayValue
		for pagesJson in pagesArray{
			let value = Page(fromJson: pagesJson)
			pages.append(value)
		}
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if pages != nil{
			var dictionaryElements = [[String:Any]]()
			for pagesElement in pages {
				dictionaryElements.append(pagesElement.toDictionary())
			}
			dictionary["pages"] = dictionaryElements
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         pages = aDecoder.decodeObject(forKey: "pages") as? [Page]
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if pages != nil{
			aCoder.encode(pages, forKey: "pages")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}
