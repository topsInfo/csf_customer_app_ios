//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class CSFNotification : NSObject, NSCoding{

	var list : [NotificationList]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		list = [NotificationList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = NotificationList(fromJson: listJson)
			list.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey: "list") as? [NotificationList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}

	}

}
class NotificationList : NSObject, NSCoding{

    var createdAt : String!
    var isRead : String!
    var message : String!
    var redirect : String!
    var type : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["created_at"].stringValue
        isRead = json["is_read"].stringValue
        message = json["message"].stringValue
        redirect = json["redirect"].stringValue
        type = json["type"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if isRead != nil{
            dictionary["is_read"] = isRead
        }
        if message != nil{
            dictionary["message"] = message
        }
        if redirect != nil{
            dictionary["redirect"] = redirect
        }
        if type != nil{
            dictionary["type"] = type
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         isRead = aDecoder.decodeObject(forKey: "is_read") as? String
         message = aDecoder.decodeObject(forKey: "message") as? String
         redirect = aDecoder.decodeObject(forKey: "redirect") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if isRead != nil{
            aCoder.encode(isRead, forKey: "is_read")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if redirect != nil{
            aCoder.encode(redirect, forKey: "redirect")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }

    }

}
