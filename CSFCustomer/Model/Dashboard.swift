//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class Dashboard : NSObject, NSCoding{

    var cartCount : Int!
    var companyName : String!
    var feedbackReason : [FeedbackReason]!
    var firstName : String!
    var noticeMsg : String!
    
    //Weekend pref new feature
    var newFeatureEndpoint : String!
    var featureUpdateMsg : String!
    var showNewFeatureUpdates : Int!
    
    var notificationCount : String!
    var osticketLogoutUrl : String!
    var osticketWebUrl : String!
    var profileImagePath : String!
    var referralAmount : Int!
    var referralCode : String!
    var serviceGuideAlert : Int!
    var serviceGuideMessage : String!
    var serviceGuideVersion : String!
    var shippingMethod : [ShippingMethod]!
    var walletTtd : String!
    var walletUsd : String!
    var onlineShopperAlert : Int!
    var onlineShopperGuideMessage : String!
    var breakingNews : String!
    var breakingNewsLink : String!
    var documentLimit : Int!
    var documentSizeLimit : Int!
    var driveThruStatus : Int!
    var driveThruStatusMsg : String!
    
    override init()
    {
        
    }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        cartCount = json["cart_count"].intValue
        companyName = json["company_name"].stringValue
        feedbackReason = [FeedbackReason]()
        let feedbackReasonArray = json["feedback_reason"].arrayValue
        for feedbackReasonJson in feedbackReasonArray{
            let value = FeedbackReason(fromJson: feedbackReasonJson)
            feedbackReason.append(value)
        }
        firstName = json["first_name"].stringValue
        noticeMsg = json["notice_msg"].stringValue
        
        //Weekend pref new feature
        newFeatureEndpoint = json["new_feature_endpoint"].stringValue
        featureUpdateMsg = json["featureUpdateMsg"].stringValue
        showNewFeatureUpdates = json["show_new_feature_updates"].intValue

        notificationCount = json["notification_count"].stringValue
        osticketLogoutUrl = json["osticket_logout_url"].stringValue
        osticketWebUrl = json["osticket_web_url"].stringValue
        profileImagePath = json["profile_image_path"].stringValue
        referralAmount = json["referral_amount"].intValue
        referralCode = json["referral_code"].stringValue
        serviceGuideAlert = json["service_guide_alert"].intValue
        serviceGuideMessage = json["service_guide_message"].stringValue
        serviceGuideVersion = json["service_guide_version"].stringValue
        shippingMethod = [ShippingMethod]()
        let shippingMethodArray = json["shipping_method"].arrayValue
        for shippingMethodJson in shippingMethodArray{
            let value = ShippingMethod(fromJson: shippingMethodJson)
            shippingMethod.append(value)
        }
        walletTtd = json["wallet_ttd"].stringValue
        walletUsd = json["wallet_usd"].stringValue
        
        onlineShopperAlert = json["online_shopper_alert"].intValue
        onlineShopperGuideMessage = json["online_shopper_message"].stringValue
        breakingNews = json["breaking_news"].stringValue
        breakingNewsLink = json["breaking_news_link"].stringValue
        documentLimit = json["document_limit"].intValue
        documentSizeLimit = json["document_size_limit"].intValue
        driveThruStatus = json["drive_thru_status"].intValue
        driveThruStatusMsg = json["drive_thru_status_msg"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cartCount != nil{
            dictionary["cart_count"] = cartCount
        }
        if companyName != nil{
            dictionary["company_name"] = companyName
        }
        if feedbackReason != nil{
            var dictionaryElements = [[String:Any]]()
            for feedbackReasonElement in feedbackReason {
                dictionaryElements.append(feedbackReasonElement.toDictionary())
            }
            dictionary["feedback_reason"] = dictionaryElements
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if noticeMsg != nil{
            dictionary["notice_msg"] = noticeMsg
        }
        
        //Weekend pref new feature
        if newFeatureEndpoint != nil{
            dictionary["new_feature_endpoint"] = newFeatureEndpoint
        }
        if featureUpdateMsg != nil{
            dictionary["featureUpdateMsg"] = featureUpdateMsg
        }
        if showNewFeatureUpdates != nil{
            dictionary["show_new_feature_updates"] = showNewFeatureUpdates
        }
        
        if notificationCount != nil{
            dictionary["notification_count"] = notificationCount
        }
        if osticketLogoutUrl != nil{
            dictionary["osticket_logout_url"] = osticketLogoutUrl
        }
        if osticketWebUrl != nil{
            dictionary["osticket_web_url"] = osticketWebUrl
        }
        if profileImagePath != nil{
            dictionary["profile_image_path"] = profileImagePath
        }
        if referralAmount != nil{
            dictionary["referral_amount"] = referralAmount
        }
        if referralCode != nil{
            dictionary["referral_code"] = referralCode
        }
        if serviceGuideAlert != nil{
            dictionary["service_guide_alert"] = serviceGuideAlert
        }
        if serviceGuideMessage != nil{
            dictionary["service_guide_message"] = serviceGuideMessage
        }
        if serviceGuideVersion != nil{
            dictionary["service_guide_version"] = serviceGuideVersion
        }
        if shippingMethod != nil{
            var dictionaryElements = [[String:Any]]()
            for shippingMethodElement in shippingMethod {
                dictionaryElements.append(shippingMethodElement.toDictionary())
            }
            dictionary["shipping_method"] = dictionaryElements
        }
        if walletTtd != nil{
            dictionary["wallet_ttd"] = walletTtd
        }
        if walletUsd != nil{
            dictionary["wallet_usd"] = walletUsd
        }
        
        if onlineShopperAlert != nil{
            dictionary["online_shopper_alert"] = onlineShopperAlert
        }
        if onlineShopperGuideMessage != nil{
            dictionary["online_shopper_message"] = onlineShopperGuideMessage
        }
        if breakingNews != nil{
            dictionary["breaking_news"] = breakingNews
        }
        if breakingNewsLink != nil{
            dictionary["breaking_news_link"] = breakingNewsLink
        }
        if documentLimit != nil{
            dictionary["document_limit"] = documentLimit
        }
        if documentSizeLimit != nil{
            dictionary["document_size_limit"] = documentSizeLimit
        }
        if driveThruStatus != nil{
            dictionary["drive_thru_status"] = driveThruStatus
        }
        if driveThruStatusMsg != nil{
            dictionary["drive_thru_status_msg"] = driveThruStatusMsg
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        cartCount = aDecoder.decodeObject(forKey: "cart_count") as? Int
        companyName = aDecoder.decodeObject(forKey: "company_name") as? String
        feedbackReason = aDecoder.decodeObject(forKey: "feedback_reason") as? [FeedbackReason]
        firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        noticeMsg = aDecoder.decodeObject(forKey: "notice_msg") as? String
        
        //Weekend pref new feature
        newFeatureEndpoint = aDecoder.decodeObject(forKey: "new_feature_endpoint") as? String
        featureUpdateMsg = aDecoder.decodeObject(forKey: "featureUpdateMsg") as? String
        showNewFeatureUpdates = aDecoder.decodeObject(forKey: "show_new_feature_updates") as? Int
        
        notificationCount = aDecoder.decodeObject(forKey: "notification_count") as? String
        osticketLogoutUrl = aDecoder.decodeObject(forKey: "osticket_logout_url") as? String
        osticketWebUrl = aDecoder.decodeObject(forKey: "osticket_web_url") as? String
        profileImagePath = aDecoder.decodeObject(forKey: "profile_image_path") as? String
        referralAmount = aDecoder.decodeObject(forKey: "referral_amount") as? Int
        referralCode = aDecoder.decodeObject(forKey: "referral_code") as? String
        serviceGuideAlert = aDecoder.decodeObject(forKey: "service_guide_alert") as? Int
        serviceGuideMessage = aDecoder.decodeObject(forKey: "service_guide_message") as? String
        serviceGuideVersion = aDecoder.decodeObject(forKey: "service_guide_version") as? String
        shippingMethod = aDecoder.decodeObject(forKey: "shipping_method") as? [ShippingMethod]
        walletTtd = aDecoder.decodeObject(forKey: "wallet_ttd") as? String
        walletUsd = aDecoder.decodeObject(forKey: "wallet_usd") as? String
        onlineShopperAlert = aDecoder.decodeObject(forKey: "online_shopper_alert") as? Int
        onlineShopperGuideMessage = aDecoder.decodeObject(forKey: "online_shopper_message") as? String
        breakingNews = aDecoder.decodeObject(forKey: "breaking_news") as? String
        breakingNewsLink = aDecoder.decodeObject(forKey: "breaking_news_link") as? String
        documentLimit = aDecoder.decodeObject(forKey: "document_limit") as? Int
        documentSizeLimit = aDecoder.decodeObject(forKey: "document_size_limit") as? Int
        driveThruStatus = aDecoder.decodeObject(forKey: "drive_thru_status") as? Int
        driveThruStatusMsg = aDecoder.decodeObject(forKey: "drive_thru_status_msg") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if cartCount != nil{
            aCoder.encode(cartCount, forKey: "cart_count")
        }
        if companyName != nil{
            aCoder.encode(companyName, forKey: "company_name")
        }
        if feedbackReason != nil{
            aCoder.encode(feedbackReason, forKey: "feedback_reason")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if noticeMsg != nil{
            aCoder.encode(noticeMsg, forKey: "notice_msg")
        }
        
        //Weekend pref new feature
        if newFeatureEndpoint != nil{
            aCoder.encode(newFeatureEndpoint, forKey: "new_feature_endpoint")
        }
        if featureUpdateMsg != nil{
            aCoder.encode(featureUpdateMsg, forKey: "featureUpdateMsg")
        }
        if showNewFeatureUpdates != nil{
            aCoder.encode(showNewFeatureUpdates, forKey: "show_new_feature_updates")
        }

        if notificationCount != nil{
            aCoder.encode(notificationCount, forKey: "notification_count")
        }
        if osticketLogoutUrl != nil{
            aCoder.encode(osticketLogoutUrl, forKey: "osticket_logout_url")
        }
        if osticketWebUrl != nil{
            aCoder.encode(osticketWebUrl, forKey: "osticket_web_url")
        }
        if profileImagePath != nil{
            aCoder.encode(profileImagePath, forKey: "profile_image_path")
        }
        if referralAmount != nil{
            aCoder.encode(referralAmount, forKey: "referral_amount")
        }
        if referralCode != nil{
            aCoder.encode(referralCode, forKey: "referral_code")
        }
        if serviceGuideAlert != nil{
            aCoder.encode(serviceGuideAlert, forKey: "service_guide_alert")
        }
        if serviceGuideMessage != nil{
            aCoder.encode(serviceGuideMessage, forKey: "service_guide_message")
        }
        if serviceGuideVersion != nil{
            aCoder.encode(serviceGuideVersion, forKey: "service_guide_version")
        }
        if shippingMethod != nil{
            aCoder.encode(shippingMethod, forKey: "shipping_method")
        }
        if walletTtd != nil{
            aCoder.encode(walletTtd, forKey: "wallet_ttd")
        }
        if walletUsd != nil{
            aCoder.encode(walletUsd, forKey: "wallet_usd")
        }
        if onlineShopperAlert != nil{
            aCoder.encode(serviceGuideAlert, forKey: "online_shopper_alert")
        }
        if onlineShopperGuideMessage != nil{
            aCoder.encode(serviceGuideMessage, forKey: "online_shopper_message")
        }
        if breakingNews != nil{
            aCoder.encode(breakingNews, forKey: "breaking_news")
        }
        if breakingNewsLink != nil{
            aCoder.encode(breakingNewsLink, forKey: "breaking_news_link")
        }
        if documentLimit != nil{
            aCoder.encode(documentLimit, forKey: "document_limit")
        }
        if documentSizeLimit != nil{
            aCoder.encode(documentSizeLimit, forKey: "document_size_limit")
        }
        if driveThruStatus != nil{
            aCoder.encode(driveThruStatus, forKey: "drive_thru_status")
        }
        if driveThruStatusMsg != nil{
            aCoder.encode(driveThruStatusMsg, forKey: "drive_thru_status_msg")
        }
    }

}

class FeedbackReason : NSObject, NSCoding{

    var id : Int!
    var title : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        title = json["title"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? Int
         title = aDecoder.decodeObject(forKey: "title") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }

    }

}
