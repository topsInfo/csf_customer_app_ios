//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class OnlineShopperGuide : NSObject, NSCoding{

	var onlineShopperAlert : Int!
	var onlineShopperMessage : String!
	var onlineShopperGuideLink : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        onlineShopperAlert = json["online_shopper_alert"].intValue
        onlineShopperMessage = json["online_shopper_message"].stringValue
        onlineShopperGuideLink = json["online_shopper_guide_link"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if onlineShopperAlert != nil{
			dictionary["online_shopper_alert"] = onlineShopperAlert
		}
		if onlineShopperMessage != nil{
			dictionary["online_shopper_message"] = onlineShopperMessage
		}
		if onlineShopperGuideLink != nil{
			dictionary["online_shopper_guide_link"] = onlineShopperGuideLink
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        onlineShopperAlert = aDecoder.decodeObject(forKey: "online_shopper_alert") as? Int
        onlineShopperMessage = aDecoder.decodeObject(forKey: "online_shopper_message") as? String
        onlineShopperGuideLink = aDecoder.decodeObject(forKey: "online_shopper_guide_link") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if onlineShopperAlert != nil{
			aCoder.encode(onlineShopperAlert, forKey: "online_shopper_alert")
		}
		if onlineShopperMessage != nil{
			aCoder.encode(onlineShopperMessage, forKey: "online_shopper_message")
		}
		if onlineShopperGuideLink != nil{
			aCoder.encode(onlineShopperGuideLink, forKey: "online_shopper_guide_link")
		}
	}

}
