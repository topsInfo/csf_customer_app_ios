//
//	DriverList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class DriverList : NSObject, NSCoding{

	var driverName : String!
	var type : String!
	var zone : String!
	var zoneList : [ZoneList]!

    var depthLevel = 0
    var isExpanded = false
    var hasChild = false

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    
    override init()
    {
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		driverName = json["driver_name"].stringValue
		type = json["type"].stringValue
		zone = json["zone"].stringValue
		zoneList = [ZoneList]()
		let zoneListArray = json["zone_list"].arrayValue
        
        for (index, zoneListJson) in zoneListArray.enumerated(){
            let value = ZoneList(fromJson: zoneListJson)
            
            if index == 0 {
                let strArea : String = value.title ?? ""
                value.title = "Area\n\n\(strArea)"
            }
            zoneList.append(value)
		}        
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if driverName != nil{
			dictionary["driver_name"] = driverName
		}
		if type != nil{
			dictionary["type"] = type
		}
		if zone != nil{
			dictionary["zone"] = zone
		}
		if zoneList != nil{
			var dictionaryElements = [[String:Any]]()
			for zoneListElement in zoneList {
				dictionaryElements.append(zoneListElement.toDictionary())
			}
			dictionary["zone_list"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         driverName = aDecoder.decodeObject(forKey: "driver_name") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String
         zone = aDecoder.decodeObject(forKey: "zone") as? String
         zoneList = aDecoder.decodeObject(forKey: "zone_list") as? [ZoneList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if driverName != nil{
			aCoder.encode(driverName, forKey: "driver_name")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if zone != nil{
			aCoder.encode(zone, forKey: "zone")
		}
		if zoneList != nil{
			aCoder.encode(zoneList, forKey: "zone_list")
		}

	}

}
