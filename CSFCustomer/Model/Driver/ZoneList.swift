//
//	ZoneList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class ZoneList : NSObject, NSCoding{

	var areaList : String!
	var title : String!

    var childMArr = [ChildModel]()
    var depthLevel = 1
    var isExpanded = false
    var hasChild = false

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    
    override init()
    {
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		areaList = json["area_list"].stringValue
		title = json["title"].stringValue
        
        let obj : ChildModel = ChildModel()
        obj.zoneDescData = areaList
        childMArr.append(obj)
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if areaList != nil{
			dictionary["area_list"] = areaList
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         areaList = aDecoder.decodeObject(forKey: "area_list") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if areaList != nil{
			aCoder.encode(areaList, forKey: "area_list")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}
