//
//	DriverDataModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class DriverDataModel : NSObject, NSCoding{

	var contactNo : String!
	var driverList : [DriverList]!
	var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    
    override init()
    {
        
    }

	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		contactNo = json["contact_no"].stringValue
        driverList = [DriverList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = DriverList(fromJson: listJson)
            driverList.append(value)
		}
		status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if contactNo != nil{
			dictionary["contact_no"] = contactNo
		}
		if driverList != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in driverList {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         contactNo = aDecoder.decodeObject(forKey: "contact_no") as? String
         driverList = aDecoder.decodeObject(forKey: "list") as? [DriverList]
         status = aDecoder.decodeObject(forKey: "status") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if contactNo != nil{
			aCoder.encode(contactNo, forKey: "contact_no")
		}
		if driverList != nil{
			aCoder.encode(driverList, forKey: "list")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
