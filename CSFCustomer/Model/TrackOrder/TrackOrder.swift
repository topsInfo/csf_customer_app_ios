//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class TrackOrder : NSObject, NSCoding{

	var claimOrder : Int!
	var createdAt : String!
	var createdBy : String!
	var csfAddress1 : String!
	var csfAddress2 : String!
	var csfCompanyName : String!
	var hawbNumber : String!
	var logs : [Log]!
	var memberAddress : String!
	var memberEmail : String!
	var memberName : String!
	var orderId : String!
	var pacakgeDetails : [PackageDetail]!
    var packageAmount : String!
	var shipperName : String!
	var status : String!
	var totalQty : Int!
	var totalWeight : String!
	var trackingNumber : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init()
    {
        
    }
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		claimOrder = json["claim_order"].intValue
		createdAt = json["created_at"].stringValue
		createdBy = json["created_by"].stringValue
		csfAddress1 = json["csf_address1"].stringValue
		csfAddress2 = json["csf_address2"].stringValue
		csfCompanyName = json["csf_company_name"].stringValue
		hawbNumber = json["hawb_number"].stringValue
		logs = [Log]()
		let logsArray = json["logs"].arrayValue
		for logsJson in logsArray{
			let value = Log(fromJson: logsJson)
			logs.append(value)
		}
		memberAddress = json["member_address"].stringValue
		memberEmail = json["member_email"].stringValue
		memberName = json["member_name"].stringValue
		orderId = json["order_id"].stringValue
		pacakgeDetails = [PackageDetail]()
		let pacakgeDetailsArray = json["pacakge_details"].arrayValue
		for pacakgeDetailsJson in pacakgeDetailsArray{
			let value = PackageDetail(fromJson: pacakgeDetailsJson)
			pacakgeDetails.append(value)
		}
        packageAmount = json["package_amount"].stringValue
		shipperName = json["shipper_name"].stringValue
		status = json["status"].stringValue
		totalQty = json["total_qty"].intValue
		totalWeight = json["total_weight"].stringValue
		trackingNumber = json["tracking_number"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if claimOrder != nil{
			dictionary["claim_order"] = claimOrder
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if csfAddress1 != nil{
			dictionary["csf_address1"] = csfAddress1
		}
		if csfAddress2 != nil{
			dictionary["csf_address2"] = csfAddress2
		}
		if csfCompanyName != nil{
			dictionary["csf_company_name"] = csfCompanyName
		}
		if hawbNumber != nil{
			dictionary["hawb_number"] = hawbNumber
		}
		if logs != nil{
			var dictionaryElements = [[String:Any]]()
			for logsElement in logs {
				dictionaryElements.append(logsElement.toDictionary())
			}
			dictionary["logs"] = dictionaryElements
		}
		if memberAddress != nil{
			dictionary["member_address"] = memberAddress
		}
		if memberEmail != nil{
			dictionary["member_email"] = memberEmail
		}
		if memberName != nil{
			dictionary["member_name"] = memberName
		}
		if orderId != nil{
			dictionary["order_id"] = orderId
		}
		if pacakgeDetails != nil{
			var dictionaryElements = [[String:Any]]()
			for pacakgeDetailsElement in pacakgeDetails {
				dictionaryElements.append(pacakgeDetailsElement.toDictionary())
			}
			dictionary["pacakge_details"] = dictionaryElements
		}
        if packageAmount != nil{
            dictionary["package_amount"] = packageAmount
        }
		if shipperName != nil{
			dictionary["shipper_name"] = shipperName
		}
		if status != nil{
			dictionary["status"] = status
		}
		if totalQty != nil{
			dictionary["total_qty"] = totalQty
		}
		if totalWeight != nil{
			dictionary["total_weight"] = totalWeight
		}
		if trackingNumber != nil{
			dictionary["tracking_number"] = trackingNumber
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         claimOrder = aDecoder.decodeObject(forKey: "claim_order") as? Int
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         csfAddress1 = aDecoder.decodeObject(forKey: "csf_address1") as? String
         csfAddress2 = aDecoder.decodeObject(forKey: "csf_address2") as? String
         csfCompanyName = aDecoder.decodeObject(forKey: "csf_company_name") as? String
         hawbNumber = aDecoder.decodeObject(forKey: "hawb_number") as? String
         logs = aDecoder.decodeObject(forKey: "logs") as? [Log]
         memberAddress = aDecoder.decodeObject(forKey: "member_address") as? String
         memberEmail = aDecoder.decodeObject(forKey: "member_email") as? String
         memberName = aDecoder.decodeObject(forKey: "member_name") as? String
         orderId = aDecoder.decodeObject(forKey: "order_id") as? String
         pacakgeDetails = aDecoder.decodeObject(forKey: "pacakge_details") as? [PackageDetail]
         packageAmount = aDecoder.decodeObject(forKey: "package_amount") as? String
         shipperName = aDecoder.decodeObject(forKey: "shipper_name") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         totalQty = aDecoder.decodeObject(forKey: "total_qty") as? Int
         totalWeight = aDecoder.decodeObject(forKey: "total_weight") as? String
         trackingNumber = aDecoder.decodeObject(forKey: "tracking_number") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if claimOrder != nil{
			aCoder.encode(claimOrder, forKey: "claim_order")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if csfAddress1 != nil{
			aCoder.encode(csfAddress1, forKey: "csf_address1")
		}
		if csfAddress2 != nil{
			aCoder.encode(csfAddress2, forKey: "csf_address2")
		}
		if csfCompanyName != nil{
			aCoder.encode(csfCompanyName, forKey: "csf_company_name")
		}
		if hawbNumber != nil{
			aCoder.encode(hawbNumber, forKey: "hawb_number")
		}
		if logs != nil{
			aCoder.encode(logs, forKey: "logs")
		}
		if memberAddress != nil{
			aCoder.encode(memberAddress, forKey: "member_address")
		}
		if memberEmail != nil{
			aCoder.encode(memberEmail, forKey: "member_email")
		}
		if memberName != nil{
			aCoder.encode(memberName, forKey: "member_name")
		}
		if orderId != nil{
			aCoder.encode(orderId, forKey: "order_id")
		}
		if pacakgeDetails != nil{
			aCoder.encode(pacakgeDetails, forKey: "pacakge_details")
		}
        if packageAmount != nil{
            aCoder.encode(packageAmount, forKey: "package_amount")
        }
		if shipperName != nil{
			aCoder.encode(shipperName, forKey: "shipper_name")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if totalQty != nil{
			aCoder.encode(totalQty, forKey: "total_qty")
		}
		if totalWeight != nil{
			aCoder.encode(totalWeight, forKey: "total_weight")
		}
		if trackingNumber != nil{
			aCoder.encode(trackingNumber, forKey: "tracking_number")
		}

	}

}
class PackageDetail : NSObject, NSCoding{

    var descriptionField : String!
    var qty : Int!
    var weight : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        descriptionField = json["description"].stringValue
        qty = json["qty"].intValue
        weight = json["weight"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if qty != nil{
            dictionary["qty"] = qty
        }
        if weight != nil{
            dictionary["weight"] = weight
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         qty = aDecoder.decodeObject(forKey: "qty") as? Int
         weight = aDecoder.decodeObject(forKey: "weight") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if qty != nil{
            aCoder.encode(qty, forKey: "qty")
        }
        if weight != nil{
            aCoder.encode(weight, forKey: "weight")
        }

    }

}
class Log : NSObject, NSCoding{

    var date : String!
    var logs : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        date = json["date"].stringValue
        logs = json["logs"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if date != nil{
            dictionary["date"] = date
        }
        if logs != nil{
            dictionary["logs"] = logs
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         date = aDecoder.decodeObject(forKey: "date") as? String
         logs = aDecoder.decodeObject(forKey: "logs") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if logs != nil{
            aCoder.encode(logs, forKey: "logs")
        }

    }

}
