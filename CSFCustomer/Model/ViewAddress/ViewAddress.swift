//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON
class ViewAddress : NSObject, NSCoding{

    var address1 : String!
    var address2 : String!
    var areaId : String!
    var areaName : String!
    var isDefault : Int!
    var officeId : String!
    var officeName : String!
    var otherTitle : String!
    var typeOfTitle : String!
    var typeOfTitleString : String!
    
    var isWeekdays : String!
    var isSaturday : String!
    var isSunday : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    override init()
    {
        
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address1 = json["address1"].stringValue
        address2 = json["address2"].stringValue
        areaId = json["area_id"].stringValue
        areaName = json["area_name"].stringValue
        isDefault = json["is_default"].intValue
        officeId = json["office_id"].stringValue
        officeName = json["office_name"].stringValue
        otherTitle = json["other_title"].stringValue
        typeOfTitle = json["type_of_title"].stringValue
        typeOfTitleString = json["type_of_title_string"].stringValue
        
        isWeekdays = json["is_weekdays"].stringValue
        isSaturday = json["is_saturday"].stringValue
        isSunday = json["is_sunday"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address1 != nil{
            dictionary["address1"] = address1
        }
        if address2 != nil{
            dictionary["address2"] = address2
        }
        if areaId != nil{
            dictionary["area_id"] = areaId
        }
        if areaName != nil{
            dictionary["area_name"] = areaName
        }
        if isDefault != nil{
            dictionary["is_default"] = isDefault
        }
        if officeId != nil{
            dictionary["office_id"] = officeId
        }
        if officeName != nil{
            dictionary["office_name"] = officeName
        }
        if otherTitle != nil{
            dictionary["other_title"] = otherTitle
        }
        if typeOfTitle != nil{
            dictionary["type_of_title"] = typeOfTitle
        }
        if typeOfTitleString != nil{
            dictionary["type_of_title_string"] = typeOfTitleString
        }
        
        if isWeekdays != nil{
            dictionary["is_weekdays"] = isWeekdays
        }
        if isSaturday != nil{
            dictionary["is_saturday"] = isSaturday
        }
        if isSunday != nil{
            dictionary["is_sunday"] = isSunday
        }
        
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         address1 = aDecoder.decodeObject(forKey: "address1") as? String
         address2 = aDecoder.decodeObject(forKey: "address2") as? String
         areaId = aDecoder.decodeObject(forKey: "area_id") as? String
         areaName = aDecoder.decodeObject(forKey: "area_name") as? String
         isDefault = aDecoder.decodeObject(forKey: "is_default") as? Int
         officeId = aDecoder.decodeObject(forKey: "office_id") as? String
         officeName = aDecoder.decodeObject(forKey: "office_name") as? String
         otherTitle = aDecoder.decodeObject(forKey: "other_title") as? String
         typeOfTitle = aDecoder.decodeObject(forKey: "type_of_title") as? String
         typeOfTitleString = aDecoder.decodeObject(forKey: "type_of_title_string") as? String

         isWeekdays = aDecoder.decodeObject(forKey: "is_weekdays") as? String
         isSaturday = aDecoder.decodeObject(forKey: "is_saturday") as? String
         isSunday = aDecoder.decodeObject(forKey: "is_sunday") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if address1 != nil{
            aCoder.encode(address1, forKey: "address1")
        }
        if address2 != nil{
            aCoder.encode(address2, forKey: "address2")
        }
        if areaId != nil{
            aCoder.encode(areaId, forKey: "area_id")
        }
        if areaName != nil{
            aCoder.encode(areaName, forKey: "area_name")
        }
        if isDefault != nil{
            aCoder.encode(isDefault, forKey: "is_default")
        }
        if officeId != nil{
            aCoder.encode(officeId, forKey: "office_id")
        }
        if officeName != nil{
            aCoder.encode(officeName, forKey: "office_name")
        }
        if otherTitle != nil{
            aCoder.encode(otherTitle, forKey: "other_title")
        }
        if typeOfTitle != nil{
            aCoder.encode(typeOfTitle, forKey: "type_of_title")
        }
        if typeOfTitleString != nil{
            aCoder.encode(typeOfTitleString, forKey: "type_of_title_string")
        }
        
        if isWeekdays != nil{
            aCoder.encode(isWeekdays, forKey: "is_weekdays")
        }
        if isSaturday != nil{
            aCoder.encode(isSaturday, forKey: "is_saturday")
        }
        if isSunday != nil{
            aCoder.encode(isSunday, forKey: "is_sunday")
        }
        
    }
}
