//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class ChildUser : NSObject, NSCoding{

    var list : [ChildUserList]!
    var nationalIdList : [NationalIDList]!
    var totalPage : Int!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    override init()
    {
        
    }
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        list = [ChildUserList]()
        let listArray = json["list"].arrayValue
        for listJson in listArray{
            let value = ChildUserList(fromJson: listJson)
            list.append(value)
        }
        nationalIdList = [NationalIDList]()
        let nationalIdListArray = json["national_id_list"].arrayValue
        for nationalIdListJson in nationalIdListArray{
            let value = NationalIDList(fromJson: nationalIdListJson)
            nationalIdList.append(value)
        }
        totalPage = json["total_page"].intValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if list != nil{
            var dictionaryElements = [[String:Any]]()
            for listElement in list {
                dictionaryElements.append(listElement.toDictionary())
            }
            dictionary["list"] = dictionaryElements
        }
        if nationalIdList != nil{
            var dictionaryElements = [[String:Any]]()
            for nationalIdListElement in nationalIdList {
                dictionaryElements.append(nationalIdListElement.toDictionary())
            }
            dictionary["national_id_list"] = dictionaryElements
        }
        if totalPage != nil{
            dictionary["total_page"] = totalPage
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         list = aDecoder.decodeObject(forKey: "list") as? [ChildUserList]
         nationalIdList = aDecoder.decodeObject(forKey: "national_id_list") as? [NationalIDList]
         totalPage = aDecoder.decodeObject(forKey: "total_page") as? Int

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if list != nil{
            aCoder.encode(list, forKey: "list")
        }
        if nationalIdList != nil{
            aCoder.encode(nationalIdList, forKey: "national_id_list")
        }
        if totalPage != nil{
            aCoder.encode(totalPage, forKey: "total_page")
        }

    }

}
class NationalIDList : NSObject, NSCoding{

    var id : Int!
    var name : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        name = json["name"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if name != nil{
            dictionary["name"] = name
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? Int
         name = aDecoder.decodeObject(forKey: "name") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }

    }

}

class ChildUserList : NSObject, NSCoding{

    var boxId : String!
    var companyName : String!
    var dateOfBirth : String!
    var dateOfBirthText : String!
    var emailAddress : String!
    var firstName : String!
    var gender : String!
    var genderText : String!
    var id : String!
    var lastName : String!
    var middleName : String!
    var mobileNo : String!
    var name : String!
    var nationalIdNumber : String!
    var nationalIdType : String!
    var nationalIdTypeText : String!
    var phoneNo : String!
    var profileImage : String!

    override init()
    {
        
    }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        boxId = json["box_id"].stringValue
        companyName = json["company_name"].stringValue
        dateOfBirth = json["date_of_birth"].stringValue
        dateOfBirthText = json["date_of_birth_text"].stringValue
        emailAddress = json["email_address"].stringValue
        firstName = json["first_name"].stringValue
        gender = json["gender"].stringValue
        genderText = json["gender_text"].stringValue
        id = json["id"].stringValue
        lastName = json["last_name"].stringValue
        middleName = json["middle_name"].stringValue
        mobileNo = json["mobile_no"].stringValue
        name = json["name"].stringValue
        nationalIdNumber = json["national_id_number"].stringValue
        nationalIdType = json["national_id_type"].stringValue
        nationalIdTypeText = json["national_id_type_text"].stringValue
        phoneNo = json["phone_no"].stringValue
        profileImage = json["profile_image"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if boxId != nil{
            dictionary["box_id"] = boxId
        }
        if companyName != nil{
            dictionary["company_name"] = companyName
        }
        if dateOfBirth != nil{
            dictionary["date_of_birth"] = dateOfBirth
        }
        if dateOfBirthText != nil{
            dictionary["date_of_birth_text"] = dateOfBirthText
        }
        if emailAddress != nil{
            dictionary["email_address"] = emailAddress
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if genderText != nil{
            dictionary["gender_text"] = genderText
        }
        if id != nil{
            dictionary["id"] = id
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if middleName != nil{
            dictionary["middle_name"] = middleName
        }
        if mobileNo != nil{
            dictionary["mobile_no"] = mobileNo
        }
        if name != nil{
            dictionary["name"] = name
        }
        if nationalIdNumber != nil{
            dictionary["national_id_number"] = nationalIdNumber
        }
        if nationalIdType != nil{
            dictionary["national_id_type"] = nationalIdType
        }
        if nationalIdTypeText != nil{
            dictionary["national_id_type_text"] = nationalIdTypeText
        }
        if phoneNo != nil{
            dictionary["phone_no"] = phoneNo
        }
        if profileImage != nil{
            dictionary["profile_image"] = profileImage
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         boxId = aDecoder.decodeObject(forKey: "box_id") as? String
         companyName = aDecoder.decodeObject(forKey: "company_name") as? String
         dateOfBirth = aDecoder.decodeObject(forKey: "date_of_birth") as? String
         dateOfBirthText = aDecoder.decodeObject(forKey: "date_of_birth_text") as? String
         emailAddress = aDecoder.decodeObject(forKey: "email_address") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? String
         genderText = aDecoder.decodeObject(forKey: "gender_text") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         middleName = aDecoder.decodeObject(forKey: "middle_name") as? String
         mobileNo = aDecoder.decodeObject(forKey: "mobile_no") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         nationalIdNumber = aDecoder.decodeObject(forKey: "national_id_number") as? String
         nationalIdType = aDecoder.decodeObject(forKey: "national_id_type") as? String
         nationalIdTypeText = aDecoder.decodeObject(forKey: "national_id_type_text") as? String
         phoneNo = aDecoder.decodeObject(forKey: "phone_no") as? String
         profileImage = aDecoder.decodeObject(forKey: "profile_image") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if boxId != nil{
            aCoder.encode(boxId, forKey: "box_id")
        }
        if companyName != nil{
            aCoder.encode(companyName, forKey: "company_name")
        }
        if dateOfBirth != nil{
            aCoder.encode(dateOfBirth, forKey: "date_of_birth")
        }
        if dateOfBirthText != nil{
            aCoder.encode(dateOfBirthText, forKey: "date_of_birth_text")
        }
        if emailAddress != nil{
            aCoder.encode(emailAddress, forKey: "email_address")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if genderText != nil{
            aCoder.encode(genderText, forKey: "gender_text")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "last_name")
        }
        if middleName != nil{
            aCoder.encode(middleName, forKey: "middle_name")
        }
        if mobileNo != nil{
            aCoder.encode(mobileNo, forKey: "mobile_no")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if nationalIdNumber != nil{
            aCoder.encode(nationalIdNumber, forKey: "national_id_number")
        }
        if nationalIdType != nil{
            aCoder.encode(nationalIdType, forKey: "national_id_type")
        }
        if nationalIdTypeText != nil{
            aCoder.encode(nationalIdTypeText, forKey: "national_id_type_text")
        }
        if phoneNo != nil{
            aCoder.encode(phoneNo, forKey: "phone_no")
        }
        if profileImage != nil{
            aCoder.encode(profileImage, forKey: "profile_image")
        }
    }
}
