//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class WalletHistory : NSObject, NSCoding{

    var blockAmount : String!
    var historyList : [HistoryList]!
    var totalPage : Int!
    var walletTtd : String!
    var walletUsd : String!
    
    //Promotional Code
    var promotionalWalletTtd : String!
    var promotionalWalletUsd : String!
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        blockAmount = json["block_amount"].stringValue
        historyList = [HistoryList]()
        let historyListArray = json["history_list"].arrayValue
        for historyListJson in historyListArray{
            let value = HistoryList(fromJson: historyListJson)
            historyList.append(value)
        }
        totalPage = json["total_page"].intValue
        walletTtd = json["wallet_ttd"].stringValue
        walletUsd = json["wallet_usd"].stringValue
        
        //Promotional Code
        promotionalWalletUsd = json["promotional_wallet_usd"].stringValue
        promotionalWalletTtd = json["promotional_wallet_ttd"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if blockAmount != nil{
            dictionary["block_amount"] = blockAmount
        }
        if historyList != nil{
            var dictionaryElements = [[String:Any]]()
            for historyListElement in historyList {
                dictionaryElements.append(historyListElement.toDictionary())
            }
            dictionary["history_list"] = dictionaryElements
        }
        if totalPage != nil{
            dictionary["total_page"] = totalPage
        }
        if walletTtd != nil{
            dictionary["wallet_ttd"] = walletTtd
        }
        if walletUsd != nil{
            dictionary["wallet_usd"] = walletUsd
        }

        //Promotional Code
        if promotionalWalletTtd != nil{
            dictionary["promotional_wallet_ttd"] = promotionalWalletTtd
        }
        if promotionalWalletUsd != nil{
            dictionary["promotional_wallet_usd"] = promotionalWalletUsd
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        blockAmount = aDecoder.decodeObject(forKey: "block_amount") as? String
         historyList = aDecoder.decodeObject(forKey: "history_list") as? [HistoryList]
         totalPage = aDecoder.decodeObject(forKey: "total_page") as? Int
         walletTtd = aDecoder.decodeObject(forKey: "wallet_ttd") as? String
         walletUsd = aDecoder.decodeObject(forKey: "wallet_usd") as? String

        //Promotional Code
        promotionalWalletTtd = aDecoder.decodeObject(forKey: "promotional_wallet_ttd") as? String
        promotionalWalletUsd = aDecoder.decodeObject(forKey: "promotional_wallet_usd") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if blockAmount != nil{
            aCoder.encode(blockAmount, forKey: "block_amount")
        }
        if historyList != nil{
            aCoder.encode(historyList, forKey: "history_list")
        }
        if totalPage != nil{
            aCoder.encode(totalPage, forKey: "total_page")
        }
        if walletTtd != nil{
            aCoder.encode(walletTtd, forKey: "wallet_ttd")
        }
        if walletUsd != nil{
            aCoder.encode(walletUsd, forKey: "wallet_usd")
        }
        
        //Promotional Code
        if promotionalWalletTtd != nil{
            aCoder.encode(promotionalWalletTtd, forKey: "promotional_wallet_ttd")
        }
        if promotionalWalletUsd != nil{
            aCoder.encode(promotionalWalletUsd, forKey: "promotional_wallet_usd")
        }
    }

}

class HistoryList : NSObject, NSCoding{

    var amountTtd : String!
    var amountUsd : String!
    var date : String!
    var historyType : Int!
    var logs : String!
    var transactionId : String!
    var type : Int!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        amountTtd = json["amount_ttd"].stringValue
        amountUsd = json["amount_usd"].stringValue
        date = json["date"].stringValue
        historyType = json["history_type"].intValue
        logs = json["logs"].stringValue
        transactionId = json["transaction_id"].stringValue
        type = json["type"].intValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amountTtd != nil{
            dictionary["amount_ttd"] = amountTtd
        }
        if amountUsd != nil{
            dictionary["amount_usd"] = amountUsd
        }
        if date != nil{
            dictionary["date"] = date
        }
        if historyType != nil{
            dictionary["history_type"] = historyType
        }
        if logs != nil{
            dictionary["logs"] = logs
        }
        if transactionId != nil{
            dictionary["transaction_id"] = transactionId
        }
        if type != nil{
            dictionary["type"] = type
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         amountTtd = aDecoder.decodeObject(forKey: "amount_ttd") as? String
         amountUsd = aDecoder.decodeObject(forKey: "amount_usd") as? String
         date = aDecoder.decodeObject(forKey: "date") as? String
         historyType = aDecoder.decodeObject(forKey: "history_type") as? Int
         logs = aDecoder.decodeObject(forKey: "logs") as? String
         transactionId = aDecoder.decodeObject(forKey: "transaction_id") as? String
         type = aDecoder.decodeObject(forKey: "type") as? Int

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if amountTtd != nil{
            aCoder.encode(amountTtd, forKey: "amount_ttd")
        }
        if amountUsd != nil{
            aCoder.encode(amountUsd, forKey: "amount_usd")
        }
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if historyType != nil{
            aCoder.encode(historyType, forKey: "history_type")
        }
        if logs != nil{
            aCoder.encode(logs, forKey: "logs")
        }
        if transactionId != nil{
            aCoder.encode(transactionId, forKey: "transaction_id")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }

    }

}
