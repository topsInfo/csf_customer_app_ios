//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class WalletFreeze : NSObject, NSCoding{

	var isWalletFreeze : Bool!
//    var walletFreezeReason : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init(){
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		isWalletFreeze = json["is_wallet_freeze"].boolValue
//        walletFreezeReason = json["wallet_freeze_reason"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if isWalletFreeze != nil{
			dictionary["is_wallet_freeze"] = isWalletFreeze
		}
//        if walletFreezeReason != nil{
//            dictionary["wallet_freeze_reason"] = walletFreezeReason
//        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         isWalletFreeze = aDecoder.decodeObject(forKey: "is_wallet_freeze") as? Bool
//         walletFreezeReason = aDecoder.decodeObject(forKey: "wallet_freeze_reason") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if isWalletFreeze != nil{
			aCoder.encode(isWalletFreeze, forKey: "is_wallet_freeze")
		}
//        if walletFreezeReason != nil{
//            aCoder.encode(walletFreezeReason, forKey: "wallet_freeze_reason")
//        }
	}

}
