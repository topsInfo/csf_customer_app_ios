//
//	PickupSettingModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class PickupSettingModel : NSObject, NSCoding{

	var date : String!
	var endTime : String!
	var startTime : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init() {
        
    }
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		date = json["date"].stringValue
		endTime = json["end_time"].stringValue
		startTime = json["start_time"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if date != nil{
			dictionary["date"] = date
		}
		if endTime != nil{
			dictionary["end_time"] = endTime
		}
		if startTime != nil{
			dictionary["start_time"] = startTime
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         date = aDecoder.decodeObject(forKey: "date") as? String
         endTime = aDecoder.decodeObject(forKey: "end_time") as? String
         startTime = aDecoder.decodeObject(forKey: "start_time") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if date != nil{
			aCoder.encode(date, forKey: "date")
		}
		if endTime != nil{
			aCoder.encode(endTime, forKey: "end_time")
		}
		if startTime != nil{
			aCoder.encode(startTime, forKey: "start_time")
		}

	}

}
