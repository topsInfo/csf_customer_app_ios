//
//	RequestDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class DriveThruRequestDetail : NSObject, NSCoding{

	var dateOfRequest : String!
	var dateOfRequestString : String!
	var id : String!
	var isQuery : Int!
	var qrFilepath : String!
	var query : String!
	var requestId : String!
	var status : String!
    var statusId : Int!
    var statusMsg : String!
    var processedTime : String!
    var pickupDate : String!
    var pickupTime : String!
    var onDelivery : Int!
    
    var isChild : Int!
    var boxId : String!
    var memberName : String!
    
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init(){
        
    }
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        dateOfRequest = json["date"].stringValue
        dateOfRequestString = json["date_string"].stringValue
		id = json["id"].stringValue
		isQuery = json["is_query"].intValue
		qrFilepath = json["qr_filepath"].stringValue
		query = json["query"].stringValue
		requestId = json["request_id"].stringValue
		status = json["status"].stringValue
        statusId = json["status_id"].intValue
        statusMsg = json["status_msg"].stringValue
        processedTime = json["processed_time"].stringValue
        pickupDate = json["pickup_date"].stringValue
        pickupTime = json["pickup_time"].stringValue
        onDelivery = json["on_delivery"].intValue
                
        isChild = json["is_child"].intValue
        boxId = json["box_id"].stringValue
        memberName = json["member_name"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if dateOfRequest != nil{
			dictionary["date"] = dateOfRequest
		}
		if dateOfRequestString != nil{
			dictionary["date_string"] = dateOfRequestString
		}
		if id != nil{
			dictionary["id"] = id
		}
		if isQuery != nil{
			dictionary["is_query"] = isQuery
		}
		if qrFilepath != nil{
			dictionary["qr_filepath"] = qrFilepath
		}
		if query != nil{
			dictionary["query"] = query
		}
		if requestId != nil{
			dictionary["request_id"] = requestId
		}
		if status != nil{
			dictionary["status"] = status
		}
        if statusId != nil{
            dictionary["status_id"] = statusId
        }
        if statusMsg != nil{
            dictionary["status_msg"] = statusMsg
        }
        if processedTime != nil{
            dictionary["processed_time"] = processedTime
        }
        if pickupDate != nil{
            dictionary["pickup_date"] = pickupDate
        }
        if pickupTime != nil{
            dictionary["pickup_time"] = pickupTime
        }
        if onDelivery != nil{
            dictionary["on_delivery"] = onDelivery
        }

        if isChild != nil{
            dictionary["is_child"] = isChild
        }
        if boxId != nil{
            dictionary["box_id"] = boxId
        }
        if memberName != nil{
            dictionary["member_name"] = memberName
        }
        
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         dateOfRequest = aDecoder.decodeObject(forKey: "date") as? String
         dateOfRequestString = aDecoder.decodeObject(forKey: "date_string") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         isQuery = aDecoder.decodeObject(forKey: "is_query") as? Int
         qrFilepath = aDecoder.decodeObject(forKey: "qr_filepath") as? String
         query = aDecoder.decodeObject(forKey: "query") as? String
         requestId = aDecoder.decodeObject(forKey: "request_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         statusId = aDecoder.decodeObject(forKey: "status_id") as? Int
         statusMsg = aDecoder.decodeObject(forKey: "status_msg") as? String
         processedTime = aDecoder.decodeObject(forKey: "processed_time") as? String
         pickupDate = aDecoder.decodeObject(forKey: "pickup_date") as? String
         pickupTime = aDecoder.decodeObject(forKey: "pickup_time") as? String
         onDelivery = aDecoder.decodeObject(forKey: "on_delivery") as? Int
        
         isChild = aDecoder.decodeObject(forKey: "is_child") as? Int
         boxId = aDecoder.decodeObject(forKey: "box_id") as? String
         memberName = aDecoder.decodeObject(forKey: "member_name") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if dateOfRequest != nil{
			aCoder.encode(dateOfRequest, forKey: "date")
		}
		if dateOfRequestString != nil{
			aCoder.encode(dateOfRequestString, forKey: "date_string")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isQuery != nil{
			aCoder.encode(isQuery, forKey: "is_query")
		}
		if qrFilepath != nil{
			aCoder.encode(qrFilepath, forKey: "qr_filepath")
		}
		if query != nil{
			aCoder.encode(query, forKey: "query")
		}
		if requestId != nil{
			aCoder.encode(requestId, forKey: "request_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
        if statusId != nil{
            aCoder.encode(statusId, forKey: "status_id")
        }
        if statusMsg != nil{
            aCoder.encode(statusMsg, forKey: "status_msg")
        }
        if processedTime != nil{
            aCoder.encode(processedTime, forKey: "processed_time")
        }
        if pickupDate != nil{
            aCoder.encode(pickupDate, forKey: "pickup_date")
        }
        if pickupTime != nil{
            aCoder.encode(pickupTime, forKey: "pickup_time")
        }
        if onDelivery != nil{
            aCoder.encode(onDelivery, forKey: "on_delivery")
        }
        
        if isChild != nil{
            aCoder.encode(isChild, forKey: "is_child")
        }
        
        if boxId != nil{
            aCoder.encode(boxId, forKey: "box_id")
        }
        
        if memberName != nil{
            aCoder.encode(memberName, forKey: "member_name")
        }
	}
}
