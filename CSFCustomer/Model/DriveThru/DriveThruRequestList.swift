//
//	List.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class DriveThruRequestList : NSObject, NSCoding{

	var count : Int!
	var date : String!
	var dateString : String!
	var id : String!
	var isQuery : Int!
	var requestId : String!
	var status : String!
    var statusId : Int!
    var pickupDate : String!
    var pickupTime : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init(){
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		count = json["count"].intValue
		date = json["date"].stringValue
		dateString = json["date_string"].stringValue
		id = json["id"].stringValue
		isQuery = json["is_query"].intValue
		requestId = json["request_id"].stringValue
		status = json["status"].stringValue
        statusId = json["status_id"].intValue
        
        pickupDate = json["pickup_date"].stringValue
        pickupTime = json["pickup_time"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if count != nil{
			dictionary["count"] = count
		}
		if date != nil{
			dictionary["date"] = date
		}
		if dateString != nil{
			dictionary["date_string"] = dateString
		}
		if id != nil{
			dictionary["id"] = id
		}
		if isQuery != nil{
			dictionary["is_query"] = isQuery
		}
		if requestId != nil{
			dictionary["request_id"] = requestId
		}
		if status != nil{
			dictionary["status"] = status
		}
        if statusId != nil{
            dictionary["status_id"] = statusId
        }
        if pickupDate != nil{
            dictionary["pickup_date"] = pickupDate
        }
        if pickupTime != nil{
            dictionary["pickup_time"] = pickupTime
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         count = aDecoder.decodeObject(forKey: "count") as? Int
         date = aDecoder.decodeObject(forKey: "date") as? String
         dateString = aDecoder.decodeObject(forKey: "date_string") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         isQuery = aDecoder.decodeObject(forKey: "is_query") as? Int
         requestId = aDecoder.decodeObject(forKey: "request_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         statusId = aDecoder.decodeObject(forKey: "status_id") as? Int
         pickupDate = aDecoder.decodeObject(forKey: "pickup_date") as? String
         pickupTime = aDecoder.decodeObject(forKey: "pickup_time") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if count != nil{
			aCoder.encode(count, forKey: "count")
		}
		if date != nil{
			aCoder.encode(date, forKey: "date")
		}
		if dateString != nil{
			aCoder.encode(dateString, forKey: "date_string")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isQuery != nil{
			aCoder.encode(isQuery, forKey: "is_query")
		}
		if requestId != nil{
			aCoder.encode(requestId, forKey: "request_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
        if statusId != nil{
            aCoder.encode(statusId, forKey: "status_id")
        }
        if pickupDate != nil{
            aCoder.encode(pickupDate, forKey: "pickup_date")
        }
        if pickupTime != nil{
            aCoder.encode(pickupTime, forKey: "pickup_time")
        }
	}

}
