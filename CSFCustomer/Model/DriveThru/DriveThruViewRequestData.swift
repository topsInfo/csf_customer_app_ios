//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class DriveThruViewRequestData : NSObject, NSCoding{

	var packageDetails : [DriveThruPackageDetail]!
	var requestDetails : DriveThruRequestDetail!
    var pickupWaitTime : String!
    var pickupSettings : [PickupSettingModel]!
    var cancellationReasonArr : [String]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init(){
        
    }
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		packageDetails = [DriveThruPackageDetail]()
		let packageDetailsArray = json["package_details"].arrayValue
		for packageDetailsJson in packageDetailsArray{
			let value = DriveThruPackageDetail(fromJson: packageDetailsJson)
			packageDetails.append(value)
		}
		let requestDetailsJson = json["request_details"]
		if !requestDetailsJson.isEmpty{
			requestDetails = DriveThruRequestDetail(fromJson: requestDetailsJson)
		}
        
        pickupWaitTime = json["pickup_wait_time"].stringValue
        pickupSettings = [PickupSettingModel]()
        let pickupSettingsArray = json["pickup_setting"].arrayValue
        for listJson in pickupSettingsArray{
            let value = PickupSettingModel(fromJson: listJson)
            pickupSettings.append(value)
        }
        cancellationReasonArr = [String]()
        let cancellationReasonArray = json["cancellation_reason"].arrayValue
        for cancellationReasonListJson in cancellationReasonArray{
            if let cancellationReason : String = cancellationReasonListJson.rawString() {
                cancellationReasonArr.append(cancellationReason)
            }
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if packageDetails != nil{
			var dictionaryElements = [[String:Any]]()
			for packageDetailsElement in packageDetails {
				dictionaryElements.append(packageDetailsElement.toDictionary())
			}
			dictionary["package_details"] = dictionaryElements
		}
		if requestDetails != nil{
			dictionary["request_details"] = requestDetails.toDictionary()
		}
        if pickupWaitTime != nil{
            dictionary["pickup_wait_time"] = pickupWaitTime
        }
        if pickupSettings != nil{
            var dictionaryElements = [[String:Any]]()
            for listElement in pickupSettings {
                dictionaryElements.append(listElement.toDictionary())
            }
            dictionary["pickup_setting"] = dictionaryElements
        }
        if cancellationReasonArr != nil{
            dictionary["cancellation_reason"] = cancellationReasonArr
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        packageDetails = aDecoder.decodeObject(forKey: "package_details") as? [DriveThruPackageDetail]
        requestDetails = aDecoder.decodeObject(forKey: "request_details") as? DriveThruRequestDetail
        pickupWaitTime = aDecoder.decodeObject(forKey: "pickup_wait_time") as? String
        pickupSettings = aDecoder.decodeObject(forKey: "pickup_setting") as? [PickupSettingModel]
        cancellationReasonArr = aDecoder.decodeObject(forKey: "cancellation_reason") as? [String]
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if packageDetails != nil{
			aCoder.encode(packageDetails, forKey: "package_details")
		}
		if requestDetails != nil{
			aCoder.encode(requestDetails, forKey: "request_details")
		}
        if pickupWaitTime != nil{
            aCoder.encode(pickupWaitTime, forKey: "pickup_wait_time")
        }
        if pickupSettings != nil{
            aCoder.encode(pickupSettings, forKey: "pickup_setting")
        }
        if cancellationReasonArr != nil{
            aCoder.encode(cancellationReasonArr, forKey: "cancellation_reason")
        }
	}
}
