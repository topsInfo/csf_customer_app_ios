//
//	PackageDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class DriveThruPackageDetail : NSObject, NSCoding{

	var hawbNumber : String!
	var status : String!
    var statusId : Int!
    var itemDescription : String!
    var notes : String!
    
	 /* Instantiate the instance using the passed json values to set the properties values
	 */
    override init(){
        
    }
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		hawbNumber = json["hawb_number"].stringValue
		status = json["status"].stringValue
        statusId = json["status_id"].intValue
        itemDescription = json["description"].stringValue
        notes = json["notes"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if hawbNumber != nil{
			dictionary["hawb_number"] = hawbNumber
		}
		if status != nil{
			dictionary["status"] = status
		}
        if statusId != nil{
            dictionary["status_id"] = statusId
        }
        if itemDescription != nil{
            dictionary["description"] = itemDescription
        }
        if notes != nil{
            dictionary["notes"] = notes
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         hawbNumber = aDecoder.decodeObject(forKey: "hawb_number") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         statusId = aDecoder.decodeObject(forKey: "status_id") as? Int
         itemDescription = aDecoder.decodeObject(forKey: "description") as? String
         notes = aDecoder.decodeObject(forKey: "notes") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if hawbNumber != nil{
			aCoder.encode(hawbNumber, forKey: "hawb_number")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
        if statusId != nil{
            aCoder.encode(statusId, forKey: "status_id")
        }
        if itemDescription != nil{
            aCoder.encode(itemDescription, forKey: "description")
        }
        if notes != nil{
            aCoder.encode(notes, forKey: "notes")
        }
	}

}
