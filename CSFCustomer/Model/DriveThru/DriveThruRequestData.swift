//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class DriveThruRequestData : NSObject, NSCoding{

	var list : [DriveThruRequestList]!
	var totalCount : Int!
	var totalPage : Int!
    var pickupSettings : [PickupSettingModel]!
    var pickupWaitTime : String!
    var cancellationReasonArr : [String]!
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		list = [DriveThruRequestList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = DriveThruRequestList(fromJson: listJson)
			list.append(value)
		}
        pickupSettings = [PickupSettingModel]()
        let pickupSettingsArray = json["pickup_setting"].arrayValue
        for listJson in pickupSettingsArray{
            let value = PickupSettingModel(fromJson: listJson)
            pickupSettings.append(value)
        }
        
		totalCount = json["total_count"].intValue
		totalPage = json["total_page"].intValue
        pickupWaitTime = json["pickup_wait_time"].stringValue
        
        cancellationReasonArr = [String]()
        let cancellationReasonArray = json["cancellation_reason"].arrayValue
        for cancellationReasonListJson in cancellationReasonArray{
            if let cancellationReason : String = cancellationReasonListJson.rawString() {
                cancellationReasonArr.append(cancellationReason)
            }
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
        if pickupSettings != nil{
            var dictionaryElements = [[String:Any]]()
            for listElement in pickupSettings {
                dictionaryElements.append(listElement.toDictionary())
            }
            dictionary["pickup_setting"] = dictionaryElements
        }
        
		if totalCount != nil{
			dictionary["total_count"] = totalCount
		}
		if totalPage != nil{
			dictionary["total_page"] = totalPage
		}
        if pickupWaitTime != nil{
            dictionary["pickup_wait_time"] = pickupWaitTime
        }
        
        if cancellationReasonArr != nil{
            dictionary["cancellation_reason"] = cancellationReasonArr
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         list = aDecoder.decodeObject(forKey: "list") as? [DriveThruRequestList]
         pickupSettings = aDecoder.decodeObject(forKey: "pickup_setting") as? [PickupSettingModel]
         totalCount = aDecoder.decodeObject(forKey: "total_count") as? Int
         totalPage = aDecoder.decodeObject(forKey: "total_page") as? Int
         pickupWaitTime = aDecoder.decodeObject(forKey: "pickup_wait_time") as? String
         cancellationReasonArr = aDecoder.decodeObject(forKey: "cancellation_reason") as? [String]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
        if pickupSettings != nil{
            aCoder.encode(pickupSettings, forKey: "pickup_setting")
        }
		if totalCount != nil{
			aCoder.encode(totalCount, forKey: "total_count")
		}
		if totalPage != nil{
			aCoder.encode(totalPage, forKey: "total_page")
		}
        if pickupWaitTime != nil{
            aCoder.encode(pickupWaitTime, forKey: "pickup_wait_time")
        }
        if cancellationReasonArr != nil{
            aCoder.encode(cancellationReasonArr, forKey: "cancellation_reason")
        }
	}

}
