//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class AddToCartDataModel : NSObject, NSCoding{

	var addedCount : Int!
	var totalTtdAmount : String!
	var totalUsdAmount : String!
	var walletTtd : String!
	var walletUsd : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    
    override init() {
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		addedCount = json["added_count"].intValue
		totalTtdAmount = json["total_ttd_amount"].stringValue
		totalUsdAmount = json["total_usd_amount"].stringValue
		walletTtd = json["wallet_ttd"].stringValue
		walletUsd = json["wallet_usd"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if addedCount != nil{
			dictionary["added_count"] = addedCount
		}
		if totalTtdAmount != nil{
			dictionary["total_ttd_amount"] = totalTtdAmount
		}
		if totalUsdAmount != nil{
			dictionary["total_usd_amount"] = totalUsdAmount
		}
		if walletTtd != nil{
			dictionary["wallet_ttd"] = walletTtd
		}
		if walletUsd != nil{
			dictionary["wallet_usd"] = walletUsd
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addedCount = aDecoder.decodeObject(forKey: "added_count") as? Int
         totalTtdAmount = aDecoder.decodeObject(forKey: "total_ttd_amount") as? String
         totalUsdAmount = aDecoder.decodeObject(forKey: "total_usd_amount") as? String
         walletTtd = aDecoder.decodeObject(forKey: "wallet_ttd") as? String
         walletUsd = aDecoder.decodeObject(forKey: "wallet_usd") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if addedCount != nil{
			aCoder.encode(addedCount, forKey: "added_count")
		}
		if totalTtdAmount != nil{
			aCoder.encode(totalTtdAmount, forKey: "total_ttd_amount")
		}
		if totalUsdAmount != nil{
			aCoder.encode(totalUsdAmount, forKey: "total_usd_amount")
		}
		if walletTtd != nil{
			aCoder.encode(walletTtd, forKey: "wallet_ttd")
		}
		if walletUsd != nil{
			aCoder.encode(walletUsd, forKey: "wallet_usd")
		}

	}

}
