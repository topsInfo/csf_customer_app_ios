//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class PreAlertList : NSObject, NSCoding{

    var list : [List]!
    var shippingMethod : [PreAlertShippingMethod]!
    var totalPage : Int!
    var totalCount : Int!
    var documentLimit : Int!
    var documentSizeLimit : Int!
    
    var trackingNumberInstruction : String!
    var shipperInstruction : String!
    var descriptionInstruction : String!
    
    override init()
    {
        
    }

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        list = [List]()
        let listArray = json["list"].arrayValue
        for listJson in listArray{
            let value = List(fromJson: listJson)
            list.append(value)
        }
        shippingMethod = [PreAlertShippingMethod]()
        let shippingMethodArray = json["shipping_method"].arrayValue
        for shippingMethodJson in shippingMethodArray{
            let value = PreAlertShippingMethod(fromJson: shippingMethodJson)
            shippingMethod.append(value)
        }
        totalPage = json["total_page"].intValue
        totalCount = json["total_count"].intValue
        documentLimit = json["document_limit"].intValue
        documentSizeLimit = json["document_size_limit"].intValue
        
        trackingNumberInstruction = json["tracking_number_instruction"].stringValue
        shipperInstruction = json["shipper_instruction"].stringValue
        descriptionInstruction = json["description_instruction"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if list != nil{
            var dictionaryElements = [[String:Any]]()
            for listElement in list {
                dictionaryElements.append(listElement.toDictionary())
            }
            dictionary["list"] = dictionaryElements
        }
        if shippingMethod != nil{
            var dictionaryElements = [[String:Any]]()
            for shippingMethodElement in shippingMethod {
                dictionaryElements.append(shippingMethodElement.toDictionary())
            }
            dictionary["shipping_method"] = dictionaryElements
        }
        if totalPage != nil{
            dictionary["total_page"] = totalPage
        }
        if totalCount != nil{
            dictionary["total_count"] = totalCount
        }
        if documentLimit != nil{
            dictionary["document_limit"] = documentLimit
        }
        if documentSizeLimit != nil{
            dictionary["document_size_limit"] = documentSizeLimit
        }
        if trackingNumberInstruction != nil{
            dictionary["tracking_number_instruction"] = trackingNumberInstruction
        }
        if shipperInstruction != nil{
            dictionary["shipper_instruction"] = shipperInstruction
        }
        if descriptionInstruction != nil{
            dictionary["description_instruction"] = descriptionInstruction
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         list = aDecoder.decodeObject(forKey: "list") as? [List]
         shippingMethod = aDecoder.decodeObject(forKey: "shipping_method") as? [PreAlertShippingMethod]
         totalPage = aDecoder.decodeObject(forKey: "total_page") as? Int
         totalCount = aDecoder.decodeObject(forKey: "total_count") as? Int
         documentLimit = aDecoder.decodeObject(forKey: "document_limit") as? Int
         documentSizeLimit = aDecoder.decodeObject(forKey: "document_size_limit") as? Int
         
         trackingNumberInstruction = aDecoder.decodeObject(forKey: "tracking_number_instruction") as? String
         shipperInstruction = aDecoder.decodeObject(forKey: "shipper_instruction") as? String
         descriptionInstruction = aDecoder.decodeObject(forKey: "description_instruction") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if list != nil{
            aCoder.encode(list, forKey: "list")
        }
        if shippingMethod != nil{
            aCoder.encode(shippingMethod, forKey: "shipping_method")
        }
        if totalPage != nil{
            aCoder.encode(totalPage, forKey: "total_page")
        }
        if totalCount != nil{
            aCoder.encode(totalPage, forKey: "total_count")
        }
        if documentLimit != nil{
            aCoder.encode(documentLimit, forKey: "document_limit")
        }
        if documentSizeLimit != nil{
            aCoder.encode(documentSizeLimit, forKey: "document_size_limit")
        }
        if trackingNumberInstruction != nil{
            aCoder.encode(trackingNumberInstruction, forKey: "tracking_number_instruction")
        }
        if shipperInstruction != nil{
            aCoder.encode(shipperInstruction, forKey: "shipper_instruction")
        }
        if descriptionInstruction != nil{
            aCoder.encode(descriptionInstruction, forKey: "description_instruction")
        }
    }

}
class List : NSObject, NSCoding{

    var date : String!
    var descriptionField : String!
    var fullDescription : String!
    var id : String!
    var isReceived : String!
    var itemList : [ItemList]!
    var itemTotalAmount : String!
    var shipperId : String!
    var shipperName : String!
    var shippingMethod : String!
    var shippingMethodName : String!
    var trackingNumber : String!
    var documentPathArr : [String]!
    var documentNameArr : [String]!
    override init()
    {
        
    }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        date = json["date"].stringValue
        descriptionField = json["description"].stringValue
        fullDescription = json["full_description"].stringValue
        id = json["id"].stringValue
        isReceived = json["is_received"].stringValue
        itemList = [ItemList]()
        let itemListArray = json["item_list"].arrayValue
        for itemListJson in itemListArray{
            let value = ItemList(fromJson: itemListJson)
            itemList.append(value)
        }
        itemTotalAmount = json["item_total_amount"].stringValue
        shipperId = json["shipper_id"].stringValue
        shipperName = json["shipper_name"].stringValue
        shippingMethod = json["shipping_method"].stringValue
        shippingMethodName = json["shipping_method_name"].stringValue
        trackingNumber = json["tracking_number"].stringValue
        
        documentPathArr = [String]()
        let documentPathArray = json["document_path"].arrayValue
        for documentListJson in documentPathArray{
            if let strPath : String = documentListJson.rawString() {
                documentPathArr.append(strPath)
            }
            
        }
        
        documentNameArr = [String]()
        let documentNameArray = json["document_name"].arrayValue
        for documentListJson in documentNameArray{
            if let strPath : String = documentListJson.rawString() {
                documentNameArr.append(strPath)
            }
            
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if date != nil{
            dictionary["date"] = date
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if fullDescription != nil{
            dictionary["full_description"] = fullDescription
        }
        if id != nil{
            dictionary["id"] = id
        }
        if isReceived != nil{
            dictionary["is_received"] = isReceived
        }
        if itemList != nil{
            var dictionaryElements = [[String:Any]]()
            for itemListElement in itemList {
                dictionaryElements.append(itemListElement.toDictionary())
            }
            dictionary["item_list"] = dictionaryElements
        }
        if itemTotalAmount != nil{
            dictionary["item_total_amount"] = itemTotalAmount
        }
        if shipperId != nil{
            dictionary["shipper_id"] = shipperId
        }
        if shipperName != nil{
            dictionary["shipper_name"] = shipperName
        }
        if shippingMethod != nil{
            dictionary["shipping_method"] = shippingMethod
        }
        if shippingMethodName != nil{
            dictionary["shipping_method_name"] = shippingMethodName
        }
        if trackingNumber != nil{
            dictionary["tracking_number"] = trackingNumber
        }
        
        if documentPathArr != nil{
            dictionary["document_path"] = documentPathArr
        }
        
        if documentNameArr != nil{
            dictionary["document_name"] = documentNameArr
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         date = aDecoder.decodeObject(forKey: "date") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         fullDescription = aDecoder.decodeObject(forKey: "full_description") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         isReceived = aDecoder.decodeObject(forKey: "is_received") as? String
         itemList = aDecoder.decodeObject(forKey: "item_list") as? [ItemList]
         itemTotalAmount = aDecoder.decodeObject(forKey: "item_total_amount") as? String
         shipperId = aDecoder.decodeObject(forKey: "shipper_id") as? String
         shipperName = aDecoder.decodeObject(forKey: "shipper_name") as? String
         shippingMethod = aDecoder.decodeObject(forKey: "shipping_method") as? String
         shippingMethodName = aDecoder.decodeObject(forKey: "shipping_method_name") as? String
         trackingNumber = aDecoder.decodeObject(forKey: "tracking_number") as? String
         documentPathArr = aDecoder.decodeObject(forKey: "document_path") as? [String]
         documentNameArr = aDecoder.decodeObject(forKey: "document_name") as? [String]

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if fullDescription != nil{
            aCoder.encode(fullDescription, forKey: "full_description")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if isReceived != nil{
            aCoder.encode(isReceived, forKey: "is_received")
        }
        if itemList != nil{
            aCoder.encode(itemList, forKey: "item_list")
        }
        if itemTotalAmount != nil{
            aCoder.encode(itemTotalAmount, forKey: "item_total_amount")
        }
        if shipperId != nil{
            aCoder.encode(shipperId, forKey: "shipper_id")
        }
        if shipperName != nil{
            aCoder.encode(shipperName, forKey: "shipper_name")
        }
        if shippingMethod != nil{
            aCoder.encode(shippingMethod, forKey: "shipping_method")
        }
        if shippingMethodName != nil{
            aCoder.encode(shippingMethodName, forKey: "shipping_method_name")
        }
        if trackingNumber != nil{
            aCoder.encode(trackingNumber, forKey: "tracking_number")
        }
        if documentPathArr != nil{
            aCoder.encode(documentPathArr, forKey: "document_path")
        }
        if documentNameArr != nil{
            aCoder.encode(documentNameArr, forKey: "document_name")
        }
    }

}

class ItemList : NSObject, NSCoding{

    var amount : String!
    var descriptionField : String!

    override init()
    {
        
    }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        amount = json["amount"].stringValue
        descriptionField = json["description"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amount != nil{
            dictionary["amount"] = amount
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         amount = aDecoder.decodeObject(forKey: "amount") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if amount != nil{
            aCoder.encode(amount, forKey: "amount")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }

    }

}

class DocumentPathArr : NSObject, NSCoding{

    override init()
    {
        
    }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {

    }

}

class PreAlertShippingMethod : NSObject, NSCoding{

    var id : String!
    var title : String!

    override init()
    {
        
    }
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        title = json["title"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }

    }

}
