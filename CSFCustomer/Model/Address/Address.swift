//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class Address : NSObject, NSCoding{

	var addressTitle : [AddressTitle]!
	var deliveryAreas : [DeliveryArea]!
	var deliveryPickupOffices : [DeliveryPickupOffice]!
	var isDefault : Int!
	var list : [AddressList]!

    /**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init()
    {
        
    }
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		addressTitle = [AddressTitle]()
		let addressTitleArray = json["address_title"].arrayValue
		for addressTitleJson in addressTitleArray{
			let value = AddressTitle(fromJson: addressTitleJson)
			addressTitle.append(value)
		}
		deliveryAreas = [DeliveryArea]()
		let deliveryAreasArray = json["delivery_areas"].arrayValue
		for deliveryAreasJson in deliveryAreasArray{
			let value = DeliveryArea(fromJson: deliveryAreasJson)
			deliveryAreas.append(value)
		}
		deliveryPickupOffices = [DeliveryPickupOffice]()
		let deliveryPickupOfficesArray = json["delivery_pickup_offices"].arrayValue
		for deliveryPickupOfficesJson in deliveryPickupOfficesArray{
			let value = DeliveryPickupOffice(fromJson: deliveryPickupOfficesJson)
			deliveryPickupOffices.append(value)
		}
		isDefault = json["is_default"].intValue
		list = [AddressList]()
		let listArray = json["list"].arrayValue
		for listJson in listArray{
			let value = AddressList(fromJson: listJson)
			list.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if addressTitle != nil{
			var dictionaryElements = [[String:Any]]()
			for addressTitleElement in addressTitle {
				dictionaryElements.append(addressTitleElement.toDictionary())
			}
			dictionary["address_title"] = dictionaryElements
		}
		if deliveryAreas != nil{
			var dictionaryElements = [[String:Any]]()
			for deliveryAreasElement in deliveryAreas {
				dictionaryElements.append(deliveryAreasElement.toDictionary())
			}
			dictionary["delivery_areas"] = dictionaryElements
		}
		if deliveryPickupOffices != nil{
			var dictionaryElements = [[String:Any]]()
			for deliveryPickupOfficesElement in deliveryPickupOffices {
				dictionaryElements.append(deliveryPickupOfficesElement.toDictionary())
			}
			dictionary["delivery_pickup_offices"] = dictionaryElements
		}
		if isDefault != nil{
			dictionary["is_default"] = isDefault
		}
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addressTitle = aDecoder.decodeObject(forKey: "address_title") as? [AddressTitle]
         deliveryAreas = aDecoder.decodeObject(forKey: "delivery_areas") as? [DeliveryArea]
         deliveryPickupOffices = aDecoder.decodeObject(forKey: "delivery_pickup_offices") as? [DeliveryPickupOffice]
         isDefault = aDecoder.decodeObject(forKey: "is_default") as? Int
         list = aDecoder.decodeObject(forKey: "list") as? [AddressList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if addressTitle != nil{
			aCoder.encode(addressTitle, forKey: "address_title")
		}
		if deliveryAreas != nil{
			aCoder.encode(deliveryAreas, forKey: "delivery_areas")
		}
		if deliveryPickupOffices != nil{
			aCoder.encode(deliveryPickupOffices, forKey: "delivery_pickup_offices")
		}
		if isDefault != nil{
			aCoder.encode(isDefault, forKey: "is_default")
		}
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}

	}

}
class AddressList : NSObject, NSCoding{

    var address : String!
    var id : String!
    var isDefault : String!
    var officeId : String!
    var title : String!
    var isSaturday : String!
    var isSunday : String!
    var isWeekdays : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address = json["address"].stringValue
        id = json["id"].stringValue
        isDefault = json["is_default"].stringValue
        officeId = json["office_id"].stringValue
        title = json["title"].stringValue
        
        isSaturday = json["is_saturday"].stringValue
        isSunday = json["is_sunday"].stringValue
        isWeekdays = json["is_weekdays"].stringValue
        
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if id != nil{
            dictionary["id"] = id
        }
        if isDefault != nil{
            dictionary["is_default"] = isDefault
        }
        if officeId != nil{
            dictionary["office_id"] = officeId
        }
        if title != nil{
            dictionary["title"] = title
        }
        if isSaturday != nil{
            dictionary["is_saturday"] = isSaturday
        }
        if isSunday != nil{
            dictionary["is_sunday"] = isSunday
        }
        if isWeekdays != nil{
            dictionary["is_weekdays"] = isWeekdays
        }
        
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         address = aDecoder.decodeObject(forKey: "address") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         isDefault = aDecoder.decodeObject(forKey: "is_default") as? String
         officeId = aDecoder.decodeObject(forKey: "office_id") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         isSaturday = aDecoder.decodeObject(forKey: "is_saturday") as? String
         isSunday = aDecoder.decodeObject(forKey: "is_sunday") as? String
         isWeekdays = aDecoder.decodeObject(forKey: "is_weekdays") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if isDefault != nil{
            aCoder.encode(isDefault, forKey: "is_default")
        }
        if officeId != nil{
            aCoder.encode(officeId, forKey: "office_id")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }

        if isSaturday != nil{
            aCoder.encode(isSaturday, forKey: "is_saturday")
        }
        if isSunday != nil{
            aCoder.encode(isSunday, forKey: "is_sunday")
        }
        if isWeekdays != nil{
            aCoder.encode(isWeekdays, forKey: "is_weekdays")
        }
        
    }

}
class DeliveryPickupOffice : NSObject, NSCoding{

    var address : String!
    var id : String!
    var map : String!
    var title : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address = json["address"].stringValue
        id = json["id"].stringValue
        map = json["map"].stringValue
        title = json["title"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if id != nil{
            dictionary["id"] = id
        }
        if map != nil{
            dictionary["map"] = map
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         address = aDecoder.decodeObject(forKey: "address") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         map = aDecoder.decodeObject(forKey: "map") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if map != nil{
            aCoder.encode(map, forKey: "map")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }

    }

}
class DeliveryArea : NSObject, NSCoding{

    var id : String!
    var title : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        title = json["title"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }

    }

}
class AddressTitle : NSObject, NSCoding{

    var id : Int!
    var name : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        name = json["name"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if name != nil{
            dictionary["name"] = name
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "id") as? Int
         name = aDecoder.decodeObject(forKey: "name") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }

    }

}
