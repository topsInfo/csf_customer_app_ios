//
//  NPConfigurations.swift
//  NPEnviornments
//
//  Created by Tops on 28/12/23.
//

import Foundation

/// This class provides access to custom configurations defined in the app's Info.plist file.
final class NPConfigurations {

    /// The keys used to access specific configuration values.
    enum Key: String {
        case apiVersion = "kAPIVersion"
        case baseUrl = "kBaseURL"
        case stripePublickKey = "kStripePublickKey"
        case botId = "kBotId"
        case yellowMsgWhatsAppUrl = "kYellowMsgWhatsAppUrl"
    }

    /// Retrieves the value associated with the specified key from the custom configurations.
    /// - Parameter key: The key used to access the configuration value.
    /// - Returns: The configuration value associated with the key, or `nil` if the key is not found.
    static func getValueFor(_ key: Key) -> String? {
        guard let dictionary = Bundle.main.object(forInfoDictionaryKey: "CustomConfigurations") as? [String: String] else { return nil }
        return dictionary[key.rawValue]
    }

}
