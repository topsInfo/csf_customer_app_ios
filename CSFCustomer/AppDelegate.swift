//
//  AppDelegate.swift
//  CSFCustomer
//
//  Created by Tops on 23/11/20.
//
import UIKit
import SWRevealViewController
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import LoginWithAmazon
import MBProgressHUD
import DropDown
import UserNotifications
import Stripe
import CoreData
@main

/**
 The AppDelegate class is responsible for managing the application's lifecycle and handling various app delegate methods.

 - Author: GitHub Copilot
 - Version: 1.0
*/
class AppDelegate: UIResponder, UIApplicationDelegate,SWRevealViewControllerDelegate,UNUserNotificationCenterDelegate,MessagingDelegate
{
    var window: UIWindow?
    var appStoreversion : String = ""
    var apiIosVersion : String = ""
    //Orientation
    var deviceOrientation = UIInterfaceOrientationMask.portrait

    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }
    var objSWRevealViewController : SWRevealViewController?
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CSFCustomer")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError?
            {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    var arrOptions = [SegmentOptions]()
    var cardExpiryMsg: String = ""
    var cardExpiredMsg: String = ""
    var appNav : CustomNavigationVC!
    var totalMinutesForTimer : Int = 15
    var totalSecondsForTimer : Int = 15 * 60
    
    //Nand : New Feature Updates Code
    var isNeedToOpenMangeAddress : Bool = false
    
    // MARK: - Appdelegate methods
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        DropDown.startListeningToKeyboard()
        
        //configure Keyboard
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
        
        UINavigationBar.appearance().isTranslucent = false
        
        //badgecounter reset
        if  UserDefaults.standard.value(forKey: kLoggedIn) == nil
        {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        
        //firebase configuration
        FirebaseApp.configure()
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "com.csfcustomer"
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        //local notifications
        let notificationCenter = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound]
        notificationCenter.requestAuthorization(options: options) { (_, error) in
            // Enable or disable features based on authorization.
            if error != nil {
                print("Request authorization failed!")
            } else {
                print("Request authorization succeeded!")
            }
        }
        
        //stripe configuration
        StripeAPI.defaultPublishableKey = stripePublishKey
        return true
    }
    
    // MARK: - applicationWillEnterForeground
    /**
        Notifies the delegate that the application is about to enter the foreground.

        This method is called as part of the transition from the background to the active state. You can use this method to undo many of the changes made on entering the background.

        - Parameter application: The singleton application instance.
    */
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")
        appDelegate.openApp()
    }
    
    // MARK: - open URL
    /**
     Handles the opening of a URL by the application.
     
     This method is called when the application is asked to open a URL. It checks if the URL is a dynamic link and handles it accordingly. If the URL is not a dynamic link, it is passed to the Amazon authorization manager to handle.
     
     - Parameters:
        - application: The singleton application object.
        - url: The URL resource to open.
        - options: A dictionary of options to use when opening the URL.
     
     - Returns: A boolean value indicating whether the URL was successfully handled.
     */
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let isDynamicLink = DynamicLinks.dynamicLinks().shouldHandleDynamicLink(fromCustomSchemeURL: url) as? Bool,
           isDynamicLink {
            let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
            handleDynamicLink(dynamicLink)
        }
        _ =
            AMZNAuthorizationManager.handleOpen(url, sourceApplication: UIApplication.OpenURLOptionsKey.sourceApplication.rawValue)
        return  true
    }
    
    // MARK: - applicationWillTerminate
    /**
     This method is called when the application is about to terminate.
     It saves the managed object context and manages the tabs data before termination.
     
     - Parameter application: The singleton app object.
     */
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
        self.saveMangeTabsData()
    }
    
    // MARK: - checkIfAnyUserActive
    /**
     Checks if any user is active.
     
     - Returns: A boolean value indicating whether any user is active or not.
     */
    func checkIfAnyUserActive() -> Bool{
        var arrSwitchUserActiveList : [SwitchAccount] = [SwitchAccount]()
        arrSwitchUserActiveList.removeAll()
        let objManageCoreData = ManageCoreData()
        let result =  objManageCoreData.fetchData(entityName: kSwitchUserEntity)
        if result.count > 0
        {
            for data in result 
            {
                let objSwitchAccount = SwitchAccount()
                objSwitchAccount.isActive = data.value(forKey: SwitchUserInfo.isActive.rawValue) as? Bool ?? false
                
                if objSwitchAccount.isActive == true
                {
                    arrSwitchUserActiveList.append(objSwitchAccount)
                }
            }
        }
        
        if arrSwitchUserActiveList.count > 0 {
            print("Any one user is active")
            return true
        }else{
            print("No any user is active")
            return false
        }
    }
    
    // MARK: - handleDynamicLink
    /**
     Handles the dynamic link received from Firebase.

     - Parameter dynamicLink: The dynamic link received from Firebase.
     */
    func handleDynamicLink(_ dynamicLink: DynamicLink?) {
        guard let dynamicLink = dynamicLink else { return  }
        guard let deepLink = dynamicLink.url else { return }
        if let queryItems = URLComponents(url: deepLink, resolvingAgainstBaseURL: true)?.queryItems
        {
            if checkIfAnyUserActive() == false{
                if let invitedBy = queryItems.filter({(item) in item.name == "referralCode"}).first?.value
                {
                    //print("======invited by from handle===\(invitedBy)")
                    if (invitedBy != "")
                    {
                        UserDefaults.standard.setValue(invitedBy, forKey: "invitedby")
                        NotificationCenter.default.post(name: Notification.Name("dynamicLinkClicked"), object: nil)
                    }
                }
            }
        }
    }
    
    // MARK: - Core Data Saving support
    /**
     Saves the changes made in the persistent container's view context.
     
     This function checks if the view context has any changes. If there are changes, it tries to save the context. If an error occurs during the save operation, the function generates a crash log and terminates the application.
     
     - Note: This function should not be used in a shipping application, as it uses `fatalError()` to handle errors. It is intended for use during development.
     */
    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    // MARK: - FCM delegate methods for Push notification
    /**
     Handles the user's response to a notification received from the user notification center.
     
     - Parameters:
        - center: The user notification center that received the notification.
        - response: The user's response to the notification.
        - completionHandler: The completion handler to execute when the handling of the notification is complete.
     */   
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        
        if UserDefaults.standard.value(forKey: kLoggedIn) == nil
        {
            UIApplication.shared.applicationIconBadgeNumber = 0
            return
        }
        
        if objSWRevealViewController == nil {
            UIApplication.shared.windows.first?.rootViewController = appDelegate.setUpSWRevealViewController()
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }

        DispatchQueue.main.async { [self] in
            
            var notiTitle :String = ""
            var msgBody :String = ""
            //  print(response.notification.request.content.userInfo)
            let dict = response.notification.request.content.userInfo as? [String:Any] ?? [:]
            print("Notification data is \(dict)")
            if response.notification.request.trigger is UNPushNotificationTrigger
            {
                if let dictaps = dict["aps"] as? NSDictionary
                {
                    if let alert = dictaps["alert"] as? NSDictionary {
                        if let strTitle = alert["title"] as? String
                        {
                            notiTitle = strTitle
                        }
                        
                        if let strBody = alert["body"] as? String
                        {
                            msgBody = strBody
                        }
                    }
                }
                if let type = (dict as NSDictionary).value(forKey: "type") as? String
                {
                    switch type
                    {
                    case "1","3","4":
                        if let invoiceId = (dict as NSDictionary).value(forKey: "invoice_id") as? String,invoiceId != ""
                        {
                            openInvoiceDetail(invoiceID:invoiceId)
                        }
                        break
                    case "2","5":
                        if let urlLink = (dict as NSDictionary).value(forKey: "link") as? String,urlLink != ""
                        {
                            openWebview(fileName:urlLink,title:notiTitle)
                        }
                        break
                    case "6" :
                        openRewardVC()
                        break
                    case "7":
                        openWalletVC()
                        break
                    case "8":
                        DispatchQueue.main.async { [self] in
                            showAppNoticeAlert(title: (notiTitle == "") ? kAppName : notiTitle, message: msgBody)
                        }
                        break
                    case "9":
                        if let requestId = (dict as NSDictionary).value(forKey: "request_id") as? String
                        {
                            if isShowDriveThruOption {
                                openDriveThruRequestDetailScreen(requestId: requestId)
                            }
                        }
                        break
                    case "10":
                        openWalletVC()
                        break
                    default:
                        print("Notification")
                        break
                    }
                }
                else
                {
                    if let notificationVC = appDelegate.getViewController("NotificationVC", onStoryboard: "Home") as? NotificationVC {
                        if let objNavigationController  = topNavigation()
                        {
                            objNavigationController.pushViewController(notificationVC, animated: true)
                        }
                    }
                }
            }
            else
            {
                if let pdfFilePath = (dict as NSDictionary).value(forKey: "link") as? String,pdfFilePath !=  ""
                {
                    if let pdfViewerVC = appDelegate.getViewController("PDFViewerVC", onStoryboard: "Invoice") as? PDFViewerVC {
                        pdfViewerVC.pdfFile = pdfFilePath
                        pdfViewerVC.strTitle = (dict as NSDictionary).value(forKey: "title") as? String ?? ""
                        if let objNavigationController  = topNavigation()
                        {
                            objNavigationController.pushViewController(pdfViewerVC, animated: true)
                        }
                    }
                }
            }
        }
    }

    /**
        Notifies the delegate that a notification is about to be presented to the user when the app is in the foreground.

        - Parameters:
            - center: The notification center that triggered the notification.
            - notification: The notification object that is about to be presented.
            - completionHandler: The block to execute when you have finished processing the user's response. You must execute this block at some point after processing the notification.

        This method is called when a notification is about to be presented to the user when the app is in the foreground. It checks if the notification trigger is a push notification trigger and posts a notification with the name "NotificationRead1" to the default notification center. Finally, it calls the completion handler with the presentation options [.alert, .badge, .sound].

        - Note: This method is part of the `UIApplicationDelegate` protocol.
    */
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if notification.request.trigger is UNPushNotificationTrigger {
            NotificationCenter.default.post(name: Notification.Name("NotificationRead1"), object: nil)
        }
        completionHandler([.alert, .badge, .sound])
    }

    // MARK: - Messaging Delegate
    /**
     Called when a new FCM registration token is assigned to the device.
     
     - Parameters:
        - messaging: The messaging instance that received the registration token.
        - fcmToken: The new FCM registration token.
     */
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?)
    {
         print("fcmtoken \(String(describing: fcmToken))")
        UserDefaults.standard.set(fcmToken, forKey: kDeviceToken)
    }

    /**
        This method is called when the Firebase Cloud Messaging (FCM) token is refreshed.
        
        - Parameters:
            - messaging: The messaging object.
            - fcmToken: The new FCM token.
    */
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("\n\n\n\n\n ==== REFRESH FCM Token:  ",fcmToken)
        UserDefaults.standard.set(fcmToken, forKey: kDeviceToken)
    }
    
    // MARK: - openInvoiceDetail
    /// Opens the invoice detail view controller with the given invoice ID.
    ///
    /// - Parameter invoiceID: The ID of the invoice to be displayed.
    func openInvoiceDetail(invoiceID: String) {
        if let invoiceDetailVC = appDelegate.getViewController("InvoiceDetailVC", onStoryboard: "Invoice") as? InvoiceDetailVC {
            invoiceDetailVC.invoiceID = invoiceID
            invoiceDetailVC.isFrom = "PushNotification"
            if let objNavigationController = topNavigation() {
                objNavigationController.pushViewController(invoiceDetailVC, animated: true)
            }
        }
    }
    
    // MARK: - openWebview
    /// Opens a webview with a PDF file.
    ///
    /// - Parameters:
    ///   - fileName: The name of the PDF file.
    ///   - title: The title of the webview.
    func openWebview(fileName: String, title: String) {
        if let pdfViewerVC = appDelegate.getViewController("PDFViewerVC", onStoryboard: "Invoice") as? PDFViewerVC {
            pdfViewerVC.pdfFile = fileName
            pdfViewerVC.strTitle = title
            if let objNavigationController = topNavigation() {
                objNavigationController.pushViewController(pdfViewerVC, animated: true)
            }
        }
    }
    
    // MARK: - openRewardVC
    /**
     Opens the RewardsVC view controller and pushes it onto the navigation stack.
     
     This function checks if the RewardsVC view controller can be instantiated from the "Referral" storyboard. If successful, it pushes the view controller onto the top navigation controller's stack, animating the transition.
     */
    func openRewardVC() {
        if let rewardVC = appDelegate.getViewController("RewardsVC", onStoryboard: "Referral") as? RewardsVC {
            if let objNavigationController = topNavigation() {
                objNavigationController.pushViewController(rewardVC, animated: true)
            }
        }
    }
    
    // MARK: - openWalletVC
    /**
     Opens the WalletVC view controller if it exists in the navigation stack, otherwise pushes it onto the stack.
     */
    func openWalletVC() {
        if let walletVC = appDelegate.getViewController("WalletVC", onStoryboard: "Referral") as? WalletVC {
            if let objNavigationController = topNavigation() {
                if objNavigationController.viewControllers.count > 0 {
                    if objNavigationController.viewControllers.last!.isKind(of: WalletVC.self) {
                        let objWalletVC: WalletVC = (objNavigationController.viewControllers.last as? WalletVC)!
                        objWalletVC.viewWillAppear(true)
                    } else {
                        objNavigationController.pushViewController(walletVC, animated: true)
                    }
                } else {
                    objNavigationController.pushViewController(walletVC, animated: true)
                }
            }
        }
    }

    // MARK: - openDriveThruRequestDetailScreen
    /**
     Opens the DriveThru request detail screen.
     */
    func openDriveThruRequestDetailScreen(requestId: String) {
        if let requestDetailVC = appDelegate.getViewController("RequestDetailVC", onStoryboard: "DriveThru") as? RequestDetailVC {
            
            requestDetailVC.isFromHistoryPage = false
            requestDetailVC.requestId = requestId
            
            if let objNavigationController = topNavigation() {
                
                if objNavigationController.viewControllers.count > 0 {
                    if objNavigationController.viewControllers.last!.isKind(of: RequestDetailVC.self) {
                        let objRequestDetailVC: RequestDetailVC = (objNavigationController.viewControllers.last as? RequestDetailVC)!
                        
                        objRequestDetailVC.requestId = requestId
                        objRequestDetailVC.viewDidLoad()
                    } else {
                        objNavigationController.pushViewController(requestDetailVC, animated: true)
                    }
                } else {
                    objNavigationController.pushViewController(requestDetailVC, animated: true)
                }
            }
        }
    }
    
    // MARK: - saveMangeTabsData
    /**
     Saves the managed tabs data to UserDefaults.
     
     This function encodes the `arrOptions` array using the `SegmentOptions` type and saves it to UserDefaults.
     */
    func saveMangeTabsData(){
        UserDefaults.standard.encode(for:appDelegate.arrOptions, using: String(describing: SegmentOptions.self))
    }
    
    //Orientation
    // this method will rotate the controller and every time once we switch our controller it will be called.
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return deviceOrientation
    }
}
extension AppDelegate
{
    // MARK: - showLoader
    /**
     Shows a loader on the main window's root view controller.
     
     This function adds a loader to the main window's root view controller, indicating that a task is in progress. The loader is displayed using the MBProgressHUD library.
     */
    func showLoader()
    {
        DispatchQueue.main.async {
            if UIApplication.shared.windows.first(where: { $0.isKeyWindow }) != nil
            {
                let loading = MBProgressHUD.showAdded(to: (UIApplication.shared.windows.first?.rootViewController?.view)!, animated: true)
                loading.mode = MBProgressHUDMode.indeterminate
                loading.bezelView.color = Colors.clearColor
            }
        }
    }
    
    // MARK: - hideLoader
    /**
     Hides the loader view.

     This function hides the loader view by using the MBProgressHUD library. It checks if the key window is available and then hides the loader from the root view controller's view.

     - Note: This function should be called after the loader view has been shown.
     */
    func hideLoader()
    {
        DispatchQueue.main.async {
            if UIApplication.shared.windows.first(where: { $0.isKeyWindow }) != nil
            {
                MBProgressHUD.hide(for: (UIApplication.shared.windows.first?.rootViewController?.view)!, animated: true)
            }
        }
    }
    
    // MARK: - heightForView
    /**
     Calculates the height required to display a given text with a specified font and width.

     - Parameters:
        - text: The text to be displayed.
        - font: The font to be used for displaying the text.
        - width: The width within which the text should be displayed.

     - Returns: The height required to display the text within the given width.

     - Note: This method creates a temporary `UILabel` to calculate the height required for the given text. The label is configured with the provided font, width, and text, and then its size is adjusted to fit the text. The resulting height of the label is returned as the calculated height.

     - Important: The `text` parameter should not be an empty string, as it will result in a height of zero.
     */
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    
    // MARK: - showToast
    /**
     Shows a toast message on the screen.

     - Parameters:
         - message: The message to be displayed in the toast.
         - bottomValue: The distance from the bottom of the screen where the toast should be displayed.
         - height: The height of the toast view. Default value is 60.
         - alignmentType: The alignment type of the toast label. Default value is 0 (center alignment).
         - isForSuccess: A boolean value indicating whether the toast is for a success message. Default value is false.
         - duration: The duration for which the toast should be displayed. Default value is 1 second.
         - position: The position of the toast on the screen. Default value is ToastPosition.bottom.

     - Note:
         - If the `message` is empty, the toast will not be displayed.
         - The `height` of the toast view is calculated based on the length of the `message` and the font size.
         - The `alignmentType` can be 0 (center alignment) or 1 (left alignment).
         - The `isForSuccess` parameter determines the background color of the toast view.
         - The `duration` parameter controls the fade-out animation duration of the toast view.
         - The `position` parameter can be ToastPosition.top or ToastPosition.bottom.

     - Requires:
         - The `message` parameter must not be nil or empty.
         - The `bottomValue` parameter must be a non-negative integer.
         - The `height` parameter must be a positive value.
         - The `alignmentType` parameter must be either 0 or 1.
         - The `duration` parameter must be a positive value.
         - The `position` parameter must be either ToastPosition.top or ToastPosition.bottom.

     - SeeAlso: `ToastPosition`
     */
    func showToast(message : String,bottomValue:Int, height : CGFloat = 60, alignmentType : Int = 0, isForSuccess : Bool = false, duration : Double = 1, position : Int = ToastPosition.bottom.rawValue) {
        if message.count == 0 { return }
        var duration : Double = 1
        let viewWidth : CGFloat = 250
        var viewHeight : CGFloat = 0
        let fontSize : CGFloat = 15.0
        
        DispatchQueue.main.async {
            if UIApplication.shared.windows.first(where: { $0.isKeyWindow }) != nil{

                print("Msg is \(message)")
                
                let font = UIFont(name: "OpenSans-Regular", size: fontSize)
                let height = self.heightForView(text: message.trimmingCharacters(in: .whitespacesAndNewlines), font: font!, width: viewWidth)
                print("lbl height is \(height)")
                
                if let window = UIApplication.shared.windows.first {
                    
                    var yValue = window.frame.size.height - CGFloat(bottomValue)
                    if position == ToastPosition.top.rawValue {
                        yValue = CGFloat(getSafeAreaTopValue())
                    }
                    viewHeight =  height
                    print("viewHeight is \(viewHeight)")

                    let backView = UIView(frame: CGRect(x: window.frame.size.width/2 - (viewWidth / 2) - 20, y:yValue, width: viewWidth + 40, height: viewHeight + 40))
                    
                    if isForSuccess {
                        backView.backgroundColor = Colors.theme_green_color
                    }else {
                        backView.backgroundColor = Colors.theme_red_color
                    }
  
                    let toastLabel = UILabel(frame: CGRect(x: 20, y:10, width: backView.frame.size.width - 40, height: backView.frame.size.height - 20))

                    configureLabel(label: toastLabel, title: message.trimmingCharacters(in: .whitespacesAndNewlines), bgColor: UIColor.clear , textColor: UIColor.white
                                   , font: "OpenSans-Regular", fontSize: Float(fontSize))
                    
                    if alignmentType == 0 {
                        toastLabel.textAlignment = .center
                    }else {
                        toastLabel.textAlignment = .left
                    }

                    backView.alpha = 1.0
                    backView.layer.cornerRadius = 15
                    backView.clipsToBounds  =  true
                    toastLabel.lineBreakMode = .byWordWrapping
                    toastLabel.numberOfLines = 0
                    
                    backView.didMoveToSuperview()
                    backView.addSubview(toastLabel)
                    window.addSubview(backView)
                    if message == Messsages.msg_invoice_exists
                    {
                        duration = 6
                    }
                    UIView.animate(withDuration: duration, delay: 3.0, options: .curveEaseOut, animations: {
                        backView.alpha = 0.0
                    }, completion: {(isCompleted) in
                        backView.removeFromSuperview()
                    })
                }
            }
        }
    }

    // MARK: - getViewController
    /**
     Returns a view controller with the specified name from the given storyboard.

     - Parameters:
        - name: The identifier of the view controller.
        - storyboardName: The name of the storyboard.

     - Returns: The view controller with the specified name.
     */
    func getViewController(_ name: String, onStoryboard storyboardName: String) -> UIViewController
    {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: name)
    }
    
    // MARK: - convertToDictionary
    /**
     Converts a JSON string to a dictionary.

     - Parameter text: The JSON string to be converted.
     - Returns: A dictionary representation of the JSON string, or `nil` if the conversion fails.
     */
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // MARK: - openApp
    /**
     Opens the app and performs necessary setup tasks.
     */
    func openApp() {
        enableAppAppearanceMode()
        
        // Check for new version and ask for update after a delay of 5 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [self] in
            checkMaintenance()
        }
    }
    
    // MARK: - checkMaintenance
    /**
        Checks the maintenance status and performs necessary actions based on the status.
    */
    func checkMaintenance(){
        appDelegate.callAPIToCheckMaintenance { [self] (status, versionNumber) in
            if status == false
            {
                do {
                    apiIosVersion = versionNumber
                    try checkApiVersionWithAppVersion()
                } catch  {
                    print(error)
                }
            }
        }
    }
    
    // MARK: - callAPIToCheckMaintenance
    /**
     Calls an API to check the maintenance status of the application.

     - Parameters:
         - newStatus: A closure that takes a tuple of type `(Bool, String)` as a parameter. The tuple represents the maintenance status and the latest version number of the application.

     - Note: This function makes a POST request to the specified API endpoint to retrieve the maintenance data. It then checks the status and message received from the API response. If the status is true and the maintenance flag is set to 1, it opens the maintenance screen and calls the `newStatus` closure with `(true, versionNumber)`. Otherwise, it hides the maintenance screen and calls the `newStatus` closure with `(false, versionNumber)`.

     - Important: The `URLManager.shared.URLCall` method is used to make the API call.

     - Requires: The `URLManager` class and the `APICall` enum must be implemented and accessible.

     */
    func callAPIToCheckMaintenance(newStatus:@escaping((Bool,String)) -> Void)
    {
        URLManager.shared.URLCall(method: .post, parameters: [:], header: false, url: APICall.maintenanceData, showLoader: false) { [self] (resultDict, status, message) in
            //print("maintanance api result \(resultDict)")
            var versionNumber : String = ""
            if status == true
            {
                if let number = (resultDict as NSDictionary).value(forKey: "SKYBOX_IOS_LATEST_VERSION") as? String
                {
                    versionNumber = number
                }
                
                if let isMainetenance = (resultDict as NSDictionary).value(forKey: "SKYBOX_IOS_MAINTENANCE") as? Int,isMainetenance == 1
                {
                    self.openMaintenanceScreen(Message:(resultDict as NSDictionary).value(forKey: "SKYBOX_MAINTENANCE_MSG") as? String ?? "")
                    newStatus((true,versionNumber))
                }
                else
                {
                    hideManintanence()
                    newStatus((false,versionNumber))
                }
            }
            else
            {
                hideManintanence()
                newStatus((false,versionNumber))
            }
        }
    }
    
    // MARK: - openMaintenanceScreen
    /**
     Opens the maintenance screen with a given message.
     
     - Parameters:
        - Message: The message to be displayed on the maintenance screen.
     */
    func openMaintenanceScreen(Message:String)
    {
        if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
            if let  vc = rootVC.presentedViewController{
                if vc.isKind(of: MaintenanceVC.self){
                    return
                }
            }
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let alertVC = storyboard.instantiateViewController(withIdentifier: "MaintenanceVC") as? MaintenanceVC {
            alertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            alertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            alertVC.message = Message
            
            if UserDefaults.standard.value(forKey: kLoggedIn) == nil
            {
                if appDelegate.appNav != nil {
                    appDelegate.appNav.present(alertVC, animated: true, completion: nil)
                }else{
                    if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                        rootVC.present(alertVC, animated: true, completion: nil)
                    }else{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            
                            if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                                rootVC.presentedViewController?.present(alertVC, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }else {
                if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                    rootVC.present(alertVC, animated: true, completion: nil)
                }else{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                            rootVC.presentedViewController?.present(alertVC, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - hideManintanence
    /**
     Hides the maintenance view controller if the user is not logged in.
     
     This function checks if the user is logged in by checking the value of the `kLoggedIn` key in `UserDefaults`. If the value is `nil`, it proceeds to check if the maintenance view controller is currently presented and dismisses it if it is.
     
     Additionally, it checks if the maintenance view controller is presented as the root view controller or as a presented view controller and dismisses it if it is.
     */
    func hideManintanence() {
        
        if UserDefaults.standard.value(forKey: kLoggedIn) == nil
        {
            if appDelegate.appNav != nil {
                if let vc = appDelegate.appNav.presentedViewController {
                    if vc.isKind(of: MaintenanceVC.self)
                    {
                        vc.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
        
        if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
            if let  vc = rootVC.presentedViewController {
                if vc.isKind(of: MaintenanceVC.self){
                    vc.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - checkApiVersionWithAppVersion
    /**
     Checks the API version with the app version and performs necessary actions based on the comparison result.
     
     - Throws: `VersionError.invalidBundleInfo` if the bundle information is invalid, and `VersionError.invalidResponse` if the API response is invalid.
     */
    func checkApiVersionWithAppVersion() throws {
        guard let info = Bundle.main.infoDictionary,
              let currentVersion = info["CFBundleShortVersionString"] as? String else {
            throw VersionError.invalidBundleInfo
        }
        
        print("currentVersion is \(currentVersion)")
        if apiIosVersion.compare(currentVersion, options: .numeric) != .orderedDescending {
            UserDefaults.standard.setValue(0, forKey: kVersionPopupCounter)
            
            //print("Not show update dialog")
            hidePopupUpdateDialogue()
        } else {
            DispatchQueue.main.async { [self] in
                
                print("apiIosVersion \(apiIosVersion)")
                print("show :::: Show update dialog")
                
                if isShowVersionPopupEvery2Days() {
                    self.popupUpdateDialogue()
                }
            }
        }
        throw VersionError.invalidResponse
    }
    
    // MARK: - popupUpdateDialogue
    /**
     Displays a popup update dialogue to the user.

     This function presents a view controller that shows a popup with an update dialogue to the user. The popup is presented over the current context with a cross dissolve transition style. The background color of the popup is set to a black color with an alpha component of 0.6.

     The `appStoreVersion` property of the `NewVersionPopupVC` view controller is set to the value of `appStoreversion`, and the `apiIOSVersion` property is set to the value of `apiIosVersion`.

     If the user is not logged in, the popup is presented using the `appNav` navigation controller if it is not nil. Otherwise, it is presented using the root view controller of the key window. If the root view controller is not available, the popup is presented after a delay of 2 seconds.

     If the user is logged in, the popup is presented using the root view controller of the key window. If the root view controller is not available, the popup is presented after a delay of 2 seconds.

     - Note: This function assumes that the `NewVersionPopupVC` view controller is defined in the "Main" storyboard with the identifier "NewVersionPopupVC".

     - Important: The `appStoreversion` and `apiIosVersion` properties must be set before calling this function.
     */
    func popupUpdateDialogue()
    {
        //new code
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let alertVC = storyboard.instantiateViewController(withIdentifier: "NewVersionPopupVC") as? NewVersionPopupVC {
            alertVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            alertVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            alertVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black") //UIColor.black.withAlphaComponent(0.6)
            alertVC.appStoreVersion = appStoreversion
            //print("apiIosVersion on popupUpdateDialog \(apiIosVersion)")
            alertVC.apiIOSVersion = apiIosVersion
            if UserDefaults.standard.value(forKey: kLoggedIn) == nil
            {
                
                if appDelegate.appNav != nil {
                    appDelegate.appNav.present(alertVC, animated: true, completion: nil)
                }else{
                    
                    if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                        rootVC.present(alertVC, animated: true, completion: nil)
                    }else{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                                rootVC.presentedViewController?.present(alertVC, animated: true, completion: nil)
                                
                            }
                        }
                    }
                }
            }else{
                
                if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                    rootVC.present(alertVC, animated: true, completion: nil)
                }else{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                            rootVC.presentedViewController?.present(alertVC, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - hidePopupUpdateDialogue
    /**
     Hides the popup update dialogue if it is currently being displayed.
     
     This function checks if the user is logged in and dismisses the `NewVersionPopupVC` view controller if it is being presented. It also dismisses the `NewVersionPopupVC` if it is being presented by the root view controller.
     */
    func hidePopupUpdateDialogue(){
        
        if UserDefaults.standard.value(forKey: kLoggedIn) == nil
        {
            if appDelegate.appNav != nil {
                if let vc = appDelegate.appNav.presentedViewController {
                    if vc.isKind(of: NewVersionPopupVC.self)
                    {
                        vc.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
        
        if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
            if let  vc = rootVC.presentedViewController {
                if vc.isKind(of: NewVersionPopupVC.self){
                    vc.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - setUpSWRevealViewController
    /**
     Sets up the SWRevealViewController and returns it.
     
     - Returns: The configured SWRevealViewController.
     */
    func setUpSWRevealViewController() -> SWRevealViewController
    {
        if let objMenuVC = self.getViewController("SideMenuVC", onStoryboard: "Home") as? SideMenuVC {
            let navMenu = UINavigationController(rootViewController: objMenuVC)
            
            navMenu.navigationBar.isHidden = true
            
            if let objTabbarVC = self.getViewController("BubbleTabBarController", onStoryboard: "Home") as? BubbleTabBarController {
                //set front and rearviewcontroller
                objSWRevealViewController = SWRevealViewController(rearViewController:navMenu, frontViewController: objTabbarVC)
            }
        }
        objSWRevealViewController!.rearViewRevealWidth = 280
        objSWRevealViewController!.delegate = self
        objSWRevealViewController?.tapGestureRecognizer()
        return objSWRevealViewController!
    }
    
    // MARK: - topNavigation
    /**
     Returns the top navigation controller in the application.
     
     - Parameter viewController: The view controller from which to start the search for the top navigation controller. If not provided, the root view controller of the key window will be used.
     - Returns: The top navigation controller found in the application.
     */
    func topNavigation(_ viewController: UIViewController? = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController) -> UINavigationController? {
        
        if let nav = viewController as? UINavigationController {
            return nav
        }
        if objSWRevealViewController != nil{
            if let tab = self.objSWRevealViewController!.frontViewController as? UITabBarController {
                if let selected = tab.selectedViewController as? UINavigationController
                {
                    return selected
                }
            }
        }
        return viewController?.navigationController
    }

    // MARK: - getCoreDataDBPath
    /// Retrieves the path of the Core Data database file.
    func getCoreDataDBPath() {
        let path = FileManager
            .default
            .urls(for: .applicationSupportDirectory, in: .userDomainMask)
            .last?
            .absoluteString
            .replacingOccurrences(of: "file://", with: "")
            .removingPercentEncoding
        
        print("Core Data DB Path :: \(path ?? "Not found")")
    }
    
    // MARK: - callAPIForValidatePromotionalCode
    /**
     Calls the API to validate a promotional code.
     
     - Parameters:
        - promotionalCode: The promotional code to be validated.
        - fromVC: The view controller from which the API is called.
        - completion: A closure that is called when the API call is completed. It returns the result dictionary, status, and message.
     
     - Note: This function shows a HUD (Heads-Up Display) on the `fromVC` view while the API call is in progress, and hides it once the call is completed.
     */
    func callAPIForValidatePromotionalCode(promotionalCode: String, fromVC: UIViewController, completion: @escaping (_ resultDict: [String:Any], _ status: Bool, _ message: String) -> Void) {
        print("Validate promotional Code API Calling...")

        var params: [String:Any] = [:]
        params["promotional_code"] = promotionalCode
        
        fromVC.showHUDOnView(view: fromVC.view)
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.checkPromotionalCode, showLoader: false) { (resultDict, status, message) in
            fromVC.hideHUDFromView(view: fromVC.view)
            completion(resultDict, status, message)
        }
    }
}

extension AppDelegate {
    // MARK: - showAppNoticeAlert
    /**
     Shows an alert with the given title and message.

     - Parameters:showAppNoticeAlert
        - title: The title of the alert.
        - message: The message to be displayed in the alert.
     */
    func showAppNoticeAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            alertController.dismiss(animated: true, completion: nil)
        }
        action1.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alertController.addAction(action1)
        if UIApplication.shared.windows.first?.rootViewController != nil
        {
            DispatchQueue.main.async {
                UIApplication.shared.windows.first?.rootViewController?.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - isShowVersionPopupEvery2Days
    /**
     Checks if the version popup should be shown every 2 days.
     
     - Returns: A boolean value indicating whether the version popup should be shown (`true`) or not (`false`).
     */
    func isShowVersionPopupEvery2Days() -> Bool {
        var isShowVersionPopup : Bool = false
        
        var appVersionPopupTimeStamp = UserDefaults.standard.double(forKey: kAppVersionPopupTimeStamp)
        
        if Double(appVersionPopupTimeStamp) == 0 {
            appVersionPopupTimeStamp = Date().timeIntervalSince1970
            return true
        }
        
        let diffInSeconds = Int(Date().timeIntervalSince1970 - appVersionPopupTimeStamp)
        // Total time difference in seconds
        
        print("appVersionPopupTimeStamp : \(appVersionPopupTimeStamp)")
        print("currentTimeStamp : \(Date().timeIntervalSince1970)")
        
        print("diffInSeconds is \(diffInSeconds)")
        let minDiff = (diffInSeconds / 60)
        print("diff in min is \(minDiff)")
        
        let hoursDiff = (minDiff / 60) // hours = diff in seconds / 60 sec per min / 60 min per hour
        print("diff in hours is \(hoursDiff)")

        if hoursDiff >= 48 {
            //Show Version Popup
            isShowVersionPopup = true
        }else{
            //Don't Show Version Popup
            isShowVersionPopup = false
        }
        return isShowVersionPopup
    }
}
