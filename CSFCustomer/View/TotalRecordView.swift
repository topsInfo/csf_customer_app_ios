//
//  TotalRecordView.swift
//  CSFCustomer
//
//  Created by Tops on 06/12/21.
//

import UIKit

class TotalRecordView: UIView {
    @IBOutlet weak var lblTotalRecordViewTitle : UILabel!
    @IBOutlet weak var lblTotalRecordViewValue : UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder : NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    /**
     Initializes the `TotalRecordView` by performing common setup tasks.
     */
    func commonInit(){
        if let viewFromXib = Bundle.main.loadNibNamed("TotalRecordView", owner: self, options: nil)![0] as? UIView {
            viewFromXib.frame = self.bounds
            addSubview(viewFromXib)
        }        
    }
}
