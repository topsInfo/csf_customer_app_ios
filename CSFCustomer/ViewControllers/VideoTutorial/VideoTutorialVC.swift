//
//  VideoTutorialVC.swift
//  CSFCustomer
//
//  Created by iMac on 17/11/22.
//

import UIKit
import SwiftyJSON

/**
 A custom table view cell used to display video tutorial information.
 
 Use this cell to present video tutorial details in a table view.
 */
class VideoTutorialListCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var viewThumb: UIView!
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgPalyButton: UIImageView!
    
    // MARK: - Viewcontroller life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 10, isTopLeftTopRight: true, isBottomLeftBottomRight: true)
        
        imgThumb.layer.cornerRadius = 10
        imgThumb.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        imgThumb.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

/**
 A view controller that displays video tutorials.
 */
class VideoTutorialVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tblVideoTutorialList : UITableView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var viewSearchBar: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnCloseSearch: UIButton!
    
    // MARK: - Global Variables
    var refreshControl = UIRefreshControl()
    var isSearchOptionOpen : Bool = false
    
    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0
    var arrVideoTutorialList : [VideoTutorialData] = [VideoTutorialData]()
    var arrVideoTutorialListTemp : [VideoTutorialData] = [VideoTutorialData]()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        UserDefaults.standard.setValue(true, forKey: kIsVideoTutorialShown)
        appDelegate.deviceOrientation = .portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    } 
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods    
    func configureControls()
    {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblVideoTutorialList.addSubview(refreshControl)
        
        tblVideoTutorialList.delegate = self
        tblVideoTutorialList.dataSource = self
        tblVideoTutorialList.estimatedRowHeight = 252.0
        hideSearchControls()
        self.txtSearch.placeholder = "Search"
        self.getVideoTutorialList(isShowLoader: true)
    }
    
    /**
     Hides the search controls in the view controller.
     */
    func hideSearchControls(){
        viewSearchBar.isHidden = true
        btnCloseSearch.isHidden = true
        btnSearch.isHidden = false
    }
    
    /**
     Refreshes the view by hiding search controls and fetching the video tutorial list.
     */
    @objc func refresh() {
        hideSearchControls()
        self.getVideoTutorialList(isShowLoader: false)
    }
    
    // MARK: - clearFilter
    /// Clears the filter by applying an empty search string.
        func clearFilter(){
            applyFilter(search: "")
        }
    
    // MARK: - applyFilter
    /**
     Applies a filter to the video tutorial list based on the given search string.
     
     - Parameters:
        - search: The search string to filter the video tutorial list.
     */
    func applyFilter(search: String) {
        var arrSearchedVideoTutorialList: [VideoTutorialData] = [VideoTutorialData]()
        
        if !search.isEmpty {
            arrSearchedVideoTutorialList = self.arrVideoTutorialListTemp.filter { ($0.title.uppercased().contains(search.trimmingCharacters(in: .whitespacesAndNewlines).uppercased())) || ($0.descriptionField.uppercased().contains(search.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()))}
            
            self.arrVideoTutorialList.removeAll()
            self.arrVideoTutorialList = arrSearchedVideoTutorialList
        } else {
            self.arrVideoTutorialList = self.arrVideoTutorialListTemp
        }
        
        self.reloadData()
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSearchClicked(_ sender: UIButton) {
        self.viewSearchBar.isHidden = false
        self.btnCloseSearch.isHidden = false
        self.btnSearch.isHidden = true
        self.txtSearch.text = ""
        txtSearch.becomeFirstResponder()
    }
    
    @IBAction func btnCloseSearchClicked(_ sender: UIButton) {
        txtSearch.resignFirstResponder()
        hideSearchControls()
        clearFilter()
    }
}

/**
 The `VideoTutorialVC` class conforms to the `UITableViewDelegate` and `UITableViewDataSource` protocols.
 It is responsible for handling the table view delegate and data source methods for the video tutorial view controller.
 */
extension VideoTutorialVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTutorialListCell", for: indexPath) as? VideoTutorialListCell {
            if self.arrVideoTutorialList.count > 0 {
                let objVideoTutorialData : VideoTutorialData = self.arrVideoTutorialList[indexPath.row]
                cell.lblTitle.text = objVideoTutorialData.title ?? ""
                cell.lblDesc.text = objVideoTutorialData.descriptionField ?? ""
                
                if let imgUrl : String = objVideoTutorialData.thumb {
                    cell.imgThumb.setImage(imgUrl, placeHolder: UIImage(named: "icVideoTutorialThumb"), isShowLoader: true)
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrVideoTutorialList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let videoPlayerVC = appDelegate.getViewController("VideoPlayerVC", onStoryboard: "Home") as? VideoPlayerVC {
            videoPlayerVC.objVideoData = self.arrVideoTutorialList[indexPath.row]
            self.navigationController?.pushViewController(videoPlayerVC, animated: false)
        }
    }
}

extension VideoTutorialVC {
    /**
     This function retrieves the video tutorial list from the server.
     
     - Parameters:
         - isShowLoader: A boolean value indicating whether to show a loader while fetching the data.
     
     - Note: This function clears the existing video tutorial list and fetches a new list from the server based on the user's ID. The fetched list is then stored in the `arrVideoTutorialList` and `arrVideoTutorialListTemp` arrays. The total number of records in the list is stored in the `totalRecordsCount` property. Finally, the `reloadData()` method is called to update the UI.
     */
    func getVideoTutorialList(isShowLoader : Bool)
    {
        self.view.endEditing(true)
        self.arrVideoTutorialListTemp.removeAll()
        self.arrVideoTutorialList.removeAll()
        
        let objUserInfo = getUserInfo()
        
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.videoTutorialList, showLoader: isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            if status == true
            {
                print("*** Tutorail list dict is \(resultDict) ****")
                let objVideotutorialList = VideoTutorialList.init(fromJson: JSON(resultDict))
                self.arrVideoTutorialList = objVideotutorialList.list
                self.arrVideoTutorialListTemp = objVideotutorialList.list
                
                self.totalRecordsCount = self.arrVideoTutorialList.count
                self.reloadData()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    /**
     Reloads the data in the table view and updates the visibility of the "No Record Found" label based on the count of video tutorial items.
     */
    func reloadData(){
        DispatchQueue.main.async { [self] in
            if self.arrVideoTutorialList.count > 0 {
                lblNoRecordFound.isHidden = true
            }else  {
                lblNoRecordFound.isHidden = false
            }
            self.tblVideoTutorialList.reloadData()
        }
    }
}

/**
 This extension conforms to the `UITextFieldDelegate` protocol and provides additional functionality for the `VideoTutorialVC` view controller related to text fields.
 
 The `UITextFieldDelegate` protocol defines methods that allow you to manage and customize the behavior of text fields. By conforming to this protocol, the `VideoTutorialVC` view controller can handle text field events and implement custom behavior.
 
 */
extension VideoTutorialVC : UITextFieldDelegate {
    // MARK: - Textfield delegate methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        applyFilter(search: textField.text ?? "")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        applyFilter(search: textField.text ?? "")
    }
}

extension VideoTutorialVC {
//    override var shouldAutorotate: Bool {
//        return false
//    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }

    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.portrait
    }
}
