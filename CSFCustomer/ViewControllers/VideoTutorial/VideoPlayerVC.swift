//
//  VideoPlayerVC.swift
//  CSFCustomer
//
//  Created by iMac on 17/11/22.
//

import UIKit
import WebKit
class VideoPlayerVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var videoPlayerView: UIView!
    
    // MARK: - Global Variables
    var videoPlayer: WKWebView!
    var objVideoData : VideoTutorialData!
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Set the background color of the video player view
        videoPlayerView.backgroundColor = Colors.theme_bgColor
        
        // Set the device orientation to landscape right
        appDelegate.deviceOrientation = .landscapeRight
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Create a WKWebView configuration for inline media playback
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.allowsInlineMediaPlayback = true
        webConfiguration.mediaTypesRequiringUserActionForPlayback = []
        
        // Create a WKWebView and add it to the video player view
        videoPlayer = WKWebView(frame: CGRect(x: 0, y: 0, width: videoPlayerView.frame.size.width, height: videoPlayerView.frame.size.height), configuration: webConfiguration)
        videoPlayer.backgroundColor = Colors.theme_bgColor
        self.videoPlayerView.addSubview(videoPlayer)
        
        // Load the video from the given URL into the WKWebView
        if let videoURL: URL = URL(string: objVideoData.url ?? "") {
            let request: URLRequest = URLRequest(url: videoURL)
            videoPlayer.load(request)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        appDelegate.deviceOrientation = .portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: false)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeRight
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.landscapeRight
    }
}