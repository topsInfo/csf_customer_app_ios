//
//  AddAddressVC.swift
//  CSFCustomer
//
//  Created by Tops on 11/01/21.
//

import UIKit
import DropDown
import SwiftyJSON
class AddressVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var deliveryOptionView : UIView!
    @IBOutlet weak var addressTitleView : UIView!
    @IBOutlet weak var otherTitleView : UIView!
    @IBOutlet weak var cityView : UIView!
    @IBOutlet weak var txtDeliveyOption : UITextField!
    @IBOutlet weak var txtAddressTitle : UITextField!
    @IBOutlet weak var txtOtherTitle : UITextField!
    @IBOutlet weak var txtAddress1 : UITextView!
    @IBOutlet weak var txtAddress2 : UITextView!
    @IBOutlet weak var txtCity : UITextField!
    //stackview
    @IBOutlet weak var addTitleStackView : UIStackView!
    @IBOutlet weak var otherTitleStackView : UIStackView!
    @IBOutlet weak var address1StackView : UIStackView!
    @IBOutlet weak var address2StackView : UIStackView!
    @IBOutlet weak var CityStackView : UIStackView!
    @IBOutlet weak var weekendPreferenceStackView : UIStackView!
    //buttons
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var btnSubmit : UIButton!
    @IBOutlet weak var btnDefaultAddress : UIButton!
    @IBOutlet weak var defaultAddressView : UIView!
    //image
    @IBOutlet weak var imgDefaultAdd : UIImageView!
    @IBOutlet weak var imgTerms : UIImageView!
    @IBOutlet weak var topBarView : UIView!
    
    @IBOutlet weak var btnWeekdays : UIButton!
    @IBOutlet weak var btnSaturday : UIButton!
    @IBOutlet weak var btnSunday : UIButton!

    @IBOutlet weak var imgWeekdayCheckBox : UIImageView!
    @IBOutlet weak var imgSaturdayCheckBox : UIImageView!
    @IBOutlet weak var imgSundayCheckBox : UIImageView!
    
    @IBOutlet weak var scrlViewAddress : UIScrollView!
    
    // MARK: - Global Variable
    var arrViews : [UIView] = [UIView]()
    var arrTextFields : [UITextField] = [UITextField]()
    var arrPlaceHolder : [String] = [String]()
    var arrTextViewPlaceHolder  : [String] = [String]()
    var arrTextViews : [UITextView] = [UITextView]()
    var arrStackViews :  [UIStackView] = [UIStackView]()
    //data
    var arrAddressTitle :[AddressTitle] = [AddressTitle]()
    var arrDeliveryOptions : [DeliveryPickupOffice] = [DeliveryPickupOffice]()
    var arrCity : [DeliveryArea] = [DeliveryArea]()
    var arrFilterCity : [DeliveryArea] = [DeliveryArea]()
    //other
    var addTitle  = ""
    var isDefault : Int = 0
    var isAgreeTermsOfUse : Int = 0
    
    var isWeekdays : String = "0"
    var isSunday : String = "0"
    var isSaturday : String = "0"
    var isDeliveryPrefSelected : Bool = false
    
    var dropDown = DropDown()
    var deliveryDropDown = DropDown()
    var cityDropDown = DropDown()
    var selectedAddressTitleID :Int = 1
    var selectedDeliveryOptionID : String = ""
    var selectedCityID : String = ""
    var userID : String = ""
    var type : String = ""
    var addressID : String = ""
    var objMyAddress = Address()
    var objViewAddress = ViewAddress()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        scrlViewAddress.isHidden = true
        let objUserInfo = getUserInfo()
        userID = objUserInfo.id ?? ""
        topBarView.topBarBGColor()
        lblTitle.text = addTitle
        setDropDownData() //new change
        configureButtons()
        txtCity.delegate = self
        arrStackViews = [addTitleStackView,address1StackView,address2StackView,CityStackView,weekendPreferenceStackView]
        arrTextViews = [txtAddress1,txtAddress2]
        arrTextViewPlaceHolder = ["Enter Address Line 1","Enter Address Line 2"]
        arrPlaceHolder = ["Select Delivery Option","Select Adress Title","Other Adderss Title","Select your city"]
        arrTextFields = [txtDeliveyOption,txtAddressTitle,txtOtherTitle,txtCity]
        arrViews = [deliveryOptionView,addressTitleView,otherTitleView,cityView]
        for item in arrViews
        {
            item.layer.cornerRadius = 2
            item.layer.masksToBounds = true
            item.layer.borderWidth = 1
            item.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
        }
        for (index,item) in arrTextFields.enumerated()
        {
            item.placeholder = arrPlaceHolder[index]
            item.textColor = UIColor(named: "theme_black_color")! //Colors.theme_black_color
            item.paddingView(xvalue: 10)
        }
        for (index,item) in arrTextViews.enumerated()
        {
            item.layer.cornerRadius = 2
            item.layer.masksToBounds = true
            item.layer.borderWidth = 1
            item.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
            item.text = arrTextViewPlaceHolder[index]
            item.textColor = UIColor(named: "theme_lightgray_color") //Colors.theme_lightgray_color
            item.textContainerInset = UIEdgeInsets(top: 5, left: 7, bottom: 5, right: 10)
            item.delegate = self
        }
        if type == "Edit"
        {
            self.callAPIForViewAddress()
        }
        if type == "Add"
        {
            isDefault = objMyAddress.isDefault ?? 0
            otherTitleStackView.isHidden = true
            scrlViewAddress.isHidden = false
            configureDefaultButton()
        }
        if type == "Register" || type == "RegisterForAppleorAmazon"
        {
            defaultAddressView.isHidden = true
            otherTitleStackView.isHidden = true
            isDefault = 1
            //            if type == "RegisterForAppleorAmazon" {
            //                defaultAddressView.isHidden = false
            //            }
            callAPIToGetAddressData()
        }
        if type  == "FromDeliveryOptions"
        {
            otherTitleStackView.isHidden = true
            defaultAddressView.isHidden = true
            callAPIToGetAddressData()
        }
    }

    // MARK: - Set DropDown Data
    func setDropDownData()
    {
        arrAddressTitle = objMyAddress.addressTitle ?? []
        arrDeliveryOptions = objMyAddress.deliveryPickupOffices ?? []
        arrCity = objMyAddress.deliveryAreas ?? []
        
        if arrAddressTitle.count > 0
        {
            setAddressTitleDropDown()
        }
        if arrDeliveryOptions.count > 0
        {
            setDeliveryOptionDropDown()
        }
        if arrCity.count > 0
        {
            setCityDropDown()
        }
    }

    // Mark: - Configure Default Button
    func configureDefaultButton()
    {
        if isDefault == 1
        {
            btnDefaultAddress.isUserInteractionEnabled = false
            imgDefaultAdd.image = UIImage(named: "select")
            btnDefaultAddress.isSelected = true
        }
        else if isDefault == 0
        {
            btnDefaultAddress.isUserInteractionEnabled = true
            btnDefaultAddress.isSelected = false
            imgDefaultAdd.image = UIImage(named: "unselect")
        }
    }
    func setDeliveryOptionDropDown()
    {
        deliveryDropDown.width = UIScreen.main.bounds.size.width - 40
        deliveryDropDown.shadowRadius = 0

        deliveryDropDown.direction = .any
        deliveryDropDown.anchorView = self.txtDeliveyOption // UIView or UIBarButtonItem
        deliveryDropDown.bottomOffset = CGPoint(x:0 , y:(txtDeliveyOption.bounds.height))
        deliveryDropDown.topOffset = CGPoint(x: 0, y:-(deliveryDropDown.anchorView?.plainView.bounds.height)!)

        deliveryDropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color

        deliveryDropDown.separatorColor = UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        deliveryDropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        deliveryDropDown.textColor = UIColor(named:"theme_text_color")!
        deliveryDropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        deliveryDropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color
                
        deliveryDropDown.dataSource = arrDeliveryOptions.map({$0.title})
        if type == "Add" || type == "Register" || type == "RegisterForAppleorAmazon" || type == "FromDeliveryOptions"
        {
            let arrResult = arrDeliveryOptions.filter({$0.id == "0"})
            if arrResult.count > 0
            {
                let index = (arrDeliveryOptions as NSArray).index(of: arrResult[0])
                deliveryDropDown.selectRow(index)
                txtDeliveyOption.text = deliveryDropDown.selectedItem
                selectedDeliveryOptionID = arrDeliveryOptions[index].id ?? ""
            }
            if selectedDeliveryOptionID == "0"
            {
                showStackViews()
            }
            else
            {
                hideStackviews()
                self.otherTitleStackView.isHidden = true
            }
        }
    }

    // MARK: - Set Address Title DropDown
    func setAddressTitleDropDown()
    {
        dropDown.width = UIScreen.main.bounds.size.width - 40
        dropDown.shadowRadius = 0

        dropDown.direction = .any
        dropDown.anchorView = self.txtAddressTitle// UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x:0 , y:(txtAddressTitle.bounds.height))
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)

        dropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDown.separatorColor = UIColor(named: "theme_lightgray_color")! //Colors.theme_lightgray_color
        dropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDown.textColor = UIColor(named:"theme_text_color")!
        dropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        dropDown.dataSource = arrAddressTitle.map({$0.name})
        if type == "Add" || type == "Register" || type == "RegisterForAppleorAmazon" || type == "FromDeliveryOptions"
        {
            dropDown.selectRow(0)
            txtAddressTitle.text = dropDown.selectedItem
        }        
    }

    // MARK: - Set City DropDown
    func setCityDropDown()
    {
        cityDropDown.width = UIScreen.main.bounds.size.width - 40
        cityDropDown.shadowRadius = 0

        cityDropDown.direction = .any
        cityDropDown.anchorView = self.txtCity // UIView or UIBarButtonItem
        cityDropDown.bottomOffset = CGPoint(x:0 , y:(txtCity.bounds.height))
        cityDropDown.topOffset = CGPoint(x: 0, y:-(cityDropDown.anchorView?.plainView.bounds.height)!)

        cityDropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        cityDropDown.separatorColor = UIColor(named: "theme_lightgray_color")! //Colors.theme_lightgray_color
        cityDropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        cityDropDown.textColor = UIColor(named:"theme_text_color")!
        cityDropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        cityDropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color

        cityDropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                let objCity = self?.arrFilterCity[index]
                self?.selectedCityID  = objCity?.id ?? ""
                self?.txtCity.text = objCity?.title ?? ""
                self?.cityDropDown.hide()
            }
        }
    }

    // MARK: - Configure Buttons
    func configureButtons()
    {
        btnBack.layer.cornerRadius = 2.0
        btnBack.layer.borderWidth = 1
        btnBack.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor //Colors.theme_lightgray_color.cgColor
        btnBack.layer.masksToBounds = true
        
        btnSubmit.layer.cornerRadius = 2.0
        btnSubmit.layer.masksToBounds = true
    }
    
    // MARK: - callAPIToGetAddressData
    func callAPIToGetAddressData()
    {
        URLManager.shared.URLCall(method: .post, parameters: [:], header: false, url: APICall.GetAddressData, showLoader:true) { (resultDict, status, message) in
            self.scrlViewAddress.isHidden = false
            if status == true
            {
                self.objMyAddress = Address(fromJson: JSON(resultDict))
                self.setDropDownData()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - callAPIForViewAddress
    func callAPIForViewAddress()
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["address_id"] = addressID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.viewAddress, showLoader:true) { (resultDict, status, message) in
            self.scrlViewAddress.isHidden = false
            if status == true
            {
                self.objViewAddress = ViewAddress(fromJson: JSON(resultDict))
                self.showData()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }

    // MARK: - showData
    func showData()
    {
        txtDeliveyOption.text = objViewAddress.officeName ?? ""
        selectedDeliveryOptionID = objViewAddress.officeId ?? ""
        if let isdefault = objViewAddress.isDefault
        {
            self.isDefault = isdefault
            self.configureDefaultButton()
        }
        
        if let officeid = objViewAddress.officeId, officeid == "0"
        {
            txtAddressTitle.text = objViewAddress.typeOfTitleString ?? ""
            selectedAddressTitleID = Int(objViewAddress.typeOfTitle ?? "0")!
            txtAddress1.text = objViewAddress.address1 ?? arrTextViewPlaceHolder[0]
            txtAddress2.text = objViewAddress.address2 ?? arrTextViewPlaceHolder[1]
            if txtAddress1.text == arrTextViewPlaceHolder[0]
            {
                txtAddress1.textColor = UIColor(named: "theme_lightgray_color")! //Colors.theme_lightgray_color
            }
            else
            {
                txtAddress1.textColor = UIColor(named: "theme_black_color")! //Colors.theme_black_color
            }
            if txtAddress2.text == arrTextViewPlaceHolder[1]
            {
                txtAddress2.textColor = UIColor(named: "theme_lightgray_color")! //Colors.theme_lightgray_color
            }
            else
            {
                txtAddress2.textColor = UIColor(named: "theme_black_color")! //Colors.theme_black_color
            }
            txtCity.text = objViewAddress.areaName ?? ""
            selectedCityID = objViewAddress.areaId ?? ""
            self.showStackViews()
        }
        else
        {
            self.hideStackviews()
        }
        if let otherTitle = objViewAddress.typeOfTitle,(otherTitle == "3")
//        if let otherTitle = objViewAddress.typeOfTitleString,(otherTitle == "Other" || otherTitle == "OTHER")
        {
            otherTitleStackView.isHidden = false
            txtOtherTitle.text = objViewAddress.otherTitle ?? ""
        }
        else
        {
            otherTitleStackView.isHidden = true
            txtOtherTitle.text = ""
        }
       
        self.enableDisalbeAddressChangeControlls()
    }
    
    // MARK: - enableDisalbeAddressChangeControlls
    func enableDisalbeAddressChangeControlls(){

        if type == "Edit" && objViewAddress.officeId == "0" && isDefault == 1 {
            self.deliveryOptionView.isUserInteractionEnabled = false
            self.addressTitleView.isUserInteractionEnabled = false
            self.otherTitleView.isUserInteractionEnabled = false
            self.txtAddress1.isUserInteractionEnabled = false
            self.txtAddress2.isUserInteractionEnabled = false
            self.cityView.isUserInteractionEnabled = false
            
            self.deliveryOptionView.backgroundColor = UIColor(named: "theme_view_disable_color")
            self.addressTitleView.backgroundColor = UIColor(named: "theme_view_disable_color")
            self.otherTitleView.backgroundColor = UIColor(named: "theme_view_disable_color")
            self.txtAddress1.backgroundColor = UIColor(named: "theme_view_disable_color")
            self.txtAddress2.backgroundColor = UIColor(named: "theme_view_disable_color")
            self.cityView.backgroundColor = UIColor(named: "theme_view_disable_color")
            
        }else{
            self.deliveryOptionView.isUserInteractionEnabled = true
            self.addressTitleView.isUserInteractionEnabled = true
            self.otherTitleView.isUserInteractionEnabled = true
            self.txtAddress1.isUserInteractionEnabled = true
            self.txtAddress2.isUserInteractionEnabled = true
            self.cityView.isUserInteractionEnabled = true
            
            self.deliveryOptionView.backgroundColor = UIColor(named: "theme_textfield_bgcolor")
            self.addressTitleView.backgroundColor = UIColor(named: "theme_textfield_bgcolor")
            self.otherTitleView.backgroundColor = UIColor(named: "theme_textfield_bgcolor")
            self.txtAddress1.backgroundColor = UIColor(named: "theme_textfield_bgcolor")
            self.txtAddress2.backgroundColor = UIColor(named: "theme_textfield_bgcolor")
            self.cityView.backgroundColor = UIColor(named: "theme_textfield_bgcolor")
        }
    }
    
    // MARK: - Hide Show Stack Views
    func hideStackviews()
    {
        for item in arrStackViews
        {
            item.isHidden = true
        }
        resetWeekendPreferenceUI()
    }

    // MARK: - Show Stack Views
    func showStackViews()
    {
        for item in arrStackViews
        {
            item.isHidden = false
        }
        
        if ((type == "Add" || type == "Edit") && (selectedDeliveryOptionID == "0") && isDefault == 1){
            weekendPreferenceStackView.isHidden = false
            setDeliveryPreferenceDataOnEdit()
        }else{
            weekendPreferenceStackView.isHidden = true
        }
        
        if self.selectedAddressTitleID == 3 {
            self.otherTitleStackView.isHidden = false
        }else{
            self.otherTitleStackView.isHidden = true
        }
    }
    
    // MARK: - callAPIForAddAddress
    func callAPIForAddAddress()
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["address_id"] = addressID
        params["office_id"] = selectedDeliveryOptionID
        params["type_of_title"] = selectedAddressTitleID

        if selectedAddressTitleID == 3  // Other / OTHER
        {
            params["other_title"] = txtOtherTitle.text ?? ""
        }

        params["address1"] = txtAddress1.text ==  arrTextViewPlaceHolder[0] ? "" : txtAddress1.text
        params["address2"] = txtAddress2.text ==  arrTextViewPlaceHolder[1] ? "" : txtAddress2.text
        params["area_id"] = selectedCityID
        params["is_default"] = isDefault
        
        if selectedDeliveryOptionID == "0" && isDefault == 1 {
            isWeekdays = self.btnWeekdays.isSelected ? "1" : "0"
            isSunday = self.btnSunday.isSelected ? "1" : "0"
            isSaturday = self.btnSaturday.isSelected ? "1" : "0"
            
            params["is_weekdays"] = isWeekdays
            params["is_saturday"] = isSaturday
            params["is_sunday"] = isSunday
        }
        
        print("Parameters are \(params)")
        
        self.showHUDOnView(view: self.view)
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.SaveAddress, showLoader:false) { [self] (resultDict, status, message) in
            self.hideHUDFromView(view: self.view)
            if status == true
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true)
                redirectAfterAddressUpdate()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if selectedDeliveryOptionID == "0"
        {
            isDeliveryPrefSelected = (self.btnWeekdays.isSelected || self.btnSunday.isSelected || self.btnSaturday.isSelected)

            if isCheckNull(strText: txtAddressTitle.text!)
            {
                appDelegate.showToast(message: Messsages.msg_select_address_title, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtOtherTitle.text!) && (txtAddressTitle.text == "Other" || txtAddressTitle.text == "OTHER")
            {
                appDelegate.showToast(message: Messsages.msg_other_title, bottomValue: getSafeAreaValue())
                return false
            }
            else if txtAddress1.text == arrTextViewPlaceHolder[0]
            {
                appDelegate.showToast(message: Messsages.msg_enter_address, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtCity.text!)
            {
                appDelegate.showToast(message: Messsages.msg_select_city, bottomValue: getSafeAreaValue())
                return false
            }
            else if selectedCityID == ""
            {
                appDelegate.showToast(message: Messsages.msg_select_valid_city, bottomValue: getSafeAreaValue())
                return false
            }
            else if ((type == "Add" || type == "Edit") && (!isDeliveryPrefSelected) && isDefault == 1){
                    appDelegate.showToast(message: Messsages.msg_select_delivery_preference, bottomValue: getSafeAreaValue())
                    return false
            }
            else if isAgreeTermsOfUse == 0
            {
                appDelegate.showToast(message: Messsages.msg_agree_terms, bottomValue: getSafeAreaValue())
                return false
            }
            return true
        }
        else if isAgreeTermsOfUse == 0
        {
            appDelegate.showToast(message: Messsages.msg_agree_terms, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    
    // MARK: - redirectAfterAddressUpdate
    func redirectAfterAddressUpdate(){
        if self.type == "RegisterForAppleorAmazon"
        {
            var params : [String:Any] = [:]
            params["deliveryOption"] = self.txtDeliveyOption.text
            UserDefaults.standard.setValue(true, forKey: kIsAddresAdded)
            UserDefaults.standard.setValue(self.txtDeliveyOption.text ?? "", forKey: kDeliveryOption)
            NotificationCenter.default.post(name: Notification.Name("GetAddressData"), object: nil,userInfo: params)
            self.navigationController?.popViewController(animated: true)
        }
        else if self.type == "FromDeliveryOptions"
        {
            NotificationCenter.default.post(name: Notification.Name("AddressUpdated"), object: nil)
            UIView.transition(with: self.view.superview!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            })
            
        }
        else
        {
            NotificationCenter.default.post(name: Notification.Name("AddressUpdated"), object: nil)
            self.navigationController?.popViewController(animated: true)
        }

    }

    // MARK: - IBAction methods
    @IBAction func btnDeliveryOptionClicked()
    {
        deliveryDropDown.show()
        deliveryDropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                let objDeliveryOption = self?.arrDeliveryOptions[index]
                self?.selectedDeliveryOptionID  = objDeliveryOption?.id ?? ""
                self?.txtDeliveyOption.text = objDeliveryOption?.title ?? ""
                self?.dropDown.hide()
                if self?.selectedDeliveryOptionID == "0"
                {
                    self?.showStackViews()
                }
                else
                {
                    self?.hideStackviews()
                    self?.otherTitleStackView.isHidden = true
                }
            }
        }
    }
    @IBAction func btnAddTitleClicked()
    {
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                let objAddressTitle = self?.arrAddressTitle[index]
                print("Address title is \(String(describing: objAddressTitle?.id) )")
                self?.selectedAddressTitleID  = objAddressTitle?.id ?? 0
                self?.txtAddressTitle.text = objAddressTitle?.name ?? ""
                self?.dropDown.hide()
                if let selectedId = objAddressTitle?.id, (selectedId == 3)      // Other or OTHER
                {
                    self?.otherTitleStackView.isHidden = false
                }
                else
                {
                    self?.otherTitleStackView.isHidden = true
                }
            }
        }
    }
    @IBAction func btnDefaultAddressClicked(_ sender:UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected
        {
            imgDefaultAdd.image = UIImage(named: "select")
            isDefault = 1
            if ((type == "Add" || type == "Edit") && (selectedDeliveryOptionID == "0")){
                self.weekendPreferenceStackView.isHidden = false
            }
        }
        else
        {
            imgDefaultAdd.image = UIImage(named: "unselect")
            isDefault = 0
            self.weekendPreferenceStackView.isHidden = true
        }
    }
    @IBAction func btnTermsClicked(_ sender:UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected
        {
            imgTerms.image = UIImage(named: "select")
            isAgreeTermsOfUse = 1
        }
        else
        {
            imgTerms.image = UIImage(named: "unselect")
            isAgreeTermsOfUse = 0
        }
    }
    @IBAction func btnBackClicked()
    {
        if type == "FromDeliveryOptions"
        {
            UIView.transition(with: self.view.superview!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            })
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnSubmitClicked()
    {
        self.view.endEditing(true)
        if isValidData()
        {
            if type == "Register"
            {
                var params : [String:Any] = [:]
                params["office_id"] = selectedDeliveryOptionID
                params["type_of_title"] = selectedAddressTitleID
                params["other_title"] = txtOtherTitle.text ?? ""
                params["address1"] = txtAddress1.text ==  arrTextViewPlaceHolder[0] ? "" : txtAddress1.text
                params["address2"] = txtAddress2.text ==  arrTextViewPlaceHolder[1] ? "" : txtAddress2.text
                params["area_id"] = selectedCityID
                params["deliveryOption"] = self.txtDeliveyOption.text
                NotificationCenter.default.post(name: Notification.Name("GetAddressData"), object: nil,userInfo: params)
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.callAPIForAddAddress()
            }
        }
    }
}
// MARK: - UITextViewDelegate
extension AddressVC : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == arrTextViewPlaceHolder[0]
        {
            txtAddress1.text = ""
        }
        else if textView.text == arrTextViewPlaceHolder[1]
        {
            txtAddress2.text = ""
        }
        textView.textColor = UIColor(named:"theme_black_color") //Colors.theme_black_color
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == txtAddress1
        {
            if textView.text.isEmpty
            {
                txtAddress1.text = arrTextViewPlaceHolder[0]
                textView.textColor = UIColor(named: "theme_lightgray_color")!
            }
        }
        else if textView == txtAddress2
        {
            if textView.text.isEmpty
            {
                txtAddress2.text = arrTextViewPlaceHolder[1]
                textView.textColor = UIColor(named: "theme_lightgray_color")!
            }
        }
    }
}

// MARK: - UITextFieldDelegate
extension AddressVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == txtCity
        {
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    selectedCityID = ""
                    return true
                }
            }
            if newString.length > 2
            {
                arrFilterCity = arrCity.filter({$0.title.localizedCaseInsensitiveContains(newString as String)})
                if arrFilterCity.count > 0
                {
                    self.txtCity.resignFirstResponder()
                    self.cityDropDown.dataSource = arrFilterCity.map({$0.title})
                    self.cityDropDown.reloadAllComponents()
                    self.cityDropDown.show()
                }
                else
                {
                    selectedCityID = ""
                }
            }
            else
            {
                selectedCityID = ""
                self.cityDropDown.dataSource = []
                self.cityDropDown.reloadAllComponents()
                self.cityDropDown.hide()
            }
        }
        else if textField == txtAddress1 || textField == txtAddress2
        {
            let newLength = text.count + string.count - range.length
            return newLength <= 10
        }
        return true
    }
}

// MARK: - Delivery Preference
extension AddressVC {
    @IBAction func btnWeekdaysClicked(_ sender: UIButton) {
        print("btnWeekdaysClicked")
        if btnWeekdays.isSelected {
            btnWeekdays.isSelected = false
            imgWeekdayCheckBox.image = UIImage(named: "uncheckbox")
        }else{
            btnWeekdays.isSelected = true
            imgWeekdayCheckBox.image = UIImage(named: "checkbox")
        }
    }
    @IBAction func btnSaturdayClicked(_ sender: UIButton) {
        print("btnSaturdayClicked")
        if btnSaturday.isSelected {
            btnSaturday.isSelected = false
            imgSaturdayCheckBox.image = UIImage(named: "uncheckbox")
        }else{
            btnSaturday.isSelected = true
            imgSaturdayCheckBox.image = UIImage(named: "checkbox")
        }
    }
    @IBAction func btnSundayClicked(_ sender: UIButton) {
        print("btnSundayClicked")
        if btnSunday.isSelected {
            btnSunday.isSelected = false
            imgSundayCheckBox.image = UIImage(named: "uncheckbox")
        }else{
            btnSunday.isSelected = true
            imgSundayCheckBox.image = UIImage(named: "checkbox")
        }
    }
    
    func resetWeekendPreferenceUI(){
        btnWeekdays.isSelected = false
        btnSaturday.isSelected = false
        btnSunday.isSelected = false
        
        imgWeekdayCheckBox.image = UIImage(named: "uncheckbox")
        imgSaturdayCheckBox.image = UIImage(named: "uncheckbox")
        imgSundayCheckBox.image = UIImage(named: "uncheckbox")
    }
    
    func setDeliveryPreferenceDataOnEdit(){
        if objViewAddress.isWeekdays == "1" {
            btnWeekdays.isSelected = true
            imgWeekdayCheckBox.image = UIImage(named: "checkbox")
        }else{
            btnWeekdays.isSelected = false
            imgWeekdayCheckBox.image = UIImage(named: "uncheckbox")
        }
        
        if objViewAddress.isSaturday == "1" {
            btnSaturday.isSelected = true
            imgSaturdayCheckBox.image = UIImage(named: "checkbox")
        }else{
            btnSaturday.isSelected = false
            imgSaturdayCheckBox.image = UIImage(named: "uncheckbox")
        }
        
        if objViewAddress.isSunday == "1" {
            btnSunday.isSelected = true
            imgSundayCheckBox.image = UIImage(named: "checkbox")
        }else{
            btnSunday.isSelected = false
            imgSundayCheckBox.image = UIImage(named: "uncheckbox")
        }
    }
}
