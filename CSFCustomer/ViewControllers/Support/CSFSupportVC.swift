//
//  CSFSupportVC.swift
//  CSFCustomer
//
//  Created by Tops on 23/04/21.
//

import UIKit
import YM_IOS_SDK
import WebKit
import IQKeyboardManagerSwift
class CSFSupportVC: UIViewController,WKNavigationDelegate, WKUIDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var topBarView : UIView!
    
    // MARK: - Global Variables
    var botId:String = ""
    var botName:String = ""
    var botPayload:String = ""
    var button:UIButton!
    var webView : WKWebView!
    var chatbotURL = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        topBarView.topBarBGColor()
        self.chatbotURL = "https://app.yellowmessenger.com/components/public/webviews/mobile-sdk/index.html"
        //old one //"https://chat.botplatform.io/mobile/"+botId
        self.botId = kBotID
        self.botName = kBotName
        self.botPayload = "" //eyJkZXZpY2UiOiJpT1MifQ=="
        let height = self.tabBarController?.tabBar.frame.height ?? 49.0
        webView = WKWebView.init(frame: CGRect(x:0, y:100, width:UIScreen.main.bounds.width, height: self.view.frame.size.height-(height+65)))
        webView.navigationDelegate = self
        webView.uiDelegate = self
        view.addSubview(webView)
        let urlString = "\(chatbotURL)?botId=\(botId)"
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnMenuClicked()
    {
        self.revealViewController().revealToggle(animated: true)
    }
    // MARK: - Webview delegate methods
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //Show loader
        self.showHUD()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //Hide loader
        self.hideHUD()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        //Hide loader
        self.hideHUD()
    }
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            if let urlAsString = navigationAction.request.url?.absoluteString.lowercased(),urlAsString != ""
            {
                if let url = URL(string: urlAsString), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
        return nil
    }
}
