//
//  ReportVC.swift
//  CSFCustomer
//
//  Created by Tops on 06/10/21.
//

import UIKit

let messageCharacterLimit : Int = 200
class ReportVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var scrlView : UIScrollView!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var stackViewAttachment1 : UIView!
    @IBOutlet weak var stackViewAttachment2 : UIView!
    @IBOutlet weak var stackViewAttachment3 : UIView!
    @IBOutlet weak var viewAttachment1 : UIView!
    @IBOutlet weak var viewAttachment2 : UIView!
    @IBOutlet weak var viewAttachment3 : UIView!
    @IBOutlet weak var imgAttachment1 : UIImageView!
    @IBOutlet weak var imgAttachment2 : UIImageView!
    @IBOutlet weak var imgAttachment3 : UIImageView!
    @IBOutlet weak var tvMessage : UITextView!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var lblDescNote : UILabel!
    @IBOutlet weak var lblAttachmentNote : UILabel!

    @IBOutlet weak var btnAttachment1 : UIButton!
    @IBOutlet weak var btnAttachment2 : UIButton!
    @IBOutlet weak var btnAttachment3 : UIButton!
    @IBOutlet weak var btnRemoveAttachment1 : UIButton!
    @IBOutlet weak var btnRemoveAttachment2 : UIButton!
    @IBOutlet weak var btnRemoveAttachment3 : UIButton!
    
    @IBOutlet weak var viewRemoveAttachment1 : UIView!
    @IBOutlet weak var viewRemoveAttachment2 : UIView!
    @IBOutlet weak var viewRemoveAttachment3 : UIView!
        
    // MARK: - Global Variables
    var type : String = ""
    var strType : String!
    var placeholderText = "What went wrong?"
    var mimeType : String  = ""
    var isImageRemoved : Int = 0
    
    var docFileName : String = ""
    var arrDocFileNames : [String] = [String]()

    var uploadData = Data()
    
    var currentAttachmentIndex : Int!
    var totalBoxCount : Int = 1
    var arrAttachment : [UIImage] = [UIImage]()
    var arrAttachmentData : [Data] = [Data]()
    var arrMimeType : [String] = [String]()
    var strDescription : String = ""
    var objUserInfo = UserInfo()

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        scrlView.isScrollEnabled = false
        topBarView.topBarBGColor()
        manageScreenTitle()
        
        viewAttachment1.layer.cornerRadius = 5
        viewAttachment1.layer.masksToBounds = true
        
        viewAttachment2.layer.cornerRadius = 5
        viewAttachment2.layer.masksToBounds = true
        
        viewAttachment3.layer.cornerRadius = 5
        viewAttachment3.layer.masksToBounds = true
        
        viewRemoveAttachment1.layer.cornerRadius = viewRemoveAttachment1.frame.size.height / 2
        viewRemoveAttachment1.layer.masksToBounds = true
        
        viewRemoveAttachment2.layer.cornerRadius = viewRemoveAttachment2.frame.size.height / 2
        viewRemoveAttachment2.layer.masksToBounds = true
        
        viewRemoveAttachment3.layer.cornerRadius = viewRemoveAttachment3.frame.size.height / 2
        viewRemoveAttachment3.layer.masksToBounds = true
        
        configureTextView(textField:tvMessage, placeHolder: placeholderText, font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named:"theme_lightgray_color")!, cornerRadius: 0, borderColor: UIColor(named:"theme_lightgray_color")!, borderWidth: 1, bgColor: UIColor(named:"theme_textfield_bgcolor")!)
        tvMessage.textContainerInset = UIEdgeInsets(top: 5, left: 7, bottom: 5, right: 10)
        tvMessage.delegate = self
        manageUI()
        
        objUserInfo = getUserInfo()
    }
    
    // MARK: - manageUI
    func manageUI(){
        
        imgAttachment1.image = nil
        imgAttachment2.image = nil
        imgAttachment3.image = nil
        
        imgAttachment1.isHidden = false
        imgAttachment2.isHidden = false
        imgAttachment3.isHidden = false
        
        viewRemoveAttachment1.isHidden = true
        viewRemoveAttachment2.isHidden = true
        viewRemoveAttachment3.isHidden = true
        
        btnAttachment1.isHidden = false
        btnAttachment2.isHidden = true
        btnAttachment3.isHidden = true
        
        if arrAttachment.count > 0 {
            if arrAttachment.count >= 1 {
                viewRemoveAttachment1.isHidden = false
                btnAttachment1.isHidden = true
                btnAttachment2.isHidden = false
                imgAttachment1.image = arrAttachment[0]
            }
            
            if arrAttachment.count >= 2 {
                viewRemoveAttachment2.isHidden = false
                btnAttachment2.isHidden = true
                btnAttachment3.isHidden = false
                imgAttachment2.image = arrAttachment[1]
            }
            
            if arrAttachment.count >= 3 {
                viewRemoveAttachment3.isHidden = false
                btnAttachment3.isHidden = true
                imgAttachment3.image = arrAttachment[2]
            }
        }
    }
   
    /**
     Manages the screen title based on the type value.
     
     If the type is "1", sets the screen title to "Report a bug".
     If the type is "2", sets the screen title to "Suggestion and improvement".
     
     Also sets the text for lblAttachmentNote and lblDescNote labels.
     */
    func manageScreenTitle(){
        //print("Type is \(String(describing: type))")
        if type == "1" {
            //print("Report a bug")
            self.lblTitle.text = "Report a bug"
        }else if type == "2" {
            //print("Suggestion an improvement")
            self.lblTitle.text = "Suggestion and improvement"
        }
        self.lblAttachmentNote.text = "The attachment size should be less than 2 MB."
        self.lblDescNote.text = "The description allowed up to 200 characters."
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.view.endEditing(true)
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = .reveal
        transition.subtype = .fromBottom
        self.navigationController?.view.layer.add(transition, forKey:kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnAddAttachment1Clicked(){
        self.view.endEditing(true)
        currentAttachmentIndex = 1
        openImagePickerActionsheet()
    }
    
    @IBAction func btnAddAttachment2Clicked(){
        self.view.endEditing(true)
        currentAttachmentIndex = 2
        openImagePickerActionsheet()
    }
    
    @IBAction func btnAddAttachment3Clicked(){
        currentAttachmentIndex = 3
        openImagePickerActionsheet()
    }
    
    @IBAction func btnRemoveAttachment1Clicked(){
        self.view.endEditing(true)
        
        showAlert(title: "", msg: Messsages.msg_remove_image_confirm, viewController: self) { [self] status in
            if status {
                if arrAttachment.count > 0 {
                    removeFromIndex(index: 0)
                }
                manageUI()
            }
        }
    }
    
    @IBAction func btnRemoveAttachment2Clicked(){
        showAlert(title: "", msg: Messsages.msg_remove_image_confirm, viewController: self) { [self] status in
            if status {
                if arrAttachment.count > 0 {
                    removeFromIndex(index: 1)
                }
                manageUI()
            }
        }
    }
    
    @IBAction func btnRemoveAttachment3Clicked(){
        self.view.endEditing(true)
        
        showAlert(title: "", msg: Messsages.msg_remove_image_confirm, viewController: self) { [self] status in
            if status {
                
                if arrAttachment.count > 0 {
                    removeFromIndex(index: 2)
                }
                manageUI()
            }
        }
    }
    
    @IBAction func btnSendReportClicked(){
        self.view.endEditing(true)
        
        strDescription = tvMessage.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if isValidData() {
            self.callAPIToSendReport()
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if ((strDescription.count == 0) || tvMessage.text == placeholderText) {
            showAlert(title: "", msg: Messsages.msg_enter_desc_report, vc: self)
            return false
        }else if type == "1" {
            if arrAttachment.count == 0 {
                showAlert(title: "", msg: Messsages.msg_enter_add_some_attachment, vc: self)
                return false
            }
        }
        return true
    }
    
    /**
     Removes an item from the specified index in the collection.
     */    
    func removeFromIndex(index : Int){
        arrAttachment.remove(at: index)
        arrAttachmentData.remove(at: index)
        arrDocFileNames.remove(at: index)
        arrMimeType.remove(at: index)
    }
    
    /**
     Shows a success popup with the given message.
     */
    func showReportSuccessPopup(msg : String){
        if let popupVC = appDelegate.getViewController("ReportBugSuccessPopupVC", onStoryboard: "Profile") as? ReportBugSuccessPopupVC {
            popupVC.modalPresentationStyle = .overFullScreen
            popupVC.successMessage = msg
            popupVC.reportVC = self
            if let objNavigationController  = appDelegate.topNavigation() as? UINavigationController
            {
                objNavigationController.present(popupVC, animated: true)
            }
        }
    }
}

extension ReportVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    // MARK: - Custom methods
    /**
     Opens an image picker actionsheet to allow the user to select an image.
     */
    func openImagePickerActionsheet(){
        let attributedString = NSAttributedString(string: "Please select option", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15), //your font here
            NSAttributedString.Key.foregroundColor :Colors.theme_gray_color
        ])
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(attributedString, forKey: "attributedTitle")
        
        let photoAction = UIAlertAction(title: "Photo Gallery" , style: .default) { (_ action) in
            self.openGallery()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel" , style: .cancel) { (_ action) in
            self.dismiss(animated: true, completion: nil)
        }
        
        photoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(photoAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    /**
     Opens the camera to capture an image.
     */
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            PermissionManager.shared.requestCameraPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.camera
                        imagePicker.allowsEditing = false
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    /**
     Opens the gallery to allow the user to select an image.
     */
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            PermissionManager.shared.requestPhotoPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                        imagePicker.allowsEditing = false
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
        {
            switch url.pathExtension
            {
            case "jpg","jpeg":
                self.mimeType = "image/jpeg"
                break
            case "png":
                self.mimeType = "image/png"
                break
            default:
                // print("No file")
                showAlert(title: "", msg: Messsages.msg_image_type, vc: self)
                return
            }
        }
       
        if let pickedImage = info[.originalImage] as? UIImage
        {
            let dblSize = pickedImage.getSizeIn(mimeType: self.mimeType, type: .megabyte)
            if dblSize > 0 {
                let imgSizeInMb : Double = dblSize
                if imgSizeInMb > 2.0 {
                    appDelegate.showToast(message: Messsages.msg_attachment_should_less_than_2_mb, bottomValue: getSafeAreaValue())
                    picker.dismiss(animated: true, completion: nil)
                    return
                }
                
                if self.mimeType == ""
                {
                    self.mimeType = "image/png"
                }
                isImageRemoved = 0
                
                let currentTimeStamp = String(Int(NSDate().timeIntervalSince1970))
                self.docFileName = String(format: "%@%@%@", "csf-Report-img-",currentTimeStamp,".png")
                setImageAndData(pickedImage: pickedImage)
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func setImageAndData(pickedImage : UIImage){
        if currentAttachmentIndex == 1 {
            imgAttachment1.contentMode = .scaleAspectFill
        }else if currentAttachmentIndex == 2 {
            imgAttachment2.contentMode = .scaleAspectFill
        }else if currentAttachmentIndex == 3 {
            imgAttachment3.contentMode = .scaleAspectFill
        }
        arrDocFileNames.append(self.docFileName)
        arrAttachment.append(pickedImage)
        arrMimeType.append(self.mimeType)
        
//        let thumb1 = pickedImage.resized(withPercentage: 0.1)
        
        let thumb1 = pickedImage
        DispatchQueue.global(qos: .background).async { [self] in
            if self.mimeType == "image/jpeg"
            {
                self.uploadData = pickedImage.jpegData(compressionQuality: 0.3)!
            }
            else if self.mimeType == "image/png"
            {
                self.uploadData = (thumb1.pngData())!
            }
            
            arrAttachmentData.append(self.uploadData)
        }
        manageUI()
    }
}

extension ReportVC : UITextViewDelegate{
    // MARK: - Textview delegate methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholderText
        {
            textView.text = ""
            textView.textColor = UIColor(named: "theme_black_color")//Colors.theme_black_color
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty
        {
            textView.text = placeholderText
            textView.textColor = UIColor(named: "theme_lightgray_color") //Colors.theme_lightgray_color
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return newText.count <= messageCharacterLimit
    }
}

extension ReportVC {
    /**
     Calls the API to send a report.     */
    func callAPIToSendReport()
    {
        let fieldName = ["report_image1","report_image2","report_image3"]
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["notes"] = self.strDescription
        params["type"] = self.type
        params["documents[]"] = self.arrAttachmentData

        //print("param is \(params)")
        
        URLManager.shared.URLCallMultipleMultipartData(method: .post, parameters: params, header: true, url: APICall.ReportIssue, showLoader: true, WithName: fieldName, docFileName: arrDocFileNames, uploadData: arrAttachmentData, mimeType: arrMimeType) { [self] (resultDict, status, message) in
          
            if status == true
            {
                showReportSuccessPopup(msg: message)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}