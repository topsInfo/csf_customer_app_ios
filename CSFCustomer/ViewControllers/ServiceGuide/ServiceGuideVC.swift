//
//  ServiceGuideVC.swift
//  CSFCustomer
//
//  Created by Tops on 25/03/21.
//

import UIKit
import SwiftyJSON
import MBProgressHUD

class ServiceGuideVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblServiceGuideText : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnAgree : UIButton!
    @IBOutlet weak var bottomConstScrollView : NSLayoutConstraint!
    @IBOutlet weak var heightConst : NSLayoutConstraint!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var contentView : UIStackView!
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    var pdfFileURL : String = ""
    var objUserInfo = UserInfo()
    var type : Int = 1 // 1 -> service guide / 2 -> online shopper guide
    
    var serviceGuideMessage : String = ""
    var onlineShopperMessage : String = ""
//    var filename : String = ""
    // MARK: - Viewcontroller life cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        heightConst.constant = 0
        btnAgree.isHidden = true
        getData()
    }
    
    @objc func willEnterForeground() {
        getData()
    }
    
    // MARK: - getData
    func getData(){
        if type == 1 {
            lblTitle.text = "Service Guide"
            getServiceGuideData()
        }else if type == 2 {
            lblTitle.text = "Online Shopper Guide"
            getOnlineShopperGuideData()
        }
    }
    
    // MARK: - getServiceGuideData
    func getServiceGuideData()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ServiceGuideList, showLoader:true) { [self] (resultDict, status, message) in
            if status == true
            {
                let objServiceGuide = ServiceGuide(fromJson: JSON(resultDict))
                if let strMessage = objServiceGuide.serviceGuideMessage
                {
                    serviceGuideMessage = strMessage
                    reloadData()
                }
                if let fileurl = objServiceGuide.serviceGuideLink,fileurl != ""
                {
                    pdfFileURL = fileurl
                }
                lblTitle.text = "Service Guide \(objServiceGuide.serviceGuideVersion ?? "")"
                if let serviceGuideAlertFlag = objServiceGuide.serviceGuideAlert, serviceGuideAlertFlag == 1
                {
                    heightConst.constant = 45
                    bottomConstScrollView.constant = 10
                    btnAgree.isHidden = false
                }
                else
                {
                    heightConst.constant = 0
                    bottomConstScrollView.constant = 10
                    btnAgree.isHidden = true
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - getOnlineShopperGuideData
    func getOnlineShopperGuideData()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.OnlineShopperGuideList, showLoader:true) { [self] (resultDict, status, message) in
            if status == true
            {
                
                let objOnlineShopperGuide = OnlineShopperGuide(fromJson: JSON(resultDict))
                if let strMessage = objOnlineShopperGuide.onlineShopperMessage
                {
                    onlineShopperMessage = strMessage
                    reloadData()
                }
                if let fileurl = objOnlineShopperGuide.onlineShopperGuideLink,fileurl != ""
                {
                    pdfFileURL = fileurl
                }

                lblTitle.text = "Online Shopper Guide"
                if let onlineShopperGuideAlertFlag = objOnlineShopperGuide.onlineShopperAlert, onlineShopperGuideAlertFlag == 1
                {
                    heightConst.constant = 45
                    bottomConstScrollView.constant = 10
                    btnAgree.isHidden = false
                }
                else
                {
                    heightConst.constant = 0
                    bottomConstScrollView.constant = 10
                    btnAgree.isHidden = true
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    /**
     Saves a PDF file from the given URL to the specified file name.
     */
    func savePdf(urlString:String, fileName:String) {
        DispatchQueue.global(qos: .background).async {
            let fileurl = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            let url = URL(string: fileurl!)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = fileName //"YourAppName-\(fileName).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                try pdfData?.write(to: actualPath, options: .atomic)
                DispatchQueue.main.async { [self] in
                    self.hideHUD()
                    print("dest file url is \(actualPath.absoluteString)")

                    if type == 1 {
                        let msgBody = Messsages.msg_service_guide_downloaded
                        self.setNotification(msgBody:msgBody,pdffileURL:actualPath.absoluteString,invoiceID:"serviceguide-notification", downloadedFileType: "ServiceGuide")
                    }else if type == 2 {
                        let msgBody = Messsages.msg_online_shopper_guide_downloaded
                        self.setNotification(msgBody: msgBody, pdffileURL: actualPath.absoluteString, invoiceID: "onlineshopperguide-notification",downloadedFileType: "OnlineShopperGuide")
                    }
                    
                }
            } catch {
                DispatchQueue.main.async { [self] in
                    self.hideHUD()
                    if type == 1 {
                        let msgBody = Messsages.msg_service_guide_download_failure
                        self.setNotification(msgBody:msgBody,pdffileURL:urlString, invoiceID:"serviceguide-notification", downloadedFileType: "ServiceGuide")
                    }else if type == 2 {
                        let msgBody = Messsages.msg_online_shopper_guide_download_failure
                        self.setNotification(msgBody: msgBody, pdffileURL: urlString, invoiceID: "onlineshopperguide-notification", downloadedFileType: "OnlineShopperGuide")
                    }
                    
                }
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        NotificationCenter.default.post(name: Notification.Name("ServiceGuideBackClicked"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAgreeClicked()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["type"] = self.type
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ServiceGuideAgree, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.popupAlert(title: "", message: message, actionTitles: ["Ok"], actions:[{action1 in
                    self.goToHomeVC()
                }])
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
        
    }
    @IBAction func btnDownloadClicked()
    {
        var filename : String = ""
        var actualFilename : String = ""
        if pdfFileURL != ""
        {
            DispatchQueue.main.async {
                self.showHUD()
            }
            //=======================
            let arrURLComponets = pdfFileURL.components(separatedBy: "/")
            if arrURLComponets.count > 0
            {
                filename = arrURLComponets.last!
            }
            
            if filename.contains(".pdf") {
                if filename.components(separatedBy: ".pdf").count > 0 {
                    actualFilename = filename.components(separatedBy: ".pdf").first!
                }
            }
            
            let time = Date().timeIntervalSince1970
            actualFilename = "CSFCouriers-\(actualFilename)-\(time).pdf"
            print("actualFilename name is \(actualFilename)")
            savePdf(urlString: pdfFileURL, fileName: actualFilename)
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Do sonthing
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                reloadData()
            }
        }else{
            reloadData()
        }
    }
    
    /**
     Reloads the data for the ServiceGuideVC view controller.
     */
    func reloadData(){
        if self.type == 1 {
            self.loadServiceGuideText(strMessage: self.serviceGuideMessage)
        }else if self.type == 2 {
            self.loadServiceGuideText(strMessage: self.onlineShopperMessage)
        }
    }
    
    /**
     Loads the service guide text with the specified message.     
     */    
    func loadServiceGuideText(strMessage : String){
        DispatchQueue.main.async {
            
            if (UserDefaults.standard.value(forKey: kIsDarkModeEnabled) as? Bool) == true{
                self.lblServiceGuideText.attributedText = strMessage.htmlToAttributedString(size: 15, hexStringColor: "#FFFFFF", fontFamily: fontname.openSansRegular)
            }
            
            if (UserDefaults.standard.value(forKey: kIsLightModeEnabled) as? Bool) == true{
                self.lblServiceGuideText.attributedText = strMessage.htmlToAttributedString(size: 15, hexStringColor: "#000000", fontFamily: fontname.openSansRegular)
            }
            
            if (UserDefaults.standard.value(forKey: kIsSystemModeModeEnabled) as? Bool) == true{
                if isDarkTheme() {
                    self.lblServiceGuideText.attributedText = strMessage.htmlToAttributedString(size: 15, hexStringColor: "#FFFFFF", fontFamily: fontname.openSansRegular)
                }else{
                    self.lblServiceGuideText.attributedText = strMessage.htmlToAttributedString(size: 15, hexStringColor: "#000000", fontFamily: fontname.openSansRegular)
                }
            }
        }
    }
}
