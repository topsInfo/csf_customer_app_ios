//
//  ViewCartVC.swift
//  CSFCustomer
//
//  Created by Tops on 30/12/20.
//

import UIKit
import SwiftyJSON
let footerHeight : CGFloat = 173
class ViewCartVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var viewTotalRecord : TotalRecordView!
    @IBOutlet weak var cnstTotalRecordsTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var tblCartList : UITableView!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var cnstViewFooterHeight : NSLayoutConstraint!
    @IBOutlet weak var totalAmount : UILabel!
    @IBOutlet weak var btnAddInvoice : UIButton!
    
    // MARK: - Global Variable
    var userID : String  = ""
    var arrCartList : [CartList] = [CartList]()
    var arrSelectedSections : [Int] = [Int]()
    var isFromVC : String = ""
    var totalUSDAmount : String  = "0"
    var totalTTDAmount : String = "0"
    var walletUSDAmount : String = ""
    var walletTTDAmount : String = ""
    var refreshControl = UIRefreshControl()
    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()

        btnDelete.isHidden = true
        if arrSelectedSections != nil {
            if arrSelectedSections.count > 0 {
                if arrSelectedSections.contains(1) {
                    btnDelete.isHidden = false
                }
            }
        }
        
        btnAddInvoice.isHidden = true
        getCartList(isShowLoader: true){status in
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Custom methods
    func configureControls()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords(_:)), name: Notification.Name("InvoiceDeleted"), object: nil)
        btnAddInvoice.layer.cornerRadius = 2
        btnAddInvoice.layer.masksToBounds = true
        tblCartList.estimatedRowHeight = 110
        tblCartList.rowHeight = UITableView.automaticDimension
        self.cnstViewFooterHeight.constant = 0
        tblCartList.dataSource = self
        tblCartList.delegate = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblCartList.addSubview(refreshControl)
        
        cnstTotalRecordsTitleHeight.constant = 0
        viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount)
        viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_INVOICE)
    }
    
    @objc func refresh()
    {
        self.arrSelectedSections.removeAll()
        btnDelete.isHidden = true
        getCartList(isShowLoader: false) { status in
        }
    }
    
    @objc func getLatestRecords(_ notification: NSNotification)
    {
        self.arrSelectedSections.removeAll()
        btnDelete.isHidden = true
        self.getCartList(isShowLoader: true){ status in
            if status == true
            {
                NotificationCenter.default.post(name: Notification.Name("CartUpdated"), object: nil) //new change
            }
        }
    }
    
    // MARK: - getCartList
    func getCartList(isShowLoader: Bool, completion:@escaping (Bool) -> Void)
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.CartList, showLoader:isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            if status == true
            {
                let objCart = ViewCart(fromJson: JSON(resultDict))
                self.totalUSDAmount = objCart.totalUsdAmount ?? "0.00"
                self.totalTTDAmount = objCart.totalTtdAmount ?? ""
                self.totalAmount.text = "\(kCurrencySymbol)\(objCart.totalUsdAmount ?? "0.00")"
                self.walletUSDAmount = objCart.walletUsd ?? ""
                self.walletTTDAmount = objCart.walletTtd ?? ""
                self.arrCartList = objCart.list ?? []
                for _ in 0..<self.arrCartList.count
                {
                    self.arrSelectedSections.append(0)
                }
                self.totalRecordsCount = self.arrCartList.count
                if self.arrCartList.count > 0
                {
                    self.tblCartList.isHidden = false
                    self.btnAddInvoice.isHidden = true
                    DispatchQueue.main.async {
                        self.cnstViewFooterHeight.constant = footerHeight
                        self.tblCartList.reloadData()
                    }
                }
                else
                {
                    self.cnstViewFooterHeight.constant = 0
                    self.tblCartList.isHidden = true
                    self.btnAddInvoice.isHidden = false
                    
                }
                
                if self.arrSelectedSections.filter { $0 == 1}.count > 0 {
                    self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
                }else {
                    self.cnstTotalRecordsTitleHeight.constant = 0
                }
                
                if self.totalRecordsCount > 0 {
                    self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
                }else {
                    self.cnstTotalRecordsTitleHeight.constant = 0
                }
                self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
                self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_INVOICE)

                completion(true)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                completion(false)
            }
        }
    }
    
    // MARK: - getInvoiceIDs
    func getInvoiceIDs() -> String
    {
        var strInvoiceID : String  = ""
        for (index,value) in arrSelectedSections.enumerated()
        {
            if value == 1 && arrCartList.count > 0
            {
                let objList = arrCartList[index]
                if let invoiceID = objList.invoiceId
                {
                    let invoiceID = String(invoiceID)
                    if strInvoiceID != ""
                    {
                        strInvoiceID += "," + invoiceID
                    }
                    else
                    {
                        strInvoiceID = invoiceID
                    }
                }
            }
        }
        return strInvoiceID
    }
    
    // MARK: - gotoInvoice
    func gotoInvoice()
    {
        if isFromVC == "MyInvoicesVC"
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if isFromVC == "Dashboard"
        {
            if let vc = appDelegate.getViewController("SegmentPageVC", onStoryboard: "Invoice") as? SegmentPageVC {
                vc.selectedIndex = 1
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    // MARK: - getAllInvoiceIDs
    func getAllInvoiceIDs() -> String
    {
        var strInvoiceID : String = ""
        for index in 0..<arrCartList.count
        {
            let objList = arrCartList[index]
            if let invoiceID = objList.invoiceId
            {
                let invoiceID = String(invoiceID)
                if strInvoiceID != ""
                {
                    strInvoiceID = strInvoiceID + "," + invoiceID
                }
                else
                {
                    strInvoiceID = invoiceID
                }
            }
        }
        return strInvoiceID
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnGoToInvoiceClicked()
    {
        gotoInvoice()
    }
    @IBAction func btnAddInvoiceClicked()
    {
        gotoInvoice()
    }
    @IBAction func btnPayClicked()
    {
        if let vc = appDelegate.getViewController("PayCheckoutVC", onStoryboard: "Referral") as? PayCheckoutVC {
            vc.totalUSDAmount = self.totalUSDAmount
            vc.totalTTDAmount = self.totalTTDAmount
            vc.invoiceIDs = getAllInvoiceIDs()
            vc.invoiceCount = arrCartList.count
            vc.walletUSDAmount = walletUSDAmount
            vc.walletTTDAmount = walletTTDAmount
            self.navigationController?.pushViewController(vc,animated:true)
        }
    }
    @IBAction func btnInfoClicked()
    {
        UIView.setAnimationsEnabled(true)
        if let popupVC = appDelegate.getViewController("InvoiceInfoVC", onStoryboard: "Invoice") as? InvoiceInfoVC {
            popupVC.isFrom = "ViewCart"
            popupVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.8)
            popupVC.modalPresentationStyle = .overFullScreen
            self.present(popupVC,animated: true, completion: nil)
            
        }
    }
    @IBAction func btnDeleteClicked()
    {
        let strInvoiceIDs = getInvoiceIDs()
        if let popupVC = appDelegate.getViewController("DeletePopupVC", onStoryboard: "PreAlert") as? DeletePopupVC {
            popupVC.view.backgroundColor =  UIColor(named: "theme_popup_bg_black") //Colors.theme_black_color.withAlphaComponent(0.8)
            popupVC.userID = userID
            popupVC.isFromVC = "ViewCartVC"
            popupVC.strInvoiceIDs = strInvoiceIDs
            popupVC.modalPresentationStyle = .overFullScreen
            self.present(popupVC,animated: true, completion: nil)
        }
    }
}
extension ViewCartVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCartList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ViewCartCell", for: indexPath) as? ViewCartCell {
            
            if arrCartList.count > 0 {
                let objCart = arrCartList[indexPath.row]
                cell.lblDate.text = objCart.date ?? ""
                cell.lblTotalUSDAmt.text = "\(kCurrencySymbol)\(objCart.totalUsd ?? "0.00")"
                cell.lblHAWNo.text = objCart.hawbNumber ?? ""
                cell.lblDesc.text = objCart.descriptionField ?? ""
                
                if let isDiscount = objCart.isDiscount, isDiscount == 1
                {
                    cell.lblDiscountAmt.text = "\(kCurrencySymbol)\(objCart.discountUsd ?? "0.00")"
                    cell.lblDiscountAmt.isHidden = false
                }
                else
                {
                    cell.lblDiscountAmt.isHidden = true
                }
                cell.imgCheck.tag = indexPath.row
                let imgtapGesture = UITapGestureRecognizer(target: self, action: #selector(imgTapped(sender:)))
                cell.imgCheck.addGestureRecognizer(imgtapGesture)
                cell.imgCheck.isUserInteractionEnabled = true
                
                if  arrSelectedSections[indexPath.row] == 1
                {
                    cell.contentView.backgroundColor = UIColor(named: "theme_lightblue_color") //Colors.theme_lightblue_color
                    cell.backgroundColor = UIColor(named: "theme_lightblue_color") //Colors.theme_lightblue_color
                    cell.imgCheck.image = UIImage(named:"checkbox")
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor(named: "theme_white_color") //Colors.theme_white_color
                    cell.backgroundColor = UIColor(named: "theme_white_color") //Colors.theme_white_color
                    cell.imgCheck.image = UIImage(named:"uncheckbox")
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    // MARK: - Custom Methods
    @objc func imgTapped(sender:UITapGestureRecognizer) //new change
    {
        let tag = sender.view!.tag
        if arrSelectedSections[tag] == 0
        {
            arrSelectedSections[tag] = 1
        }
        else
        {
            arrSelectedSections[tag] = 0
        }
        if arrSelectedSections.contains(1)
        {
            btnDelete.isHidden = false
        }
        else
        {
            btnDelete.isHidden = true
        }
        DispatchQueue.main.async {
            self.tblCartList.reloadData()
            self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
            self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_INVOICE)
        }
    }
}

class ViewCartCell : UITableViewCell
{
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTotalUSDAmt : UILabel!
    @IBOutlet weak var lblHAWNo : TapAndCopyLabel!
    @IBOutlet weak var lblDiscountAmt  : UILabel!
    @IBOutlet weak var lblDesc  : UILabel!
    @IBOutlet weak var imgCheck : UIImageView!
    override func awakeFromNib() {
    }
}
