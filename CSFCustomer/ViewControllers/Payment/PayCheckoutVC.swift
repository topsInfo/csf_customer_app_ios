//
//  CheckoutVC.swift
//  CSFCustomer
//
//  Created by Tops on 22/03/21.
//

import UIKit
import PassKit
import Stripe
import SwiftyJSON

class PayCheckoutVC: UIViewController, AddPartialAmountPopupDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var CartSummaryView : UIView!
    @IBOutlet weak var lblNoofInvoice : UILabel!
    @IBOutlet weak var lblTotalTTDAmount : UILabel!
    @IBOutlet weak var lblTotalUSDAmount : UILabel!
    
    @IBOutlet weak var PaymentMethodView : UIView!
    @IBOutlet weak var imgWallet : UIImageView!
    @IBOutlet weak var lblWalletUSDAmount : UILabel!
    @IBOutlet weak var btnWallet : UIButton!
    
    @IBOutlet weak var imgPaypal : UIImageView!
    @IBOutlet weak var imgCard : UIImageView!
    @IBOutlet weak var imgApplePay : UIImageView!
    @IBOutlet weak var scrollView : UIScrollView!
    
    //Nand : Partial payment changes
    
    @IBOutlet weak var viewWallet: UIView!
    @IBOutlet weak var viewPartialPayment : UIView!
    @IBOutlet weak var viewPartialPaymentYes : UIView!
    @IBOutlet weak var viewPartialPaymentAdded : UIView!
    @IBOutlet weak var lblPartialPaymentAmount : UILabel!
    @IBOutlet weak var stackViewWallateBalance : UIStackView!
    @IBOutlet weak var stackViewPaybleAmount : UIStackView!
    @IBOutlet weak var lblWalletBalance : UILabel!
    @IBOutlet weak var lblPayableAmount : UILabel!
    @IBOutlet weak var cnstPyamentMethodHeight : NSLayoutConstraint!
    @IBOutlet weak var cnstPartialPaymentViewHeight : NSLayoutConstraint!
    @IBOutlet weak var btnView : UIButton!
    @IBOutlet weak var viewClosePartialPayment : UIView!
    @IBOutlet weak var lblwantToAddPartialWalletAmount : UILabel!
    @IBOutlet weak var lblAdddedPartialAmount : UILabel!

    @IBOutlet weak var imgFreeze: UIImageView!
    @IBOutlet weak var lblFreezeTitle: UILabel!
    @IBOutlet weak var lblFreezeSubTitle: UILabel!
    @IBOutlet weak var stackViewWalletFreezeMsg: UIStackView!
    @IBOutlet weak var stackViewWalletFreezeSeperator: UIStackView!
    @IBOutlet weak var btnPay : UIButton!

    // MARK: - Global Variable
    //Nand : Partial payment changes
    var totalUSDAmount : String  = "0"
    var totalTTDAmount : String  = "0"
    var invoiceIDs : String = ""
    var invoiceCount : Int = 0
    var type : String = WALLET
    var walletUSDAmount : String = "0"
    var walletTTDAmount : String = "0"
    var stripeToken : String = ""
    
    //Nand : Partial payment changes
    var nSelectedType : Int = -1
    var partialPrice : Float = 0
    var isPartialPayementSelected : Bool = false // TRUE IF TWO PAYMENT METHOD IS SELECTED
    var isPartialPaymentOptionSelected : Bool = false // TRUE IF WALLET PARTIAL YES

    //Nand : Partial payment changes
    //Partial payment payment type constants for UI changes
    var TYPE_NONE: Int = -1
    var TYPE_WALLET: Int = 0
    var TYPE_PAYPAL: Int = 1
    var TYPE_CARD_PAYMENT: Int = 2
    var TYPE_APPLE_PAY: Int = 3
    
    //Nand : Partial payment changes
    //Partial payment payment option click types
    var CLICKED_DEFAULT : Int = 0
    var CLICKED_WALLET  : Int = 1
    var CLICKED_VIEW : Int = 2
    var CLICKED_APPLY  : Int = 3
    var CLICKED_REMOVE  : Int = 4
    
    //Partial payment wallet and payable amount variables
    var walletTTDBalance : String  = "0"
    var payableTTDAmount : String  = "0"
    
    //DriveThru Request
    var isFromDriveThruRequest : Bool = false
    var strInvoiceIdsForDriveThruRequest = ""
    var strPickupDateForDriveThruRequest = ""
    var strPickupTimeForDriveThruRequest = ""
    var strDateToDisplayForDriveThruRequest = ""
    var strTimeToDisplayForDriveThruRequest = ""

    //Freeze Wallet
    var objWalletFreeze = WalletFreeze()

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideViews()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Custom Methods
    func configureControls()
    {
        checkWalletFreeze()
        CartSummaryView.dropShadow()
        PaymentMethodView.dropShadow()

        viewPartialPaymentYes.isHidden = false
        viewPartialPaymentAdded.isHidden = true
        //Nand : Partial payment changes
        
        self.imgWallet.isHidden = true
        self.imgPaypal.isHidden = true
        self.imgCard.isHidden = true
        self.imgApplePay.isHidden = true
        
        //Wallet option enable / disable as per wallet amount
        if Float(walletTTDAmount)! > 1 {
            // Wallet Enable
            // Default wallet Selected
            
            //Nand : Partial payment changes
            lblWalletUSDAmount.text = "\(kTTCurrency) \(kCurrencySymbol)\(walletTTDAmount) / \(kCurrency) \(kCurrencySymbol)\(walletUSDAmount)"

            partialPrice = ((Float(walletTTDAmount)! <= Float(totalTTDAmount)!) ? Float(walletTTDAmount) : Float(totalTTDAmount))!
            isPartialPayementSelected = true
            
            btnWallet.isUserInteractionEnabled = true
            btnWallet.backgroundColor = Colors.clearColor
            imgWallet.isHidden = false
            nSelectedType = TYPE_WALLET
            btnView.isUserInteractionEnabled = true
        }
        else{
            // Wallet Disable
            // User have to do full payment.
            lblWalletUSDAmount.text = "\(kTTCurrency) \(kCurrencySymbol)\(0.00) / \(kCurrency) \(kCurrencySymbol)\(0.00)"

            btnWallet.isUserInteractionEnabled = false
            btnWallet.backgroundColor = UIColor(named: "theme_white_color")?.withAlphaComponent(0.5)
            imgWallet.isHidden = true
            btnView.isUserInteractionEnabled = false
            
            btnView.alpha = 0.5
            lblwantToAddPartialWalletAmount.alpha = 0.5
            lblAdddedPartialAmount.alpha = 0.5
        }
             
        if isFromDriveThruRequest {
            self.btnPay.setTitle("Pay & Porceed", for: .normal)
        }else{
            self.btnPay.setTitle("Pay", for: .normal)
        }
        setView(clickType: CLICKED_DEFAULT)
    }
    
    // MARK: - setView
    func setView(clickType : Int){
        // 0 = Not Any View Click, 1 = Wallet Click, 2 = Other View Click
        if (clickType == CLICKED_VIEW) {
            if isPartialPayementSelected && partialPrice >= Float(totalTTDAmount)! {
//                appDelegate.showToast(message: Messsages.msg_wallet_balance_sufficient, bottomValue: getSafeAreaValue())
                nSelectedType = TYPE_NONE
                return
            }
        }else if clickType == CLICKED_REMOVE || clickType == CLICKED_APPLY || clickType == CLICKED_WALLET {
            if isPartialPayementSelected && partialPrice >= Float(totalTTDAmount)! {
                nSelectedType = TYPE_NONE
            }else if clickType == CLICKED_WALLET && partialPrice >= Float(totalTTDAmount)! {
                nSelectedType = TYPE_NONE
            }
        }else {
            nSelectedType = TYPE_NONE
        }
        
        imgWallet.isHidden = isPartialPayementSelected ? false : true
        imgPaypal.isHidden = nSelectedType == TYPE_PAYPAL ? false : true
        imgCard.isHidden = nSelectedType == TYPE_CARD_PAYMENT ? false : true
        imgApplePay.isHidden = nSelectedType == TYPE_APPLE_PAY ? false : true
        
        setPriceView()
    }
    
    // MARK: - setPriceView
    func setPriceView(){
        
        lblNoofInvoice.text = String(invoiceCount)
        lblTotalTTDAmount.text = "\(kCurrencySymbol)\(totalTTDAmount)"
        lblTotalUSDAmount.text = "\(kCurrencySymbol)\(totalUSDAmount)"

        walletTTDBalance = String(format: "%.2f", partialPrice)
        
        if isPartialPayementSelected {
            payableTTDAmount = String(format: "%.2f", (Float(totalTTDAmount)! - partialPrice))
        }else {
            payableTTDAmount = String(format: "%.2f", (Float(totalTTDAmount)!))
        }
        
        self.lblWalletBalance.text = String(format: "- $%@", walletTTDBalance)
        self.lblPayableAmount.text = String(format: "$%@", payableTTDAmount)
        
        if isPartialPayementSelected {
            stackViewWallateBalance.isHidden = false
            stackViewPaybleAmount.isHidden = false
        }else{
            stackViewWallateBalance.isHidden = true
            stackViewPaybleAmount.isHidden = true
        }
    }
    
    // MARK: - Hide Show Views
    func hideViews(){
        btnPay.alpha = 0
        CartSummaryView.alpha = 0
        PaymentMethodView.alpha = 0
    }
   
    func showViews(){
        btnPay.alpha = 1
        CartSummaryView.alpha = 1
        PaymentMethodView.alpha = 1
    }
    
    // MARK: - callAPIForWalletCheckout
    func callAPIForWalletCheckout()
    {
        let objUserInfo = getUserInfo()
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"]  = self.invoiceIDs
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.WalletCheckout, showLoader:true) { (resultDict, status, message) in
            if status == true
            {                
                if self.isFromDriveThruRequest {
                    //Add drive thru request
                    if !self.strInvoiceIdsForDriveThruRequest.isEmpty {
                        
                        let objUserInfo = getUserInfo()
                        self.createDriveThruRequest(userId: objUserInfo.id ?? "", invoiceId: self.strInvoiceIdsForDriveThruRequest, pickupDate: self.strPickupDateForDriveThruRequest , pickupTime: self.strPickupTimeForDriveThruRequest,selectedDateToDisplay: self.strDateToDisplayForDriveThruRequest, selectedTimeToDisplay: self.strTimeToDisplayForDriveThruRequest,type: 2, resultDate: resultDict, fromVC: self)
                    }
                }else {
                    if let vc = appDelegate.getViewController("PaymentSuccessVC", onStoryboard: "Invoice") as? PaymentSuccessVC {
                        vc.dict = resultDict
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - Show PaypalVC
    func showPaypalVC()
    {
        if let vc = appDelegate.getViewController("PaypalPaymentVC", onStoryboard: "Referral") as? PaypalPaymentVC {
            vc.invoiceIDs = self.invoiceIDs
            
            //Nand : Partial payment changes
            vc.partialPrice = self.partialPrice
            vc.isPartialPayementSelected = self.isPartialPayementSelected
            vc.isPartialPaymentOptionSelected = self.isPartialPaymentOptionSelected
            
            //DriveThru Request
            vc.isFromDriveThruRequest = self.isFromDriveThruRequest
            vc.strInvoiceIdsForDriveThruRequest = self.strInvoiceIdsForDriveThruRequest
            vc.strPickupDateForDriveThruRequest = self.strPickupDateForDriveThruRequest
            vc.strPickupTimeForDriveThruRequest = self.strPickupTimeForDriveThruRequest
            vc.strDateToDisplayForDriveThruRequest = self.strDateToDisplayForDriveThruRequest
            vc.strTimeToDisplayForDriveThruRequest = self.strTimeToDisplayForDriveThruRequest

            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - Show Card PaymentVC
    func showCardPaymentVC()
    {
        if let vc = appDelegate.getViewController("CardPaymentVC", onStoryboard: "Referral") as? CardPaymentVC {
            vc.invoiceIDs = self.invoiceIDs
            vc.totalUSDAmt = self.totalUSDAmount
            vc.totalTTDAmt = self.totalTTDAmount
            vc.type = "1"
            
            //Nand : Partial payment changes
            vc.partialPrice = self.partialPrice
            vc.isPartialPayementSelected = self.isPartialPayementSelected
            vc.isPartialPaymentOptionSelected = self.isPartialPaymentOptionSelected
            vc.payableTTDAmount = self.isPartialPayementSelected ? self.payableTTDAmount : "0"
            
            //DriveThru Request
            vc.isFromDriveThruRequest = self.isFromDriveThruRequest
            vc.strInvoiceIdsForDriveThruRequest = self.strInvoiceIdsForDriveThruRequest
            vc.strPickupDateForDriveThruRequest = self.strPickupDateForDriveThruRequest
            vc.strPickupTimeForDriveThruRequest = self.strPickupTimeForDriveThruRequest
            vc.strDateToDisplayForDriveThruRequest = self.strDateToDisplayForDriveThruRequest
            vc.strTimeToDisplayForDriveThruRequest = self.strTimeToDisplayForDriveThruRequest
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - Show Apple Pay View
    func showApplePayView()
    {
//        let paymentNetworks = [PKPaymentNetwork.amex, .discover, .masterCard, .visa]
//        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks)
//        {
            let request = PKPaymentRequest()
            request.merchantIdentifier = "merchant.com.csfcustomer"
            request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
            request.merchantCapabilities = PKMerchantCapability.capability3DS
//            request.countryCode = "US"
//            request.currencyCode = "USD"
            request.countryCode = "US"
            request.currencyCode = "TTD"
          //  request.requiredShippingAddressFields = PKAddressField.all
//            let USDAmount = Decimal(string: self.totalUSDAmount)

        let USDAmount = Decimal(string: payableTTDAmount)
            if USDAmount! > 0
            {
                request.paymentSummaryItems = [
                    PKPaymentSummaryItem(label: "CSF Couriers Ltd.", amount:USDAmount! as NSDecimalNumber)
                ]
                
                let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
                applePayController?.delegate = self
                self.present(applePayController!, animated: true, completion: nil)
            }
            else
            {
                showAlert(title: "", msg: Messsages.msg_invalid_usdamount, vc: self)
            }

    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func btnInfoClicked()
//    {
//        let msg : String = self.objWalletFreeze.walletFreezeReason
//        appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue(), height: 80, alignmentType: 0)
//    }
    
    @IBAction func btnPaymentMethodClicked(_ sender:UIButton){
        
        if sender.tag == 101 {
            //Wallet
            isPartialPayementSelected = !isPartialPayementSelected
            
            if isPartialPaymentOptionSelected {
                onClosePartialPaymentOption()
            }else{
                setView(clickType: CLICKED_WALLET)
            }
        }
        
        if sender.tag == 102 {
            //Paypal
            if isPartialPayementSelected && partialPrice >= Float(totalTTDAmount)! {
                isPartialPayementSelected = !isPartialPayementSelected
                setView(clickType: CLICKED_WALLET)
                
                nSelectedType = TYPE_PAYPAL
                setView(clickType: CLICKED_VIEW)
            }else{
                nSelectedType = TYPE_PAYPAL
                setView(clickType: CLICKED_VIEW)
            }
        }

        if sender.tag == 103 {
            //Card
            if isPartialPayementSelected && partialPrice >= Float(totalTTDAmount)! {
                isPartialPayementSelected = !isPartialPayementSelected
                setView(clickType: CLICKED_WALLET)
                
                nSelectedType = TYPE_CARD_PAYMENT
                setView(clickType: CLICKED_VIEW)
            }else{
                nSelectedType = TYPE_CARD_PAYMENT
                setView(clickType: CLICKED_VIEW)
            }
        }

        if sender.tag == 104 {
            //Card
            if isPartialPayementSelected && partialPrice >= Float(totalTTDAmount)! {
                isPartialPayementSelected = !isPartialPayementSelected
                setView(clickType: CLICKED_WALLET)
                
                nSelectedType = TYPE_APPLE_PAY
                setView(clickType: CLICKED_VIEW)
            }else{
                nSelectedType = TYPE_APPLE_PAY
                setView(clickType: CLICKED_VIEW)
            }
        }
    }
    
    @IBAction func btnPayClicked()
    {
        if isValidated() {
            
//            let strPrams = "validated :: Type : \(nSelectedType) :: isPartialPayementSelected :: \(isPartialPayementSelected) :: isPartialPaymentOptionSelected :: \(isPartialPaymentOptionSelected) :: partialPrice :: \(self.partialPrice) :: totalTTDAmount :: \(totalTTDAmount) :: walletTTDAmount ::  \(walletTTDAmount) :: payableTTDAmount :: \(payableTTDAmount) walletTTDBalance :: \(walletTTDBalance)"
//
//
//            print("validated :: Type : \(nSelectedType) :: isPartialPayementSelected :: \(isPartialPayementSelected) :: isPartialPaymentOptionSelected :: \(isPartialPaymentOptionSelected) :: partialPrice :: \(self.partialPrice) :: totalTTDAmount :: \(totalTTDAmount) :: walletTTDAmount ::  \(walletTTDAmount) :: payableTTDAmount :: \(payableTTDAmount) walletTTDBalance :: \(walletTTDBalance)")
//
////            showAlert(title: "", msg: strPrams, vc: self)
            
            if nSelectedType == TYPE_WALLET
            {
                //print("callAPIForWalletCheckout")
                callAPIForWalletCheckout()
            }
            else if nSelectedType == TYPE_PAYPAL
            {
                //print("showPaypalVC")
                showPaypalVC()
            }
            else if nSelectedType  == TYPE_CARD_PAYMENT
            {
                //print("showCardPaymentVC")
                showCardPaymentVC()
            }
            else if nSelectedType == TYPE_APPLE_PAY
            {
                //print("showApplePayView")
                showApplePayView()
                
            }
            else if type == ""
            {
                self.showAlert(title: "", msg: Messsages.msg_select_payment_method, vc: self)
            }
        }
    }
    
    //Nand : Partial payment changes
    @IBAction func btnPartialPaymentYesClicked(_ sender:UIButton)
    {
        //print("btnPartialPaymentYesClicked")
        openAddPartialAmountVC()
    }
    
    @IBAction func btnPartialPaymentAddedCloseClicked(_ sender:UIButton)
    {
        onClosePartialPaymentOption()
    }
    
    func onClosePartialPaymentOption(){
        //print("btnPartialPaymentAddedCloseClicked")
        isPartialPaymentOptionSelected = false
        partialPrice = ((Float(walletTTDAmount)! <= Float(totalTTDAmount)!) ? Float(walletTTDAmount) : Float(totalTTDAmount))!
        viewPartialPaymentYes.isHidden = false
        viewPartialPaymentAdded.isHidden = true
        stackViewWallateBalance.isHidden = true
        stackViewPaybleAmount.isHidden = true
        setView(clickType: CLICKED_REMOVE)
    }
    
    func openAddPartialAmountVC(){
        UIView.setAnimationsEnabled(true)
        if let addPartialAmountVC = appDelegate.getViewController("AddPartialAmountVC", onStoryboard: "Referral") as? AddPartialAmountVC{
            addPartialAmountVC.modalPresentationStyle = .overFullScreen
            addPartialAmountVC.addPartialAmountDelegate = self
            addPartialAmountVC.totalTTDAmount = self.totalTTDAmount
            addPartialAmountVC.walletTTDAmount = self.walletTTDAmount
            self.present(addPartialAmountVC,animated: true, completion: nil)
        }
    }
    
    func getAmount(amount:String)
    {
        //print("Amount is \(amount)")
        self.partialPrice = Float(amount)!
        isPartialPayementSelected = true
        isPartialPaymentOptionSelected = true
        self.lblPartialPaymentAmount.text = String(format: "%.2f TTD", self.partialPrice)
        viewPartialPaymentYes.isHidden = true
        viewPartialPaymentAdded.isHidden = false
        setView(clickType: CLICKED_APPLY)
    }
    
    // MARK: - isValidated
    func isValidated() -> Bool{

        if !isPartialPayementSelected && nSelectedType < 0 {
            showAlert(title: "", msg: Messsages.msg_please_select_payment_method_checkout, vc: self)
            return false
        }
        
        if isPartialPayementSelected && nSelectedType < 0 {
            if isPartialPaymentOptionSelected {
                if Float(totalTTDAmount)! > partialPrice {
                    showAlert(title: "", msg: Messsages.msg_please_select_partial_payment_method_checkout, vc: self)
                    return false
                }
            }else{
                if Float(totalTTDAmount)! > Float(walletTTDAmount)! {
                    showAlert(title: "", msg: Messsages.msg_wallet_balance_not_sufficient, vc: self)
                    return false
                }
            }
            nSelectedType = TYPE_WALLET
        }
        return true
    }
}
extension PayCheckoutVC: PKPaymentAuthorizationViewControllerDelegate {
        func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        self.dismiss(animated: true, completion: nil)
        
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        // Send the payment object info to Stripe to get the token
        STPAPIClient.shared.createToken(with: payment) { (token, error) in
            if error != nil {
                self.showAlert(title: "", msg: "error", vc: self)
               // print("There is an error")
            }
            else {
              //  print(token)
                self.stripeToken = token!.tokenId
                self.type = "3"
                self.callAPIForApplePay()
            }
        }
        completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.success, errors: []))
    }
    
    // MARK: - Call API For ApplePay
    func callAPIForApplePay()
    {
        var params : [String:Any] = [:]
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"]  = invoiceIDs
        params["token"] = self.stripeToken
        params["type"] = self.type
        
        //Nand : Partial payment changes
        params["is_partial"]  = self.isPartialPayementSelected
        params["partial_wallet"]  = self.isPartialPaymentOptionSelected
        params["partial_wallet_ttd"]  = self.partialPrice
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.StripePayment, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                if self.isFromDriveThruRequest {
                    //Add drive thru request
                    if !self.strInvoiceIdsForDriveThruRequest.isEmpty {
                        
                        let objUserInfo = getUserInfo()
                        self.createDriveThruRequest(userId: objUserInfo.id ?? "", invoiceId: self.strInvoiceIdsForDriveThruRequest, pickupDate: self.strPickupDateForDriveThruRequest, pickupTime: self.strPickupTimeForDriveThruRequest, selectedDateToDisplay: self.strDateToDisplayForDriveThruRequest, selectedTimeToDisplay: self.strTimeToDisplayForDriveThruRequest,type: 2, resultDate: resultDict, fromVC: self)
                    }
                }else{
                    if let vc = appDelegate.getViewController("PaymentSuccessVC", onStoryboard: "Invoice") as? PaymentSuccessVC {
                        vc.dict = resultDict
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
        
    }
    
    // MARK: - Hide Show Wallet Freeze Views
    func hideWalletFreezeViews(){
        self.stackViewWalletFreezeMsg.isHidden = true
        self.stackViewWalletFreezeSeperator.isHidden = true
        
        self.viewWallet.isHidden = false
        self.viewPartialPayment.isHidden = false
    }
    
    func showWalletFreezeViews(){
        self.stackViewWalletFreezeMsg.isHidden = false
        self.stackViewWalletFreezeSeperator.isHidden = false
        
        self.viewWallet.isHidden = true
        self.viewPartialPayment.isHidden = true
    }
}
extension PayCheckoutVC {
    
    // MARK: - checkWalletFreeze
    func checkWalletFreeze()
    {
        let objUserInfo = getUserInfo()

        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""

        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.checkWalletFreez, showLoader: true) { [self] (resultDict, status, message) in
            
            self.showViews()
            
            if status == true
            {
                self.objWalletFreeze = WalletFreeze(fromJson: JSON(resultDict))
                print("isWalletFreeze is \(String(describing: self.objWalletFreeze.isWalletFreeze))")

                if self.objWalletFreeze.isWalletFreeze {
                    self.lblFreezeSubTitle.text = message
                    self.showWalletFreezeViews()
                    self.isPartialPayementSelected = false
                    self.isPartialPaymentOptionSelected = false
                    self.payableTTDAmount = "0"
                    
                    self.setView(clickType: CLICKED_DEFAULT)
                }else {
                    self.hideWalletFreezeViews()
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}
