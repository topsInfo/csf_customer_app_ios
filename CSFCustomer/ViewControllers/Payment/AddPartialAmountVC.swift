//
//  AddPartialAmountVC.swift
//  CSFCustomer
//
//  Created by Tops on 27/10/21.
//

import UIKit
protocol AddPartialAmountPopupDelegate {
    func getAmount(amount:String)
}

class AddPartialAmountVC: UIViewController, UITextFieldDelegate {
    // MARK: - IBOutlets
    @IBOutlet weak var tblAddPartialAmount : UITableView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var dimmerView: UIView!
    @IBOutlet weak var txtAmount : UITextField!
    @IBOutlet weak var viewAddPartialAmountBox: UIView!
    @IBOutlet weak var btnSubmit : UIButton!
    
    // MARK: - Global Variable
    var addPartialAmountDelegate : AddPartialAmountPopupDelegate?
    var walletTTDAmount : String = "0"
    var totalTTDAmount : String = "0"
    var partialPrice : Float = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        showView()
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black") //UIColor.black.withAlphaComponent(0.5)
        configureTextFields()
        txtAmount.keyboardDistanceFromTextField = 100
        txtAmount.paddingView(xvalue: 10)
        txtAmount.delegate  = self
        tblAddPartialAmount.tableFooterView = UIView()
        btnSubmit.layer.cornerRadius = 2.0
        btnSubmit.layer.masksToBounds = true
    }
    
    // MARK: - Show View
    func showView()
    {
        self.view.layoutIfNeeded()
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - 330
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
    }
    
    // MARK: - configureTextFields
    func configureTextFields()
    {
        configureTextField(textField: txtAmount, placeHolder: "Amount TTD", font: fontname.openSansRegular, fontSize: 14, textColor:  UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
    }
    
    // MARK: - IBAction methods
    @IBAction func btnSubmitClicked(_ sender:UIButton)
    {
        self.view.endEditing(true)
        //print("btnSubmitClicked")
        if txtAmount.text == "" {
            partialPrice = 0
        }else {
            partialPrice = Float(txtAmount.text!)!
        }
        
        if isValidated(){
            self.dismiss(animated: true) { [self] in
                addPartialAmountDelegate?.getAmount(amount: self.txtAmount.text!)
            }
        }
    }
    
    @IBAction func btnCloseClicked(_ sender:UIButton)
    {
        //print("btnCloseClicked")
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Validation
    func isValidated() -> Bool{
        
        if partialPrice <= 0 {
            showAlert(title: "", msg: Messsages.msg_enter_partial_amount, vc: self)
            return false
        }
        
        if partialPrice > Float(walletTTDAmount)! {
            showAlert(title: "", msg: Messsages.msg_enter_amount_should_not_be_greater_than_wallet_amount, vc: self)
            return false
        }
        
        if partialPrice > Float(totalTTDAmount)! {
            showAlert(title: "", msg: Messsages.msg_wallet_amount_should_not_be_greater_than_total_amount, vc: self)
            return false
        }
        return true
    }
    
    // MARK: - UITextfiled delegate methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let r = Range(range, in: text)
        let newText = text.replacingCharacters(in: r!, with: string)
        let newLength = text.count + string.count - range.length

        if textField == txtAmount {
            if newText.contains(".")
            {
                let isNumeric = newText.isEmpty || (Double(newText) != nil)
                let numberOfDots = newText.components(separatedBy: ".").count - 1
            
                let numberOfDecimalDigits: Int
                if let dotIndex = newText.firstIndex(of: ".") {
                    numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
                } else {
                    numberOfDecimalDigits = 0
                }
                return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
            }
            else
            {
                return newLength <= 5
            }
        }
        return true
    }
}
