//
//  PaymentSuccessVC.swift
//  CSFCustomer
//
//  Created by Tops on 04/01/21.
//

import UIKit

class PaymentSuccessVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblTransactionNo : UILabel!
    @IBOutlet weak var lblInvoiceDt : UILabel!
    @IBOutlet weak var lblTotalAmount : UILabel!
   
    @IBOutlet weak var transStackview : UIStackView!
    @IBOutlet weak var lblSeparator : UILabel!
    @IBOutlet weak var paymentStackview : UIStackView!
    @IBOutlet weak var totalPaidStackview : UIStackView!
    @IBOutlet weak var btnDone : UIButton!
    
    // MARK: - Global Variable
    var dict : [String:Any] = [String:Any]()
    var isFrom : String  = ""
    var TTDAmount : String = ""
    var timer : Timer?
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        if isFrom == "AddMoney"
        {
            hideControls()
            lblTitle.text = "Transaction Successful"
            lblTransactionNo.text = "\(kCurrencySymbol)\(TTDAmount) \(kCurrency) has been credited to your wallet."
            timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(PaymentSuccessVC.timerEventCalled), userInfo: nil, repeats: false)
        }
        else
        {
            showControls()
            lblTitle.text = "Payment Successful"
            if let transID = (dict as NSDictionary).value(forKey: "transaction_id") as? String
            {
                lblTransactionNo.text = transID
            }
            if let totalAmount = (dict as NSDictionary).value(forKey: "tota_amount") as? String
            {
                lblTotalAmount.text = "\(kCurrencySymbol) \(totalAmount)  \(kCurrency)"
            }
            if let invoiceDt = (dict as NSDictionary).value(forKey: "date") as? String
            {
                lblInvoiceDt.text = invoiceDt
            }
            //Nand : Payment detail changes
            if let totalAmountTTD = (dict as NSDictionary).value(forKey: "tota_amount_ttd") as? String
            {                
                if let totalAmount = (dict as NSDictionary).value(forKey: "tota_amount") as? String
                {
                    lblTotalAmount.text = "\(kCurrencySymbol)\(totalAmountTTD) \(kTTCurrency)/ \(kCurrencySymbol)\(totalAmount) \(kCurrency)"
                }
            }
        }
    }
    
    // MARK: - Show Hide Controls
    func hideControls()
    {
        transStackview.isHidden = true
        lblSeparator.isHidden = true
        paymentStackview.isHidden = true
        totalPaidStackview.isHidden = true
        btnDone.isHidden = true
    }
    func showControls()
    {
        transStackview.isHidden = false
        lblSeparator.isHidden = false
        paymentStackview.isHidden = false
        totalPaidStackview.isHidden = false
        btnDone.isHidden = false
    }
    @objc func timerEventCalled()
    {
        timer?.invalidate()
        timer = nil
        gotoWalletVC()
    }
    
    // MARK: - IBAction methods
    @IBAction func btnDoneClicked()
    {
        NotificationCenter.default.post(name: Notification.Name("CartUpdated"), object: nil)
       // NotificationCenter.default.post(name: Notification.Name("PaymentSuccessful"), object: nil)
        goToHomeVC()
    }
}
