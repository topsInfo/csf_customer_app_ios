//
//  PaymentDiscountVC.swift
//  CSFCustomer
//
//  Created by iMac on 09/02/23.
//

import UIKit

class PaymentDiscountVC: UIViewController {

    // MARK: - Global Variables
    var objCart : CartList = CartList()
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblTotalAmountUsd  : UILabel!
    @IBOutlet weak var lblDiscountAmountUsd  : UILabel!
    @IBOutlet weak var lblPayableAmountUsd  : UILabel!
    
    @IBOutlet weak var lblTotalAmountTtd  : UILabel!
    @IBOutlet weak var lblDiscountAmountTtd  : UILabel!
    @IBOutlet weak var lblPayableAmountTtd  : UILabel!
    @IBOutlet weak var lblHawbNumber  : TapAndCopyLabel!
    @IBOutlet weak var topConst : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        showCard()

        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black") //UIColor.black.withAlphaComponent(0.5)

//        self.lblTitle.text = "Discount"
        self.lblTotalAmountUsd.text = objCart.beforeDiscountUsd ?? "0"
        self.lblDiscountAmountUsd.text = objCart.discountUsd ?? "0"
        self.lblPayableAmountUsd.text = objCart.totalUsd ?? "0"
        
        self.lblTotalAmountTtd.text = objCart.beforeDiscountTtd ?? "0"
        self.lblDiscountAmountTtd.text = objCart.discountTtd ?? "0"
        self.lblPayableAmountTtd.text = objCart.totalTtd ?? "0"
        self.lblHawbNumber.text = objCart.hawbNumber ?? ""
    }
    
    func showCard()
    {
        self.view.layoutIfNeeded()
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - 250
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }

}
