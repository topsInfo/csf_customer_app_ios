//
//  TrackOrderSearchVC.swift
//  CSFCustomer
//
//  Created by Tops on 08/02/21.
//

import UIKit
import SwiftyJSON
class TrackOrderSearchVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    
    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    var strSearchText : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    /**
     Configures the controls in the TrackOrderSearchVC view controller.
     
     This function is responsible for setting up and configuring the various controls used in the TrackOrderSearchVC view controller.
     It should be called after the view controller is loaded and before it is presented to the user.
     */
    func configureControls()
    {
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.8)
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - 300
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
        objUserInfo = getUserInfo()
        txtSearch.text = strSearchText
        txtSearch.paddingView(xvalue: 10)
    }
    
    // MARK: - callAPIForTrackOrder
    /**
     Calls the API to track an order.
     
     This function is responsible for making the API call to track an order. It retrieves the necessary data from the server and updates the UI accordingly.
     */
    func callAPIForTrackOrder()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["tracking_number"] = txtSearch.text
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.TrackOrder, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                let objTrackOrder = TrackOrder(fromJson: JSON(resultDict))
                let filterDict :[String: Any] = [
                    "TrackingObject" : objTrackOrder ,
                ]
                NotificationCenter.default.post(name: Notification.Name("TrackOrderSearch"), object: nil,userInfo: filterDict)
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
               appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - isValidData
    /**
        Checks if the data entered in the text field is valid.

        - Returns: A boolean value indicating whether the data is valid or not.
    */
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtSearch.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_trackorder, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    
    // MARK: - IBAction methods
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSearchClicked()
    {
        if isValidData()
        {
            callAPIForTrackOrder()
        }
    }
}
