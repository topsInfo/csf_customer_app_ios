//
//  TrackOrderVC.swift
//  CSFCustomer
//
//  Created by Tops on 04/02/21.
//

import UIKit
import SwiftyJSON

class TrackOrderVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var txtSearch : UITextField!
    
    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls() {
        objUserInfo = getUserInfo()
        txtSearch.keyboardDistanceFromTextField = 100
        txtSearch.paddingView(xvalue: 10)
    }
    
    // MARK: - callAPIForTrackOrder
    /// Calls the API to track an order.
    func callAPIForTrackOrder() {
        var params: [String: Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["tracking_number"] = txtSearch.text
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.TrackOrder, showLoader: true) { (resultDict, status, message) in
            if status == true {
                let objTrackOrder = TrackOrder(fromJson: JSON(resultDict))
                if let vc = appDelegate.getViewController("OrderDetailsVC", onStoryboard: "Home") as? OrderDetailsVC {
                    vc.objTrackOrder = objTrackOrder
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - isValidData
    /**
        Checks if the data entered is valid.
     
        - Returns: A boolean value indicating whether the data is valid or not.
    */
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtSearch.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_trackorder, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSearchClicked()
    {
        self.view.endEditing(true)
        if isValidData() {
            callAPIForTrackOrder()
        }
    }
}
