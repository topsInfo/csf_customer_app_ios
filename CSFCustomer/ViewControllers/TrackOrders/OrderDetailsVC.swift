//
//  OrderDetailsVC.swift
//  CSFCustomer
//
//  Created by Tops on 05/02/21.
//

import UIKit

class OrderDetailsVC: UIViewController,UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var HAWNo : TapAndCopyLabel!
    @IBOutlet weak var lblCreatedDt : UILabel!
    @IBOutlet weak var lblCreatedBy : UILabel!
    @IBOutlet weak var lblOrderStatus : UILabel!
    
    @IBOutlet weak var lblAddress1 : TapAndCopyLabel!
    @IBOutlet weak var lblAddress2 : TapAndCopyLabel!
    @IBOutlet weak var lblCompanyName : UILabel!
    @IBOutlet weak var lblShipperName : TapAndCopyLabel!
    
    @IBOutlet weak var lblMemberName : TapAndCopyLabel!
    @IBOutlet weak var lblMemberAddress : TapAndCopyLabel!
    @IBOutlet weak var lblMemberEmail : TapAndCopyLabel!
    @IBOutlet weak var btnClaim : UIButton!
    
    @IBOutlet weak var lblTotalPcs : UILabel!
    @IBOutlet weak var lblTotalWeight : UILabel!
    
    @IBOutlet weak var tblTrackingDetail : UITableView!
    @IBOutlet weak var tblPackageDetail : UITableView!
    @IBOutlet weak var packageFooterView : UIView!
    @IBOutlet weak var heightConstTblTracking : NSLayoutConstraint!
    @IBOutlet weak var heightConstTblPackaging : NSLayoutConstraint!
    @IBOutlet weak var lblNoLogsFound : UILabel!
    
    // MARK: - Global Variable
    var objTrackOrder = TrackOrder()
    var objUserInfo = UserInfo()
    var arrLogs : [Log] = [Log]()
    var strTrackingNo : String = ""
    var arrPackageDetail : [PackageDetail] =  [PackageDetail]()
    var packageAmt : Float = 0
    
    // MARK: - Viewcontroller life cycle    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
        
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    /**
     Configures the controls in the OrderDetailsVC.
     */
    func configureControls()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(getFilterRecords(_:)), name: NSNotification.Name(rawValue: "TrackOrderSearch"), object: nil)
        
        tblTrackingDetail.estimatedRowHeight = 110
        tblTrackingDetail.rowHeight = UITableView.automaticDimension
        tblTrackingDetail.dataSource = self
        tblTrackingDetail.delegate = self
        
        tblPackageDetail.estimatedRowHeight = 50
        tblPackageDetail.rowHeight = UITableView.automaticDimension
        tblPackageDetail.dataSource = self
        tblPackageDetail.delegate = self
        lblNoLogsFound.isHidden = true
        objUserInfo = getUserInfo()
        showData()
    }
    
    // MARK: - showData
    /**
     Displays the data for the order details on the screen.
     */
    func showData() {
        // Retrieve the tracking number, HAWB number, created date, created by, and order status from the `objTrackOrder` object
        strTrackingNo = objTrackOrder.trackingNumber ?? ""
        HAWNo.text = objTrackOrder.hawbNumber ?? ""
        lblCreatedDt.text = objTrackOrder.createdAt ?? ""
        lblCreatedBy.text = objTrackOrder.createdBy ?? ""
        lblOrderStatus.text = objTrackOrder.status ?? ""
        
        // Retrieve the company name and address from the `objTrackOrder` object
        lblCompanyName.text = objTrackOrder.csfCompanyName ?? ""
        lblAddress1.text = objTrackOrder.csfAddress1 ?? ""
        lblAddress2.text = objTrackOrder.csfAddress2 ?? ""
        
        // Retrieve the shipper name, member name, member address, and member email from the `objTrackOrder` object
        lblShipperName.text = objTrackOrder.shipperName ?? ""
        lblMemberName.text = objTrackOrder.memberName ?? ""
        lblMemberAddress.text = objTrackOrder.memberAddress ?? ""
        lblMemberEmail.text = objTrackOrder.memberEmail ?? ""
        
        // Retrieve the total quantity and total weight from the `objTrackOrder` object
        lblTotalPcs.text =  String(objTrackOrder.totalQty ?? 0)
        lblTotalWeight.text = objTrackOrder.totalWeight ?? ""
        
        // Check if the claim order status is 0, and hide the claim button accordingly
        if let claimStatus = objTrackOrder.claimOrder, claimStatus == 0 {
            btnClaim.isHidden = true
        } else {
            btnClaim.isHidden = false
        }
        
        // Retrieve the logs from the `objTrackOrder` object
        arrLogs = objTrackOrder.logs ?? []
        
        // Adjust the table view height and visibility based on the number of logs
        if arrLogs.count == 0 {
            heightConstTblTracking.constant = 100
            lblNoLogsFound.isHidden = false
            tblTrackingDetail.isHidden = true
        } else if arrLogs.count > 0 && arrLogs.count <= 2 {
            heightConstTblTracking.constant = CGFloat((arrLogs.count * 110))
            tblTrackingDetail.isHidden = false
            lblNoLogsFound.isHidden = true
        } else {
            heightConstTblTracking.constant = CGFloat((2 * 110))
            tblTrackingDetail.isHidden = false
            lblNoLogsFound.isHidden = true
        }
        
        // Retrieve the package details from the `objTrackOrder` object
        arrPackageDetail = objTrackOrder.pacakgeDetails ?? []
        
        // Adjust the table view height based on the number of package details
        if arrPackageDetail.count > 0 && arrPackageDetail.count < 4 {
            heightConstTblPackaging.constant =  CGFloat(100 + (arrPackageDetail.count * 50))
        } else {
            heightConstTblPackaging.constant = CGFloat(100 + (4 * 50))
        }
        
        // Reload the table views on the main thread
        DispatchQueue.main.async {
            self.tblTrackingDetail.reloadData()
            self.tblPackageDetail.reloadData()
        }
    }
    
    /**
     This method is called when a notification is received to get filter records.
     
     - Parameter notification: The notification object containing the tracking object.
     */
    @objc func getFilterRecords(_ notification : Notification)
    {
        self.objTrackOrder = notification.userInfo?["TrackingObject"] as! TrackOrder
        showData()
    }
    
    // MARK: - getPackageAmount
    /**
     This function is used to display an alert controller with a text field for the user to enter a package amount. It also validates the entered amount and performs actions based on the entered value.
     */
    func getPackageAmount()
    {
        let attributedString = NSAttributedString(string: "Enter Claimed Package Amount", attributes: [
            NSAttributedString.Key.font : UIFont.init(name: fontname.openSansBold, size: 14) as Any, //your font here
            NSAttributedString.Key.foregroundColor :Colors.theme_orange_color
        ])
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alertController.setValue(attributedString, forKey: "attributedTitle")
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Package Amount"
            textField.keyboardType = .decimalPad
            textField.delegate = self
        }
        let doneAction = UIAlertAction(title: "Done", style: .default, handler: { alert -> Void in
            let txtAmount = alertController.textFields![0] as UITextField
            let amount = Float(txtAmount.text ?? "0")
            if amount == 0 || amount == nil
            {
                self.showAlert(title: "", msg: Messsages.msg_invalid_amount, vc: self)
            }
            else
            {
                self.packageAmt = amount!
                self.showAlertForClaim(title: "",msg: Messsages.msg_claim)
            }
        })
        doneAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alertController.addAction(doneAction)
        
        alertController.view.backgroundColor = Colors.theme_white_color
        alertController.view.layer.cornerRadius = 10
        alertController.view.layer.masksToBounds = true
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - showAlertForClaim
    /**
     Shows an alert with the specified title and message, allowing the user to claim a package.
     
     - Parameters:
        - title: The title of the alert.
        - msg: The message displayed in the alert.
     */
    func showAlertForClaim(title:String,msg:String)
    {
        let alert = UIAlertController(title: title,message: msg,preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes",style: .default) { (action:UIAlertAction) in
            self.callAPIForClaimPackage()
        }
        let noAction = UIAlertAction(title: "No",style: .default) { (action:UIAlertAction) in
            self.dismiss(animated: true,completion: nil)
        }
        yesAction.setValue(UIColor(named: "theme_black_color"),forKey: "titleTextColor")
        noAction.setValue(UIColor(named: "theme_black_color"),forKey: "titleTextColor")
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert,animated: true,completion: nil)
    }
    
    // MARK: - callAPIForClaimPackage
    /**
     Calls the API to claim a package.
     
     This function sends a POST request to the server with the necessary parameters to claim a package. The parameters include the user ID, tracking number, HAWB number, order ID, and package amount. The response from the server is handled in the completion block, where a success message is displayed if the status is true, otherwise an error message is shown.
     */
    func callAPIForClaimPackage()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["tracking_number"] = strTrackingNo
        params["hawb_number"]  = HAWNo.text ?? ""
        params["order_id"] = objTrackOrder.orderId ?? ""
        params["package_amount"] = self.packageAmt
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ClaimOrder, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.showAlert(title: "", msg: message, vc: self)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnClaimClicked()
    {
        guard let packageAmount = objTrackOrder.packageAmount else
        {
            getPackageAmount()
            return
        }
        if packageAmount != ""
        {
            if Float(packageAmount)! <= 0
            {
                getPackageAmount()
            }
            else
            {
                self.showAlertForClaim(title: "", msg: Messsages.msg_claim)
            }
        }
        else
        {
            getPackageAmount()
        }
    }
    @IBAction func btnSearchClicked()
    {
        if let vc = appDelegate.getViewController("TrackOrderSearchVC", onStoryboard: "Home") as? TrackOrderSearchVC {
            vc.strSearchText = objTrackOrder.trackingNumber ?? ""
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc,animated: true, completion: nil)
        }
    }
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Tableview delegate methods
    /// Returns the number of sections in the table view.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    /// Returns the number of rows in the specified section of the table view.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblTrackingDetail {
            return arrLogs.count
        } else if tableView == tblPackageDetail {
            return arrPackageDetail.count + 1
        }
        return 0
    }

    /// Returns the height for the header of the specified section of the table view.
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }

    /// Returns the height for the specified row of the table view.
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblTrackingDetail {
            return 110
        } else if tableView == tblPackageDetail {
            return 50
        }
        return 0
    }

    /// Returns the height for the footer of the specified section of the table view.
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == tblPackageDetail {
            return 50
        }
        return .leastNormalMagnitude
    }

    /// Returns the view for the footer of the specified section of the table view.
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if tableView == tblPackageDetail {
            packageFooterView.frame = CGRect(x: 0, y: 0, width: self.tblPackageDetail.frame.size.width, height: 50)
            return packageFooterView
        }
        return nil
    }

    /// Returns the cell for the specified row and section of the table view.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblTrackingDetail {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsCell", for: indexPath) as? OrderDetailsCell {
                let objLog = arrLogs[indexPath.row]
                cell.lblTitle.text = objLog.logs ?? ""
                cell.lblDate.text = objLog.date ?? ""
                if indexPath.row == arrLogs.count - 1 {
                    cell.view1.isHidden = true
                    cell.view2.isHidden = true
                    cell.view3.isHidden = true
                } else {
                    cell.view1.isHidden = false
                    cell.view2.isHidden = false
                    cell.view3.isHidden = false
                }
                return cell
            }
        } else if tableView == tblPackageDetail {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "PackageDetailsCell", for: indexPath) as? PackageDetailsCell {
                if indexPath.row == 0 {
                    let attributes = [NSAttributedString.Key.foregroundColor:UIColor(named: "theme_label_blue_color"),
                                      NSAttributedString.Key.font : UIFont(name: fontname.openSansBold, size: 15)]
                    cell.lblDesc.attributedText = NSAttributedString(string: "Description", attributes: attributes)
                    cell.lblPiece.attributedText = NSAttributedString(string: "Pcs", attributes: attributes)
                    cell.lblWeight.attributedText = NSAttributedString(string: "Weight", attributes: attributes)
                    return cell
                } else {
                    let objPackageDetail = arrPackageDetail[indexPath.row - 1]
                    cell.lblDesc.text = objPackageDetail.descriptionField ?? ""
                    cell.lblPiece.text = String(objPackageDetail.qty ?? 0)
                    cell.lblWeight.text = objPackageDetail.weight ?? ""
                    return cell
                }
            }
        }
        return UITableViewCell()
    }
    // MARK: - Textfield delgate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let r = Range(range, in: text)
        let newText = text.replacingCharacters(in: r!, with: string)
        let newLength = text.count + string.count - range.length
        if newText.contains(".")
        {
            let isNumeric = newText.isEmpty || (Double(newText) != nil)
            let numberOfDots = newText.components(separatedBy: ".").count - 1
            
            let numberOfDecimalDigits: Int
            if let dotIndex = newText.firstIndex(of: ".") {
                numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
            } else {
                numberOfDecimalDigits = 0
            }
            return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
        }
        else
        {
            return newLength <= 5
        }
        
    }
}
/// A custom table view cell used for displaying order details.
class OrderDetailsCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var view1 : UIView!
    @IBOutlet weak var view2 : UIView!
    @IBOutlet weak var view3 : UIView!
    @IBOutlet weak var view4 : UIView!
    
    override func awakeFromNib() {
        view1.layer.cornerRadius = view1.layer.frame.size.height/2
        view2.layer.cornerRadius = view2.layer.frame.size.height/2
        view3.layer.cornerRadius = view3.layer.frame.size.height/2
    }
}
/// A custom table view cell for displaying package details.
class PackageDetailsCell : UITableViewCell
{
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var lblPiece : UILabel!
    @IBOutlet weak var lblWeight : UILabel!
    
    override func awakeFromNib() {
        
    }
}
