//
//  PaypalPaymentVC.swift
//  CSFCustomer
//
//  Created by Tops on 16/02/21.
//

import UIKit
import WebKit
class PaypalPaymentVC: UIViewController,WKNavigationDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var webView : WKWebView!
    
    // MARK: - Global Variable
    var paypalURL : String = ""
    var cancelURL : String = ""
    var successURL : String = ""
    var token : String = ""
    var payerID : String = ""
    var invoiceIDs : String = ""
    var flag : Bool = true
    var timer : Timer?
    var startTimer : Timer?
    var isFromVC : String  = ""
    var TTDAmount : String = "0"
    var USDAmount : String = "0"

    //Nand : Partial payment changes
    var partialPrice : Float = 0
    var isPartialPayementSelected : Bool = false
    var isPartialPaymentOptionSelected : Bool = false
    
    //DriveThru Request
    var isFromDriveThruRequest : Bool = false
    var strInvoiceIdsForDriveThruRequest = ""
    var strPickupDateForDriveThruRequest = ""
    var strPickupTimeForDriveThruRequest = ""
    var strDateToDisplayForDriveThruRequest = ""
    var strTimeToDisplayForDriveThruRequest = ""

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if startTimer != nil
        {
            startTimer?.invalidate()
            startTimer = nil
        }
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        startTimer = Timer.scheduledTimer(timeInterval: 1800.0, target: self, selector: #selector(PaypalPaymentVC.timerEventCalled), userInfo: nil, repeats: false)
        webView.navigationDelegate = self
        getPaypalURL()
    }
    
    // MARK: - Timer Methods
    @objc func timerEventCalled()
    {
        startTimer?.invalidate()
        startTimer = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    func setTimer()
    {
        self.showHUD()
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(stopTimer), userInfo: nil, repeats: false)
    }
    @objc func stopTimer()
    {
        timer?.invalidate()
        timer = nil
        self.hideHUD()
    }
    
    // MARK: - getPaypalURL
    func getPaypalURL()
    {
        let objUserInfo = getUserInfo()
        var params : [String:Any] = [:]
        var strURL : String = ""
        if isFromVC == "AddMoney"
        {
            params["user_id"] = objUserInfo.id ?? ""
            params["total_ttd"] = TTDAmount
            strURL = APICall.GetWalletPaypalURL
        }
        else
        {
            params["user_id"] = objUserInfo.id ?? ""
            params["invoice_id"]  = invoiceIDs
            
            //Nand : Partial payment changes
            params["is_partial"]  = self.isPartialPayementSelected
            params["partial_wallet"]  = self.isPartialPaymentOptionSelected
            params["partial_wallet_ttd"]  = self.partialPrice
            
            strURL = APICall.GetPaypalURL
        }
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url:strURL , showLoader:true) { [self] (resultDict, status, message) in
            if status == true
            {
                paypalURL = (resultDict as NSDictionary).value(forKey: "payment_url") as? String ?? ""
                cancelURL = (resultDict as NSDictionary).value(forKey: "cancel_url") as? String ?? ""
                successURL = (resultDict as NSDictionary).value(forKey: "success_url") as? String ?? ""
                setTimer()
                loadPaypalURL(paypalURL : paypalURL)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }

    func loadPaypalURL(paypalURL:String)
    {
        let link = URL(string:paypalURL)!
        let request = URLRequest(url: link)
        webView.navigationDelegate = self
        webView.load(request)
    }
    
    // MARK: - IBAction methods    
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webview delegate methods
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //Show loader
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //Hide loader
//        let cssString = "@media (prefers-color-scheme: dark) {body {color: black;}a:link {color: #0096e2;}a:visited {color: #9d57df;}}"
//        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
//        webView.evaluateJavaScript(jsString, completionHandler: nil)
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        //Hide loader
        self.hideHUD()
    }
    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        let url =  navigationAction.request.url!
        guard let urlComponents = URLComponents(string: url.absoluteString) else { return  }
        if navigationAction.request.url?.path == URL(string:successURL)?.path
        {
            let queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
            if let queryitemToken = queryItems.filter({$0.name == "token"}).first
            {
                token = queryitemToken.value ?? ""
            }
            if let queryitemPayerID = queryItems.filter({$0.name == "PayerID"}).first
            {
                payerID = queryitemPayerID.value ?? ""
            }
            if isFromVC == "AddMoney"
            {
                self.callAPIForPaypalDataToWallet()
            }
            else
            {
                self.callAPIForPaypalData()
            }
        }
        else if navigationAction.request.url?.path == URL(string:cancelURL)?.path
        {
            showAlertForCancel(title: "", msg: Messsages.msg_paypal_cancel, vc: self)
        }
        else
        {
            // showAlert(title: "", msg: "Please try again", vc: self)
        }
        decisionHandler(.allow)
    }
    func showAlertForCancel(title:String,msg:String,vc:UIViewController)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        action1.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(action1)
        vc.present(alert, animated: true, completion: nil)
    }
    func callAPIForPaypalDataToWallet()
    {
        let objUserInfo = getUserInfo()
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["payer_id"]  = payerID
        params["token"]  = token
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.PostWalletPaypalURL, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                NotificationCenter.default.post(name: Notification.Name("WalletUpdated"), object: nil,userInfo: nil)
                if let vc = appDelegate.getViewController("PaymentSuccessVC", onStoryboard: "Invoice") as? PaymentSuccessVC {
                    vc.isFrom = "AddMoney"
                    vc.TTDAmount = self.USDAmount //self.TTDAmount
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - callAPIForPaypalData
    func callAPIForPaypalData()
    {
        let objUserInfo = getUserInfo()
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"]  = invoiceIDs
        params["payer_id"]  = payerID
        params["token"]  = token
        
        //Nand : Partial payment changes
        params["is_partial"]  = self.isPartialPayementSelected
        params["partial_wallet"]  = self.isPartialPaymentOptionSelected
        params["partial_wallet_ttd"]  = self.partialPrice

        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.PostPaypalDataURL, showLoader:false) { (resultDict, status, message) in
            if status == true
            {                
                if self.isFromDriveThruRequest {
                    //Add drive thru request
                    if !self.strInvoiceIdsForDriveThruRequest.isEmpty {
                        
                        let objUserInfo = getUserInfo()
                        self.createDriveThruRequest(userId: objUserInfo.id ?? "", invoiceId: self.strInvoiceIdsForDriveThruRequest, pickupDate: self.strPickupDateForDriveThruRequest, pickupTime: self.strPickupTimeForDriveThruRequest,selectedDateToDisplay: self.strDateToDisplayForDriveThruRequest, selectedTimeToDisplay: self.strTimeToDisplayForDriveThruRequest,type: 2, resultDate: resultDict, fromVC: self)
                    }
                }else {
                    if let vc = appDelegate.getViewController("PaymentSuccessVC", onStoryboard: "Invoice") as? PaymentSuccessVC {
                        vc.dict = resultDict
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}
