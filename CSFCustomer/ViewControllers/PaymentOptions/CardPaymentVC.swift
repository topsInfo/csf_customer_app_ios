//
//  CardPaymentVC.swift
//  CSFCustomer
//
//  Created by Tops on 10/03/21.
//

import UIKit
import Stripe
import SwiftyJSON
class CardPaymentVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var payableAmountView : UIView!
    @IBOutlet weak var cardOptionsView : UIView! //new change
    @IBOutlet weak var cardTextField : STPPaymentCardTextField!
    @IBOutlet weak var btnPay : UIButton!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblAmountTitle : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    //card outlets
    @IBOutlet weak var imgNewCard : UIImageView!
    @IBOutlet weak var imgSelectCard : UIImageView!
    @IBOutlet weak var addCardView : UIStackView!
    @IBOutlet weak var selectCardView : UIView!
    @IBOutlet weak var tblCard : UITableView!
    @IBOutlet weak var lblNoCardFound : UILabel!
    @IBOutlet weak var btnSelectedCardPay : UIButton!
    @IBOutlet weak var btnSaveCard : UIButton!
    
    // MARK: - Global Variable
    var isSavedCard : Int = 0
    var invoiceIDs : String = ""
    var totalTTDAmt : String = ""
    var totalUSDAmt : String = ""
    //Nand : Payment detail changes
    var payableTTDAmount : String = "0"
    let cardParams = STPCardParams()
    var stripeToken : String = ""
    var isFromVC : String  = ""
    var arrCardList : [StripeCardList] = [StripeCardList]()
    var selectedIndex : Int = 0
    var cardFormat : String  = "XXXX XXXX XXXX"
    var objUserInfo = UserInfo()
    var cardID : String = ""
    var type : String =  ""
    
    //Nand : Partial payment changes
    var partialPrice : Float = 0
    var isPartialPayementSelected : Bool = false
    var isPartialPaymentOptionSelected : Bool = false
    var isAddNewCardSelectd : Bool = false
    
    //DriveThru Request
    var isFromDriveThruRequest : Bool = false
    var strInvoiceIdsForDriveThruRequest = ""
    var strPickupDateForDriveThruRequest = ""
    var strPickupTimeForDriveThruRequest = ""
    var strDateToDisplayForDriveThruRequest = ""
    var strTimeToDisplayForDriveThruRequest = ""

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        objUserInfo = getUserInfo()
        payableAmountView.dropShadow()
        cardOptionsView.dropShadow()
        selectCardView.dropShadow()
        cardTextField.keyboardDistanceFromTextField = 100
        
        getCardList()
        //Nand : Payment detail changes
        if isFromVC == "AddMoney"
        {
            lblTitle.text = "Add Amount to Wallet"
            lblAmountTitle.text = "Add Amount"
            lblAmount.text = "\(kCurrencySymbol)\(totalUSDAmt) \(kCurrency) / \(kCurrencySymbol)\(totalTTDAmt)  \(kTTCurrency)"
        }
        else
        {
            lblTitle.text = "Card Payment"
            lblAmountTitle.text = "Payable Amount"
            if payableTTDAmount == "0" {
                lblAmount.text = "\(kCurrencySymbol)\(totalTTDAmt)  \(kTTCurrency)"
            }else{
                lblAmount.text = "\(kCurrencySymbol)\(payableTTDAmount)  \(kTTCurrency)"
            }
            
        }
        btnPay.layer.cornerRadius = 2
        btnPay.layer.masksToBounds = true
        btnPay.isMultipleTouchEnabled = false
        btnPay.isExclusiveTouch = true
        
        tblCard.estimatedRowHeight = 50
        tblCard.rowHeight = UITableView.automaticDimension
        tblCard.dataSource = self
        tblCard.delegate = self
        tblCard.tableFooterView = UIView()
    }
    
    // MARK: - getCardList
    func getCardList()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.StripeCardList, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                let objCard = StripeCards(fromJson: JSON(resultDict))
                self.arrCardList = objCard.list ?? []
                self.arrCardList = self.arrCardList.filter { !$0.isExpired }

                if self.arrCardList.count > 0
                {
                    self.selectedIndex = 0
                    self.lblNoCardFound.isHidden = true
                    self.btnSelectedCardPay.isHidden = false
                    self.tblCard.isHidden = false
                    DispatchQueue.main.async {
                        self.tblCard.reloadData()
                    }
                    self.imgNewCard.image = UIImage(named: "radio_btn2")
                    self.imgSelectCard.image = UIImage(named: "radio_btn")
                    self.selectCardView.isHidden = false
                    self.addCardView.isHidden = true
                }
                else
                {
                    self.lblNoCardFound.isHidden = false
                    self.tblCard.isHidden = true
                    self.btnSelectedCardPay.isHidden = true
                    self.imgNewCard.image = UIImage(named: "radio_btn")
                    self.imgSelectCard.image = UIImage(named: "radio_btn2")
                    self.selectCardView.isHidden = true
                    self.addCardView.isHidden = false
                }
            }
            else
            {
                print("Result dict is \(resultDict)")
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCardOptionClicked(_ sender:UIButton)
    {
        if sender.tag == 101
        {
            isAddNewCardSelectd = true
            imgNewCard.image = UIImage(named: "radio_btn")
            imgSelectCard.image = UIImage(named: "radio_btn2")
            selectCardView.isHidden = true
            addCardView.isHidden = false
            btnPay.setTitle("Pay", for: .normal)
        }
        else
        {
            isAddNewCardSelectd = false
            imgNewCard.image = UIImage(named: "radio_btn2")
            imgSelectCard.image = UIImage(named: "radio_btn")
            selectCardView.isHidden = false
            btnPay.setTitle("Pay", for: .normal)
            if arrCardList.count > 0
            {
                tblCard.isHidden = false
                lblNoCardFound.isHidden = true
                btnSelectedCardPay.isHidden = false
            }
            else
            {
                tblCard.isHidden = true
                lblNoCardFound.isHidden = false
                btnSelectedCardPay.isHidden = true
            }
            addCardView.isHidden = true
        }
    }
    @IBAction func btnPayClicked()
    {
       // print("payment")
        StripePayment()
    }
    @IBAction func btnSaveCardClicked(_ sender:UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected
        {
            btnSaveCard.setImage(UIImage(named: "checkbox"), for: .normal)
            isSavedCard = 1
        }
        else
        {
            btnSaveCard.setImage(UIImage(named: "uncheckbox"), for: .normal)
            isSavedCard = 0
        }
    }
    @IBAction func btnSelectedCardPayClicked(_ sender:UIButton)
    {
       // print("payment")
        if arrCardList.count > 0
        {
            if self.isFromVC == "AddMoney"
            {
                self.showHUD()
                self.callAPIForStripeToWalletTransfer()
            }
            else
            {   self.showHUD()
                self.callAPIForStripePayment()
            }
            
        }
    }
    // MARK: - Custom methods
    func StripePayment()
    {
        self.showHUD()
        cardParams.number =  cardTextField.cardNumber ?? ""
        cardParams.expMonth = UInt(( cardTextField.expirationMonth) )
        cardParams.expYear = UInt(( cardTextField.expirationYear))
        cardParams.cvc =  cardTextField.cvc
        cardParams.addressZip = cardTextField.postalCode
        if isValidData()
        {
            STPAPIClient.shared.createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
                
                guard let token = token, error == nil else {
                    self.hideHUD()
                    self.showAlert(title: "", msg: error!.localizedDescription, vc: self)
                    //print(error?.localizedDescription)
                    return
                }
               // print(token)
                self.stripeToken = token.tokenId
                self.cardID = ""
                if self.isFromVC == "AddMoney"
                {
                    self.callAPIForStripeToWalletTransfer()
                }
                else
                {
                    self.callAPIForStripePayment()
                }
            }
        }
        else
        {
            self.hideHUD()
            showAlert(title: "", msg: Messsages.msg_no_valid_card, vc: self)
        }
    }
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if STPCardValidator.validationState(forCard: cardParams) == .valid {
            return true
        } else {
            return false
        }
    }
    // MARK: - callAPIForStripeToWalletTransfer
    func callAPIForStripeToWalletTransfer()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["token"] = self.stripeToken
        params["total_ttd"]  = self.totalTTDAmt //need to change
        params["card_id"] = self.cardID
        params["type"] = self.type
        params["save_card"] = isSavedCard //need to check
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.StripeToWalletTransfer, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()
                //                self.popupAlert(title: "", message: message, actionTitles: ["Ok"], actions:[{action1 in
                //                    self.gotoWalletVC()
                //                }])
                NotificationCenter.default.post(name: Notification.Name("WalletUpdated"), object: nil,userInfo: nil)
                if let vc = appDelegate.getViewController("PaymentSuccessVC", onStoryboard: "Invoice") as? PaymentSuccessVC {
                    vc.isFrom = "AddMoney"
                    vc.TTDAmount = self.totalUSDAmt
                    NotificationCenter.default.post(name: Notification.Name("CardUpdated"), object: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - callAPIForStripePayment
    func callAPIForStripePayment()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"]  = invoiceIDs
        params["token"] = self.stripeToken
        params["card_id"] = self.cardID
        params["type"] = self.type
        params["save_card"] = isSavedCard

        //Nand : Partial payment changes
        params["is_partial"]  = self.isPartialPayementSelected
        params["partial_wallet"]  = self.isPartialPaymentOptionSelected
        params["partial_wallet_ttd"]  = self.partialPrice

        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.StripePayment, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()

                if self.isFromDriveThruRequest {
                    //Add drive thru request
                    if !self.strInvoiceIdsForDriveThruRequest.isEmpty {
                        
                        let objUserInfo = getUserInfo()
                        self.createDriveThruRequest(userId: objUserInfo.id ?? "", invoiceId: self.strInvoiceIdsForDriveThruRequest, pickupDate: self.strPickupDateForDriveThruRequest , pickupTime: self.strPickupTimeForDriveThruRequest , selectedDateToDisplay: self.strDateToDisplayForDriveThruRequest, selectedTimeToDisplay: self.strTimeToDisplayForDriveThruRequest,type: 2, resultDate: resultDict, fromVC: self)
                    }
                } else {
                    if let vc = appDelegate.getViewController("PaymentSuccessVC", onStoryboard: "Invoice") as? PaymentSuccessVC {
                        vc.dict = resultDict
                        if self.isAddNewCardSelectd {
                            NotificationCenter.default.post(name: Notification.Name("CardUpdated"), object: nil)
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - Tableview datasource and delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCardList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "StripCardCell") as? StripCardCell {
            let objList = arrCardList[indexPath.row]
            
            cell.lblCardName.text = "\(cardFormat) \(objList.last4 ?? "")"
            cell.btnDelete.tag = indexPath.row
            cell.btnSelect.tag = indexPath.row
            if selectedIndex == indexPath.row
            {
                cell.imgSelect.image = UIImage(named: "radio_btn")
                self.cardID = objList.cardId ?? ""
            }
            else
            {
                cell.imgSelect.image = UIImage(named: "radio_btn2")
            }
            cell.btnSelect.addTarget(self, action: #selector(self.btnSelectClicked(_:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(self.btnDeleteClicked(_:)), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
    // MARK: - Custom Methods
    @objc func btnSelectClicked(_ sender:UIButton)
    {
        self.selectedIndex = sender.tag
        DispatchQueue.main.async {
            self.tblCard.reloadData()
        }
    }
    @objc func btnDeleteClicked(_ sender:UIButton)
    {
        self.popupAlert(title: "", message: Messsages.msg_delete_card_confirmation, actionTitles: ["Confirm","Cancel"], actions:[{action1 in
            if self.arrCardList.count > 0
            {
                let objList = self.arrCardList[sender.tag]
                let cardID = objList.cardId ?? ""
                self.callAPIToDeleteCard(cardID : cardID)
            }
        },{ action2 in
            self.dismiss(animated: true, completion: nil)
        }])
    }
    // MARK: - callAPIToDeleteCard
    func callAPIToDeleteCard(cardID:String)
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["card_id"] = cardID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.DeleteStripeCard, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.popupAlert(title: "", message: message, actionTitles: ["Ok"], actions:[{action1 in
                    self.getCardList()
                }])
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}
class StripCardCell : UITableViewCell
{
    @IBOutlet weak var imgSelect : UIImageView!
    @IBOutlet weak var lblCardName : UILabel!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnSelect : UIButton!
}
