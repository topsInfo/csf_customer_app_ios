//
//  LightDarkModeVC.swift
//  CSFCustomer
//
//  Created by iMac on 28/01/22.
//

import UIKit

class LightDarkModeVC: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var imgDarkThemeCheckBox: UIImageView!
    @IBOutlet weak var imgLightThemeCheckBox: UIImageView!
    @IBOutlet weak var imgSystemThemeCheckBox: UIImageView!
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        imgDarkThemeCheckBox.layer.cornerRadius = imgDarkThemeCheckBox.frame.size.height / 2
        imgLightThemeCheckBox.layer.cornerRadius = imgLightThemeCheckBox.frame.size.height / 2
        imgSystemThemeCheckBox.layer.cornerRadius = imgSystemThemeCheckBox.frame.size.height / 2
        imgDarkThemeCheckBox.layer.masksToBounds = true
        imgLightThemeCheckBox.layer.masksToBounds = true
        imgSystemThemeCheckBox.layer.masksToBounds = true
        
        setThemeSelection()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Set Theme Selection
    func setThemeSelection(){
        if (UserDefaults.standard.value(forKey: kIsDarkModeEnabled) as? Bool) == true{
            imgDarkThemeCheckBox.image = UIImage(named: "select")
            imgDarkThemeCheckBox.isHidden = false
        }else{
            imgDarkThemeCheckBox.isHidden = true
        }
        
        if (UserDefaults.standard.value(forKey: kIsLightModeEnabled) as? Bool) == true{
            imgLightThemeCheckBox.image = UIImage(named: "select")
            imgLightThemeCheckBox.isHidden = false
        }else{
            imgLightThemeCheckBox.isHidden = true
        }
        
        if (UserDefaults.standard.value(forKey: kIsSystemModeModeEnabled) as? Bool) == true{
            imgSystemThemeCheckBox.image = UIImage(named: "select")
            imgSystemThemeCheckBox.isHidden = false
        }else{
            imgSystemThemeCheckBox.isHidden = true
        }
    }

    // MARK: - Enable Theme
    func enableDarkTheme(){
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let appDel = UIApplication.shared.windows.first
                appDel?.overrideUserInterfaceStyle = .dark
            }
        }
    }
    
    func enableLightTheme(){
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let appDel = UIApplication.shared.windows.first
                appDel?.overrideUserInterfaceStyle = .light
            }
        }
    }
    
    func enableUnspecifiedTheme(){
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let appDel = UIApplication.shared.windows.first
                appDel?.overrideUserInterfaceStyle = .unspecified
            }
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnDarkThemeClicked(_ sender: UIButton) {
        enableDarkTheme()
        UserDefaults.standard.setValue(true, forKey: kIsDarkModeEnabled)
        UserDefaults.standard.setValue(false, forKey: kIsLightModeEnabled)
        UserDefaults.standard.setValue(false, forKey: kIsSystemModeModeEnabled)
        setThemeSelection()
    }
    
    @IBAction func btnLightThemeClicked(_ sender: UIButton) {
        enableLightTheme()
        UserDefaults.standard.setValue(true, forKey: kIsLightModeEnabled)
        UserDefaults.standard.setValue(false, forKey: kIsDarkModeEnabled)
        UserDefaults.standard.setValue(false, forKey: kIsSystemModeModeEnabled)
        setThemeSelection()
    }
    
    @IBAction func btnSystemThemeClicked(_ sender: UIButton) {
        enableUnspecifiedTheme()
        UserDefaults.standard.setValue(true, forKey: kIsSystemModeModeEnabled)
        UserDefaults.standard.setValue(false, forKey: kIsDarkModeEnabled)
        UserDefaults.standard.setValue(false, forKey: kIsLightModeEnabled)
        setThemeSelection()
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
