//
//  CaclulatorDataVC.swift
//  CSFCustomer
//
//  Created by Tops on 03/02/21.
//

import UIKit

class CaclulatorDataVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var tblCalculator : UITableView!
    @IBOutlet weak var footerView :UIView!
    @IBOutlet weak var lblTotalUSDAmt : TapAndCopyLabel!
    @IBOutlet weak var lblTotalTTDAmt : TapAndCopyLabel!
    @IBOutlet weak var lblTTDAmt : TapAndCopyLabel!
    @IBOutlet weak var lblUSDAmt : TapAndCopyLabel!
    @IBOutlet weak var lblUSDtoTTDConversionValue : UILabel!
    
    var objCalcData = CalculatorData()
    var arrEstimationList : [EstimationListData] = [EstimationListData]()
    var msgQuantity = "Count (lbs/CF) : "
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        tblCalculator.dataSource = self
        tblCalculator.delegate = self
        tblCalculator.estimatedRowHeight = 60
        tblCalculator.rowHeight = UITableView.automaticDimension
        tblCalculator.tableFooterView = UIView()
        arrEstimationList = objCalcData.estimationList ?? []
        
        lblTotalUSDAmt.text = objCalcData.finalUsd ?? ""
        lblTotalTTDAmt.text = objCalcData.finalTtd ?? ""
        lblTTDAmt.text = objCalcData.totalAmountTtd ?? ""
        lblUSDAmt.text = objCalcData.totalAmountUsd ?? ""
        
        //(USD to TTD Conversion Value = 6.80)
        if let conversationRate = objCalcData.conversionRate {
            self.lblUSDtoTTDConversionValue.text = "(USD to TTD Conversion Value = \(conversationRate)"
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Tableview Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 245
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerView.frame = CGRect(x: 0, y: 0, width: tblCalculator.frame.size.width, height: 215)
        return footerView
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrEstimationList.count > 0
        {
            return arrEstimationList.count + 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CalculatorCell", for: indexPath) as? CalculatorCell {
            if indexPath.row == 0
            {
                //Colors.theme_blue_color
                cell.lblQuantity.isHidden = true
                let attributes = [NSAttributedString.Key.foregroundColor:UIColor(named:"theme_label_blue_color")]
                cell.lblTitle.attributedText = NSAttributedString(string: "DESCRIPTION OF CHARGES", attributes: attributes)
                cell.lblTTDAmt.attributedText = NSAttributedString(string: "TTD", attributes: attributes)
                cell.lblUSDAmt.attributedText = NSAttributedString(string: "USD", attributes: attributes)
                return cell
            }
            else if indexPath.row >= 1 && indexPath.row <= arrEstimationList.count
            {
                let objCalcData = arrEstimationList[indexPath.row-1]
                cell.lblTitle.text =  objCalcData.title ?? ""
                cell.lblUSDAmt.text = objCalcData.amountUsd ?? "0.00"
                cell.lblTTDAmt.text = objCalcData.amountTtd ?? "0.00"
                cell.lblQuantity.text = "\(msgQuantity)\(objCalcData.qty ?? "")"
            }
            return cell
        }
        return UITableViewCell()
    }
}
// MARK: - CalculatorCell
class CalculatorCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblUSDAmt : UILabel!
    @IBOutlet weak var lblTTDAmt : UILabel!
    @IBOutlet weak var lblQuantity : UILabel!
}
