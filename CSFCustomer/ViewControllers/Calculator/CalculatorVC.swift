//
//  CalculatorVC.swift
//  CSFCustomer
//
//  Created by Tops on 02/02/21.
//

import UIKit
import SwiftyJSON
import DropDown

/**
 An enumeration representing the status of crating.

 - Yes: Indicates that crating is required.
 - No: Indicates that crating is not required.
 */
enum CratingStatus : String
{
    case Yes = "Yes"
    case No = "No"
}

/**
 An enumeration representing the status of EEI (Electronic Export Information).

 - Yes: Indicates that EEI is required.
 - No: Indicates that EEI is not required.
 */
enum EEIStatus : String
{
    case Yes = "Yes"
    case No = "No"
}

class CalculatorVC: UIViewController,UITextFieldDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var tblCalculator : UITableView!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var txtShippingMethod : UITextField!
    @IBOutlet weak var txtCrating : UITextField!
    @IBOutlet weak var txtEEI : UITextField!
    @IBOutlet weak var txtItemDesc : UITextField!
    @IBOutlet weak var txtTotalAmt : UITextField!
    @IBOutlet weak var txtWeight : UITextField!
    @IBOutlet weak var btnSubmit : UIButton!
    @IBOutlet weak var btnReset : UIButton!
    @IBOutlet weak var shippingContainer : UIView!
    @IBOutlet weak var cratingContainer : UIView!
    //for ocean calculator
    @IBOutlet weak var txtNNumber : UITextField!
    @IBOutlet weak var txtLength : UITextField!
    @IBOutlet weak var txtWidth : UITextField!
    @IBOutlet weak var txtHeight : UITextField!
    @IBOutlet weak var NNumberStackview : UIStackView!
    @IBOutlet weak var shippingDimesionStackview : UIStackView!
    @IBOutlet weak var imgAirOcean : UIImageView!
    @IBOutlet weak var imgOnlineShopper : UIImageView!
    
    @IBOutlet weak var imgTrininad : UIImageView!
    @IBOutlet weak var imgTobago : UIImageView!

    @IBOutlet weak var viewCalcImg: UIView!
    @IBOutlet weak var imgCalculator: UIImageView!
    @IBOutlet weak var stackViewEEI: UIStackView!
    
    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    var arrDescList : [ShippingList] = [ShippingList]()
    var arrFilterDesc : [ShippingList] = [ShippingList]()
    var arrShippingMethod : [ShippingMethod] = [ShippingMethod]()
    var arrCratingStatus : [String] = [String]()
    var arrEEIStatus : [String] = [String]()
    let dropdown = DropDown()
    var selectedShippingID : String = ""
    var selectedDescID : String = ""
    var selectedItemDesc : String = ""
    var type : String = ""
    var arrTextFields = [UITextField]()
    var isAirOceanSelected : Bool = true
    var isOnlineShopperSelected : Bool = false
    var isTrininadSelected : Bool = true
    var isTobagoSelected : Bool = false
    
    var objCalcInfo = CalculatorInfo()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        self.setTableviewHeight()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        topBarView.topBarBGColor()
        objUserInfo = getUserInfo()
        getCalculatorData()
        setDropDown()
        
        txtShippingMethod.paddingView(xvalue: 10)
        txtCrating.paddingView(xvalue: 10)
        txtEEI.paddingView(xvalue: 10)
        txtNNumber.paddingView(xvalue: 10)
        txtItemDesc.paddingView(xvalue: 10)
        txtTotalAmt.paddingView(xvalue: 10)
        txtWeight.paddingView(xvalue: 10)
        
        arrCratingStatus.append(CratingStatus.Yes.rawValue)
        arrCratingStatus.append(CratingStatus.No.rawValue)
        
        arrEEIStatus.append(EEIStatus.Yes.rawValue)
        arrEEIStatus.append(EEIStatus.No.rawValue)
        
        txtCrating.text = arrCratingStatus[1]
        txtEEI.text = arrEEIStatus[1]
        
        txtNNumber.text = arrCratingStatus[1]
        self.NNumberStackview.isHidden = true
        self.shippingDimesionStackview.isHidden = true
        
        arrTextFields = [txtLength,txtWidth,txtHeight]
        for item in arrTextFields
        {
            item.layer.cornerRadius = 2
            item.layer.borderWidth = 1
            item.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor
            item.paddingView(xvalue: 10)
            item.delegate = self
        }
                
        txtItemDesc.delegate = self
        txtTotalAmt.delegate = self
        txtWeight.delegate = self
        
        //new change
        txtItemDesc.keyboardDistanceFromTextField = 200
        
        imgTrininad.image = UIImage(named: "radio_btn")
        isTrininadSelected = true
        
        imgAirOcean.image = UIImage(named: "radio_btn")
        isAirOceanSelected = true
    }
    
    // MARK: - setDropDown
    func setDropDown()
    {
        dropdown.width = UIScreen.main.bounds.size.width - 40
        dropdown.shadowRadius = 0
        dropdown.direction = .any
        dropdown.backgroundColor = UIColor(named:"theme_dropdown_color")!
        dropdown.separatorColor = UIColor(named:"theme_lightgray_color")!
        dropdown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropdown.textColor = UIColor(named:"theme_text_color")!
        dropdown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropdown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        dropdown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                if self!.type == "Crating"
                {
                    self?.txtCrating.text = item
                }
                if self!.type == "EEI"
                {
                    self?.txtEEI.text = item
                }
                else if self!.type == "NNumber"
                {
                    self?.txtNNumber.text = item
                }
                else if self!.type == "Shipping"
                {
                    let objShippingMethod = self?.arrShippingMethod[index]
                    self?.selectedShippingID  = objShippingMethod?.id ?? ""
                    self?.txtShippingMethod.text = item
                    self?.arrDescList = objShippingMethod?.list ?? []
                    self?.txtItemDesc.text = ""
                    self?.txtTotalAmt.text = ""
                    self?.txtWeight.text = ""
                    self?.selectedDescID = ""
                    self?.selectedItemDesc = ""
                    self?.txtCrating.text = self?.arrCratingStatus[1]
                    self?.txtEEI.text = self?.arrEEIStatus[1]

                    if self?.selectedShippingID == "1" // Air / AIR
                    {
                        //Air
                        self?.NNumberStackview.isHidden = true
                        self?.shippingDimesionStackview.isHidden = true
                    }
                    else
                    {
                        self?.txtLength.text = ""
                        self?.txtWidth.text = ""
                        self?.txtHeight.text = ""
                        self?.txtNNumber.text = self?.arrCratingStatus[1]
                        self?.NNumberStackview.isHidden = false
                        self?.shippingDimesionStackview.isHidden = false
                    }
                    self?.setTableviewHeight()
                }
                else if self?.type == "ItemDesc"
                {
                    let objDesc = self?.arrFilterDesc[index]
                    self?.selectedDescID  = objDesc?.id ?? ""
                    self?.txtItemDesc.text = item
                    self?.selectedItemDesc = item
                }
                self?.dropdown.hide()
                    
            }
        }
    }
    /**
     Sets the height of the tableview.
     
     This function is responsible for adjusting the height of the tableview based on its content. It is typically called after the tableview's data has been updated or when the view is being displayed.
     */
    func setTableviewHeight()
    {
        guard let headerView = tblCalculator.tableHeaderView else {return}
            let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
            if headerView.frame.size.height != size.height {
                headerView.frame.size.height = size.height
                tblCalculator.tableHeaderView = headerView
                tblCalculator.layoutIfNeeded()
            }
    }
    
    // MARK: - getCalculatorData
    func getCalculatorData()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.GetCalculator, showLoader:true) { [self] (resultDict, status, message) in
            if status == true
            {
                objCalcInfo = CalculatorInfo(fromJson: JSON(resultDict))
                
                self.arrShippingMethod = objCalcInfo.shippingMethod ?? []
                
                if self.arrShippingMethod.count > 0
                {
                    let objShippingMethod = self.arrShippingMethod[0]
                    self.txtShippingMethod.text = objShippingMethod.title ?? ""
                    self.selectedShippingID = objShippingMethod.id ?? ""
                    self.arrDescList = objShippingMethod.list ?? []
                    
                    if objCalcInfo.customsMsg != "" {
                        viewCalcImg.isHidden = false
                        self.imgCalculator.setImage(objCalcInfo.customsMsg)
                        self.imgCalculator.contentMode = .scaleAspectFill
                    }else {
                        viewCalcImg.isHidden = true
                    }
                }
            }
            else
            {
               appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCalcImgClicked(_ sender: UIButton) {
        if let webViewVC = appDelegate.getViewController("PDFViewerVC", onStoryboard: "Invoice") as? PDFViewerVC {
            
            if let link : String = objCalcInfo.customsLink {
                webViewVC.pdfFile = link
                webViewVC.strTitle = "Calculator"
                self.navigationController?.pushViewController(webViewVC, animated: true)
            }
        }
    }
    
    @IBAction func btnCratingClicked()
    {
        dropdown.anchorView = txtCrating
        dropdown.bottomOffset = CGPoint(x: 0, y:(txtCrating.bounds.size.height))
        dropdown.topOffset = CGPoint(x: 0, y:-(dropdown.anchorView?.plainView.bounds.height)!)
        
        dropdown.dataSource = arrCratingStatus
        dropdown.reloadAllComponents()
        type = "Crating"
        dropdown.show()
    }
    @IBAction func btnEEIClicked()
    {
        dropdown.anchorView = txtEEI
        dropdown.bottomOffset = CGPoint(x: 0, y:(txtEEI.bounds.size.height))
        dropdown.topOffset = CGPoint(x: 0, y:-(dropdown.anchorView?.plainView.bounds.height)!)

        dropdown.dataSource = arrEEIStatus
        dropdown.reloadAllComponents()
        type = "EEI"
        dropdown.show()
    }
    @IBAction func btnNNumberClicked()
    {
        dropdown.anchorView = txtNNumber
        dropdown.bottomOffset = CGPoint(x: 0, y:(txtNNumber.bounds.size.height))
        dropdown.topOffset = CGPoint(x: 0, y:-(dropdown.anchorView?.plainView.bounds.height)!)
        
        dropdown.dataSource = arrCratingStatus
        dropdown.reloadAllComponents()
        type = "NNumber"
        dropdown.show()
    }
    @IBAction func btnShippingClicked()
    {
        dropdown.anchorView = txtShippingMethod
        dropdown.bottomOffset = CGPoint(x: 0, y:(txtShippingMethod.bounds.size.height))
        dropdown.topOffset = CGPoint(x: 0, y:-(dropdown.anchorView?.plainView.bounds.height)!)
        
        dropdown.dataSource = arrShippingMethod.map({$0.title})
        dropdown.reloadAllComponents()
        type = "Shipping"
        dropdown.show()
    }
    @IBAction func btnTrininadClicked(_ sender: UIButton) {
        imgTrininad.image = UIImage(named: "radio_btn")
        isTrininadSelected = true

        imgTobago.image = UIImage(named: "radio_btn2")
        isTobagoSelected = false
    }    
    @IBAction func btnTobagoClicked(_ sender: UIButton) {
        imgTrininad.image = UIImage(named: "radio_btn2")
        isTrininadSelected = false

        imgTobago.image = UIImage(named: "radio_btn")
        isTobagoSelected = true
    }
    @IBAction func btnAirOceanClicked(_ sender:UIButton)
    {
        imgAirOcean.image = UIImage(named: "radio_btn")
        isAirOceanSelected = true

        imgOnlineShopper.image = UIImage(named: "radio_btn2")
        isOnlineShopperSelected = false
    }
    
    @IBAction func btnOnlineShopperClicked(_ sender:UIButton)
    {
        imgAirOcean.image = UIImage(named: "radio_btn2")
        isAirOceanSelected = false

        imgOnlineShopper.image = UIImage(named: "radio_btn")
        isOnlineShopperSelected = true
    }
    
    @IBAction func btnResetClicked()
    {
        txtCrating.text = arrCratingStatus[1]
        txtEEI.text = arrCratingStatus[1]
        txtItemDesc.text = ""
        txtTotalAmt.text = ""
        txtWeight.text = ""
        selectedDescID = ""
        selectedItemDesc = ""

        if self.selectedShippingID == "2" //OCEAN / Ocean
        {
            txtNNumber.text = arrCratingStatus[1]
            txtEEI.text = arrEEIStatus[1]
            txtLength.text = ""
            txtWidth.text = ""
            txtHeight.text = ""
        }
        if self.arrShippingMethod.count > 0
        {
            var objShippingMethod = ShippingMethod()
            if self.selectedShippingID == "1" {
                objShippingMethod = self.arrShippingMethod[0]
            }else{
                objShippingMethod = self.arrShippingMethod[1]
            }
            self.txtShippingMethod.text = objShippingMethod.title ?? ""
            self.selectedShippingID = objShippingMethod.id ?? ""
            self.arrDescList = objShippingMethod.list ?? []
        }
    }
    @IBAction func btnSubmitClicked()
    {
        if isValidData()
        {
            var params : [String:Any] = [:]
            objUserInfo = getUserInfo()
            params["user_id"] = objUserInfo.id ?? ""
            params["shipping_id"] = selectedShippingID
            params["description_id"] = selectedDescID == "" ? txtItemDesc.text : selectedDescID
            params["total_amount"] = txtTotalAmt.text
            params["weight"] = txtWeight.text
            params["crating_type"] = txtCrating.text == CratingStatus.Yes.rawValue ? 1 : 0
            params["eei_type"] = txtEEI.text == EEIStatus.Yes.rawValue ? 1 : 0
            params["length"] = txtLength.text
            params["width"] = txtWidth.text
            params["height"] = txtHeight.text
            params["n_number"] = txtNNumber.text == CratingStatus.Yes.rawValue ? 1 : 0
            params["type"] = isAirOceanSelected ? 1 : 2
            params["country_type"] = isTrininadSelected ? 1 : 2
            callAPIForCalculator(params:params)
        }
    }

    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtItemDesc.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_itemdesc, bottomValue: getSafeAreaValue())
            return false
        }
        else if self.selectedItemDesc != txtItemDesc.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        {
            appDelegate.showToast(message: Messsages.msg_enter_itemdesc, bottomValue: getSafeAreaValue())
            return false
        }
        else if self.selectedShippingID == "2"  //OCEAN / Ocean
        {
            if isCheckNull(strText: txtLength.text!)
            {
                appDelegate.showToast(message: Messsages.msg_enter_shipping_dimension, bottomValue: getSafeAreaValue())
                return false
            }
            else if Float(txtLength.text!) == 0
            {
                appDelegate.showToast(message: Messsages.msg_invalid_shipping_dimension, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtWidth.text!)
            {
                appDelegate.showToast(message: Messsages.msg_enter_shipping_dimension, bottomValue: getSafeAreaValue())
                return false
            }
            else if Float(txtWidth.text!) == 0
            {
                appDelegate.showToast(message: Messsages.msg_invalid_shipping_dimension, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtHeight.text!)
            {
                appDelegate.showToast(message: Messsages.msg_enter_shipping_dimension, bottomValue: getSafeAreaValue())
                return false
            }
            else if Float(txtHeight.text!) == 0
            {
                appDelegate.showToast(message: Messsages.msg_invalid_shipping_dimension, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtTotalAmt.text!)
            {
                appDelegate.showToast(message: Messsages.msg_enter_totalamt, bottomValue: getSafeAreaValue())
                return false
            }
            else if Float(txtTotalAmt.text!) == 0
            {
                appDelegate.showToast(message: Messsages.msg_invalid_totalamt, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtWeight.text!)
            {
                appDelegate.showToast(message: Messsages.msg_enter_weight, bottomValue: getSafeAreaValue())
                return false
            }
            else if Float(txtWeight.text!) == 0
            {
                appDelegate.showToast(message: Messsages.msg_invalid_weight, bottomValue: getSafeAreaValue())
                return false
            }
        }
        else if isCheckNull(strText: txtTotalAmt.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_totalamt, bottomValue: getSafeAreaValue())
            return false
        }
        else if Float(txtTotalAmt.text!) == 0
        {
            appDelegate.showToast(message: Messsages.msg_invalid_totalamt, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtWeight.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_weight, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    
    // MARK: - callAPIForCalculator
    func callAPIForCalculator(params:[String:Any])
    {
       
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.SubmitCalculatorData, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                let objCalcData = CalculatorData(fromJson: JSON(resultDict))
                if let vc = appDelegate.getViewController("CaclulatorDataVC", onStoryboard: "Invoice") as? CaclulatorDataVC {
                    vc.objCalcData = objCalcData
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
               appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - Textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let r = Range(range, in: text)
        let newText = text.replacingCharacters(in: r!, with: string)
        let newLength = text.count + string.count - range.length
        if textField == txtItemDesc
        {
            dropdown.anchorView = txtItemDesc
            dropdown.bottomOffset = CGPoint(x: 0, y:(txtItemDesc.bounds.size.height))
            dropdown.topOffset = CGPoint(x: 0, y:-(dropdown.anchorView?.plainView.bounds.height)!)

            if newLength >= 1
            {
                //Get all data which match with started searched content
                let array1 : [ShippingList] = arrDescList.filter({$0.title.lowercased().starts(with: newText.lowercased())})
                
                //Get all data which contains searched content but not included in array1
                let array2 : [ShippingList] = arrDescList.filter({$0.title.localizedCaseInsensitiveContains(newText) == true && !array1.contains($0)})

                //sort array1 by title length
                var sortedArray1 = array1.sorted(by: {$0.title.count < $1.title.count})
                
                //sort array2 by title length
                let sortedArray2 = array2.sorted(by: {$0.title.count < $1.title.count})
                
                //Append sorted array with sorted array2.
                sortedArray1.append(contentsOf: sortedArray2)
                let arrResult = sortedArray1
                
                if arrResult.count > 0
                {
                    type = "ItemDesc"
                    self.arrFilterDesc = arrResult
                    dropdown.dataSource = self.arrFilterDesc.map({$0.title!})
                    dropdown.reloadAllComponents()
                    dropdown.show()
                }
                else
                {
                    selectedDescID = ""
                    selectedItemDesc = ""
                    self.dropdown.dataSource = []
                    self.dropdown.reloadAllComponents()
                    self.dropdown.hide()
                }
            }
            else
            {
                selectedDescID = ""
                selectedItemDesc = ""
                self.dropdown.dataSource = []
                self.dropdown.reloadAllComponents()
                self.dropdown.hide()
            }
        }
        else if textField == txtTotalAmt || textField == txtWeight
        {
            if newText.contains(".")
            {
                let isNumeric = newText.isEmpty || (Double(newText) != nil)
                let numberOfDots = newText.components(separatedBy: ".").count - 1
            
                let numberOfDecimalDigits: Int
                if let dotIndex = newText.firstIndex(of: ".") {
                    numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
                } else {
                    numberOfDecimalDigits = 0
                }
                return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
            }
            else
            {
                return newLength <= 5
            }
        }
        else if textField == txtLength || textField == txtWidth || textField == txtHeight
        {
            return newLength <= 3
        }
        return true
    }
}
