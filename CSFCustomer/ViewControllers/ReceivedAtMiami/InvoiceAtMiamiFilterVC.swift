//
//  InvoiceAtMiamiFilterVC.swift
//  CSFCustomer
//
//  Created by Tops on 18/05/21.
//

import UIKit

class InvoiceAtMiamiFilterVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var dimmerView :UIView!
    @IBOutlet weak var imgHAWNo : UIImageView!
    @IBOutlet weak var imgTrackingNo : UIImageView!
    @IBOutlet weak var imgWeight : UIImageView!
    @IBOutlet weak var imgDesc : UIImageView!
    
    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    var strSearchType : String = "1"
    var strSearch : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        txtSearch.keyboardDistanceFromTextField = 10
        objUserInfo = getUserInfo()
        showData()
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.8)
        dimmerView.backgroundColor = UIColor(named: "theme_white_color") //Colors.theme_white_color
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - 410
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
        txtSearch.paddingView(xvalue: 10)
    }
    
    // MARK: - showData
    func showData()
    {
        if strSearch != ""
        {
            txtSearch.text = strSearch
        }
        if strSearchType == "2"
        {
            imgTrackingNo.image = UIImage(named: "radio_btn")
        }
        else if strSearchType == "3"
        {
            imgWeight.image = UIImage(named: "radio_btn")
        }
        else if strSearchType == "4"
        {
            imgDesc.image = UIImage(named: "radio_btn")
        }
        else
        {
            imgHAWNo.image = UIImage(named: "radio_btn")
            strSearchType = "1"
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtSearch.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_search_text, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    // MARK: - IBAction methods
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSearchOptionsClicked(_ sender:UIButton)
    {
        imgHAWNo.image = UIImage(named: "unselect")
        imgTrackingNo.image = UIImage(named: "unselect")
        imgWeight.image = UIImage(named: "unselect")
        imgDesc.image = UIImage(named: "unselect")
        if sender.tag == 101
        {
            imgHAWNo.image = UIImage(named: "radio_btn")
            strSearchType = "1"
        }
        else if sender.tag == 102
        {
            imgTrackingNo.image = UIImage(named: "radio_btn")
            strSearchType = "2"
        }
        else if sender.tag == 103
        {
            imgWeight.image = UIImage(named: "radio_btn")
            strSearchType = "3"
        }
        else if sender.tag == 104
        {
            imgDesc.image = UIImage(named: "radio_btn")
            strSearchType = "4"
        }
    }
    @IBAction func btnApplyClicked()
    {
        if isValidData()
        {
            let filterDict :[String: Any] = [
                "search_text" : txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
                "search_type" : strSearchType
            ]
            NotificationCenter.default.post(name: Notification.Name("InvoiceAtMiamiFilterApplied"), object: nil,userInfo: filterDict)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
