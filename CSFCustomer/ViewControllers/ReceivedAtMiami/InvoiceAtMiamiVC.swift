//
//  InvoiceAtMiami.swift
//  CSFCustomer
//
//  Created by Tops on 17/05/21.
//

import UIKit
import SwiftyJSON
class InvoiceAtMiamiVC: UIViewController, UIGestureRecognizerDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var viewTotalRecord : TotalRecordView!
    @IBOutlet weak var cnstTotalRecordsTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var tblInvoiceList : UITableView!
    @IBOutlet weak var btnClearFilter : UIView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    
    @IBOutlet weak var btnClearFilterHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var btnExportReport : UIButton!

    // MARK: - Global Variable
    var refreshControl = UIRefreshControl()
    var type : String = InvoiceType.MyInvoice.dispValue()
    var objUserInfo = UserInfo()
    var invoiceCellHeight : CGFloat = 154
    var arrSections : [Int] = [Int]()
    var arrSelectedSections : [Int] = [Int]()
    var isLoadingList : Bool = false
    var pageNo : Int = 1
    var totalPages : Int = 0
    var arrList : [InvoiceAtMiamiList] = [InvoiceAtMiamiList]()
    var arrMemberList : [MemberList] = [MemberList]()
    var isShowLoader : Bool = true
    var filename : String  = ""
    //filter vars
    var strSearch : String = ""
    var strSearchType : String = ""
    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    // MARK: - Custom Methods
    func configureControls()
    {
        objUserInfo = getUserInfo()
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(getFilterRecords(_:)), name: NSNotification.Name(rawValue: "InvoiceAtMiamiFilterApplied"), object: nil)
        //            NotificationCenter.default.addObserver(self, selector: #selector(showFilterVC(_:)), name: Notification.Name("FilterClicked"), object: nil)
        
        cnstTotalRecordsTitleHeight.constant = 0
        viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount)
        viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PACKAGES)

        getInvoiceList()
        tblInvoiceList.dataSource = self
        tblInvoiceList.delegate = self
        tblInvoiceList.estimatedRowHeight = 114
        tblInvoiceList.rowHeight = UITableView.automaticDimension
        tblInvoiceList.estimatedSectionHeaderHeight = 80
        tblInvoiceList.sectionHeaderHeight = UITableView.automaticDimension
        tblInvoiceList.tableFooterView? = UIView()
        btnClearFilterHeightConstant.constant = 0
        btnClearFilter.layer.cornerRadius = 2.0
        btnClearFilter.layer.masksToBounds = true
        btnClearFilter.isHidden = true
        lblNoRecordFound.isHidden = true
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(getLatestRecords(_:)), for: .valueChanged)
        tblInvoiceList.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    // MARK: - getInvoiceList
    func getInvoiceList()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["page_id"] = pageNo
        params["search_content"] = strSearch
        params["search_by"] = strSearchType
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ReceiptList, showLoader: isShowLoader) { (resultDict, status, message) in
            self.tblInvoiceList.isHidden = false
            if status == true
            {
                self.isLoadingList = false
                self.refreshControl.endRefreshing()
                if self.pageNo == 1
                {
                    self.arrList.removeAll()
                }
                let objMyInvoiceList = InvoiceAtMiami.init(fromJson: JSON(resultDict))
                if self.arrList.count > 0
                {
                    self.arrList.append(contentsOf: objMyInvoiceList.list)
                    for _ in 0..<objMyInvoiceList.list.count
                    {
                        self.arrSections.append(0)
                        self.arrSelectedSections.append(0)
                    }
                }
                else
                {
                    self.arrList = objMyInvoiceList.list
                    self.arrSections.removeAll()
                    self.arrSelectedSections.removeAll()
                    for _ in 0..<self.arrList.count
                    {
                        self.arrSections.append(0)
                        self.arrSelectedSections.append(0)
                    }
                }
                self.totalRecordsCount = objMyInvoiceList.totalCount ?? 0
                if self.arrList.count > 0
                {
                    self.lblNoRecordFound.isHidden = true
                    self.btnExportReport.isHidden = false
                }
                else
                {
                    self.lblNoRecordFound.isHidden = false
                    self.btnExportReport.isHidden = true
                }
                self.totalPages = objMyInvoiceList.totalPage ?? 0
                DispatchQueue.main.async {
                    self.tblInvoiceList.reloadData()
                }
                
                if self.totalRecordsCount > 0 {
                    self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
                }else {
                    self.cnstTotalRecordsTitleHeight.constant = 0
                }
                self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
                self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PACKAGES)
            }
            else
            {
                self.refreshControl.endRefreshing()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    @objc func getLatestRecords(_ sender: AnyObject)
    {
        self.pageNo = 1
        getInvoiceList()
    }

    @objc func getFilterRecords(_ notification: NSNotification)
    {
        pageNo = 1
        //      self.arrList.removeAll()
        strSearch = notification.userInfo?["search_text"] as? String ?? ""
        strSearchType = notification.userInfo?["search_type"] as? String ?? ""
        btnClearFilterHeightConstant.constant = 45
        btnClearFilter.isHidden = false
        getInvoiceList()
    }
    func clearData()
    {
        strSearch =  ""
        strSearchType = ""
        pageNo = 1
        //   self.arrList.removeAll()
        self.btnClearFilter.isHidden = true
        btnClearFilterHeightConstant.constant = 0
        tblInvoiceList.layoutIfNeeded()
        getInvoiceList()
    }
    
    @objc func sectionTapped(sender:UITapGestureRecognizer) //new change
    {   let tag = sender.view!.tag

        if arrSections.count > 0
        {
            if arrSections[tag] == 0
            {
                arrSections[tag] = 1
            }
            else
            {
                arrSections[tag] = 0
            }
        }
        DispatchQueue.main.async {
            // UIView.setAnimationsEnabled(true)
            let section = NSIndexSet(index: tag)
            self.tblInvoiceList.reloadSections(section as IndexSet, with: .none)
            self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
            self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PACKAGES)
        }
    }
    func getInvoiceObject(tag:Int) -> InvoiceAtMiamiList
    {
        if arrList.count > 0
        {
            return arrList[tag]
        }
        return InvoiceAtMiamiList()
    }
    // MARK: - IBAction methods
    @IBAction func btnClearFilterClicked()
    {
        clearData()
    }
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnFilterClicked()
    {
        UIView.setAnimationsEnabled(true)
        if let filterVC = appDelegate.getViewController("InvoiceAtMiamiFilterVC", onStoryboard: "Invoice") as? InvoiceAtMiamiFilterVC {
            filterVC.strSearch = self.strSearch
            filterVC.strSearchType = self.strSearchType
            filterVC.modalPresentationStyle = .overFullScreen
            self.present(filterVC,animated: true, completion: nil)
        }
    }
    
    @IBAction func btnExportReportClicked(_ sender: UIButton) {
        print("btnExportReportClicked - InvoiceAtMiamiVC")
        var params : [String:Any] = [:]
        self.showHUD()
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["search_content"] = strSearch
        params["search_by"] = strSearchType
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.exportReceipt, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                if let filePath = (resultDict as NSDictionary).value(forKey: "link") as? String
                {
                    self.downloadFile(InvoiceID:"" ,pdfFileURL:filePath, downloadedFileType: "Report")
                }
                else
                {
                    self.hideHUD()
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}

extension InvoiceAtMiamiVC : UITableViewDataSource,UITableViewDelegate
{
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrSections.count > 0
        {
            if arrSections[indexPath.section] == 1
            {
                return UITableView.automaticDimension
            }
        }
        else
        {
            return 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let HeaderCellIdentifier : String  = "MiamiInvoiceHeaderCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCellIdentifier) as? MiamiInvoiceHeaderCell {
            //tap and gesture
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(sectionTapped(sender:)))
            cell.contentView.addGestureRecognizer(tapGesture)
            cell.contentView.tag = section
            cell.contentView.isUserInteractionEnabled = true
            
            //        cell.btnSection.addGestureRecognizer(tapGesture)
            //new changes
            if arrSections.count > 0
            {
                if  arrSections[section] == 1
                {
                    cell.imgUpDown.image = UIImage(named: "upArrow")
                    cell.longSeperator.isHidden = true
                    cell.shortSeperator.isHidden = false
                }
            }
            else
            {
                cell.imgUpDown.image = UIImage(named: "Down-1")
                cell.longSeperator.isHidden = false
                cell.shortSeperator.isHidden = true
            }
            //data
            if arrList.count > 0
            {
                let objList = getInvoiceObject(tag: section)
                cell.lblDate.text = objList.createdAt ?? ""
                cell.lblInvoiceNo.text = objList.hawbNumber ?? ""
                cell.lblShippingMethod.text = objList.shippingType ?? ""
            }
            return cell.contentView
        }
        return UITableViewCell().contentView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MiamiInvoiceListCell", for: indexPath) as? MiamiInvoiceListCell {
            if arrList.count > 0
            {
                let objList = getInvoiceObject(tag: indexPath.section)
                cell.lblShipperType.text = objList.shipperName ?? ""
                cell.lblTrackingNo.text = objList.trackingNumber ?? ""
                cell.lblWeight.text = "\(objList.weight ?? "0") \(kWeightUnit)"
                cell.lblStatus.text = objList.status ?? ""
                cell.lblDesc.text = objList.descriptionField ?? ""
            }
            //long press gesture
            cell.contentView.tag = indexPath.section
            return cell
        }
        return UITableViewCell()
    }
    
    //Pagination
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isLoadingList = false
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (Int(tblInvoiceList.contentOffset.y + tblInvoiceList.frame.size.height) >= Int(tblInvoiceList.contentSize.height))
        {
            if !isLoadingList{
                isLoadingList = true
                if self.totalPages == pageNo
                {
                    return
                }
                pageNo += 1
                getInvoiceList()
            }
        }
    }
}

class MiamiInvoiceHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblInvoiceNo : TapAndCopyLabel!
    @IBOutlet weak var lblShippingMethod : UILabel!
    @IBOutlet weak var longSeperator : UILabel!
    @IBOutlet weak var shortSeperator : UILabel!
//    @IBOutlet weak var btnSection : UIButton!
    @IBOutlet weak var imgUpDown : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class MiamiInvoiceListCell : UITableViewCell
{
    @IBOutlet weak var lblShipperType : UILabel!
    @IBOutlet weak var lblTrackingNo : TapAndCopyLabel!
    @IBOutlet weak var lblWeight : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    override func awakeFromNib() {
        
    }
}
