//
//  ChangePasswordVC.swift
//  CSFCustomer
//
//  Created by Tops on 13/01/21.
//

import UIKit

class ChangePasswordVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var txtOldPassword : UITextField!
    @IBOutlet weak var txtNewPassword : UITextField!
    @IBOutlet weak var txtConfirmPassword : UITextField!
    @IBOutlet weak var lblPwdDesc : UILabel!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnSubmit : UIButton!
    @IBOutlet weak var topBarView : UIView!
    
    // MARK: - Global Variable
    var arrTextFields : [UITextField] =  [UITextField]()
    var userID : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        topBarView.topBarBGColor()
        arrTextFields = [txtOldPassword,txtNewPassword,txtConfirmPassword]
        for item in arrTextFields
        {
            item.layer.cornerRadius = 2
            item.layer.borderWidth = 1
            item.layer.borderColor = Colors.theme_lightgray_color.cgColor
            item.layer.masksToBounds = true
            item.paddingView(xvalue: 10)
        }
        btnCancel.layer.cornerRadius = 2.0
        btnCancel.layer.masksToBounds = true
        btnCancel.layer.borderColor = Colors.theme_lightgray_color.cgColor
        btnCancel.layer.borderWidth = 1
      
        btnSubmit.layer.cornerRadius = 2.0
        btnSubmit.layer.masksToBounds = true
        lblPwdDesc.text = Messsages.msg_pwd_note
    }
    
    // MARK: - callAPIForChangePassword
    func callAPIForChangePassword()
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["new_password"] = txtNewPassword.text ?? ""
        params["confirm_new_password"] = txtConfirmPassword.text ?? ""
        params["old_password"] = txtOldPassword.text ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ChangePassword, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.showAlertForChangePassword(title: "", msg: message)
            }
            else
            {
               appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    /**
     Shows an alert with the specified title and message for the change password functionality.
     */
    func showAlertForChangePassword(title:String,msg:String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }
        okAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtOldPassword.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_old_pwd, bottomValue: getSafeAreaValue())
            return false
        }
        else if txtOldPassword.text!.count > 12
        {
            appDelegate.showToast(message: Messsages.msg_old_pwd_length, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtNewPassword.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_new_pwd, bottomValue: getSafeAreaValue())
            return false
        }
        else if !isValidNewPassword(strPwd: txtNewPassword.text!)
        {
            appDelegate.showToast(message: Messsages.msg_pwd_rules, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtConfirmPassword.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_confirm_pwd, bottomValue: getSafeAreaValue())
            return false
        }
        else if txtNewPassword.text != txtConfirmPassword.text
        {
            appDelegate.showToast(message: Messsages.msg_pwd_mismatch, bottomValue: getSafeAreaValue())
            return false
        }
        
        return true
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCancelClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitClicked()
    {
        self.view.endEditing(true)
        if isValidData()
        {
            callAPIForChangePassword()
        }
    }
}
