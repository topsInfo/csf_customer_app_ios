//
//  UserChangePasswordVC.swift
//  CSFCustomer
//
//  Created by Tops on 18/01/21.
//

import UIKit

class UserChangePasswordVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var txtNewPassword : UITextField!
    @IBOutlet weak var txtConfirmPassword : UITextField!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnSubmit : UIButton!
    @IBOutlet weak var topBarView : UIView!

    // MARK: - Global Variable
    var arrTextFields : [UITextField] =  [UITextField]()
    var userID : String = ""
    var childUserID : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        topBarView.topBarBGColor()
        arrTextFields = [txtNewPassword,txtConfirmPassword]
        for item in arrTextFields
        {
            item.layer.cornerRadius = 2
            item.layer.borderWidth = 1
            item.layer.borderColor = Colors.theme_lightgray_color.cgColor
            item.layer.masksToBounds = true
            item.paddingView(xvalue: 10)
        }
        btnCancel.layer.cornerRadius = 2.0
        btnCancel.layer.masksToBounds = true
        btnCancel.layer.borderColor = Colors.theme_lightgray_color.cgColor
        btnCancel.layer.borderWidth = 1
        
        btnSubmit.layer.cornerRadius = 2.0
        btnSubmit.layer.masksToBounds = true
    }
    
    // MARK: - callAPIForUserChangePassword
    func callAPIForUserChangePassword()
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["child_user_id"] = childUserID
        params["new_password"] = txtNewPassword.text ?? ""
        params["confirm_new_password"] = txtConfirmPassword.text ?? ""
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.UserChangePassword, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.showAlertForChangePassword(title: "", msg: message)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    /**
     Shows an alert for changing the password.
     */    
    func showAlertForChangePassword(title:String,msg:String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }
        okAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtNewPassword.text!)
        {
            showAlert(title: "", msg: Messsages.msg_enter_new_pwd, vc: self)
            return false
        }
        else if txtNewPassword.text!.count < 6 || txtNewPassword.text!.count > 12
        {
            showAlert(title: "", msg: Messsages.msg_pwd_length, vc: self)
            return false
        }
        else if isCheckNull(strText: txtConfirmPassword.text!)
        {
            showAlert(title: "", msg: Messsages.msg_enter_confirm_pwd, vc: self)
            return false
        }
        else if txtNewPassword.text != txtConfirmPassword.text
        {
            showAlert(title: "", msg: Messsages.msg_pwd_mismatch, vc: self)
            return false
        }
        return true
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCancelClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitClicked()
    {
        self.view.endEditing(true)
        if isValidData()
        {
            callAPIForUserChangePassword()
        }
    }
}
