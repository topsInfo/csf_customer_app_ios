//
//  RegisterVC.swift
//  CSFCustomer
//
//  Created by Tops on 04/02/21.
//

import UIKit
import WebKit
import InputMask

class RegisterVC: UIViewController,WKNavigationDelegate,MaskedTextFieldDelegateListener
{
    // MARK: - IBOutlets
    @IBOutlet weak var firstnameView : UIView!
    @IBOutlet weak var lastnameView : UIView!
    @IBOutlet weak var emailView : UIView!
    @IBOutlet weak var mobileView : UIView!
    @IBOutlet weak var companyView : UIView!
    @IBOutlet weak var addressView : UIView!
    @IBOutlet weak var txtFirstname : UITextField!
    @IBOutlet weak var txtLastname : UITextField!
    @IBOutlet weak var txtCompanyname : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtMobile : UITextField!
    @IBOutlet weak var txtAddress : UITextField!
    @IBOutlet weak var btnSignup : UIButton!
    @IBOutlet weak var listener: MaskedTextFieldDelegate!
    
    //Promotional Code
    @IBOutlet weak var promotionalCodeView : UIView!
    @IBOutlet weak var txtPromotionalCode : UITextField!
    @IBOutlet weak var stackViewPromotionalCode: UIStackView!

    // MARK: - Global Variable
    var arrViews = [UIView]()
    var defaultPhoneValue : String = "1 868 XXX XXXX" //"1 (868) XXX-XXXX"
    var addressParams : [String:Any] = [String:Any]()
    
    var strPromotionalCode : String = ""
    var isValidPromotionalCodeFound : Bool = false
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        managePromotionalCodeInputField()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.removeObserver(self)

        NotificationCenter.default.addObserver(self, selector: #selector(setAddress(_:)), name: NSNotification.Name(rawValue: "GetAddressData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI(_:)), name: Notification.Name("dynamicLinkClicked"), object: nil)

        setPhoneMaskFormat()
        btnSignup.layer.cornerRadius = 2
        btnSignup.layer.masksToBounds = true
        arrViews = [firstnameView,lastnameView,emailView,mobileView,promotionalCodeView,companyView,addressView]
        for view in arrViews
        {
            view.layer.borderWidth = 1
            view.layer.cornerRadius = 2
            view.layer.masksToBounds = true
            view.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
        }
        txtMobile.text = defaultPhoneValue
        txtFirstname.autocapitalizationType = .words
        txtLastname.autocapitalizationType = .words
        txtCompanyname.autocapitalizationType = .words
        
        txtPromotionalCode.delegate = self
    }
    
    // MARK: - updateUI
    @objc func updateUI(_ notification: NSNotification){
        txtPromotionalCode.text = ""
        managePromotionalCodeInputField()
    }
    
    // MARK: - managePromotionalCodeInputField
    func managePromotionalCodeInputField(){
        txtPromotionalCode.keyboardType = .asciiCapable
        txtPromotionalCode.autocapitalizationType = .allCharacters
        if UserDefaults.standard.value(forKey: "invitedby") != nil
        {
            stackViewPromotionalCode.isHidden = true
        }else {
            stackViewPromotionalCode.isHidden = false
        }
    }
    
    // MARK: - setPhoneMaskFormat
    func setPhoneMaskFormat()
    {
        listener.affinityCalculationStrategy = .wholeString
        listener.primaryMaskFormat = "1 [000] [000] [0000]"
        listener.affineFormats = [
            "1 [000] [000] [0000]",
        ]
    }
    
    // MARK: - setAddress
    @objc func setAddress(_ notification:NSNotification)
    {
        txtAddress.text = notification.userInfo?["deliveryOption"] as? String ?? ""
        addressParams = notification.userInfo as? [String:Any] ?? [:]
    }
    
    // MARK: - callAPIForSignUp
    func callAPIForSignUp()
    {
        //new change
        var strMobileNo = txtMobile.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        strMobileNo = String(strMobileNo!.dropFirst(2))
        //over
        var params : [String:Any] = [:]
        params["first_name"] =  txtFirstname.text ?? ""
        params["last_name"]  =  txtLastname.text ?? ""
        params["company_name"]  = txtCompanyname.text ?? ""
        params["email_address"]  = txtEmail.text ?? ""
        params["mobile_no"]  = strMobileNo //txtMobile.text ?? ""
        params["referral_code"] = UserDefaults.standard.value(forKey: "invitedby") as? String  ?? ""
        params["office_id"] = addressParams["office_id"] ?? ""
        params["type_of_title"] = addressParams["type_of_title"] ?? ""
        params["other_title"] = addressParams["other_title"] ?? ""
        params["address1"] = addressParams["address1"] ?? ""
        params["address2"] = addressParams["address2"] ?? ""
        params["area_id"] = addressParams["area_id"] ?? ""
        
        if isValidPromotionalCodeFound {
            params["promotional_code"] = self.strPromotionalCode
        }
        
        print("params are \(params)")
        
        self.showHUDOnView(view: self.view)
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.Register, showLoader:false) { (resultDict, status, message) in
            self.hideHUDFromView(view: self.view)
            if status == true
            {
                if UserDefaults.standard.value(forKey: "invitedby") != nil
                {
                    UserDefaults.standard.removeObject(forKey: "invitedby")
                }
                if let vc = appDelegate.getViewController("RegisterSuccessVC", onStoryboard: "Main") as? RegisterSuccessVC {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtFirstname.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_firstname, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtLastname.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_lastname, bottomValue: getSafeAreaValue())
            return false
        }
        if isCheckNull(strText: txtEmail.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_email, bottomValue: getSafeAreaValue())
            return false
        }
        else if !isValidEmail(testStr: txtEmail.text!)
        {
            appDelegate.showToast(message: Messsages.msg_invalid_email, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtMobile.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_mobile, bottomValue: getSafeAreaValue())
            return false
        }
        else if txtMobile.text == defaultPhoneValue
        {
            appDelegate.showToast(message: Messsages.msg_mobile_length, bottomValue: getSafeAreaValue())
            return false
        }
        else if self.txtMobile.text!.count < 14
        {
            appDelegate.showToast(message: Messsages.msg_mobile_length, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtAddress.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_addresses, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    // MARK: - IBAction methods
    @IBAction func btnAddAddressClicked()
    {
        self.view.endEditing(true)
        if let vc = appDelegate.getViewController("AddressVC", onStoryboard: "Profile") as? AddressVC {
            //        vc.objMyAddress = self.objMyAddress
            // vc.userID = objUserInfo.id ?? ""
            vc.type = "Register"
            vc.addTitle = "Add New Address"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnSignupClicked()
    {
        self.view.endEditing(true)
        if isValidData()
        {
            self.strPromotionalCode = txtPromotionalCode.text ?? ""
            isValidPromotionalCodeFound = false
            
            if self.strPromotionalCode.count > 0 {
                if isValidatePromotionalCode(promotionalCode: self.strPromotionalCode) {
                    appDelegate.callAPIForValidatePromotionalCode(promotionalCode: self.strPromotionalCode, fromVC: self) { [self] resultDict, status, message in
                        if status {
                            isValidPromotionalCodeFound = true
                            callAPIForSignUp()
                        }else{
                            appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                        }
                    }
                }
            }else{
                callAPIForSignUp()
            }
        }
    }
    
    @IBAction func btnSignInClicked()
    {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension RegisterVC : UITextFieldDelegate {
    // MARK: - Textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                return true
            }
        }
        if textField == txtMobile
        {
            return newLength <= 10
        }
        else if textField == txtFirstname || textField == txtLastname
        {
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_")
            if newLength <= 50
            {
                if string.rangeOfCharacter(from: set) != nil
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }else if textField == txtPromotionalCode {
            if string == "" {
                // User presses backspace
                textField.deleteBackward()
            } else {
                // User presses a key or pastes
                textField.insertText(string.uppercased())
            }
            // Do not let specified text range to be changed
            return false
        }
        return true
    }
}
