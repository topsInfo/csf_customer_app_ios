//
//  MainLoginVC.swift
//  CSFCustomer
//
//  Created by Tops on 30/03/21.
//

import UIKit
import CoreData

class MainLoginVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var userView : UIView!
    @IBOutlet weak var signupSignInContainer : UIView!
    @IBOutlet weak var tblUserList : UITableView!
    @IBOutlet weak var footerView : UIView!

    // MARK: - Global Variable
    var arrSwitchUserList : [SwitchAccount] = [SwitchAccount]()
    var arrSwitchUserActiveList : [SwitchAccount] = [SwitchAccount]()
    var arrSwitchUserInactiveList : [SwitchAccount] = [SwitchAccount]()
    let footerViewHeight : Int = 92
    var activeUserCount : Int = 0
    var isAmazonLoginExists : Bool = false
    var isAppleLoginExists : Bool  = false
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        
//        let msg : String = "\(apiVersion)\n\(baseURL)\n\(stripePublishKey)\n\(kBotID)\n\(yellowMessangerWhatsAppURL)"
//        showAlert(title: "", msg: msg, vc: self)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(fetchRecords(_:)), name: NSNotification.Name(rawValue: "DBRecordDeleted"), object: nil)
        tblUserList.isHidden = true
        signupSignInContainer.isHidden = true
        tblUserList.dataSource = self
        tblUserList.delegate = self
        userView.layer.cornerRadius = userView.frame.size.height/2
        userView.layer.borderWidth = 1
        userView.layer.borderColor = Colors.theme_orange_color.cgColor
        userView.clipsToBounds = true
        fetchRecords()
    }
    
    // MARK: - fetchRecords
    func fetchRecords()
    {
        arrSwitchUserList.removeAll()
        arrSwitchUserActiveList.removeAll()
        arrSwitchUserInactiveList.removeAll()
        activeUserCount = 0
        isAmazonLoginExists = false
        isAppleLoginExists = false
        let objManageCoreData = ManageCoreData()
        let result =  objManageCoreData.fetchData(entityName: kSwitchUserEntity)
        if result.count > 0
        {
            for data in result as! [NSManagedObject]
            {
                let objSwitchAccount = SwitchAccount()
                objSwitchAccount.userID = data.value(forKey: SwitchUserInfo.userID.rawValue) as? String ?? ""
                objSwitchAccount.uniqueID = data.value(forKey: SwitchUserInfo.uniqueID.rawValue) as? String ?? ""
                objSwitchAccount.boxID = data.value(forKey: SwitchUserInfo.boxID.rawValue) as? String ?? ""
                objSwitchAccount.imgUser = data.value(forKey: SwitchUserInfo.imgUser.rawValue) as? String ?? ""
                objSwitchAccount.isDefault =  data.value(forKey: SwitchUserInfo.isDefault.rawValue) as? Bool ?? false
                objSwitchAccount.firstName = data.value(forKey: SwitchUserInfo.firstName.rawValue) as? String ?? ""
                objSwitchAccount.lastName = data.value(forKey: SwitchUserInfo.lastName.rawValue) as? String ?? ""
                objSwitchAccount.authToken = data.value(forKey: SwitchUserInfo.authToken.rawValue) as? String ?? ""
                objSwitchAccount.isActive = data.value(forKey: SwitchUserInfo.isActive.rawValue) as? Bool ?? false
                objSwitchAccount.LoginType =  data.value(forKey: SwitchUserInfo.loginType.rawValue) as? String ?? ""
                if objSwitchAccount.LoginType == LoginType.AmazonLogin.rawValue && objSwitchAccount.isActive == true
                {
                    isAmazonLoginExists = true
                }
                if objSwitchAccount.LoginType == LoginType.AppleLogin.rawValue && objSwitchAccount.isActive == true
                {
                    isAppleLoginExists = true
                }
                if objSwitchAccount.isActive == true
                {
                    activeUserCount += 1
                    arrSwitchUserActiveList.append(objSwitchAccount)
                }
                else
                {
                    arrSwitchUserInactiveList.append(objSwitchAccount)
                }
                /* print("\(objSwitchAccount.userID)")
                 print("\(objSwitchAccount.uniqueID)")
                 print("\(objSwitchAccount.boxID)")
                 print("\(objSwitchAccount.imgUser)")
                 print("\(objSwitchAccount.isDefault)")
                 print("\(objSwitchAccount.isActive)") */
                
                // arrSwitchUserList.append(objSwitchAccount)
                
            } //for over
            var sortedActiveArray : [SwitchAccount] = [SwitchAccount]()
            var sortedInActiveArray : [SwitchAccount] = [SwitchAccount]()
            if arrSwitchUserActiveList.count > 0
            {
                sortedActiveArray = arrSwitchUserActiveList.sorted { $0.firstName < $1.firstName }
                // activeUserCount = arrSwitchUserActiveList.count
            }
            if arrSwitchUserInactiveList.count > 0
            {
                sortedInActiveArray = arrSwitchUserInactiveList.sorted { $0.firstName < $1.firstName }
                //  inactiveUserCount = arrSwitchUserInactiveList.count
            }
            arrSwitchUserList = sortedActiveArray + sortedInActiveArray
            if arrSwitchUserList.count > 0
            {
                DispatchQueue.main.async { [self] in
                    tblUserList.isHidden = false
                    signupSignInContainer.isHidden = true
                }
                
            }
            else
            {
                DispatchQueue.main.async { [self] in
                    tblUserList.isHidden = true
                    signupSignInContainer.isHidden = false
                }
            }
            DispatchQueue.main.async {
                self.tblUserList.reloadData()
            }
        }
        else
        {
            DispatchQueue.main.async { [self] in
                tblUserList.isHidden  = true
                signupSignInContainer.isHidden = false
            }
        }
    }
    @objc func fetchRecords(_ notification: NSNotification)
    {
        fetchRecords()
    }
    
    // MARK: - Tableview delegate methods
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(footerViewHeight)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSwitchUserList.count
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerView.frame = CGRect(x: 0, y: 0, width: Int(self.tblUserList.frame.size.width), height:footerViewHeight)
        return footerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchProfileCell", for: indexPath) as? SwitchProfileCell {
            let objSwitchAccount = arrSwitchUserList[indexPath.row]
            cell.lblName.text = objSwitchAccount.firstName  + " " + objSwitchAccount.lastName
            
            cell.imgUser.setImage(objSwitchAccount.imgUser, placeHolder: UIImage(named: "default-image"), isShowLoader: false)
            cell.imgUser.contentMode = .scaleAspectFill
            
            cell.lblBoxID.text = "BOX ID : \(objSwitchAccount.boxID)"
            cell.lblStatus.text = objSwitchAccount.isActive == false ? "Inactive" : "Active"
            if let loginType = objSwitchAccount.LoginType as? String
            {
                if loginType == LoginType.AppLogin.rawValue
                {
                    cell.imgLoginType.image = UIImage(named: "logo")
                    cell.containerView.backgroundColor = Colors.theme_white_color
                }
                else if loginType == LoginType.AmazonLogin.rawValue
                {
                    cell.imgLoginType.image = UIImage(named: "amazon")
                    cell.containerView.backgroundColor = Colors.theme_light_yellow_color
                }
                else if loginType == LoginType.AppleLogin.rawValue
                {
                    cell.imgLoginType.image = UIImage(named: "apple-login")
                    cell.containerView.backgroundColor = Colors.theme_black_color
                }
            }
            cell.btnRemove.tag = indexPath.row
            cell.btnRemove.addTarget(self, action: #selector(btnRemoveClicked(sender:)), for: .touchUpInside)
            cell.btnRow.tag = indexPath.row
            cell.btnRow.addTarget(self, action: #selector(btnRowClicked(sender:)), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
    @objc func btnRowClicked(sender:UIButton)
    {
        let objSwitchAccount = arrSwitchUserList[sender.tag]
        if let status = objSwitchAccount.isActive as? Bool, status == true
        {
            callAPIForSwitchProfile(objSwitchAccount:objSwitchAccount)
        }
        else
        {
            if let vc = appDelegate.getViewController("LoginVC", onStoryboard: "Main") as? LoginVC {
                vc.boxID = objSwitchAccount.boxID
                vc.loginType = objSwitchAccount.LoginType
                vc.isAmazonLoginExists = isAmazonLoginExists
                vc.isAppleLoginExists = isAppleLoginExists
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    @objc func btnRemoveClicked(sender:UIButton)
    {
        let objSwitchAccount = arrSwitchUserList[sender.tag]
        self.popupAlert(title: "", message: "Are you sure you want to remove \(objSwitchAccount.boxID) account?", actionTitles: ["Yes","No"], actions:[{action1 in
            if let status = objSwitchAccount.isActive as? Bool, status == true
            {
                //UserDefaults.standard.setValue("AppLogin", forKey: kTypeOfLogin) //need to change
                self.callAPIForLogout(userID: objSwitchAccount.userID,isAction: "Remove",boxID: objSwitchAccount.boxID,token:objSwitchAccount.authToken )
            }
            else
            {
                let objManageCoreData = ManageCoreData()
                objManageCoreData.deleteRecord(entityName: kSwitchUserEntity, BoxId: objSwitchAccount.boxID) { (status) in
                    if status == true
                    {
                        self.fetchRecords()

//                        This old code is now replaced by below new code - SegmentOptions
//                        let decodedSegments  = UserDefaults.standard.data(forKey: kArrSegmentOptons)
//                        if decodedSegments != nil {
//                            if let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decodedSegments!) as? [SegmentOptions] {
//                                appDelegate.arrOptions = decodedTeams
//                            }
//                        }
                        
                        if let arrData = UserDefaults.standard.decode(for: [SegmentOptions].self, using: String(describing: SegmentOptions.self)){
                            appDelegate.arrOptions = arrData
                        }

                        getSegmentOptons()
                    }
                    else
                    {
                        
                    }
                }
                
            }
        },{ action2 in
            
        }])
    }
    
    // MARK: - IBAction methods
    @IBAction func btnCreateAccountClicked()
    {
        if activeUserCount >= 5 //need to check
        {
            showAlert(title: "", msg: Messsages.msg_switch_user, vc: self)
        }
        else
        {
            if let vc = appDelegate.getViewController("LoginVC", onStoryboard: "Main") as? LoginVC {
                vc.isFromVC = ""
                vc.isAmazonLoginExists = isAmazonLoginExists
                vc.isAppleLoginExists = isAppleLoginExists
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    @IBAction func btnSignInClicked()
    {
        if let vc = appDelegate.getViewController("LoginVC", onStoryboard: "Main") as? LoginVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnSignUpClicked()
    {
        if let vc = appDelegate.getViewController("RegisterVC", onStoryboard: "Main") as? RegisterVC{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

class SwitchProfileCell : UITableViewCell
{
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblBoxID : UILabel!
    @IBOutlet weak var btnRemove : UIButton!
    @IBOutlet weak var btnRow : UIButton!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var imgLoginType :UIImageView!
    override func awakeFromNib() {
        imgUser.layer.cornerRadius = imgUser.frame.size.height/2
        containerView.layer.cornerRadius = containerView.frame.size.height/2
        btnRemove.layer.cornerRadius = btnRemove.frame.size.height/2
        btnRemove.layer.borderColor = UIColor(named: "theme_orange_color")?.cgColor
        btnRemove.layer.borderWidth = 1
    }
}
