//
//  ViewController.swift
//  CSFCustomer
//
//  Created by Tops on 23/11/20.
//

import UIKit
import SwiftyJSON
import LoginWithAmazon
import AuthenticationServices
import LocalAuthentication
import CoreData
class LoginVC: UIViewController
{
    // MARK: - IBOutlets
    @IBOutlet weak var userContainerView : UIView!
    @IBOutlet weak var txtUsername : UITextField!
    @IBOutlet weak var txtShowPassword : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    @IBOutlet weak var btnShowPassword :UIButton!
    @IBOutlet weak var btnHidePassword :UIButton!
    @IBOutlet weak var passwordContainerView :UIView!
    @IBOutlet weak var passwordStackView : UIStackView!
    @IBOutlet weak var showPasswordStackView : UIStackView!
    @IBOutlet weak var mainContainerView : UIView!
    @IBOutlet weak var img : UIImageView!
    @IBOutlet weak var imgRemember : UIImageView!
    @IBOutlet weak var btnRemember : UIButton!
    @IBOutlet weak var btnBiometricLogin : UIButton!
    @IBOutlet weak var topView : UIView!
    @IBOutlet weak var btnApple : UIButton!
    @IBOutlet weak var btnAmazon : UIButton!

    @IBOutlet weak var viewConnectWith: UIView!
    @IBOutlet weak var viewOtherLogin: UIView!
    
    // MARK: - Global Variable
    let laContext = LAContext()
    var authError: NSError?
    var isFromVC : String = ""
    var boxID : String = ""
    var isAmazonLoginExists : Bool = false
    var isAppleLoginExists : Bool = false
    var loginType : String = ""
    var userID : String = ""
    var loginUniqueID : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        txtPassword.text = nil
        txtShowPassword.text = nil
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        let objManageCoreData = ManageCoreData()
        let results = objManageCoreData.recordExists(BoxId:boxID , entityName: kSwitchUserEntity)
        if results.count > 0
        {
            if let records = results as? [NSManagedObject]
            {
                let item = records[0]
                if let loginType =  item.value(forKey: SwitchUserInfo.loginType.rawValue) as? String
                {
                    if loginType == LoginType.AppLogin.rawValue
                    {
                        if let isBiometricEnabled = item.value(forKey: SwitchUserInfo.isBiometricEnabled.rawValue) as? Bool,isBiometricEnabled == true
                        {
                            userID = item.value(forKey: SwitchUserInfo.userID.rawValue) as? String ?? ""
                            loginUniqueID = item.value(forKey: SwitchUserInfo.uniqueID.rawValue) as? String ?? ""
                            checkForBiometricAvailability()
                        }
                    }
                }
            }
        }
        //new change in apr-2021
        txtUsername.text = boxID
        if isAmazonLoginExists == true
        {
            btnAmazon.isHidden = true
        }
        if isAppleLoginExists == true
        {
            btnApple.isHidden = true
        }
        if isAppleLoginExists && isAmazonLoginExists {
            self.viewConnectWith.isHidden = true
            self.viewOtherLogin.isHidden = true
        }else{
            self.viewConnectWith.isHidden = false
            self.viewOtherLogin.isHidden = false
        }
        
        if loginType == LoginType.AmazonLogin.rawValue
        {
            showAlert(title: "", msg: "\(boxID) is an Amazon user account.Please login with Amazon credential.", vc: self)
            txtUsername.text = ""
        }
        else if loginType == LoginType.AppleLogin.rawValue
        {
            showAlert(title: "", msg: "\(boxID) is Apple user account.Please sign in with Apple.", vc: self)
            txtUsername.text = ""
        }
        userContainerView.layer.borderWidth = 1.0
        userContainerView.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor
        passwordContainerView.layer.borderWidth = 1.0
        passwordContainerView.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor
        txtUsername.paddingView(xvalue: 2)
        txtPassword.paddingView(xvalue: 2)
        txtShowPassword.paddingView(xvalue: 2)
        showPasswordStackView.isHidden = true
        configureTextField(textField: txtUsername, placeHolder: "Enter Your Box ID", font:fontname.openSansRegular, fontSize: 14, textColor:  UIColor(named:"theme_text_color")!, cornerRadius: 0,borderColor:Colors.clearColor,borderWidth:0,bgColor:UIColor(named:"theme_textfield_bgcolor")!)
        configureTextField(textField: txtShowPassword, placeHolder: "Password", font:fontname.openSansRegular, fontSize: 14, textColor:  UIColor(named:"theme_text_color")!, cornerRadius: 0,borderColor:Colors.clearColor,borderWidth:0,bgColor:UIColor(named:"theme_textfield_bgcolor")!)
        configureTextField(textField: txtPassword, placeHolder: "Password", font:fontname.openSansRegular, fontSize: 14, textColor: UIColor(named:"theme_text_color")!, cornerRadius: 0,borderColor:Colors.clearColor,borderWidth:0,bgColor:UIColor(named:"theme_textfield_bgcolor")!)
        btnApple.layer.cornerRadius = btnApple.frame.size.height / 2
        btnApple.layer.masksToBounds = true
        btnAmazon.layer.cornerRadius = btnAmazon.frame.size.height / 2
        btnAmazon.layer.masksToBounds = true
    }
    
    // MARK: - checkForBiometricAvailability
    func checkForBiometricAvailability()
    {
        if laContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError)
        {
            if authError != nil {
                // handle error
                // showAlert(title: "", msg: Messsages.msg_pls_try_again, vc: self)
            }
            else
            {
                if #available(iOS 11.0, *) {
                    0.1
                }
                do {
                    if laContext.biometryType == .faceID {
                        //localizedReason = "Unlock using Face ID"
                        openBioAuthenticationVC(type:"face")
                        //print("FaceId support")
                    } else if laContext.biometryType == .touchID {
                        //localizedReason = "Unlock using Touch ID"
                        openBioAuthenticationVC(type:"finger")
                        //print("TouchId support")
                    } else {
                        showAlert(title: "", msg: Messsages.msg_no_biometric, vc: self)
                    }
                } //do over
            } //else over
        }
        else
        {
            // Fallback on earlier versions
            showAlert(title: "", msg: Messsages.msg_biometric_error, vc: self)
        }
    }
    
    // MARK: - openBioAuthenticationVC
    func openBioAuthenticationVC(type:String)
    {
        if let vc = appDelegate.getViewController("BiometricAuthenticationVC", onStoryboard: "Main") as? BiometricAuthenticationVC {
            vc.userID = userID
            vc.loginUniqueID = loginUniqueID
            if #available(iOS 13.0, *) {
                vc.isModalInPresentation = true
            } else {
                // Fallback on earlier versions
            }
            if type == "face"
            {
                vc.typeOfBiometric = 2
            }
            else if type == "finger"
            {
                vc.typeOfBiometric = 1
            }
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true)
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnLoginWithAmazonClicked(_ sender: Any)
    {
        self.showHUD()
        let request = AMZNAuthorizeRequest()
        request.scopes = [AMZNProfileScope.profile(), AMZNProfileScope.postalCode()]
        
        // Make an Authorize call to the Login with Amazon SDK.
        AMZNAuthorizationManager.shared().authorize(
            request,
            withHandler: { result, userDidCancel, error in
                self.hideHUD()
                if error != nil
                {
                    self.hideHUD()
                    self.showAlert(title: "", msg:  "There is an error with Amazon login.Please try after some time.", vc: self)
                }
                else if userDidCancel
                {
                    self.hideHUD()
                    self.showAlert(title: "", msg: Messsages.msg_cancle_action, vc: self)
                }
                else
                {
                    // Authentication was successful.
                    // Obtain the access token and user profile data.
                    let user = result?.user
                    let userID = user?.userID ?? ""
                    let name = result?.user?.name ?? ""
                    let email = result?.user?.email ?? ""
                    self.callAPIForAmazonLogin(name:name,email:email,amazonID:userID)
                }
            })
    }
    @IBAction func btnSignInWithAppleClicked()
    {
        if #available(iOS 13.0, *) {
            let request = ASAuthorizationAppleIDProvider().createRequest()
            request.requestedScopes = [.fullName, .email]
            let controller = ASAuthorizationController(authorizationRequests: [request])
            controller.delegate = self
            controller.presentationContextProvider = self
            controller.performRequests()
        } else {
            showAlert(title: "", msg: Messsages.msg_not_supported_apple_signin, vc: self)
        }
    }
    @IBAction func btnRegisterClicked()
    {
        if let vc = appDelegate.getViewController("RegisterVC", onStoryboard: "Main") as? RegisterVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnRememberMeClicked(_ sender:UIButton)
    {
        sender.isSelected = !sender.isSelected
        if sender.isSelected
        {
            imgRemember.image = UIImage(named: "checked")
            UserDefaults.standard.setValue(true, forKey: kRememberMe)
        }
        else
        {
            imgRemember.image = UIImage(named: "unchecked")
            UserDefaults.standard.setValue(false, forKey: kRememberMe)
        }
    }
    @IBAction func btnBiometricLoginClicked(_ sender:UIButton)
    {
        if let vc = appDelegate.getViewController("BiometricAuthenticationVC", onStoryboard: "Main") as? BiometricAuthenticationVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - callAPIForAmazonLogin
    func callAPIForAmazonLogin(name:String,email:String,amazonID:String)
    {
        var params : [String:Any] = [:]
        params["device_token"] = UserDefaults.standard.value(forKey: kDeviceToken) as? String ?? ""
        params["device_type"] = kDeviceType
        params["name"] = name
        params["email"] = email
        params["amazon_id"] = amazonID
        params["referral_code"] = UserDefaults.standard.value(forKey: "invitedby") as? String  ?? ""
        params["device_name"] = kDeviceName
        params["device_brand"] = kDeviceBrand
        params["os_version"] = kOSVersion
        params["app_version"] = kVersion
        params["api_version"] = apiVersion
        // print(params)
        URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.LoginWithAmazon, showLoader: true) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()
                let objUserInfo = UserInfo(fromJson: JSON(resultDict))
                UserDefaults.standard.setValue((resultDict as NSDictionary).value(forKey: "token"), forKey: kAuthToken)
                UserDefaults.standard.setValue(true, forKey: kLoggedIn)
                //Nand : New Feature Updates Code
                UserDefaults.standard.setValue(true, forKey: kShowFeatureMsg)
                UserDefaults.standard.setValue(LoginType.AmazonLogin.rawValue,forKey: kTypeOfLogin)
                
//                This old code is now replaced by below new code
//                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: objUserInfo)
//                UserDefaults.standard.set(encodedData, forKey: kUserInfo)
//                self.saveCoreData(objUserInfo:objUserInfo,TypeOfLogin:LoginType.AmazonLogin.rawValue) //new change core data
                
                do {
                    let encodedData = try NSKeyedArchiver.archivedData(withRootObject: objUserInfo, requiringSecureCoding: true)
                    UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                    self.saveCoreData(objUserInfo:objUserInfo,TypeOfLogin:LoginType.AmazonLogin.rawValue) //new change core data
                } catch {
                    print("NSKeyedArchiver Error is : \(error.localizedDescription)")
                }

                if UserDefaults.standard.value(forKey: "invitedby") != nil
                {
                    UserDefaults.standard.removeObject(forKey: "invitedby")
                }

                if let isProfileRequired = objUserInfo.profileRequired, isProfileRequired == 0
                {
                    self.goToHomeVC()
                }
                //isProfileRequired - Flow Change -by Nand
                else if let isProfileRequired = objUserInfo.profileRequired, isProfileRequired == 1 {
                    self.goToEditProfileVCFromLogin(userID: objUserInfo.id ?? "")
                }
                else
                {
                    //new change
                    UIApplication.shared.windows.first?.rootViewController = appDelegate.setUpSWRevealViewController()
                    //                    appDelegate.window?.rootViewController = appDelegate.setUpSWRevealViewController()
                    //over
                    
                    //self.goToEditProfileVC(userID:objUserInfo.id ?? "")
                }
                
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - callAPIForAppleLogin
    func callAPIForAppleLogin(email:String,appleID:String,name:String,lastName:String)
    {
        if email.isEmpty {
            //If Apple login and email address not found then we will redirect to setting of device.
            showAlertWithOk(title: "Message", msg: Messsages.apple_login_info_require_msg, vc: self) { status in
                if status {
                    guard let settingsUrl = URL(string: "App-Prefs:root=General") else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }
            }
            return
        }
        
        var params : [String:Any] = [:]
        params["device_token"] = UserDefaults.standard.value(forKey: kDeviceToken) as? String ?? ""
        params["device_type"] = kDeviceType
        params["email"] = email
        params["apple_id"] = appleID
        params["name"] = name
        params["last_name"] = lastName
        params["referral_code"] = UserDefaults.standard.value(forKey: "invitedby") as? String  ?? ""
        params["device_name"] = kDeviceName
        params["device_brand"] = kDeviceBrand
        params["os_version"] = kOSVersion
        params["app_version"] = kVersion
        params["api_version"] = apiVersion
        self.showHUD()
        URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.LoginWithApple, showLoader: false) { (resultDict, status, message) in
            self.hideHUD()
            if status == true
            {
                let objUserInfo = UserInfo(fromJson: JSON(resultDict))
                //UserInfo.shared.setData(fromJson: JSON(resultDict))
                UserDefaults.standard.setValue((resultDict as NSDictionary).value(forKey: "token"), forKey: kAuthToken)
                UserDefaults.standard.setValue(true, forKey: kLoggedIn)
                //Nand : New Feature Updates Code
                UserDefaults.standard.setValue(true, forKey: kShowFeatureMsg)
                UserDefaults.standard.setValue(LoginType.AppleLogin.rawValue,forKey: kTypeOfLogin)
                
                //                This old code is now replaced by below new code
                //                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: objUserInfo)
                //                UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                //                self.saveCoreData(objUserInfo:objUserInfo,TypeOfLogin:LoginType.AppleLogin.rawValue) //new change core data
                
                do {
                    let encodedData = try NSKeyedArchiver.archivedData(withRootObject: objUserInfo, requiringSecureCoding: true)
                    UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                    self.saveCoreData(objUserInfo:objUserInfo,TypeOfLogin:LoginType.AppleLogin.rawValue) //new change core data
                } catch {
                    print("NSKeyedArchiver Error is : \(error.localizedDescription)")
                }
                
                //new change
                if UserDefaults.standard.value(forKey: "invitedby") != nil
                {
                    UserDefaults.standard.removeObject(forKey: "invitedby")
                }
                //over
                if let isProfileRequired = objUserInfo.profileRequired, isProfileRequired == 0
                {
                    self.goToHomeVC()
                }
                //isProfileRequired - Flow Change -by Nand
                else if let isProfileRequired = objUserInfo.profileRequired, isProfileRequired == 1 {
                    self.goToEditProfileVCFromLogin(userID: objUserInfo.id ?? "")
                }
                else
                {
                    //new change
                    UIApplication.shared.windows.first?.rootViewController = appDelegate.setUpSWRevealViewController()
                    //                    appDelegate.window?.rootViewController = appDelegate.setUpSWRevealViewController()
                    //over
                    // self.goToEditProfileVC(userID:objUserInfo.id ?? "")
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - callAPIForLogin
    func callAPIForLogin()
    {
        var params : [String:Any] = [:]
        let objUserInfo = getUserInfo()
        params["username"] = txtUsername.text
        params["password"] = passwordStackView.isHidden == false ? txtPassword.text : txtShowPassword.text
        params["device_token"] = UserDefaults.standard.value(forKey: kDeviceToken) as? String ?? "123"
        params["device_type"] = kDeviceType
        params["unique_id"] = objUserInfo.uniqueId ?? ""
        params["device_name"] = kDeviceName
        params["device_brand"] = kDeviceBrand
        params["os_version"] = kOSVersion
        params["app_version"] = kVersion
        params["api_version"] = apiVersion
        self.showHUD()
        URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.Login, showLoader:false) { [self] (resultDict, status, message) in
            self.hideHUD()
            if status == true
            {
                let objUserInfo = UserInfo(fromJson: JSON(resultDict))
                UserDefaults.standard.setValue((resultDict as NSDictionary).value(forKey: "token"), forKey: kAuthToken)
                UserDefaults.standard.setValue(true, forKey: kLoggedIn)
                //Nand : New Feature Updates Code
                UserDefaults.standard.setValue(true, forKey: kShowFeatureMsg)
                UserDefaults.standard.setValue(LoginType.AppLogin.rawValue,forKey: kTypeOfLogin)
                
//                This old code is now replaced by below new code
//                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: objUserInfo)
//                UserDefaults.standard.set(encodedData, forKey: kUserInfo)
//                saveCoreData(objUserInfo:objUserInfo,TypeOfLogin:LoginType.AppLogin.rawValue) //new change core data
//                self.goToHomeVC()
                
                do {
                    let encodedData = try NSKeyedArchiver.archivedData(withRootObject: objUserInfo, requiringSecureCoding: true)
                    UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                    saveCoreData(objUserInfo:objUserInfo,TypeOfLogin:LoginType.AppLogin.rawValue) //new change core data
                    self.goToHomeVC()
                } catch {
                    print("NSKeyedArchiver Error is : \(error.localizedDescription)")
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        let strPassword = passwordStackView.isHidden == false ? txtPassword.text! : txtShowPassword.text!
        if isCheckNull(strText: txtUsername.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_username, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText:strPassword)
        {
            appDelegate.showToast(message: Messsages.msg_enter_password ,bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    // MARK: - IBAction methods
    @IBAction func btnShowPasswordClicked(_ sender:UIButton)
    {
        if passwordStackView.isHidden == false
        {
            txtShowPassword.text = txtPassword.text
            passwordStackView.isHidden = true
            showPasswordStackView.isHidden = false
        }
        else
        {
            txtPassword.text = txtShowPassword.text
            passwordStackView.isHidden = false
            showPasswordStackView.isHidden = true
        }
    }
    @IBAction func btnForgotPasswordClicked(_ sender:UIButton)
    {
        if let vc = appDelegate.getViewController("ForgotPasswordVC", onStoryboard: "Main") as? ForgotPasswordVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnLoginClicked(_ sender:UIButton)
    {
        self.view.endEditing(true)
        //enableAppAppearanceMode()
        if isValidData()
        {
            let objManageCoreData = ManageCoreData()
            let results = objManageCoreData.recordExists(BoxId:txtUsername.text! , entityName: kSwitchUserEntity)
            if results.count > 0
            {
                if let records = results as? [NSManagedObject]
                {
                    let item = records[0]
                    if let status =  item.value(forKey: SwitchUserInfo.isActive.rawValue) as? Bool,status == true
                    {
                        self.popupAlert(title: "", message:"\(txtUsername.text!) user has already logged In. Access switch account list in a Profile screen to switch to this profile." , actionTitles: ["Ok"], actions:[{action1 in
                            self.navigationController?.popViewController(animated: true)
                        }])
                    }
                    else
                    {
                        self.callAPIForLogin()
                    }
                }
                else
                {
                    self.callAPIForLogin()
                }
            }
            else
            {
                self.callAPIForLogin()
            }
        }
    }
    
    func goToEditProfileVCScreen(userID:String)
    {
        if let vc = appDelegate.getViewController("EditProfileVC", onStoryboard: "Profile") as? EditProfileVC {
            vc.userID = userID
            vc.isFromVC = "Register"
            vc.isProfileRequired = 1
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension LoginVC: ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization)
    {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential
        {
            let userIdentifier = appleIDCredential.user
            let authorizationProvider = ASAuthorizationAppleIDProvider()
            authorizationProvider.getCredentialState(forUserID: userIdentifier) { (state, error) in
                switch (state) {
                case .authorized:
                    // print("Account Found - Signed In")
                    let userID = appleIDCredential.user
                    let email = appleIDCredential.email ?? KeychainItem.currentUserEmail ?? ""
                    let firstName = appleIDCredential.fullName?.givenName ??  KeychainItem.currentUserFirstName ?? ""
                    let lastName = appleIDCredential.fullName?.familyName ??  KeychainItem.currentUserLastName ?? ""
                    let fullName = firstName + " " + lastName
                    print("Apple fullName name \(fullName)")
                    print("Apple First name \(firstName)")
                    print("Apple LastName name \(lastName)")
                    

                    //self.json(from: appleIDCredential.authorizationCode!)!
                    
                    let currentUserToken = String(data: appleIDCredential.authorizationCode!, encoding: String.Encoding.utf8) ?? KeychainItem.currentUserToken ?? ""
                    print("currentUserToken is \(currentUserToken)")

                    let clientSecrete = String(data: appleIDCredential.identityToken!, encoding: String.Encoding.utf8) ?? KeychainItem.clientSecrete ?? ""
                    print("clientSecrete is \(clientSecrete)")

                    KeychainItem.currentUserIdentifier = userID
                    KeychainItem.currentUserEmail = email
                    KeychainItem.currentUserFirstName = firstName
                    KeychainItem.currentUserLastName = lastName
                    
                    KeychainItem.currentUserToken = currentUserToken
                    KeychainItem.clientSecrete = clientSecrete
                    
                    DispatchQueue.main.async {
                        self.callAPIForAppleLogin(email:email,appleID:userID,name:firstName, lastName: lastName)
                    }
                    break
                case .revoked, .notFound:
                    // print("No Account Found")
                    DispatchQueue.main.async {
                        if let navvc = appDelegate.getViewController("navvc", onStoryboard: "Main") as? UINavigationController{
                            UIApplication.shared.windows.first?.rootViewController = navvc
                        }
                    }
                    fallthrough
                default:
                    break
                } //switch
            } //credential state
        }
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error)
    {
        // showAlert(title: "", msg: Messsages.msg_pls_try_again, vc: self)
    }
    
}
extension LoginVC : ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
