//
//  CustomNavigationVC.swift
//  CSFCustomer
//
//  Created by Tops on 11/02/21.
//

import UIKit
class CustomNavigationVC: UINavigationController {
    
    // MARK: - Global Variable
    var isFromVC : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        if UserDefaults.standard.value(forKey: kLoggedIn) == nil
        {
            if let vc = appDelegate.getViewController("MainLoginVC", onStoryboard: "Main") as? MainLoginVC {
                self.pushViewController(vc, animated: true)
            }
        }else{
            //isProfileRequired - Flow Change -by Nand
            let objUserInfo = getUserInfo()
            if let isProfileRequired = objUserInfo.profileRequired, isProfileRequired == 1 //1 change to 1
            {
                if let vc = appDelegate.getViewController("EditProfileVC", onStoryboard: "Profile") as? EditProfileVC {
                    vc.userID = objUserInfo.id ?? ""
                    vc.isFromVC = "Register"
                    vc.isProfileRequired = 1
                    self.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
