//
//  RegisterSuccessVC.swift
//  CSFCustomer
//
//  Created by Tops on 22/02/21.
//

import UIKit

class RegisterSuccessVC: UIViewController {
    
    // MARK: - Global Variable
    var timer : Timer?
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    func configureControls()
    {
        timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(RegisterSuccessVC.timerEventCalled), userInfo: nil, repeats: false)
    }
    @objc func timerEventCalled()
    {
        timer?.invalidate()
        timer = nil
        self.navigationController?.popToRootViewController(animated: true)
    }
}
