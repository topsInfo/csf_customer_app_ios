//
//  SplashVC.swift
//  CSFCustomer
//
//  Created by Tops on 29/01/21.
//

import UIKit

class SplashVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblVersion : UILabel!
    @IBOutlet weak var lblAppVersion : UILabel!
    @IBOutlet var viewMain: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBottom: UIImageView!
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewMain.backgroundColor = UIColor(named: "theme_light_bg_color")//UIColor.green
        viewTop.backgroundColor = UIColor(named: "theme_light_bg_color")//UIColor.green
        lblTitle.textColor = UIColor(named: "theme_label_black_color")//Colors.theme_black_color
        lblTitle.textColor = UIColor(named: "theme_label_gray_color")//Colors.theme_black_color
        imgBottom.image = UIImage(named: "splash-img")
//        lblVersion.text = "Version : \(String(describing: Bundle.main.versionNumber!))"
        lblAppVersion.text = "\(String(describing: Bundle.main.versionNumber!))"

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            
            if UserDefaults.standard.value(forKey: kLoggedIn) != nil
            {
                //isProfileRequired - Flow Change -by Nand -----
                let objUserInfo = getUserInfo()
                if let isProfileRequired = objUserInfo.profileRequired, isProfileRequired == 1 //1 change to 1
                {
                    if let navVC = appDelegate.getViewController("navvc", onStoryboard: "Main") as?  CustomNavigationVC {
                        navVC.modalPresentationStyle = .overFullScreen
                        appDelegate.appNav = navVC
                        self.present(navVC, animated: false, completion: nil)
                    }
                    return
                }
                //-------------------------------------------
                if appDelegate.objSWRevealViewController == nil {
                    UIApplication.shared.windows.first?.rootViewController = appDelegate.setUpSWRevealViewController()
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                }
            }else{
                if let navVC = appDelegate.getViewController("navvc", onStoryboard: "Main") as?  CustomNavigationVC {
                    navVC.modalPresentationStyle = .overFullScreen
                    appDelegate.appNav = navVC
                    self.present(navVC, animated: false, completion: nil)
                }
            }
        }
        
    }
}
