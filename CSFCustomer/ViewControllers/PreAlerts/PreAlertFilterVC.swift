//
//  PreAlertFilterVC.swift
//  CSFCustomer
//
//  Created by Tops on 28/11/20.
//

import UIKit
import DropDown
class PreAlertFilterVC: UIViewController,UITextFieldDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var tblFilter : UITableView!
    @IBOutlet weak var shippingContainer : UIView!
    @IBOutlet weak var txtShippingMethod : UITextField!
    @IBOutlet weak var btnShippingMethod : UIButton!
    @IBOutlet weak var shipperContainer : UIView!
    @IBOutlet weak var txtShipper : UITextField!
    @IBOutlet weak var statusContainer : UIView!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var descContainer : UIView!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var searchContainer : UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var dimmerView: UIView!
    @IBOutlet weak var btnApply : UIButton!

    // MARK: - Global Variable
    var arrShippingMethods : [NSDictionary] = [NSDictionary]()
    var arrShippingTitle = [String]()
    var arrShippingID = [String]()
    var arrShipperTitle = [String]()
    var arrShipperID = [String]()
    var arrDescTitle =  [String]()
    var arrDescID =  [String]()
    var selectedShippingMethodID :String = ""
    var selectedShipperID : String  = ""
    let dropDown = DropDown()
    var currentControl : String = ""
    var arrContainers : [UIView] = [UIView]()
    var arrStatus = [String]()
    //vars
    var strShipperText : String  = ""
    var strDescID : String  = ""
    var strStatusText :String = ""
    var strSearch : String = ""
    var strShippingMethodName : String  = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
        showCard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        arrContainers = [shippingContainer,shipperContainer,statusContainer,descContainer,searchContainer]
        for item in arrContainers
        {
            item.layer.borderWidth = 1
            item.layer.borderColor = Colors.theme_lightgray_color.cgColor
        }
        arrStatus = ["PENDING","RECEIVED"]
        configureTextFields()
        arrShippingTitle = arrShippingMethods.map {($0["title"]) as! String}
        arrShippingID = arrShippingMethods.map {($0["id"]) as! String}
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black") //UIColor.black.withAlphaComponent(0.5)
        tblFilter.tableFooterView = UIView()
        setShippingMethodDropDown()
        //textfields
        txtShipper.delegate = self
        txtDescription.delegate = self
        //button
        btnApply.layer.cornerRadius = 2.0
        btnApply.layer.masksToBounds = true
    }
    
    // MARK: - configureTextFields
    func configureTextFields()
    {
        //Colors.theme_black_color for all textfield , bgcolor: Colors.theme_white_color
        configureTextField(textField: txtShippingMethod, placeHolder: "Type of shipping method", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")! , cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtShipper, placeHolder: "Type of shipper", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtStatus, placeHolder: "Status", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtDescription, placeHolder: "Enter description", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtSearch, placeHolder: "Search", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        txtShippingMethod.paddingView(xvalue: 10)
        txtShipper.paddingView(xvalue: 10)
        txtStatus.paddingView(xvalue: 10)
        txtDescription.paddingView(xvalue: 10)
        txtSearch.paddingView(xvalue: 10)
        showData()
    }
    
    // MARK: - showData
    func showData()
    {
        txtShippingMethod.text = strShippingMethodName
        txtShipper.text = strShipperText
        txtDescription.text = strDescID
        txtStatus.text = strStatusText
        txtSearch.text = strSearch
    }
    
    // MARK: - setShippingMethodDropDown
    func setShippingMethodDropDown()
    {
        
        dropDown.width = UIScreen.main.bounds.size.width - 60
        dropDown.shadowRadius = 0
        dropDown.direction = .any
        dropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDown.separatorColor = UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        dropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDown.textColor = UIColor(named:"theme_text_color")!
        dropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        dropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                if self!.currentControl == "Shipping"
                {
                    self?.selectedShippingMethodID  = (self?.arrShippingID[index])!
                    self?.txtShippingMethod.text = item
                    self?.dropDown.hide()
                }
                else if self!.currentControl == "Shipper"
                {
                    self?.selectedShipperID  = (self?.arrShipperID[index])!
                    self?.txtShipper.text = item
                    self?.dropDown.hide()
                }
                else if self!.currentControl == "Description"
                {
                    //                    self?.selectedShippingMethodID  = (self?.arrDescID[index])!
                    self?.txtDescription.text = item
                    self?.dropDown.hide()
                }
                else if self!.currentControl == "Status"
                {
                    self?.txtStatus.text = item
                    self?.dropDown.hide()
                }
            }
        }
    }
    
    // MARK: - showCard
    func showCard()
    {
        self.view.layoutIfNeeded()
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - 600
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
    }
    
    // MARK: - callAPIForMerchantList
    func callAPIForMerchantList(searchText:String)
    {
        DispatchQueue.global(qos: .background).async {
            var params : [String:Any] = [:]
            params["searchtext"] = searchText
            
            URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.ShipperList, showLoader:false) { (resultDict, status, message) in
                if status == true
                {
                    if let arrList = (resultDict as NSDictionary).value(forKey: "list") as? [NSDictionary]
                    {
                        self.arrShipperTitle = arrList.map{$0["title"] as! String}
                        self.arrShipperID = arrList.map{$0["id"] as! String}
                    }
                    DispatchQueue.main.async {
                        self.dropDown.anchorView = self.txtShipper // UIView or UIBarButtonItem
                        self.dropDown.bottomOffset = CGPoint(x:0 , y:(self.txtShipper.bounds.height))
                        self.dropDown.topOffset = CGPoint(x: 0, y:-(self.dropDown.anchorView?.plainView.bounds.height)!)

                        self.dropDown.dataSource = self.arrShipperTitle
                        self.dropDown.reloadAllComponents()
                        self.dropDown.show()
                    }
                }
                else
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    
    // MARK: - callAPIForDescription
    func callAPIForDescription(searchText:String)
    {
        DispatchQueue.global(qos: .background).async {
            var params : [String:Any] = [:]
            params["searchtext"] = searchText
            URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.DescriptionList, showLoader:false) { (resultDict, status, message) in
                if status == true
                {
                    if let arrList = (resultDict as NSDictionary).value(forKey: "list") as? [NSDictionary]
                    {
                        self.arrDescTitle = arrList.map{$0["title"] as! String}
                        self.arrDescID = arrList.map{$0["id"] as! String}
                    }
                    DispatchQueue.main.async {
                        
                        self.dropDown.anchorView = self.txtDescription // UIView or UIBarButtonItem
                        self.dropDown.bottomOffset = CGPoint(x:0 , y:(self.txtDescription.bounds.height))
                        self.dropDown.topOffset = CGPoint(x: 0, y:-(self.dropDown.anchorView?.plainView.bounds.height)!)

                        self.dropDown.dataSource = self.arrDescTitle
                        self.dropDown.reloadAllComponents()
                        self.dropDown.show()
                    }
                }
                else
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtShippingMethod.text!) == true && isCheckNull(strText: txtShipper.text!) == true && isCheckNull(strText: txtDescription.text!) == true && isCheckNull(strText: txtStatus.text!) == true && isCheckNull(strText: txtSearch.text!) == true
        {
            return false
        }
        else
        {
            return true
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnStatusClicked()
    {
        currentControl = "Status"
        
        self.dropDown.anchorView = self.txtStatus // UIView or UIBarButtonItem
        self.dropDown.bottomOffset = CGPoint(x:0 , y:(self.txtStatus.bounds.height))
        self.dropDown.topOffset = CGPoint(x: 0, y:-(self.dropDown.anchorView?.plainView.bounds.height)!)

        self.dropDown.dataSource = self.arrStatus
        self.dropDown.reloadAllComponents()
        self.dropDown.show()
    }
    @IBAction func btnShippingMethodClicked()
    {
        currentControl = "Shipping"

        dropDown.anchorView = btnShippingMethod // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x:0 , y:(txtShippingMethod.bounds.height))
        dropDown.topOffset = CGPoint(x: 0, y:-(self.dropDown.anchorView?.plainView.bounds.height)!)

        dropDown.dataSource = arrShippingTitle
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnApplyClicked()
    {
        if isValidData()
        {
            var strStatus : String = ""
            if txtStatus.text == "PENDING"
            {
                strStatus = "2"
            }
            else if txtStatus.text == "RECEIVED"
            {
                strStatus = "1"
            }
            else
            {
                strStatus = ""
            }
            
            let filterDict :[String: Any] = [
                "shipping_method": selectedShippingMethodID ,
                "status":  strStatus,
                "shipper_id": selectedShipperID != "" ? selectedShipperID : txtShipper.text ?? "",
                "description_id" : txtDescription.text ?? "",
                "search_text" : txtSearch.text ?? "",
                "strShipperText" : txtShipper.text ?? "",
                "strStatus" : txtStatus.text ?? "",
                "strShippingMethodName" : txtShippingMethod.text ?? ""
            ]
            NotificationCenter.default.post(name: Notification.Name("FilterApplied"), object: nil,userInfo: filterDict)
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length >= 1
        {
            if textField == txtShipper
            {
                currentControl = "Shipper"
                callAPIForMerchantList(searchText:String(newString))
            }
            else if textField == txtDescription
            {
                currentControl = "Description"
                callAPIForDescription(searchText:String(newString))
            }
        }
        else
        {
            self.dropDown.dataSource = []
            self.dropDown.reloadAllComponents()
            self.dropDown.hide()
        }
        return true
    }
}
