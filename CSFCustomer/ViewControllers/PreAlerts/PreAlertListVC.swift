//
//  PreAlertListVC.swift
//  CSFCustomer
//
//  Created by Tops on 28/11/20.
//

import UIKit
import SwiftyJSON
class PreAlertListVC: UIViewController,UITableViewDataSource,UITableViewDelegate, UIGestureRecognizerDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var viewTotalRecord : TotalRecordView!
    @IBOutlet weak var cnstTotalRecordsTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var tblPreAlertList : UITableView!
    @IBOutlet weak var lblVCTitle : UILabel!
    @IBOutlet weak var btnAddDelete : UIButton!
    @IBOutlet weak var filterView : UIView!
    @IBOutlet weak var tblFooterView : UIView!
    @IBOutlet weak var btnClearFilter : UIView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    
    @IBOutlet weak var tblBottomConstant : NSLayoutConstraint!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var btnMenu : UIButton!
    @IBOutlet weak var btnExportReport : UIButton!
    @IBOutlet weak var btnClearFilterHeightConstant: NSLayoutConstraint!
    
    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    var preAlertCellHeight : CGFloat = 163
    var arrSections : [Int] = [Int]()
    var arrSelectedSections : [Int] = [Int]()
    var isLoadingList : Bool = false
    var pageNo : Int = 1
    var totalPages : Int = 0
    var arrShippingMethods : [NSDictionary] = [NSDictionary]()
    var objPreAlertList = PreAlertList()
    var arrList : [List] = [List]()
    var preAlertID : String = ""
    var longPressGesture : String = "Stopped"
    //filter vars
    var strShippingMethod : String = ""
    var strShipperID : String  = ""
    var strDescID : String  = ""
    var strStatus :String = ""
    var strSearch : String = ""
    var strShipperText : String = ""
    var strStatusText : String = ""
    var strShippingMethodName : String  = ""
    var isShowLoader : Bool = true
    var isFromMenu :Bool = false
    var refreshControl = UIRefreshControl()
    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        NotificationCenter.default.removeObserver(self)
        if  let decoded = UserDefaults.standard.object(forKey: kUserInfo) as? Data
        {
//            This old code is now replaced by below new code
//            objUserInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserInfo
            do {
                if let unarchivedObject = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserInfo.self, from: decoded) {
                    // Handle the unarchived object
                    objUserInfo = unarchivedObject
                } else {
                    // Handle the case where unarchiving failed
                }
            } catch {
                // Handle the error
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords(_:)), name: Notification.Name("RecordDeleted"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getFilterRecords(_:)), name: NSNotification.Name(rawValue: "FilterApplied"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords(_:)), name: Notification.Name("NewPreAlertAdded"), object: nil)
        topBarView.topBarBGColor()
        cnstTotalRecordsTitleHeight.constant = 0
        viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount)
        viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PREALERT)
        
        getPrealertList(isShowLoader: true)
        btnAddDelete.setImage(UIImage(named: "add"), for: .normal)
        tblPreAlertList.dataSource = self
        tblPreAlertList.delegate = self
        tblPreAlertList.estimatedRowHeight = 105
        tblPreAlertList.rowHeight = UITableView.automaticDimension
        tblPreAlertList.estimatedSectionHeaderHeight = 76
        tblPreAlertList.sectionHeaderHeight = UITableView.automaticDimension
        tblPreAlertList.tableFooterView? = UIView()
        btnClearFilterHeightConstant.constant = 0
        btnClearFilter.isHidden = true
        btnClearFilter.layer.cornerRadius = 2.0
        btnClearFilter.layer.masksToBounds = true

        btnAddDelete.addTarget(self, action:#selector(btnAddDeleteClicked(sender:)) , for: .touchUpInside)
        lblNoRecordFound.isHidden = true
        if isFromMenu == true
        {
            btnMenu.isHidden = false
            btnBack.isHidden = true
        }
        else
        {
            btnMenu.isHidden = true
            btnBack.isHidden = false
        }
        //btnAddDeleteBottomConstant.constant = 25
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblPreAlertList.addSubview(refreshControl)
    }
    
    @objc func refresh()
    {
        pageNo = 1
        getPrealertList(isShowLoader: false)
    }
    
    // MARK: - getPrealertList
    func getPrealertList(isShowLoader : Bool)
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["page_id"] = pageNo
        params["search_text"] = strSearch
        params["shipper_id"] = strShipperID
        params["shipping_method"] = strShippingMethod
        params["description_id"] = strDescID
        params["status"] =  strStatus
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.PreAlertList, showLoader: isShowLoader) { [self] (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            self.tblPreAlertList.isHidden = false
            if status == true
            {
                self.isLoadingList = false
                if self.pageNo == 1
                {
                    self.arrList.removeAll()
                }
                objPreAlertList = PreAlertList.init(fromJson: JSON(resultDict))
                if self.arrList.count > 0
                {
                    self.arrList.append(contentsOf: objPreAlertList.list)
                    for _ in 0..<objPreAlertList.list.count
                    {
                        self.arrSections.append(0)
                        self.arrSelectedSections.append(0)
                    }
                }
                else
                {
                    self.arrList = objPreAlertList.list
                    self.arrSections.removeAll()
                    self.arrSelectedSections.removeAll()
                    for _ in 0..<self.arrList.count
                    {
                        self.arrSections.append(0)
                        self.arrSelectedSections.append(0)
                    }
                }
                //replace right side value with total record count from api when you get it from api
                if let totalCount = objPreAlertList.totalCount
                {
                    self.totalRecordsCount = totalCount
                }
                if self.arrList.count > 0
                {
                    self.lblNoRecordFound.isHidden = true
                    self.btnExportReport.isHidden = false
                }
                else
                {
                    self.lblNoRecordFound.isHidden = false
                    self.btnExportReport.isHidden = true
                }
                if let totalPage = objPreAlertList.totalPage
                {
                    self.totalPages = totalPage
                }
                if let arrShippingMethod = (resultDict as NSDictionary).value(forKey: "shipping_method") as? [NSDictionary]
                {
                    self.arrShippingMethods = arrShippingMethod
                }
                
                DispatchQueue.main.async {
                    self.manageAddDeleteButton()
                    self.tblPreAlertList.reloadData()
                }
                
                if self.totalRecordsCount > 0 {
                    self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
                }else {
                    self.cnstTotalRecordsTitleHeight.constant = 0
                }
                self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
                self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PREALERT)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    @objc func getLatestRecords(_ notification: NSNotification)
    {
        longPressGesture = "Stopped" //newly added
        pageNo = 1
        getPrealertList(isShowLoader: true)
    }
    @objc func getFilterRecords(_ notification: NSNotification)
    {
        tblPreAlertList.layoutIfNeeded()
        pageNo = 1
        strShippingMethod = notification.userInfo?["shipping_method"] as? String ?? ""
        strShippingMethodName = notification.userInfo?["strShippingMethodName"] as? String ?? ""
        strShipperID = notification.userInfo?["shipper_id"] as? String ?? ""
        strShipperText = notification.userInfo?["strShipperText"] as? String ?? ""
        strDescID = notification.userInfo?["description_id"] as? String ?? ""
        strStatus = notification.userInfo?["status"] as? String ?? ""
        strStatusText = notification.userInfo?["strStatus"] as? String ?? ""
        strSearch = notification.userInfo?["search_text"] as? String ?? ""
        btnClearFilterHeightConstant.constant = 45
        btnClearFilter.isHidden = false
        getPrealertList(isShowLoader: true)
    }
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrSections.count > 0
        {
            if arrSections[indexPath.section] == 1
            {
                return UITableView.automaticDimension
            }
        }
        else
        {
            return 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let HeaderCellIdentifier : String  = "HeaderCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCellIdentifier) as! HeaderCell
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(sectionTapped(sender:)))
        cell.stackViewHeader.addGestureRecognizer(tapGesture)
        cell.stackViewHeader.tag = section
        cell.stackViewHeader.isUserInteractionEnabled = true
        
        //Checkbox tapped for selection
        let imgtapGesture = UITapGestureRecognizer(target: self, action: #selector(imgTapped(sender:)))
        cell.imgChecked.addGestureRecognizer(imgtapGesture)
        cell.imgChecked.tag = section
        cell.imgChecked.isUserInteractionEnabled = true

        if arrSections.count > 0
        {
            if  arrSections[section] == 1
            {
                cell.imgUpDown.image = UIImage(named: "upArrow")
                cell.longSeperator.isHidden = true
                cell.shortSeperator.isHidden = false
            }
        }
        else
        {
            cell.imgUpDown.image = UIImage(named: "Down-1")
            cell.longSeperator.isHidden = false
            cell.shortSeperator.isHidden = true
        }

        //change background color of selected cell
        if arrSelectedSections.count > 0
        {
            if  arrSelectedSections[section] == 1
            {
                cell.contentView.backgroundColor = UIColor(named: "theme_lightblue_color")//Colors.theme_lightblue_color
                cell.backgroundColor = UIColor(named: "theme_lightblue_color") //Colors.theme_lightblue_color
                cell.imgChecked.image = UIImage(named: "checkbox")
            }
            else
            {
                //theme_change
                cell.contentView.backgroundColor = UIColor(named: "theme_white_color")
                cell.backgroundColor = UIColor(named: "theme_white_color")
                cell.imgChecked.image = UIImage(named: "uncheckbox")
            }
            self.manageAddDeleteButton()
        }
        else
        {
            cell.contentView.backgroundColor = UIColor(named: "theme_white_color")//theme_change Colors.theme_white_color
            cell.backgroundColor = UIColor(named: "theme_white_color") // theme_change Colors.theme_white_color
            cell.imgChecked.image = UIImage(named: "uncheckbox")
        }
        
        //data
        if arrList.count > 0
        {
            let objList = arrList[section]
            cell.lblDesc.text = objList.descriptionField ?? ""
            cell.lblTrackingNo.text = objList.trackingNumber ?? ""
            cell.lblDate.text = objList.date ?? ""
            if objList.isReceived == "0"
            {
                cell.lblStatus.text = "Pending"
                cell.lblStatus.textColor = UIColor(named: "theme_orange_color")//Colors.theme_orange_color
                cell.imgChecked.isHidden = false
            }
            else
            {
                cell.lblStatus.text = "Received"
                cell.lblStatus.textColor =  UIColor(named: "theme_green_color") //Colors.theme_green_color
                cell.imgChecked.isHidden = true
            }
        }
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PreAlertListCell", for: indexPath) as? PreAlertListCell {
            if arrSelectedSections.count > 0
            {
                if  arrSelectedSections[indexPath.section] == 1
                {
                    cell.contentView.backgroundColor = UIColor(named:"theme_lightblue_color") // Colors.theme_lightblue_color
                    cell.backgroundColor = UIColor(named:"theme_lightblue_color") //Colors.theme_lightblue_color
                }
                else
                {
                    cell.contentView.backgroundColor =  UIColor(named: "theme_bg_color") //Colors.theme_white_color
                    cell.backgroundColor =  UIColor(named: "theme_bg_color") //Colors.theme_white_color
                }
            }
            else
            {
                cell.contentView.backgroundColor =  UIColor(named: "theme_bg_color") //Colors.theme_white_color
                cell.backgroundColor =  UIColor(named: "theme_bg_color") //Colors.theme_white_color
            }
            
            if arrList.count > 0
            {
                let objList = arrList[indexPath.section]
                cell.lblFullDesc.text = objList.fullDescription ?? ""
                cell.lblShippingMethod.text = objList.shippingMethodName
                cell.lblShipperType.text = objList.shipperName
                cell.lblUSDValue.text = "\(kCurrency) \(objList.itemTotalAmount ?? "0.00")"
                cell.lblDocumentName.text = "\(objList.documentPathArr.count) Document(s)"//objList.documentName
                if objList.isReceived == "0"
                {
                    cell.btnEditWidthConst.constant = 30
                    cell.LeadingConstOfDescView.constant = 10
                    cell.btnEdit.isHidden = false
                    cell.deleteContainer.isHidden = false
                }
                else
                {
                    cell.btnEditWidthConst.constant = 0
                    cell.LeadingConstOfDescView.constant = 0
                    cell.btnEdit.isHidden = true
                    cell.deleteContainer.isHidden = true
                }
            }
            cell.btnDelete.tag = indexPath.section
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteClicked(sender:)), for: .touchUpInside)
            
            cell.btnEdit.tag = indexPath.section
            cell.btnEdit.addTarget(self, action: #selector(btnEditClicked(sender:)), for: .touchUpInside)
            
            cell.btnView.tag = indexPath.section
            cell.btnView.addTarget(self, action: #selector(btnViewClicked(sender:)), for: .touchUpInside)
            
            //long press gesture
            let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
            cell.contentView.addGestureRecognizer(longGesture)
            cell.contentView.tag = indexPath.section
            
            cell.btnRow.tag = indexPath.section
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(btnRowClicked(sender:)))
            cell.contentView.addGestureRecognizer(tapGesture)
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: - Action Methods
    @objc func btnAddDeleteClicked(sender:UIButton)
    {
        if arrSelectedSections.isEmpty || !arrSelectedSections.contains(1) {
            if let addPreAlertVC = appDelegate.getViewController("AddPreAlertVC", onStoryboard: "PreAlert") as? AddPreAlertVC {
                addPreAlertVC.type = "Add"
                addPreAlertVC.mediaSizeLimit = objPreAlertList.documentSizeLimit
                addPreAlertVC.mediaLimit = objPreAlertList.documentLimit
                addPreAlertVC.arrShippingMethod = self.arrShippingMethods
                addPreAlertVC.objPreAlertList = self.objPreAlertList
                self.navigationController?.pushViewController(addPreAlertVC, animated: true)
            }
        }
        else
        {
            var strPreAlertID : String  = ""
            if arrSelectedSections.count > 0 {
                for (index,value) in arrSelectedSections.enumerated()
                {
                    if value == 1
                    {
                        let objList = arrList[index]
                        if strPreAlertID != ""
                        {
                            strPreAlertID += "," + objList.id
                        }
                        else
                        {
                            strPreAlertID =  objList.id
                        }
                    }
                }
                
                if let popupVC = appDelegate.getViewController("DeletePopupVC", onStoryboard: "PreAlert") as? DeletePopupVC {
                    popupVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
                    popupVC.userID = objUserInfo.id
                    popupVC.preAlertID = strPreAlertID
                    popupVC.isFromVC = "PreAlertListVC" //new change
                    self.preAlertID = strPreAlertID
                    popupVC.modalPresentationStyle = .overFullScreen
                    self.present(popupVC,animated: true, completion: nil)
                }
            }
        }
    }
    @objc func btnEditClicked(sender:UIButton)
    {
        if arrList.count > 0
        {
            let objList = arrList[sender.tag]
            if let vc = appDelegate.getViewController("AddPreAlertVC", onStoryboard: "PreAlert") as? AddPreAlertVC {
                vc.objList = objList
                vc.type = "Edit"
                vc.objPreAlertList = self.objPreAlertList
                vc.mediaSizeLimit = objPreAlertList.documentSizeLimit
                vc.mediaLimit = objPreAlertList.documentLimit
                vc.arrShippingMethod = self.arrShippingMethods
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    @objc func imgTapped(sender:UITapGestureRecognizer) //new change
    {
        let tag = sender.view!.tag
        if arrSelectedSections[tag] == 0
        {
            arrSelectedSections[tag] = 1
        }
        else
        {
            arrSelectedSections[tag] = 0
        }
        DispatchQueue.main.async {
            // UIView.setAnimationsEnabled(true)
            let section = NSIndexSet(index: tag)
            self.tblPreAlertList.reloadSections(section as IndexSet, with: .none)
            self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
            self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PREALERT)
        }
    }
    
    @objc func longPress(gesture: UILongPressGestureRecognizer)
    {
        if gesture.state == .ended {
            //print("UIGestureRecognizerStateEnded")
        }
        else if gesture.state == .began {
            // print("UIGestureRecognizerStateBegan.")
            longPressGesture = "Started"
            let tag = gesture.view!.tag
            if arrSelectedSections[tag] == 0
            {
                arrSelectedSections[tag] = 1
            }
            else
            {
                arrSelectedSections[tag] = 0
            }
            DispatchQueue.main.async {
                UIView.setAnimationsEnabled(false)
                self.tblPreAlertList.reloadData()
            }
        }
    }
    @objc func btnRowClicked(sender:UITapGestureRecognizer)
    {
        let tag = sender.view!.tag
        if longPressGesture == "Started"
        {
            if arrSelectedSections[tag] == 0
            {
                arrSelectedSections[tag] = 1
            }
            else
            {
                arrSelectedSections[tag] = 0
            }
            if arrSelectedSections.count > 0
            {
                if !arrSelectedSections.contains(1)
                {
                    longPressGesture = "Stopped"
                    UIView.setAnimationsEnabled(false)
                    DispatchQueue.main.async {
                        self.tblPreAlertList.reloadData()
                    }
                    return
                }
            }
            DispatchQueue.main.async {
                UIView.setAnimationsEnabled(true)
                let section = NSIndexSet(index: tag)
                self.tblPreAlertList.reloadSections(section as IndexSet, with: .none)
            }
        }
    }
    
    @objc func sectionTapped(sender:UITapGestureRecognizer)
    {
        let tag = sender.view!.tag
        print("tag is \(tag)")
        
        if arrSections.count > 0
        {
            if arrSections[tag] == 0
            {
                arrSections[tag] = 1
            }
            else
            {
                arrSections[tag] = 0
            }
        }
        DispatchQueue.main.async {
            UIView.performWithoutAnimation {
                self.tblPreAlertList.beginUpdates()
                let section = NSIndexSet(index: tag)
                self.tblPreAlertList.reloadSections(section as IndexSet, with: .none)
                self.tblPreAlertList.endUpdates()
            }
        }
    }

    // MARK: - ScrollView delegate methods
    //Pagination
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isLoadingList = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        if (Int(tblPreAlertList.contentOffset.y + tblPreAlertList.frame.size.height) >= Int(tblPreAlertList.contentSize.height))
        {
            print("load new data")
            if !isLoadingList{
                isLoadingList = true
                if self.totalPages == pageNo
                {
                    return
                }
                pageNo += 1
                getPrealertList(isShowLoader: true)
            }
        }
    }
    
    // MARK: - Manage AddDelete Button
    func manageAddDeleteButton(){
        self.arrSelectedSections.isEmpty || !self.arrSelectedSections.contains(1) ? self.btnAddDelete.setImage(UIImage(named: "add"), for: .normal) : self.btnAddDelete.setImage(UIImage(named: "redDelete"), for: .normal)
    }
    // MARK: - IBAction Methods
    @IBAction func btnClearFilterClicked()
    {
        clearFilter()
    }
    func clearFilter(){
        strShippingMethod =  ""
        strShippingMethodName = ""
        strShipperID = ""
        strShipperText = ""
        strDescID = ""
        strStatus = ""
        strStatusText =  ""
        strSearch =  ""
        pageNo = 1
        btnClearFilterHeightConstant.constant = 0
        self.btnClearFilter.isHidden = true
        tblPreAlertList.layoutIfNeeded()
        getPrealertList(isShowLoader: true)
    }
    @objc func btnViewClicked(sender:UIButton)
    {
        let objList = arrList[sender.tag]
        if let vc = appDelegate.getViewController("PreAlertItemDescVC", onStoryboard: "PreAlert") as? PreAlertItemDescVC {
            vc.arrItemsList = objList.itemList ?? []
            vc.strTotalAmount = objList.itemTotalAmount ?? "0.00"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @objc func btnDeleteClicked(sender:UIButton)
    {
        if arrList.count > 0
        {
            let objList = arrList[sender.tag]
            if let popupVC = appDelegate.getViewController("DeletePopupVC", onStoryboard: "PreAlert") as? DeletePopupVC {
                popupVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
                popupVC.userID = objUserInfo.id
                popupVC.preAlertID = objList.id
                popupVC.isFromVC = "PreAlertListVC" //new change
                self.preAlertID = objList.id
                popupVC.modalPresentationStyle = .overFullScreen
                self.present(popupVC,animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnMenuClicked()
    {
        self.revealViewController().revealToggle(animated: true)
    }
    @IBAction func btnFilterClicked()
    {
        tblPreAlertList.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        if let filterVC = appDelegate.getViewController("PreAlertFilterVC", onStoryboard: "PreAlert") as? PreAlertFilterVC {
            filterVC.arrShippingMethods = self.arrShippingMethods
            filterVC.selectedShippingMethodID = self.strShippingMethod
            filterVC.strShippingMethodName = self.strShippingMethodName
            filterVC.strShipperText = self.strShipperText
            filterVC.strDescID = self.strDescID
            filterVC.strStatusText = self.strStatusText
            filterVC.strSearch = self.strSearch
            filterVC.modalPresentationStyle = .overFullScreen
            self.present(filterVC,animated: true, completion: nil)
        }
    }
    
    @IBAction func btnExportReportClicked(_ sender: UIButton) {
        print("btnExportReportClicked - PreAlertListVC")
        var params : [String:Any] = [:]
        self.showHUD()
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["search_text"] = strSearch
        params["shipper_id"] = strShipperID
        params["shipping_method"] = strShippingMethod
        params["description_id"] = strDescID
        params["status"] =  strStatus
        print("params are \(params)")
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.exportPrealert, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                if let filePath = (resultDict as NSDictionary).value(forKey: "link") as? String
                {
                    self.downloadFile(InvoiceID:"" ,pdfFileURL:filePath, downloadedFileType: "Report")
                }
                else
                {
                    self.hideHUD()
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Do sonthing
        super.traitCollectionDidChange(previousTraitCollection)
//        if #available(iOS 13.0, *) {
//            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
//                self.tblPreAlertList.reloadData()
//            }
//        }else{
//            self.tblPreAlertList.reloadData()
//        }
    }
}
class HeaderCell : UITableViewCell
{
    @IBOutlet weak var imgChecked : UIImageView!
    @IBOutlet weak var imgUpDown : UIImageView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTrackingNo : TapAndCopyLabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var stackViewHeader : UIStackView!
    @IBOutlet weak var longSeperator : UILabel!
    @IBOutlet weak var shortSeperator : UILabel!
}
class PreAlertListCell : UITableViewCell
{
    @IBOutlet weak var lblFullDesc : UILabel!
    @IBOutlet weak var lblShippingMethod : UILabel!
    @IBOutlet weak var lblShipperType : UILabel!
    @IBOutlet weak var lblUSDValue : UILabel!
    @IBOutlet weak var lblDocumentName : UILabel!
    @IBOutlet weak var editContainer : UIView!
    @IBOutlet weak var deleteContainer : UIView!
    @IBOutlet weak var descViewContainer : UIView!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnView : UIButton!
    @IBOutlet weak var btnRow : UIButton!
    @IBOutlet weak var btnEditWidthConst : NSLayoutConstraint!
    @IBOutlet weak var LeadingConstOfDescView : NSLayoutConstraint!
    override func awakeFromNib() {
        editContainer.layer.cornerRadius = btnEdit.frame.size.height/2
        editContainer.layer.masksToBounds = true
        deleteContainer.layer.cornerRadius = btnDelete.frame.size.height/2
        deleteContainer.layer.masksToBounds = true
        descViewContainer.layer.cornerRadius = btnView.frame.size.height/2
        descViewContainer.layer.masksToBounds = true
        btnRow.isHidden = true
    }
}
