//
//  AddItemsVC.swift
//  CSFCustomer
//
//  Created by Tops on 07/07/21.
//

import UIKit
import DropDown
class AddItemsVC: UIViewController,UITextFieldDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var txtDescription : UITextField!
    @IBOutlet weak var txtUSDValue : UITextField!
    @IBOutlet weak var lblGoodsDesc : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnAdd : UIButton!
    
    // MARK: - Global Variable
    var type : String = ""
    var strTitle : String = ""
    var strButtonTitle : String = ""
    let descDropdown = DropDown()
    //shipper list
    var arrDescTitle = [String]()
    var arrDescID = [String]()
    var selectedDescID : String   = ""
    var completionBlock : ((ItemList) -> Void)?
    var arrTextFields : [UITextField] = [UITextField]()
    var objItemList = ItemList()
    var objPreAlertList = PreAlertList()

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        self.descDropdown.hide()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        self.lblTitle.text = strTitle
        btnAdd.setTitle(strButtonTitle, for: .normal)
        setDescDropDown()
        configureTextField(textField: txtDescription, placeHolder: "Description of goods", font: fontname.openSansRegular, fontSize: 14, textColor:UIColor(named: "theme_text_color")! , cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtUSDValue, placeHolder: "USD value", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        arrTextFields = [txtDescription,txtUSDValue]
        for txtField in arrTextFields
        {
            txtField.layer.borderWidth = 1
            txtField.layer.borderColor = Colors.theme_lightgray_color.cgColor
            txtField.paddingView(xvalue: 10)
        }
        txtUSDValue.delegate = self
        txtDescription.delegate = self
       
        lblGoodsDesc.text = self.objPreAlertList.descriptionInstruction ?? ""       
        
        if type == "Edit"
        {
            self.txtDescription.text = objItemList.descriptionField ?? ""
            self.txtUSDValue.text = objItemList.amount ?? ""
        }
    }
    
    // MARK: - setDescDropDown
    func setDescDropDown()
    {
        //desc dropdown
        descDropdown.anchorView = txtDescription
        descDropdown.bottomOffset = CGPoint(x: 0, y:(txtDescription.bounds.size.height))
        descDropdown.topOffset = CGPoint(x: 0, y:-(descDropdown.anchorView?.plainView.bounds.height)!)
        
        descDropdown.width = UIScreen.main.bounds.size.width - 30
        descDropdown.shadowRadius = 0
        descDropdown.direction = .any
        descDropdown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        descDropdown.separatorColor = UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        descDropdown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        descDropdown.textColor = UIColor(named:"theme_text_color")!
        descDropdown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        descDropdown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        descDropdown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                self?.selectedDescID = (self?.arrDescID[index])!
                self?.txtDescription.text = item
                self?.descDropdown.hide()
            }
        }
    }
    
    // MARK: - callAPIForDescription
    func callAPIForDescription(searchText:String)
    {
        DispatchQueue.global(qos: .background).async {
            var params : [String:Any] = [:]
            params["searchtext"] = searchText
            URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.DescriptionList, showLoader:false) { (resultDict, status, message) in
                if status == true
                {
                    if let arrList = (resultDict as NSDictionary).value(forKey: "list") as? [NSDictionary]
                    {
                        self.arrDescTitle = arrList.map{$0["title"] as! String}
                        self.arrDescID = arrList.map{$0["id"] as! String}
                    }
                    DispatchQueue.main.async {
                        self.descDropdown.dataSource = self.arrDescTitle
                        self.descDropdown.reloadAllComponents()
                        self.descDropdown.show()
                    }
                }
                else
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        let bottomValue = getSafeAreaValue() + 60
        if isCheckNull(strText: txtDescription.text!)
        {
            appDelegate.showToast(message: Messsages.msg_goods_description, bottomValue: bottomValue)
            return false
        }
        else if isCheckNull(strText: txtUSDValue.text!)
        {
            appDelegate.showToast(message: Messsages.msg_usdvalue, bottomValue:bottomValue)
            return false
        }
        else if txtUSDValue.text!.contains(".") && self.txtUSDValue.text!.count > 8
        {
            appDelegate.showToast(message: Messsages.msg_usdvalue_exceeds , bottomValue:bottomValue)
            return false
        }
        else if !txtUSDValue.text!.contains(".") && self.txtUSDValue.text!.count > 5
        {
            appDelegate.showToast(message: Messsages.msg_usdvalue_exceeds , bottomValue:bottomValue)
            return false
        }
        else if Float(txtUSDValue.text!)! < 0.5
        {
            appDelegate.showToast(message: Messsages.msg_invalid_USDAmount, bottomValue:bottomValue)
            return false
        }
        return true
    }
    
    // MARK: - IBAction Methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAddClicked()
    {
        if isValidData()
        {
            objItemList.descriptionField = txtDescription.text ?? ""
            if txtUSDValue.text!.contains(".")
            {
                objItemList.amount = txtUSDValue.text ?? "0.00"
            }
            else
            {
                objItemList.amount = "\(txtUSDValue.text!).00"
            }
            completionBlock!(objItemList)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - Textfield delegate methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let r = Range(range, in: text)
        let currentString: NSString = (textField.text ?? "") as NSString
        //        let newString: NSString =
        //            currentString.replacingCharacters(in: range, with: string) as NSString
        let newText = text.replacingCharacters(in: r!, with: string)
        let newLength = text.count + string.count - range.length
        if textField == txtDescription
        {
            if newLength >= 1
            {
                if newLength > 100
                {
                    return newLength <= 100
                }
                callAPIForDescription(searchText:String(newText))
            }
            else
            {
                self.descDropdown.dataSource = []
                self.descDropdown.reloadAllComponents()
                self.descDropdown.hide()
            }
            return true
        }
        else if textField == txtUSDValue
        {
            if newText.contains(".")
            {
                if newLength > 11
                {
                    return newLength <= 11
                }
                let isNumeric = newText.isEmpty || (Double(newText) != nil)
                let numberOfDots = newText.components(separatedBy: ".").count - 1
                
                let numberOfDecimalDigits: Int
                if let dotIndex = newText.firstIndex(of: ".") {
                    numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
                } else {
                    numberOfDecimalDigits = 0
                }
                return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
            }
            else
            {
                return newLength <= 8
            }
        }
        return true
    }
}
