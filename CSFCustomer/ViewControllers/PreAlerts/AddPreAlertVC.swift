//  AddPreAlertVC.swift
//  CSFCustomer
//
//  Created by Tops on 27/11/20.
//

import UIKit
import DropDown
import MobileCoreServices
import Photos
import SwiftyJSON

let AUTO_FILL_MERCHANT = "amazon"
let TBA_TRACKING_NO_PREFIX = "tba"

class AddPreAlertVC: UIViewController,UITextFieldDelegate,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    // MARK: - IBOutlets
    @IBOutlet weak var lblVCTitle : UILabel!
    @IBOutlet weak var shippingContainer : UIView!
    @IBOutlet weak var txtShippingMethod : UITextField!
    @IBOutlet weak var txtMerchant : UITextField!
    @IBOutlet weak var txtTrackingNo : UITextField!
    @IBOutlet weak var lblMerchantDesc : UILabel!
    @IBOutlet weak var lblTrackingDesc : UILabel!
    @IBOutlet weak var btnSubmit :UIButton!
    @IBOutlet weak var btnShipping :UIButton!
    @IBOutlet weak var imgTerms : UIImageView!
    @IBOutlet weak var lblTerms : UILabel!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var btnDisclaimer : UIButton!
    @IBOutlet weak var tblDesc : UITableView!
    @IBOutlet weak var tblDescHeightConst : NSLayoutConstraint!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var stackViewHeightConst : NSLayoutConstraint!
    @IBOutlet weak var lblTotalAmount : UILabel!
    @IBOutlet weak var footerView : UIView!
    @IBOutlet weak var clvPreAlertMedia : UICollectionView!
    @IBOutlet weak var cnstClvPreAlertMediaHeight: NSLayoutConstraint!
    @IBOutlet weak var lblUpoadInfo: UILabel!

    // MARK: - Global Variable
    var arrItemsList : [ItemList] = [ItemList]()
    var arrParamItemsList : [[String:Any]] = [[String:Any]]()
    var arrTextFields : [UITextField] = [UITextField]()
    var arrShippingMethod : [NSDictionary] = [NSDictionary]()
    var arrShippingTitle = [String]()
    var arrShippingID = [String]()
    var selectedShippingMethodID : String = ""
    var objList = List()
    //merchant list
    var arrMerchantList = [MerchantList]()
    var selectedMerchantID : String   = ""
    //other vars
    var documentUpload : Int = 0
    var objUserInfo = UserInfo()
    var termsSelected : Bool = false
    // var fileURLToUpload : String = ""
    var uploadData = Data()
    var mimeType : String = ""
    var type : String  = ""
    var filteredText = String()
    let dropDown = DropDown()
    let merchantDropdown = DropDown()
    var placeHolder = "File Name"
    var AcceptableChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    var clvHeight : CGFloat = 0
    var totalItems : Int = 1
    
    var arrMedia : [MediaModel] = [MediaModel]()
    var intSelectedIndex : Int = 0
    var uploadDataArray = [Data()]
    var docFileNamesArray : [String] = [String]()
    var mimeTypeArray : [String] = [String]()
    var strDocumentName : String = ""
    var mediaLimit : Int = 5
    var mediaSizeLimit : Int = 2
    var arrDeletedDocuments : [String] = [String]()
    var supportedFileExtensions : [String] = ["pdf","jpg","jpeg","png","doc","docx","txt"]
    var strUploadInfo : String = ""
    var objPreAlertList = PreAlertList()
    var isAutoFillAPICallNeeded : Bool = true
    
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isAutoFillAPICallNeeded = false
        self.merchantDropdown.hide()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    // MARK: - Custom Methods
    func configureControls()
    {
        clvPreAlertMedia.delegate = self
        clvPreAlertMedia.dataSource = self
        
        if  let decoded = UserDefaults.standard.object(forKey: kUserInfo) as? Data
        {
            
            //          This old code is now replaced by below new code
            //          objUserInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserInfo
            
            do {
                if let unarchivedObject = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserInfo.self, from: decoded) {
                    // Handle the unarchived object
                    objUserInfo = unarchivedObject
                } else {
                    // Handle the case where unarchiving failed
                }
            } catch {
                // Handle the error
            }
        }
        
        topBarView.topBarBGColor()
        arrShippingTitle = arrShippingMethod.map {($0["title"]) as! String}
        arrShippingID = arrShippingMethod.map {($0["id"]) as! String}
        txtMerchant.delegate = self
        
        setShippingMethodDropDown()
        setMerchantDropDown()
        
        lblTrackingDesc.text = self.objPreAlertList.trackingNumberInstruction ?? ""
        lblMerchantDesc.text = self.objPreAlertList.shipperInstruction ?? ""
        
        shippingContainer.layer.borderWidth = 1
        shippingContainer.layer.borderColor = Colors.theme_lightgray_color.cgColor
        txtShippingMethod.paddingView(xvalue: 10)
        
        configureTextFields()
        
        btnSubmit.layer.cornerRadius = 2
        btnSubmit.layer.masksToBounds = true
        
        if type == "Add"
        {
            lblVCTitle.text = "Add Pre-Alert"
            self.intSelectedIndex = 0
            self.addDataForAddMediaView()
        }
        else
        {
            lblVCTitle.text = "Edit Pre-Alert"
            showData()
        }
        let str = Messsages.msg_terms_condition
        var termsAttString = NSMutableAttributedString(string: str, attributes: [NSAttributedString.Key.font :UIFont(name: fontname.openSansSemiBold, size: 14.0)!])
        termsAttString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location: str.count-1,length:1))
        termsAttString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: fontname.openSansSemiBold, size: 14.0)!, range: NSRange(location: str.count-1,length:1))
        lblTerms.attributedText = termsAttString
        
        let attrs : [NSAttributedString.Key: Any] = [
            .font: UIFont(name: fontname.openSansBold, size: 14)!,
            .foregroundColor: Colors.theme_blue_color,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Pre-Alert Disclaimer",
                                                        attributes: attrs)
        btnDisclaimer.setAttributedTitle(attributeString, for: .normal)
        btnDisclaimer.addTarget(self, action: #selector(btnDisclaimerClicked(sender:)), for: .touchUpInside)
        
        //Tableview
        arrItemsList = objList.itemList ?? []
        var totalAmount : Float = 0.00
        for (_,objItemList) in arrItemsList.enumerated()
        {
            let dict = [
                "description" : objItemList.descriptionField ?? "",
                "amount" : objItemList.amount ?? "0.00"
            ]
            self.arrParamItemsList.append(dict)
            let amount = Float(objItemList.amount ?? "0.00")!
            totalAmount += amount
        }
        tblDesc.dataSource = self
        tblDesc.delegate = self
        tblDesc.estimatedRowHeight = 60
        tblDesc.rowHeight = UITableView.automaticDimension
        self.lblTotalAmount.text = "\(kCurrency) \(String(format:"%.2f",totalAmount))"
        showDescriptionData()
        reloadPreAlertMedia()
        
        self.strUploadInfo = "jpg, jpeg, png, doc, docx, txt, pdf file allowed and per document size up to \(self.mediaSizeLimit) MB"
        self.lblUpoadInfo.text = self.strUploadInfo
    }
    
    @objc func btnDisclaimerClicked(sender:UIButton)
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        if let popupVC = appDelegate.getViewController("PreAlertDisclaimerVC", onStoryboard: "PreAlert") as? PreAlertDisclaimerVC {
            popupVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
            popupVC.modalPresentationStyle = .overFullScreen
            self.present(popupVC,animated: true, completion: nil)
        }
    }
    
    // MARK: - Reload Pre Alert Media
    func reloadPreAlertMedia(){
        DispatchQueue.main.async {
            self.clvPreAlertMedia.reloadData()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.cnstClvPreAlertMediaHeight.constant = self.clvPreAlertMedia.contentSize.height
        }
    }
    
    // MARK: - Show Data
    func showData()
    {
        documentUpload = 0
        txtShippingMethod.text = objList.shippingMethodName ?? ""
        txtMerchant.text = objList.shipperName ?? ""
        txtTrackingNo.text = objList.trackingNumber ?? ""
        selectedShippingMethodID = objList.shippingMethod ?? ""
        selectedMerchantID = objList.shipperId ?? ""
        
        // Add Object For Media View
        if objList.documentPathArr != nil {
            if objList.documentPathArr.count > 0 {
                for obj in objList.documentPathArr {
                    
                    if let pathUrl = obj as? String {
                        
                        let docName : String  = URL(string: pathUrl)!.lastPathComponent
                        let docMimeType : String  = URL(string: pathUrl)!.pathExtension.lowercased()
                        
                        let objMedia : MediaModel = MediaModel()
                        self.intSelectedIndex += 1
                        objMedia.id = self.intSelectedIndex
                        
                        if docMimeType == "jpeg" || docMimeType == "jpg" || docMimeType == "png" {
                            objMedia.mediaType = 1
                        }else {
                            objMedia.mediaType = 2
                        }
                        
                        objMedia.type = 2
                        objMedia.docFileNames = docName
                        objMedia.attachment = nil
                        objMedia.attachmentData = self.uploadData
                        objMedia.mimeType = docMimeType
                        objMedia.attachmentURL = pathUrl
                        objMedia.isFrom = 1
                        self.arrMedia.append(objMedia)
                    }
                }
            }
        }
        self.intSelectedIndex += 1
        self.addDataForAddMediaView()
    }
    
    // MARK: - Show Description Data
    func showDescriptionData()
    {
        if self.arrItemsList.count == 0
        {
            tblDesc.isHidden = true
            DispatchQueue.main.async {
                self.view.layoutIfNeeded()
                let height : CGFloat = 50
                self.tblDescHeightConst.constant = height
                self.stackViewHeightConst.constant = CGFloat(height + 82)
                self.view.layoutIfNeeded()
            }
            lblNoRecordFound.isHidden = false
        }
        else
        {
            tblDesc.isHidden = false
            lblNoRecordFound.isHidden = true
            DispatchQueue.main.async {
                self.view.layoutIfNeeded()
                let height = self.tblDesc.contentSize.height
                self.tblDescHeightConst.constant = height
                self.stackViewHeightConst.constant = 82 + height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - Set Shipping Method DropDown
    func setShippingMethodDropDown()
    {
        dropDown.width = UIScreen.main.bounds.size.width - 40
        dropDown.shadowRadius = 0
        dropDown.direction = .any
        dropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDown.separatorColor =  UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        dropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDown.textColor = UIColor(named:"theme_text_color")!
        dropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color

        dropDown.anchorView = btnShipping // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x:0 , y:(txtShippingMethod.bounds.height))
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)

        dropDown.dataSource = arrShippingTitle
        if type == "Add"
        {
            self.txtShippingMethod.text = "AIR"
            if self.arrShippingID.count > 1
            {
                self.selectedShippingMethodID = self.arrShippingID[1]
            }
        }
        dropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                self?.selectedShippingMethodID  = (self?.arrShippingID[index])!
                self?.txtShippingMethod.text = item
                self?.dropDown.hide()
            }
        }
    }
    
    // MARK: - Set Merchant DropDown
    func setMerchantDropDown()
    {
        //merchant dropdown
        merchantDropdown.width = UIScreen.main.bounds.size.width - 40
        merchantDropdown.shadowRadius = 0
        
        merchantDropdown.direction = .any
        merchantDropdown.anchorView = txtMerchant // UIView or UIBarButtonItem
        merchantDropdown.bottomOffset = CGPoint(x: 0, y:(txtMerchant.bounds.size.height))
        merchantDropdown.topOffset = CGPoint(x: 0, y:-(merchantDropdown.anchorView?.plainView.bounds.height)!)
        
        merchantDropdown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        merchantDropdown.separatorColor = UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        merchantDropdown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        merchantDropdown.textColor = UIColor(named:"theme_text_color")!
        merchantDropdown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        merchantDropdown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        merchantDropdown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                if self?.arrMerchantList != nil {
                    if (self?.arrMerchantList.count)! > 0 {
                        self?.selectedMerchantID  = (self?.arrMerchantList[index].id)!
                        self?.txtMerchant.text = item
                        self?.merchantDropdown.hide()
                        
                        print("selectedMerchantID (shipper_id) is \(String(describing: self?.selectedMerchantID))")
                    }
                }
            }
            
        }
    }
    
    // MARK: - Configure Text Fields
    func configureTextFields()
    {
        txtTrackingNo.delegate = self
        arrTextFields = [txtMerchant,txtTrackingNo]
        for txtField in arrTextFields
        {
            txtField.layer.borderWidth = 1
            txtField.layer.borderColor = Colors.theme_lightgray_color.cgColor
            txtField.paddingView(xvalue: 10)
        }
        configureTextField(textField: txtShippingMethod, placeHolder: "Type of shipping method", font: fontname.openSansRegular, fontSize: 14, textColor:UIColor(named: "theme_text_color")! , cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor:UIColor(named: "theme_textfield_bgcolor")! )
        configureTextField(textField: txtMerchant, placeHolder: "Please select merchant/store", font: fontname.openSansRegular, fontSize: 14, textColor:UIColor(named: "theme_text_color")! , cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtTrackingNo, placeHolder: "Tracking number", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
    }

    // MARK: - IBAction methods
    @IBAction func btnAddItemsClicked()
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        if let addItemVC = appDelegate.getViewController("AddItemsVC", onStoryboard: "PreAlert") as? AddItemsVC {
            addItemVC.type = "Add"
            addItemVC.strTitle = "Add Item"
            addItemVC.strButtonTitle = "Add"
            addItemVC.objPreAlertList = self.objPreAlertList
            addItemVC.completionBlock = { [self] objItemList in
                var totalAmount : Float = 0.00
                totalAmount = Float(objItemList.amount ?? "0.00")!
                for objItem in arrItemsList
                {
                    let amount = Float(objItem.amount ?? "0.00")!
                    totalAmount += amount
                }
                self.arrItemsList.append(objItemList)
                let dict = [
                    "description" : objItemList.descriptionField ?? "",
                    "amount" : objItemList.amount ?? "0.00"
                ]
                self.arrParamItemsList.append(dict)
                DispatchQueue.main.async {
                    self.lblTotalAmount.text = "\(kCurrency) \(String(format:"%.2f",totalAmount))"
                    self.tblDesc.reloadData()
                    self.showDescriptionData()
                }
            }
            self.navigationController?.pushViewController(addItemVC, animated: true)
        }
    }
    @IBAction func btnShippingClicked()
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        dropDown.show()
    }
    @IBAction func btnBackClicked()
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTermsClicked(_ sender:UIButton)
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        if termsSelected == false
        {
            imgTerms.image = UIImage(named: "select")
            termsSelected = true
        }
        else
        {
            imgTerms.image = UIImage(named: "unselect")
            termsSelected = false
        }
    }
    @IBAction func btnSubmitClicked()
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        self.uploadDataArray = self.arrMedia.filter( { $0.type == 2 && $0.isFrom == 0} ).map( {$0.attachmentData} )
        self.docFileNamesArray = (self.arrMedia.filter( { $0.type == 2 && $0.isFrom == 0 } ).map( {$0.docFileNames} ))
        self.mimeTypeArray = (self.arrMedia.filter( { $0.type == 2 && $0.isFrom == 0 } ).map( {$0.mimeType} ))
        
        print("self.uploadDataArray \(self.uploadDataArray)")
        print("self.docFileNamesArray \(self.docFileNamesArray)")
        print("self.mimeTypeArray \(self.mimeTypeArray)")
        
        var strJSON : String = ""
        if self.arrParamItemsList.count > 0
        {
            strJSON = json(from: arrParamItemsList)!
        }
        if isValidData()
        {
            var params : [String:Any] = [:]
            objUserInfo = getUserInfo()
            params["user_id"] = objUserInfo.id ?? ""
            params["prealert_id"] = objList.id ?? ""
            
            params["shipping_method"] = selectedShippingMethodID
            params["tracking_number"] = txtTrackingNo.text

            if selectedMerchantID == ""{
                params["shipper_type"] = txtMerchant.text ?? ""
            }else{
                params["shipper_id"] = selectedMerchantID
            }
            
            params["item_details"] = strJSON
            params["document_upload"] = documentUpload
            
            let strDelDocJsonArray =  json(from: arrDeletedDocuments)!
            params["deleted_documents"] = strDelDocJsonArray
            params["document[]"] = self.uploadDataArray

            print("params are \(params)")
            
            URLManager.shared.URLCallMultipleMultipartDataForPreAlert(method: .post, parameters: params, header: true, url: APICall.AddEditPreAlert, showLoader: true, WithName: docFileNamesArray, docFileName: docFileNamesArray, uploadData: uploadDataArray, mimeType: mimeTypeArray) { [self] (resultDict, status, message) in
              
                if status == true
                {
                    NotificationCenter.default.post(name: Notification.Name("NewPreAlertAdded"), object: nil)
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(),isForSuccess: true)
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    
    // MARK: - Textfield delegate methods
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.text?.isEmpty == true){
            self.merchantDropdown.isHidden = true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtTrackingNo{
            isAutoFillAPICallNeeded = true
        }else if textField == txtMerchant {
            isAutoFillAPICallNeeded = true
            let trackingNo : String = txtTrackingNo.text ?? ""
            print("my text is \(String(describing: trackingNo))")
            callAutofillMerchantAPI(trackingNo: trackingNo)
        }
    }
    
    @IBAction func txtMerchantDidChange(_ sender: UITextField) {
        guard let text = sender.text,
        let first = text.first else {
            self.merchantDropdown.dataSource = []
            self.merchantDropdown.reloadAllComponents()
            self.merchantDropdown.hide()
            return
        }
        let firstCharacterString = String(first)
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if firstCharacterString.rangeOfCharacter(from: characterset.inverted) != nil {
            appDelegate.showToast(message: Messsages.msg_enter_only_alpha_numeric_character, bottomValue: getSafeAreaValue(), position: ToastPosition.top.rawValue)
            let stringWithoutSpecialCharacterPerfix = getStringByRemovingPrefixSpecialCharacters(string: text)
            sender.text = stringWithoutSpecialCharacterPerfix
        }
        
        let finalString = sender.text!
        if !finalString.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            callAPIForMerchantList(searchText:finalString)
        }else{
            sender.text = ""
            self.merchantDropdown.dataSource = []
            self.merchantDropdown.reloadAllComponents()
            self.merchantDropdown.hide()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMerchant {
            self.selectedMerchantID = ""
            
            //50 character limit logic - start
            var isBackClicked : Bool = false
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    isBackClicked = true
                }
            }
            
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length > 50 && !isBackClicked{
                return false
            }
            //50 character limit logic - end
            
            if textField.text!.isEmpty {
                if isFirstCharacterSpecial(string: textField.text!) {
                    let stringWithoutSpecialCharacterPerfix = getStringByRemovingPrefixSpecialCharacters(string: textField.text!)
                    textField.text!.append(stringWithoutSpecialCharacterPerfix)
                    return false
                } else {
                    return true
                }
            } else {
                return true
            }
        }
        else if textField == txtTrackingNo
        {
            let cs = NSCharacterSet(charactersIn: AcceptableChars).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == txtTrackingNo{
            if let trackingNo : String = textField.text {
                print("my text is \(String(describing: trackingNo))")
                callAutofillMerchantAPI(trackingNo: trackingNo)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Call Autofill API
    func callAutofillMerchantAPI(trackingNo : String){
        if isAutoFillAPICallNeeded {
            if trackingNo.lowercased().hasPrefix(TBA_TRACKING_NO_PREFIX){
                let strSelectedMerchant : String = txtMerchant.text ?? ""
                if strSelectedMerchant.isEmpty {
                    print("Call API")
                    callAPIForMerchantList(searchText:AUTO_FILL_MERCHANT, isCalledFromTrackingNo: true)
                }else{
                    print("Don't Call API")
                }
            }else{
                print("Don't Call API")
            }
        }
    }
    
    // MARK: - getStringByRemovingPrefixSpecialCharacters
    private func getStringByRemovingPrefixSpecialCharacters(string: String) -> String {
        let inputString = string
        let regex = try! NSRegularExpression(pattern: "^[^a-zA-Z0-9]+", options: .caseInsensitive)
        let range = NSRange(location: 0, length: inputString.utf16.count)
        let modifiedString = regex.stringByReplacingMatches(in: inputString, options: [], range: range, withTemplate: "")
        return modifiedString
    }
    
    // MARK: - isFirstCharacterSpecial
    private func isFirstCharacterSpecial(string: String) -> Bool {
        guard let first = string.first else {
            return false
        }
        let firstCharacterString = String(first)
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if firstCharacterString.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        } else {
            return false
        }
    }
    
    // MARK: - firstChar
    func firstChar(str:String) -> String {
        return String(Array(str)[0])
    }
    
    // MARK: - callAPIForMerchantList
    func callAPIForMerchantList(searchText:String, isCalledFromTrackingNo : Bool = false)
    {
        DispatchQueue.global(qos: .background).async {
            var params : [String:Any] = [:]
            params["searchtext"] = searchText
            
            URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.ShipperList, showLoader:isCalledFromTrackingNo ? true : false) { (resultDict, status, message) in
                if status == true
                {
                    let objMerchantData : MerchantData = MerchantData(fromJson: JSON(resultDict))
                    let objMerchantList : [MerchantList] = objMerchantData.list
                    
                    if isCalledFromTrackingNo {
                        var arrMerchantListForTrackingNo = [MerchantList]()
                        arrMerchantListForTrackingNo = objMerchantList
                        if arrMerchantListForTrackingNo.count > 0 {
                            print("New Filter Data is \(arrMerchantListForTrackingNo.filter { $0.title.lowercased() == AUTO_FILL_MERCHANT})")
                            
                            let arrFiltered : [MerchantList] = arrMerchantListForTrackingNo.filter { $0.title.lowercased() == AUTO_FILL_MERCHANT}
                            if arrFiltered.count > 0 {
                                let objFiltered : MerchantList = arrFiltered.first!
                                print("Id is \(objFiltered.id ?? "")")
                                
                                self.selectedMerchantID  = objFiltered.id ?? ""
                                self.txtMerchant.text = objFiltered.title ?? ""
                            }
                        }
                    }else{
                        self.arrMerchantList = objMerchantList
                        
                        DispatchQueue.main.async {
                            if self.arrMerchantList.count > 0 {
                                self.merchantDropdown.dataSource = self.arrMerchantList.map{$0.title}
                                self.merchantDropdown.reloadAllComponents()
                                self.merchantDropdown.show()
                            }else{
                                self.merchantDropdown.hide()
                            }
                        }
                    }
                }
                else
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        var firstDocName : String = ""
        if arrMedia.count > 0 {
            if let docName : String = arrMedia[0].docFileNames {
                firstDocName = docName
            }
        }
        
        let bottomValue = getSafeAreaValue() + 60
        if isCheckNull(strText: txtShippingMethod.text!)
        {
            appDelegate.showToast(message: Messsages.msg_shipping_method, bottomValue:bottomValue )
            return false
        }
        else if isCheckNull(strText: txtTrackingNo.text!)
        {
            appDelegate.showToast(message: Messsages.msg_tracking_no, bottomValue: bottomValue)
            return false
        }
        else if txtTrackingNo.text!.count < 7 || txtTrackingNo.text!.count > 50
        {
            appDelegate.showToast(message: Messsages.msg_length_of_tracking_no, bottomValue: bottomValue)
            return false
        }
        else if isCheckNull(strText: txtMerchant.text!)
        {
            appDelegate.showToast(message: Messsages.msg_merchant_store, bottomValue: bottomValue)
            return false
        }
        else if firstDocName.isEmpty {
            appDelegate.showToast(message: Messsages.msg_choose_file, bottomValue: bottomValue)
            return false
        }
        else if arrItemsList.count == 0
        {
            appDelegate.showToast(message: Messsages.msg_add_item, bottomValue: bottomValue)
            return false
        }
        else if termsSelected == false
        {
            appDelegate.showToast(message: Messsages.msg_agree_terms, bottomValue: bottomValue)
            return false
        }
        return true
    }

    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    // MARK: - CollectionView Delegate and Datasource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell : AddPreAlertDocCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPreAlertDocCell", for: indexPath) as? AddPreAlertDocCell {
            cell.btnAddMedia.tag = indexPath.row
            cell.btnAddMedia.addTarget(self, action: #selector(btnAddMediaClicked(sender:)), for: .touchUpInside)

            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteMediaClicked(sender:)), for: .touchUpInside)
            
            let objMedia : MediaModel = self.arrMedia[indexPath.row]
            if objMedia.type == 1 {
                //Add
                cell.viewAddMedia.isHidden = false
                cell.viewMedia.isHidden = true
            }else {
                //Media
                cell.viewAddMedia.isHidden = true
                cell.viewMedia.isHidden = false
                
                if objMedia.mediaType == 1 {
                    //Image
                    if objMedia.isFrom == 0 {
                        //Local
                        cell.activityIndiCatorMedia.isHidden = true

                        if let img : UIImage = objMedia.attachment {
                            cell.imgThumb.image = img
                        }
                    }else if objMedia.isFrom == 1 {
                        //Web
                        if let imgUrl : String = objMedia.attachmentURL {
                            cell.activityIndiCatorMedia.isHidden = false
                            cell.activityIndiCatorMedia.startAnimating()
                            cell.imgThumb.sd_setImage(with: URL(string:imgUrl), completed: { (image, error, type, url) in
                                if image != nil {
                                    cell.imgThumb.image = image
                                    cell.activityIndiCatorMedia.stopAnimating()
                                    cell.activityIndiCatorMedia.isHidden = true
                                }else {
                                    //Image Not found
                                }
                            })
                        }
                    }
                    cell.imgThumb.isHidden = false
                    cell.imgDocThumb.isHidden = true
                }else if objMedia.mediaType == 2 {
                    //Doc
                    cell.imgThumb.isHidden = true
                    cell.imgDocThumb.isHidden = false
                }
                cell.imgThumb.contentMode = .scaleAspectFill
                cell.imgDocThumb.contentMode = .scaleAspectFit
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objMedia : MediaModel = self.arrMedia[indexPath.row]
        if let fileUrl : String = objMedia.attachmentURL {
            print("Medai url is \(fileUrl)")
        }
        
        if let preAlertViewerVC = appDelegate.getViewController("PreAlertViewerVC", onStoryboard: "PreAlert") as? PreAlertViewerVC {
            preAlertViewerVC.arrMedia = self.arrMedia.filter( { $0.type == 2} )
            preAlertViewerVC.intSelectedIndex = indexPath.row
            preAlertViewerVC.currentPage = indexPath.row
            self.navigationController?.pushViewController(preAlertViewerVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width : CGFloat = (clvPreAlertMedia.frame.size.width / 3) - 10
        clvHeight = width
        print("cell width is \(width)")
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    }
    
    @objc func btnAddMediaClicked(sender : UIButton)
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        
        print("Add media cllicked at \(sender.tag)")
        
        self.intSelectedIndex = sender.tag
        self.chooseFile()
    }
    
    @objc func btnDeleteMediaClicked(sender : UIButton)
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        print("Delete media cllicked at \(sender.tag)")
        
        self.popupAlert(title: "", message: Messsages.msg_delete_document, actionTitles: ["Confirm","Cancel"], actions:[{action1 in
            
            let objMedia : MediaModel = self.arrMedia[sender.tag]
            if let mediaUrl : String = objMedia.attachmentURL {
                if !mediaUrl.isEmpty {
                    let url : URL = URL(string: mediaUrl)!
                    let deletedFileName : String = url.lastPathComponent
                    self.arrDeletedDocuments.append(deletedFileName)
                }
            }
            
            self.arrMedia.remove(at: sender.tag)
            self.reloadPreAlertMedia()
            
            //If all media limit comes and user delete the last media then we will show add media option via this code.
            if self.arrMedia.count > 0 {
                if let objMedia : MediaModel = self.arrMedia.last {
                    if objMedia.type == 2 {
                        self.addDataForAddMediaView()
                    }
                }
            }
        },{ action2 in
            self.dismiss(animated: true, completion: nil)
        }])
    }
    
    func addDataForAddMediaView(){
        // Add Object For Add Media View
        
        //This will limit to add media
        if self.arrMedia.count <= self.mediaLimit - 1 {
            let objNewMedia : MediaModel = MediaModel()
            objNewMedia.id = self.intSelectedIndex + 1
            objNewMedia.type = 1
            self.arrMedia.append(objNewMedia)
        }
        self.reloadPreAlertMedia()
    }
}
extension AddPreAlertVC : UIDocumentPickerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func chooseFile(){
        let attributedString = NSAttributedString(string: "Please select option", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15), //your font here
            NSAttributedString.Key.foregroundColor :Colors.theme_gray_color
        ])
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(attributedString, forKey: "attributedTitle")
        
        let takePhotoAction = UIAlertAction(title: "Take a Photo" , style: .default) { (_ action) in
            self.openCamera()
        }
        let photoAction = UIAlertAction(title: "Photo Gallery" , style: .default) { (_ action) in
            
            let status = PHPhotoLibrary.authorizationStatus()
            print("status is \(status)")
            self.openGallery()
        }
        let browseAction = UIAlertAction(title: "Browse" , style: .default) { (_ action) in
            self.openDocuments()
        }
        let cancelAction = UIAlertAction(title: "Cancel" , style: .cancel) { (_ action) in
            self.dismiss(animated: true, completion: nil)
        }
        takePhotoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        photoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        browseAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(takePhotoAction)
        alert.addAction(photoAction)
        alert.addAction(browseAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            PermissionManager.shared.requestCameraPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.camera
                        imagePicker.allowsEditing = false
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            PermissionManager.shared.requestPhotoPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                        imagePicker.allowsEditing = false
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openDocuments()
    {
        let types = [String(kUTTypeJPEG),String(kUTTypePNG),"com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document",String(kUTTypePlainText),String(kUTTypePDF)] as [String] // You can add more types here as pr your expectation
        let documentPicker = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .formSheet
        present(documentPicker, animated: true, completion: nil)
    }
    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var pickedImageAsset : PHAsset!
        
        if (picker.sourceType != UIImagePickerController.SourceType.camera) {
            //If image picked from photo gallary
            
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            {
                print("image url is \(url)")
                switch (url.pathExtension).lowercased()
                {
                case "jpg","jpeg":
                    self.mimeType = "image/jpeg"
                    break
                case "png":
                    self.mimeType = "image/png"
                    break
                default:
                    //  print("No file")
                    appDelegate.showToast(message: Messsages.msg_image_type, bottomValue: getSafeAreaValue())
                    return
                }
            }
            
            if let pickedImage = info[.originalImage] as? UIImage
            {
                DispatchQueue.main.async { [self] in
                    let dblSize = pickedImage.getSizeIn(mimeType: self.mimeType, type: .megabyte)
                    if dblSize > 0 {
                        let imgSizeInMb : Double = dblSize
                        print("Image size is \(imgSizeInMb)")
                        print("mediaSizeLimit is \(self.mediaSizeLimit)")
                        if imgSizeInMb > Double(self.mediaSizeLimit) {
                            let msg : String = "Selected attachment should be less than \(self.mediaSizeLimit) mb."
                            appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                            picker.dismiss(animated: true, completion: nil)
                        }else{
                            print("\(self.arrMedia.filter( { $0.type == 2 } ).map( {$0.attachment} ))")
                            let docFileNames = self.arrMedia.filter( { $0.type == 2 } ).map( {$0.attachment} )
                            
                            var isFileTaken : Bool = false
                            for doc in docFileNames {
                                if let tmpFile : UIImage = doc {
                                    if tmpFile.isEqualToImage(pickedImage) {
                                        isFileTaken = true
                                        break
                                    }
                                }
                            }
                            
                            if isFileTaken {
                                appDelegate.showToast(message: Messsages.msg_doc_already_uploaded, bottomValue: getSafeAreaValue())
                                pickedImageAsset = nil
                                picker.dismiss(animated: true, completion: nil)
                            }else{
                                setUpImageData(info: info, pickedImageAsset: pickedImageAsset, isFromCamera: false)
                            }
                        }
                    }
                }
            }
            
        }else {
            //If image picked from camera
            appDelegate.showLoader()
            var pickedImageAsset : PHAsset!
            pickedImageAsset = nil
            setUpImageData(info: info, pickedImageAsset: pickedImageAsset, isFromCamera: true)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func setUpImageData(info : [UIImagePickerController.InfoKey : Any], pickedImageAsset : PHAsset?, isFromCamera : Bool){
        var fileType : String = ""

        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
        {
            print("image url is \(url)")
            fileType  = url.lastPathComponent.lowercased()
            switch (url.pathExtension).lowercased()
            {
            case "jpg","jpeg":
                self.mimeType = "image/jpeg"
                break
            case "png":
                self.mimeType = "image/png"
                break
            default:
                //  print("No file")
                appDelegate.showToast(message: Messsages.msg_image_type, bottomValue: getSafeAreaValue())
                return
            }
        }
        
        if let pickedImage = info[.originalImage] as? UIImage
        {
            documentUpload = 1
            if fileType != ""
            {
                self.strDocumentName = fileType
            }
            else
            {
                self.mimeType = "image/png"
                let currentTimeStamp = String(Int(NSDate().timeIntervalSince1970))
                self.strDocumentName = "csf-\(currentTimeStamp).png"
            }
            DispatchQueue.global(qos: .background).async {
                
                var finalImage : UIImage = pickedImage
                if isFromCamera {
                    finalImage = pickedImage.resized(withPercentage: 0.1)!
                }

                if self.mimeType == "image/jpeg"
                {
                    self.uploadData = finalImage.jpegData(compressionQuality: 0.3)!
                }
                else if self.mimeType == "image/png"
                {
                    self.uploadData = finalImage.pngData()!
                }
                appDelegate.hideLoader()
                // Add Object For Media View
                let objMedia : MediaModel = MediaModel()
                objMedia.id = self.intSelectedIndex
                objMedia.type = 2
                
                objMedia.docFileNames = self.strDocumentName
                objMedia.attachment = pickedImage
                objMedia.attachmentData = self.uploadData
                objMedia.mimeType = self.mimeType
                objMedia.phAsset = pickedImageAsset
                objMedia.mediaType = 1
                objMedia.isFrom = 0
                self.arrMedia[self.intSelectedIndex] = objMedia
                
                // Add Object For Add Media View
                self.addDataForAddMediaView()
                print("Now total items are \(self.arrMedia.count)")
            }
        }
    }
    // MARK: - Document picker delegate
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let url = urls.first else {
            return
        }
        if !self.supportedFileExtensions.contains((url.pathExtension).lowercased()) {
            appDelegate.showToast(message: self.strUploadInfo, bottomValue: getSafeAreaValue())
            return
        }
        
        print("\(self.arrMedia.filter( { $0.type == 2 } ).map( {$0.docFileNames} ))")
        let docFileNames = self.arrMedia.filter( { $0.type == 2 } ).compactMap( {$0.docFileNames} )
        if docFileNames.contains(url.lastPathComponent.lowercased()) {
            appDelegate.showToast(message: Messsages.msg_doc_already_uploaded, bottomValue: getSafeAreaValue())
            return
        }
        
        // print("import result : \(url)")
        self.strDocumentName = url.lastPathComponent.lowercased()
        let pathExtension = url.pathExtension.lowercased()
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                self.mimeType = mimetype as String
            }
        }
        
        if self.mimeType == ""
        {
            switch (url.pathExtension).lowercased()
            {
            case "pdf":
                self.mimeType = "application/pdf"
                break
            case "jpg","jpeg":
                self.mimeType = "image/jpeg"
                break
            case "png":
                self.mimeType = "image/png"
                break
            case "doc":
                self.mimeType = "application/msword"
                break
            case "docx":
                self.mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                break
            case "txt":
                self.mimeType = "text/*"
                break
            default:
                print("No file")
            }
        }
        
        DispatchQueue.global(qos: .background).async {
            do{
                let documentData : Data = try Data(contentsOf:url.asURL())
                
                DispatchQueue.main.async {
                    let strSize = documentData.getSizeInFromData(data: documentData, type: .megabyte)
                    if strSize.count > 0 {
                        let documentSizeInMb : Float = Float(strSize)!
                        print("Document size is \(documentSizeInMb)")
                        print("mediaSizeLimit is \(self.mediaSizeLimit)")
                        if documentSizeInMb > Float(self.mediaSizeLimit) {
                            let msg : String = "Selected attachment should be less than \(self.mediaSizeLimit) mb."
                            appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                            return
                        }
                    }
                    
                    self.uploadData = documentData
                    
                    // Add Object For Media View
                    self.documentUpload = 1
                    let objMedia : MediaModel = MediaModel()
                    objMedia.id = self.intSelectedIndex
                    objMedia.type = 2
                    
                    objMedia.docFileNames = self.strDocumentName
                    objMedia.attachmentData = self.uploadData
                    objMedia.mimeType = self.mimeType
                    objMedia.isFrom = 0
                    objMedia.localDocURL = url.absoluteString
                    if self.mimeType == "image/jpeg" || self.mimeType == "image/png"{
                        //Image
                        objMedia.attachment = UIImage(data: self.uploadData)
                        objMedia.mediaType = 1
                    }else {
                        //Doc
                        objMedia.attachment = nil
                        objMedia.mediaType = 2
                    }
                    
                    self.arrMedia[self.intSelectedIndex] = objMedia
                    
                    // Add Object For Add Media View
                    self.addDataForAddMediaView()
                    print("Now total items are \(self.arrMedia.count)")

                }
            }
            catch {
                print("Unable to load data: \(error)")
            }
        }
        
    } //function
}
extension AddPreAlertVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItemsList.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == tblDesc
        {
            return 50
        }
        return .leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if tableView == tblDesc
        {
            footerView.frame = CGRect(x: tblDesc.frame.origin.x, y: 0, width: self.tblDesc.frame.size.width, height: 65)
            return footerView
        }
        return nil
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ItemDescCell", for: indexPath) as? ItemDescCell {
            let objItemList = arrItemsList[indexPath.row]
            cell.lblUSDValue.text = "\(kCurrency) \(objItemList.amount ?? "0.00")"
            cell.lblDesc.text = objItemList.descriptionField ?? ""
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(btnEditClicked(sender:)), for: .touchUpInside)
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteClicked(sender:)), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
    // MARK: - Custom Methods
    @objc func btnEditClicked(sender:UIButton)
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        if let addItemVC = appDelegate.getViewController("AddItemsVC", onStoryboard: "PreAlert") as? AddItemsVC {
            addItemVC.type = "Edit"
            addItemVC.strTitle = "Edit Item"
            addItemVC.strButtonTitle = "Update"
            addItemVC.objItemList = arrItemsList[sender.tag]
            addItemVC.completionBlock = { objItemList in
                var totalAmount : Float = 0.00
                
                var objItem = self.arrItemsList[sender.tag]
                objItem = objItemList
                self.arrItemsList[sender.tag] = objItem
                
                for objItem in self.arrItemsList
                {
                    let amount = Float(objItem.amount ?? "0.00")!
                    totalAmount += amount
                }
                let dict = [
                    "description" : objItemList.descriptionField ?? "",
                    "amount" : objItemList.amount ?? "0.00"
                ]
                var dictItem =  self.arrParamItemsList[sender.tag]
                dictItem = dict
                self.arrParamItemsList[sender.tag] = dictItem
                DispatchQueue.main.async {
                    self.lblTotalAmount.text = "\(kCurrency) \(String(format:"%.2f",totalAmount))"
                    self.tblDesc.reloadData()
                    self.showDescriptionData()
                }
            }
            self.navigationController?.pushViewController(addItemVC, animated: true)
        }
    }
    @objc func btnDeleteClicked(sender:UIButton)
    {
        isAutoFillAPICallNeeded = false
        self.view.endEditing(true)
        var totalAmount : Float = 0.00
        if let popupVC = appDelegate.getViewController("DeletePopupVC", onStoryboard: "PreAlert") as? DeletePopupVC {
            popupVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
            objUserInfo = getUserInfo()
            popupVC.userID = objUserInfo.id ?? ""
            popupVC.isFromVC = "DeleteItem"
            popupVC.completionBlock = { status in
                if status == true
                {
                    self.arrItemsList.remove(at: sender.tag)
                    for objItem in self.arrItemsList
                    {
                        let amount = Float(objItem.amount ?? "0.00")!
                        totalAmount += amount
                    }
                    self.arrParamItemsList.remove(at: sender.tag)
                    DispatchQueue.main.async {
                        self.lblTotalAmount.text = "\(kCurrency) \(String(format:"%.2f",totalAmount))"
                        self.tblDesc.reloadData()
                        self.showDescriptionData()
                    }
                }
            }
            popupVC.modalPresentationStyle = .overFullScreen
            self.present(popupVC,animated: true, completion: nil)
        }
    }
}
class ItemDescCell : UITableViewCell
{
    @IBOutlet weak var lblUSDValue : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var redView : UIView!
    @IBOutlet weak var greenView : UIView!
    override func awakeFromNib() {
        redView.layer.cornerRadius = redView.frame.size.height/2
        redView.layer.masksToBounds = true
        greenView.layer.cornerRadius = greenView.frame.size.height/2
        greenView.layer.masksToBounds = true
    }
}

class MediaModel : NSObject{

    var id : Int!
    var type : Int! // type = 1 - Add / type = 2 - Media
    
    var docFileNames : String!
    var attachment : UIImage!
    var attachmentData : Data!
    var mimeType : String!
    var mediaType : Int! // 1 - Image 2 - Document
    var attachmentURL : String!
    var phAsset : PHAsset!
    var isFrom : Int = 0 // 0 -> local 1 -> web
    var localDocURL : String!
    override init() {
        
    }
}

//This will disable copy paste from textfield
class CustomTextField: UITextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
