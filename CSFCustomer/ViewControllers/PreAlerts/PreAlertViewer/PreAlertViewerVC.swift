//
//  PreAlertViewerVC.swift
//  CSFCustomer
//
//  Created by iMac on 04/08/22.
//

import UIKit

class PreAlertViewerVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var clvPreAlertViewer : UICollectionView!
    @IBOutlet weak var pageProgress: UIProgressView!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var viewNote: UIView!
    @IBOutlet weak var viewPreview: UIView!
    @IBOutlet weak var btnPreview: UIButton!
    // MARK: - Global Variables
    var currentPage : Int = 0
    var numberOfPages : Int = 0
    var scrollViewHeightConstraint: NSLayoutConstraint!
    var arrMedia : [MediaModel] = [MediaModel]()
    var intSelectedIndex : Int = 0
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        self.lblTitle.text = "Pre Alert Viewer"
        
        self.clvPreAlertViewer.delegate = self
        self.clvPreAlertViewer.isUserInteractionEnabled = true
        self.clvPreAlertViewer.isPagingEnabled = true
        
        viewPreview.layer.cornerRadius = viewPreview.frame.size.height/2
        viewPreview.layer.masksToBounds = true

        numberOfPages = arrMedia.count
        
        DispatchQueue.main.async { [self] in
            clvPreAlertViewer.reloadData()
            clvPreAlertViewer.scrollToItem(at: IndexPath(row: intSelectedIndex, section: 0), at: .top, animated: false)
            configureUI()
        }
    }
    
    // MARK: - configureUI
    func configureUI(){
        self.lblTitle.text = "\(self.currentPage + 1) of \(numberOfPages)"
        updateUI()
    }
    
    // MARK: - IBAction Methods
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPreviewClicked(_ sender: UIButton) {
        print("btnPreviewClicked")
        
        if let preAlertDocViewerVC = appDelegate.getViewController("PreAlertDocViewerVC", onStoryboard: "PreAlert") as? PreAlertDocViewerVC {
            let objMedia : MediaModel = self.arrMedia[self.currentPage]
            if objMedia.mediaType == 2 {
                preAlertDocViewerVC.objMedia = objMedia
                self.navigationController?.pushViewController(preAlertDocViewerVC, animated: true)
            }
        }
    }    
    
    // MARK: - Scroll View Delegate Methods
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        self.currentPage = Int(ceil(x/w))// + 1
        print("Current page is \(self.currentPage)")
        configureUI()
    }
    
    // MARK: - Update UI
    func updateUI(){
        
        let progressValue : Float = Float(self.currentPage + 1) / Float(self.numberOfPages)
        
        print("progress is \(progressValue)")
        self.pageProgress.progress = progressValue
        
        let objMedia : MediaModel = self.arrMedia[self.currentPage]
        self.lblFileName.text = "\(objMedia.docFileNames ?? "")"
        
        if objMedia.mediaType == 1 {
            self.viewPreview.isUserInteractionEnabled = false
            self.viewPreview.alpha = 0.7
        }else{
            self.viewPreview.isUserInteractionEnabled = true
            self.viewPreview.alpha = 1.0
        }
    }
}

extension PreAlertViewerVC {
    // MARK: - CollectionView Delegate and Datasource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell : PreAlertViewerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreAlertViewerCell", for: indexPath) as? PreAlertViewerCell{
            
            cell.imgPage.contentMode = .scaleAspectFit
            cell.imgPage.isHidden = false
            
            let objMedia : MediaModel = self.arrMedia[indexPath.row]
            if objMedia.mediaType == 1 {
                
                cell.imgPage.isHidden = false
                cell.imgDoc.isHidden = true
                
                if objMedia.isFrom == 0 {
                    //Local
                    cell.imgPage.image = objMedia.attachment
                }else {
                    //Web
                    if objMedia.attachmentURL != nil {
                        cell.imgPage.sd_setImage(with: URL(string:objMedia.attachmentURL), completed: { (image, error, type, url) in
                            if image != nil {
                                cell.imgPage.image = image
                            }else {
                                //Image Not found
                            }
                        })
                    }
                }
            }else {
                cell.imgPage.isHidden = true
                cell.imgDoc.isHidden = false
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
