//
//  AddPreAlertDocCell.swift
//  CSFMerchant
//
//  Created by Tops on 2/08/22.
//

import UIKit

class AddPreAlertDocCell: UICollectionViewCell {
    
    @IBOutlet weak var viewMedia: UIView!
    @IBOutlet weak var viewAddMedia: UIView!
    
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var imgDocThumb: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var btnAddMedia: UIButton!
    
    @IBOutlet weak var activityIndiCatorMedia: UIActivityIndicatorView!
    
    override class func awakeFromNib() {
    }

    override func layoutSubviews() {
    }
}
