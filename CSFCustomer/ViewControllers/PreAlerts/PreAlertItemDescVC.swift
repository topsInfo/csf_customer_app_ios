//
//  PreAlertItemDescVC.swift
//  CSFCustomer
//
//  Created by Tops on 07/07/21.
//

import UIKit

class PreAlertItemDescVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var tblItems : UITableView!
    @IBOutlet weak var lblTotalAmount : UILabel!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    
    // MARK: - Global Variable
    var arrItemsList : [ItemList] = [ItemList]()
    var strTotalAmount : String = "0.00"
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        tblItems.dataSource = self
        tblItems.delegate = self
        tblItems.estimatedRowHeight = 57
        tblItems.rowHeight = UITableView.automaticDimension
        if arrItemsList.count > 0
        {
            tblItems.isHidden = false
            lblNoRecordFound.isHidden = true
            lblTotalAmount.text = "\(kCurrency) \(strTotalAmount)"
        }
        else
        {
            tblItems.isHidden = true
            lblNoRecordFound.isHidden = false
            lblTotalAmount.text = "\(kCurrency) 0.00"
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItemsList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ItemsCell", for: indexPath) as? ItemsCell{
            let objItemList = arrItemsList[indexPath.row]
            cell.lblDesc.text = objItemList.descriptionField ?? ""
            cell.lblUSDAmount.text = "\(kCurrency) \(objItemList.amount ?? "0.00")"
            return cell
        }
        return UITableViewCell()
    }
}
class ItemsCell : UITableViewCell
{
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var lblUSDAmount : UILabel!
}
