//
//  DriverContactDetailVC.swift
//  CSFCustomer
//
//  Created by iMac on 11/02/22.
//

import UIKit
import SwiftyJSON

class DriverContactDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var tblDriverContactDetail: UITableView!
    @IBOutlet weak var viewList: UIView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewSearchBar: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnCloseSearch: UIButton!
    
    // MARK: - Global Variables
    var objUserInfo = UserInfo()
    
    var objDriverData = DriverDataModel()
    var arrDriverDataList : [DriverList] = [DriverList]()
    var tableDataArr = Array<Any>()
    var refreshControl = UIRefreshControl()
    var screenTitle : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
        getDriverDetailData(isShowLoader: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - configureControls
    func configureControls(){
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblDriverContactDetail.addSubview(refreshControl)
        
        tblDriverContactDetail.delegate = self
        tblDriverContactDetail.dataSource = self
        tblDriverContactDetail.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tblDriverContactDetail.separatorStyle = .none
        tblDriverContactDetail.sectionHeaderHeight = 0
        
        lblDescription.text = "To assist members with queries or requests relating to the services of CSF Couriers Ltd. for example details on your invoice or package."
        
        txtSearch.delegate  = self
        txtSearch.paddingView(xvalue: 10)
        
        hideSearchControls()
    }
    
    // MARK: - hideSearchControls
    func hideSearchControls(){
        viewSearchBar.isHidden = true
        btnCloseSearch.isHidden = true
        btnSearch.isHidden = false
    }
    
    // MARK: - showSearchControls
    func showSearchControls(){
        viewSearchBar.isHidden = false
        btnSearch.isHidden = false
    }
    
    // MARK: - Refresh
    /**
        This method is called when the refresh action is triggered.
        It is marked with @objc attribute to make it accessible from Objective-C code.
    */    
    @objc func refresh()
    {
        self.getDriverDetailData(isShowLoader: false)
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked(_ sender: UIButton) {
        print("btnBackClicked")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCallUsClicked(_ sender: UIButton) {
        print("btnCallUsClicked")
        if let contactNumber : String = objDriverData.contactNo {
            callUs(contactNumber: contactNumber)
        }
    }
    
    @IBAction func btnContactSupportClicked(_ sender: UIButton) {
        print("btnContactSupportClicked")
        if let tabBarVC = self.revealViewController()?.frontViewController as? UITabBarController
        {
            tabBarVC.selectedIndex = 2
        }
    }
    
    @IBAction func btnSearchClicked(_ sender: UIButton) {
        self.viewSearchBar.isHidden = false
        self.btnCloseSearch.isHidden = false
        self.btnSearch.isHidden = true
        self.txtSearch.text = ""
        txtSearch.becomeFirstResponder()
    }
    
    @IBAction func btnCloseSearchClicked(_ sender: UIButton) {
        txtSearch.resignFirstResponder()
        hideSearchControls()
        clearFilter()
    }
    
    // MARK: - CallUs
    func callUs(contactNumber : String){
        let mobile = contactNumber.replacingOccurrences(of: " ", with: "")
        if let url = URL(string: "tel://+1\(mobile)"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    
    // MARK: - Tableview delegate and datasource methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : DriverCell?
        let member = tableDataArr[indexPath.row]

        if let grandP = member as? DriverList
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "DriverCell") as? DriverCell
            cell!.lblInitials.text = grandP.driverName.initials
            cell!.lblName.text = grandP.driverName
            cell!.lblPosition.text = grandP.type

            if tableDataArr.indices.contains(indexPath.row + 1) {
                let nextMember = tableDataArr[indexPath.row + 1]
                if (nextMember as? ZoneList) != nil {
                    cell!.cnstDriverCardBottom.constant = 0
                    cell!.viewDriverCardSeperator.isHidden = false
                    
                    cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 10, isTopLeftTopRight: true, isBottomLeftBottomRight: false)
                }else{
                    cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 10, isTopLeftTopRight: true, isBottomLeftBottomRight: true)
                    cell!.cnstDriverCardBottom.constant = 12
                    cell!.viewDriverCardSeperator.isHidden = true
                }
            }else{

                cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 10, isTopLeftTopRight: true, isBottomLeftBottomRight: true)
                cell!.cnstDriverCardBottom.constant = 12
                cell!.viewDriverCardSeperator.isHidden = true
            }
            return cell!
        }
        else if let parent = member as? ZoneList
        {
            let cell : ZoneCell?
            cell = tableView.dequeueReusableCell(withIdentifier: "ZoneCell") as? ZoneCell
            cell!.lblZoneName.text = parent.title

            if parent.isExpanded {
                cell!.imgDropDown.image = UIImage(named: "upArrow")
            }else {
                cell!.imgDropDown.image = UIImage(named: "Down-1")
            }
            
            if tableDataArr.indices.contains(indexPath.row + 1) {
                let nextMember = tableDataArr[indexPath.row + 1]
                
                if (nextMember as? ZoneList) != nil {
                    cell!.cnstZoneCardBottom.constant = 0
                    cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 0, isTopLeftTopRight: false, isBottomLeftBottomRight: false)
                }else{
                    if (nextMember as? ChildModel) != nil {
                        cell!.cnstZoneCardBottom.constant = 0
                        cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 0, isTopLeftTopRight: false, isBottomLeftBottomRight: false)
                    }else{
                        cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 10, isTopLeftTopRight: false, isBottomLeftBottomRight: true)
                        cell!.cnstZoneCardBottom.constant = 12
                    }
                }
            }else{

                cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 10, isTopLeftTopRight: false, isBottomLeftBottomRight: true)
                cell!.cnstZoneCardBottom.constant = 12
            }
            return cell!
        }
        else if let child = member as? ChildModel
        {
            let cell : DescInfoCell?
            cell = tableView.dequeueReusableCell(withIdentifier: "DescInfoCell") as? DescInfoCell
            cell!.lblZoneDescription.text = child.zoneDescData
            
            if tableDataArr.indices.contains(indexPath.row + 1) {
                let nextMember = tableDataArr[indexPath.row + 1]
                
                if (nextMember as? ZoneList) != nil {
                    cell!.cnstZoneDescBottom.constant = 0
                    
                    cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 0, isTopLeftTopRight: false, isBottomLeftBottomRight: false)
                }else{
                    cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 10, isTopLeftTopRight: false, isBottomLeftBottomRight: true)
                    cell!.cnstZoneDescBottom.constant = 12
                }
            }else{
                cell!.viewCard.addShadowToCard(offset: CGSize.init(width: 0, height: 0), color: Colors.support_card_shadow_color, radius: 3.0, cornerRadius: 10, isTopLeftTopRight: false, isBottomLeftBottomRight: true)
                cell!.cnstZoneDescBottom.constant = 12
            }
            return cell!
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        let row = indexPath.row
        let member = tableDataArr[row]
        var ipsArr = Array<IndexPath>()
        
        if (member as? DriverList) != nil {
//            if !grandP.isExpanded
//            {
//                // Insert next level items
//                grandP.isExpanded = true
//                for (index, value) in grandP.zoneList.enumerated()
//                {
//                    self.tableDataArr.insert(value, at: row + index + 1)
//                    let ip = IndexPath(row: row + index + 1, section: 0)
//                    ipsArr.append(ip)
//                }
//                tableView.beginUpdates()
//                tableView.insertRows(at: ipsArr, with: .left)
//                tableView.endUpdates()
//            }
//            else
//            {
//                // Delete next level items
//                var count = 1
//                while row + 1 < tableDataArr.count
//                {
//                    let element = tableDataArr[row + 1]
//                    if !(element is DriverList)
//                    {
//                        (element as? ZoneList)?.isExpanded = false
//                        (element as? ChildModel)?.isExpanded = false
//                        self.tableDataArr.remove(at: row + 1)
//                        let ip = IndexPath(row: row + count, section: 0)
//                        ipsArr.append(ip)
//                        count += 1
//
//                    }
//                    else if (element is DriverList)
//                    {
//                        break
//                    }
//                }
//                grandP.isExpanded = false
//                tableView.beginUpdates()
//                tableView.deleteRows(at: ipsArr, with: .right)
//                tableView.endUpdates()
//            }
        }
        else if let parent = member as? ZoneList
        {
//            if parent.areaList == "" { return }
            if !parent.isExpanded
            {
                parent.isExpanded = true
                for (index, value) in parent.childMArr.enumerated()
                {
                    self.tableDataArr.insert(value, at: row + index + 1)
                    let ip = IndexPath(row: row + index + 1, section: 0)
                    ipsArr.append(ip)
                }
                tableView.beginUpdates()
                tableView.insertRows(at: ipsArr, with: .none)
                tableView.endUpdates()
            }
            else
            {
                // Delete next level items
                var count = 1
                while row + 1 < tableDataArr.count
                {
                    let element = tableDataArr[row + 1]
                    if (element is DriverList) || (element is ZoneList)
                    {
                        break
                    }
                    else if !(element is DriverList)
                    {
                        (element as? ZoneList)?.isExpanded = false
                        (element as? ChildModel)?.isExpanded = false
                        self.tableDataArr.remove(at: row + 1)
                        let ip = IndexPath(row: row + count, section: 0)
                        ipsArr.append(ip)
                        count += 1
                    }
                }
                parent.isExpanded = false
                tableView.beginUpdates()
                tableView.deleteRows(at: ipsArr, with: .none)
                tableView.endUpdates()
            }
            tblDriverContactDetail.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    // MARK: - Textfield delegate methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        applyFilter(search: textField.text ?? "")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        applyFilter(search: textField.text ?? "")
    }
}

// MARK: - Extension DriverContactDetailVC
extension DriverContactDetailVC {
    /**
     Retrieves the driver detail data.
     */
    func getDriverDetailData(isShowLoader : Bool)
    {
        objUserInfo = getUserInfo()

        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? "" //UserInfo.shared.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driverDetail, showLoader: isShowLoader) { [self] (resultDict, status, message) in
            refreshControl.endRefreshing()

            if status == true
            {
                print("resultDict is \(resultDict)")
                objDriverData = DriverDataModel(fromJson: JSON(resultDict))
                arrDriverDataList = objDriverData.driverList
                tableDataArr = arrDriverDataList
                self.tblDriverContactDetail.reloadData()
                formateListData()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - formateListData
    /**
     Formats the list data.
     */     
    func formateListData(){
        // Code implementation goes here
    
        DispatchQueue.main.async { [self] in
            
            var row = 0
            
            for (i, obj) in tableDataArr.enumerated() {
                let indexPath : IndexPath = IndexPath(row: i, section: 0)
                tblDriverContactDetail.cellForRow(at: indexPath)?.isSelected = false
                
                if i == 0 {
                    row = 0
                }else{
                    print("Row value is \n\(row)")
                    if tableDataArr.indices.contains(row) {
                        if let obj = tableDataArr[row] as? DriverList {
                            row += obj.zoneList.count + 1
                        }
                    }
                }
                let member = obj//tableDataArr[i]
                var ipsArr = Array<IndexPath>()
                if let grandP = member as? DriverList
                {
                    if !grandP.isExpanded
                    {
                        // Insert next level items
                        grandP.isExpanded = true
                        for (index, value) in grandP.zoneList.enumerated()
                        {
                            self.tableDataArr.insert(value, at: row + index + 1)
                            let ip = IndexPath(row: row + index + 1, section: 0)
                            ipsArr.append(ip)
                        }
                        tblDriverContactDetail.beginUpdates()
                        tblDriverContactDetail.insertRows(at: ipsArr, with: .left)
                        tblDriverContactDetail.endUpdates()
                    }
                }
            }
            reloadData()
        }
    }

    // MARK: - reloadData
    /**
     Reloads the data for the DriverContactDetailVC view controller.
     */     
    func reloadData(){
        DispatchQueue.main.async { [self] in
            print("Array is\n \(tableDataArr)")
            
            if tableDataArr.count > 0 {
                lblNoDataFound.isHidden = true
            }else  {
                lblNoDataFound.isHidden = false
            }
            self.tblDriverContactDetail.reloadData()
        }
    }
    
    // MARK: - applyFilter
    /**
     Applies a filter to the data based on the given search string.
     */     
    func applyFilter(search : String){
        //Make all data as collapsable by default
        arrDriverDataList = objDriverData.driverList
        arrDriverDataList.indices.forEach {
            arrDriverDataList[$0].isExpanded = false
        }
        if !search.isEmpty {
            arrDriverDataList = arrDriverDataList.filter { ((($0 as? DriverList)?.driverName.uppercased().contains(search.trimmingCharacters(in: .whitespacesAndNewlines).uppercased())) as! Bool) || ((($0 as? DriverList)?.zone.uppercased().contains(search.trimmingCharacters(in: .whitespacesAndNewlines).uppercased())) as! Bool)}
        }
        
        tableDataArr = Array<Any>()
        tableDataArr = arrDriverDataList
        self.tblDriverContactDetail.reloadData()
        formateListData()
    }
    
    // MARK: - clearFilter
    /**
     Clears the filter applied to the driver contact details.
     */     
    func clearFilter(){
        applyFilter(search: "")
    }
}

// MARK: - DriverCell
class DriverCell : UITableViewCell {
    @IBOutlet weak var lblInitials : UILabel!
    @IBOutlet weak var lblName : TapAndCopyLabel!
    @IBOutlet weak var lblPosition : UILabel!
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var viewProfilePic: UIView!
    @IBOutlet weak var cnstDriverCardBottom: NSLayoutConstraint!
    
    @IBOutlet weak var viewDriverCardSeperator: UIView!
    override func awakeFromNib() {
        viewProfilePic.layer.cornerRadius = viewProfilePic.frame.size.height/2
        viewProfilePic.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
    }
}

// MARK: - ZoneCell
class ZoneCell : UITableViewCell {
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var lblZoneName : UILabel!
    @IBOutlet weak var imgDropDown: UIImageView!    
    @IBOutlet weak var cnstZoneCardTop: NSLayoutConstraint!
    @IBOutlet weak var cnstZoneCardBottom: NSLayoutConstraint!
    override func awakeFromNib() {

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
    }
}

// MARK: - DescInfoCell
class DescInfoCell : UITableViewCell {
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var lblZoneDescription: TapAndCopyLabel!
    @IBOutlet weak var cnstZoneDescTop: NSLayoutConstraint!
    @IBOutlet weak var cnstZoneDescBottom: NSLayoutConstraint!
    override func awakeFromNib() {

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
    }
}

// MARK: - ChildModel
class ChildModel: NSObject {
    var depthLevel = 2
    var isExpanded = false
    var zoneDescData : String!
}

extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
