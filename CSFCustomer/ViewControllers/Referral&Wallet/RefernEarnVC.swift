//
//  InviteVC.swift
//  CSFCustomer
//
//  Created by Tops on 24/02/21.
//

import UIKit
import Firebase
import FirebaseDynamicLinks
class RefernEarnVC: UIViewController
{
    // MARK: - IBOutlets
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var lblReferralCode : UILabel!
    @IBOutlet weak var btnCopy : UIButton!
    @IBOutlet weak var codeStackview : UIStackView!
    @IBOutlet weak var lblDesc : UILabel!
    
    // MARK: - Global Variable
    var invitationCode :String = ""
    var shareURL : URL?
    var objUserInfo = UserInfo()
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        topBarView.topBarBGColor()
        objUserInfo = getUserInfo()
        invitationCode = objUserInfo.referralCode ?? ""
        createReferLink()
        lblReferralCode.text = "    \(objUserInfo.referralCode ?? "")"
        
        codeStackview.setDotBorder(newwidth:170)
        
        lblReferralCode.clipsToBounds = true
        lblReferralCode.layer.cornerRadius = 20
        lblReferralCode.layer.maskedCorners = [.layerMinXMinYCorner,.layerMinXMaxYCorner]
        
        btnCopy.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        btnCopy.layer.cornerRadius = 20
        btnCopy.clipsToBounds = true
        
        lblDesc.text = "Invite your friends to join CSF and receive $\(referralAmount) \n in your wallet for each friend that joins using your referral code.\n\n\nYour friend will also receive $\(referralAmount) \(kCurrency) in their wallet \nfor this referral."
    }
    
    // MARK: - createReferLink
    func createReferLink() {
        self.showHUD()
        //     guard (Auth.auth().currentUser?.uid) != nil else { return }
        // "https://vtab.page.link/referralcode?referralcode=\(lblInviteCode.text ?? "")"
        let link = URL(string: "https://csfcustomer.page.link/open?referralCode=\(invitationCode)")
        let referralLink = DynamicLinkComponents(link: link!, domainURIPrefix: "https://csfcustomer.page.link")
        
        referralLink!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.csfcustomer")
        referralLink!.iOSParameters?.minimumAppVersion = kMiniumAppVersion
        referralLink!.iOSParameters?.appStoreID = kAppstoreID
        
        referralLink!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.csfcustomer")
        referralLink!.androidParameters?.minimumVersion = 1
        
        referralLink!.navigationInfoParameters = DynamicLinkNavigationInfoParameters()
        referralLink!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        referralLink!.shorten { (shortURL, warnings, error) in
            if let error = error {
                self.hideHUD()
               // print(error.localizedDescription)
                return
            }
            if let shorturl = shortURL
            {
                self.hideHUD()
               // print(shortURL)
                self.shareURL = shorturl
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCopyCodeClicked()
    {
        guard let shareURL = self.shareURL else {
            return
        }
        if shareURL.absoluteString != ""
        {
            UIPasteboard.general.string = shareURL.absoluteString
            showAlert(title: "", msg: Messsages.msg_code_copied, vc: self)
        }
    }
    @IBAction func btnInviteNowClicked()
    {
        if let url : URL = shareURL {
            let message = "Congratulations! Your Friend \(objUserInfo.firstName ?? "") \(objUserInfo.lastName ?? "")  wishes to invite you to sign up with CSF Couriers and you will receive 5 USD rewards.Click the link below to Register!\n\n\(url)"
            
            let textToShare : [Any] = [message]
    //        let textToShare : [Any] = [message,shareURL!]
            let activityViewController = UIActivityViewController(activityItems: textToShare as [Any], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            // exclude some activity types from the list (optional)
            //  activityViewController.excludedActivityTypes = []
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
}
