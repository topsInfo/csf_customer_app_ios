//
//  WalletVC.swift
//  CSFCustomer
//
//  Created by Tops on 08/03/21.
//

import UIKit
import SwiftyJSON
class WalletVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var lblTTDWalletAmt : UILabel!
    @IBOutlet weak var lblUSDWalletAmt : UILabel!
    
    @IBOutlet weak var lblPromotionalTTDAmt : UILabel!
    @IBOutlet weak var lblPromotionalUSDAmt : UILabel!
    
    @IBOutlet weak var tblWallet : UITableView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var stackViewPromotionaAmt : UIStackView!
    
    @IBOutlet weak var imgFreeze: UIImageView!
    @IBOutlet weak var lblFreezeTitle: UILabel!
    @IBOutlet weak var lblFreezeSubTitle: UILabel!
    
    @IBOutlet weak var viewWalletFreezeMsg: UIView!
    @IBOutlet weak var viewWalletBalance: UIView!
    @IBOutlet weak var viewAddToWallet: UIView!
    @IBOutlet weak var viewWalletData: UIView!
    @IBOutlet weak var viewTblHeaderSection: UIView!

    // MARK: - Global Variable
    var arrHistoryList : [HistoryList] = [HistoryList]()
    var creditSign : String = "+"
    var debitSign : String = "-"
    var isLoadingList : Bool = false
    var pageNo : Int = 1
    var totalPages : Int = 0
    var blockedAmt = "Blocked Amount"
    var refreshControl = UIRefreshControl()
    
    //Freeze Wallet
    var objWalletFreeze = WalletFreeze()
    var walletFreezeMsg : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideViews()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        getWalletDetails(isShowLoader: true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }

    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(getUpdatedRecords(_:)), name: NSNotification.Name(rawValue: "WalletUpdated"), object: nil)
        topBarView.topBarBGColor()
        lblNoRecordFound.isHidden = true
        tblWallet.dataSource = self
        tblWallet.delegate = self
        tblWallet.estimatedRowHeight = 80
        tblWallet.rowHeight = UITableView.automaticDimension
        tblWallet.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblWallet.addSubview(refreshControl)
    }
    
    @objc func refresh()
    {
        getWalletDetails(isShowLoader: false)
    }
    
    /**
     Retrieves the wallet details.     
     */
    func getWalletDetails(isShowLoader : Bool){
        getWalletHistory(isShowLoader: isShowLoader)
        checkWalletFreeze(isShowLoader: isShowLoader)
    }
    
    /**
     Hides the views in the WalletVC.
     */
    func hideViews(){
        viewWalletData.alpha = 0
        tblWallet.alpha = 0
        viewTblHeaderSection.alpha = 0
    }
   
    /**
     Shows the views for the WalletVC.
     */
    func showViews(){        
        viewWalletData.alpha = 1
        tblWallet.alpha = 1
        viewTblHeaderSection.alpha = 1
    }
    
    /**
        This method is called when an updated record notification is received.
    */
    @objc func getUpdatedRecords(_ notification : Notification)
    {
        pageNo = 1
        getWalletHistory(isShowLoader: true)
    }
    
    // MARK: - getWalletHistory
    func getWalletHistory(isShowLoader : Bool)
    {
        let objUserInfo = getUserInfo()
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["page_id"] = pageNo
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.GetWalletHistory, showLoader:isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            if status == true
            {
                self.isLoadingList = false
                let objWalletHistory = WalletHistory(fromJson: JSON(resultDict))
                if self.pageNo == 1
                {
                    self.arrHistoryList.removeAll()
                }
                if self.arrHistoryList.count > 0
                {
                    self.arrHistoryList.append(contentsOf: objWalletHistory.historyList ?? [])
                }
                else
                {
                    self.arrHistoryList = objWalletHistory.historyList ?? []
                }
                self.totalPages = objWalletHistory.totalPage ?? 1
                if self.arrHistoryList.count > 0
                {
                    self.lblNoRecordFound.isHidden = true
                }
                else
                {
                    self.lblNoRecordFound.isHidden = false
                }
                
                self.lblUSDWalletAmt.text = "   Wallet USD : \(kCurrencySymbol)\(objWalletHistory.walletUsd ?? "0")"
                self.lblTTDWalletAmt.text = "   Wallet TTD : \(kCurrencySymbol)\(objWalletHistory.walletTtd ?? "0")"
                
                self.lblPromotionalUSDAmt.text = "   Promotional USD : \(kCurrencySymbol)\(objWalletHistory.promotionalWalletUsd ?? "0")"
                self.lblPromotionalTTDAmt.text = "   Promotional TTD : \(kCurrencySymbol)\(objWalletHistory.promotionalWalletTtd ?? "0")"
                
                DispatchQueue.main.async {
                    self.tblWallet.reloadData()
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnAddMoneyClicked()
    {
        if let vc = appDelegate.getViewController("AddMoneyVC", onStoryboard: "Referral") as? AddMoneyVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHistoryList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell") as? WalletCell {
            let objWalletHistory = arrHistoryList[indexPath.row]
            cell.lblTransID.text = objWalletHistory.transactionId ?? ""
            cell.lblLogs.text = objWalletHistory.logs ?? ""
            cell.lblLogs.numberOfLines = 0
            cell.lblDate.text = objWalletHistory.date ?? ""
            cell.lblTTDAmt.text = "\(kTTCurrency) \(kCurrencySymbol)\(objWalletHistory.amountTtd ?? "0")"
            
            if let type = objWalletHistory.type,type == 1
            {
                cell.lblUSDAmt.textColor = UIColor(named:"theme_green_color")//Colors.theme_green_color
                cell.lblUSDAmt.text = "\(creditSign) \(kCurrency) \(kCurrencySymbol)\(objWalletHistory.amountUsd ?? "0")"
            }
            else
            {
                cell.lblUSDAmt.textColor = UIColor(named:"theme_red_color")//Colors.theme_red_color
                cell.lblUSDAmt.text = "\(debitSign) \(kCurrency) \(kCurrencySymbol)\(objWalletHistory.amountUsd ?? "0")"
            }
            return cell
        }
       return UITableViewCell()
    }
    //Pagination
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isLoadingList = false
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (Int(tblWallet.contentOffset.y + tblWallet.frame.size.height) >= Int(tblWallet.contentSize.height))
        {
            if !isLoadingList{
                isLoadingList = true
                if self.totalPages == pageNo
                {
                    return
                }
                pageNo += 1
                getWalletHistory(isShowLoader: true)
            }
        }
    }
}
class WalletCell : UITableViewCell
{
    @IBOutlet weak var lblTransID : TapAndCopyLabel!
    @IBOutlet weak var lblLogs : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTTDAmt : UILabel!
    @IBOutlet weak var lblUSDAmt : UILabel!
}

extension WalletVC {
    func checkWalletFreeze(isShowLoader : Bool)
    {
        let objUserInfo = getUserInfo()

        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""

        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.checkWalletFreez, showLoader: isShowLoader) { (resultDict, status, message) in
            self.showViews()
            if status == true
            {
                self.objWalletFreeze = WalletFreeze(fromJson: JSON(resultDict))
                print("isWalletFreeze is \(String(describing: self.objWalletFreeze.isWalletFreeze))")
                self.walletFreezeMsg = message
                
                if self.objWalletFreeze.isWalletFreeze {
                    self.lblFreezeSubTitle.text = message
                    self.viewWalletFreezeMsg.isHidden = false
                    self.viewAddToWallet.isHidden = true
                }else {
                    self.viewWalletFreezeMsg.isHidden = true
                    self.viewAddToWallet.isHidden = false
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}
