//
//  ScannerVC.swift
//  CSFCustomer
//
//  Created by iMac on 08/08/22.
//
protocol SannerOutputDelegate {
    func getScannerOutput(output:String)
}

import UIKit
import AVFoundation

class ScannerVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate{

    // MARK: - IBOutlets
    @IBOutlet weak var scannerView: UIView!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var imgFlash: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!

    // MARK: - Global Variables
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var scannerOutputDelegte : SannerOutputDelegate?
    var isFlashOn : Bool = false
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        lblTitle.text = "Pickup Kiosk"
        setUpScanner()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        flashOff()
        stopScanner()
    }
    
    // MARK: - setUpScanner
    func setUpScanner(){
        
        if let captureDevice = AVCaptureDevice.default(for: .video) {
            do {
                // Get an instance of the AVCaptureDeviceInput class using the previous device object.
                let input = try AVCaptureDeviceInput(device: captureDevice)
                
                // Set the input device on the capture session.
                
                if (captureSession.canAddInput(input)) {
                    captureSession.addInput(input)
                } else {
                    failed()
                    return
                }
                
                // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
                let captureMetadataOutput = AVCaptureMetadataOutput()
                captureSession.addOutput(captureMetadataOutput)
                
                // Set delegate and use the default dispatch queue to execute the call back
                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
                
                // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                videoPreviewLayer?.frame = view.layer.bounds
                scannerView.layer.addSublayer(videoPreviewLayer!)
                
                // Start video capture.
                DispatchQueue.global(qos: .background).async {
                    self.captureSession.startRunning()
                }
                
                qrCodeFrameView = UIView()
                
                if let qrCodeFrameView = qrCodeFrameView {
                    qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                    qrCodeFrameView.layer.borderWidth = 2
                    scannerView.addSubview(qrCodeFrameView)
                    scannerView.bringSubviewToFront(qrCodeFrameView)
                }
                
            } catch {
                // If any error occurs, simply print it out and don't continue any more.
                print(error)
                return
            }
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            print("No QR code is detected")
            return
        }
        
        // Get the metadata object.
        if let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
            if metadataObj.type == AVMetadataObject.ObjectType.qr {
                // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
                let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
                qrCodeFrameView?.frame = barCodeObject!.bounds
                
                if metadataObj.stringValue != nil {
                    print("Scan output is \(String(describing: metadataObj.stringValue))")
                    
                    if let strQRCodeValue : String = metadataObj.stringValue {
                        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                        found(code: strQRCodeValue)
                    }
                }
            }
        }
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
//        captureSession = nil
    }
    func found(code: String) {
        print(code)
        stopScanner()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [self] in            
            dismiss(animated: true) {
                scannerOutputDelegte?.getScannerOutput(output: code)
            }
        }
    }
    
    func stopScanner(){
        if (captureSession.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnBackClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnFlashClicked(_ sender: UIButton) {
        if !isFlashOn {
            imgFlash.image = UIImage(named: "icFlashOn")
            self.flashOn()
        }else {
            imgFlash.image = UIImage(named: "icFlashOff")
            self.flashOff()
        }
    }
    
    func flashOn(){
        toggleTorch(on: true)
        isFlashOn = true
    }
    
    func flashOff(){
        toggleTorch(on: false)
        isFlashOn = false
    }
    
    func toggleTorch(on: Bool) {
        guard
            let device = AVCaptureDevice.default(for: AVMediaType.video),
            device.hasTorch
        else { return }

        do {
            try device.lockForConfiguration()
            device.torchMode = on ? .on : .off
            device.unlockForConfiguration()
        } catch {
            print("Torch could not be used")
        }
    }
}
