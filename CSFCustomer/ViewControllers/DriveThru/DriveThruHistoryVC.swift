//
//  DriveThruHistoryVC.swift
//  CSFCustomer
//
//  Created by iMac on 21/06/22.
//

import UIKit
import SwiftyJSON

class DriveThruHistoryVC: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tblDriveThruHistoryList : UITableView!
    @IBOutlet weak var viewTotalRecord : TotalRecordView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var viewSearchOption : UIView!
    @IBOutlet weak var viewSearchTextContainer : UIView!

    @IBOutlet weak var btnSearchOption : UIButton!
    @IBOutlet weak var cnstTotalRecordsTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var cnstSearchOptionHeight : NSLayoutConstraint!

    // MARK: - Global Variables
    var refreshControl = UIRefreshControl()
    var pageNo : Int = 1
    var totalPages : Int = 0
    var isShowLoader : Bool = true
    var isLoadingList : Bool = false
    var objDriveThruRequest = DriveThruRequestList()
    var arrDriveThruRequestList : [DriveThruRequestList] = [DriveThruRequestList]()
    var isSearchOptionOpen : Bool = false
    
    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        hideSearchOption()
//        self.btnSearchOption.isUserInteractionEnabled = false
        
        viewSearchTextContainer.layer.borderWidth = 1
        viewSearchTextContainer.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor

        cnstTotalRecordsTitleHeight.constant = 0
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblDriveThruHistoryList.addSubview(refreshControl)
        
        tblDriveThruHistoryList.delegate = self
        tblDriveThruHistoryList.dataSource = self
        tblDriveThruHistoryList.estimatedRowHeight = 123.0
        tblDriveThruHistoryList.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tblDriveThruHistoryList.frame.size.width, height: .leastNormalMagnitude))
        configureTextFields()
        getDriveThruList(searchText: txtSearch.text ?? "", isShowLoader: true)
     }
    
    // MARK: - Custom methods
    @objc func refresh()
    {
        self.pageNo = 1
        getDriveThruList(searchText: txtSearch.text ?? "", isShowLoader: false)
    }
    
    func showSearchOption(){
//        DispatchQueue.main.async {
            self.cnstSearchOptionHeight.constant = 62
            self.viewSearchOption.isHidden = false
//        }
    }
    
    func hideSearchOption(){
        self.view.endEditing(true)
        self.cnstSearchOptionHeight.constant = 0
        self.viewSearchOption.isHidden = true
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func btnSearchOptionClicked(_ sender: UIButton) {
        print("btnSearchOptionClicked")
        if !isSearchOptionOpen {
            isSearchOptionOpen = true
            self.showSearchOption()
        }else {
            isSearchOptionOpen = false
            self.hideSearchOption()
        }

    }
    
    @IBAction func btnSearchClicked(_ sender: UIButton) {
        print("btnSearchClicked")
        self.pageNo = 1
        self.getDriveThruList(searchText: txtSearch.text ?? "", isShowLoader: true)
    }
}

extension DriveThruHistoryVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DriveThruHistoryCell", for: indexPath) as? DriveThruHistoryCell {
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(driveThruDetailTapped(sender:)))
            cell.stackViewDriveThru.tag = indexPath.row
            cell.stackViewDriveThru.isUserInteractionEnabled = true
            cell.stackViewDriveThru.addGestureRecognizer(tapGesture)
            
            cell.btnCellBack.tag = indexPath.row
            cell.btnCellBack.addTarget(self, action: #selector(self.btnCellBackClicked(_:)), for: .touchUpInside)
            
            if self.arrDriveThruRequestList.count > 0 {
                let objRequest : DriveThruRequestList = self.arrDriveThruRequestList[indexPath.row]
                cell.lblRequestId.text = objRequest.requestId ?? ""
                cell.lblPackages.text = "Invoice(s) : \(objRequest.count ?? 0)"
                cell.lblDate.text = objRequest.dateString ?? ""
                cell.lblStatus.text = objRequest.status ?? ""
                
                if (objRequest.statusId == DriveThruRequestStatusId.CustomerConfirmation.rawValue || objRequest.statusId == DriveThruRequestStatusId.Cancelled.rawValue) {
                    cell.lblStatus.textColor = Colors.theme_orange_color
                    cell.viewStatus.backgroundColor = Colors.theme_orange_color.withAlphaComponent(0.2)
                }else{
                    cell.lblStatus.textColor = Colors.theme_green_color
                    cell.viewStatus.backgroundColor = Colors.theme_green_color.withAlphaComponent(0.2)
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDriveThruRequestList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    @objc func driveThruDetailTapped(sender:UITapGestureRecognizer) //new change
    {
        let tag = sender.view!.tag
        redirectToRequestDetailVC(index: tag)
    }
    @objc func btnCellBackClicked(_ sender:UIButton)
    {
        self.redirectToRequestDetailVC(index: sender.tag)
    }
    
    func redirectToRequestDetailVC(index : Int){
        if let vc = appDelegate.getViewController("RequestDetailVC", onStoryboard: "DriveThru") as? RequestDetailVC{
            vc.isFromHistoryPage = true
            let objRequest : DriveThruRequestList = self.arrDriveThruRequestList[index]
            vc.requestId = objRequest.id ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isLoadingList &&
            indexPath.row == arrDriveThruRequestList.count - 1 &&
            pageNo < self.totalPages {
            isLoadingList = true
            pageNo += 1
            self.getDriveThruList(searchText: txtSearch.text ?? "", isShowLoader: pageNo > 2)
        }
    }
}

extension DriveThruHistoryVC {
    func getDriveThruList(searchText : String, isShowLoader : Bool)
    {
        self.view.endEditing(true)

        let objUserInfo = getUserInfo()

        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["page_id"] = pageNo
        params["search_text"] = searchText
        params["type"] = DriveThruRequestListType.History.rawValue

        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driveThruList, showLoader: isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            if status == true
            {
                self.isLoadingList = false
                if self.pageNo == 1
                {
                    self.arrDriveThruRequestList.removeAll()
                }
                let objDriveThruRequestList = DriveThruRequestData.init(fromJson: JSON(resultDict))
                if self.arrDriveThruRequestList.count > 0
                {
                    self.arrDriveThruRequestList.append(contentsOf: objDriveThruRequestList.list)
                }
                else
                {
                    self.arrDriveThruRequestList = objDriveThruRequestList.list
                }
                
                if let totalCount = objDriveThruRequestList.totalCount
                {
                    self.totalRecordsCount = totalCount
                }
                
                if let totalPage = objDriveThruRequestList.totalPage
                {
                    self.totalPages = totalPage
                }

                DispatchQueue.main.async {
                    self.reloadData()
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    func reloadData(){
        self.tblDriveThruHistoryList.isHidden = false

        if self.arrDriveThruRequestList.count > 0
        {
            self.lblNoRecordFound.isHidden = true
        }
        else
        {
            self.lblNoRecordFound.isHidden = false
        }
        self.tblDriveThruHistoryList.reloadData()
        
        if self.totalRecordsCount > 0 {
            self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
        }else {
            self.cnstTotalRecordsTitleHeight.constant = 0
        }
        
        self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: 0, totalRecords: totalRecordsCount)
        self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: 0, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_REQUESTS)
    }
}

extension DriveThruHistoryVC : UITextFieldDelegate {
    func configureTextFields()
    {
        txtSearch.delegate = self
        //Colors.theme_black_color for all textfield , bgcolor: Colors.theme_white_color
        configureTextField(textField: txtSearch, placeHolder: "Search by request number", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        txtSearch.paddingView(xvalue: 10)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtSearch {
            self.pageNo = 1
            getDriveThruList(searchText: txtSearch.text ?? "", isShowLoader: true)
        }
        return true
    }
}
