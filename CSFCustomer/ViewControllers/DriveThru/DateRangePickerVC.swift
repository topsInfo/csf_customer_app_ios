//
//  DateRangePickerVC.swift
//  FSCalendarRangeDemo
//
//  Created by iMac on 30/12/21.
//

import UIKit
import FSCalendar
import DropDown

protocol DateRangePickerDelegate {
    func selectedDateRange(dateRange:[Date], startTime : String, endTime : String)
    func cancelDateRangePickerClicked()
}

class DateRangePickerVC: UIViewController, FSCalendarDelegate, FSCalendarDelegateAppearance {
    // MARK: - Global Variable
    var firstDate: Date?
    var datesRange: [Date]?
    
    var dateRangeDelegate : DateRangePickerDelegate?
    var arrDatesToSelect : [Date]!
    var arrPickupSettingList : [PickupSettingModel] = [PickupSettingModel]()
    var timer : Timer?
    var timerCount : Int = 0

    // MARK: - IB-Outlets
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var calendar: CSFFSCalendar!
    @IBOutlet weak var btnMonthName: UIButton!
    @IBOutlet weak var btnYearName: UIButton!

    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    // MARK: - ViewLifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureControls()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        let nextNDaysDates : [String] = self.arrPickupSettingList.map { $0.date }
        self.arrDatesToSelect = nextNDaysDates.map { destinationFormatter().date(from: $0)! }

        calendar.delegate = self
        
        calendar.allowsMultipleSelection = false
        calendar.swipeToChooseGesture.isEnabled = false // It will work on long press of date in calendar view
        calendar.scrollEnabled = false
//        calendar.placeholderType = .none
        calendar.today = nil

        let weekDayLabel = calendar.calendarWeekdayView.weekdayLabels
        for weekDay in weekDayLabel { weekDay.textColor = Colors.theme_label_black_color }
        
        viewMain.layer.cornerRadius = 10
        viewMain.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        viewMain.layer.masksToBounds = true

        selectPreSelectedDates()
        self.manageNextPrev()
        setCurrentMonthAndYearAtHeader()
        calendar.backgroundColor = Colors.theme_bgColor//UIColor.white
        setTimeoutTimer()
    }
    
    // MARK: - Timer Methods
    func setTimeoutTimer(){
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DateRangePickerVC.timerEventCalled), userInfo: nil, repeats: true)
    }
    
    @objc func timerEventCalled(){
        if timerCount >= 30 {
            self.stopTimer()
            self.closeDatePicker()
        }
//        print("Timer Picker timerCount is \(timerCount)")
        timerCount += 1
    }
    
    func stopTimer(){
        timer?.invalidate()
        timerCount = 0
    }
    
    // MARK: - Dates Methods
    func selectPreSelectedDates(){
        if datesRange != nil {
            if datesRange!.count > 0 {
                for d in datesRange! {
                    self.calendar.select(d)
                }
            }
            firstDate = datesRange?.first
//            print("selectPreSelectedDates \(String(describing: firstDate))")
        }
    }

    func setCurrentMonthAndYearAtHeader(){
        let currentPageDate = calendar.currentPage
        let month = Calendar.current.component(.month, from: currentPageDate)
        let year = Calendar.current.component(.year, from: currentPageDate)
        
        let monthName = DateFormatter().monthSymbols[month - 1]
        btnMonthName.setTitle(monthName, for: .normal)
        btnYearName.setTitle(String(format: "%d", year), for: .normal)
    }
    
    func submitDates(){
        if firstDate != nil {
            datesRange = [firstDate!]
        }
        if datesRange != nil {
            if datesRange!.count > 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    
                    self.dismiss(animated: true) { [self] in
                        
                        let selectedDate = getDateToExpectedFormate(dateVal: datesRange!.first!, formate: "yyyy-MM-dd")
//                        print("selected date is \(selectedDate)")
                        let objPickupSettingModel : PickupSettingModel = self.arrPickupSettingList.filter { $0.date == selectedDate }[0]
                        let startTime : String = objPickupSettingModel.startTime
                        let endTime : String = objPickupSettingModel.endTime
                        
//                        print("start time is \(startTime)")
//                        print("endTime time is \(endTime)")
                        
                        self.stopTimer()
                        dateRangeDelegate?.selectedDateRange(dateRange: datesRange!, startTime: startTime, endTime: endTime)
                    }
                }
            }else {
                appDelegate.showToast(message: "Please select date", bottomValue: getSafeAreaValue())
            }
        }else {
            appDelegate.showToast(message: "Please select date", bottomValue: getSafeAreaValue())
        }
    }
    
    func getDateToExpectedFormate(dateVal : Date, formate : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        let dateString = dateFormatter.string(from: dateVal)
        return dateString
    }
    
    // MARK: - FS Calendar delegate methods
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
        
        if self.arrDatesToSelect.contains(date.removeTimeStamp!) {
            firstDate = date
            
            //Set the month and year name when user select first date and update the calendar also
            if firstDate != nil {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [self] in
                    self.calendar.currentPage = firstDate!
                    self.calendar.reloadData()
                    self.calendar.reloadInputViews()
                    setCurrentMonthAndYearAtHeader()
                    manageNextPrev()
                }
            }
        }else{
            print("if any thing is selected")
        }
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        if !self.arrDatesToSelect.contains(date.removeTimeStamp!) //|| components == 1 || components == 7
        {
            return false
        }
        return true
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        
        if self.arrDatesToSelect.contains(date.removeTimeStamp!) {
            return Colors.theme_label_black_color//UIColor.black
        }else {
            return Colors.theme_calendar_dates_disable_color//UIColor.lightGray
        }
    }
    
    // MARK: - DateRange
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    
    func closeDatePicker(){
        self.dismiss(animated: true) {
            self.dateRangeDelegate?.cancelDateRangePickerClicked()
        }
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnNextMonthClicked(_ sender: UIButton) {
        print("btnNextMonthClicked")
        calendar.setCurrentPage(getNextMonth(date: calendar.currentPage), animated: true)
        setCurrentMonthAndYearAtHeader()
        self.manageNextPrev()
    }
    
    @IBAction func btnPreMonthClicked(_ sender: UIButton) {
        print("btnPreMonthClicked")
        calendar.setCurrentPage(getPreviousMonth(date: calendar.currentPage), animated: true)
        setCurrentMonthAndYearAtHeader()
        self.manageNextPrev()
    }
    
    func manageNextPrev(){
        self.btnNext.isHidden = true
        self.btnPrevious.isHidden = true

        var isShowNext : Bool = false
        var isShowPrev : Bool = false

//        print("self.arrDatesToSelect is \(String(describing: self.arrDatesToSelect))")
//        print("calendar.currentPage is \(calendar.currentPage)")
        
        for newDates in self.arrDatesToSelect {
//            print("newDates is \(newDates)")
            if newDates.isDateInFutureMonth(newDates, calendar.currentPage) {
                isShowNext = true
                break
            }

            if newDates.isDateInPastMonth(newDates, calendar.currentPage) {
                isShowPrev = true
            }
        }

        if isShowNext {
            self.btnNext.isHidden = false
        }

        if isShowPrev {
            self.btnPrevious.isHidden = false
        }
    }
    
    @IBAction func btnClosePopupClicked(_ sender: UIButton) {
        print("btnClosePopupClicked")
        self.closeDatePicker()
    }
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        print("btnClosePopupClicked")
        self.submitDates()
    }
    
    // MARK: - Get next month
    func getNextMonth(date:Date) -> Date {
        return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
    }
    
    // MARK: - Get Previous month
    func getPreviousMonth(date:Date) -> Date {
        return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
    }
}

extension Date {
    var removeTimeStamp : Date? {
        guard let date = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month, .day], from: self)) else {
            return nil
        }
        return date
    }
}
