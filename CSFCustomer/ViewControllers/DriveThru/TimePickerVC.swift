//
//  TimePickerVC.swift
//  CSFCustomer
//
//  Created by iMac on 16/08/22.
//

import UIKit
protocol TimePickerDelegate {
    func selectedTime(time:String, timeIn24Hour : String ,dateTime : Date)
    func cancelTimePickerClicked()
}

class TimePickerVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewMain: UIView!

    // MARK: - Global Variables
    var startTime : String!
    var endTime : String!
    var selectedDateForDriveThruReq : Date!
    var selectedDateToDisplay : String = ""
    
    var timePickerDelegate : TimePickerDelegate?
    var selectedTime : String = ""
    var selectedTimeIn24Hour : String = ""
    var selectedDateTime : Date = Date()
    var timer : Timer?
    var timerCount : Int = 0
    
    lazy var targetFormatter : DateFormatter =
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        return formatter
    }()
    
    lazy var target24HourFormatter : DateFormatter =
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter
    }()

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.selectedTime = ""
        timer?.invalidate()
        timerCount = 0
    }
    
    // MARK: - Custom Methods
    func configureView(){
        viewMain.layer.cornerRadius = 10
        viewMain.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        viewMain.layer.masksToBounds = true
        self.configureTimerPicker()
        self.setTimeoutTimer()
    }
    
    // MARK: - Timer Methods
    func setTimeoutTimer(){
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(TimePickerVC.timerEventCalled), userInfo: nil, repeats: true)
    }
    
    @objc func timerEventCalled(){
        if timerCount >= 30 {
            timer?.invalidate()
            timerCount = 0
            self.dismiss(animated: true) {
                self.timePickerDelegate?.cancelTimePickerClicked()
            }
        }
        print("Timer Picker timerCount is \(timerCount)")
        timerCount += 1
    }
    
    // MARK: - configureTimerPicker
    func configureTimerPicker(){
        //https://stackoverflow.com/questions/49520781/restricting-enabled-time-to-a-specific-time-span-in-uidatepicker-swift-4
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .time
        datePicker.minuteInterval = 1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "HH:mm"
        
        print("selectedDateForDriveThruReq is \(String(describing: selectedDateForDriveThruReq))")
        
        if selectedDateForDriveThruReq.isToday {
            let apiDate : String = getDateToExpectedFormate(dateVal: self.selectedDateForDriveThruReq, formate: "yyyy-MM-dd")
            let startTimeOfAPI : String = self.startTime
            print("startTimeOfAPI \(startTimeOfAPI)")
            let startDateTimeOfAPI : Date  = self.getDateOfAPI(startTime: "\(apiDate) \(startTimeOfAPI)")
            print("startDateTimeOfAPI is \(String(describing: startDateTimeOfAPI))")
            print("current Date is \(String(describing: Date()))")
            
            //If current date is today and if current time > start Time of api then only we will add extra 15 min other wise we will consider current date's api start time as as start time for time picker
            if Date() > startDateTimeOfAPI {
                print("Date() > startDateTimeOfAPI")
                print("Today")
                let add15MinToCurrentDateTime : Date = Date().adding(minutes: appDelegate.totalMinutesForTimer)
                self.startTime = getDateToExpectedFormate(dateVal: add15MinToCurrentDateTime, formate: "HH:mm")
                print("=== Now start time is \(String(describing: self.startTime))")
                //                print("current date is \(Date())")
                //                print("*** Now start time is \(String(describing: self.startTime))")
            }else{
                print("Date() < startDateTimeOfAPI")
            }
        }else {
            print("Other day")
        }
        
        //Setting start time, max and min time of picker
        let currentDate = dateFormatter.date(from: self.startTime)
        datePicker.date = currentDate!
        
        let min = dateFormatter.date(from: self.startTime)  //8 AM  //createing min time
        let max = dateFormatter.date(from: self.endTime)   //9 PM  //creating max time
        datePicker.minimumDate = min  //setting min time to picker
        datePicker.maximumDate = max
        
        datePicker.addTarget(self, action: #selector(dateChange(datePicker:)), for: UIControl.Event.valueChanged)
        datePicker.frame.size = CGSize(width: self.viewPicker.frame.size.width, height: self.viewPicker.frame.size.height)
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        
        self.selectedDateToDisplay = getDateToExpectedFormate(dateVal: selectedDateForDriveThruReq, formate: "dd-MMM-yyyy")
        
        let strTime : String = targetFormatter.string(from: currentDate!)
        self.selectedTime = strTime
        self.selectedDateTime = datePicker.date
        print("selected time is \(strTime)")
        
        let strTimeIn24Hour : String = target24HourFormatter.string(from: currentDate!)
        self.selectedTimeIn24Hour = strTimeIn24Hour
        print("selected time in 24 Hour is \(strTimeIn24Hour)")
        
        self.lblTime.text = "\(selectedDateToDisplay)" + "  " + "\(strTime)"
        
        self.viewPicker.addSubview(datePicker)
    }
    
    func getDateOfAPI(startTime : String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "yyyy-MM-dd HH:mm"
        return dateFormatter.date(from: startTime)!
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnClosePopupClicked(_ sender: UIButton) {
        print("btnClosePopupClicked")
        self.dismiss(animated: true) {
            self.timePickerDelegate?.cancelTimePickerClicked()
        }
    }
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        print("btnClosePopupClicked")
        timePickerDelegate?.selectedTime(time: self.selectedTime, timeIn24Hour: self.selectedTimeIn24Hour, dateTime: self.selectedDateTime)
        self.dismiss(animated: true) {
        }
    }
    
    // MARK: - dateChange
    @objc func dateChange(datePicker: UIDatePicker)
    {

        let strTime : String = targetFormatter.string(from: datePicker.date)
        print("selected time is \(strTime)")
        self.lblTime.text = "\(selectedDateToDisplay)" + "  " + "\(strTime)"
        self.selectedTime = strTime
        self.selectedDateTime = datePicker.date
        
        let strTimeIn24Hour : String = target24HourFormatter.string(from: datePicker.date)
        self.selectedTimeIn24Hour = strTimeIn24Hour
        print("selected time in 24 Hour is \(strTimeIn24Hour)")
    }

}
extension Date {

    var isToday: Bool {
        Calendar.current.isDateInToday(self)
    }
    
    func isDateInFutureMonth(_ date: Date, _ date2 : Date,in calendar: Calendar = .current) -> Bool {
        guard let nextMonth = calendar.date(byAdding: DateComponents(month: 1), to: date2)
        else {
            return false
        }
        return (calendar.isDate(date, equalTo: nextMonth, toGranularity: .month) || date > nextMonth)
    }
    
    func isDateInPastMonth(_ date: Date, _ date2 : Date, in calendar: Calendar = .current) -> Bool {
        guard let prevMonth = calendar.date(byAdding: DateComponents(month: -1), to: date2) else {
            return false
        }
        return (calendar.isDate(date, equalTo: prevMonth, toGranularity: .month) || date < prevMonth)
    }
}
