//
//  DriveThruVC.swift
//  CSFCustomer
//
//  Created by iMac on 21/06/22.
//

import UIKit
import SwiftyJSON
class DriveThruVC: UIViewController,UITextFieldDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var tblDriveThruList : UITableView!
    @IBOutlet weak var viewTotalRecord : TotalRecordView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var cnstTotalRecordsTitleHeight : NSLayoutConstraint!

    // MARK: - Global Variables
    var refreshControl = UIRefreshControl()
    var pageNo : Int = 1
    var totalPages : Int = 0
    var strSearch : String = ""
    var isShowLoader : Bool = true
    var isLoadingList : Bool = false
    var objDriveThruRequest = DriveThruRequestList()
    var arrDriveThruRequestList : [DriveThruRequestList] = [DriveThruRequestList]()
    var arrPickupSettingList : [PickupSettingModel] = [PickupSettingModel]()

    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0
    var pickupWaitTime : Int = 15
    var cancellationReasonArr : [String]!

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        cnstTotalRecordsTitleHeight.constant = 0
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblDriveThruList.addSubview(refreshControl)
        
        tblDriveThruList.delegate = self
        tblDriveThruList.dataSource = self
        tblDriveThruList.estimatedRowHeight = 83.0
        tblDriveThruList.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tblDriveThruList.frame.size.width, height: .leastNormalMagnitude))
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshDriveThruRequestList(_:)), name: Notification.Name("refreshDriveThruRequestList"), object: nil)

        getDriveThruList(isShowLoader: true)
     }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Custom methods
    @objc func refreshDriveThruRequestList(_ notification: NSNotification)
    {
        self.pageNo = 1
        getDriveThruList(isShowLoader: true)
    }
    
    @objc func refresh()
    {
        self.pageNo = 1
        getDriveThruList(isShowLoader: false)
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHistoryClicked(_ sender: UIButton) {
        print("btnHistoryClicked")
        if let vc = appDelegate.getViewController("DriveThruHistoryVC", onStoryboard: "DriveThru") as? DriveThruHistoryVC{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnAddDriveThruRequestClicked(_ sender: UIButton) {
        print("btnAddDriveThruRequestClicked")
        if self.arrPickupSettingList.count > 0 {
            if let vc = appDelegate.getViewController("AddDriveThruRequestVC", onStoryboard: "DriveThru") as? AddDriveThruRequestVC{
                vc.arrPickupSettingList = self.arrPickupSettingList
                vc.arrPickupSettingListMain = self.arrPickupSettingList
                vc.pickupWaitTime = self.pickupWaitTime
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            appDelegate.showToast(message: Messsages.msg_pickup_dates_not_available, bottomValue: getSafeAreaValue())
        }
    }
}

extension DriveThruVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DriveThruCell", for: indexPath) as? DriveThruCell {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(driveThruDetailTapped(sender:)))
            cell.stackViewDriveThru.tag = indexPath.row
            cell.stackViewDriveThru.isUserInteractionEnabled = true
            cell.stackViewDriveThru.addGestureRecognizer(tapGesture)
            
            cell.btnCellBack.tag = indexPath.row
            cell.btnCellBack.addTarget(self, action: #selector(self.btnCellBackClicked(_:)), for: .touchUpInside)

            if self.arrDriveThruRequestList.count > 0 {
                let objRequest : DriveThruRequestList = self.arrDriveThruRequestList[indexPath.row]
                cell.lblRequestId.text = objRequest.requestId ?? ""
                cell.lblPackages.text = "Invoice(s) : \(objRequest.count ?? 0)"
                cell.lblDate.text = objRequest.dateString ?? ""
                cell.lblStatus.text = objRequest.status ?? ""

                if (objRequest.statusId == DriveThruRequestStatusId.CustomerConfirmation.rawValue || objRequest.statusId == DriveThruRequestStatusId.Cancelled.rawValue) {
                    cell.lblStatus.textColor = Colors.theme_orange_color
                    cell.viewStatus.backgroundColor = Colors.theme_orange_color.withAlphaComponent(0.2)
                }else{
                    cell.lblStatus.textColor = Colors.theme_green_color
                    cell.viewStatus.backgroundColor = Colors.theme_green_color.withAlphaComponent(0.2)
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDriveThruRequestList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func driveThruDetailTapped(sender:UITapGestureRecognizer) //new change
    {
        let tag = sender.view!.tag
        self.redirectToRequestDetailVC(index: tag)
        print("tag clicked \(tag)")

    }
    
    // MARK: - Custom Methods
    @objc func btnCellBackClicked(_ sender:UIButton)
    {
        self.redirectToRequestDetailVC(index: sender.tag)
    }
    
    func redirectToRequestDetailVC(index : Int){
        let tag = index
        if let vc = appDelegate.getViewController("RequestDetailVC", onStoryboard: "DriveThru") as? RequestDetailVC{
            vc.isFromHistoryPage = false
            if arrDriveThruRequestList.count > 0{
                let objRequest : DriveThruRequestList = self.arrDriveThruRequestList[tag]
                vc.requestId = objRequest.id ?? ""
                vc.arrPickupSettingList = self.arrPickupSettingList
                vc.arrPickupSettingListMain = self.arrPickupSettingList
                vc.pickupWaitTime = self.pickupWaitTime
                vc.cancellationReasonArr = self.cancellationReasonArr
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isLoadingList &&
            indexPath.row == arrDriveThruRequestList.count - 1 &&
            pageNo < self.totalPages {
            isLoadingList = true
            pageNo += 1
            getDriveThruList(isShowLoader: pageNo > 2)
        }
    }
}

extension DriveThruVC {
    func getDriveThruList(isShowLoader : Bool)
    {
        let objUserInfo = getUserInfo()
        
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["page_id"] = pageNo
        params["search_text"] = strSearch
        params["type"] = DriveThruRequestListType.RequestList.rawValue
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driveThruList, showLoader: isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            if status == true
            {
                self.isLoadingList = false
                if self.pageNo == 1
                {
                    self.arrDriveThruRequestList.removeAll()
                }
                
                let objDriveThruRequestList = DriveThruRequestData.init(fromJson: JSON(resultDict))
                self.cancellationReasonArr = objDriveThruRequestList.cancellationReasonArr
                
//                print("cancellationReasonArr is \(self.cancellationReasonArr))")
//                print(self.cancellationReasonArr)
                if self.arrDriveThruRequestList.count > 0
                {
                    self.arrDriveThruRequestList.append(contentsOf: objDriveThruRequestList.list)
                }
                else
                {
                    self.arrDriveThruRequestList = objDriveThruRequestList.list
                }
                
                self.arrPickupSettingList = objDriveThruRequestList.pickupSettings
                
                if let waitTime : String = objDriveThruRequestList.pickupWaitTime {
                    self.pickupWaitTime = Int(waitTime)!
                }
                
                if let totalCount = objDriveThruRequestList.totalCount
                {
                    self.totalRecordsCount = totalCount
                }
                
                if let totalPage = objDriveThruRequestList.totalPage
                {
                    self.totalPages = totalPage
                }
                DispatchQueue.main.async {
                    self.reloadData()
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    func reloadData(){
        self.tblDriveThruList.isHidden = false

        if self.arrDriveThruRequestList.count > 0
        {
            self.lblNoRecordFound.isHidden = true
        }
        else
        {
            self.lblNoRecordFound.isHidden = false
        }
        self.tblDriveThruList.reloadData()
        
        if self.totalRecordsCount > 0 {
            self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
        }else {
            self.cnstTotalRecordsTitleHeight.constant = 0
        }
        
        self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: 0, totalRecords: totalRecordsCount)
        self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: 0, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_REQUESTS)
    }
}
