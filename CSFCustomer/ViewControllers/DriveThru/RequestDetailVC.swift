//
//  RequestDetailVC.swift
//  CSFCustomer
//
//  Created by iMac on 22/06/22.
//

import UIKit
import SwiftyJSON
import Kronos
class RequestDetailVC: UIViewController,CancelDriveThruRequestPopupVCVCDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblRequestId: TapAndCopyLabel!
    @IBOutlet weak var lblRequestStatus : UILabel!
    @IBOutlet weak var viewRequestStatus : UIView!
    @IBOutlet weak var lblRequestDate : UILabel!
    @IBOutlet weak var lblPickupDateAndTime: UILabel!
    @IBOutlet weak var lblTimer : UILabel!
    @IBOutlet weak var lblRequestReson : UILabel!
    @IBOutlet weak var stackViewTimer : UIStackView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var tblRequestedPackages : UITableView!
    @IBOutlet weak var viewHeader : UIView!
    @IBOutlet weak var stackViewQRCodeDownload : UIStackView!
    @IBOutlet weak var viewActionButtons : UIView!
    @IBOutlet weak var stackViewActionButtons : UIStackView!
    @IBOutlet weak var stackViewApproveRejectButtons : UIStackView!
    @IBOutlet weak var stackviewReason : UIStackView!
    @IBOutlet weak var lblPackages: UILabel!
    @IBOutlet weak var cnstTimerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cnstActionButtonsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cnstTblHeight: NSLayoutConstraint!
    @IBOutlet weak var scrlViewDriveThruDetail: UIScrollView!
    @IBOutlet weak var stackViewTracker: UIStackView!
    @IBOutlet weak var stackViewTrackerDescription: UIStackView!
    
    @IBOutlet weak var lblTrackerStage1Active: UILabel!
    @IBOutlet weak var lblTrackerStage2Active: UILabel!
    
    @IBOutlet weak var lblTrackerStage1DeActive: UILabel!
    @IBOutlet weak var lblTrackerStage2DeActive: UILabel!
    
    @IBOutlet weak var imgTrackerStage1: UIImageView!
    @IBOutlet weak var imgTrackerStage2: UIImageView!
    @IBOutlet weak var imgTrackerStage3: UIImageView!
    @IBOutlet weak var lblTrackerDetail: UILabel!
    
    @IBOutlet weak var btnStage1: UIButton!
    @IBOutlet weak var btnStage2: UIButton!
    @IBOutlet weak var btnStage3: UIButton!
    @IBOutlet weak var btnOSTicket: UIButton!
    @IBOutlet weak var viewLogs: UIView!
    @IBOutlet weak var lblTotalPackages: UILabel!
    @IBOutlet weak var viewPullToRefresh: UIView!
    @IBOutlet weak var viewReschedule: UIView!
    
    @IBOutlet weak var btnApprove: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    @IBOutlet weak var viewCustomerName: UIStackView!
    @IBOutlet weak var lblCustomerName: UILabel!
    
    var strStage1Msg : String = ""
    var strStage2Msg : String = ""
    var strStage3Msg : String = ""
    
    // MARK: - Global Variables
    var refreshControl = UIRefreshControl()
    var isFromHistoryPage : Bool = false
    var isShowLoader : Bool = true
    var arrPackages : [DriveThruPackageDetail] =  [DriveThruPackageDetail]()
    var requestDetailObj : DriveThruRequestDetail = DriveThruRequestDetail()
    
    var requestId : String = ""
    var timerCount : Int = 0
    var timer : Timer?
    var currentTimeStamp : TimeInterval!
    
    var objDriveThruViewRequestData : DriveThruViewRequestData!
    var objLogMainModel : LogMainModel!
    var arrDriveThruLogs : [DriveThruLogsModel] =  [DriveThruLogsModel]()
    var option : Int = 1
    
    var selectedDate : String = ""
    var selectedPickerDate : Date!
    var selectedTime : String = ""
    var selectedTimePickerDate : Date!
    var selectedPickupDate : String = ""
    var selectedPickupTime : String = ""
    var arrPickupSettingListMain : [PickupSettingModel] = [PickupSettingModel]()
    var arrPickupSettingList : [PickupSettingModel] = [PickupSettingModel]()
    var pickupWaitTime : Int = 15
    var cancellationReasonArr : [String]!
    var isShowReason : Bool = false
    var isNeedToRemoveCurrentDate : Bool = false

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeDown.direction = .down
        self.scrlViewDriveThruDetail.addGestureRecognizer(swipeDown)
        
        self.lblTrackerStage1Active.isHidden = false
        self.lblTrackerStage2Active.isHidden = false
        
        self.lblTrackerStage1DeActive.isHidden = true
        self.lblTrackerStage2DeActive.isHidden = true
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        scrlViewDriveThruDetail.addSubview(refreshControl)
        
        tblRequestedPackages.delegate = self
        tblRequestedPackages.dataSource = self
        tblRequestedPackages.tableHeaderView = viewHeader
        
        scrlViewDriveThruDetail.isScrollEnabled = true
        tblRequestedPackages.isScrollEnabled = false
        
        self.setUpInitialViews()
        self.getDriveThruRequestDetail(isShowLoader: true)
        self.syncClockTime()
    }
    
    // MARK: - refresh
    @objc func refresh()
    {
        self.getDriveThruRequestDetail(isShowLoader: false)
    }
    
    // MARK: - syncClockTime
    func syncClockTime(){
        appDelegate.showLoader()
        Clock.sync(completion:  { date, offset in
            appDelegate.hideLoader()
        })
    }
    
    // MARK: - respondToSwipeGesture
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .down:
                self.viewPullToRefresh.isHidden = false
                self.getDriveThruRequestDetail(isShowLoader: false)
                break
            default:
                break
            }
        }
    }

    // MARK: - getCountDownTimer
    func getCountDownTimer(strDate: String) -> Int64 {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let dtStartDate = dateFormatter.date(from: strDate) else { return 0 }

        let dtCurrentDateTime = Date()

        let calendar = Calendar.current
        let dtEndDate = calendar.date(byAdding: .minute, value: 15, to: dtStartDate)!

        let diff = Int64(dtEndDate.timeIntervalSince(dtCurrentDateTime))

        return diff
    }
    
    // MARK: - manageTimer
    func manageTimer(){
        currentTimeStamp = Date().timeIntervalSince1970
        if !isFromHistoryPage {
            timer?.invalidate()
            
            if (self.requestDetailObj.statusId == DriveThruRequestStatusId.Approved.rawValue) {
                
                if let processedTime : String = self.requestDetailObj.processedTime {
                    
                    if !processedTime.isEmpty {
                        
                        let diff = getCountDownTimer(strDate: processedTime)
                        print("diff is \(diff)")
                        if (diff > 0) {
                            //   startCountDown(diff)
                            stackViewTimer.isHidden = false
                            self.cnstTimerViewHeight.constant = 50
                            showTimerValue()
                            
                            timerCount = Int(diff)
                            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(RequestDetailVC.timerEventCalled), userInfo: nil, repeats: true)
                        }else{
                            // hide
                            stackViewTimer.isHidden = true
                            self.cnstTimerViewHeight.constant = 0
                        }
                    }
                }
            }else {
                stackViewTimer.isHidden = true
                self.cnstTimerViewHeight.constant = 0
            }
        }
    }
    
    // MARK: - timerEventCalled
    @objc func timerEventCalled()
    {
        if(timerCount > 0){
            showTimerValue()
            timerCount -= 1
        }else {
            timer?.invalidate()
            stackViewTimer.isHidden = true
            self.cnstTimerViewHeight.constant = 0
        }
    }
    
    // MARK: - showTimerValue
    func showTimerValue(){
        
        let minutes = String(format: "%02d",((timerCount % 3600) / 60), 0)
        let seconds = String(format: "%02d",((timerCount % 3600) % 60), 0)
        
        self.lblTimer.text = minutes + ":" + seconds
    }
    
    // MARK: - getTimerValue
    func getTimerValue(remainingSeconds : Int) -> Int{
        let totalSeconds : Int = appDelegate.totalSecondsForTimer
        let counter : Int = totalSeconds - remainingSeconds
        print("Now Timer will work for \(totalSeconds) - \(remainingSeconds) =  \(counter) seconds")
        return counter
    }
    
    // MARK: - getSecondsDifferenceFromTwoDates
    func getSecondsDifferenceFromTwoDates(startTimeStamp: Double, endTimeStamp: Double) -> Int
    {
        
        let diff = Int(endTimeStamp - startTimeStamp)
        
        let hours = diff / 3600
        let seconds = (diff - hours * 3600)
        return seconds
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPackageLogsClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        self.getDriveThruLogs()
    }
    
    @IBAction func btnApproveDriveThruRequestClicked()
    {
        showConfirmationPopup(title: "", msg: Messsages.msg_approve_request) { [self] status in
            if status {
                print("Approve the request")
                approveRejectDriverThruRequest(type: "4")
            }
        }
    }
    
    @IBAction func btnRejectDriveThruRequestClicked(){
        showConfirmationPopup(title: "", msg: Messsages.msg_reject_request) { [self] status in
            if status {
                print("Reject the request")
                approveRejectDriverThruRequest(type: "3")
            }
        }
    }
    
    @IBAction func btnCancelDriveThruRequestClicked()
    {
        if let cancelDriveThruRequestVC = appDelegate.getViewController("CancelDriveThruRequestPopupVC", onStoryboard: "DriveThru") as? CancelDriveThruRequestPopupVC {
            cancelDriveThruRequestVC.cancelDriveThruRequestVCDelegate = self
            cancelDriveThruRequestVC.cancellationReasonArr = self.cancellationReasonArr
            cancelDriveThruRequestVC.modalPresentationStyle = .overFullScreen
            self.present(cancelDriveThruRequestVC,animated: true, completion: nil)
        }
    }
    
    @IBAction func btnQRCodeDownloadClicked(_ sender:UIButton)
    {
        print("btnQRCodeDownloadClicked")
        if let scanQRCodeVC = appDelegate.getViewController("ScanQRCodeVC", onStoryboard:"DriveThru" ) as? ScanQRCodeVC {
            scanQRCodeVC.objRequestDetail = self.requestDetailObj
            scanQRCodeVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
            scanQRCodeVC.modalPresentationStyle = .overFullScreen
            self.present(scanQRCodeVC,animated: true, completion: nil)
        }
    }
    
    @IBAction func btnOpenOSTicketClicked(_ sender:UIButton)
    {
        print("btnOpenOSTicketClicked")
        if let openTicketVC = appDelegate.getViewController("OpenATicketVC", onStoryboard: "Home") as? OpenATicketVC {
            self.navigationController?.pushViewController(openTicketVC, animated: true)
        }
    }
    
    func getReasonToCancelRequest(reason: String) {
        print("Reason to cancel request is \(reason)")
        self.driveThruCancelRequest(reason: reason)
    }
    
    @IBAction func btnRescheduleClicked(_ sender: UIButton) {
        self.checkSystemTimeChangedOrNot()
    }
}

extension RequestDetailVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RequestedPackagesCell", for: indexPath) as? RequestedPackagesCell {
            if self.arrPackages.count > 0 {
                let objPackage : DriveThruPackageDetail = self.arrPackages[indexPath.row]
                cell.lblHawbNumber.text = objPackage.hawbNumber ?? ""
                cell.lblStatus.text = objPackage.status ?? ""
                cell.lblName.text = objPackage.itemDescription ?? ""
                
                if !objPackage.notes.isEmpty {
                    cell.lblNote.text = objPackage.notes ?? ""
                    cell.viewNote.isHidden = false
                }else{
                    cell.lblNote.text = ""
                    cell.viewNote.isHidden = true
                }
                
                if (objPackage.statusId == DriveThruPackageStatusId.Query.rawValue || objPackage.statusId == DriveThruPackageStatusId.Cancelled.rawValue) {
                    cell.lblStatus.textColor = Colors.theme_orange_color
                    cell.viewStatus.backgroundColor = Colors.theme_orange_color.withAlphaComponent(0.2)
                }else{
                    cell.lblStatus.textColor = Colors.theme_green_color
                    cell.viewStatus.backgroundColor = Colors.theme_green_color.withAlphaComponent(0.2)
                }
            }
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPackages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension RequestDetailVC {
    
    // MARK: - getDriveThruRequestDetail
    func getDriveThruRequestDetail(isShowLoader : Bool)
    {
        let objUserInfo = getUserInfo()
        
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["request_id"] = self.requestId
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driveThruDetails, showLoader: isShowLoader) { [self] (resultDict, status, message) in
            self.viewPullToRefresh.isHidden = true
            self.refreshControl.endRefreshing()
            if status == true
            {
                objDriveThruViewRequestData = DriveThruViewRequestData.init(fromJson: JSON(resultDict))
                self.requestDetailObj = objDriveThruViewRequestData.requestDetails
                self.requestId = self.requestDetailObj.id
                self.arrPackages = objDriveThruViewRequestData.packageDetails
                self.pickupWaitTime = Int(objDriveThruViewRequestData.pickupWaitTime ?? "0")!
                self.cancellationReasonArr = objDriveThruViewRequestData.cancellationReasonArr
                self.arrPickupSettingList = objDriveThruViewRequestData.pickupSettings
                self.arrPickupSettingListMain = objDriveThruViewRequestData.pickupSettings
                
                DispatchQueue.main.async {
                    if (self.requestDetailObj.statusId == DriveThruRequestStatusId.Cancelled.rawValue || self.requestDetailObj.statusId == DriveThruRequestStatusId.Delivered.rawValue) {
                        self.isFromHistoryPage = true
                    }else{
                        self.isFromHistoryPage = false
                    }
                    self.setUpData()
                    self.reloadData()
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - setUpInitialViews
    func setUpInitialViews(){
        self.btnApprove.setTitle("Continue", for: .normal)
        self.btnReject.setTitle("Cancel", for: .normal)
        
        self.scrlViewDriveThruDetail.isHidden = true
        self.stackViewTimer.isHidden = true
        self.cnstTimerViewHeight.constant = 0
        self.viewActionButtons.isHidden = true
        self.stackViewActionButtons.isHidden = true
        self.stackViewApproveRejectButtons.isHidden = true
        self.cnstActionButtonsViewHeight.constant = 0
        self.stackViewQRCodeDownload.isHidden = true
        self.viewReschedule.isHidden = true
    }
    
    // MARK: - setUpData
    func setUpData()
    {
        if !isFromHistoryPage {
            self.btnOSTicket.isHidden = false
        }else {
            self.btnOSTicket.isHidden = true
        }
        
        self.scrlViewDriveThruDetail.isHidden = false
        self.lblRequestId.text = self.requestDetailObj.requestId ?? ""
        self.lblRequestDate.text = self.requestDetailObj.dateOfRequestString ?? ""
        
        self.manageTimer()
        if self.requestDetailObj.query == "" {
            self.stackviewReason.isHidden = true
        }else {
            self.stackviewReason.isHidden = false
            self.lblRequestReson.text = self.requestDetailObj.query ?? ""
        }
        
        if (self.requestDetailObj.statusId != DriveThruRequestStatusId.CustomerConfirmation.rawValue && self.requestDetailObj.statusId != DriveThruRequestStatusId.Cancelled.rawValue &&
            self.requestDetailObj.statusId != DriveThruRequestStatusId.Delivered.rawValue && !isFromHistoryPage) {
            self.viewActionButtons.isHidden = false
            self.stackViewActionButtons.isHidden = false
            self.stackViewApproveRejectButtons.isHidden = true
            self.cnstActionButtonsViewHeight.constant = 64
        }else {
            self.viewActionButtons.isHidden = true
            self.stackViewActionButtons.isHidden = true
            self.stackViewApproveRejectButtons.isHidden = true
            self.cnstActionButtonsViewHeight.constant = 0
        }
        
        if(self.requestDetailObj.isQuery == 1 && !isFromHistoryPage) {
            //Show Approve Reject buttons
            self.viewActionButtons.isHidden = false
            self.stackViewActionButtons.isHidden = true
            self.stackViewApproveRejectButtons.isHidden = false
            self.cnstActionButtonsViewHeight.constant = 64
        }else{
            //Show Hide Cancel Request button and Approve Reject button
            if (self.requestDetailObj.statusId != DriveThruRequestStatusId.CustomerConfirmation.rawValue && self.requestDetailObj.statusId != DriveThruRequestStatusId.Cancelled.rawValue &&
                self.requestDetailObj.statusId != DriveThruRequestStatusId.Delivered.rawValue && !isFromHistoryPage) {
                self.viewActionButtons.isHidden = false
                self.stackViewActionButtons.isHidden = false
                self.stackViewApproveRejectButtons.isHidden = true
                self.cnstActionButtonsViewHeight.constant = 64
            }else {
                self.viewActionButtons.isHidden = true
                self.stackViewActionButtons.isHidden = true
                self.stackViewApproveRejectButtons.isHidden = true
                self.cnstActionButtonsViewHeight.constant = 0
            }
        }
        
        if ((self.requestDetailObj.statusId == DriveThruRequestStatusId.Approved.rawValue ||
             self.requestDetailObj.statusId == DriveThruRequestStatusId.Received.rawValue ||
             self.requestDetailObj.statusId == DriveThruRequestStatusId.Verified.rawValue) && !isFromHistoryPage) {
            self.stackViewQRCodeDownload.isHidden = false
        }else {
            self.stackViewQRCodeDownload.isHidden = true
        }
        
        self.lblRequestStatus.text = self.requestDetailObj.status ?? ""
        
        if (self.requestDetailObj.statusId == DriveThruRequestStatusId.CustomerConfirmation.rawValue || self.requestDetailObj.statusId == DriveThruRequestStatusId.Cancelled.rawValue) {
            self.lblRequestStatus.textColor = Colors.theme_orange_color
            self.viewRequestStatus.backgroundColor = Colors.theme_orange_color.withAlphaComponent(0.2)
        }else{
            self.lblRequestStatus.textColor = Colors.theme_green_color
            self.viewRequestStatus.backgroundColor = Colors.theme_green_color.withAlphaComponent(0.2)
        }
        
        self.lblTotalPackages.text = "\(self.arrPackages.count) Invoice(s)"
        
        self.lblPickupDateAndTime.text = "\(self.requestDetailObj.pickupDate ?? "")" + " " + "\(self.requestDetailObj.pickupTime ?? "")"
        
        if !isFromHistoryPage {
            if self.requestDetailObj.statusId == DriveThruRequestStatusId.Pending.rawValue {
                self.viewReschedule.isHidden = false
            }else{
                self.viewReschedule.isHidden = true
            }
        }else{
            self.viewReschedule.isHidden = true
        }
        
        if self.requestDetailObj.isChild == 1 {
            hideClickableForChildUsers()
        }else{
            self.viewCustomerName.isHidden = true
        }
    }
    
    // MARK: - hideClickableForChildUsers
    func hideClickableForChildUsers(){
        self.viewActionButtons.isHidden = true
        self.viewReschedule.isHidden = true
        self.viewCustomerName.isHidden = false
        self.cnstActionButtonsViewHeight.constant = 0
        self.lblCustomerName.text = "\(self.requestDetailObj.memberName ?? "") | \(self.requestDetailObj.boxId ?? "")"
    }
    
    // MARK: - reloadData
    func reloadData(){
        if self.arrPackages.count > 0
        {
            self.tblRequestedPackages.isHidden = false
            self.lblNoRecordFound.isHidden = true
        }
        else
        {
            self.tblRequestedPackages.isHidden = true
            self.lblNoRecordFound.isHidden = false
        }
        
        cnstTblHeight.constant = CGFloat.greatestFiniteMagnitude
        self.tblRequestedPackages.reloadData()
        self.tblRequestedPackages.layoutIfNeeded()
        cnstTblHeight.constant = tblRequestedPackages.contentSize.height
        
        setTrackerUI()
    }
}

extension RequestDetailVC {
    // MARK: - approveRejectDriverThruRequest
    func approveRejectDriverThruRequest(type : String)
    {
        let objUserInfo = getUserInfo()
        
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["request_id"] = self.requestId
        params["type"] = type // 3 - Reject, 4 - Approve
        
        print("approveRejectDriverThruRequest params are \(params)")
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driveThruApproval, showLoader: true) { (resultDict, status, message) in
            if status == true
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true)
                NotificationCenter.default.post(name: Notification.Name("refreshDriveThruRequestList"), object: nil)
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}

extension RequestDetailVC {
    //Tracker Section
    
    // MARK: - btnStage1Clicked
    @IBAction func btnStage1Clicked()
    {
        print("btnStage1Clicked clicked")
        appDelegate.showToast(message: self.strStage1Msg, bottomValue: getSafeAreaValue(), isForSuccess: true)
    }
    
    // MARK: - btnStage2Clicked
    @IBAction func btnStage2Clicked()
    {
        print("btnStage2Clicked clicked")
        appDelegate.showToast(message: self.strStage2Msg, bottomValue: getSafeAreaValue(), isForSuccess: true)
    }
    
    // MARK: - btnStage3Clicked
    @IBAction func btnStage3Clicked()
    {
        print("btnStage3Clicked clicked")
        appDelegate.showToast(message: self.strStage3Msg, bottomValue: getSafeAreaValue(), isForSuccess: true)
    }
    
    // MARK: - hideAllLines
    func hideAllLines(){
        self.lblTrackerStage1Active.isHidden = true
        self.lblTrackerStage2Active.isHidden = true
        self.lblTrackerStage1DeActive.isHidden = true
        self.lblTrackerStage2DeActive.isHidden = true
    }
    
    // MARK: - showStage1ActiveLine
    func showStage1ActiveLine(){
        self.lblTrackerStage1Active.isHidden = false
    }
    
    // MARK: - showStage2ActiveLine
    func showStage2ActiveLine(){
        self.lblTrackerStage2Active.isHidden = false
    }
    
    // MARK: - showStage1DeActiveLine
    func showStage1DeActiveLine(){
        self.lblTrackerStage1DeActive.isHidden = false
    }
    
    // MARK: - showStage2DeActiveLine
    func showStage2DeActiveLine(){
        self.lblTrackerStage2DeActive.isHidden = false
    }
    
    // MARK: - setTrackerUI
    func setTrackerUI(){
        if let statusMsg : String = self.requestDetailObj.statusMsg {
            self.lblTrackerDetail.text = statusMsg
        }
        if let statusId : Int = self.requestDetailObj.statusId {
            
            switch statusId {
            case DriveThruRequestStatusId.Pending.rawValue:
                
                self.imgTrackerStage1.image = UIImage(named: "ic_dt_pending_processed")
                self.imgTrackerStage2.image = UIImage(named: "ic_dt_pkgfind")
                self.imgTrackerStage3.image = UIImage(named: "ic_dt_delivered")
                
                self.hideAllLines()
                self.showStage1DeActiveLine()
                self.showStage2DeActiveLine()
                
                self.btnStage1.isUserInteractionEnabled = true
                self.btnStage2.isUserInteractionEnabled = false
                self.btnStage3.isUserInteractionEnabled = false
                
                self.strStage1Msg = "Your request has been added."
                self.strStage2Msg = "We are working on your request!"
                self.strStage3Msg = ""
                
                break
            case DriveThruRequestStatusId.CustomerConfirmation.rawValue:
                
                self.imgTrackerStage1.image = UIImage(named: "ic_dt_pending_completed")
                self.imgTrackerStage2.image = UIImage(named: "ic_dt_query_processed")
                self.imgTrackerStage3.image = UIImage(named: "ic_dt_delivered")
                
                self.hideAllLines()
                self.showStage1ActiveLine()
                self.showStage2DeActiveLine()
                
                self.btnStage1.isUserInteractionEnabled = true
                self.btnStage2.isUserInteractionEnabled = true
                self.btnStage3.isUserInteractionEnabled = false
                
                self.strStage1Msg = "Your request has been added."
                self.strStage2Msg = "We are awaiting your confirmation. Please view in app or on the website."
                self.strStage3Msg = ""
                break
            case DriveThruRequestStatusId.Approved.rawValue:
                
                self.imgTrackerStage1.image = UIImage(named: "ic_dt_pending_completed")
                self.imgTrackerStage2.image = UIImage(named: "ic_dt_pkgfind_processed")
                self.imgTrackerStage3.image = UIImage(named: "ic_dt_received")
                
                self.hideAllLines()
                self.showStage1ActiveLine()
                self.showStage2DeActiveLine()
                
                self.btnStage1.isUserInteractionEnabled = true
                self.btnStage2.isUserInteractionEnabled = true
                self.btnStage3.isUserInteractionEnabled = false
                
                self.strStage1Msg = "Your request has been added."
                self.strStage2Msg = "Your request has been Approved! We are working on it."
                self.strStage3Msg = ""
                
                break
            case DriveThruRequestStatusId.Received.rawValue:
                
                self.imgTrackerStage1.image = UIImage(named: "ic_dt_pending_completed_ready_pickup")
                self.imgTrackerStage2.image = UIImage(named: "ic_dt_pkgfind_completed")
                self.imgTrackerStage3.image = UIImage(named: "ic_dt_received_processed")
                
                self.hideAllLines()
                self.showStage1ActiveLine()
                self.showStage2ActiveLine()
                
                self.btnStage1.isUserInteractionEnabled = true
                self.btnStage2.isUserInteractionEnabled = true
                self.btnStage3.isUserInteractionEnabled = true
                
                self.strStage1Msg = "Your request has been added."
                self.strStage2Msg = "Your request has been processed by the CSF Team."
                self.strStage3Msg = "Kindly proceed to Drive-Thru window to collect your order. Our friendly staff is waiting to serve you."
                break
                
            case DriveThruRequestStatusId.Verified.rawValue:
                
                self.imgTrackerStage1.image = UIImage(named: "ic_dt_pending_completed")
                self.imgTrackerStage2.image = UIImage(named: "ic_dt_verified_completed")
                self.imgTrackerStage3.image = UIImage(named: "ic_dt_delivered_processed")
                
                self.hideAllLines()
                self.showStage1ActiveLine()
                self.showStage2ActiveLine()
                
                self.btnStage1.isUserInteractionEnabled = true
                self.btnStage2.isUserInteractionEnabled = true
                self.btnStage3.isUserInteractionEnabled = true
                
                self.strStage1Msg = "Your request has been added."
                self.strStage2Msg = "Your Drive-Thru request has been verified!"
                self.strStage3Msg = "Your Order is being prepared!"
                
                break
                
            case DriveThruRequestStatusId.Delivered.rawValue:
                
                self.imgTrackerStage1.image = UIImage(named: "ic_dt_pending_completed")
                self.imgTrackerStage2.image = UIImage(named: "ic_dt_verified_completed")
                self.imgTrackerStage3.image = UIImage(named: "ic_dt_delivered_completed")
                
                self.hideAllLines()
                self.showStage1ActiveLine()
                self.showStage2ActiveLine()
                
                self.btnStage1.isUserInteractionEnabled = true
                self.btnStage2.isUserInteractionEnabled = true
                self.btnStage3.isUserInteractionEnabled = true
                
                self.strStage1Msg = "Your request has been added."
                self.strStage2Msg = "Your Drive-Thru request has been verified!"
                self.strStage3Msg = "Your package has been delivered!"
                
                break
            case DriveThruRequestStatusId.Cancelled.rawValue:
                
                self.imgTrackerStage1.image = UIImage(named: "ic_dt_pending_completed")
                self.imgTrackerStage2.image = UIImage(named: "ic_dt_pkgfind_completed")
                self.imgTrackerStage3.image = UIImage(named: "ic_dt_cancelled_completed")
                
                self.hideAllLines()
                self.showStage1ActiveLine()
                self.showStage2ActiveLine()
                
                self.btnStage1.isUserInteractionEnabled = true
                self.btnStage2.isUserInteractionEnabled = true
                self.btnStage3.isUserInteractionEnabled = true
                
                self.strStage1Msg = "Your request has been added."
                self.strStage2Msg = "Your request has been processed by the CSF Team."
                self.strStage3Msg = "Your request has been cancelled!"
                
                break
            default:
                break
            }
        }
    }
}

extension RequestDetailVC {
    
    // MARK: - driveThruCancelRequest
    func driveThruCancelRequest(reason : String)
    {
        let objUserInfo = getUserInfo()
        
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["request_id"] = self.requestId
        params["reason"] = reason
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driveThruCancel, showLoader: isShowLoader) { [self] (resultDict, status, message) in
            if status == true
            {
                print("Drive thru request cancelled successfully....")
                
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true)
                NotificationCenter.default.post(name: Notification.Name("refreshDriveThruRequestList"), object: nil)
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}

extension RequestDetailVC {
    // MARK: - getDriveThruLogs
    func getDriveThruLogs()
    {
        let objUserInfo = getUserInfo()
        
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["request_id"] = self.requestId
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driveThruLogs, showLoader: isShowLoader) { [self] (resultDict, status, message) in
            if status == true
            {
                
                objLogMainModel = LogMainModel.init(fromJson: JSON(resultDict))
                if objLogMainModel.list != nil {
                    self.arrDriveThruLogs = objLogMainModel.list
                    
                    if let vc = appDelegate.getViewController("PackageLogsVC", onStoryboard:"DriveThru" ) as? PackageLogsVC {
                        vc.arrDriveThruLogs = self.arrDriveThruLogs
                        vc.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
                        vc.modalPresentationStyle = .overFullScreen
                        self.present(vc,animated: true, completion: nil)
                    }
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}

extension RequestDetailVC {
    // MARK: - checkSystemTimeChangedOrNot
    func checkSystemTimeChangedOrNot(){
        
        if Clock.now != nil {
            print("Clock date is \(String(describing: Clock.now))")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let clockDate : String = dateFormatter.string(from: Clock.now!)
            print("Clock date in my formate is \(clockDate)")
            
            let currentDate : String = dateFormatter.string(from: Date())
            print("CurrentDate date in my formate is \(currentDate)")
            
            guard let firstTimeInterval = Clock.now?.timeIntervalSince1970 else {
                print("** Couldn't get first time interval **")
                appDelegate.showToast(message: "Please try again after some time and check your network provided time from device's Date & Time settings.", bottomValue: getSafeAreaValue())
                return
            }
            
            let secondTimeInterval = Date().timeIntervalSince1970
            
            print("first time interval is \(String(describing: firstTimeInterval))")
            print("second time interval is \(String(describing: secondTimeInterval))")
            
            let timeDiff = abs(Int(firstTimeInterval) - Int(secondTimeInterval))
            print("time diff is \(timeDiff)")
            
            if (timeDiff > 1) {
                appDelegate.showToast(message: "Please enable network provided time from device's Date & Time settings.", bottomValue: getSafeAreaValue())
            }else {
                print("Time is proper")
                self.showDatePicker()
            }
        }else{
            syncClockTime()
        }
    }
    
    // MARK: - showDatePicker
    func showDatePicker(){
        if let dateRangePickerVC = appDelegate.getViewController("DateRangePickerVC", onStoryboard: "DriveThru") as? DateRangePickerVC {
            
            if self.arrPickupSettingListMain.count > 0 {
                
                var arrPickupSettingListTemp : [PickupSettingModel] = [PickupSettingModel]()
                
                let isOnDelivery : Int = self.requestDetailObj.onDelivery ?? 0
                
                if isOnDelivery == 1{
                    isNeedToRemoveCurrentDate = true
                }else{
                    isNeedToRemoveCurrentDate = false
                }
                
                if isNeedToRemoveCurrentDate {
                    self.arrPickupSettingList = removeCurrentDateFromDateArr(arrPickupSettingList: self.arrPickupSettingListMain)
                    
                }else {
                    self.arrPickupSettingList = self.arrPickupSettingListMain
                }
                
                arrPickupSettingListTemp = self.arrPickupSettingList
                
                if arrPickupSettingListTemp.count > 0 {
                    dateRangePickerVC.arrPickupSettingList = arrPickupSettingListTemp
                    dateRangePickerVC.dateRangeDelegate = self
                    
                    let dateArrayOfPickupSettings : PickupSettingModel = arrPickupSettingListTemp[0]
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    if let dateOfPickupSettings : String = dateArrayOfPickupSettings.date {
                        
                        var dateToShowSelected : Date = dateFormatter.date(from: dateOfPickupSettings)!
                        print("dateToShowSelected is \(dateToShowSelected)")
                        
                        if dateToShowSelected.isToday {
                            print("Today from Add drive thru request screen")
                            print("end time is \(String(describing: dateArrayOfPickupSettings.endTime))")
                            let startTimeOfCurrentDate : Date = Date().adding(minutes: self.pickupWaitTime)
                            print("after adding 15 min \(startTimeOfCurrentDate)")
                            
                            let calendar = Calendar.current
                            let hour = calendar.component(.hour, from: startTimeOfCurrentDate)
                            let minutes = calendar.component(.minute, from: startTimeOfCurrentDate)
                            let seconds = calendar.component(.second, from: startTimeOfCurrentDate)
                            print("hour min sec \(hour) \(minutes) \(seconds)")
                            
                            let newStartTime : String = "\(hour):\(minutes)"
                            print("newStartTime is  \(newStartTime)")
                            
                            let apiEndTime = "\(dateOfPickupSettings)" + " " + "\(dateArrayOfPickupSettings.endTime ?? "20:00")"
                            let newEndTime = "\(dateOfPickupSettings)" + " " + "\(newStartTime)"
                            print("A is \(apiEndTime)")
                            print("B is \(newEndTime)")
                            
                            let dateFormatter1 = DateFormatter()
                            dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm"
                            
                            if let apiDate : Date = dateFormatter1.date(from: apiEndTime), let newDate : Date = dateFormatter1.date(from: newEndTime) {
                                
                                
                                print("dateA is \(apiDate)")
                                print("dateB is \(newDate)")
                                
                                let apiTimeStamp = apiDate.timeIntervalSince1970
                                let newTimeStamp = newDate.timeIntervalSince1970
                                
                                if newTimeStamp >= apiTimeStamp {
                                    print("discard today date")
                                    if arrPickupSettingListTemp.count > 1 {
                                        arrPickupSettingListTemp.remove(at: 0)
                                        dateRangePickerVC.arrPickupSettingList = arrPickupSettingListTemp
                                        
                                        let dateArrayOfPickupSettings : PickupSettingModel = arrPickupSettingListTemp[0]
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "yyyy-MM-dd"
                                        if let dateOfPickupSettings : String = dateArrayOfPickupSettings.date {
                                            dateToShowSelected = dateFormatter.date(from: dateOfPickupSettings)!
                                        }
                                    }
                                }else {
                                    print("accept today date")
                                }
                            }
                        }
                        
                        dateRangePickerVC.datesRange = [dateToShowSelected]
                        
                        dateRangePickerVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                        dateRangePickerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        dateRangePickerVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
                        
                        if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                            rootVC.present(dateRangePickerVC, animated: true, completion: nil)
                        }
                    }
                }else {
                    appDelegate.showToast(message: Messsages.msg_pickup_dates_not_available, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    
    // MARK: - showTimePicker
    func showTimePicker(startTime : String, endTime : String, selectedDateForDriveThruReq : Date){
        
        if let timerPickerVC = appDelegate.getViewController("TimePickerVC", onStoryboard: "DriveThru") as? TimePickerVC {
            
            timerPickerVC.timePickerDelegate = self
            
            timerPickerVC.startTime = startTime
            timerPickerVC.endTime = endTime
            timerPickerVC.selectedDateForDriveThruReq = selectedDateForDriveThruReq
            
            if !selectedTime.isEmpty {
                timerPickerVC.selectedTime = self.selectedTime
                timerPickerVC.selectedDateTime = self.selectedTimePickerDate
            }else {
                timerPickerVC.selectedTime = ""
                timerPickerVC.selectedDateTime  = Date()
            }
            
            timerPickerVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            timerPickerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            timerPickerVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
            
            if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                rootVC.present(timerPickerVC, animated: true, completion: nil)
            }            
        }
    }
}

extension RequestDetailVC : DateRangePickerDelegate {
    // MARK: - cancelDateRangePickerClicked
    func cancelDateRangePickerClicked() {
    }
    
    // MARK: - selectedDateRange
    func selectedDateRange(dateRange: [Date], startTime : String, endTime : String) {
        
        self.selectedDate = getDateToExpectedFormate(dateVal: dateRange.first!, formate: "dd MMM yyyy")
        self.selectedPickerDate = dateRange.first!
        print("self.selectedDate \(self.selectedDate)")
        
        self.selectedPickupDate = getDateToExpectedFormate(dateVal: dateRange.first!, formate: "yyyy-MM-dd")
        print("selectedPickupDate \(String(describing: self.selectedPickupDate))")
        
        showTimePicker(startTime: startTime, endTime: endTime, selectedDateForDriveThruReq : dateRange.first!)
    }
}

extension RequestDetailVC : TimePickerDelegate {
    // MARK: - selectedTime
    func selectedTime(time: String, timeIn24Hour: String, dateTime: Date) {
        self.selectedTimePickerDate = dateTime
        if !time.isEmpty {
            self.selectedTime = time
            self.selectedPickupTime = timeIn24Hour
            print("selectedPickupTime \(String(describing: self.selectedPickupTime))")
            
            DispatchQueue.main.async {
                if let rescheduleDriveThruRequestVC = appDelegate.getViewController("RescheduleDriveThruRequestVC", onStoryboard: "DriveThru") as? RescheduleDriveThruRequestVC {
                    rescheduleDriveThruRequestVC.rescheduleDriveThruRequestVCDelegate = self
                    rescheduleDriveThruRequestVC.modalPresentationStyle = .overFullScreen
                    self.present(rescheduleDriveThruRequestVC,animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - cancelTimePickerClicked
    func cancelTimePickerClicked() {
        
    }
}

extension RequestDetailVC : RescheduleDriveThruRequestPopupVCDelegate {
    // MARK: - getRescheduleReason
    func getRescheduleReason(reason: String) {
        print("===== RESCHEDULE =====")
        self.callRescheduleAPI(reason: reason)
    }
    
    // MARK: - callRescheduleAPI
    func callRescheduleAPI(reason : String){
        var params : [String:Any] = [:]
        
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["request_id"] = self.requestDetailObj.id ?? ""
        params["pickup_date"] = self.selectedPickupDate
        params["pickup_time"] = self.selectedPickupTime
        params["notes"] = reason
        
        print("Reschudle Params are \(params)")
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driveThruReschedule, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true)
                self.getDriveThruRequestDetail(isShowLoader: true)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}

/*
 Drive thru process
 Drive Thru  - Simple flow
 1. From Courier app, create 1 drive thru request request by adding invoice  -> PENDING
 
 2.  Ware house attendee, Drive Thru attendee.
 From Ware house app go to the Drive thru option select Ware House option and scan or enter HAWB number of your package -> APPROVED (timer will start)
 
 3. In same app Click on proceed button -> RECEIVED ( Timer will gone now )
 
 4 From KIOSK Validate app, Scan qr code from courier app’s QR code and Click on confirm button - VERIFIED (Timer will gone now)
 
 5.From Ware house app go to the Drive thru option and click on Drive thru option then Click on deliver button by giving name of staff, signature of staff and customer -> COMPLETED
 
 =======
 If Any package has query.
 2) From Ware house app go to Drive Thru option and go to Ware house option, Scan 1 package only and then back to listing and added comment for remaining package by clicking proceed button. QUERY
 
 3) From CSF Courier customer can Approve or reject it from request detail screen. On Approve it will start timer
 Start from 3rd main point
 
 === Demo Points ===
 1. Customer App Add Timeout while Adding Request. - Application + Web Panel (Add 30 second timeout for time picker)
 2. Change to Download to Share. - Application (UI Required)
 3. Add Pre-Define Option (Reasons) for cancel request. - (API + Application + Web Panel)
 4. Both should be able to reschedule request. - API + Application + Web Panel + Warehouse Attd.
 
 Note : We will require Pickup wait time and pickup setting in push notification response also for request detail screen
 */
