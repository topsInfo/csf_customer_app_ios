//
//  DriveThruRequestSuccessVC.swift
//  CSFCustomer
//
//  Created by iMac on 22/06/22.
//

import UIKit

class DriveThruRequestSuccessVC: UIViewController {
   
    // MARK: - IBOutlets
    @IBOutlet weak var viewPaymentSuccess: UIView!
    @IBOutlet weak var lblPaymentSuccessTitle : UILabel!
    
    @IBOutlet weak var lblPaymentSuccessDesc: UILabel!
    @IBOutlet weak var lblTotalPackages : UILabel!
    
    @IBOutlet weak var lblRequestDate: UILabel!
    @IBOutlet weak var lblTransactionId: UILabel!
    @IBOutlet weak var lblTransactionDate: UILabel!
    @IBOutlet weak var lblTotalPaid: UILabel!
    
    @IBOutlet weak var viewPaymentSuccessHeader: UIView!
    @IBOutlet weak var viewTransactionDetails: UIView!
    
    @IBOutlet weak var btnGoBack : UIButton!
    
    @IBOutlet weak var stackViewPaymentDetail: UIStackView!
    @IBOutlet weak var viewPaymentDetail: UIView!
    @IBOutlet weak var cnstImgY: NSLayoutConstraint!

    // MARK: - Global Variables
    var successPopupType : String = "1" // 1 -> Request 2- Transaction
    var timer : Timer?
    var resultDate : [String : Any]!
    var strSelectedDate : String = ""
    var strSelectedTime : String = ""
    var totalPackages : Int = 0
    var strSuccessMsg : String = ""
    
    // MARK: - Viewcontroller life cycle
     override func viewDidLoad() {
         super.viewDidLoad()
         configureControls()
     }
     
    func configureGoBackButton(){
        self.btnGoBack.layer.cornerRadius = 5
        self.btnGoBack.layer.masksToBounds = true
        self.btnGoBack.layer.borderWidth = 1
        self.btnGoBack.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor
    }
    
    // MARK: - Custom Methods
     func configureControls()
    {
        configureGoBackButton()
        self.viewPaymentDetail.layer.cornerRadius = 5
        self.viewPaymentDetail.layer.masksToBounds = true

        self.lblPaymentSuccessDesc.text = self.strSuccessMsg

        if successPopupType == "1" {
            self.cnstImgY.constant = -180
            self.viewTransactionDetails.isHidden = true
            
            self.viewPaymentSuccessHeader.layer.cornerRadius = 5
            self.viewPaymentSuccessHeader.layer.masksToBounds = true
            
            if !self.strSelectedDate.isEmpty && !self.strSelectedTime.isEmpty {
                self.lblRequestDate.text = "\(self.strSelectedDate) \(self.strSelectedTime)"
            }
            
            self.lblTotalPackages.text = "\(self.totalPackages) PACKAGES"
            
        }else if successPopupType == "2"{
            NotificationCenter.default.post(name: Notification.Name("CartUpdated"), object: nil)

            self.cnstImgY.constant = -220
            self.viewTransactionDetails.isHidden = false
            
            self.viewPaymentSuccessHeader.layer.cornerRadius = 5
            self.viewPaymentSuccessHeader.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            self.viewPaymentSuccessHeader.layer.masksToBounds = true

            self.viewTransactionDetails.layer.cornerRadius = 5
            self.viewTransactionDetails.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            self.viewTransactionDetails.layer.masksToBounds = true

            if !self.strSelectedDate.isEmpty && !self.strSelectedTime.isEmpty {
                self.lblRequestDate.text = "\(self.strSelectedDate) \(self.strSelectedTime)"
            }else {
                self.lblRequestDate.text = "-"
            }
            self.lblTotalPackages.text = "\(self.totalPackages) PACKAGES"
            
            if resultDate != nil {
                
                if let transactionId : String = resultDate["transaction_id"] as? String {
                    self.lblTransactionId.text = transactionId
                }else{
                    self.lblTransactionId.text = "-"
                }
                
                if let transactionDate : String = resultDate["date"] as? String {
                    self.lblTransactionDate.text = transactionDate
                }else {
                    self.lblTransactionDate.text = "-"
                }
                
                if let totalUSDAmount : String = resultDate["tota_amount"] as? String {
                    
                    if let totalTTDAmount : String = resultDate["tota_amount_ttd"] as? String {
                        
                        self.lblTotalPaid.text = "\(kCurrencySymbol)\(totalTTDAmount) \(kTTCurrency) /  \(kCurrencySymbol)\(totalUSDAmount) \(kCurrency)"
                    }
                }else{
                    self.lblTotalPaid.text = "-"
                }
            }
        }
        self.viewPaymentSuccess.dropShadow()
        self.viewPaymentDetail.dropShadow()

        timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(DriveThruRequestSuccessVC.timerEventCalled), userInfo: nil, repeats: false)
    }
    
    @objc func timerEventCalled()
    {
        timer?.invalidate()
        timer = nil
        self.redirectBackToDriveThruList()
    }

    // MARK: - IB-Action Methods
    @IBAction func btnGoBackClicked(_ sender: UIButton) {
        self.redirectBackToDriveThruList()
    }
    
    func redirectBackToDriveThruList(){
        if self.navigationController != nil {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DriveThruVC.self) {
                    NotificationCenter.default.post(name: Notification.Name("refreshDriveThruRequestList"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name("PickupRequestCompleted"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name("RefreshInvoiceList"), object: nil)
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
}
