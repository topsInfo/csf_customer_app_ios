//
//  DriveThruCell.swift
//  CSFCustomer
//
//  Created by iMac on 21/06/22.
//

import UIKit

//MARK: - DriveThruHistoryCell
class DriveThruHistoryCell: UITableViewCell {
    
    @IBOutlet weak var lblRequestId: TapAndCopyLabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPackages: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var stackViewDriveThru: UIStackView!
    @IBOutlet weak var btnCellBack: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
