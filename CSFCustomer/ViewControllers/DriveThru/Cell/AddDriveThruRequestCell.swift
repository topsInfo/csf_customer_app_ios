//
//  AddDriveThruRequestCell.swift
//  CSFCustomer
//
//  Created by iMac on 21/06/22.
//

import UIKit

//MARK: - AddDriveThruRequestCell
class AddDriveThruRequestCell: UITableViewCell {
    @IBOutlet weak var colorBar: UIView!
    @IBOutlet weak var imgChecked: UIImageView!
    @IBOutlet weak var lblHawbNumber: TapAndCopyLabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnViewRequest: UIButton!
    @IBOutlet weak var viewRequest: UIView!
    @IBOutlet weak var viewDriveThruRequest: UIView!
    @IBOutlet weak var viewStatus: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
