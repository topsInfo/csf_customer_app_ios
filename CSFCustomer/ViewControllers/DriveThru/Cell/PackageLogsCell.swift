//
//  PackageLogsCell.swift
//  CSFCustomer
//
//  Created by iMac on 09/08/22.
//

import UIKit

//MARK: - PackageLogsCell
class PackageLogsCell: UITableViewCell {
    
    @IBOutlet weak var lblCreatedDate : UILabel!
    @IBOutlet weak var lblCreatedTime : UILabel!
    @IBOutlet weak var lblLogs : UILabel!
    @IBOutlet weak var lblCreatedBy : UILabel!
    @IBOutlet weak var circleView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        circleView.layer.cornerRadius = circleView.frame.size.height / 2
        circleView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
