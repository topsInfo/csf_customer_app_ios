//
//  RequestedPackagesCell.swift
//  CSFCustomer
//
//  Created by iMac on 22/06/22.
//

import UIKit

//MARK: - RequestedPackagesCell
class RequestedPackagesCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var lblHawbNumber: TapAndCopyLabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var viewStatus : UIView!
    @IBOutlet weak var lblNote : UILabel!
    @IBOutlet weak var viewNote : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
