//
//  RescheduleDriveThruRequestVC.swift
//  CSFCustomer
//
//  Created by iMac on 20/09/22.
//

import UIKit
import IQKeyboardManagerSwift
import DropDown

let messageCharacterLimitForRescheduleRequest : Int = 200

protocol RescheduleDriveThruRequestPopupVCDelegate {
    func getRescheduleReason(reason:String)
}
class RescheduleDriveThruRequestVC: UIViewController, UITextViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tvDesc : UITextView!
    @IBOutlet weak var viewDesc : UIView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var tblRescheduleDriveThruRequest : UITableView!
    
    // MARK: - Global Variable
    var rescheduleDriveThruRequestVCDelegate : RescheduleDriveThruRequestPopupVCDelegate?
    var placeholderText = "Enter Notes"
    var rescheduleReason : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black") //UIColor.black.withAlphaComponent(0.5)

        configureTextView(textField:tvDesc, placeHolder: placeholderText, font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named:"theme_lightgray_color")!, cornerRadius: 5, borderColor: UIColor(named:"theme_lightgray_color")!, borderWidth: 1, bgColor: UIColor(named:"theme_textfield_bgcolor")!)
        tvDesc.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - 300
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
        tblRescheduleDriveThruRequest.tableFooterView = UIView()
        
        tvDesc.keyboardDistanceFromTextField = 80
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if (self.rescheduleReason.isEmpty || self.rescheduleReason == placeholderText) {
            appDelegate.showToast(message: Messsages.msg_enter_note, bottomValue: getSafeAreaValue())
            return false
        }
        
        return true
    }
    
    // MARK: - IBAction Methods
    @IBAction func btnSubmitClicked()
    {
        self.view.endEditing(true)
        self.rescheduleReason = self.tvDesc.text
        if isValidData() {
            print("Call reschedule drive thru api")
            self.dismiss(animated: true) {
                self.rescheduleDriveThruRequestVCDelegate?.getRescheduleReason(reason: self.rescheduleReason.trimmingCharacters(in: .whitespacesAndNewlines))
            }
        }
    }
    
    @IBAction func btnCancelClicked()
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension RescheduleDriveThruRequestVC {
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == placeholderText
        {
            textView.text = ""
            textView.textColor = UIColor(named: "theme_black_color")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty
        {
            textView.text = placeholderText
            textView.textColor = UIColor(named: "theme_lightgray_color") //Colors.theme_lightgray_color
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return newText.count <= messageCharacterLimitForRescheduleRequest
    }
}
