//
//  AddDriveThruRequestVC.swift
//  CSFCustomer
//
//  Created by iMac on 21/06/22.
//

import UIKit
import SwiftyJSON
import Kronos

class AddDriveThruRequestVC: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var stackViewListHeader: UIStackView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblTTDAmount: UILabel!
    @IBOutlet weak var tblAddDriveThruRequest : UITableView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var viewPayToProceed: UIView!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var stackViewProceedMain: UIStackView!
    @IBOutlet weak var cnstStackViewProceedMain: NSLayoutConstraint!
    
    @IBOutlet weak var viewTotalRecord: UIView!
    @IBOutlet weak var lblTotalRequests: UILabel!
    
    // MARK: - Global Variables
    var refreshControl = UIRefreshControl()
    var objUserInfo = UserInfo()
    var isLoadingList : Bool = false
    var pageNo : Int = 1
    var totalPages : Int = 0
    var totalRecordsCount : Int = 0
    var strMemberID : String = ""
    var strMemberName  : String = ""
    var strStartDate : String  = ""
    var strEndDate : String  = ""
    var strDateType :String = ""
    var strSearch : String = ""
    var isShowLoader : Bool = true
    var arrList : [InvoiceList] = [InvoiceList]()
    var arrMemberList : [MemberList] = [MemberList]()
    var arrSelectedList : [InvoiceList] = [InvoiceList]()
    var arrSelectedSections : [Int] = [Int]()
    var strInvoiceIdsForDriveThruRequest : String = ""
    
    var arrSelectedPaidInvoiceList : [InvoiceList] = [InvoiceList]()
    var arrSelectedUnPaidInvoiceList : [InvoiceList] = [InvoiceList]()
    
    var strPaidInvoiceListIds : String = ""
    var strUnPaidInvoiceListIds : String = ""
    
    var walletUSDAmount : String = ""
    var walletTTDAmount : String = ""
    var totalTTDAmount : String = ""
    var totalUSDAmount : String = ""
    
    var selectedDate : String = ""
    var selectedPickerDate : Date!
    
    var selectedTime : String = ""
    var selectedTimePickerDate : Date!
    
    var selectedPickupDate : String = ""
    var selectedPickupTime : String = ""
    
    var proccedBtnType : Int = 0 // 1 - Proceed 2 - Pay to Proceed
    var arrPickupSettingListMain : [PickupSettingModel] = [PickupSettingModel]()
    var arrPickupSettingList : [PickupSettingModel] = [PickupSettingModel]()
    var pickupWaitTime : Int = 15
    
    var objMyInvoiceObj : Invoice = Invoice()
    var isNeedToRemoveCurrentDate : Bool = false
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
        syncClockTime()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblAddDriveThruRequest.addSubview(refreshControl)
        
        tblAddDriveThruRequest.delegate = self
        tblAddDriveThruRequest.dataSource = self
        tblAddDriveThruRequest.estimatedRowHeight = 50.0
        tblAddDriveThruRequest.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tblAddDriveThruRequest.frame.size.width, height: .leastNormalMagnitude))
        
        self.stackViewListHeader.isHidden = true
        self.viewTotalRecord.alpha = 0
        self.cnstStackViewProceedMain.constant = 0
        self.stackViewProceedMain.isHidden = true
        self.tblAddDriveThruRequest.isHidden = true
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshAddDriveThruRequestList(_:)), name: Notification.Name("refreshAddDriveThruRequestList"), object: nil)
        
        self.getInvoiceList(isShowLoader: true)
    }
    
    // MARK: - Custom methods
    @objc func refresh()
    {
        pageNo = 1
        getInvoiceList(isShowLoader: false)
    }
    
    // MARK: - Custom methods
    @objc func refreshAddDriveThruRequestList(_ notification: NSNotification)
    {
        self.pageNo = 1
        getInvoiceList(isShowLoader: true)
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPayToProceedClicked(_ sender: UIButton) {
        checkDateToRemoveFromCalendar()
        self.proccedBtnType = 2
        checkSystemTimeChangedOrNot()
    }
    
    @IBAction func btnProceedClicked(_ sender: UIButton) {
        checkDateToRemoveFromCalendar()
        self.proccedBtnType = 1
        checkSystemTimeChangedOrNot()
    }
    
    // MARK: - Check Date To Remove From Calander
    func checkDateToRemoveFromCalendar(){
        isNeedToRemoveCurrentDate = false
        if self.arrSelectedPaidInvoiceList.count > 0 {
            if arrSelectedPaidInvoiceList.filter({ $0.onDelivery == 1 }).count > 0 {
                isNeedToRemoveCurrentDate = true
            }
        }
        
        if self.arrSelectedUnPaidInvoiceList.count > 0 {
            if arrSelectedUnPaidInvoiceList.filter({ $0.onDelivery == 1 }).count > 0 {
                isNeedToRemoveCurrentDate = true
            }
        }
    }
    
    // MARK: - Show date range picker
    func showDatePicker(){
        if let dateRangePickerVC = appDelegate.getViewController("DateRangePickerVC", onStoryboard: "DriveThru") as? DateRangePickerVC {
            
            if self.arrPickupSettingListMain.count > 0 {
                var arrPickupSettingListTemp : [PickupSettingModel] = [PickupSettingModel]()
                
                if isNeedToRemoveCurrentDate {
                    self.arrPickupSettingList = removeCurrentDateFromDateArr(arrPickupSettingList: self.arrPickupSettingListMain)
                }else {
                    self.arrPickupSettingList = self.arrPickupSettingListMain
                }
                
                arrPickupSettingListTemp = self.arrPickupSettingList
                
                if arrPickupSettingListTemp.count > 0 {
                    dateRangePickerVC.arrPickupSettingList = arrPickupSettingListTemp
                    dateRangePickerVC.dateRangeDelegate = self
                    
                    let dateArrayOfPickupSettings : PickupSettingModel = arrPickupSettingListTemp[0]
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    if let dateOfPickupSettings : String = dateArrayOfPickupSettings.date {
                        print("dateOfPickupSettings is \(dateOfPickupSettings)")

                        var dateToShowSelected : Date = dateFormatter.date(from: dateOfPickupSettings)!
                        print("dateToShowSelected is \(dateToShowSelected)")
                        
                        if dateToShowSelected.isToday {
                            print("Today from Add drive thru request screen")
                            print("end time is \(String(describing: dateArrayOfPickupSettings.endTime))")
                            let startTimeOfCurrentDate : Date = Date().adding(minutes: self.pickupWaitTime)
                            print("after adding 15 min \(startTimeOfCurrentDate)")
                            
                            let calendar = Calendar.current
                            let hour = calendar.component(.hour, from: startTimeOfCurrentDate)
                            let minutes = calendar.component(.minute, from: startTimeOfCurrentDate)
                            let seconds = calendar.component(.second, from: startTimeOfCurrentDate)
                            print("hour min sec \(hour) \(minutes) \(seconds)")
                            
                            let newStartTime : String = "\(hour):\(minutes)"
                            print("newStartTime is  \(newStartTime)")
                            
                            let apiEndTime = "\(dateOfPickupSettings)" + " " + "\(dateArrayOfPickupSettings.endTime ?? "20:00")"
                            let newEndTime = "\(dateOfPickupSettings)" + " " + "\(newStartTime)"
                            print("A is \(apiEndTime)")
                            print("B is \(newEndTime)")
                            
                            let dateFormatter1 = DateFormatter()
                            dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm"
                            
                            if let apiDate : Date = dateFormatter1.date(from: apiEndTime), let newDate : Date = dateFormatter1.date(from: newEndTime) {
                                
                                print("dateA is \(apiDate)")
                                print("dateB is \(newDate)")
                                
                                let apiTimeStamp = apiDate.timeIntervalSince1970
                                let newTimeStamp = newDate.timeIntervalSince1970
                                
                                if newTimeStamp >= apiTimeStamp {
                                    print("discard today date")
                                    if arrPickupSettingListTemp.count > 1 {
                                        arrPickupSettingListTemp.remove(at: 0)
                                        dateRangePickerVC.arrPickupSettingList = arrPickupSettingListTemp
                                        
                                        let dateArrayOfPickupSettings : PickupSettingModel = arrPickupSettingListTemp[0]
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "yyyy-MM-dd"
                                        if let dateOfPickupSettings : String = dateArrayOfPickupSettings.date {
                                            dateToShowSelected = dateFormatter.date(from: dateOfPickupSettings)!
                                        }
                                    }
                                }else {
                                    print("accept today date")
                                }
                            }
                        }
                        
                        dateRangePickerVC.datesRange = [dateToShowSelected]
                        
                        dateRangePickerVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                        dateRangePickerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        dateRangePickerVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
                        
                        if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                            rootVC.present(dateRangePickerVC, animated: true, completion: nil)
                        }
                    }
                }else {
                    appDelegate.showToast(message: Messsages.msg_pickup_dates_not_available, bottomValue: getSafeAreaValue())
                }
            }
        }
    }

    func showTimePicker(startTime : String, endTime : String, selectedDateForDriveThruReq : Date){
        
        if let timerPickerVC = appDelegate.getViewController("TimePickerVC", onStoryboard: "DriveThru") as? TimePickerVC {
            
            timerPickerVC.timePickerDelegate = self
            
            timerPickerVC.startTime = startTime
            timerPickerVC.endTime = endTime
            timerPickerVC.selectedDateForDriveThruReq = selectedDateForDriveThruReq
            
            if !selectedTime.isEmpty {
                timerPickerVC.selectedTime = self.selectedTime
                timerPickerVC.selectedDateTime = self.selectedTimePickerDate
            }else {
                timerPickerVC.selectedTime = ""
                timerPickerVC.selectedDateTime  = Date()
            }
            
            timerPickerVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            timerPickerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            timerPickerVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
            
            if let rootVC = UIApplication.shared.windows.filter({ $0.isKeyWindow }).first?.rootViewController {
                rootVC.present(timerPickerVC, animated: true, completion: nil)
            }
        }
    }
    
    func getInvoiceIdsOfSelectedRequests() -> String{
        var strInvoiceIds : String  = ""
        
        for (index,value) in arrSelectedSections.enumerated()
        {
            if value == 1
            {
                let objList = arrList[index]
                if strInvoiceIds != ""
                {
                    strInvoiceIds = strInvoiceIds + "," + objList.id
                }
                else
                {
                    strInvoiceIds =  objList.id
                }
            }
        }
        return strInvoiceIds
    }
    
    // MARK: - isValidated
    func isValidated() -> Bool{
        
        if self.selectedPickupDate.isEmpty {
            appDelegate.showToast(message: "Please select pickup date", bottomValue: getSafeAreaValue())
            return false
        }else if self.selectedPickupTime.isEmpty {
            appDelegate.showToast(message: "Please select pickup time", bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    
    // MARK: - checkSystemTimeChangedOrNot
    func checkSystemTimeChangedOrNot(){

        if Clock.now != nil {
            print("Clock date is \(String(describing: Clock.now))")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let clockDate : String = dateFormatter.string(from: Clock.now!)
            print("Clock date in my formate is \(clockDate)")
            
            let currentDate : String = dateFormatter.string(from: Date())
            print("CurrentDate date in my formate is \(currentDate)")
            
            guard let firstTimeInterval = Clock.now?.timeIntervalSince1970 else {
                print("** Couldn't get first time interval **")
                appDelegate.showToast(message: "Please try again after some time and check your network provided time from device's Date & Time settings.", bottomValue: getSafeAreaValue())
                return
            }
            let secondTimeInterval = Date().timeIntervalSince1970
            
            print("first time interval is \(String(describing: firstTimeInterval))")
            print("second time interval is \(String(describing: secondTimeInterval))")
            
            let timeDiff = abs(Int(firstTimeInterval) - Int(secondTimeInterval))
            print("time diff is \(timeDiff)")
            
            if (timeDiff > 1) {
                appDelegate.showToast(message: "Please enable network provided time from device's Date & Time settings.", bottomValue: getSafeAreaValue())
            }else {
                print("Time is proper")
                self.showDatePicker()
            }
        }else{
            syncClockTime()
        }
    }
    
    // MARK: - syncClockTime
    func syncClockTime(){
        appDelegate.showLoader()
        Clock.sync(completion:  { date, offset in
            appDelegate.hideLoader()
        })
    }
}

extension AddDriveThruRequestVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AddDriveThruRequestCell", for: indexPath) as? AddDriveThruRequestCell{
            cell.btnViewRequest.tag = indexPath.row
            cell.btnViewRequest.addTarget(self, action: #selector(self.btnViewRequestClicked(_:)), for: .touchUpInside)
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(driveThruDetailTapped(sender:)))
            cell.viewDriveThruRequest.tag = indexPath.row
            cell.viewDriveThruRequest.isUserInteractionEnabled = true
            cell.viewDriveThruRequest.addGestureRecognizer(tapGesture)
            
            if arrSelectedSections[indexPath.row] == 1
            {
                cell.imgChecked.image = UIImage(named: "checkbox")
                cell.contentView.backgroundColor =  Colors.theme_lightblue_color
                cell.backgroundColor =  Colors.theme_lightblue_color
            }
            else
            {
                cell.imgChecked.image = UIImage(named: "uncheckbox")
                cell.contentView.backgroundColor = Colors.theme_bgColor
                cell.backgroundColor = Colors.theme_bgColor
            }
            
            let objList = getInvoiceObject(tag: indexPath.row)
            
            if objList.isAdded == 1 {
                cell.imgChecked.isHidden = true
            }else {
                cell.imgChecked.isHidden = false
            }
            
            if objList.paidStatus == DriveThruPaymentStatus.Paid.dispValue()
            {
                cell.colorBar.backgroundColor = Colors.theme_green_color
            }else if objList.paidStatus == DriveThruPaymentStatus.UnPaid.dispValue() {
                cell.colorBar.backgroundColor = Colors.theme_red_color
            }else if objList.paidStatus == DriveThruPaymentStatus.fullDiscount.dispValue() {
                cell.colorBar.backgroundColor = Colors.theme_blue_color
            }
            
            cell.lblHawbNumber.text = objList.hawbNumber ?? ""
            cell.lblName.text = objList.descriptionName ?? ""
            cell.lblName.lineBreakMode = .byTruncatingTail
            cell.lblName.numberOfLines = 3
            
            cell.lblDate.text = objList.dateOfInvoice ?? ""
            cell.lblAmount.text = "\(kTTCurrency) \(objList.totalTtd ?? "")"
            
            let isOnDelivery = objList.onDelivery ?? 0
            if isOnDelivery == 1 {
                //show Truck
                cell.viewStatus.isHidden = false
            }else{
                //Not show Truck
                cell.viewStatus.isHidden = true
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isLoadingList &&
            indexPath.row == arrList.count - 1 &&
            pageNo < self.totalPages {
            isLoadingList = true
            pageNo += 1
            getInvoiceList(isShowLoader: pageNo > 2)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MARK: - Custom Methods
    @objc func btnViewRequestClicked(_ sender:UIButton)
    {
        print("btnViewRequestClicked")
        self.redirectToInvoiceDetail(index: sender.tag)
    }
    
    @objc func redirectToInvoiceDetail(index : Int)
    {
        var params : [String:Any] = [:]
        let objInvoice = getInvoiceObject(tag: index)
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"] = objInvoice.id ?? ""
        print("Params are \(params)")
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ViewInvoice, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                let objInvoiceDetail = InvoiceDetail(fromJson: JSON(resultDict))
                if let vc = appDelegate.getViewController("InvoiceDetailVC", onStoryboard: "Invoice") as? InvoiceDetailVC {
                    vc.userID = self.objUserInfo.id ?? ""
                    vc.isFullRefund = objInvoiceDetail.fullRefund ?? ""
                    vc.objInvoiceDetail = objInvoiceDetail
                    if let status = objInvoiceDetail.paidStatus as? String, status == "0"
                    {
                        vc.isPaid = false
                    }
                    else
                    {
                        vc.isPaid = true
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    @objc func driveThruDetailTapped(sender:UITapGestureRecognizer) //new change
    {
        let tag = sender.view!.tag
        print("check box clicked \(tag)")
        
        let objList = getInvoiceObject(tag: tag)
        
        let isAllowed = objList.allowed ?? 0
        if isAllowed == 0 {
            self.showAlert(title: "", msg: self.objMyInvoiceObj.restrictedItemsAlert ?? "", vc: self)
            return
        }
        
//        if !objList.isAdded {
        if objList.isAdded != 1 {
            
            if arrSelectedSections[tag] == 0
            {
                arrSelectedSections[tag] = 1
            }
            else
            {
                arrSelectedSections[tag] = 0
            }
            
            arrSelectedList.removeAll()
            showTotalSelectedCount()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt")
    }
    
    func getInvoiceObject(tag:Int) -> InvoiceList
    {
        if arrList.count > 0
        {
            return arrList[tag]
        }
        return InvoiceList()
    }
}

extension AddDriveThruRequestVC {
    func  getInvoiceList(isShowLoader : Bool)
    {
        objUserInfo = getUserInfo()
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["page_id"] = pageNo
        params["search_text"] = strSearch
        params["member_id"] = strMemberID
        params["start_date"] = strStartDate
        params["end_date"] = strEndDate
        params["date_type"] =  strDateType
        params["is_drive_thru"] =  "1"
        print("params are \(params)")
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.UndeliveredInvoiceList, showLoader: isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            
            self.arrSelectedList.removeAll()
            if status == true
            {
                self.isLoadingList = false
                if self.pageNo == 1
                {
                    self.arrList.removeAll()
                }
                let objMyInvoiceList = Invoice.init(fromJson: JSON(resultDict))
                self.objMyInvoiceObj = objMyInvoiceList
                
                if self.arrList.count > 0
                {
                    self.arrList.append(contentsOf: objMyInvoiceList.list)
                    for _ in 0..<objMyInvoiceList.list.count
                    {
                        self.arrSelectedSections.append(0)
                    }
                }
                else
                {
                    self.arrList = objMyInvoiceList.list
                    self.arrSelectedSections.removeAll()
                    for _ in 0..<self.arrList.count
                    {
                        self.arrSelectedSections.append(0)
                    }
                }
                
                if let totalCount = objMyInvoiceList.totalCount
                {
                    self.totalRecordsCount = totalCount
                }
                
                if let totalPage = objMyInvoiceList.totalPage
                {
                    self.totalPages = totalPage
                }
                self.reloadData()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    func reloadData(){
        DispatchQueue.main.async {
            self.viewTotalRecord.alpha = 1
            self.showTotalSelectedCount()
            
            if self.arrList.count > 0 {
                self.stackViewListHeader.isHidden = false
                self.viewTotalRecord.isHidden = false
                self.tblAddDriveThruRequest.isHidden = false
                self.lblNoRecordFound.isHidden = true
            }else {
                self.stackViewListHeader.isHidden = true
                self.viewTotalRecord.isHidden = true
                self.tblAddDriveThruRequest.isHidden = true
                self.lblNoRecordFound.isHidden = false
            }
            
            self.tblAddDriveThruRequest.reloadData()
        }
    }
    
    func showTotalSelectedCount(){
        
        for var i in 0..<arrSelectedSections.count {
            if arrSelectedSections[i] == 1 {
                arrSelectedList.append(self.arrList[i])
            }
            i += 1
        }
        DispatchQueue.main.async { [self] in
            self.tblAddDriveThruRequest.reloadData()
        }
        
        if self.arrSelectedList.count > 0 {
            
            arrSelectedPaidInvoiceList = self.arrSelectedList.filter({ $0.paidStatus == 1 || $0.paidStatus == 2 })
            arrSelectedUnPaidInvoiceList = self.arrSelectedList.filter({ $0.paidStatus == 0 })
            
            if arrSelectedPaidInvoiceList.count > 0 {
                let arrPaidInvoiceIds : [String]  = arrSelectedPaidInvoiceList.map({ $0.id })
                strPaidInvoiceListIds = arrPaidInvoiceIds.joined(separator: ",")
            }
            
            if arrSelectedUnPaidInvoiceList.count > 0 {
                let arrUnPaidInvoiceIds : [String]  = arrSelectedUnPaidInvoiceList.map({ $0.id })
                strUnPaidInvoiceListIds = arrUnPaidInvoiceIds.joined(separator: ",")
            }
            
            if arrSelectedUnPaidInvoiceList.count > 0 {
                let sumOfSelectedUnPaidRequestUSD : Float = arrSelectedUnPaidInvoiceList.map({Float($0.totalUsd ?? "0")!}).sum()
                let sumOfSelectedUnPaidRequestTTD : Float = arrSelectedUnPaidInvoiceList.map({Float($0.totalTtd ?? "0")!}).sum()
                print("sumOfSelectedUnPaidRequestUSD is \(sumOfSelectedUnPaidRequestUSD)")
                print("sumOfSelectedUnPaidRequestTTD is \(sumOfSelectedUnPaidRequestTTD)")
                
                totalUSDAmount = String(format: "%.2f", sumOfSelectedUnPaidRequestUSD)
                totalTTDAmount = String(format: "%.2f", sumOfSelectedUnPaidRequestTTD)
                
                self.lblTTDAmount.text = "\(kTTCurrency) \(String(format: "%.2f", sumOfSelectedUnPaidRequestTTD))"
                
                self.cnstStackViewProceedMain.constant = 45
                self.stackViewProceedMain.isHidden = false
                self.btnProceed.isHidden = true
                self.viewPayToProceed.isHidden = false
            }else {
                self.cnstStackViewProceedMain.constant = 45
                self.stackViewProceedMain.isHidden = false
                self.btnProceed.isHidden = false
                self.viewPayToProceed.isHidden = true
            }
        }else{
            self.cnstStackViewProceedMain.constant = 0
            self.stackViewProceedMain.isHidden = true
            self.btnProceed.isHidden = true
            self.viewPayToProceed.isHidden = true
        }
        
        if self.arrSelectedList.count > 0 {
            self.lblTotalRequests.text = "Selected \(arrSelectedList.count) / \(self.totalRecordsCount) Invoices"
        }
        else{
            self.lblTotalRequests.text = "Total - \(self.totalRecordsCount) Invoices"
        }
    }
}

extension AddDriveThruRequestVC {
    
    func callAPIForAddToCart(params:[String:Any])
    {
        print("Add to cart Drive Thru Parmas are \(params)")
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.addToCart, showLoader:true) { [self] (resultDict, status, message) in
            if status == true
            {
                UserDefaults.standard.setValue(true, forKey: kCartItemAvailable)
                if arrSelectedUnPaidInvoiceList.count > 0  {
                    
                    let objAddToCart = AddToCartDataModel(fromJson: JSON(resultDict))
                    
                    if let vc = appDelegate.getViewController("PayCheckoutVC", onStoryboard: "Referral") as? PayCheckoutVC {
                        
                        vc.totalUSDAmount = objAddToCart.totalUsdAmount
                        vc.totalTTDAmount = objAddToCart.totalTtdAmount
                        vc.invoiceIDs = strUnPaidInvoiceListIds
                        vc.invoiceCount = arrSelectedUnPaidInvoiceList.count
                        vc.walletUSDAmount = objAddToCart.walletUsd
                        vc.walletTTDAmount = objAddToCart.walletTtd
                        
                        //DriveThru Request
                        vc.isFromDriveThruRequest = true
                        vc.strInvoiceIdsForDriveThruRequest = self.strInvoiceIdsForDriveThruRequest
                        vc.strPickupDateForDriveThruRequest = self.selectedPickupDate
                        vc.strPickupTimeForDriveThruRequest = self.selectedPickupTime
                        vc.strDateToDisplayForDriveThruRequest = self.selectedDate
                        vc.strTimeToDisplayForDriveThruRequest = self.selectedTime
                        self.navigationController?.pushViewController(vc,animated:true)
                    }
                }
            }
            else
            {
                self.showAlert(title: "", msg: message, vc: self)
            }
        }
    }
}

extension AddDriveThruRequestVC : DateRangePickerDelegate {
    func cancelDateRangePickerClicked() {
    }
    
    func selectedDateRange(dateRange: [Date], startTime : String, endTime : String) {
        
        self.selectedDate = getDateToExpectedFormate(dateVal: dateRange.first!, formate: "dd MMM yyyy")
        self.selectedPickerDate = dateRange.first!
        print("self.selectedDate \(self.selectedDate)")
        
        self.selectedPickupDate = self.getDateToExpectedFormate(dateVal: dateRange.first!, formate: "yyyy-MM-dd")
        print("selectedPickupDate \(String(describing: self.selectedPickupDate))")
        
        showTimePicker(startTime: startTime, endTime: endTime, selectedDateForDriveThruReq : dateRange.first!)
    }
    
    func getDateToExpectedFormate(dateVal : Date, formate : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        let dateString = dateFormatter.string(from: dateVal)
        return dateString
    }
}

extension AddDriveThruRequestVC : TimePickerDelegate {
    func selectedTime(time: String, timeIn24Hour: String, dateTime: Date) {
        self.selectedTimePickerDate = dateTime
        if !time.isEmpty {
            self.selectedTime = time
            self.selectedPickupTime = timeIn24Hour
            print("selectedPickupTime \(String(describing: self.selectedPickupTime))")
            
            if self.proccedBtnType == 1 {
                if isValidated() {
                    strInvoiceIdsForDriveThruRequest  = getInvoiceIdsOfSelectedRequests()
                    print("selected req ids are \(strInvoiceIdsForDriveThruRequest)")
                    if !strInvoiceIdsForDriveThruRequest.isEmpty {
                        let objUserInfo = getUserInfo()
                        self.createDriveThruRequest(userId: objUserInfo.id ?? "", invoiceId: self.strInvoiceIdsForDriveThruRequest, pickupDate: self.selectedPickupDate, pickupTime: self.selectedPickupTime, selectedDateToDisplay: self.selectedDate, selectedTimeToDisplay: self.selectedTime,type: 1, resultDate: nil, fromVC: self)
                    }
                }
            }
            else if self.proccedBtnType == 2 {
                if isValidated() {
                    print("btnPayToProceedClicked")
                    strInvoiceIdsForDriveThruRequest  = getInvoiceIdsOfSelectedRequests()
                    print("selected req ids are \(strInvoiceIdsForDriveThruRequest)")

                    let objUserInfo = getUserInfo()
                    var params : [String:Any] = [:]
                    params["user_id"] = objUserInfo.id ?? ""
                    params["invoice_id"] = strUnPaidInvoiceListIds
                    callAPIForAddToCart(params: params)
                }
            }
        }
    }
    
    func cancelTimePickerClicked() {
        
    }
}
