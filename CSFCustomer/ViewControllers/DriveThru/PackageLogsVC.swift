//
//  PackageLogsVC.swift
//  CSFCustomer
//
//  Created by iMac on 09/08/22.
//

import UIKit

class PackageLogsVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
  
    // MARK: - IBOutlets
    @IBOutlet weak var tblPackageLogs : UITableView!
    @IBOutlet weak var dimmerView : UIView!
    @IBOutlet weak var topConstant : NSLayoutConstraint!
   
    // MARK: - Global Variables
    var arrDriveThruLogs : [DriveThruLogsModel] =  [DriveThruLogsModel]()

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        showCard()
    }
    
    // MARK: - ShowCard
    func showCard()
    {
        var height : CGFloat = 0.0
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            DispatchQueue.main.async {
                self.view.layoutIfNeeded()
                height = self.tblPackageLogs.contentSize.height
                self.view.layoutIfNeeded()
                let calculatedValue = self.view.frame.size.height/3.5
                let topValue = (safeAreaHeight - CGFloat((Int(height+66))))
                if topValue <= calculatedValue
                {
                    self.topConstant.constant = calculatedValue
                    self.tblPackageLogs.isScrollEnabled = true
                }
                else
                {
                    self.topConstant.constant = topValue
                    self.tblPackageLogs.isScrollEnabled = false
                }
            }
            
            dimmerView.backgroundColor = UIColor(named:"theme_bg_color")//Colors.theme_white_color
        }
    }
    // MARK: - IB-Action Methods
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PackageLogsVC {
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDriveThruLogs.count
      //  return arrSwitchUserList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PackageLogsCell", for: indexPath) as? PackageLogsCell{
            let objLog  : DriveThruLogsModel = self.arrDriveThruLogs[indexPath.row]
            
            if let strDateTime : String = objLog.createdAt {
                if strDateTime.contains(" ") {
                    let dateString : String = strDateTime.components(separatedBy: " ")[0]
                    let timeString = objLog.createdAt.replacingOccurrences(of: dateString, with: "").trimmingCharacters(in: .whitespacesAndNewlines)
                    cell.lblCreatedDate.text = dateString
                    cell.lblCreatedTime.text = timeString
                }
            }else {
                cell.lblCreatedDate.text = ""
                cell.lblCreatedTime.text = ""
            }
            
            if let strCreatedBy : String = objLog.createdBy {
                cell.lblCreatedBy.text = strCreatedBy.uppercased()
            }
            cell.lblLogs.text = objLog.logs ?? ""
            return cell
        }
        return UITableViewCell()
    }
}
