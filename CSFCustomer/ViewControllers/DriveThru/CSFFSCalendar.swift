//
//  XYZFSCalendar.swift
//  FSCalendarDemo
//
//  Created by Tops on 26/02/24.
//

import Foundation
import FSCalendar

class CSFFSCalendar: FSCalendar {

   override init(frame: CGRect) {
      super.init(frame: frame)
   }

   required init?(coder aDecoder: NSCoder) {

       super.init(coder: aDecoder)

       let currentTimeZone = TimeZone.current
       print("currentTimeZone is \(String(describing: currentTimeZone))")
       let timeZoneIdentifier : String = currentTimeZone.identifier
       let timeZone = TimeZone(identifier: timeZoneIdentifier)
       print("timeZoneIdentifier is \(String(describing: timeZoneIdentifier))")
       if !timeZoneIdentifier.isEmpty {
           if let abbreviation = timeZone?.abbreviation() {
               print("Abbreviation for \(timeZoneIdentifier): \(abbreviation)")
               if let projectTimeZone =  TimeZone(identifier: abbreviation) {
                   self.formatter.timeZone = projectTimeZone
//                   self.setValue(projectTimeZone, forKey: "timeZone")
//                   self.perform(Selector(("invalidateDateTools")))
               }
           } else {
               print("Unable to determine the abbreviation for the time zone = \(timeZoneIdentifier).")
           }
       }

   }
}
/*
 currentTimeZone is America/Mazatlan (fixed)
 timeZoneIdentifier is America/Mazatlan
 
 currentTimeZone is Asia/Kolkata (fixed)
 timeZoneIdentifier is Asia/Kolkata

 currentTimeZone is America/Hermosillo (fixed)
 timeZoneIdentifier is America/Hermosillo
 
 currentTimeZone is America/New_York (fixed)
 timeZoneIdentifier is America/New_York
 
 currentTimeZone is America/Guyana (fixed)
 timeZoneIdentifier is America/Guyana
 */
