//
//  SwitchProfileVC.swift
//  CSFCustomer
//
//  Created by Tops on 06/04/21.
//

import UIKit
import CoreData
import SwiftyJSON
import SWRevealViewController
class SwitchAccountVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var tblUserList : UITableView!
    @IBOutlet weak var footerView : UIView!
    @IBOutlet weak var dimmerView : UIView!
    @IBOutlet weak var topConstant : NSLayoutConstraint!
    
    // MARK: - Global Variable
    var arrSwitchUserList : [SwitchAccount] = [SwitchAccount]()
    var arrSwitchUserActiveList : [SwitchAccount] = [SwitchAccount]()
    var arrSwitchUserInactiveList : [SwitchAccount] = [SwitchAccount]()
    var rowHeight : Int = 92
    var objUserInfo = UserInfo()
    var selectedIndex : Int = -1
    var topBarHeight = 60
    var footerViewHeight = 92
    var navvc : UINavigationController?
    var isAmazonLoginExists : Bool = false
    var isAppleLoginExists : Bool  = false
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(fetchLatestRecords(_:)), name: NSNotification.Name(rawValue: "DBRecordDeleted"), object: nil)
        tblUserList.estimatedRowHeight = 92
        tblUserList.rowHeight = UITableView.automaticDimension
        tblUserList.dataSource = self
        tblUserList.delegate = self
        fetchRecords()
    }
    
    /**
     Fetches the latest records.
     */
    @objc func fetchLatestRecords(_ notification: NSNotification)
    {
        fetchRecords()
    }
    
    /**
     Fetches the records for the current user.
     */    
    func fetchRecords()
    {
        arrSwitchUserList.removeAll()
        arrSwitchUserActiveList.removeAll()
        arrSwitchUserInactiveList.removeAll()
        footerViewHeight = 92
        isAmazonLoginExists = false
        isAppleLoginExists = false
        let objManageCoreData = ManageCoreData()
        let result =  objManageCoreData.fetchData(entityName: kSwitchUserEntity)
        if result.count > 0
        {
            for data in result as [NSManagedObject]
            {
                let objSwitchAccount = SwitchAccount()
                if let activeStatus = data.value(forKey: SwitchUserInfo.isActive.rawValue) as? Bool
                {
                    objSwitchAccount.userID = data.value(forKey: SwitchUserInfo.userID.rawValue) as? String ?? ""
                    objSwitchAccount.uniqueID = data.value(forKey: SwitchUserInfo.uniqueID.rawValue) as? String ?? ""
                    objSwitchAccount.boxID = data.value(forKey: SwitchUserInfo.boxID.rawValue) as? String ?? ""
                    objSwitchAccount.imgUser = data.value(forKey: SwitchUserInfo.imgUser.rawValue) as? String ?? ""
                    objSwitchAccount.isDefault =  data.value(forKey: SwitchUserInfo.isDefault.rawValue) as? Bool ?? false
                    objSwitchAccount.firstName = data.value(forKey: SwitchUserInfo.firstName.rawValue) as? String ?? ""
                    objSwitchAccount.lastName = data.value(forKey: SwitchUserInfo.lastName.rawValue) as? String ?? ""
                    objSwitchAccount.authToken = data.value(forKey: SwitchUserInfo.authToken.rawValue) as? String ?? ""
                    objSwitchAccount.isActive = data.value(forKey: SwitchUserInfo.isActive.rawValue) as? Bool ?? false
                    objSwitchAccount.LoginType = data.value(forKey: SwitchUserInfo.loginType.rawValue) as? String ?? ""
                    if objSwitchAccount.LoginType == LoginType.AmazonLogin.rawValue && objSwitchAccount.isActive == true
                    {
                        isAmazonLoginExists = true
                    }
                    else if objSwitchAccount.LoginType == LoginType.AppleLogin.rawValue && objSwitchAccount.isActive == true
                    {
                        isAppleLoginExists = true
                    }
                /*    print("\(objSwitchAccount.userID)")
                    print("\(objSwitchAccount.uniqueID)")
                    print("\(objSwitchAccount.boxID)")
                    print("\(objSwitchAccount.imgUser)")
                    print("\(objSwitchAccount.isDefault)") */
                    if activeStatus == true
                    {
                        arrSwitchUserActiveList.append(objSwitchAccount)
                        if arrSwitchUserActiveList.count >= 5
                        {
                            footerViewHeight = 0
                        }
                    }
                    else
                    {
                        arrSwitchUserInactiveList.append(objSwitchAccount)
                    }
                }
            }
            var sortedActiveArray : [SwitchAccount] = [SwitchAccount]()
            var sortedInActiveArray : [SwitchAccount] = [SwitchAccount]()
            if arrSwitchUserActiveList.count > 0
            {
                sortedActiveArray = arrSwitchUserActiveList.sorted { $0.firstName < $1.firstName }
                // activeUserCount = arrSwitchUserActiveList.count
            }
            if arrSwitchUserInactiveList.count > 0
            {
                sortedInActiveArray = arrSwitchUserInactiveList.sorted { $0.firstName < $1.firstName }
                //  inactiveUserCount = arrSwitchUserInactiveList.count
            }
            arrSwitchUserList = sortedActiveArray + sortedInActiveArray
            
            DispatchQueue.main.async {
                self.hideHUD()
                self.tblUserList.reloadData()
                self.showCard()
            }
        }
    }
    /**
     Shows the card view and adjusts its position based on the content size and safe area layout guide.
     */
    func showCard() {
        var height: CGFloat = 0.0
        
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat {
            
            DispatchQueue.main.async {
                self.view.layoutIfNeeded()
                height = self.tblUserList.contentSize.height
                self.view.layoutIfNeeded()
                
                let calculatedValue = self.view.frame.size.height / 3.5
                let topValue = (safeAreaHeight - CGFloat((Int(height + 62))))
                
                if topValue <= calculatedValue {
                    self.topConstant.constant = calculatedValue
                } else {
                    self.topConstant.constant = topValue
                }
            }
            
            dimmerView.backgroundColor = UIColor(named: "theme_bg_color") // Colors.theme_white_color
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnCreateAccountClicked()
    {
        if let vc = appDelegate.getViewController("LoginVC", onStoryboard: "Main") as? LoginVC {
            vc.isFromVC = "SwitchAccount"
            vc.isAmazonLoginExists = self.isAmazonLoginExists
            vc.isAppleLoginExists = self.isAppleLoginExists
            self.dismiss(animated: true, completion: nil)
            navvc?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Tableview delegate methods
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(footerViewHeight)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension//92
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSwitchUserList.count
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerView.frame = CGRect(x: 0, y: 0, width: Int(self.tblUserList.frame.size.width), height:footerViewHeight)
        return footerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchAccountCell", for: indexPath) as? SwitchAccountCell{
            let objSwitchAccount = arrSwitchUserList[indexPath.row]
            cell.lblName.text = objSwitchAccount.firstName  + " " + objSwitchAccount.lastName
            if let imgUserURL = objSwitchAccount.imgUser as? String, imgUserURL != ""
            {
                cell.imgUser.setImage(imgUserURL)
            }
            if let loginType = objSwitchAccount.LoginType as? String
            {
                if loginType == LoginType.AppLogin.rawValue
                {
                    cell.imgLoginType.image = UIImage(named: "logo")
                    cell.containerView.backgroundColor = UIColor(named:"theme_bg_color")//Colors.theme_white_color
                }
                else if loginType == LoginType.AmazonLogin.rawValue
                {
                    cell.imgLoginType.image = UIImage(named: "amazon")
                    cell.containerView.backgroundColor = Colors.theme_light_yellow_color
                }
                else if loginType == LoginType.AppleLogin.rawValue
                {
                    cell.imgLoginType.image = UIImage(named: "apple-login")
                    cell.containerView.backgroundColor = UIColor(named:"theme_black_color") //Colors.theme_black_color
                }
            }
            cell.lblBoxID.text = "BOX ID : \(objSwitchAccount.boxID)"
            if objSwitchAccount.isDefault == true
            {
                cell.backgroundColor = Colors.theme_orange_bg_color
                cell.btnRemove.isHidden = true
                
                cell.lblName.textColor = UIColor(named: "theme_switch_ac_text_color")
                cell.lblBoxID.textColor = UIColor(named: "theme_switch_ac_text_color")
                cell.lblStatus.textColor = UIColor(named: "theme_switch_ac_text_color")
                
            }
            else
            {
                cell.backgroundColor = UIColor(named:"theme_bg_color") //Colors.theme_white_color
                cell.btnRemove.isHidden = false
                
                cell.lblName.textColor = UIColor(named: "theme_label_black_color")
                cell.lblBoxID.textColor = UIColor(named: "theme_label_black_color")
                cell.lblStatus.textColor = UIColor(named: "theme_label_black_color")
            }
            
            cell.lblStatus.text = objSwitchAccount.isActive == false ? "Inactive" : "Active"
            cell.btnRemove.tag = indexPath.row
            cell.btnRemove.addTarget(self, action: #selector(btnRemoveClicked(sender:)), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objSwitchAccount = arrSwitchUserList[indexPath.row]
        if objSwitchAccount.isDefault == true
        {
            self.showAlert(title: "", msg: "\(objSwitchAccount.boxID) is your Current User", vc: self)
        }
        else
        {
            let objSwitchAccount = self.arrSwitchUserList[indexPath.row]
            if objSwitchAccount.isActive == true
            {
                self.popupAlert(title: "", message:"Are you sure you want to switch to \(objSwitchAccount.boxID) account?", actionTitles: ["Yes","No"], actions:[{action1 in
                    self.callAPIForSwitchProfile(objSwitchAccount:objSwitchAccount)
                },{ action2 in
                    
                }])
            }
            else
            {
                if arrSwitchUserActiveList.count >= 5
                {
                    showAlert(title: "", msg: Messsages.msg_max_active_user_limit, vc: self)
                }
                else
                {
                    if let vc = appDelegate.getViewController("LoginVC", onStoryboard: "Main") as? LoginVC {
                        vc.boxID = objSwitchAccount.boxID
                        vc.loginType = objSwitchAccount.LoginType
                        vc.isAmazonLoginExists = self.isAmazonLoginExists
                        vc.isAppleLoginExists = self.isAppleLoginExists
                        self.dismiss(animated: true, completion: nil)
                        navvc?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    /**
        Handles the button click event when the remove button is clicked.        
    */
    @objc func btnRemoveClicked(sender:UIButton)
    {
        let objSwitchAccount = arrSwitchUserList[sender.tag]
        self.popupAlert(title: "", message: "Are you sure you want to remove \(objSwitchAccount.boxID) account?", actionTitles: ["Yes","No"], actions:[{action1 in
            if let status = objSwitchAccount.isActive as? Bool, status == true
            {
                //UserDefaults.standard.setValue("AppLogin", forKey: kTypeOfLogin) //need to change
                self.showHUD()
                self.callAPIForLogout(userID: objSwitchAccount.userID,isAction: "Remove",boxID: objSwitchAccount.boxID,token:objSwitchAccount.authToken )
            }
            else
            {
                let objManageCoreData = ManageCoreData()
                objManageCoreData.deleteRecord(entityName: kSwitchUserEntity, BoxId: objSwitchAccount.boxID) { (status) in
                    if status == true
                    {
                        DispatchQueue.main.async {
                            self.showHUD()
                        }
                        self.fetchRecords()
                    }
                    else
                    {
                        
                    }
                }
            }
        },{ action2 in
            
        }])
    }
}
class SwitchAccountCell : UITableViewCell
{
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var lblName : TapAndCopyLabel!
    @IBOutlet weak var lblBoxID : TapAndCopyLabel!
    @IBOutlet weak var lblStatus : UILabel!
    //    @IBOutlet weak var imgRight : UIImageView!
    @IBOutlet weak var imgLoginType : UIImageView!
    @IBOutlet weak var btnRemove : UIButton!
    @IBOutlet weak var containerView : UIView!
    override func awakeFromNib() {
        imgUser.layer.cornerRadius = imgUser.frame.size.height/2
        containerView.layer.cornerRadius = containerView.frame.size.height/2
        btnRemove.layer.cornerRadius = btnRemove.frame.size.height/2
        btnRemove.layer.borderColor = UIColor(named: "theme_orange_color")?.cgColor
        btnRemove.layer.borderWidth = 1
    }
}
