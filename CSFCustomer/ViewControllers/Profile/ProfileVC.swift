//
//  ProfileVC.swift
//  CSFCustomer
//
//  Created by Tops on 05/01/21.
//

import UIKit
import SwiftyJSON
import StoreKit

class ProfileVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tblProfile : UITableView!
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var imgContainer : UIView!
    @IBOutlet weak var lblUsername : UILabel!
    @IBOutlet weak var lblBoxID : TapAndCopyLabel!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var lblVersion : UILabel!
    @IBOutlet weak var btnSwitchAccount : UIButton!
    
    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    var arrSections = ["Profile Info","Manage Address","Manage Cards","Refer a Friend","Security Center","Rate the App", "Report","Account Closure"]
    var arrMoreActions = ["Edit","Add","","","","","",""]
    var selectedSectionIndex : Int  = 0
    var arrShippingMethod : [NSDictionary] = [NSDictionary]()
    var objDashboard = Dashboard()
    var arrSelectedSections : [Int] = [Int]()
    var isShowLoader : Bool = true
    var objProfileInfo = Profile()
    var objMyAddress = Address()
    var arrCardList : [StripeCardList] = [StripeCardList]()
    var arrAddressList : [AddressList] = [AddressList]()
    var cardCellHeight = 0
    var addressCellHeight = 0
    var refreshControl = UIRefreshControl()
    let dispatchGroup = DispatchGroup()
    
    var objMyCard = StripeCards()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Nand : New Feature Updates Code
        if appDelegate.isNeedToOpenMangeAddress == true {
            appDelegate.isNeedToOpenMangeAddress = false
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
                openMangeAddress()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(getUpdatedUserInfo(_:)), name: Notification.Name("UserInfoUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestCardDetail(_:)), name: Notification.Name("CardUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloaddata(_:)), name: NSNotification.Name(rawValue: "CollCellHeightUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestAddressDetail(_:)), name: Notification.Name("AddressUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestCardDetail(_:)), name: NSNotification.Name(rawValue: "CardDeleted"), object: nil)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tblProfile.addSubview(refreshControl)
        
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGesture(_:)))
        btnSwitchAccount.addGestureRecognizer(gestureRecognizer)
        btnSwitchAccount.isUserInteractionEnabled = true
        
        showUserInfoData()
        imgUser.layer.cornerRadius = imgUser.frame.size.height / 2
        imgUser.layer.masksToBounds = true
        imgContainer.layer.cornerRadius = imgContainer.frame.size.height / 2
        imgContainer.layer.masksToBounds = true
        topBarView.topBarBGColor()
        if let isParent = objUserInfo.isParent,isParent == "0"
        {
            //            arrSections.remove(at: 2)
            //            arrMoreActions.remove(at: 2)
        }
        for _ in 0..<arrSections.count
        {
            self.arrSelectedSections.append(0)
        }
        isShowLoader = true
        getProfileDetails(isShowLoader: true)
        getCardDetails(isShowLoader: true)
        getAddressDetails(isShowLoader: true)
        
        tblProfile.dataSource = self
        tblProfile.delegate = self
        tblProfile.tableFooterView = UIView()
        tblProfile.estimatedRowHeight = 100
        tblProfile.rowHeight = UITableView.automaticDimension
        lblVersion.text = "Version : \(kVersion)"
    }
    func showUserInfoData()
    {
        if  let decoded = UserDefaults.standard.object(forKey: kUserInfo) as? Data
        {
            //            This old code is now replaced by below new code
            //            if let userInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? UserInfo {
            //                objUserInfo = userInfo
            //                lblUsername.text = "Hi \(objUserInfo.firstName ?? "")"
            //                lblBoxID.text = "BOX ID : \(String(describing: objUserInfo.boxId ?? ""))"
            //                imgUser.setImage(objUserInfo.profileImagePath,placeHolder: UIImage(named: "default-image"))
            //                imgUser.contentMode = .scaleAspectFill
            //            }
            
            do {
                if let userInfo = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserInfo.self, from: decoded) {
                    // Handle the unarchived object
                    objUserInfo = userInfo
                    lblUsername.text = "Hi \(objUserInfo.firstName ?? "")"
                    lblBoxID.text = "BOX ID : \(String(describing: objUserInfo.boxId ?? ""))"
                    imgUser.setImage(objUserInfo.profileImagePath,placeHolder: UIImage(named: "default-image"))
                    imgUser.contentMode = .scaleAspectFill
                } else {
                    // Handle the case where unarchiving failed
                }
            } catch {
                // Handle the error
            }
        }
    }
    @objc func refresh(_ sender: AnyObject)
    {
        isShowLoader = true
        getProfileDetails(isShowLoader: false)
        getCardDetails(isShowLoader: false)
        getAddressDetails(isShowLoader: false)
    }
    @objc func getLatestCardDetail(_ notification:NSNotification)
    {
        getCardDetails(isShowLoader: true)
    }
    @objc func getLatestAddressDetail(_ notification:NSNotification)
    {
        getAddressDetails(isShowLoader: true)
        getProfileDetails(isShowLoader: true)
    }
    @objc func getUpdatedUserInfo(_ notification: NSNotification)
    {
        showUserInfoData()
        isShowLoader = true
        getProfileDetails(isShowLoader: true)
    }
    @objc func reloaddata(_ notification: NSNotification)
    {
        let height =  notification.userInfo?["height"] as? String ?? ""
        DispatchQueue.main.async {
            self.tblProfile.reloadData()
        }
    }
    
    // MARK: - getProfileDetails
    func getProfileDetails(isShowLoader :Bool)
    {
        dispatchGroup.enter() //new
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ProfieInfo, showLoader:isShowLoader) { (resultDict, status, message) in
            if status == true
            {
                self.refreshControl.endRefreshing()
                self.objProfileInfo = Profile(fromJson : JSON(resultDict))
                //when details change from portal ,fetch latest profile data and show
                if  let decoded = UserDefaults.standard.object(forKey: kUserInfo) as? Data
                {
                    //                  This old code is now replaced by below new code
                    //                  self.objUserInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserInfo
                    do {
                        // Handle the unarchived object
                        if let userInfo = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserInfo.self, from: decoded) {
                            self.objUserInfo = userInfo
                        } else {
                            // Handle the case where unarchiving failed
                        }
                    } catch {
                        // Handle the error
                    }
                    
                    self.objUserInfo.firstName = self.objProfileInfo.firstName ?? ""
                    self.objUserInfo.profileImagePath = self.objProfileInfo.profileImage ?? ""
                    self.objUserInfo.referralCode = self.objProfileInfo.referralCode ?? ""
                    
                    //                  This old code is now replaced by below new code
                    //                  let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: self.objUserInfo)
                    //                  UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                    //                  self.showUserInfoData()
                    
                    do {
                        // Handle the archived data
                        let encodedData = try NSKeyedArchiver.archivedData(withRootObject: self.objUserInfo, requiringSecureCoding: true)
                        UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                        self.showUserInfoData()
                    } catch {
                        // Handle the error
                        print("NSKeyedArchiver Error is : \(error.localizedDescription)")
                    }
                }
                DispatchQueue.main.async {
                    self.dispatchGroup.leave() //new
                    self.tblProfile.reloadData()
                }
            }
            else
            {
                self.refreshControl.endRefreshing()
                DispatchQueue.main.async {
                    self.dispatchGroup.leave() //new
                }
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - getCardDetails
    func getCardDetails(isShowLoader :Bool)
    {
        dispatchGroup.enter() //new
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.StripeCardList, showLoader:isShowLoader) { [self] (resultDict, status, message) in
            if status == true
            {
                objMyCard = StripeCards(fromJson: JSON(resultDict))
                
                if let cardExpiredMsg = objMyCard.cardExpiredMsg {
                    appDelegate.cardExpiredMsg = cardExpiredMsg
                    print("cardExpiredMsg is \(cardExpiredMsg)")
                }
                
                if let cardExpiryMsg = objMyCard.cardExpiryMsg {
                    appDelegate.cardExpiryMsg = cardExpiryMsg
                    print("cardExpiryMsg is \(cardExpiryMsg)")
                }
                
                self.arrCardList = objMyCard.list ?? []
                DispatchQueue.main.async {
                    self.dispatchGroup.leave() //new
                    self.tblProfile.reloadData()
                }
            }
            else
            {
                print("Result dict is \(resultDict)")

                DispatchQueue.main.async {
                    self.dispatchGroup.leave() //new
                }
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - getAddressDetails
    func getAddressDetails(isShowLoader :Bool)
    {
        dispatchGroup.enter() //new
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.MyAddress, showLoader:isShowLoader) { (resultDict, status, message) in
            if status == true
            {
                self.objMyAddress = Address(fromJson:JSON(resultDict))
                self.arrAddressList = self.objMyAddress.list ?? []
                DispatchQueue.main.async {
                    self.dispatchGroup.leave() //new
                    self.tblProfile.reloadData()
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.dispatchGroup.leave() //new
                }
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    @objc func handleLongPressGesture(_ recognizer: UIGestureRecognizer) {
        guard recognizer.state == .recognized else { return }
        
        let copiedText = lblUsername.text?.stringByRemovingAll(subStrings: ["BOX ID : ","Hi "]) ?? ""
        
        if copiedText.count > 0 {
            //print("Copied to clipboard")
            UIPasteboard.general.string = copiedText
            appDelegate.showToast(message: Messsages.msg_copied_to_clipboard, bottomValue: getSafeAreaValue())
        }
    }
}
extension ProfileVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1
        {
            return CGFloat(addressCellHeight)
        }
        else if indexPath.section == 2
        {
            return CGFloat(cardCellHeight)
        }
        else if arrSelectedSections[indexPath.section] == 1
        {
            return UITableView.automaticDimension
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let HeaderCellIdentifier : String  = "ProfileInfoHeaderCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCellIdentifier) as? ProfileInfoHeaderCell {
            cell.btnSection.addTarget(self, action: #selector(btnSectionClicked(sender:)), for: .touchUpInside )
            cell.btnSection.tag = section
            cell.lblSectionTitle.text = arrSections[section]
            cell.btnMore.tag = section
            cell.btnMore.setTitle(arrMoreActions[section], for: .normal)
            cell.btnMore.addTarget(self, action: #selector(btnMoreClicked(sender:)), for: .touchUpInside )
            if  arrSelectedSections[section] == 1
            {
                cell.imgUpDown.image = UIImage(named: "up")
            }
            else
            {
                cell.imgUpDown.image = UIImage(named: "down")
            }
            if section == 3 || section == 4 || section == 5 || section == 6 || section == 7 //(delete account)
            {
                cell.imgUpDown.isHidden = true
            }
            return cell.contentView
        }
        return UITableViewCell().contentView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoCell", for: indexPath) as? ProfileInfoCell {
                if let name = objProfileInfo.memberName, name != ""
                {
                    cell.lblName.text = name
                }
                if let email = objProfileInfo.emailAddress, email != ""
                {
                    cell.lblEmail.text = email
                }
                if let deliveryOption = objProfileInfo.deliveryOption, deliveryOption != ""
                {
                    cell.lblDeliveryOption.text = deliveryOption
                }
                else
                {
                    cell.lblDeliveryOption.text = "-"
                }
                if let nationalIDStr = objProfileInfo.nationalIdString ,nationalIDStr != ""
                {
                    cell.lblNationalID.text = nationalIDStr
                }
                if let natioanlIDString = objProfileInfo.nationalIdString , natioanlIDString == ""
                {
                    cell.lblDriverNoTitle.text = "ID Number"
                }
                else
                {
                    cell.lblDriverNoTitle.text = "\(objProfileInfo.nationalIdString ?? "") NO"
                }
                if let strNationalID = objProfileInfo.nationalIdNumber,strNationalID != ""
                {
                    cell.lblDriverPermitNo.text = strNationalID
                }
                if let phone = objProfileInfo.maskPhoneNo, phone != ""
                {
                    cell.lblPhoneNo.text =  "1 \(phone)"
                    cell.stackViewPhoneNo.isHidden = false
                }
                else
                {
                    cell.lblPhoneNo.text =  "-"
                    cell.stackViewPhoneNo.isHidden = true
                }
                if let mobile = objProfileInfo.maskMobileNo, mobile != ""
                {
                    cell.lblMobileNo.text = "1 \(mobile)"
                }
                else
                {
                    cell.lblMobileNo.text = "-"
                }
                if let strDOB = objProfileInfo.dateOfBirthString , strDOB != ""
                {
                    cell.lblDOB.text = strDOB
                }
                if let strGenderString = objProfileInfo.genderString, strGenderString != ""
                {
                    cell.lblGender.text = strGenderString
                }
                return cell
            }
        }
        else if indexPath.section == 1
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ManageAddressCell", for: indexPath) as? ManageAddressCell {
                objUserInfo = getUserInfo()
                cell.userID = objUserInfo.id ?? ""
                cell.objMyAddress = self.objMyAddress
                if arrAddressList.count > 0
                {
                    cell.addressCollectionView.isHidden = false
                    cell.lblNoAddressFound.isHidden = true
                    cell.setData(arrAddress: arrAddressList)
                }
                else
                {
                    cell.addressCollectionView.isHidden = true
                    cell.lblNoAddressFound.isHidden = false
                }
                return cell
            }
        }
        else if indexPath.section == 2
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ManageCreditCardCell", for: indexPath) as? ManageCreditCardCell {
                cell.cardExpiredMsg = appDelegate.cardExpiredMsg
                cell.cardExpiryMsg = appDelegate.cardExpiryMsg
                cell.currentVC = self
                
                objUserInfo = getUserInfo()
                cell.userID = objUserInfo.id ?? ""
                if arrCardList.count > 0 //need to change > 0
                {
                    cell.cardCollectionView.isHidden = false
                    cell.lblNoCardFound.isHidden = true
                    cell.setData(arrCards: arrCardList)
                }
                else
                {
                    cell.cardCollectionView.isHidden = true
                    cell.lblNoCardFound.isHidden = false
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    // MARK: - Custom Methods
    @objc func btnSectionClicked(sender:UIButton)
    {
        if arrSections[sender.tag] == "Manage User"
        {
            if let vc = appDelegate.getViewController("UserListVC", onStoryboard: "Profile") as? UserListVC {
                objUserInfo = getUserInfo()
                vc.userID = objUserInfo.id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if arrSections[sender.tag] == "Refer a Friend"
        {
            if let vc = appDelegate.getViewController("RefernEarnVC", onStoryboard: "Referral") as? RefernEarnVC {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if arrSections[sender.tag] == "Security Center"
        {
            if let vc = appDelegate.getViewController("SecurityCenterVC", onStoryboard: "Home") as? SecurityCenterVC {
                // vc.userID = objUserInfo.id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if arrSections[sender.tag] == "Rate the App"
        {
            print("Rate the Application called")
            self.openRateReview()
        }
        else if arrSections[sender.tag] == "Report"{
            if let vc = appDelegate.getViewController("ReportBugVC", onStoryboard:"Profile" ) as? ReportBugVC {
                vc.navvc = self.navigationController
                vc.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc,animated: true, completion: nil)
            }
        }
        else if arrSections[sender.tag] == "Account Closure"{
            
            if let deleteAccountUserInfoVC = appDelegate.getViewController("DeleteAccountUserInfoVC", onStoryboard: "Home") as? DeleteAccountUserInfoVC {
                deleteAccountUserInfoVC.refVC = self
                self.navigationController?.pushViewController(deleteAccountUserInfoVC, animated: true)
            }
        }
        else
        {
            if arrSelectedSections[sender.tag] == 0
            {
                if sender.tag == 1
                {
                    addressCellHeight = 260
                }
                else if sender.tag == 2
                {
                    cardCellHeight = 232
                }
                arrSelectedSections[sender.tag] = 1
            }
            else
            {
                if sender.tag == 1
                {
                    addressCellHeight = 0
                }
                else if sender.tag == 2
                {
                    cardCellHeight = 0
                }
                arrSelectedSections[sender.tag] = 0
            }
            UIView.performWithoutAnimation {
                self.tblProfile.beginUpdates()
                self.tblProfile.reloadSections(NSIndexSet(index: sender.tag) as IndexSet, with: .none)
                self.tblProfile.endUpdates()
            }
        }
    }
    
    func openMangeAddress(){
        addressCellHeight = 260
        arrSelectedSections[1] = 1

         UIView.performWithoutAnimation {
             self.tblProfile.beginUpdates()
             self.tblProfile.reloadSections(NSIndexSet(index: 1) as IndexSet, with: .none)
             self.tblProfile.endUpdates()
         }
    }
  
    @objc func btnMoreClicked(sender:UIButton)
    {
        if sender.tag == 0
        {
            openEditProfile()
        }
        else if sender.tag == 1
        {
            openAddAddress()
        }
    }
    func openEditProfile()
    {
        if let vc = appDelegate.getViewController("EditProfileVC", onStoryboard: "Profile") as? EditProfileVC {
            objUserInfo = getUserInfo()
            vc.isFromVC = "Profile"
            vc.arrNationalIDs = objProfileInfo.nationalIdList ?? []
            vc.objProfileInfo = objProfileInfo
            vc.userID = objUserInfo.id ?? ""
            vc.objMyAddress = objMyAddress
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func openAddAddress()
    {
        if let vc = appDelegate.getViewController("AddressVC", onStoryboard: "Profile") as? AddressVC {
            objUserInfo = getUserInfo()
            vc.objMyAddress = self.objMyAddress
            vc.userID = objUserInfo.id ?? ""
            vc.type = "Add"
            vc.addTitle = "Add New Address"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnMenuClicked()
    {
        self.revealViewController().revealToggle(animated: true)
    }
    @IBAction func btnSwitchAccountClicked()
    {
        if let vc = appDelegate.getViewController("SwitchAccountVC", onStoryboard:"Profile" ) as? SwitchAccountVC {
            vc.navvc = self.navigationController
            vc.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc,animated: true, completion: nil)
        }
    }
    @IBAction func btnLogoutClicked()
    {
        objUserInfo = getUserInfo()
        self.showAlertForLogout(title: "", msg: Messsages.msg_logout,userID:objUserInfo.id ?? "",type:"")
    }
}
class ProfileInfoHeaderCell : UITableViewCell
{
    @IBOutlet weak var lblSectionTitle : UILabel!
    @IBOutlet weak var btnSection : UIButton!
    @IBOutlet weak var btnMore : UIButton!
    @IBOutlet weak var imgUpDown : UIImageView!
}
class ProfileInfoCell :UITableViewCell
{
    @IBOutlet weak var lblName : TapAndCopyLabel!
    @IBOutlet weak var lblEmail : TapAndCopyLabel!
    @IBOutlet weak var lblDeliveryOption : UILabel!
    @IBOutlet weak var lblNationalID : UILabel!
    @IBOutlet weak var lblDriverPermitNo : UILabel!
    @IBOutlet weak var lblDriverNoTitle : UILabel!
    @IBOutlet weak var stackViewPhoneNo : UIStackView!
    @IBOutlet weak var lblPhoneNo : TapAndCopyLabel!
    @IBOutlet weak var lblMobileNo : TapAndCopyLabel!
    @IBOutlet weak var lblDOB : UILabel!
    @IBOutlet weak var lblGender : UILabel!
    override  func awakeFromNib() {
        lblEmail.isUserInteractionEnabled = false
    }
}
extension ProfileVC {
    
    func openRateReview(){
        let strURL : String = "https://apps.apple.com/us/app/csf-couriers/id1547257972"
        
        let productURL : URL = URL(string: strURL)!
        
        var components = URLComponents(url: productURL, resolvingAgainstBaseURL: false)
        
        // 2.
        components?.queryItems = [
            URLQueryItem(name: "action", value: "write-review")
        ]
        
        // 3.
        guard let writeReviewURL = components?.url else {
            return
        }
        
        // 4.
        UIApplication.shared.open(writeReviewURL)
    }
}
