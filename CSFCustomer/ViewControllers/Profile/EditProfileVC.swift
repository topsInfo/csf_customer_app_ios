//
//  EditProfileVC.swift
//  CSFCustomer
//
//  Created by Tops on 05/01/21.
//

import UIKit
import DropDown
import SwiftyJSON
import InputMask
protocol NotifyingMaskedTextFieldDelegateListener: class {
    func onEditingChanged(inTextField: UITextField)
}
class EditProfileVC: UIViewController ,MaskedTextFieldDelegateListener
{
    // MARK: - IBOutlets
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var imgContainer : UIView!
    @IBOutlet weak var txtFirstname : UITextField!
    @IBOutlet weak var txtMiddlename : UITextField!
    @IBOutlet weak var txtLastname : UITextField!
    @IBOutlet weak var txtCompanyName : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    @IBOutlet weak var txtMobile : UITextField!
    @IBOutlet weak var txtNationalID : UITextField!
    @IBOutlet weak var txtDOB : UITextField!
    @IBOutlet weak var txtDriverPermitNo : UITextField!
    @IBOutlet weak var DOBView : UIView!
    @IBOutlet weak var NationalIDView : UIView!
    @IBOutlet weak var lblPermitNoTitle : UILabel!
    @IBOutlet weak var addressView : UIStackView!
    @IBOutlet weak var txtAddress :UITextField!
    @IBOutlet weak var imgMale : UIImageView!
    @IBOutlet weak var imgFemale : UIImageView!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnSubmit : UIButton!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnRemove : UIButton!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var listener: MaskedTextFieldDelegate! //new mask
    @IBOutlet weak var viewAppleEmailInfo: UIView!
    
    //Promotional Code
    @IBOutlet weak var txtPromotionalCode : UITextField!
    @IBOutlet weak var stackViewPromotionalCode: UIStackView!
    
    @IBOutlet weak var stackViewFirstName: UIStackView!
    @IBOutlet weak var stackViewLastName: UIStackView!
    @IBOutlet weak var stackViewEmail: UIStackView!
    
    // MARK: - Global Variable
    var isImageRemoved : Int = 0
    var isFromVC : String = ""
    var isProfileRequired : Int = 0
    
    //Promotional Code
    var strPromotionalCode : String = ""
    var isValidPromotionalCodeFound : Bool = false

    lazy var targetFormatter : DateFormatter =
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
    lazy  var sourceFormatter : DateFormatter =
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            return formatter
        }()
    var arrTextFields : [UITextField] = [UITextField]()
    var arrPlaceHolder = [String]()
    var arrNationalIDs : [NationalIdList] = [NationalIdList]()
    let datePicker = UIDatePicker()
    let dropDown = DropDown()
    var selectedNationalID : Int  = -1
    var mimeType : String  = ""
    var uploadData = Data()
    var objProfileInfo  = Profile()
    var objMyAddress = Address()
    var userID : String  = ""
    var strGender : String = gender.Male.dispValue()
    var docFileName : String = ""
    var defaultPhoneValue : String = "1 868 XXX XXXX"
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(setAddress(_:)), name: NSNotification.Name(rawValue: "GetAddressData"), object: nil)
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2
        self.imgUser.clipsToBounds = true
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        imgMale.image = UIImage(named: "radio_btn")
        imgFemale.image = UIImage(named: "unselect")
        setPhoneMaskFormat()
        
        arrTextFields = [txtFirstname,txtMiddlename,txtLastname,txtCompanyName,txtEmail,txtPhone,txtMobile,txtDOB,txtNationalID,txtDriverPermitNo,txtAddress,txtPromotionalCode]
        arrPlaceHolder = ["First Name","Middle Name","Last Name","Company Name","Email","Phone","Mobile","Date of Birth","National ID Type","Permit No","Address","Promotional Code"]
        for (index,item) in arrTextFields.enumerated()
        {
            item.layer.cornerRadius = 2
            item.layer.masksToBounds = true
            item.layer.borderWidth = 1
            item.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
            item.placeholder = arrPlaceHolder[index]
            item.textColor = UIColor(named: "theme_black_color")//Colors.theme_black_color
            item.paddingView(xvalue: 10)
        }
        txtDOB.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed),selectorChange:#selector(dateValueChanged),formatType:DateFormatType.Day.dispValue(),from:"EditProfile")
        if isProfileRequired == 1
        {
            btnBack.isHidden = true
            btnCancel.isHidden = false
            showAlert(title: "", msg: Messsages.msg_fillup_profile, vc: self)
        }
        else
        {
            btnBack.isHidden = false
            btnCancel.isHidden = false
        }
        configureViews()
        self.enableDisableViewsForSignUpAfterAppleLogin()
        setNationalIDDropDown()
        if isFromVC == "Profile"
        {
            addressView.isHidden = true
            showData()
        }
        else if isFromVC == "Register"
        {
            addressView.isHidden = false
            getProfileDetails()
        }
        else
        {
            addressView.isHidden = true
            getProfileDetails()
        }
        //textfield delegates
        txtFirstname.delegate = self
        txtLastname.delegate = self
        txtMiddlename.delegate = self
        txtDriverPermitNo.delegate = self
        txtPromotionalCode.delegate = self
//        if let typeOfLogin = UserDefaults.standard.value(forKey: kTypeOfLogin) as? String,typeOfLogin == LoginType.AppleLogin.rawValue
//        {
//            self.viewAppleEmailInfo.isHidden = false
//        }else{
//            self.viewAppleEmailInfo.isHidden = true
//        }
    }
    
    func enableDisableViewsForSignUpAfterAppleLogin(){
        self.viewAppleEmailInfo.isHidden = true
        
        //We will disable FirstName, LastName and Email Address field while SignIn With Apple
        if let typeOfLogin = UserDefaults.standard.value(forKey: kTypeOfLogin) as? String,typeOfLogin == LoginType.AppleLogin.rawValue{

            if let firstname = objProfileInfo.firstName,firstname != ""
            {
                print("firstname is \(firstname)")
                stackViewFirstName.alpha = 0.5
                txtFirstname.isUserInteractionEnabled = false
            }else{
                stackViewFirstName.alpha = 1.0
                txtFirstname.isUserInteractionEnabled = true
            }
            
            if let lastname = objProfileInfo.lastName,lastname != ""
            {
                print("lastname is \(lastname)")
                stackViewLastName.alpha = 0.5
                txtLastname.isUserInteractionEnabled = false
            }else{
                stackViewLastName.alpha = 1.0
                txtLastname.isUserInteractionEnabled = true
            }
            
            if let email = objProfileInfo.emailAddress,email != ""
            {
                print("email is \(email)")
                stackViewEmail.alpha = 0.5
                txtEmail.isUserInteractionEnabled = false
            }else{
                stackViewEmail.alpha = 1.0
                txtEmail.isUserInteractionEnabled = true
            }
            
        }else{
            [stackViewFirstName, stackViewLastName, stackViewEmail].forEach { $0.alpha = 1.0 }
            [txtFirstname, txtLastname, txtEmail].forEach { $0.isUserInteractionEnabled = true }
        }
    }
    
    func enableViewsForEditProfileAfterAppleLogin(){
        if let typeOfLogin = UserDefaults.standard.value(forKey: kTypeOfLogin) as? String,typeOfLogin == LoginType.AppleLogin.rawValue{
            
            [stackViewFirstName, stackViewLastName].forEach { $0.alpha = 1.0 }
            [txtFirstname, txtLastname].forEach { $0.isUserInteractionEnabled = true }
            
            [stackViewEmail].forEach { $0.alpha = 0.5 }
            [txtEmail].forEach { $0.isUserInteractionEnabled = false }


        }else{
            [stackViewFirstName, stackViewLastName, stackViewEmail].forEach { $0.alpha = 1.0 }
            [txtFirstname, txtLastname, txtEmail].forEach { $0.isUserInteractionEnabled = true }
        }
    }
    
    func showData()
    {
        if let imgUserURL = objProfileInfo.profileImage, imgUserURL != ""
        {
            imgUser.setImage(imgUserURL)
            btnEdit.isHidden = true
            btnRemove.isHidden = false
        }
        else
        {
            imgUser.image = UIImage(named: "default-image")
            btnEdit.isHidden = false
            btnRemove.isHidden = true
        }
        if let firstname = objProfileInfo.firstName,firstname != ""
        {
            txtFirstname.text = firstname.capitalized
        }
        if let middlename = objProfileInfo.middleName,middlename != ""
        {
            txtMiddlename.text = middlename.capitalized
        }
        if let lastname = objProfileInfo.lastName,lastname != ""
        {
            txtLastname.text = lastname.capitalized
        }
        if let companyname = objProfileInfo.companyName,companyname != ""
        {
            txtCompanyName.text = companyname.capitalized
        }
        txtEmail.text = objProfileInfo.emailAddress ?? ""
        if let phone = objProfileInfo.maskPhoneNo , phone != ""
        {
            txtPhone.text = "1 \(objProfileInfo.maskPhoneNo ?? "")"
        }
        else
        {
            txtPhone.text = defaultPhoneValue
        }
        if let mobile = objProfileInfo.maskMobileNo , mobile != ""
        {
            txtMobile.text = "1 \(objProfileInfo.maskMobileNo ?? "")"
        }
        else
        {
            txtMobile.text = defaultPhoneValue
        }
        txtNationalID.text = objProfileInfo.nationalIdString ?? ""
        txtDriverPermitNo.text = objProfileInfo.nationalIdNumber ?? ""
        if txtNationalID.text == ""
        {
            lblPermitNoTitle.text = "National ID No"
        }
        else
        {
            lblPermitNoTitle.text = "\(String(describing: txtNationalID.text!)) No"
        }
        if let strNationalIDType = objProfileInfo.nationalIdType,strNationalIDType != ""
        {
            selectedNationalID = Int(strNationalIDType)!
        }
        if let strgender = objProfileInfo.gender , strgender == ""
        {
            strGender = gender.Male.dispValue()
        }
        if let strgender = objProfileInfo.genderString, strgender == "Male"
        {
            imgMale.image = UIImage(named: "radio_btn")
            imgFemale.image = UIImage(named: "unselect")
            strGender = gender.Male.dispValue()
        }
        else if let strgender = objProfileInfo.genderString, strgender == "Female"
        {
            imgFemale.image = UIImage(named: "radio_btn")
            imgMale.image = UIImage(named: "unselect")
            strGender = gender.Female.dispValue()
        }
        if let dob = objProfileInfo.dateOfBirth
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dt = formatter.date(from: dob)
            if dt != nil
            {
                formatter.dateFormat = "dd-MMM-yyyy"
                txtDOB.text = formatter.string(from: dt!)
            }
        }
        txtAddress.text = UserDefaults.standard.value(forKey: kDeliveryOption) as? String ?? ""
        
        //Profmotional Code
        managePromotionalCodeInputField()
//        stackViewPromotionalCode.isHidden = false
        if isProfileRequired == 0 {
            self.enableViewsForEditProfileAfterAppleLogin()
        }else{
            self.enableDisableViewsForSignUpAfterAppleLogin()
        }
    }
    func managePromotionalCodeInputField(){
        txtPromotionalCode.keyboardType = .asciiCapable
        txtPromotionalCode.autocapitalizationType = .allCharacters

        if isProfileRequired == 1 && objProfileInfo.isReferalApplied == 0{
            stackViewPromotionalCode.isHidden = false
        }else{
            stackViewPromotionalCode.isHidden = true
        }
    }
    func configureViews()
    {
        topBarView.topBarBGColor()
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2
        self.imgUser.clipsToBounds = true
        
        txtDOB.layer.borderColor = Colors.clearColor.cgColor
        txtDOB.layer.borderWidth = 0
        
        txtNationalID.layer.borderColor = Colors.clearColor.cgColor
        txtNationalID.layer.borderWidth = 0
        
        DOBView.layer.cornerRadius = 2
        DOBView.layer.masksToBounds = true
        DOBView.layer.borderWidth = 1
        DOBView.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor //Colors.theme_lightgray_color.cgColor
        
        NationalIDView.layer.cornerRadius = 2
        NationalIDView.layer.masksToBounds = true
        NationalIDView.layer.borderWidth = 1
        NationalIDView.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor //Colors.theme_lightgray_color.cgColor
        
        btnCancel.layer.cornerRadius = 2
        btnCancel.layer.masksToBounds = true
        btnCancel.layer.borderWidth = 1
        btnCancel.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor //Colors.theme_lightgray_color.cgColor
        
        btnSubmit.layer.cornerRadius = 2
        btnSubmit.layer.masksToBounds = true
        
        txtFirstname.autocapitalizationType = .words
        txtLastname.autocapitalizationType = .words
        txtMiddlename.autocapitalizationType = .words
        txtCompanyName.autocapitalizationType = .words
    }
    @objc func setAddress(_ notification:NSNotification)
    {
        txtAddress.text = notification.userInfo?["deliveryOption"] as? String ?? ""
    }
    func setPhoneMaskFormat()
    {
        listener.affinityCalculationStrategy = .wholeString
        listener.primaryMaskFormat = "1 [000] [000] [0000]"
        listener.affineFormats = [
            "1 [000] [000] [0000]",
        ]
    }
    
    // MARK: - getProfileDetails
    func getProfileDetails()
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ProfieInfo, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.objProfileInfo = Profile(fromJson : JSON(resultDict))
                self.arrNationalIDs = self.objProfileInfo.nationalIdList ?? []
                self.dropDown.dataSource = self.arrNationalIDs.map({$0.name})
                self.dropDown.reloadAllComponents()
                self.showData()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    func setNationalIDDropDown()
    {
        dropDown.width = UIScreen.main.bounds.size.width - 40
        dropDown.shadowRadius = 0

        dropDown.direction = .any
        dropDown.anchorView = self.txtNationalID // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x:0 , y:(txtNationalID.bounds.height))
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)

        dropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDown.separatorColor = UIColor(named:"theme_lightgray_color")!//Colors.theme_lightgray_color
        dropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDown.textColor = UIColor(named:"theme_text_color")!
        dropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        
        dropDown.dataSource = arrNationalIDs.map({$0.name})
        dropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                let objNationalID = self?.arrNationalIDs[index]
                self?.selectedNationalID  = objNationalID?.id ?? 0
                self?.lblPermitNoTitle.text = "\(item) No"
                self?.txtNationalID.text = item
                self?.txtDriverPermitNo.text = ""
                self?.txtDriverPermitNo.placeholder = "Enter \(item) no"
                self?.dropDown.hide()
            }
        }
    }
    
    @objc func dateValueChanged(_ datePicker: UIDatePicker) {
        //        let formatter = DateFormatter()
        //        formatter.dateFormat = "dd-MMM-yyyy"
        //        txtDOB.text = formatter.string(from: datePicker.date)
        //        self.view.endEditing(true)
    }
    @objc func doneButtonPressed()
    {
        if let  datePicker = txtDOB.inputView as? UIDatePicker {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            txtDOB.text = formatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }
    // MARK: - IBAction methods
    @IBAction func btnAddAddressClicked(_ sender:UIButton)
    {
        if UserDefaults.standard.value(forKey: kIsAddresAdded) == nil
        {
            if let vc = appDelegate.getViewController("AddressVC", onStoryboard: "Profile") as? AddressVC {
                vc.type = "RegisterForAppleorAmazon"
                vc.addTitle = "Add New Address"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else
        {
            appDelegate.showToast(message: Messsages.msg_address_already_added, bottomValue: getSafeAreaValue())
        }
        
    }
    @IBAction func btnGenderClicked(_ sender:UIButton)
    {
        if sender.tag == 101
        {
            imgMale.image = UIImage(named: "radio_btn")
            imgFemale.image = UIImage(named: "unselect")
            strGender = gender.Male.dispValue()
        }
        else
        {
            imgFemale.image = UIImage(named: "radio_btn")
            imgMale.image = UIImage(named: "unselect")
            strGender = gender.Female.dispValue()
        }
    }
    @IBAction func btnNationalIDClicked()
    {
        self.dropDown.show()
    }
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnPhotoEditClicked()
    {
        let attributedString = NSAttributedString(string: "Please select option", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15), //your font here
            NSAttributedString.Key.foregroundColor :Colors.theme_gray_color
        ])
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(attributedString, forKey: "attributedTitle")
        
        let takePhotoAction = UIAlertAction(title: "Take a Photo" , style: .default) { (_ action) in
            self.openCamera()
        }
        let photoAction = UIAlertAction(title: "Photo Gallery" , style: .default) { (_ action) in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel" , style: .cancel) { (_ action) in
            self.dismiss(animated: true, completion: nil)
        }
        takePhotoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        photoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(takePhotoAction)
        alert.addAction(photoAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnRemoveClicked()
    {
        isImageRemoved = 1
        imgUser.image = UIImage(named: "default-image")
        uploadData = Data()
        self.mimeType = "image/png"
        btnEdit.isHidden = false
        btnRemove.isHidden = true
    }
    @IBAction func btnCancelClicked()
    {
        if isProfileRequired == 1{
            let objUserInfo = getUserInfo()
            showAlert(title: "", msg: Messsages.msg_cancel_process, viewController: self) { isSuccess in
                if isSuccess{
                    self.callAPIForLogout(userID: objUserInfo.id ?? "", isAction: "", boxID: objUserInfo.boxId ?? "", token: objUserInfo.token ?? "")
                }
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnSubmitClicked()
    {
        self.view.endEditing(true)
        if isValidData()
        {
            self.strPromotionalCode = txtPromotionalCode.text ?? ""
            isValidPromotionalCodeFound = false
            
            if self.strPromotionalCode.count > 0 && isProfileRequired == 1{
                if isValidatePromotionalCode(promotionalCode: self.strPromotionalCode) {
                    appDelegate.callAPIForValidatePromotionalCode(promotionalCode: self.strPromotionalCode, fromVC: self) { [self] resultDict, status, message in
                        if status {
                            isValidPromotionalCodeFound = true
                            callAPIToEditProfile()
                        }else{
                            appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                        }
                    }
                }
            }
            else{
                callAPIToEditProfile()
            }
        }
    }
    // MARK: - Custom methods
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            
            PermissionManager.shared.requestCameraPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.camera
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            PermissionManager.shared.requestPhotoPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - callAPIToEditProfile
    func callAPIToEditProfile()
    {
        let fieldName = "profile_image"
        var date = ""
        let dob = sourceFormatter.date(from: txtDOB.text!)
        if dob != nil
        {
            date = targetFormatter.string(from: dob!)
        }
        var strPhone = txtPhone.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if strPhone == defaultPhoneValue || strPhone?.count == 5 || strPhone == ""
        {
            strPhone = ""
        }
        else
        {
            strPhone = String(strPhone!.dropFirst(2)) //new change
        }
        //new change
        var strMobileNo = txtMobile.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        strMobileNo = String(strMobileNo!.dropFirst(2))
        //over
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["first_name"] = txtFirstname.text ?? ""
        params["middle_name"] = txtMiddlename.text ?? ""
        params["last_name"] = txtLastname.text ?? ""
        params["company_name"] = txtCompanyName.text ?? ""
        params["gender"] = strGender
        params["date_of_birth"] =  date
        params["mobile_no"] = strMobileNo //txtMobile.text ?? ""
        params["phone_no"] = strPhone
        params["national_id_type"] = selectedNationalID
        params["national_id_number"] = txtDriverPermitNo.text ?? ""
        params["image_removed"] = isImageRemoved
        params["profile_required"] = isProfileRequired
        params["email_address"] = txtEmail.text ?? ""
        
        if isValidPromotionalCodeFound {
            params["promotional_code"] = self.strPromotionalCode
        }
        
        print("Params are \(params)")
        self.showHUDOnView(view: self.view)
        URLManager.shared.URLCallMultipartData(method: .post, parameters: params, header: true, url: APICall.SaveProfile, showLoader: false, WithName: fieldName, docFileName: self.docFileName,uploadData: uploadData, mimeType: self.mimeType) { (resultDict, status, message) in
            
            self.hideHUDFromView(view: self.view)
            if status == true
            {
                print("Response dict is \(resultDict)")
                if  let decoded = UserDefaults.standard.object(forKey: kUserInfo) as? Data
                {
                    //                    This old code is now replaced by below new code
                    //                    let objUserInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserInfo
                    //                    objUserInfo.firstName = (resultDict as NSDictionary).value(forKey: "first_name") as? String ?? ""
                    //                    objUserInfo.lastName = (resultDict as NSDictionary).value(forKey: "last_name") as? String ?? ""
                    //                    objUserInfo.companyName = (resultDict as NSDictionary).value(forKey: "company_name") as? String ?? ""
                    //                    objUserInfo.profileImagePath = (resultDict as NSDictionary).value(forKey: "profile_image_path") as? String ?? ""
                    //                    objUserInfo.emailAddress = (resultDict as NSDictionary).value(forKey: "email_address") as? String ?? ""
                    //                    objUserInfo.profileRequired = 0 //change to zero
                    //                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: objUserInfo)
                    //                    UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                    //                    if let typeOfLogin = UserDefaults.standard.value(forKey: kTypeOfLogin)
                    //                    {
                    //                        self.saveCoreData(objUserInfo:objUserInfo,TypeOfLogin:typeOfLogin as! String)
                    //                    }
                    
                    do {
                        // Handle the unarchived object
                        if let objUserInfo = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserInfo.self, from: decoded) {
                            objUserInfo.firstName = (resultDict as NSDictionary).value(forKey: "first_name") as? String ?? ""
                            objUserInfo.lastName = (resultDict as NSDictionary).value(forKey: "last_name") as? String ?? ""
                            objUserInfo.companyName = (resultDict as NSDictionary).value(forKey: "company_name") as? String ?? ""
                            objUserInfo.profileImagePath = (resultDict as NSDictionary).value(forKey: "profile_image_path") as? String ?? ""
                            objUserInfo.emailAddress = (resultDict as NSDictionary).value(forKey: "email_address") as? String ?? ""
                            objUserInfo.profileRequired = 0 //change to zero
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: objUserInfo, requiringSecureCoding: true)
                            UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                            
                            if let typeOfLogin = UserDefaults.standard.value(forKey: kTypeOfLogin)
                            {
                                self.saveCoreData(objUserInfo:objUserInfo,TypeOfLogin:typeOfLogin as! String)
                            }
                            
                        } else {
                            // Handle the case where unarchiving failed
                        }
                    } catch {
                        // Handle the error
                        print("NSKeyedArchiver Error is : \(error.localizedDescription)")
                    }
                }
                
                //new change
                if UserDefaults.standard.value(forKey: "invitedby") != nil
                {
                    UserDefaults.standard.removeObject(forKey: "invitedby")
                }
                if UserDefaults.standard.value(forKey: kIsAddresAdded) != nil
                {
                    UserDefaults.standard.removeObject(forKey: kIsAddresAdded)
                    if UserDefaults.standard.value(forKey: kDeliveryOption) != nil
                    {
                        UserDefaults.standard.removeObject(forKey: kDeliveryOption)
                    }
                }
                //over
                self.showAlertForSaveProfile(title: "", msg: message)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - showAlertForSaveProfile
    func showAlertForSaveProfile(title:String,msg:String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            NotificationCenter.default.post(name: Notification.Name("UserInfoUpdated"), object: nil)
            if self.isProfileRequired == 1
            {
                self.goToHomeVC()
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
            
        }
        okAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtFirstname.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_firstname, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtLastname.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_lastname, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtMobile.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_mobile, bottomValue: getSafeAreaValue())
            return false
        }
        else if txtMobile.text == defaultPhoneValue
        {
            appDelegate.showToast(message: Messsages.msg_mobile_length, bottomValue: getSafeAreaValue())
            return false
        }
        else if self.txtMobile.text!.count < 14 //16
        {
            appDelegate.showToast(message: Messsages.msg_mobile_length, bottomValue: getSafeAreaValue())
            return false
        }
        else if (txtPhone.text!.trimmingCharacters(in: .whitespaces).count > 5 && txtPhone.text!.trimmingCharacters(in: .whitespaces).count < 14) // >7 && , 16
        {
            appDelegate.showToast(message: Messsages.msg_phone_length, bottomValue: getSafeAreaValue())
            return false
        }
        else if isProfileRequired == 1
        {
            if isCheckNull(strText: txtEmail.text!)
            {
                appDelegate.showToast(message: Messsages.msg_enter_email, bottomValue: getSafeAreaValue())
                return false
            }
            else if !isValidEmail(testStr: txtEmail.text!)
            {
                appDelegate.showToast(message: Messsages.msg_invalid_email, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtDOB.text!)
            {
                appDelegate.showToast(message: Messsages.msg_select_dob, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtNationalID.text!)
            {
                appDelegate.showToast(message: Messsages.msg_select_nationalId, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtDriverPermitNo.text!)
            {
                appDelegate.showToast(message: Messsages.msg_enter_permitno + " " + lblPermitNoTitle.text!, bottomValue: getSafeAreaValue())
                return false
            }
            else if isCheckNull(strText: txtAddress.text!)
            {
                appDelegate.showToast(message: Messsages.msg_enter_addresses, bottomValue: getSafeAreaValue())
                return false
            }
        }
        else if isCheckNull(strText: txtDOB.text!)
        {
            appDelegate.showToast(message: Messsages.msg_select_dob, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtNationalID.text!)
        {
            appDelegate.showToast(message: Messsages.msg_select_nationalId, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtDriverPermitNo.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_permitno + " " + lblPermitNoTitle.text!, bottomValue: getSafeAreaValue())
            return false
        }
        
        return true
    }
}
extension EditProfileVC : UITextFieldDelegate
{
    open func textField(
        _ textField: UITextField,
        didFillMandatoryCharacters complete: Bool,
        didExtractValue value: String
    ) {
        //    print(value)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                return true
            }
        }
        if textField == txtDriverPermitNo
        {
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_")
            if newLength <= 50
            {
                if string.rangeOfCharacter(from: set) != nil
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        if textField == txtFirstname || textField == txtMiddlename || textField == txtLastname ||  textField == txtDriverPermitNo
        {
            return newLength <= 50
        }
        else if textField == txtPhone || textField == txtMobile
        {
            return newLength <= 10
        }
        else if textField == txtPromotionalCode {
            if string == "" {
                // User presses backspace
                textField.deleteBackward()
            } else {
                // User presses a key or pastes
                textField.insertText(string.uppercased())
            }
            // Do not let specified text range to be changed
            return false
        }
        return true
    }
}
extension EditProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
        {
            
            switch url.pathExtension
            {
            case "jpg","jpeg":
                self.mimeType = "image/jpeg"
                break
            case "png":
                self.mimeType = "image/png"
                break
            default:
               // print("No file")
                appDelegate.showToast(message: Messsages.msg_image_type, bottomValue: getSafeAreaValue())
                return
            }
        }
        if let pickedImage = info[.editedImage] as? UIImage
        {
            if self.mimeType == ""
            {
                self.mimeType = "image/png"
            }
            isImageRemoved = 0
            self.docFileName = "Profile-img.png"
            imgUser.image = pickedImage
            let thumb1 = pickedImage.resized(withPercentage: 0.1)
            btnEdit.isHidden = true
            btnRemove.isHidden = false
            
            DispatchQueue.global(qos: .background).async {
                if self.mimeType == "image/jpeg"
                {
                    self.uploadData = pickedImage.jpegData(compressionQuality: 0.3)!
                }
                else if self.mimeType == "image/png"
                {
                    self.uploadData = (thumb1?.pngData())!
                    
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
