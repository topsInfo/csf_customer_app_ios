//
//  UpdateCardDetailVC.swift
//  CSFCustomer
//
//  Created by iMac on 23/05/22.
//

import UIKit
import DropDown

protocol cardUpdatedDelegate {
    func cardUpdated()
}

class UpdateCardDetailVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var dimmerView : UIView!
    @IBOutlet weak var topConstant : NSLayoutConstraint!
    @IBOutlet weak var viewUpdateCardDetail: UIView!
    @IBOutlet weak var txtMonth: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var viewMonth: UIView!
    @IBOutlet weak var viewYear: UIView!
    
    // MARK: - Global Variables
    let dropDownMonth = DropDown()
    let dropDownYear = DropDown()
    let arrMonths : [String] = ["1","2","3","4","5","6","7","8","9","10","11","12"]
    var arrYears : [String] = [String]()
    var objCard : StripeCardList = StripeCardList()
    var delegate : cardUpdatedDelegate!
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            DispatchQueue.main.async { [self] in
                //   self.tblUserList.reloadData()
                self.view.layoutIfNeeded()
                
                let topHeight = CGFloat(safeAreaHeight) - viewUpdateCardDetail.frame.size
                    .height
                topConstant.constant = topHeight
            }
            
            dimmerView.backgroundColor = UIColor(named:"theme_bg_color")//Colors.theme_white_color
            
        }
        
        let intCurrentYear = Date().get(.year)
        let intNext10thYear = intCurrentYear + 10
        arrYears = (intCurrentYear...intNext10thYear).map { String($0) }
        txtMonth.text = "Month"
        txtYear.text = "Year"
        setMonthDropDown()
        setYearDropDown()
    }
    
    // MARK: - IBAction Methods
    @IBAction func btnMonthDropDownClicked(_ sender: UIButton) {
        
        dropDownMonth.anchorView = viewMonth
        dropDownMonth.bottomOffset = CGPoint(x: 0, y:(txtMonth.bounds.size.height))
        dropDownMonth.topOffset = CGPoint(x: 0, y:-(dropDownMonth.anchorView?.plainView.bounds.height)!)
        
        dropDownMonth.dataSource = arrMonths
        dropDownMonth.reloadAllComponents()
        dropDownMonth.show()

    }
    
    @IBAction func btnYearDropDownClicked(_ sender: UIButton) {
        dropDownYear.anchorView = viewYear
        dropDownYear.bottomOffset = CGPoint(x: 0, y:(txtYear.bounds.size.height))
        dropDownYear.topOffset = CGPoint(x: 0, y:-(dropDownYear.anchorView?.plainView.bounds.height)!)
        
        dropDownYear.dataSource = arrYears
        dropDownYear.reloadAllComponents()
        dropDownYear.show()

    }
    
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnUpdateClicked(_ sender: UIButton) {
        if isValidData(){
            callAPIToUpdateCard()
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if txtMonth.text == "Month" {
            appDelegate.showToast(message: Messsages.msg_select_month_and_year, bottomValue: getSafeAreaValue())
            return false
        }else if txtYear.text == "Year" {
            appDelegate.showToast(message: Messsages.msg_select_month_and_year, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
}

extension UpdateCardDetailVC {
    func setMonthDropDown()
    {
        dropDownMonth.width = viewMonth.frame.size.width
        dropDownMonth.shadowRadius = 0
        dropDownMonth.direction = .any
        dropDownMonth.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDownMonth.separatorColor = UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        dropDownMonth.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDownMonth.textColor = UIColor(named:"theme_text_color")!
        dropDownMonth.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDownMonth.selectedTextColor = Colors.theme_dropdown_selection_text_color
        dropDownMonth.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                
                self?.txtMonth.text = self?.arrMonths[index]
                self?.dropDownMonth.hide()
                    
            }
        }
    }
    
    func setYearDropDown()
    {
        dropDownYear.width = viewYear.frame.size.width
        dropDownYear.shadowRadius = 0
        dropDownYear.direction = .any
        dropDownYear.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDownYear.separatorColor = UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        dropDownYear.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDownYear.textColor = UIColor(named:"theme_text_color")!
        dropDownYear.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDownYear.selectedTextColor = Colors.theme_dropdown_selection_text_color
        dropDownYear.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                
                self?.txtYear.text = self?.arrYears[index]
                self?.dropDownYear.hide()
                    
            }
        }
    }
}

extension UpdateCardDetailVC {
    
    // MARK: - callAPIToUpdateCard
    func callAPIToUpdateCard()
    {
        var objUserInfo = UserInfo()
        objUserInfo = getUserInfo()

        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["card_id"] = objCard.cardId
        params["expiry_month"] = txtMonth.text
        params["expiry_year"] = txtYear.text
        print("params is \(params)")
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.UpdateStripeCard, showLoader:true) { (resultDict, status, message) in
            print("Respons is \(resultDict)")
            if status == true
            {
                self.dismiss(animated: true) { [self] in
                    delegate.cardUpdated()
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}
