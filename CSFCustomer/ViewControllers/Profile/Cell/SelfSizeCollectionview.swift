//
//  SelfSizeCollectionview.swift
//  CSFCustomer
//
//  Created by Tops on 10/01/21.
//

import UIKit

class SelfSizeCollectionview: UICollectionView {

    override func reloadData() {
           super.reloadData()
           self.invalidateIntrinsicContentSize()
       }

       override var intrinsicContentSize: CGSize {
           let s = self.collectionViewLayout.collectionViewContentSize
           return CGSize(width: max(s.width, 1), height: max(s.height,1))
       }

}
