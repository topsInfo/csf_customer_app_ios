//Original version
//  ManageAddressCell.swift
//  CSFCustomer
//
//  Created by Tops on 05/01/21.
//

import UIKit
import SWRevealViewController
class ManageAddressCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var addressCollectionView : UICollectionView!
    @IBOutlet weak var lblNoAddressFound : UILabel!
    @IBOutlet weak var collecHeightConst: NSLayoutConstraint!
    @IBOutlet weak var pageControl : UIPageControl!
    
    // MARK: - Global Variable
    var arrAddressList :[AddressList] = [AddressList]()
    var userID : String = ""
    var objMyAddress = Address()
    var heightForAddress : CGFloat = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        addressCollectionView.dataSource = self
        addressCollectionView.delegate = self
        addressCollectionView.contentInsetAdjustmentBehavior = .never
    }
    // MARK: - IBOutlets
    func setData(arrAddress:[AddressList])
    {
        arrAddressList = arrAddress
        if arrAddressList.count > 1
        {
            self.pageControl.isHidden = false
            self.pageControl.numberOfPages = arrAddressList.count
        }
        else
        {
            self.pageControl.isHidden = true
        }
        DispatchQueue.main.async { [self] in
            self.addressCollectionView.reloadData()
            if arrAddressList.count > 1 {
                self.addressCollectionView?.scrollToItem(at: IndexPath(row: 0, section: 0),at: .top, animated: false)
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
extension ManageAddressCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAddressList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"AddressCell", for: indexPath) as? AddressCell{
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteClicked(sender:)), for: .touchUpInside)
            cell.btnDefaultAddress.tag = indexPath.row
            cell.btnDefaultAddress.addTarget(self, action: #selector(btnDefaultAddressClicked(sender:)), for: .touchUpInside)
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(btnEditClicked(sender:)), for: .touchUpInside)
            if arrAddressList.count > 0
            {
                let objAddressList = arrAddressList[indexPath.row]
                cell.lblAddTitle.text = objAddressList.title ?? ""
                cell.lblAddress.text = objAddressList.address ?? ""
                if objAddressList.isDefault == "1"
                {
                    cell.imgCheck.image = UIImage(named: "select")
                    cell.btnDelete.isHidden = true
                }
                else
                {
                    cell.imgCheck.image = UIImage(named: "unselect")
                    cell.btnDelete.isHidden = false
                }
                
                let isSunday = objAddressList.isSunday ?? ""
                let isSaturday = objAddressList.isSaturday ?? ""
                let isWeekdays = objAddressList.isWeekdays ?? ""
                
                if ((isSunday == "1" || isSaturday == "1" || isWeekdays == "1") && (objAddressList.isDefault == "1")){
                    cell.viewWeekendPreference.isHidden = false
                }else{
                    cell.viewWeekendPreference.isHidden = true
                }
                
                if isSunday == "1"{
                    cell.lblSunday.isHidden = false
                }else{
                    cell.lblSunday.isHidden = true
                }
                
                if isSaturday == "1"{
                    cell.lblSaturday.isHidden = false
                }else{
                    cell.lblSaturday.isHidden = true
                }
                
                if isWeekdays == "1"{
                    cell.lblWeekdays.isHidden = false
                }else{
                    cell.lblWeekdays.isHidden = true
                }
            }
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
        return CGSize(width: addressCollectionView.frame.size.width, height: 200)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let roundedIndex = scrollView.contentOffset.x / scrollView.frame.size.width
        self.pageControl.currentPage = Int(roundedIndex)
    }
    
    func getHeightForLable(labelWidth: CGFloat, numberOfLines: Int = 0, labelText: String, labelFont: UIFont) -> CGFloat {
        let tempLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = numberOfLines
        tempLabel.text = labelText
        tempLabel.font = labelFont
        tempLabel.sizeToFit()
        return tempLabel.frame.height
    }
    // MARK: - Custom methods
    @objc func btnDefaultAddressClicked(sender:UIButton)
    {
        callAPIForDefaultAddress(index:sender.tag)
    }
    @objc func btnEditClicked(sender:UIButton)
    {
       let objAddress = arrAddressList[sender.tag]
        if let vc = appDelegate.getViewController("AddressVC", onStoryboard: "Profile") as? AddressVC {
            vc.type = "Edit"
            vc.addTitle = "Edit Address"
            vc.addressID = objAddress.id ?? ""
            vc.userID = self.userID
            vc.objMyAddress = self.objMyAddress
            if let navvc = appDelegate.topNavigation()
            {
                navvc.pushViewController(vc, animated: true)
            }
        }
    }
    @objc func btnDeleteClicked(sender:UIButton)
    {
      // showAlertToDeleteAddress(title: "", msg:Messsages.msg_delete_address, index:sender.tag)
        if arrAddressList.count > 0
        {
            let objAddress = arrAddressList[sender.tag]
            if let popupVC = appDelegate.getViewController("DeletePopupVC", onStoryboard: "PreAlert") as? DeletePopupVC {
                popupVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
                popupVC.userID = userID
                popupVC.addressID = objAddress.id ?? ""
                popupVC.isFromVC = "DeleteAddress"
                popupVC.modalPresentationStyle = .overFullScreen
                if let rootvc = UIApplication.shared.windows.first?.rootViewController
                {
                    rootvc.present(popupVC,animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - callAPIForDefaultAddress
    func callAPIForDefaultAddress(index:Int)
    {
        let objAddress = arrAddressList[index]
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["address_id"] = objAddress.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.DefaultAddress, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true)
                NotificationCenter.default.post(name: Notification.Name("AddressUpdated"), object: nil)
            }
            else
            {
               appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}

class AddressCell : UICollectionViewCell
{
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var containerStackView : UIStackView!
    @IBOutlet weak var lblAddTitle : UILabel!
    @IBOutlet weak var lblAddress : TapAndCopyLabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var imgCheck : UIImageView!
    @IBOutlet weak var btnDefaultAddress : UIButton!
    @IBOutlet weak var viewWeekendPreference : UIView!
    
    @IBOutlet weak var lblWeekdays : UILabel!
    @IBOutlet weak var lblSaturday : UILabel!
    @IBOutlet weak var lblSunday : UILabel!

    override func awakeFromNib() {
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = Colors.theme_lightgray_color.cgColor
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
    }
}
