//
//  ManageCreditCardCell.swift
//  CSFCustomer
//
//  Created by Tops on 07/01/21.
//

import UIKit

class ManageCreditCardCell: UITableViewCell, cardUpdatedDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var cardCollectionView : UICollectionView!
    @IBOutlet weak var lblNoCardFound : UILabel!
    @IBOutlet weak var pageControl : UIPageControl!
    
    // MARK: - Global Variable
    var arrCardList : [StripeCardList] = [StripeCardList]()
    var userID : String = ""
    var cardFormat : String  = "XXXX XXXX XXXX"
    var cardExpiryMsg : String = ""
    var cardExpiredMsg : String = ""
    var currentVC : ProfileVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblNoCardFound.isHidden = true
        cardCollectionView.dataSource = self
        cardCollectionView.delegate = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    // MARK: - Custom methods
    func setData(arrCards:[StripeCardList])
    {
        arrCardList = arrCards
        if arrCardList.count > 1
        {
            self.pageControl.isHidden = false
            self.pageControl.numberOfPages = arrCardList.count
        }
        else
        {
            self.pageControl.isHidden = true
        }
        DispatchQueue.main.async {
            self.cardCollectionView.reloadData()
        }
    }
    
    func cardUpdated() {
        currentVC.getCardDetails(isShowLoader: true)
    }
}
extension ManageCreditCardCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCardList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"CardCell", for: indexPath) as? CardCell{
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteClicked(sender:)), for: .touchUpInside)
            
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(btnEditClicked(sender:)), for: .touchUpInside)
            
            cell.btnAlert.tag = indexPath.row
            cell.btnAlert.addTarget(self, action: #selector(btnAlertClicked(sender:)), for: .touchUpInside)
            
            if arrCardList.count > 0
            {
                let objCardList = arrCardList[indexPath.row]
                cell.lblCardNo.text = "\(cardFormat) \(objCardList.last4 ?? "")"
                
                cell.lblCardType.text = objCardList.brand ?? ""
                
                cell.lblPostalCode.text = objCardList.postalCode ?? "-"
                
                cell.lblExpiryDate.text = objCardList.expiry ?? "-"
                
                cell.containerView.layer.borderWidth = 1
                
                cell.viewCardHeader.backgroundColor = Colors.theme_blue_color
                cell.containerView.layer.borderColor = Colors.theme_lightgray_color.cgColor
                
                if objCardList.isExpired {
                    cell.viewCardHeader.backgroundColor = Colors.theme_red_color
                    cell.viewAlert.isHidden = false
                    cell.containerView.layer.borderColor = Colors.theme_red_color.cgColor
                    cell.btnEdit.isHidden = false
                }else if objCardList.isExpiry{
                    cell.viewAlert.isHidden = false
                    cell.btnEdit.isHidden = true
                }else {
                    cell.viewAlert.isHidden = true
                    cell.btnEdit.isHidden = true
                }
            }
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cardCollectionView.frame.size.width, height: 193)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let roundedIndex = scrollView.contentOffset.x / scrollView.frame.size.width
        self.pageControl.currentPage = Int(roundedIndex)
        
    }
    
    // MARK: - Custom methods
    @objc func btnEditClicked(sender:UIButton)
    {
        print("btnEditClicked \(sender.tag)")
        
        if let vc = appDelegate.getViewController("UpdateCardDetailVC", onStoryboard:"Profile" ) as? UpdateCardDetailVC {

            let objCardList = arrCardList[sender.tag]
            vc.delegate = self
            vc.objCard = objCardList
            vc.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
            vc.modalPresentationStyle = .overFullScreen
            
            if let rootvc = UIApplication.shared.windows.first?.rootViewController
            {
                rootvc.present(vc,animated: true, completion: nil)
            }
        }
    }
    
    @objc func btnAlertClicked(sender:UIButton)
    {
        print("btnAlertClicked \(sender.tag)")
        
        let objCardList = arrCardList[sender.tag]
        var msg : String = ""
        
        if objCardList.isExpired {
            msg = appDelegate.cardExpiredMsg
        }else if objCardList.isExpiry {
            msg = appDelegate.cardExpiryMsg
        }
        showAlertForExppiryMsg(title: "Alert", msg: msg,index: sender.tag)
    }
    
    func showAlertForExppiryMsg(title:String,msg:String,index : Int)
    {
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            //self.navigationController?.popViewController(animated: true)
        }
        okAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(okAction)
        
        if let rootvc = UIApplication.shared.windows.first?.rootViewController
        {
            rootvc.present(alert,animated: true, completion: nil)
        }
    }
    
    @objc func btnDeleteClicked(sender:UIButton)
    {
        if arrCardList.count > 0
        {
            let objMyCard = arrCardList[sender.tag]
            if let popupVC = appDelegate.getViewController("DeletePopupVC", onStoryboard: "PreAlert") as? DeletePopupVC {
                popupVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
                popupVC.userID = userID
                popupVC.cardID = objMyCard.cardId ?? ""
                popupVC.isFromVC = "ManageCreditCard"
                popupVC.modalPresentationStyle = .overFullScreen
                if let rootvc = UIApplication.shared.windows.first?.rootViewController
                {
                    rootvc.present(popupVC,animated: true, completion: nil)
                }
            }
        }
    }
}

class CardCell : UICollectionViewCell
{
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var viewCardHeader : UIView!

    @IBOutlet weak var lblCardNo : UILabel!
    @IBOutlet weak var lblCardType : UILabel!
    @IBOutlet weak var lblExpiryDate : UILabel!
    @IBOutlet weak var lblPostalCode : UILabel!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnAlert : UIButton!
    @IBOutlet weak var imgViewAlert : UIImageView!
    @IBOutlet weak var viewAlert : UIView!

    @IBOutlet weak var cardView : UIView!
    override func awakeFromNib() {
        containerView.layer.cornerRadius = 5
        containerView.layer.masksToBounds = true
        
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = Colors.theme_lightgray_color.cgColor
    }
}
