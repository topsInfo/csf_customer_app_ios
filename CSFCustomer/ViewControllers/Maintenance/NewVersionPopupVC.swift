//
//  NewVersionPopupVC.swift
//  CSFCustomer
//
//  Created by Tops on 14/05/21.
//

import UIKit

class NewVersionPopupVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var viewIllDoItLater : UIView!
    @IBOutlet weak var lblUpdateAvailableText : UILabel!
    
    var appStoreVersion : String = ""
    var apiIOSVersion : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showHideIllDoItLaterView()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
    }
    // MARK: - IBAction Methods
    @IBAction func btnDownloadClicked()
    {
        let appId = "1547257972"
        let appName = Bundle.main.infoDictionary!["CFBundleName"] as? String ?? ""
        if let url = URL(string: "https://itunes.apple.com/in/app/\(appName)/id\(appId)?mt=8")
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else {
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }
        UserDefaults.standard.set(appStoreVersion, forKey: kCurrentAppStoreVersion)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnUpdateLaterClicked()
    {
        //print("apiIOSVersion on btnUpdateLaterClicked \(apiIOSVersion)")
        UserDefaults.standard.set(appStoreVersion, forKey: kCurrentAppStoreVersion)
        UserDefaults.standard.set(apiIOSVersion, forKey: kApiIOSVersion)

        var versionPopupCounter : Int = UserDefaults.standard.integer(forKey: kVersionPopupCounter)
        UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: kAppVersionPopupTimeStamp)
        
        versionPopupCounter += 1
        UserDefaults.standard.setValue(versionPopupCounter, forKey: kVersionPopupCounter)

        self.dismiss(animated: true, completion: nil)
    }
    
    /**
     Shows or hides the "I'll Do It Later" view based on the version popup counter.
     
     If the version popup counter is greater than 1, the label text is set to "An update is available: Please install new version" and the "I'll Do It Later" view is hidden. Otherwise, the label text is set to "An update is available: Do you want to install new version now?" and the "I'll Do It Later" view is shown.
     */
    func showHideIllDoItLaterView() {
        let versionPopupCounter: Int = UserDefaults.standard.integer(forKey: kVersionPopupCounter)

        if versionPopupCounter > 1 {
            self.lblUpdateAvailableText.text = "An update is available:\nPlease install new version"
            self.viewIllDoItLater.isHidden = true
        } else {
            self.lblUpdateAvailableText.text = "An update is available:\nDo you want to install new version now?"
            self.viewIllDoItLater.isHidden = false
        }
    }
}
