//
//  MaintenanceVC.swift
//  CSFCustomer
//
//  Created by Tops on 13/05/21.
//

import UIKit

class MaintenanceVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var lblMessage : UILabel!
    var message : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblMessage.text = message
    }
}
