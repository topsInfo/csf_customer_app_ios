//
//  DeleteAccountReasonVC.swift
//  CSFCustomer
//
//  Created by iMac on 29/08/22.
//

import UIKit
import WebKit
import DropDown
let messageCharacterLimitForDeleteAccountReason : Int = 200

class DeleteAccountReasonVC: UIViewController, UITextViewDelegate {
    // MARK: - IBOutlets
    @IBOutlet weak var txtReason : UITextField!
    @IBOutlet weak var tvDesc : UITextView!
    @IBOutlet weak var viewCancelReason : UIView!

    // MARK: - Global Variable
    let dropDown = DropDown()
    var selectReasonPlaceholderText = "Select reason"
    var placeholderText = "Additional notes"
    var refVC : UIViewController!
    var objDeleteAccountInfoModel = DeleteAccountInfoModel()
    var arrFeedbackReason :[FeedbackReasonDelAc] = [FeedbackReasonDelAc]()
    var selectedReasonID : Int   = -1
    var reasonId : Int = 0
    var reasonNote : String = ""

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - configureControls
    func configureControls()
    {
        arrFeedbackReason = self.objDeleteAccountInfoModel.feedbackReason
        
        configureTextView(textField:tvDesc, placeHolder: placeholderText, font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named:"theme_lightgray_color")!, cornerRadius: 5, borderColor: UIColor(named:"theme_lightgray_color")!, borderWidth: 1, bgColor: UIColor(named:"theme_textfield_bgcolor")!)
        tvDesc.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        tvDesc.delegate = self
        
        tvDesc.keyboardDistanceFromTextField = 80
        tvDesc.isUserInteractionEnabled = true

        viewCancelReason.layer.borderWidth = 1
        viewCancelReason.layer.borderColor = Colors.theme_lightgray_color.cgColor
        txtReason.paddingView(xvalue: 10)
        configureTextField(textField: txtReason, placeHolder: selectReasonPlaceholderText, font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 0, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        
        setDropDown()
    }
    
    // MARK: - setDropDown
    func setDropDown()
    {
        dropDown.width = UIScreen.main.bounds.size.width - 30
        dropDown.shadowRadius = 0

        dropDown.direction = .any
        dropDown.anchorView = txtReason
        dropDown.bottomOffset = CGPoint(x: 0, y:(txtReason.bounds.size.height))
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)

        dropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDown.separatorColor =  UIColor(named:"theme_lightgray_color")!
        dropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDown.textColor = UIColor(named:"theme_text_color")!
        dropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        dropDown.dataSource = arrFeedbackReason.map({$0.title})
        dropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                let objReason = self?.arrFeedbackReason[index]
                self?.selectedReasonID  = objReason?.id ?? -1
                self?.txtReason.text = item
                self?.dropDown.hide()
            }
            
        }
    }    
   
    // MARK: - IB-Action Methods
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnKeepMyAccountClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        print("btnKeepMyAccountClicked")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfirmClicked(_ sender: UIButton) {
        print("btnConfirmClicked")
        if isValidData() {
            if let finalDeleteAccountConfirmationVC = appDelegate.getViewController("FinalDeleteAccountConfirmationVC", onStoryboard: "Home") as? FinalDeleteAccountConfirmationVC {
                finalDeleteAccountConfirmationVC.objDeleteAccountInfoModel = self.objDeleteAccountInfoModel
                finalDeleteAccountConfirmationVC.refVC = self.refVC
                finalDeleteAccountConfirmationVC.reasonId  = self.selectedReasonID
                finalDeleteAccountConfirmationVC.reasonNote  = self.tvDesc.text
                finalDeleteAccountConfirmationVC.modalPresentationStyle = .overFullScreen
                finalDeleteAccountConfirmationVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
                self.refVC.present(finalDeleteAccountConfirmationVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func btnReasonDropDownClicked(_ sender: UIButton) {
        self.dropDown.show()
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtReason.text!)
        {
            appDelegate.showToast(message: Messsages.msg_select_reason, bottomValue: getSafeAreaValue())
            return false
        }
        else if tvDesc.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            appDelegate.showToast(message: Messsages.msg_delete_reason, bottomValue: getSafeAreaValue())
            return false
        }
        else if tvDesc.text == placeholderText
        {
            appDelegate.showToast(message: Messsages.msg_delete_reason, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
}

extension DeleteAccountReasonVC {
    // MARK: - UITextView Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == placeholderText
        {
            textView.text = ""
            textView.textColor = UIColor(named: "theme_black_color")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty
        {
            textView.text = placeholderText
            textView.textColor = UIColor(named: "theme_lightgray_color")
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let status : Bool = newText.count <= messageCharacterLimitForDeleteAccountReason
        if status == false {
            self.view.endEditing(true)
            appDelegate.showToast(message: Messsages.msg_textview_additionalNotes_limit, bottomValue: getSafeAreaValue())
        }
        return status
    }
}
