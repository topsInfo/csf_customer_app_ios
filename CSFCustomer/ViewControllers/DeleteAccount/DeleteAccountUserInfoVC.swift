//
//  DeleteAccountUserInfoVC.swift
//  CSFCustomer
//
//  Created by iMac on 29/08/22.
//
import UIKit
import SwiftyJSON

// MARK: - BalanceCell
class BalanceCell : UITableViewCell
{
    @IBOutlet weak var stackViewCard: UIStackView!
    
    @IBOutlet weak var viewNonRefundableBalance: UIView!
    @IBOutlet weak var viewRefundableBalance: UIView!
    
    @IBOutlet weak var lblNonRefundableBalanceTTD: UILabel!
    @IBOutlet weak var lblNonRefundableBalanceUSD: UILabel!
    
    @IBOutlet weak var lblRefundableBalanceTTD: UILabel!
    @IBOutlet weak var lblRefundableBalanceUSD: UILabel!
    
    @IBOutlet weak var viewNotes: UIView!
    @IBOutlet weak var lblNotes: UILabel!
    
    @IBOutlet weak var lblNonRefundableBalanceText: UILabel!

    @IBOutlet weak var lblRefundableBalanceText: UILabel!
    override func awakeFromNib() {
    }
}

// MARK: - InvoicesCell
class InvoicesCell : UITableViewCell
{
    @IBOutlet weak var stackViewCard: UIStackView!
    
    @IBOutlet weak var lblUndeliveredInvoices: UILabel!
    @IBOutlet weak var lblReturnInvoices: UILabel!
    @IBOutlet weak var lblPendingInvoices: UILabel!
    
    @IBOutlet weak var viewNotes: UIView!
    @IBOutlet weak var lblNotes: UILabel!
    
    override func awakeFromNib() {
    }
}

// MARK: - CreditCell
class CreditCell : UITableViewCell
{
    @IBOutlet weak var stackViewCard: UIStackView!
    
    @IBOutlet weak var lblOutstandingBalanceTTD: UILabel!
    @IBOutlet weak var lblOutstandingBalanceUSD: UILabel!
    
    @IBOutlet weak var viewNotes: UIView!
    @IBOutlet weak var lblNotes: UILabel!
}

// MARK: - SavedCardCell
class SavedCardCell : UITableViewCell
{
    @IBOutlet weak var imgCardThumb : UIImageView!
    @IBOutlet weak var lblCardNumber : UILabel!
    
    override func awakeFromNib() {
    }
}

// MARK: - AccountInfoHeaderCell
class AccountInfoHeaderCell : UITableViewCell
{
    @IBOutlet weak var lblHeader : UILabel!
    @IBOutlet weak var imgInfo : UIImageView!
    @IBOutlet weak var btnSection : UIButton!
    @IBOutlet weak var imgDropDown: UIImageView!
    @IBOutlet weak var viewDropDown: UIStackView!
    @IBOutlet weak var viewInfo: UIStackView!
    
    override func awakeFromNib() {
    }
}

// MARK: - AccountInfoCell
class AccountInfoCell : UITableViewCell
{
    @IBOutlet weak var lblBalance : UILabel!
    @IBOutlet weak var lblNote : UILabel!
    @IBOutlet weak var stackViewNote: UIStackView!
    @IBOutlet weak var viewNoteBack: UIView!
    override func awakeFromNib() {
    }
}

// MARK: - AccountBalanceHeaderCell
class AccountBalanceHeaderCell : UITableViewCell
{
    @IBOutlet weak var viewBalanceInfo: UIView!
    @IBOutlet weak var lblBalanceTitle: UILabel!
    @IBOutlet weak var lblTTD: UILabel!
    @IBOutlet weak var lblUSD: UILabel!
    @IBOutlet weak var viewSeperator: UIView!
    override func awakeFromNib() {
    }
}

// MARK: - DeleteAccountUserInfoVC
class DeleteAccountUserInfoVC: UIViewController, UITextViewDelegate, DeleteAccountTermsAndConditionProtocol {
    
    // MARK: - Global Variable
    var refVC : UIViewController!
    var noOfRowForSection1 : Int = 2
    var noOfRowForSection2 : Int = 1
    var noOfRowForSection3 : Int = 0
    var noOfRowForSection4 : Int = 0
    var pendingInvoiceCount : Int = 0
    var isAgreeToDeleAc : Bool = false
    var arrSelectedSections : [Int] = [Int]()
    var objDeleteAccountInfoModel = DeleteAccountInfoModel()
    var isShowAccountInfoNotes : Bool = true
    var isShowInvoiceNotes : Bool = true
    var isShowCreditNotes : Bool = true
    var cardFormat : String  = "XXXX XXXX XXXX"
    var refreshControl = UIRefreshControl()

    // MARK: - IBOutlets
    @IBOutlet weak var tblAccountInfo: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var imgAcceptTermsCondition: UIImageView!
    @IBOutlet weak var viewBottomBar: UIView!
    @IBOutlet weak var lblTermsCondition: UILabel!
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.enableDisalbeContinueButton()
        UserDefaults.standard.setValue(true, forKey: kIsAccountClosureShown)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblAccountInfo.addSubview(refreshControl)

        self.tblAccountInfo.isHidden = true
        self.viewBottomBar.isHidden = true
        
        self.arrSelectedSections.removeAll()
        self.arrSelectedSections.append(1)
        for _ in 0..<4
        {
            self.arrSelectedSections.append(0)
        }
        
        tblAccountInfo.delegate = self
        tblAccountInfo.dataSource = self
        tblAccountInfo.contentInsetAdjustmentBehavior = .never
        //        self.automaticallyAdjustsScrollViewInsets = false
        if #available(iOS 15.0, *) {
            tblAccountInfo.sectionHeaderTopPadding = 0
        }
        disableContinueButton()
        
        getDeleteAccountUserInfo(isShowLoader: true)
    }
    
    @objc func refresh()
    {
        getDeleteAccountUserInfo(isShowLoader: false)
        self.isAgreeToDeleAc = false
        self.enableDisalbeContinueButton()
    }
    
    // MARK: - enableDisalbeContinueButton
    func enableDisalbeContinueButton(){
        if isAgreeToDeleAc {
            self.imgAcceptTermsCondition.image = UIImage(named: "checkbox")
            if !isValid() {
                appDelegate.showToast(message: Messsages.msg_disalbeContinueButton_msg, bottomValue: getSafeAreaValue(), duration: 200)
            }
        }else{
            self.imgAcceptTermsCondition.image = UIImage(named: "uncheckbox")
        }
        
        if isValid() {
            enableContinueButton()
        }else{
            disableContinueButton()
        }
        self.tblAccountInfo.reloadData()
    }
    
    // MARK: - enableContinueButton
    func enableContinueButton(){
        self.btnContinue.backgroundColor = Colors.theme_orange_color
        self.btnContinue.setTitleColor(UIColor.white, for: .normal)
        self.btnContinue.isUserInteractionEnabled = true
    }
    
    // MARK: - disableContinueButton
    func disableContinueButton(){
        self.btnContinue.backgroundColor = Colors.theme_dele_ac_disable_button_back_color
        self.btnContinue.setTitleColor(Colors.theme_dele_ac_disable_button_text_color, for: .normal)
        self.btnContinue.isUserInteractionEnabled = false
    }
    
    // MARK: - isValid
    func isValid() -> Bool{
        if isAgreeToDeleAc && self.objDeleteAccountInfoModel.isAllowed == 1 {
            return true
        }
        return false
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnKeepMyAccountClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAcceptDelAccountClicked(_ sender: UIButton) {
        if let popupVC = appDelegate.getViewController("DeleteAccountTermsAndCondition", onStoryboard: "Home") as? DeleteAccountTermsAndCondition {
            if self.refVC == nil {
                //When user comes from side menu, the refVC will be null so, we will make current vc as refVC.
                self.refVC = self
            }
            popupVC.refVC = self.refVC
            popupVC.deleteAccountTermsAndConditionDelegate = self
            popupVC.objDeleteAccountInfoModel = self.objDeleteAccountInfoModel
            popupVC.modalPresentationStyle = .overFullScreen
            popupVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
            self.refVC.present(popupVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnConfirmClicked(_ sender: UIButton) {
        print("btnConfirmClicked")
        if let deleteAccountReasonVC = appDelegate.getViewController("DeleteAccountReasonVC", onStoryboard: "Home") as? DeleteAccountReasonVC {
            if self.refVC == nil {
                //When user comes from side menu, the refVC will be null so, we will make current vc as refVC.
                self.refVC = self
            }
            deleteAccountReasonVC.refVC  = self.refVC
            deleteAccountReasonVC.objDeleteAccountInfoModel = self.objDeleteAccountInfoModel
            refVC.navigationController?.pushViewController(deleteAccountReasonVC, animated: true)
        }
    }
    
    // MARK: - Protocol's Delegate Method
    func deleteAccountTermsAndCondition(isAgree: Bool) {
        isAgreeToDeleAc = isAgree
        self.enableDisalbeContinueButton()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Do sonthing
        print("traitCollectionDidChange called")
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                self.tblAccountInfo.reloadData()
            }
        }else{
            self.tblAccountInfo.reloadData()
        }
    }
}

// MARK: - Tableview Delegate Methods
extension DeleteAccountUserInfoVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return noOfRowForSection1
        }else if section == 1 {
            return noOfRowForSection2
        }else if section == 2 {
            return noOfRowForSection3
        }else if section == 3 {
            return noOfRowForSection4
        }else if section == 4 {
            return 1
        }else if section == 5 {
            return 1
        }
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            //Your Account Information section's cells
            if indexPath.row == 0 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountInfoCell", for: indexPath) as? AccountInfoCell {
                    
                    if let walletDetails = self.objDeleteAccountInfoModel.walletDetails {
                        if let ttdBalance = walletDetails.mainBalanceTtd {
                            if let usdBalance = walletDetails.mainBalanceUsd {
                                cell.lblBalance.text = "\(kTTCurrency) \((String(describing: ttdBalance)))/\(kCurrency) \((String(describing: usdBalance)))"
                            }
                        }
                        let info = walletDetails.instruction ?? ""
                        cell.lblNote.text = info
                        cell.stackViewNote.isHidden = info.isEmpty
                    }
                    return cell
                }
            }
            if indexPath.row == 1 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "BalanceCell", for: indexPath) as? BalanceCell {
                    
                    cell.stackViewCard.borderWidth = 1.0
                    cell.stackViewCard.borderColor = Colors.theme_dele_ac_border_color
                    cell.stackViewCard.layer.cornerRadius = 5.0
                    cell.stackViewCard.layer.masksToBounds = true
                    
                    if let  walletDetails = self.objDeleteAccountInfoModel.walletDetails {
                        cell.lblNonRefundableBalanceTTD.text = walletDetails.nonRefundedTtd ?? "0"
                        cell.lblNonRefundableBalanceUSD.text = walletDetails.nonRefundedUsd ?? "0"
                        cell.lblRefundableBalanceTTD.text = walletDetails.refundedTtd ?? "0"
                        cell.lblRefundableBalanceUSD.text = walletDetails.refundedUsd ?? "0"
                        
                        cell.lblRefundableBalanceText.attributedText = NSAttributedString().getRefundableText()
                        cell.lblNonRefundableBalanceText.attributedText = NSAttributedString().getNonRefundableText()
                        //"Refundable (Credit card top ups, Refunds from invoices)"
                        //"Non-refundable (Referrals and Promotional)"
                        
                        let info = walletDetails.instructionTwo ?? ""
                        cell.lblNotes.text = info
                        cell.viewNotes.isHidden = info.isEmpty
                    }
                    return cell
                }
            }
        }else if indexPath.section == 1 {
            //Pending Invoices section's cells
            if indexPath.row == 0 {
                //Cell's Header cell
                if let cell = tableView.dequeueReusableCell(withIdentifier: "InvoicesCell", for: indexPath) as? InvoicesCell {
                    
                    cell.stackViewCard.borderWidth = 1.0
                    cell.stackViewCard.borderColor = Colors.theme_dele_ac_border_color
                    cell.stackViewCard.layer.cornerRadius = 5.0
                    cell.stackViewCard.layer.masksToBounds = true
                    
                    if let  packageDetails = self.objDeleteAccountInfoModel.packageDetails {
                        cell.lblUndeliveredInvoices.text = "\(packageDetails.undeliveredInvoices ?? 0)"
                        cell.lblReturnInvoices.text = "\(packageDetails.returnInvoices ?? 0)"
                        cell.lblPendingInvoices.text = "\(packageDetails.pendingReceipts ?? 0)"
                        
                        let info = packageDetails.instruction ?? ""
                        cell.lblNotes.text = info
                        cell.viewNotes.isHidden = info.isEmpty
                    }
                    return cell
                }
            }
        }
        else if indexPath.section == 2 {
            if indexPath.row == 0 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CreditCell", for: indexPath) as? CreditCell {
                    
                    cell.stackViewCard.borderWidth = 1.0
                    cell.stackViewCard.borderColor = Colors.theme_dele_ac_border_color
                    cell.stackViewCard.layer.cornerRadius = 5.0
                    cell.stackViewCard.layer.masksToBounds = true
                    
                    if let  creditDetails = self.objDeleteAccountInfoModel.creditDetails {
                        cell.lblOutstandingBalanceTTD.text = creditDetails.totalOutstandingTtd ?? "0"
                        cell.lblOutstandingBalanceUSD.text = creditDetails.totalOutstandingUsd ?? "0"
                        
                        let info = creditDetails.instruction ?? ""
                        cell.lblNotes.text = info
                        cell.viewNotes.isHidden = info.isEmpty
                    }
                    return cell
                }
            }
        }
        else if indexPath.section == 3 {
            //Saved Card section's cells
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SavedCardCell", for: indexPath) as? SavedCardCell {
                
                if let cardDetails = self.objDeleteAccountInfoModel.cardDetails {
                    
                    if let lastFourDigits = cardDetails[indexPath.row].last4 {
                        if let brand = cardDetails[indexPath.row].brand {
                            
                            //\(cardFormat) \(lastFourDigits) - \(brand)
                            cell.lblCardNumber.text = "\(cardFormat) \(lastFourDigits) - (\(brand))"
                            
//                            cell.lblCardNumber.text = "**** **** **** \(lastFourDigits) - (\(brand))"
                        }
                    }
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrSelectedSections[indexPath.section] == 1 {
            //Expand
            if indexPath.section == 3{
                //Saved Card
                return 45.0
            }
            return UITableView.automaticDimension
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            //Credit Details
            if noOfRowForSection3 == 0 {
                return CGFloat.leastNonzeroMagnitude
            }
        }
        if section == 3 {
            //Saved Card
            if noOfRowForSection4 == 0 {
                return CGFloat.leastNonzeroMagnitude
            }
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountInfoHeaderCell") as? AccountInfoHeaderCell {
            if section == 0 {
                cell.lblHeader.text = "Wallet Information"
                cell.viewInfo.isHidden = true
                cell.viewDropDown.isHidden = false
            }else if section == 1 {
                if pendingInvoiceCount == 0 {
                    cell.lblHeader.text = "Pending Packages (\(pendingInvoiceCount))"
                    cell.imgInfo.image = UIImage(named: "icCorrect")
                    cell.viewInfo.isHidden = false
                    cell.viewDropDown.isHidden = false
                }else{
                    cell.lblHeader.text = "Pending Packages (\(pendingInvoiceCount))"
                    cell.imgInfo.image = UIImage(named: "icInfo")
                    cell.viewInfo.isHidden = false
                    cell.viewDropDown.isHidden = false
                }
            }else if section == 2 {
                cell.lblHeader.text = "Credit Details"
                cell.viewDropDown.isHidden = false
                
                if let creditDetails = self.objDeleteAccountInfoModel.creditDetails {
                    if creditDetails.status == 1 {
                        cell.imgInfo.image = UIImage(named: "icCorrect")
                        
                        if let outstandingTTD : Float = Float(creditDetails.totalOutstandingTtd)
                        {
                            if let outstandingUSD : Float = Float(creditDetails.totalOutstandingUsd) {
                                
                                if outstandingTTD > 0 || outstandingUSD > 0 {
                                    cell.viewInfo.isHidden = false
                                    cell.imgInfo.image = UIImage(named: "icInfo")
                                }else{
                                    cell.viewInfo.isHidden = false
                                }
                            }
                        }
                    }else {
                        cell.imgInfo.image = UIImage(named: "icInfo")
                        cell.viewInfo.isHidden = true
                    }
                }
                
            }else if section == 3 {
                cell.lblHeader.text = "Saved Card (\(noOfRowForSection4))"
                cell.viewInfo.isHidden = true
                cell.viewDropDown.isHidden = false
            }
            cell.btnSection.addTarget(self, action: #selector(btnSectionClicked(sender:)), for: .touchUpInside )
            cell.btnSection.tag = section
            
            if arrSelectedSections.count > 0 {
                if arrSelectedSections[section] == 1
                {
                    cell.imgDropDown.image = UIImage(named: "icUpArrow")
                }else{
                    cell.imgDropDown.image = UIImage(named: "icDownArrow")
                }
            }
            return cell.contentView
        }
        return UIView()
    }

    // MARK: - btnSectionClicked
    @objc func btnSectionClicked(sender:UIButton)
    {
        if arrSelectedSections[sender.tag] == 0
        {
            arrSelectedSections[sender.tag] = 1
        }else{
            arrSelectedSections[sender.tag] = 0
        }
        DispatchQueue.main.async {
            let section = NSIndexSet(index: sender.tag)
            self.tblAccountInfo.reloadSections(section as IndexSet, with: .automatic)
        }
    }
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension DeleteAccountUserInfoVC {

    // MARK: - getDeleteAccountUserInfo
    func getDeleteAccountUserInfo(isShowLoader : Bool)
    {
        var params : [String:Any] = [:]
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? "" //UserInfo.shared.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.deletePreview, showLoader: isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()

            self.tblAccountInfo.isHidden = false
            self.viewBottomBar.isHidden = false
            
            if status == true
            {
                self.objDeleteAccountInfoModel = DeleteAccountInfoModel(fromJson: JSON(resultDict))
                
                print("**** dele preview response is **** \n\(resultDict)")
                print("Card details : \(String(describing: self.objDeleteAccountInfoModel.cardDetails))")
                print("Credit details : \(String(describing: self.objDeleteAccountInfoModel.creditDetails))")
                print("FeedbackReason details : \(String(describing: self.objDeleteAccountInfoModel.feedbackReason))")
                print("package details : \(String(describing: self.objDeleteAccountInfoModel.packageDetails))")
                print("termsDocument : \(String(describing: self.objDeleteAccountInfoModel.termsDocument))")
                print("walletDetails : \(String(describing: self.objDeleteAccountInfoModel.walletDetails))")
                
                if let packageDetails = self.objDeleteAccountInfoModel.packageDetails {
                    self.pendingInvoiceCount = packageDetails.pendingReceipts + packageDetails.undeliveredInvoices + packageDetails.returnInvoices // It is total of all receipts count
                }
                
                if let walletDetails = self.objDeleteAccountInfoModel.walletDetails {
                    if let info = walletDetails.instructionTwo {
                        if info.isEmpty {
                            self.isShowAccountInfoNotes = false
                        }
                    }
                }
                
                if let packageDetails = self.objDeleteAccountInfoModel.packageDetails {
                    if let info = packageDetails.instruction {
                        if info.isEmpty {
                            self.isShowInvoiceNotes = false
                        }
                    }
                }
                
                if let creditDetails = self.objDeleteAccountInfoModel.creditDetails {
                    if creditDetails.status == 1 {
                        self.noOfRowForSection3 = 1
                    }else {
                        self.noOfRowForSection3 = 0
                    }
                    
                    if let info = creditDetails.instruction {
                        if info.isEmpty {
                            self.isShowCreditNotes = false
                        }
                    }
                }
                
                if let cardDetails = self.objDeleteAccountInfoModel.cardDetails {
                    self.noOfRowForSection4 = cardDetails.count
                }
                
                self.lblTermsCondition.text = Messsages.msg_agree_terms_to_delete_ac
                
                self.tblAccountInfo.reloadData()
                print("summary array is \(String(describing: self.objDeleteAccountInfoModel.finalSummaryArr))")
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                self.tblAccountInfo.isHidden = true
                self.viewBottomBar.isHidden = true
            }
        }
    }
}

extension NSAttributedString {

    // MARK: - getNonRefundableText
    func getNonRefundableText() -> NSAttributedString {
        
        let strFirstText :  String = "Non-refundable\n"
        let strSecondText :  String = "(Referrals and Promotional)"

        let str = "\(strFirstText)\(strSecondText)"
        
        let attributedString = NSMutableAttributedString(string: str, attributes: [
            .font: UIFont(name: fontname.openSansRegular, size: 15.0)!,
            .foregroundColor: Colors.theme_label_black_color
        ])
        
        attributedString.addAttribute(.foregroundColor,
                                      value: Colors.theme_orange_color,
                                      range: NSRange(location: strFirstText.count, length: strSecondText.count))

        attributedString.addAttribute(.font,
                                      value: UIFont(name: fontname.openSansItalic, size: 10.0)!,
                                      range: NSRange(location: strFirstText.count, length: strSecondText.count))
        return attributedString
    }
    
    // MARK: - getRefundableText
    func getRefundableText() -> NSAttributedString {
        
        let strFirstText :  String = "Refundable\n"
        let strSecondText :  String = "(Credit card top ups, Refunds from invoices)"

        let str = "\(strFirstText)\(strSecondText)"
        
        let attributedString = NSMutableAttributedString(string: str, attributes: [
            .font: UIFont(name: fontname.openSansRegular, size: 15.0)!,
            .foregroundColor: Colors.theme_label_black_color
        ])
        
        attributedString.addAttribute(.foregroundColor,
                                      value: Colors.theme_orange_color,
                                      range: NSRange(location: strFirstText.count, length: strSecondText.count))
        
        attributedString.addAttribute(.font,
                                      value: UIFont(name: fontname.openSansItalic, size: 10.0)!,
                                      range: NSRange(location: strFirstText.count, length: strSecondText.count))
        return attributedString
    }
}
