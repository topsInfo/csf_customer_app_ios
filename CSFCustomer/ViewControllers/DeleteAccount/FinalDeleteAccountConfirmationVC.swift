//
//  FinalDeleteAccountConfirmationVC.swift
//  CSFCustomer
//
//  Created by iMac on 29/08/22.
//

import UIKit
import SwiftyJSON

class InfoCell : UITableViewCell
{
    @IBOutlet weak var lblInfo : UILabel!
    @IBOutlet weak var imgBullet : UIImageView!
    
    override func awakeFromNib() {
    }
}

class FinalDeleteAccountConfirmationVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tblInfo : UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var cnstTblHeight: NSLayoutConstraint!
    
    // MARK: - Global Variable
    var refVC : UIViewController!
    var htmlString :String = ""
    var objDashboard = Dashboard()
    var objDeleteAccountInfoModel = DeleteAccountInfoModel()
    var reasonId : Int = 0
    var reasonNote : String = ""

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        configureData()
        self.containerView.isHidden = true
        self.tblInfo.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            self.containerView.isHidden = false
            self.cnstTblHeight.constant = self.tblInfo.contentSize.height
            if (self.tblInfo.contentSize.height) >= UIScreen.main.bounds.size.height - 410 {
                self.cnstTblHeight.constant = UIScreen.main.bounds.size.height - 410
                self.tblInfo.isScrollEnabled = true
            }else{
                self.cnstTblHeight.constant = self.tblInfo.contentSize.height + 10
                self.tblInfo.isScrollEnabled = false
            }
        }
    }
  
    func configureData(){
        tblInfo.delegate = self
        tblInfo.dataSource = self
        self.lblTitle.text = Messsages.msg_deleteAccount_final_confirmation_text
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        print("btnCancelClicked")
        
        self.popupAlert(title: "", message: Messsages.msg_cancel_delete_account, actionTitles: ["Confirm","Cancel"], actions:[{action1 in
            self.dismiss(animated: true) { [self] in
                if let navVC = refVC.navigationController {
                    for viewController in navVC.viewControllers {
                        if viewController.isKind(of: ProfileVC.self) {
                            refVC.navigationController?.popToViewController(viewController, animated: true)
                            break
                        }
                        if viewController.isKind(of: HomeVC.self) {
                            refVC.navigationController?.popToViewController(viewController, animated: true)
                            break
                        }
                    }
                }
            }
        },{ action2 in
        }])
    }
    
    @IBAction func btnConfirmClicked(_ sender: UIButton) {
        print("btnConfirmClicked")
        self.dismiss(animated: true) {
            if let OTPVerificationVC = appDelegate.getViewController("OTPVerificationPopupVC", onStoryboard: "Home") as? OTPVerificationPopupVC {
                OTPVerificationVC.refVC = self.refVC
                OTPVerificationVC.reasonId = self.reasonId
                OTPVerificationVC.reasonNote = self.reasonNote
                OTPVerificationVC.modalPresentationStyle = .overFullScreen
                OTPVerificationVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
                self.refVC.present(OTPVerificationVC, animated: true, completion: nil)
            }
        }
    }
}

extension FinalDeleteAccountConfirmationVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objDeleteAccountInfoModel.finalSummaryArr.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as? InfoCell {
            cell.lblInfo.text = self.objDeleteAccountInfoModel.finalSummaryArr[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
