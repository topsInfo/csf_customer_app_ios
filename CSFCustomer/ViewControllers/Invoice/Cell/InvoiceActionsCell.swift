//
//  SegmentOptionsCell.swift
//  CSFCustomer
//
//  Created by Tops on 29/09/21.

import UIKit

// MARK: - InvoiceActionsCell
class InvoiceActionsCell : UITableViewCell
{
    @IBOutlet weak var imgInvoiceAction : UIImageView!
    @IBOutlet weak var lblInvoiceAction : UILabel!
    @IBOutlet weak var viewImgBack : UIView!
    @IBOutlet weak var viewSeperator : UIView!
    
    override func layoutSubviews() {
        viewImgBack.clipsToBounds = true
        viewImgBack.layer.cornerRadius = viewImgBack.frame.size.height / 2
    }
}
