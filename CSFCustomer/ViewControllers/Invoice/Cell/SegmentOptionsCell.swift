//
//  SegmentOptionsCell.swift
//  CSFCustomer
//
//  Created by Tops on 29/09/21.

import UIKit

// MARK: - SegmentOptionsCell
class SegmentOptionsCell : UITableViewCell
{
    @IBOutlet weak var imgSegmentOption : UIImageView!
    @IBOutlet weak var imgChecked : UIImageView!
    @IBOutlet weak var lblOption : UILabel!
    @IBOutlet weak var viewSeperator : UIView!
}
