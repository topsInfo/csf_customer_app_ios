//
//  ChargesCell.swift
//  CSFCustomer
//
//  Created by Tops on 28/12/20.
// with footer cell

import UIKit
class ChargesCell: UITableViewCell,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var tblCharges : ChargeTableView!
    @IBOutlet weak var imgBackground : UIImageView!

    var arrChargesInfo :[EstimationList] = [EstimationList]()
    var msgQuantity = "Count (lbs/CF) : "
    var mainTableView = UITableView()
    var numberofrows : Int  = 0
    var objInvoiceDetail = InvoiceDetail()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func dummyDiscountData() -> [DiscountData] {
        var discountDataArr : [DiscountData] = [DiscountData]()
        let obj1 : DiscountData = DiscountData()
        obj1.discountNote = "Testing with 1001 TTD"
        obj1.discountUsd = "55.45"
        obj1.discountTtd = "377.08"
        discountDataArr.append(obj1)
        
        let obj2 : DiscountData = DiscountData()
        obj2.discountNote = "Testing with 2002 TTD Testing with 2002 TTD Testing with 2002 TTD Testing with 2002 TTD Testing with 2002 TTD, Hello brother 123"
        obj2.discountUsd = "45.45"
        obj2.discountTtd = "177.08"
        discountDataArr.append(obj2)
        
        let obj3 : DiscountData = DiscountData()
        obj3.discountNote = "Testing with 3003 TTD"
        obj3.discountUsd = "75.45"
        obj3.discountTtd = "877.08"
        discountDataArr.append(obj3)

        return discountDataArr
    }
    
    func setData()
    {
        tblCharges.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        if arrChargesInfo.count > 0
        {
            numberofrows = arrChargesInfo.count + 2
        }
        tblCharges.rowHeight = UITableView.automaticDimension
        tblCharges.dataSource = self
        tblCharges.delegate = self
        if let refund = objInvoiceDetail.isRefund, refund == "1"
        {
            let isFullRefund = objInvoiceDetail.fullRefund ?? ""
            if isFullRefund == "1"
            {
                imgBackground.image = UIImage(named: "ic_Full_Refund_Stemp")
            }
            else
            {
                imgBackground.image = UIImage(named: "ic_Partial_Refund_Stemp")
            }
        }
        else if let paidStatus = objInvoiceDetail.paidStatus, paidStatus == "1"
        {
            if let creditPaymentStatus = objInvoiceDetail.isCreditPayment,creditPaymentStatus == 1{
                imgBackground.image = UIImage(named: "ic_Collected_But_Not_Paid_Stemp")
            }else{
                imgBackground.image = UIImage(named: "ic_Paid_Stemp")
            }
        }
        else
        {
            imgBackground.image = UIImage(named: "")
        }
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblCharges.layer.removeAllAnimations()
       let newheight = tblCharges.contentSize.height
        let dict = [
            "height" : newheight
        ]
        NotificationCenter.default.post(name: Notification.Name("CellHeightUpdated"), object: nil,userInfo: dict)
    }
    // MARK: - Tableview delegate methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberofrows
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ChargeCell") as? ChargeCell {
            if indexPath.row == 0
            {
                cell.lblQuantity.isHidden = true
                let attributes = [NSAttributedString.Key.foregroundColor:UIColor(named:"theme_label_blue_color")]
                cell.lblDesc.attributedText = NSAttributedString(string: "DESCRIPTION OF CHARGES", attributes: attributes)
                cell.lblTTD.attributedText = NSAttributedString(string: "TTD", attributes: attributes)
                cell.lblUSD.attributedText = NSAttributedString(string: "USD", attributes: attributes)
                return cell
            }
            else if indexPath.row == numberofrows - 1
            {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as? FooterCell{
                    cell.lblGrandTotalTTD.text = objInvoiceDetail.totalTtd ?? ""
                    cell.lblGrandTotalUSD.text = objInvoiceDetail.totalUsd ?? ""
                    if let isDiscount = objInvoiceDetail.isDiscount
                    {
                        if isDiscount == "0"
                        {
                            cell.GrandTotalViewMain.isHidden = false
                            cell.SubtotalViewMain.isHidden = true
                            cell.constHeightTotalStackview.constant = 24
                        }
                        else
                        {
                            cell.lblSubTotalTTD.text = objInvoiceDetail.disAtTtd ?? ""
                            cell.lblSubTotalUSD.text = objInvoiceDetail.disAtUsd ?? ""
                            
                            cell.updateUI(discounts: objInvoiceDetail.discountData)
                            cell.setNeedsUpdateConstraints()
                            cell.updateConstraintsIfNeeded()
                        }
                    }
                    return cell
                }
                return UITableViewCell()
            }
            else
            {
                if let objEstimation = arrChargesInfo[indexPath.row-1] as? EstimationList
                {
                    cell.lblDesc.text = objEstimation.descriptionField ?? ""
                    cell.lblTTD.text = objEstimation.amountTtd ?? ""
                    cell.lblUSD.text = objEstimation.amountUsd ?? ""
                    if let strQty = objEstimation.quantity
                    {
                        let qtyString = "\(msgQuantity)\(strQty)"
                        let attrString = NSMutableAttributedString(string: qtyString)
                        let myRange = NSRange(location: msgQuantity.count, length:strQty.count)
                        attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named:"theme_black_color"), range: myRange)
                        cell.lblQuantity.attributedText = attrString
                    }
                }
                return cell
            }
        }
        return UITableViewCell()
    }
}
class ChargeCell : UITableViewCell
{
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var lblQuantity : UILabel!
    @IBOutlet weak var lblTTD : UILabel!
    @IBOutlet weak var lblUSD : UILabel!
}
class FooterCell : UITableViewCell
{
    @IBOutlet weak var lblGrandTotalTTD : UILabel!
    @IBOutlet weak var lblGrandTotalUSD : UILabel!
    @IBOutlet weak var lblSubTotalTTD : UILabel!
    @IBOutlet weak var lblSubTotalUSD : UILabel!
    @IBOutlet weak var GrandTotalViewMain : UIView!
    @IBOutlet weak var SubtotalViewMain: UIView!
    @IBOutlet weak var GrandTotalStackview : UIStackView!
    @IBOutlet weak var SubtotalStackview : UIStackView!
    @IBOutlet weak var constHeightTotalStackview: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    
    func getDiscountView(discount: DiscountData) -> UIView {
        // 1. View
        let view = UIView()
        view.backgroundColor = Colors.theme_green_color
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: discount.height).isActive = true
        // 2. Stack View
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 0
        // 3. Discount Note label
        let lblDiscountNote = UILabel()
        lblDiscountNote.translatesAutoresizingMaskIntoConstraints = false
        //lblDiscountNote.heightAnchor.constraint(equalToConstant: discount.height).isActive = true
        lblDiscountNote.numberOfLines = 0
        lblDiscountNote.font = UIFont(name: fontname.openSansBold, size: 14)
        lblDiscountNote.text = discount.discountNote
        lblDiscountNote.textColor = Colors.theme_white_color
        lblDiscountNote.lineBreakMode = .byWordWrapping
        // 4. Discount TDD label
        let lblDiscountTTD = UILabel()
        lblDiscountTTD.translatesAutoresizingMaskIntoConstraints = false
        lblDiscountTTD.font = UIFont(name: fontname.openSansBold, size: 14)
        lblDiscountTTD.textAlignment = .right
        
        lblDiscountTTD.text = "- \(discount.discountTtd ?? "")"
        lblDiscountTTD.textColor = Colors.theme_white_color
        let tddWidthConstraint = lblDiscountTTD.widthAnchor.constraint(equalToConstant: 80)
        tddWidthConstraint.isActive = true
        // 3. Discount USD label
        let lblDiscountUSD = UILabel()
        lblDiscountUSD.translatesAutoresizingMaskIntoConstraints = false
        lblDiscountUSD.font = UIFont(name: fontname.openSansBold, size: 14)
        lblDiscountUSD.textAlignment = .right
        lblDiscountUSD.text = "- \(discount.discountUsd ?? "")"
        lblDiscountUSD.textColor = Colors.theme_white_color
        lblDiscountTTD.widthAnchor.constraint(equalToConstant: 80).isActive = true
        let usdWidthConstraint = lblDiscountUSD.widthAnchor.constraint(equalToConstant: 80)
        usdWidthConstraint.isActive = true
        // 4. Add lables to stack view
        stackView.addArrangedSubview(lblDiscountNote)
        stackView.addArrangedSubview(lblDiscountTTD)
        stackView.addArrangedSubview(lblDiscountUSD)
        // 5. Add stack view
        view.addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        stackView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        // 6. Return view
        return view
    }
    
    func updateUI(discounts: [DiscountData]) {
        stackView.spacing = 0
        stackView.distribution = .fill
        stackView.alignment = .fill
        for i in discounts.indices {
            stackView.insertArrangedSubview(getDiscountView(discount: discounts[i]), at: i + 1)
        }
        let subTotalAndGrandTotalHeight: CGFloat = CGFloat(24 * 2)
        let notesHeight: CGFloat = discounts.reduce(0, { $0 + $1.height })
        let totalHeight: CGFloat = subTotalAndGrandTotalHeight + notesHeight
        print("-------- note height: \(notesHeight), total height: \(totalHeight) -------")
        constHeightTotalStackview.constant = totalHeight
        stackView.setNeedsLayout()
        stackView.layoutIfNeeded()
    }
}
