//
//  ChargeTableView.swift
//  CSFCustomer
//
//  Created by Tops on 28/12/20.
//

import UIKit

class ChargeTableView: UITableView {
    
      override var intrinsicContentSize: CGSize {
          self.layoutIfNeeded()
            return self.contentSize
        
      }
      override var contentSize: CGSize {
          didSet{
              self.invalidateIntrinsicContentSize()
          }
      }
      override func reloadData() {
          super.reloadData()
//            self.estimatedRowHeight = 60.0;
//            self.rowHeight = UITableView.automaticDimension
           self.invalidateIntrinsicContentSize()
      }
  }
