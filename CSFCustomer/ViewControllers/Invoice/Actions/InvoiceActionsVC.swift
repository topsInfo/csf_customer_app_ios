//
//  InvoiceActionsVC.swift
//  CSFCustomer
//
//  Created by Tops on 18/12/20.
//

import UIKit

let cellInvoiceActionsHeight : CGFloat = 44
let invoiceActionsPaddingHeight : CGFloat = 60

protocol InvoiceActionsVCDelegate {
    func getSelectedInvoiceAction(rowIndex : Int, invoiceActionIndex : Int, invoiceActionName : String)
}

class InvoiceActionsVC: UIViewController
{
    // MARK: - IB-Outlets
    @IBOutlet weak var tblInvoiceActions : UITableView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var dimmerView: UIView!

    // MARK: - Global Varialbes
    var isChanged : Bool = false
    var arrInvoiceActions = [InvoiceActions]()
    var InvoiceActionsVCDelegate : InvoiceActionsVCDelegate?
    var invoiceRowIndex : Int = 0
    var isShowCartOption : Bool!
    var isShowOpenTicketOption : Bool!
    
    // MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
        showData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //Mark :-Custom methods
    func configureControls()
    {
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
        self.tblInvoiceActions.backgroundColor = UIColor(named: "theme_white_color")
    }
    
    func showData()
    {
        arrInvoiceActions = getInvoiceActions(isShowCartOption: self.isShowCartOption, isShowOpenTicketOption: self.isShowOpenTicketOption)
        
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
//            let topHeight = safeAreaHeight - 510
            let topHeight = CGFloat(safeAreaHeight) - ((CGFloat(arrInvoiceActions.count) * cellInvoiceActionsHeight) + (2 * invoiceActionsPaddingHeight))
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
        tblInvoiceActions.delegate = self
        tblInvoiceActions.dataSource = self
        tblInvoiceActions.isScrollEnabled = false
        self.tblInvoiceActions.reloadData()
    }
   
    // MARK: - IB-Action Methods
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension InvoiceActionsVC : UITableViewDataSource,UITableViewDelegate {
    // MARK: - Tableview delegte methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrInvoiceActions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceActionsCell") as? InvoiceActionsCell {
            cell.contentView.backgroundColor = UIColor(named: "theme_white_color") // Colors.theme_white_color
            cell.backgroundColor = UIColor(named: "theme_white_color") //Colors.theme_white_color
            
            cell.lblInvoiceAction.text = arrInvoiceActions[indexPath.row].actionName
            cell.imgInvoiceAction.image = UIImage(named: arrInvoiceActions[indexPath.row].actionImage)
            cell.viewImgBack.backgroundColor = hexStringToUIColor(hex: arrInvoiceActions[indexPath.row].imageBackColor, alpha: 1.0)
            
            if indexPath.row == arrInvoiceActions.count - 1 {
                cell.viewSeperator.isHidden = true
            }else{
                cell.viewSeperator.isHidden = false
            }
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("\(indexPath.row) Selected")
        
        let strInvoiceActionName = arrInvoiceActions[indexPath.row].actionName
        
        self.InvoiceActionsVCDelegate?.getSelectedInvoiceAction(rowIndex: invoiceRowIndex, invoiceActionIndex: indexPath.row, invoiceActionName: strInvoiceActionName!)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellInvoiceActionsHeight
    }
}
