//
//  SegmentOptionsVC.swift
//  CSFCustomer
//
//  Created by Tops on 18/12/20.
//

import UIKit

let cellSegmentOptionsHeight : CGFloat = 44
let segmentOptionsPaddingHeight : CGFloat = 60

protocol SegmentOptionsVCDelegate {
    func getUpdatedSegments(arrSegments : [SegmentOptions])
}

class SegmentOptionsVC: UIViewController, UITableViewDragDelegate
{
    // MARK: - IB-Outlets
    @IBOutlet weak var tblSegmentOptions : UITableView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var dimmerView: UIView!

    // MARK: - Global Varialbes
    var segmentOptionsVCDelegate : SegmentOptionsVCDelegate?
    var isChanged : Bool = false
    
    // MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
        showData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
        self.tblSegmentOptions.backgroundColor = UIColor(named: "theme_white_color")
    }
    
    func showData()
    {
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            //let topHeight = safeAreaHeight - 510
            let topHeight = CGFloat(safeAreaHeight) - ((CGFloat(appDelegate.arrOptions.count) * cellSegmentOptionsHeight) + (2 * segmentOptionsPaddingHeight))
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
        
        tblSegmentOptions.delegate = self
        tblSegmentOptions.dataSource = self
//        tblSegmentOptions.isScrollEnabled = false

        //MoveRow
        tblSegmentOptions.isScrollEnabled = true
        tblSegmentOptions.alwaysBounceVertical = false
//        self.tblSegmentOptions.setEditing(true, animated: true)
//        tblSegmentOptions.semanticContentAttribute = .forceRightToLeft
        
        self.tblSegmentOptions.isUserInteractionEnabled = true
        self.tblSegmentOptions.dragInteractionEnabled = true
        self.tblSegmentOptions.dragDelegate = self
        self.tblSegmentOptions.allowsSelection = true
        self.tblSegmentOptions.allowsSelectionDuringEditing = true
        
        self.tblSegmentOptions.reloadData()
    }
    
    // MARK: - IBAction methods
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.dismiss(animated: true) { [self] in
            if isChanged {
                isChanged = false
                appDelegate.saveMangeTabsData()
                self.segmentOptionsVCDelegate?.getUpdatedSegments(arrSegments: appDelegate.arrOptions)
            }
        }
    }
}

extension SegmentOptionsVC : UITableViewDataSource,UITableViewDelegate {
    // MARK: - Tableview delegte methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDelegate.arrOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SegmentOptionsCell") as? SegmentOptionsCell {
            let objOption = appDelegate.arrOptions[indexPath.row]
            cell.lblOption.text = objOption.optionName
            
            if objOption.isChecked == "1" {
                cell.imgChecked.image = UIImage(named: "checkbox")
                cell.contentView.backgroundColor = UIColor(named: "theme_lightblue_color")//Colors.theme_lightblue_color
                cell.backgroundColor = UIColor(named: "theme_lightblue_color") //Colors.theme_lightblue_color
            }else {
                cell.imgChecked.image = UIImage(named: "uncheckbox")
                cell.contentView.backgroundColor = UIColor(named: "theme_white_color")//Colors.theme_white_color
                cell.backgroundColor = UIColor(named: "theme_white_color") //Colors.theme_white_color
            }
            
            cell.viewSeperator.isHidden = false
            cell.imgSegmentOption.image = UIImage(named: "reorder")
            //MoveRow
            //cell.showsReorderControl = true
            
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellSegmentOptionsHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isChanged = true
        let objOption = appDelegate.arrOptions[indexPath.row]
        if objOption.isChecked == "1" {
            objOption.isChecked = "0"
        }else {
            objOption.isChecked = "1"
        }
        
        let arrSelectedSegments = appDelegate.arrOptions.filter { $0.isChecked == "1" }
        if arrSelectedSegments.count < 3 {
            objOption.isChecked = "1"
            appDelegate.showToast(message: Messsages.msg_select_3_to_5_tabs, bottomValue: getSafeAreaValue())
        }else if arrSelectedSegments.count > 5 {
            objOption.isChecked = "0"
            appDelegate.showToast(message: Messsages.msg_select_3_to_5_tabs, bottomValue: getSafeAreaValue())
        }
        self.tblSegmentOptions.reloadData()
        self.tblSegmentOptions.dataSource?.tableView?(tableView, canMoveRowAt: indexPath)
    }

    //MoveRow
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        // fake implementation with empty list
        return []
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        let itemToMove = appDelegate.arrOptions[indexPath.row]
//        if let isChecked = itemToMove.isChecked {
//            if isChecked == "1" {
//                return true
//            }
//        }
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let itemToMove = appDelegate.arrOptions[sourceIndexPath.row]
        appDelegate.arrOptions.remove(at: sourceIndexPath.row)
        appDelegate.arrOptions.insert(itemToMove, at: destinationIndexPath.row)
        reArrangeSegmentOptions()
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
  
    func reArrangeSegmentOptions(){
        var newIndex : Int = 0
        var newArrOptions = [SegmentOptions]()

        for obj in appDelegate.arrOptions {
            //print("Name : \(obj.optionName!) Index : \(obj.index!)")
            
            var newObj = SegmentOptions()
            newObj = obj
            newObj.index = String(newIndex)
            newArrOptions.append(newObj)
            newIndex += 1
        }
        
        appDelegate.arrOptions.removeAll()
        appDelegate.arrOptions = newArrOptions
        isChanged = true
    }
//https://www.generacodice.com/en/articolo/4412395/how-do-i-make-reorder-control-of-uitableviewcell-in-left-side
//https://swiftdeveloperblog.com/uitableviewcontroller-rearrange-or-reorder-table-cells-example-in-swift/
//https://stackoverflow.com/questions/54911214/drag-and-drop-works-for-uitableview-without-uitableviewdropdelegate
}
