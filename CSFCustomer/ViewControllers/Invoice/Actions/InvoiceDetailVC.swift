//
//  PaidInvoiceVC.swift
//  CSFCustomer
//
//  Created by Tops on 25/12/20.
//

import UIKit
import SwiftyJSON

class InvoiceDetailVC: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tblInvoiceDetail : UITableView!
    @IBOutlet weak var btnScanQRCodeButton : UIButton!

    // MARK: - Global Variable
    var objInvoiceDetail = InvoiceDetail()
    var arrSectionTitle = ["","","Description of Items","Package Status","","","Payment Status"]
    var arrDesc : [String] = [String]()
    var arrPkgStatus : [PackageStatus] = [PackageStatus]()
    var arrPaymentDetails : [PaymentDetails] = [PaymentDetails]()
    var isPaid : Bool = false
    var arrChargesInfo :[EstimationList] = [EstimationList]()
    var userID : String  = ""
    var invoiceID : String = ""
    var invoiceNo : String = ""
    var isFullRefund : String  = ""
    var isFrom : String  = ""

    var isNeedToRefresh : Bool = false

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - Configure Views
    func configureViews()
    {
        btnScanQRCodeButton.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(reloaddata(_:)), name: NSNotification.Name(rawValue: "CellHeightUpdated"), object: nil)
        self.tblInvoiceDetail.isHidden = true
        
        if isFrom == "PushNotification"
        {
            getInvoiceDetail()
        }
        else
        {
            showData()
        }
        tblInvoiceDetail.dataSource = self
        tblInvoiceDetail.delegate = self
        tblInvoiceDetail.estimatedRowHeight = 100
        tblInvoiceDetail.rowHeight = UITableView.automaticDimension
    }
    
    func showData()
    {
        arrDesc = objInvoiceDetail.descriptionList ?? []
        //Nand : Payment detail changes
        if let arrPaymentDetails = objInvoiceDetail.paymentDetails {
            self.arrPaymentDetails = arrPaymentDetails
            if self.arrPaymentDetails.count > 0
            {
                arrSectionTitle.append("Payment Details")
            }
        }
        if let arrPkgStatus = objInvoiceDetail.packageStatus
        {
            self.arrPkgStatus = arrPkgStatus
        }
        if let arrCharges = objInvoiceDetail.estimationList
        {
            self.arrChargesInfo = arrCharges
            if self.arrChargesInfo.count > 0
            {
                arrSectionTitle.append("Charges Information")
            }
        }

        DispatchQueue.main.async { [self] in
            
            self.tblInvoiceDetail.reloadData()

            self.tblInvoiceDetail.isHidden = false
        }
    }
    
    @objc func reloaddata(_ notification: NSNotification)
    {
       let height =  notification.userInfo?["height"] as? String ?? ""
        DispatchQueue.main.async {
            self.tblInvoiceDetail.reloadData()
        }
    }
    func getInvoiceDetail()
    {
        let objUserInfo = getUserInfo()
        var params : [String:Any] = [:]
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"] = invoiceID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ViewInvoice, showLoader:true) { (resultDict, status, message) in
            self.tblInvoiceDetail.isHidden = false
            if status == true
            {
                if self.isNeedToRefresh {
                    //This is because, If we not remove all section then, it will make duplicate section entry. Additional section will be added from showData() function.
                    self.isNeedToRefresh = false
                    self.arrSectionTitle.removeAll()
                    self.arrSectionTitle = ["","","Description of Items","Package Status","","","Payment Status"]
                }
                
                let objInvoiceDetail = InvoiceDetail(fromJson: JSON(resultDict))
                self.isFullRefund = objInvoiceDetail.fullRefund ?? ""
                self.objInvoiceDetail = objInvoiceDetail
                self.invoiceNo = objInvoiceDetail.invoiceNumber ?? ""
                if let status = objInvoiceDetail.paidStatus as? String, status == "0"
                {
                    self.isPaid = false
                }
                else
                {
                    self.isPaid = true
                }
                self.showData()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnScanQRCodeClicked()
    {

    }
    @IBAction func btnDownloadClicked()
    {
        var params : [String:Any] = [:]
        self.showHUD()
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"] = objInvoiceDetail.invoiceId ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.DownloadInvoice, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                if let filePath = (resultDict as NSDictionary).value(forKey: "filepath") as? String
                {
                    self.downloadFile(InvoiceID:self.objInvoiceDetail.hawbNumber ?? "",pdfFileURL:filePath)
                }
                else
                {
                    self.hideHUD()
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
}
extension InvoiceDetailVC : UITableViewDataSource,UITableViewDelegate
{
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSectionTitle.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2
        {
            return arrDesc.count
        }
        else if section == 3 || section == 5
        {
            return 0
        }
        else if section == 4
        {
            return arrPkgStatus.count
        }
        else if section == 6
        {
            return 0
        }else if section == 7 {
            return arrPaymentDetails.count + 1
        }
        else if section == ((arrPaymentDetails.count > 0) ? 8 : 7)
        {
//            if arrChargesInfo.count > 0
//            {
//                return arrChargesInfo.count + 1
//            }
//            else
//            {
//                return 0
//            }
            return 1
        }
        return 1
    }
   
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1
        {
           return CGFloat.leastNormalMagnitude
        }
        else if section == 4 || section == 5
        {
            return 30
        }
        return 45
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 || section == 8
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SectionHeaderCell") as? SectionHeaderCell {
                cell.lblTitle.text = arrSectionTitle[section]
                return cell.contentView
            }
        }
        else if section == 3
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "PkgStatusHeaderCell") as?   PkgStatusHeaderCell {
                cell.lblTitle.text = arrSectionTitle[section]
                cell.btnViewHistory.addTarget(self, action: #selector(btnViewHistoryClicked(sender:)), for: .touchUpInside)
                return cell.contentView
            }
        }
        else if section == 6
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentStatusHeaderCell") as?   PaymentStatusHeaderCell {
                //Colors.theme_white_color
                cell.btnPaidorUnpaid.setTitleColor(UIColor.white, for: .normal)
                cell.btnPaidorUnpaid.layer.cornerRadius = 5
                cell.btnPaidorUnpaid.layer.masksToBounds = true
                if isPaid == false
                {
                    //Colors.theme_red_color
                    cell.btnPaidorUnpaid.backgroundColor = UIColor(named: "theme_red_color")!
                    cell.btnPaidorUnpaid.setTitle("Unpaid", for: .normal)
                }
                else
                {
                    //Colors.theme_green_color
                    if let creditPaymentStatus = objInvoiceDetail.isCreditPayment,creditPaymentStatus == 1{
                        cell.btnPaidorUnpaid.backgroundColor = UIColor(named: "theme_red_color")!
                        cell.btnPaidorUnpaid.setTitle("Unpaid", for: .normal)
                    }else{
                        cell.btnPaidorUnpaid.backgroundColor = UIColor(named: "theme_green_color")!
                        cell.btnPaidorUnpaid.setTitle("Paid", for: .normal)
                    }
                }
                if let isRefund = objInvoiceDetail.isRefund,isRefund == "1"
                {
                    cell.btnPaidorUnpaid.backgroundColor = UIColor(named: "theme_green_color")! //Colors.theme_green_color
                    cell.btnPaidorUnpaid.setTitle("Refund", for: .normal)
                }
                cell.lblTitle.text = arrSectionTitle[section]
                return cell.contentView
                
            }
        }
        else if section == 7
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentDetailHeaderHeaderCell") as?   PaymentDetailHeaderHeaderCell {
                cell.lblTitle.text = arrSectionTitle[section]
                return cell.contentView
            }
        }
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//As now footer is created by cell and we are not using the footer view so, we will pass 0 as height
//        if section == ((arrPaymentDetails.count > 0) ? 8 : 7)
//        {
//            return 90
//        }
        return 0
    }
   
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if section == 7
//        {
//            footerView.frame = CGRect(x: 20, y: tblInvoiceDetail.frame.size.height, width: self.tblInvoiceDetail.frame.size.width-40, height: 90)
//            return footerView
//        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return UITableView.automaticDimension
        }else if indexPath.section == 7 && arrPaymentDetails.count > 0{
            return 28
        }
//        else if indexPath.section == 7
//        {
//            return CGFloat(arrChargesInfo.count)
//        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath) as? MemberCell{
                return setMemberDetail(cell:cell)
            }
        }
        else if indexPath.section == 1
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceDtlHeaderCell", for: indexPath) as? InvoiceDtlHeaderCell{
                return setInvoiceDetail(cell:cell)
            }
        }
        else if indexPath.section == 2
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "DescCell", for: indexPath) as? DescCell{
                cell.lblTitle.text = arrDesc[indexPath.row]
                cell.lblTitle.numberOfLines = 0
                return cell
            }
        }
        else if indexPath.section == 4
        {
            let objPackageStatus = arrPkgStatus[indexPath.row]
            var arrViews : [UIView] = [UIView]()
            if  indexPath.row < arrPkgStatus.count - 1
            {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "PackageStatusCell", for: indexPath) as? PackageStatusCell {
                    arrViews = [cell.view1,cell.view2,cell.view3]
                    if objPackageStatus.status == 1
                    {
                        //Colors.theme_green_color
                        return setPackageStatusColors(cell: cell, img: "select_status", textcolor: UIColor(named: "theme_green_color")!,views:arrViews,viewColor:UIColor(named: "theme_green_color")!,title:objPackageStatus.title ?? "",date:objPackageStatus.date ?? "")
                    }
                    else if objPackageStatus.status == 0
                    {
                        //Colors.theme_black_color
                        //Colors.theme_gray_color
                        return setPackageStatusColors(cell: cell, img: "radio_btn2", textcolor:UIColor(named: "theme_text_color")! ,views:arrViews,viewColor:UIColor(named: "theme_gray_color")!,title:objPackageStatus.title ?? "",date:"")
                    }
                }
            }
            else
            {
                if let cell1 = tableView.dequeueReusableCell(withIdentifier: "PackageStatusCell1", for: indexPath) as? PackageStatusCell1 {
                    if objPackageStatus.status == 1
                    {
                        //Colors.theme_green_color
                        return setPackageStatusColors1(cell: cell1, img: "select_status", textcolor: UIColor(named: "theme_green_color")!,title:objPackageStatus.title ?? "",date:objPackageStatus.date ?? "")
                    }
                    else if objPackageStatus.status == 0
                    {
                        //Colors.theme_black_color
                        return setPackageStatusColors1(cell: cell1, img: "radio_btn2", textcolor:UIColor(named: "theme_text_color")! ,title:objPackageStatus.title ?? "",date:"")
                    }
                }
            }
        }
        else if indexPath.section == ((arrPaymentDetails.count > 0) ? 8 : 7)
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ChargesCell", for: indexPath) as? ChargesCell{
                cell.arrChargesInfo = self.arrChargesInfo
                cell.objInvoiceDetail = self.objInvoiceDetail
                cell.mainTableView = self.tblInvoiceDetail
                cell.setData()
                return cell
            }
        }
        else if indexPath.section == 7
        {
            //Nand : Payment detail changes
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ViewPaymentDetailsCell", for: indexPath) as? ViewPaymentDetailsCell{
                if indexPath.row == 0 {
                    let attributes = [NSAttributedString.Key.foregroundColor:UIColor(named:"theme_label_blue_color")]
                    cell.lblTitle.attributedText = NSAttributedString(string: "PAYMENT TYPE", attributes: attributes as [NSAttributedString.Key : Any])
                    cell.lblTTD.text = "TTD"
                    cell.lblUSD.text = "USD"
                }else {
                    let objPaymentDetails = arrPaymentDetails[indexPath.row - 1]
                    
                    let attributes = [NSAttributedString.Key.foregroundColor:UIColor(named:"theme_black_color")]
                    
                    cell.lblTitle.attributedText = NSAttributedString(string: "\(objPaymentDetails.paymentType!)", attributes: attributes as [NSAttributedString.Key : Any])
                    cell.lblTTD.attributedText = NSAttributedString(string: "\(objPaymentDetails.totalTtd!)", attributes: attributes as [NSAttributedString.Key : Any])
                    cell.lblUSD.attributedText = NSAttributedString(string: "\(objPaymentDetails.totalUsd!)", attributes: attributes as [NSAttributedString.Key : Any])
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    // MARK: - Custom Methods
    @objc func btnViewHistoryClicked(sender:UIButton)
    {
        let objUserInfo = getUserInfo()
        if let vc = appDelegate.getViewController("PackageStatusVC", onStoryboard: "Invoice") as? PackageStatusVC{
            vc.invoiceNumber = invoiceNo
            vc.userID = objUserInfo.id ?? ""
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc,animated: true, completion: nil)
        }
    }
    func setPackageStatusColors(cell:PackageStatusCell,img:String,textcolor:UIColor,views:[UIView],viewColor:UIColor,title:String,date:String) -> PackageStatusCell
    {
        cell.imgStatus.image = UIImage(named:img) //green image
        cell.lblStatus.textColor = textcolor
        for view in views
        {
               view.backgroundColor = viewColor
               view.layer.cornerRadius = view.frame.size.height/2
               view.layer.masksToBounds = true
                
        }
        if date != ""
        {
            cell.lblStatus.text = "\(title) (\(date))"
        }
        else
        {
            cell.lblStatus.text = title
        }
        
        return cell
    }
    func setPackageStatusColors1(cell:PackageStatusCell1,img:String,textcolor:UIColor,title:String,date:String) -> PackageStatusCell1
    {
        cell.imgStatus.image = UIImage(named:img)
        cell.lblStatus.textColor = textcolor
        if date != ""
        {
            cell.lblStatus.text = "\(title) (\(date))"
        }
        else
        {
            cell.lblStatus.text = title
        }
             
        return cell
    }
    func setMemberDetail(cell:MemberCell) -> MemberCell
    {
        cell.lblMemberName.text = objInvoiceDetail.memberName ?? ""
        cell.lblBoxID.text = objInvoiceDetail.boxId ?? ""
        cell.lblAddress.text = objInvoiceDetail.address ?? ""
        cell.lblCountry.text = objInvoiceDetail.country ?? ""
        return cell
    }
    func setInvoiceDetail(cell:InvoiceDtlHeaderCell) -> InvoiceDtlHeaderCell
    {
        cell.lblDeliveryAgent.text = (objInvoiceDetail.deliveryAgent ?? "").isEmpty ? "-" : objInvoiceDetail.deliveryAgent
        cell.lblInvoiceDt.text = objInvoiceDetail.dateOfInvoice ?? ""
        cell.lblInvoiceNo.text = objInvoiceDetail.invoiceNumber ?? ""
        invoiceNo = objInvoiceDetail.invoiceNumber ?? ""
        cell.lblHAWNo.text = (objInvoiceDetail.hawbNumber ?? "").isEmpty ? "-" : objInvoiceDetail.hawbNumber
        cell.lblShipper.text = (objInvoiceDetail.shipper ?? "").isEmpty ? "-" : objInvoiceDetail.shipper
        cell.lblWeight.text = (objInvoiceDetail.weight ?? "").isEmpty ? "-" : objInvoiceDetail.weight
        cell.lblTrackingNo.text = (objInvoiceDetail.trackingNumber ?? "").isEmpty ? "-" : objInvoiceDetail.trackingNumber
        cell.lblExchangeRate.text = objInvoiceDetail.conversionRate ?? ""
       
        if objInvoiceDetail.paymentType != ""
        {
            cell.PaidModeView.isHidden = false
            cell.TransIDView.isHidden = false
            cell.PaidDateView.isHidden = false
            cell.lblPaidMode.text = objInvoiceDetail.paymentType ?? ""
            cell.lblPaidDate.text = objInvoiceDetail.paidDate ?? ""
            cell.lblTransID.text = objInvoiceDetail.transactionId ?? ""
        }
        else
        {
            cell.PaidModeView.isHidden = true
            cell.PaidDateView.isHidden = true
            cell.TransIDView.isHidden = true
        }
        if objInvoiceDetail.isRefund == "1"
        {
            cell.RefundAmountView.isHidden = false
            cell.lblRefundAmount.text = objInvoiceDetail.refundAmount as? String ?? ""
        }
        else
        {
            cell.RefundAmountView.isHidden = true
        }
        return cell
    }
}
class SectionHeaderCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
}

class MemberCell : UITableViewCell
{
    @IBOutlet weak var lblMemberName : TapAndCopyLabel!
    @IBOutlet weak var lblBoxID : TapAndCopyLabel!
    @IBOutlet weak var lblAddress : TapAndCopyLabel!
    @IBOutlet weak var lblCountry : UILabel!
    
    override func layoutSubviews() {
        lblAddress.sizeToFit()
    }
}
class InvoiceDtlHeaderCell : UITableViewCell
{
    @IBOutlet weak var lblInvoiceDt : UILabel!
    @IBOutlet weak var lblInvoiceNo : TapAndCopyLabel!
    @IBOutlet weak var lblHAWNo : TapAndCopyLabel!
    @IBOutlet weak var lblShipper : UILabel!
    @IBOutlet weak var lblDeliveryAgent : UILabel!
    @IBOutlet weak var lblTrackingNo : TapAndCopyLabel!
    @IBOutlet weak var lblExchangeRate : UILabel!
    @IBOutlet weak var PaidModeView : UIStackView!
    @IBOutlet weak var lblPaidMode : UILabel!
    @IBOutlet weak var TransIDView : UIStackView!
    @IBOutlet weak var lblTransID : TapAndCopyLabel!
    @IBOutlet weak var PaidDateView : UIStackView!
    @IBOutlet weak var lblPaidDate : UILabel!
    @IBOutlet weak var RefundAmountView : UIStackView!
    @IBOutlet weak var lblRefundAmount : UILabel!
    @IBOutlet weak var lblWeight : UILabel!
    
}
class DescCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
}
class PackageStatusCell : UITableViewCell
{
    @IBOutlet weak var imgStatus : UIImageView!
    @IBOutlet weak var view1 : UIView!
    @IBOutlet weak var view2 : UIView!
    @IBOutlet weak var view3 : UIView!
    @IBOutlet weak var lblStatus : UILabel!

}
class PackageStatusCell1 : UITableViewCell
{
    @IBOutlet weak var imgStatus : UIImageView!
    @IBOutlet weak var lblStatus : UILabel!
}
class PkgStatusHeaderCell : UITableViewCell
{
   @IBOutlet weak var lblTitle : UILabel!
   @IBOutlet weak var btnViewHistory : UIButton!
}
class PaymentStatusHeaderCell : UITableViewCell
{
   @IBOutlet weak var lblTitle : UILabel!
   @IBOutlet weak var btnPaidorUnpaid : UIButton!
}
class BlankCell : UITableViewCell
{
    @IBOutlet weak var blankView :UIView!
}
class ChargesHeaderCell : UITableViewCell
{
   @IBOutlet weak var lblTitle : UILabel!
}

//Nand : Payment detail changes
class PaymentDetailHeaderHeaderCell : UITableViewCell
{
   @IBOutlet weak var lblTitle : UILabel!
}

class ViewPaymentDetailsCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblTTD : UILabel!
    @IBOutlet weak var lblUSD : UILabel!
}
