//
//  PackageStatusVC.swift
//  CSFCustomer
//
//  Created by Tops on 29/12/20.
//

import UIKit
import SwiftyJSON
class PackageStatusVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var tblPackageStatus : UITableView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
//    @IBOutlet weak var bottomConst : NSLayoutConstraint!
    
    // MARK: - Global Variable
    var invoiceNumber : String  = ""
    var userID : String  = ""
    var arrHistory : [InvoiceHistoryList] =  [InvoiceHistoryList]()

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Tableview delegate methods
    func configureControls()
    {
        showCard()
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")//UIColor.black.withAlphaComponent(0.4)
        getInvoiceHistory()
        tblPackageStatus.estimatedRowHeight = 100
        tblPackageStatus.rowHeight = UITableView.automaticDimension
        tblPackageStatus.dataSource = self
        tblPackageStatus.delegate = self
        tblPackageStatus.tableFooterView = UIView()
    }
    
    func showCard()
    {
        self.view.layoutIfNeeded()
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let height = safeAreaHeight - 80
            let topHeight = safeAreaHeight - height //height
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
//            bottomConst.constant = 45
        }
    }
    func getInvoiceHistory()
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["invoice_number"] = invoiceNumber
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.InvoiceHistory, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                let objHistory = InvoiceHistory(fromJson: JSON(resultDict))
                self.arrHistory = objHistory.list ?? []
                DispatchQueue.main.async {
                    self.tblPackageStatus.reloadData()
                }             
            }
            else
            {
               appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHistory.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceHistoryCell", for: indexPath) as? InvoiceHistoryCell {
            let objHistory = arrHistory[indexPath.row]
            cell.lblDate.text = objHistory.date ?? ""
            cell.lblTime.text = objHistory.time ?? ""
            cell.lblTitle.text = objHistory.title ?? ""
            cell.lblDesc.text = objHistory.descriptionField ?? ""
            return cell
        }
        return UITableViewCell()
    }
}
class InvoiceHistoryCell : UITableViewCell
{
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var circleView : UIView!
    override  func awakeFromNib() {
        circleView.layer.cornerRadius = circleView.frame.size.height / 2
        circleView.layer.masksToBounds = true
    }
}
