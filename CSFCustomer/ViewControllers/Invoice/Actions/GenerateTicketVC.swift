//
//  GenerateTicketVC.swift
//  CSFCustomer
//
//  Created by Tops on 23/12/20.
//

import UIKit

class GenerateTicketVC: UIViewController,UITextViewDelegate
{
    @IBOutlet weak var txtSubject : UITextField!
    @IBOutlet weak var txtReport : UITextView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var btnSend : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var containerView : UIView!
    
    //Attachment
    @IBOutlet weak var stackViewAttachment1 : UIView!
    @IBOutlet weak var stackViewAttachment2 : UIView!
    @IBOutlet weak var stackViewAttachment3 : UIView!
    @IBOutlet weak var viewAttachment1 : UIView!
    @IBOutlet weak var viewAttachment2 : UIView!
    @IBOutlet weak var viewAttachment3 : UIView!
    @IBOutlet weak var imgAttachment1 : UIImageView!
    @IBOutlet weak var imgAttachment2 : UIImageView!
    @IBOutlet weak var imgAttachment3 : UIImageView!
    @IBOutlet weak var btnAttachment1 : UIButton!
    @IBOutlet weak var btnAttachment2 : UIButton!
    @IBOutlet weak var btnAttachment3 : UIButton!
    @IBOutlet weak var btnRemoveAttachment1 : UIButton!
    @IBOutlet weak var btnRemoveAttachment2 : UIButton!
    @IBOutlet weak var btnRemoveAttachment3 : UIButton!

    @IBOutlet weak var viewRemoveAttachment1 : UIView!
    @IBOutlet weak var viewRemoveAttachment2 : UIView!
    @IBOutlet weak var viewRemoveAttachment3 : UIView!

    var placeholderText = "Report an issue"
    var objUserInfo = UserInfo()
    var hawb_number : String = ""

    //Attachment
    var mimeType : String  = ""
    var currentAttachmentIndex : Int!
    var totalBoxCount : Int = 1
    var arrAttachment : [UIImage] = [UIImage]()
    var arrAttachmentData : [Data] = [Data]()
    var arrMimeType : [String] = [String]()
    var strDescription : String = ""
    var arrDocFileNames : [String] = [String]()
    var uploadData = Data()
    var isImageRemoved : Int = 0
    var docFileName : String = ""
    let imagePicker = UIImagePickerController()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //Mark :- Custom Methods
    func configureControls()
    {
        objUserInfo = getUserInfo()
        showCard()
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")//UIColor.black.withAlphaComponent(0.5)
        //Colors.theme_lightgray_color
        txtSubject.attributedPlaceholder = NSAttributedString(string: "Subject",
                                                             attributes: [NSAttributedString.Key.foregroundColor:UIColor(named: "theme_lightgray_color")! ,
                                                                          NSAttributedString.Key.font : UIFont(name: fontname.openSansRegular, size: CGFloat(14))])
        txtSubject.font =  UIFont.init(name: fontname.openSansRegular, size: CGFloat(14))
        txtSubject.paddingView(xvalue: 10)
        //Colors.theme_white_color - bgcolor
        configureTextView(textField:txtReport, placeHolder: placeholderText, font: fontname.openSansRegular, fontSize: 14, textColor:UIColor(named: "theme_lightgray_color")!  , cornerRadius: 0, borderColor:UIColor(named: "theme_lightgray_color")! , borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")! )
        
        btnSend.layer.cornerRadius = 2
        btnSend.layer.masksToBounds = true
        btnCancel.layer.cornerRadius = 2
        btnCancel.layer.masksToBounds = true
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.borderColor = UIColor(named: "theme_lightgray_color")!.cgColor //Colors.theme_lightgray_color.cgColor
      
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor(named: "theme_lightgray_color")!.cgColor//Colors.theme_lightgray_color.cgColor
        
        txtReport.textContainerInset = UIEdgeInsets(top: 5, left: 7, bottom: 5, right: 10)
        txtReport.delegate = self
        
        viewAttachment1.layer.cornerRadius = 5
        viewAttachment1.layer.masksToBounds = true
        
        viewAttachment2.layer.cornerRadius = 5
        viewAttachment2.layer.masksToBounds = true
        
        viewAttachment3.layer.cornerRadius = 5
        viewAttachment3.layer.masksToBounds = true
        
        viewRemoveAttachment1.layer.cornerRadius = viewRemoveAttachment1.frame.size.height / 2
        viewRemoveAttachment1.layer.masksToBounds = true
        
        viewRemoveAttachment2.layer.cornerRadius = viewRemoveAttachment2.frame.size.height / 2
        viewRemoveAttachment2.layer.masksToBounds = true
        
        viewRemoveAttachment3.layer.cornerRadius = viewRemoveAttachment3.frame.size.height / 2
        viewRemoveAttachment3.layer.masksToBounds = true
        manageUI()
     }
    func showCard()
    {
        self.view.layoutIfNeeded()
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
//            let topHeight = safeAreaHeight - 430
            let topHeight = safeAreaHeight - 635
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
    }
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnCancelClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    //Mark :- Textview delegate methods
    func textViewDidBeginEditing(_ textView: UITextView) {
         if textView.text == placeholderText
         {
            textView.text = ""
            textView.textColor = UIColor(named: "theme_text_color")!//Colors.theme_black_color
         }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty
        {
            textView.text = placeholderText
            textView.textColor = UIColor(named: "theme_lightgray_color") //Colors.theme_lightgray_color
        }
    }
    //Mark :- Custom Methods
    @IBAction func btnSendClicked()
    {
        if isValidData()
        {
            let fieldName = ["report_image1","report_image2","report_image3"]
            var params : [String:Any] = [:]
            objUserInfo = getUserInfo()
            params["user_id"] = objUserInfo.id ?? ""
            params["hawb_number"] = hawb_number
            params["subject"] = txtSubject.text ?? ""
            params["message"] = txtReport.text ?? ""
            params["attachment[]"] = self.arrAttachmentData
            //print("param is \(params)")
         
            URLManager.shared.URLCallMultipleMultipartTicketData(method: .post, parameters: params, header: true, url: APICall.CreateTicket, showLoader: true, WithName: fieldName, docFileName: arrDocFileNames, uploadData: arrAttachmentData, mimeType: arrMimeType) { [self] (resultDict, status, message) in
                
                if status == true
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(),isForSuccess: true)
                    self.dismiss(animated: true, completion: nil)
                }
                else
                {
                   appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtSubject.text!)
        {
            appDelegate.showToast(message: Messsages.msg_subject, bottomValue: getSafeAreaValue())
            return false
        }
        else if txtReport.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            appDelegate.showToast(message: Messsages.msg_report, bottomValue: getSafeAreaValue())
            return false
        }
        else if txtReport.text == placeholderText
        {
            appDelegate.showToast(message: Messsages.msg_report, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    func TicketAlert(message:String)
    {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let okaction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        //Colors.theme_black_color
        okaction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(okaction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension GenerateTicketVC {
    @IBAction func btnAddAttachment1Clicked(){
        self.view.endEditing(true)
        currentAttachmentIndex = 1
        openImagePickerActionsheet()
    }
    
    @IBAction func btnAddAttachment2Clicked(){
        self.view.endEditing(true)
        currentAttachmentIndex = 2
        openImagePickerActionsheet()
    }
    
    @IBAction func btnAddAttachment3Clicked(){
        currentAttachmentIndex = 3
        openImagePickerActionsheet()
    }
    
    @IBAction func btnRemoveAttachment1Clicked(){
        self.view.endEditing(true)
        
        showAlert(title: "", msg: Messsages.msg_remove_image_confirm, viewController: self) { [self] status in
            if status {
                if arrAttachment.count > 0 {
                    removeFromIndex(index: 0)
                }
                manageUI()
            }
        }
    }
    
    @IBAction func btnRemoveAttachment2Clicked(){
        showAlert(title: "", msg: Messsages.msg_remove_image_confirm, viewController: self) { [self] status in
            if status {
                if arrAttachment.count > 0 {
                    removeFromIndex(index: 1)
                }
                manageUI()
            }
        }
    }
    
    @IBAction func btnRemoveAttachment3Clicked(){
        self.view.endEditing(true)
        
        showAlert(title: "", msg: Messsages.msg_remove_image_confirm, viewController: self) { [self] status in
            if status {
                
                if arrAttachment.count > 0 {
                    removeFromIndex(index: 2)
                }
                manageUI()
            }
        }
    }
    
    func manageUI(){
        
        imgAttachment1.image = nil
        imgAttachment2.image = nil
        imgAttachment3.image = nil
        
        imgAttachment1.isHidden = false
        imgAttachment2.isHidden = false
        imgAttachment3.isHidden = false
        
        viewRemoveAttachment1.isHidden = true
        viewRemoveAttachment2.isHidden = true
        viewRemoveAttachment3.isHidden = true
        
        btnAttachment1.isHidden = false
        btnAttachment2.isHidden = true
        btnAttachment3.isHidden = true
        
        if arrAttachment.count > 0 {
            if arrAttachment.count >= 1 {
                viewRemoveAttachment1.isHidden = false
                btnAttachment1.isHidden = true
                btnAttachment2.isHidden = false
                imgAttachment1.image = arrAttachment[0]
            }
            
            if arrAttachment.count >= 2 {
                viewRemoveAttachment2.isHidden = false
                btnAttachment2.isHidden = true
                btnAttachment3.isHidden = false
                imgAttachment2.image = arrAttachment[1]
            }
            
            if arrAttachment.count >= 3 {
                viewRemoveAttachment3.isHidden = false
                btnAttachment3.isHidden = true
                imgAttachment3.image = arrAttachment[2]
            }
        }
    }
    
    func removeFromIndex(index : Int){
        arrAttachment.remove(at: index)
        arrAttachmentData.remove(at: index)
        arrDocFileNames.remove(at: index)
        arrMimeType.remove(at: index)
    }
}

extension GenerateTicketVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    // MARK: - Custom methods
    func openImagePickerActionsheet(){
        let attributedString = NSAttributedString(string: "Please select option", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15), //your font here
            NSAttributedString.Key.foregroundColor :Colors.theme_gray_color
        ])
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(attributedString, forKey: "attributedTitle")
        
//        let takePhotoAction = UIAlertAction(title: "Take a Photo" , style: .default) { (_ action) in
//            self.openCamera()
//        }
        
        let photoAction = UIAlertAction(title: "Photo Gallery" , style: .default) { (_ action) in
            self.openGallery()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel" , style: .cancel) { (_ action) in
            self.imagePicker.dismiss(animated: true, completion: nil)

        }
        photoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(photoAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            PermissionManager.shared.requestCameraPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.camera
                        imagePicker.allowsEditing = false
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            PermissionManager.shared.requestPhotoPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                        imagePicker.allowsEditing = false
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
        {
            switch url.pathExtension
            {
            case "jpg","jpeg":
                self.mimeType = "image/jpeg"
                break
            case "png":
                self.mimeType = "image/png"
                break
            default:
                // print("No file")
                showAlert(title: "", msg: Messsages.msg_image_type, vc: self)
                return
            }
        }
       
        if let pickedImage = info[.originalImage] as? UIImage
        {
            let dblSize = pickedImage.getSizeIn(mimeType: self.mimeType, type: .megabyte)
            if dblSize > 0 {
                let imgSizeInMb : Double = dblSize
                if imgSizeInMb > 2.0 {
                    appDelegate.showToast(message: Messsages.msg_attachment_should_less_than_2_mb, bottomValue: getSafeAreaValue())
                    picker.dismiss(animated: true, completion: nil)
                    return
                }

                if self.mimeType == ""
                {
                    self.mimeType = "image/png"
                }
                isImageRemoved = 0
                
                let currentTimeStamp = String(Int(NSDate().timeIntervalSince1970))
                self.docFileName = String(format: "%@%@%@", "csf-Report-img-",currentTimeStamp,".png")
                setImageAndData(pickedImage: pickedImage)
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func setImageAndData(pickedImage : UIImage){
        if currentAttachmentIndex == 1 {
            imgAttachment1.contentMode = .scaleAspectFill
        }else if currentAttachmentIndex == 2 {
            imgAttachment2.contentMode = .scaleAspectFill
        }else if currentAttachmentIndex == 3 {
            imgAttachment3.contentMode = .scaleAspectFill
        }
        arrDocFileNames.append(self.docFileName)
        arrAttachment.append(pickedImage)
        arrMimeType.append(self.mimeType)

        let thumb1 = pickedImage
        DispatchQueue.global(qos: .background).async { [self] in
            if self.mimeType == "image/jpeg"
            {
                self.uploadData = pickedImage.jpegData(compressionQuality: 0.3)!
            }
            else if self.mimeType == "image/png"
            {
                self.uploadData = (thumb1.pngData())!
            }
            
            arrAttachmentData.append(self.uploadData)
        }
        manageUI()
    }
}
