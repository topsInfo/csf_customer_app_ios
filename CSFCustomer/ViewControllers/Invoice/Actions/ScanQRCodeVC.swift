//
//  ScanQRCodeVC.swift
//  CSFCustomer
//
//  Created by iMac on 07/04/22.
//

import UIKit
import ScrollableSegmentedControl

class ScanQRCodeVC: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var bottomConst : NSLayoutConstraint!
    @IBOutlet weak var dimmerView : UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblNoQrCodeFound: UILabel!
    @IBOutlet weak var qrCodeActivityIndicator:
    UIActivityIndicatorView!
    // MARK: - Global Variables
    var objRequestDetail : DriveThruRequestDetail = DriveThruRequestDetail()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        DispatchQueue.main.async {
            self.topConst.constant = 64
            self.bottomConst.constant = 0
        }
        dimmerView.backgroundColor = UIColor(named:"theme_bg_color")

        viewContainer.dropShadow()

        btnDownload.layer.cornerRadius = btnDownload.frame.size.height / 2
        btnDownload.layer.masksToBounds = true
        btnDownload.layer.borderWidth = 1
        btnDownload.layer.borderColor = Colors.theme_orange_color.cgColor
        lblDesc.text = "Please keep your phone straight to scan from kiosk machine. If failed, please cancel and retry Please carry your id proof along for security check and here is the OTP required at kiosk 9678 Please do not share your QR Code with anyone as doing this may loose your package."
        
        self.imgQRCode.sd_setImage(with: URL(string:objRequestDetail.qrFilepath), completed: { (image, error, type, url) in
            
            if image != nil {
                self.imgQRCode.image = image
                self.btnDownload.isEnabled = true
            }else {
                self.lblNoQrCodeFound.isHidden = false
                self.btnDownload.isEnabled = false
            }
            self.qrCodeActivityIndicator.isHidden = true
        })
        imgQRCode.contentMode = .scaleAspectFill
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDownloadClicked(_ sender: UIButton) {
        self.showHUD()

        self.downloadFile(InvoiceID:self.objRequestDetail.id ?? "",pdfFileURL:self.objRequestDetail.qrFilepath,downloadedFileType: "QRCode")

    }
}
