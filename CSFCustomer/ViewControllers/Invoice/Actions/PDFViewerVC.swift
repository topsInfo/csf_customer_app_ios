//
//  PDFViewerVC.swift
//  CSFCustomer
//
//  Created by Tops on 23/12/20.
//

import UIKit
import WebKit
class PDFViewerVC: UIViewController,WKNavigationDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var dimmerView : UIView!
    @IBOutlet weak var webView : WKWebView!
    @IBOutlet weak var lblTitle : UILabel!
    
    // MARK: - Global Variable
    var pdfFile : String = ""
    var strTitle : String = ""
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strTitle
        webView.navigationDelegate = self
        print("pdfFile is \(pdfFile)")
        clearWebviewCache()
        
        loadPDFFile(file:pdfFile)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideHUDFromView(view: dimmerView)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func clearWebviewCache(){
        WKWebsiteDataStore.default().removeData(ofTypes: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache], modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
    }
    
    // MARK: - Custom Methods
    func loadPDFFile(file:String)
    {
        if let url: URL = URL(string: file)
        {
            var strFileMimeType : String = ""
            if (url.pathExtension == "pdf"){
                strFileMimeType = "application/pdf"
                self.convertToDataAndLoadToWebView(withMimeType: strFileMimeType, ofURL: url)
            }else{
                webView.load(URLRequest(url: url))
            }
        }
    }
    func convertToDataAndLoadToWebView(withMimeType : String, ofURL : URL){
        DispatchQueue.main.async {
            if let data = try? Data(contentsOf: ofURL) {
                self.webView.load(data, mimeType: withMimeType, characterEncodingName: "", baseURL: ofURL)
            }
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webview delegate methods
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("SHOW PROGRESS")
        self.showHUDOnView(view: dimmerView)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("HIDE PROGRESS")
        self.hideHUDFromView(view: dimmerView)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("HIDE PROGRESS")
        self.hideHUDFromView(view: dimmerView)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        self.hideHUDFromView(view: dimmerView)
        if error._code == -1001 { // TIMED OUT:
            print("HIDE PROGRESS")
            // CODE to handle TIMEOUT
        } else if error._code == -1003 { // SERVER CANNOT BE FOUND
            print("HIDE PROGRESS")
            // CODE to handle SERVER not found
        } else if error._code == -1100 { // URL NOT FOUND ON SERVER
            print("HIDE PROGRESS")
            // CODE to handle URL not found
        } else if error._code == -1200 { // URL NOT FOUND ON SERVER
            print("HIDE PROGRESS")
            // CODE to handle SSL Error and Secure Connection
        }
    }
}
