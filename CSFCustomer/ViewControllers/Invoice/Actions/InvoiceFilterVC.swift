//
//  PreAlertFilterVC.swift
//  CSFCustomer
//
//  Created by Tops on 18/12/20.
//

import UIKit
import DropDown
class InvoiceFilterVC: UIViewController,UITextFieldDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var tblFilter : UITableView!
    @IBOutlet weak var customerContainer : UIView!
    @IBOutlet weak var startDateContainer : UIView!
    @IBOutlet weak var endDateContainer : UIView!
    @IBOutlet weak var txtCustomer : UITextField!
    @IBOutlet weak var txtStartDate : UITextField!
    @IBOutlet weak var txtEndDate : UITextField!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var dimmerView: UIView!
    @IBOutlet weak var btnApply : UIButton!
    @IBOutlet weak var btnCustomer : UIButton!
    @IBOutlet weak var imgDay : UIImageView!
    @IBOutlet weak var imgMonth : UIImageView!
    @IBOutlet weak var imgYear : UIImageView!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var imgDeliveredCheckBox: UIImageView!
    @IBOutlet weak var stackViewCustomer : UIStackView!
    @IBOutlet weak var stackViewDelivered: UIStackView!
    
    // MARK: - Global Variable
    let dropDown = DropDown()
    var arrContainers : [UIView] = [UIView]()
    var arrMemberList : [MemberList] = [MemberList]()
    var selectedMemberID : String  = ""
    var monthComponents = [String]()
    var currentControl = UITextField()
    var isFrom : String = ""
    var arrYears = [String]()
    lazy var monthFormatter : DateFormatter =
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM"
            return formatter
        }()
    lazy var yearFormatter : DateFormatter =
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            return formatter
        }()
    //vars
    var strMemberID : String  = ""
    var strMemberName : String = ""
    var strStartDt : String  = ""
    var strEndDt :String = ""
    var strSearch : String  = ""
    var dateType : String = ""
    
    //Qx-T6131 - Credit management filter
    var isDeliveredByClicked : Bool = false
    var isDelivered : String  = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        showCard()
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black") //UIColor.black.withAlphaComponent(0.5)
        btnCancel.layer.cornerRadius = 2
        btnCancel.layer.masksToBounds = true
        
        arrContainers = [customerContainer,startDateContainer,endDateContainer]
        for item in arrContainers
        {
            item.layer.borderWidth = 1
            item.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
        }
        tblFilter.tableFooterView = UIView()
        txtSearch.paddingView(xvalue: 10)
        setCustomerDropDown()
        configureTextFields()
        //==button
        btnApply.layer.cornerRadius = 2.0
        btnApply.layer.masksToBounds = true
        //        //==set day picker
        //        setDayPicker()
        //==set monthnames
        let formatter = DateFormatter()
        monthComponents = formatter.shortMonthSymbols
        txtStartDate.delegate = self
        txtEndDate.delegate = self
        //==set year picker
        let pastDate = Calendar.current.date(byAdding: .year, value: -5, to: Date())!
        let pastYear = Calendar.current.component(.year, from: pastDate)
        let currentYear = Calendar.current.component(.year, from: Date())
        arrYears = (pastYear...currentYear).map { String($0) }
        //==showData
        showData()
    }
    func configureTextFields()
    {
        //Colors.theme_black_color for all textfield , bgcolor: Colors.theme_white_color
        configureTextField(textField: txtCustomer, placeHolder: "Select Customer", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtStartDate, placeHolder: "Start Date", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtEndDate, placeHolder: "End Date", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        configureTextField(textField: txtSearch, placeHolder: "Search", font: fontname.openSansRegular, fontSize: 14, textColor:  UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: UIColor(named: "theme_textfield_bgcolor")!)
        txtCustomer.paddingView(xvalue: 10)
        txtStartDate.paddingView(xvalue: 10)
        txtEndDate.paddingView(xvalue: 10)
    }
    func showData()
    {
        selectedMemberID = strMemberID
        txtCustomer.text = strMemberName
        txtSearch.text = strSearch
        if dateType  == DateFormatType.Month.dispValue()
        {
            txtStartDate.text = strStartDt
            txtEndDate.text = strEndDt
            setMonthPicker()
        }
        else if dateType == DateFormatType.Year.dispValue()
        {
            txtStartDate.text = strStartDt
            txtEndDate.text = strEndDt
            setYearPicker()
        }
        else
        {
            if let dt = destinationFormatter().date(from: strStartDt)
            {
                let startDt = sourceFormatter().string(from: dt)
                txtStartDate.text = startDt
            }
            if let dt1 = destinationFormatter().date(from: strEndDt)
            {
                let endDt = sourceFormatter().string(from: dt1)
                txtEndDate.text = endDt
            }
            setDayPicker()
        }
        
        if isFrom == "Unpaid"{
            if isDelivered == "1"{
                self.imgDeliveredCheckBox.image = UIImage(named: "checkbox")
                isDeliveredByClicked = true
            }else{
                self.imgDeliveredCheckBox.image = UIImage(named: "uncheckbox")
                isDeliveredByClicked = false
            }
        }
    }
    func setDayPicker()
    {
        txtStartDate.placeholder = "Start Date"
        txtEndDate.placeholder = "End Date"
        dateType = DateFormatType.Day.dispValue()
        imgDay.image = UIImage(named: "radio_btn")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        
        //        if txtStartDate.text == "" && txtEndDate.text == ""
        //        {
        //            txtStartDate.text =  dateFormatter.string(from: Date())
        //            txtEndDate.text = txtStartDate.text
        //        }
        txtStartDate.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed),selectorChange:#selector(dateValueChanged),formatType:DateFormatType.Day.dispValue(),from:"InvoiceFilter")
        txtEndDate.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed),selectorChange:#selector(dateValueChanged),formatType:DateFormatType.Day.dispValue(),from:"InvoiceFilter")
    }
    func setMonthPicker()
    {
        txtStartDate.placeholder = "Month"
        txtEndDate.placeholder = "Month"
        dateType = DateFormatType.Month.dispValue()
        imgMonth.image = UIImage(named: "radio_btn")
        let monthno = Calendar.current.component(.month, from: Date())
        //        if txtStartDate.text == "" && txtEndDate.text == ""
        //        {
        //            txtStartDate.text = monthComponents[monthno-1]
        //            txtEndDate.text = txtStartDate.text
        //        }
        let picker = txtStartDate.addCustomPicker(target: self, selector: #selector(doneButtonPressed))
        let picker1 =  txtEndDate.addCustomPicker(target: self, selector: #selector(doneButtonPressed))
        picker.delegate = self
        picker1.delegate = self
        if monthno > 0
        {
            picker.selectRow(monthno-1, inComponent: 0, animated: true)
            picker1.selectRow(monthno-1, inComponent: 0, animated: true)
        }
    }
    func setYearPicker()
    {
        txtStartDate.placeholder = "Year"
        txtEndDate.placeholder = "Year"
        imgYear.image = UIImage(named: "radio_btn")
        dateType = DateFormatType.Year.dispValue()
        let year = Calendar.current.component(.year, from: Date())
        //        if txtStartDate.text == "" && txtEndDate.text == ""
        //        {
        //            txtStartDate.text = String(year)
        //            txtEndDate.text = txtStartDate.text
        //        }
        let picker = txtStartDate.addCustomPicker(target: self, selector: #selector(doneButtonPressed))
        let picker1 =  txtEndDate.addCustomPicker(target: self, selector: #selector(doneButtonPressed))
        picker.delegate = self
        picker.selectRow(5, inComponent: 0, animated: true)
        picker1.delegate = self
        picker1.selectRow(5, inComponent: 0, animated: true)
    }
    func setCustomerDropDown()
    {
        dropDown.width = customerContainer.bounds.size.width
        dropDown.shadowRadius = 0

        dropDown.direction = .any
        dropDown.anchorView = self.btnCustomer // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x:0 , y:(txtCustomer.bounds.height))
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)

        dropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDown.separatorColor = UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        dropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDown.textColor = UIColor(named:"theme_text_color")!
        dropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color

        dropDown.dataSource = arrMemberList.map({$0.name}) 
        dropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                self?.selectedMemberID  = (self?.arrMemberList[index].id ?? "")
                self?.txtCustomer.text = item
                self?.dropDown.hide()
            }
        }
    }
    func showCard()
    {
        self.view.layoutIfNeeded()
        
        var padding : CGFloat = 510
        if isFrom == "Upcoming"{
            
            padding = 380
            
            //This will hide customer dropdown
            stackViewCustomer.isHidden = true
            
            //Qx-T6131 - Credit management filter
            stackViewDelivered.isHidden = true
            
        }else if isFrom == "Unpaid"{
        
            padding = 510
            
            //This will show customer dropdown
            stackViewCustomer.isHidden = false
            
            //Qx-T6131 - Credit management filter
            stackViewDelivered.isHidden = false
        }
        else{
            
            padding = 455
            
            //This will show customer dropdown
            stackViewCustomer.isHidden = false
            
            //Qx-T6131 - Credit management filter
            stackViewDelivered.isHidden = true
            
        }
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - padding
            topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnCustomerClicked()
    {
        self.dropDown.show()
    }
    @IBAction func btnClearClicked()
    {
        txtCustomer.text = ""
        selectedMemberID = ""
    }
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnDateFormatsClicked(_ sender: UIButton)
    {
        imgDay.image = UIImage(named: "radio_btn2")
        imgMonth.image = UIImage(named: "radio_btn2")
        imgYear.image = UIImage(named: "radio_btn2")
        self.view.endEditing(true)
        txtStartDate.text = ""
        txtEndDate.text = ""
        if sender.tag == 101
        {
            setDayPicker()
        }
        else if sender.tag == 102
        {
            setMonthPicker()
        }
        else if sender.tag == 103
        {
            setYearPicker()
        }
    }
    @objc func doneButtonPressed()
    {
        if dateType == DateFormatType.Day.dispValue()
        {
            if let  datePicker = currentControl.inputView as? UIDatePicker {
                setDate(datePicker: datePicker)
            }
        }
        else if dateType == DateFormatType.Month.dispValue()
        {
            if let  pickerView = currentControl.inputView as? UIPickerView {
                let row = pickerView.selectedRow(inComponent: 0)
                pickerView.selectRow(row, inComponent: 0, animated: false)
                setData(name:self.monthComponents[row])
            }
        }
        else if dateType == DateFormatType.Year.dispValue()
        {
            if let  pickerView = currentControl.inputView as? UIPickerView {
                let row = pickerView.selectedRow(inComponent: 0)
                pickerView.selectRow(row, inComponent: 0, animated: false)
                setData(name:self.arrYears[row])
            }
        }
        currentControl.resignFirstResponder()
        self.view.endEditing(true)
    }
    @objc func dateValueChanged(_ datePicker: UIDatePicker) {
        // setDate(datePicker: datePicker)
    }
    func setData(name:String)
    {
        currentControl.text = name
        if (currentControl == txtStartDate && txtEndDate.text == "") || (currentControl == txtEndDate && txtStartDate.text == "")
        {
            txtStartDate.text = name
            txtEndDate.text = name
        }
        else
        {
            if dateType == DateFormatType.Month.dispValue()
            {
                let sDate = monthFormatter.date(from: txtStartDate.text!)!
                let dDate = monthFormatter.date(from: txtEndDate.text!)!
                compareTwoDates(date1:sDate,date2:dDate)
            }
            else if dateType == DateFormatType.Year.dispValue()
            {
                let sDate = yearFormatter.date(from: txtStartDate.text!)!
                let dDate = yearFormatter.date(from: txtEndDate.text!)!
                compareTwoDates(date1:sDate,date2:dDate)
            }
        }
    }
    func setDate(datePicker:UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        currentControl.text = dateFormatter.string(from: datePicker.date)
        if (currentControl == txtStartDate && txtEndDate.text == "") || (currentControl == txtEndDate && txtStartDate.text == "")
        {
            if txtEndDate.text == ""
            {
                txtEndDate.text = dateFormatter.string(from: datePicker.date)
            }
            else if txtStartDate.text == ""
            {
                txtStartDate.text = dateFormatter.string(from: datePicker.date)
            }
        }
        else
        {
            let date1 = dateFormatter.date(from: txtStartDate.text!)!
            let date2 = dateFormatter.date(from: txtEndDate.text!)!
            compareTwoDates(date1:date1,date2:date2)
        }
        
    }
    func compareTwoDates(date1:Date,date2:Date)
    {
        switch date1.compare(date2) {
        case .orderedAscending:
            // print("asc")
            break
        case .orderedSame:
            //print("same")
            if currentControl == txtStartDate
            {
                txtEndDate.text = txtStartDate.text
            }
            else
            {
                txtStartDate.text = txtEndDate.text
            }
            break
        case .orderedDescending:
            // print("des")
            if currentControl == txtStartDate
            {
                txtEndDate.text = txtStartDate.text
            }
            else
            {
                txtStartDate.text = txtEndDate.text
            }
            break
            
        }
    }
    @IBAction func btnApplyClicked()
    {
        var startDt : String = ""
        var endDt : String = ""

        if txtStartDate.text == "" && txtEndDate.text == ""
        {
            dateType = ""
        }
        else if dateType == DateFormatType.Day.dispValue()
        {
            if let dt = sourceFormatter().date(from: txtStartDate.text ?? "")
            {
                startDt = destinationFormatter().string(from: dt)
            }
            if let dt1 = sourceFormatter().date(from: txtEndDate.text ?? "")
            {
                endDt = destinationFormatter().string(from: dt1)
            }
        }
        else if dateType == DateFormatType.Month.dispValue() || dateType ==  DateFormatType.Year.dispValue()
        {
            startDt = txtStartDate.text!
            endDt = txtEndDate.text!
        }
        let filterDict :[String: Any] = [
            "member_id": selectedMemberID ,
            "member_name" : txtCustomer.text ?? "",
            "start_date" : startDt ,
            "end_date": endDt,
            "date_type" : self.dateType ,
            "search_text" : txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
        ]
        if isFrom == "PickupRequest"
        {
            NotificationCenter.default.post(name: Notification.Name("UnderliverdListFilter"), object: nil,userInfo: filterDict)
        }else if isFrom == "Upcoming"{
            
            let filterDict :[String: Any] = [
                "start_date" : startDt ,
                "end_date": endDt,
                "date_type" : self.dateType ,
                "search_content" : txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            ]
            
            NotificationCenter.default.post(name: Notification.Name("InvoiceAtMiamiFilterApplied"), object: nil,userInfo: filterDict)
        }
        else if isFrom == "Unpaid"{
            let filterDict :[String: Any] = [
                "member_id": selectedMemberID ,
                "member_name" : txtCustomer.text ?? "",
                "start_date" : startDt ,
                "end_date": endDt,
                "date_type" : self.dateType ,
                "search_text" : txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
                "is_delivered" : isDelivered,
            ]
            NotificationCenter.default.post(name: Notification.Name("InvoiceFilterApplied"), object: nil,userInfo: filterDict)
        }
        else
        {
            NotificationCenter.default.post(name: Notification.Name("InvoiceFilterApplied"), object: nil,userInfo: filterDict)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDeliveredCheckBoxClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        isDeliveredByClicked = !isDeliveredByClicked
        
        if isDeliveredByClicked{
            isDelivered = "1"
            self.imgDeliveredCheckBox.image = UIImage(named: "checkbox")
        }else{
            isDelivered = "0"
            self.imgDeliveredCheckBox.image = UIImage(named: "uncheckbox")
        }
    }
    
    // MARK: - Textfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentControl = textField
        if textField.text != ""
        {
            if dateType == DateFormatType.Day.dispValue()
            {
                if let dt = sourceFormatter().date(from: textField.text ?? "")
                {
                    if let  datePicker = currentControl.inputView as? UIDatePicker {
                        datePicker.setDate(dt, animated: false)
                    }
                }
            }
            else if dateType == DateFormatType.Month.dispValue()
            {
                if let  monthPicker = currentControl.inputView as? UIPickerView {
                    let index = (monthComponents as NSArray).index(of: textField.text!)
                    monthPicker.selectRow(index, inComponent: 0, animated: true)
                }
            }
            else if dateType == DateFormatType.Year.dispValue()
            {
                if let  yearPicker = currentControl.inputView as? UIPickerView {
                    let index = (arrYears as NSArray).index(of: textField.text!)
                    yearPicker.selectRow(index, inComponent: 0, animated: true)
                }
            }
        }
        
    }
}
extension InvoiceFilterVC : UIPickerViewDataSource,UIPickerViewDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if dateType == DateFormatType.Month.dispValue()
        {
            return self.monthComponents.count
        }
        return self.arrYears.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if dateType == DateFormatType.Month.dispValue()
        {
            return self.monthComponents[row]
        }
        return arrYears[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if dateType == DateFormatType.Month.dispValue()
        {
            currentControl.text =  monthComponents[row]
        }
        else
        {
            currentControl.text =  arrYears[row]
        }
    }
}
