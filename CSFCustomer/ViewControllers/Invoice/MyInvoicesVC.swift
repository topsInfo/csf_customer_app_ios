//
//  MyInvoicesVC.swift
//  CSFCustomer
//
//  Created by Tops on 28/11/20.
//

import UIKit
import SwiftyJSON

class MyInvoicesVC: UIViewController, UIGestureRecognizerDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var viewTotalRecord : TotalRecordView!
    @IBOutlet weak var cnstTotalRecordsTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var tblInvoiceList : UITableView!
    @IBOutlet weak var filterView : UIView!
    @IBOutlet weak var btnClearFilter : UIView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
//    @IBOutlet weak var tblBottomConstant : NSLayoutConstraint!
//    @IBOutlet weak var btnCartBottomConstant: NSLayoutConstraint!
    @IBOutlet weak var btnCart : UIButton!
    @IBOutlet weak var btnExportReport : UIButton!
    @IBOutlet weak var btnClearFilterHeightConstant: NSLayoutConstraint!
    // MARK: - Global Variable
    var refreshControl = UIRefreshControl()
    var type : String = InvoiceType.MyInvoice.dispValue()
    var objUserInfo = UserInfo()
    var invoiceCellHeight : CGFloat = 154
    var arrSections : [Int] = [Int]()
    var arrSelectedSections : [Int] = [Int]()
    var isLoadingList : Bool = false
    var pageNo : Int = 1
    var totalPages : Int = 0
    var arrList : [InvoiceList] = [InvoiceList]()
    var arrMemberList : [MemberList] = [MemberList]()
    
    var arrMiamiInvoicelist : [InvoiceAtMiamiList] = [InvoiceAtMiamiList]()
    var arrMiamiSections : [Int] = [Int]()
    var arrMiamiSelectedSections : [Int] = [Int]()

    var preAlertID : String = ""
    var isShowLoader : Bool = true
    var filename : String  = ""
    //filter vars
    var strMemberID : String = ""
    var strMemberName  : String = ""
    var strStartDate : String  = ""
    var strEndDate : String  = ""
    var strDateType :String = ""
    var strSearch : String = ""
    var isShowCartOption : Bool!
    var isShowOpenTicketOption : Bool!
    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0
    var strOlderInvoiceDate : String = ""
    var strDriveThruReqURL : String = ""

    //Qx-T6131 - Credit management filter
    var isDelivered : String  = ""

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        objUserInfo = getUserInfo()
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(getFilterRecords(_:)), name: NSNotification.Name(rawValue: "InvoiceFilterApplied"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showFilterVC(_:)), name: Notification.Name("FilterClicked"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(segmentChanged(_:)), name: NSNotification.Name(rawValue: "SegmentChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getCartCounter(_:)), name: Notification.Name("InvoiceDeleted"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords(_:)), name: Notification.Name("PickupRequestCompleted"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getFilterRecords(_:)), name: NSNotification.Name(rawValue: "InvoiceAtMiamiFilterApplied"), object: nil)

        tblInvoiceList.dataSource = self
        tblInvoiceList.delegate = self
        tblInvoiceList.estimatedRowHeight = 154
        tblInvoiceList.rowHeight = UITableView.automaticDimension
        tblInvoiceList.estimatedSectionHeaderHeight = 80
        tblInvoiceList.sectionHeaderHeight = UITableView.automaticDimension
        tblInvoiceList.tableFooterView? = UIView()
        btnClearFilterHeightConstant.constant = 0
        btnClearFilter.layer.cornerRadius = 2.0
        btnClearFilter.layer.masksToBounds = true
        btnClearFilter.isHidden = true
        lblNoRecordFound.isHidden = true
        btnCart.isHidden = true
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(getLatestRecords(_:)), for: .valueChanged)
        tblInvoiceList.addSubview(refreshControl) // not required when using UITableViewController
        
        cnstTotalRecordsTitleHeight.constant = 0
        
        if type == InvoiceType.Upcoming.rawValue {
            viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrMiamiSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount)
            viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrMiamiSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PACKAGES)
        }else{
            viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount)
            viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_INVOICE)
        }
    }
    @objc func segmentChanged(_ notification: NSNotification)
    {
        clearData()
    }
    func getInvoiceList(isShowLoader: Bool)
    {
        if type == InvoiceType.Upcoming.rawValue {
            getUpcomingInvoiceData(isShowLoader: isShowLoader)
        }else {
            getInvoiceData(isShowLoader: isShowLoader)
        }
    }
    
    func getInvoiceData(isShowLoader: Bool){
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["page_id"] = pageNo
        params["type"] = type
        params["search_text"] = strSearch
        params["member_id"] = strMemberID
        params["start_date"] = strStartDate
        params["end_date"] = strEndDate
        params["date_type"] =  strDateType
        
        if type == InvoiceType.UnpaidInvoice.rawValue {
            params["is_delivered"] =  isDelivered
        }
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.MyInvoiceList, showLoader: isShowLoader) { (resultDict, status, message) in
            self.tblInvoiceList.isHidden = false
            if status == true
            {
                self.isLoadingList = false
                self.refreshControl.endRefreshing()
                if self.pageNo == 1
                {
                    self.arrList.removeAll()
                }
                let objMyInvoiceList = Invoice.init(fromJson: JSON(resultDict))
                self.strDriveThruReqURL = objMyInvoiceList.driveThruReqUrl ?? ""

                if self.arrList.count > 0
                {
                    self.arrList.append(contentsOf: objMyInvoiceList.list)
                    for _ in 0..<objMyInvoiceList.list.count
                    {
                        self.arrSections.append(0)
                        self.arrSelectedSections.append(0)
                    }
                }
                else
                {
                    self.arrList = objMyInvoiceList.list
                    self.arrSections.removeAll()
                    self.arrSelectedSections.removeAll()
                    for _ in 0..<self.arrList.count
                    {
                        self.arrSections.append(0)
                        self.arrSelectedSections.append(0)
                    }
                }
                self.totalRecordsCount = objMyInvoiceList.totalCount ?? 0
                if self.arrList.count > 0
                {
                    self.lblNoRecordFound.isHidden = true
                    self.arrMemberList = objMyInvoiceList.memberList
                    self.btnExportReport.isHidden = false
                }
                else
                {
                    self.lblNoRecordFound.isHidden = false
                    self.btnCart.isHidden = true
                    self.btnExportReport.isHidden = true
                }
                self.totalPages = objMyInvoiceList.totalPage ?? 0
                //new change
                let cartDict :[String: Any] = [
                    "cartCounter" : objMyInvoiceList.cartCount ?? 0
                ]
                NotificationCenter.default.post(name: Notification.Name("CartCounterUpdated"), object: nil,userInfo: cartDict)
                DispatchQueue.main.async {
                    self.tblInvoiceList.reloadData()
                }
                
                if self.totalRecordsCount > 0 {
                    self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
                }else {
                    self.cnstTotalRecordsTitleHeight.constant = 0
                }
                self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
                self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_INVOICE)

            }
            else
            {
                self.refreshControl.endRefreshing()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    @objc func getCartCounter(_ notification: NSNotification)
    {
        getData()
    }
    @objc func getLatestRecords(_ sender: AnyObject)
    {
        print("**** v1.7.6_InvoiceListUpdated ****")
        btnCart.isHidden = true
        self.pageNo = 1
        getInvoiceList(isShowLoader: false)
    }
    func getData()
    {
        btnCart.isHidden = true
        pageNo = 1
        //  self.arrList.removeAll()
        getInvoiceList(isShowLoader: true)
    }
    @objc func getFilterRecords(_ notification: NSNotification)
    {
        btnCart.isHidden = true
        self.view.layoutIfNeeded()
        
        if type == InvoiceType.Upcoming.rawValue {
            pageNo = 1
//            strSearch = notification.userInfo?["search_text"] as? String ?? ""
//            strSearchType = notification.userInfo?["search_type"] as? String ?? ""
            
            strStartDate = notification.userInfo?["start_date"] as? String ?? ""
            strEndDate = notification.userInfo?["end_date"] as? String ?? ""
            strDateType = notification.userInfo?["date_type"] as? String ?? ""
            strSearch = notification.userInfo?["search_content"] as? String ?? ""
            btnClearFilterHeightConstant.constant = 45
            btnClearFilter.isHidden = false
            getInvoiceList(isShowLoader: true)
        }else{
            pageNo = 1
            strMemberID = notification.userInfo?["member_id"] as? String ?? ""
            strMemberName = notification.userInfo?["member_name"] as? String ?? ""
            strStartDate = notification.userInfo?["start_date"] as? String ?? ""
            strEndDate = notification.userInfo?["end_date"] as? String ?? ""
            strDateType = notification.userInfo?["date_type"] as? String ?? ""
            strSearch = notification.userInfo?["search_text"] as? String ?? ""
            isDelivered = notification.userInfo?["is_delivered"] as? String ?? ""
            btnClearFilterHeightConstant.constant = 45
            btnClearFilter.isHidden = false
            getInvoiceList(isShowLoader: true)
        }
    }
    @objc func btnViewClicked(sender:UIButton)
    {
        var params : [String:Any] = [:]
        let objInvoice = getInvoiceObject(tag: sender.tag)
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"] = objInvoice.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ViewInvoice, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                let objInvoiceDetail = InvoiceDetail(fromJson: JSON(resultDict))
                if let vc = appDelegate.getViewController("InvoiceDetailVC", onStoryboard: "Invoice") as? InvoiceDetailVC {
                    vc.userID = self.objUserInfo.id ?? ""
                    vc.isFullRefund = objInvoiceDetail.fullRefund ?? ""
                    vc.objInvoiceDetail = objInvoiceDetail
                    if let status = objInvoiceDetail.paidStatus as? String, status == "0"
                    {
                        vc.isPaid = false
                    }
                    else
                    {
                        vc.isPaid = true
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    func clearData()
    {
        if type == InvoiceType.Upcoming.rawValue {
            strStartDate = ""
            strEndDate = ""
            strDateType = ""
            strSearch =  ""
            pageNo = 1
        }else{
            strMemberID =  ""
            strStartDate = ""
            strEndDate = ""
            strDateType = ""
            strSearch =  ""
            strMemberName = ""
            pageNo = 1
        }
        isDelivered = ""
        self.btnClearFilter.isHidden = true
        btnClearFilterHeightConstant.constant = 0
        tblInvoiceList.layoutIfNeeded()
        getInvoiceList(isShowLoader: true)
    }
    @objc func imgTapped(sender:UITapGestureRecognizer) //new change
    {
        let tag = sender.view!.tag
        if arrSelectedSections[tag] == 0
        {
            arrSelectedSections[tag] = 1
        }
        else
        {
            arrSelectedSections[tag] = 0
        }
        DispatchQueue.main.async {
            // UIView.setAnimationsEnabled(true)
            let section = NSIndexSet(index: tag)
            self.tblInvoiceList.reloadSections(section as IndexSet, with: .none)
            self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
            self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_INVOICE)
        }
    }
    
    @objc func stackViewHeaderClicked(sender:UITapGestureRecognizer)
    {
        if type == InvoiceType.Upcoming.rawValue {
            let tag = sender.view!.tag
            if arrMiamiSections.count > 0
            {
                if arrMiamiSections[tag] == 0
                {
                    arrMiamiSections[tag] = 1
                }
                else
                {
                    arrMiamiSections[tag] = 0
                }
                
                DispatchQueue.main.async {
                    // UIView.setAnimationsEnabled(true)
                    let section = NSIndexSet(index: tag)
                    self.tblInvoiceList.reloadSections(section as IndexSet, with: .none)
                }
            }
        }else{
            //print("stackViewHeaderClicked")
            let tag = sender.view!.tag
            
            if arrSections.count > 0 {
                if arrSections[tag] == 0
                {
                    arrSections[tag] = 1
                }
                else
                {
                    arrSections[tag] = 0
                }
                DispatchQueue.main.async {
                    // UIView.setAnimationsEnabled(true)
                    let section = NSIndexSet(index: tag)
                    self.tblInvoiceList.reloadSections(section as IndexSet, with: .none)
                }
            }
        }
    }
    
    func getInvoiceObject(tag:Int) -> InvoiceList
    {
        if arrList.count > 0
        {
            return arrList[tag]
        }
        return InvoiceList()
    }
    @objc func showFilterVC(_ notification: NSNotification)
    {
        tblInvoiceList.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        if type == InvoiceType.Upcoming.rawValue {
            if let filterVC = appDelegate.getViewController("InvoiceFilterVC", onStoryboard: "Invoice") as? InvoiceFilterVC {
                //        filterVC.arrShippingMethods = self.arrShippingMethods
                filterVC.isFrom = "Upcoming"
                filterVC.arrMemberList = arrMemberList
                filterVC.strMemberID = self.strMemberID
                filterVC.strStartDt = self.strStartDate
                filterVC.strEndDt = self.strEndDate
                filterVC.dateType = self.strDateType
                filterVC.strSearch = self.strSearch
                filterVC.strMemberName = self.strMemberName
                filterVC.modalPresentationStyle = .overFullScreen
                self.present(filterVC,animated: true, completion: nil)
            }
        }else{
            if let filterVC = appDelegate.getViewController("InvoiceFilterVC", onStoryboard: "Invoice") as? InvoiceFilterVC {
                //        filterVC.arrShippingMethods = self.arrShippingMethods
                filterVC.arrMemberList = arrMemberList
                filterVC.strMemberID = self.strMemberID
                filterVC.strStartDt = self.strStartDate
                filterVC.strEndDt = self.strEndDate
                filterVC.dateType = self.strDateType
                filterVC.strSearch = self.strSearch
                filterVC.strMemberName = self.strMemberName
                
                if type == InvoiceType.UnpaidInvoice.rawValue{
                    filterVC.isFrom = "Unpaid"
                    filterVC.isDelivered = self.isDelivered
                }
                
                filterVC.modalPresentationStyle = .overFullScreen
                self.present(filterVC,animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnClearFilterClicked()
    {
        clearData()
    }
    
    @IBAction func btnExportReportClicked(_ sender: UIButton) {
        print("btnExportReportClicked - MyInvoicesVC")
        
        var params : [String:Any] = [:]
        self.showHUD()
        let objUserInfo = getUserInfo()
        
        if type == InvoiceType.Upcoming.rawValue {
            params["user_id"] = objUserInfo.id ?? ""
            params["search_content"] = strSearch
            params["start_date"] = strStartDate
            params["end_date"] = strEndDate
            params["date_type"] =  strDateType
        }else{
            params["user_id"] = objUserInfo.id ?? ""
            params["type"] = type
            params["search_text"] = strSearch
            params["member_id"] = strMemberID
            params["start_date"] = strStartDate
            params["end_date"] = strEndDate
            params["date_type"] =  strDateType
        }
        
        if type == InvoiceType.UnpaidInvoice.rawValue {
            params["is_delivered"] =  isDelivered
        }
        
        
        var strAPIUrl : String = ""
        if type == InvoiceType.Upcoming.rawValue {
            strAPIUrl = APICall.exportReceipt
        }else{
            strAPIUrl = APICall.exportInvoices
        }
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: strAPIUrl, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                if let filePath = (resultDict as NSDictionary).value(forKey: "link") as? String
                {
                    self.downloadFile(InvoiceID:"" ,pdfFileURL:filePath, downloadedFileType: "Report")
                }
                else
                {
                    self.hideHUD()
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnViewReceiptTapped(sender:UITapGestureRecognizer){
        if let receiptViewerVC = appDelegate.getViewController("ReceiptViewerVC", onStoryboard: "Invoice") as? ReceiptViewerVC {
            receiptViewerVC.intSelectedIndex = 0
            receiptViewerVC.currentPage = 0
            
            let tag = sender.view!.tag
            
            var arrDocuments : [String] = [String]()
            
            if type == InvoiceType.Upcoming.rawValue {
                arrDocuments = self.arrMiamiInvoicelist[tag].documentsArr
            }else{
                arrDocuments = self.arrList[tag].documentsArr
            }
            
            var arrFinalDocuments : [String] = [String]()
            arrFinalDocuments = arrDocuments.filter({ $0 != ""})
            if arrFinalDocuments.count > 0 {
                receiptViewerVC.arrDocuments = arrFinalDocuments
                self.navigationController?.pushViewController(receiptViewerVC, animated: true)
            }
        }
    }
    
    @objc func btnMoreTapped(sender:UITapGestureRecognizer)
    {
        //print("More tapped")
        var totalAmount : Float = 0
        let tag = sender.view!.tag
        
        if arrList.count > 0 {
            let objList = getInvoiceObject(tag: tag)
            
            if objList.status == PaymentStatus.UnPaid.dispValue()
            {
                //totalAmount = Float((objList.totalUsd ?? "0")!)! //new change apr-2021
                if let totalUSD = objList.totalUsd,totalUSD != ""
                {
                    totalAmount = Float(totalUSD)!
                }
                
            }
            if objList.status == PaymentStatus.UnPaid.dispValue() && totalAmount > 0
            {
                isShowCartOption = true
            }
            else
            {
                isShowCartOption = false
            }
            if let isAllowedPickupRequest = objList.allowRequest, (isAllowedPickupRequest == 1 && type == InvoiceType.PackageRequest.dispValue())
            {
                isShowOpenTicketOption = true
            }
            else
            {
                isShowOpenTicketOption = false
            }
        }
        
        showInvoiceActionsVC(index: tag)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Do sonthing
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                self.tblInvoiceList.reloadData()
            }
        }else{
            self.tblInvoiceList.reloadData()
        }
    }
    
}
extension MyInvoicesVC : UITableViewDataSource,UITableViewDelegate
{
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        if type == InvoiceType.Upcoming.rawValue {
            return arrMiamiInvoicelist.count
        }else {
            return arrList.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if type == InvoiceType.Upcoming.rawValue {
            if arrMiamiSections.count > 0
            {
                if arrMiamiSections[indexPath.section] == 1
                {
                    return UITableView.automaticDimension
                }
            }
            else
            {
                return 0
            }
        }else{
            if arrSections.count > 0
            {
                if arrSections[indexPath.section] == 1
                {
                    return UITableView.automaticDimension
                }
            }
            else
            {
                return 0
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if type == InvoiceType.Upcoming.rawValue {
            let HeaderCellIdentifier : String  = "InvoiceHeaderCell"
            if let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCellIdentifier) as? InvoiceHeaderCell {
  
                cell.lblName.isUserInteractionEnabled = true
                cell.lblInvoiceNo.isUserInteractionEnabled = true

                cell.stackViewHeader.tag = section
                cell.stackViewHeader.isUserInteractionEnabled = true
                
                cell.contentView.backgroundColor =  UIColor(named: "theme_bg_color")
                cell.backgroundColor = UIColor(named: "theme_bg_color")

                let tapGestureStackViewHeader = UITapGestureRecognizer(target: self, action: #selector(stackViewHeaderClicked(sender:)))
                cell.stackViewHeader.addGestureRecognizer(tapGestureStackViewHeader)

                if arrMiamiInvoicelist.count > 0
                {
                    let objList = getMiamiInvoiceObject(tag: section)

                    /*
                     - Date                     Shipment status
                     - HAWB Number
                     - Shipping Method - Air/Ocean
                     */

                    cell.lblDate.text = objList.createdAt ?? ""
//                    cell.lblName.text = objList.hawbNumber ?? ""
                    cell.lblInvoiceNo.text = objList.hawbNumber ?? ""
                    cell.lblAmount.text = objList.status ?? ""
                }
                cell.lblName.isHidden = true
                cell.viewName.isHidden = true
                
                cell.moreContainer.isHidden = true
                cell.imgChecked.isHidden = true
                cell.colorBar.backgroundColor = Colors.theme_black_color
                
                if arrMiamiSections.count > 0
                {
                    if  arrMiamiSections[section] == 1
                    {
                        cell.imgUpDown.image = UIImage(named: "upArrow")
                        cell.longSeperator.isHidden = true
                        cell.shortSeperator.isHidden = false
                    }
                }
                else
                {
                    cell.imgUpDown.image = UIImage(named: "Down-1")
                    cell.longSeperator.isHidden = false
                    cell.shortSeperator.isHidden = true
                }
                
                cell.viewMore.isHidden = true
                
                if self.arrMiamiInvoicelist.count > 0 {
                    if let arrReceipt = self.arrMiamiInvoicelist[section].documentsArr {
                        print("Receipt count is \(arrReceipt.count)")
                        
                        if arrReceipt.count > 0 {
                            cell.viewReceipt.isHidden = false
                        }else{
                            cell.viewReceipt.isHidden = true
                        }
                    }
                    cell.receiptContainer.backgroundColor = Colors.theme_yellow_color
                    cell.btnViewReceipt.isUserInteractionEnabled = true
                    cell.btnViewReceipt.tag = section
                    let btntapViewReciept = UITapGestureRecognizer(target: self, action: #selector(btnViewReceiptTapped(sender:)))
                    cell.btnViewReceipt.addGestureRecognizer(btntapViewReciept)
                    cell.btnViewReceipt.bringSubviewToFront(cell.contentView)
                }else{
                    cell.viewReceipt.isHidden = true
                }
                
                return cell.contentView
            }
        }
        else{
            var totalAmount : Float = 0
            let objList = getInvoiceObject(tag: section)
            let HeaderCellIdentifier : String  = "InvoiceHeaderCell"
            if let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCellIdentifier) as? InvoiceHeaderCell {
                
                cell.lblName.isHidden = false
                cell.viewName.isHidden = false
                
                cell.lblName.isUserInteractionEnabled = true
                cell.lblInvoiceNo.isUserInteractionEnabled = true
                
                //====
                cell.stackViewHeader.tag = section
                cell.stackViewHeader.isUserInteractionEnabled = true
                
                //tap and gesture
                let tapGestureStackViewHeader = UITapGestureRecognizer(target: self, action: #selector(stackViewHeaderClicked(sender:)))
                cell.stackViewHeader.addGestureRecognizer(tapGestureStackViewHeader)
                //======
                
                //Checkbox tapped for selection
                let imgtapGesture = UITapGestureRecognizer(target: self, action: #selector(imgTapped(sender:)))
                cell.imgChecked.addGestureRecognizer(imgtapGesture)
                cell.imgChecked.tag = section
                cell.imgChecked.isUserInteractionEnabled = true
                if arrSections.count > 0
                {
                    if  arrSections[section] == 1
                    {
                        cell.imgUpDown.image = UIImage(named: "upArrow")
                        cell.longSeperator.isHidden = true
                        cell.shortSeperator.isHidden = false
                    }
                }
                else
                {
                    cell.imgUpDown.image = UIImage(named: "Down-1")
                    cell.longSeperator.isHidden = false
                    cell.shortSeperator.isHidden = true
                }
                //change background color of selected cell
                if arrSelectedSections.count > 0
                {
                    if  arrSelectedSections[section] == 1
                    {
                        cell.contentView.backgroundColor = UIColor(named: "theme_lightblue_color")//Colors.theme_lightblue_color
                        cell.backgroundColor =  UIColor(named: "theme_lightblue_color") //Colors.theme_lightblue_color
                        cell.imgChecked.image = UIImage(named: "checkbox")
                        cell.imgChecked.isHidden = false
                    }
                    else
                    {
                        cell.contentView.backgroundColor =  UIColor(named: "theme_bg_color") //Colors.theme_white_color
                        cell.backgroundColor = UIColor(named: "theme_bg_color") //Colors.theme_white_color
                        if objList.status == PaymentStatus.UnPaid.dispValue()
                        {
                            if let totalUSD = objList.totalUsd,totalUSD != ""
                            {
                                totalAmount = Float(totalUSD)!
                            }
                        }
                        if type == InvoiceType.PackageRequest.dispValue()
                        {
                            cell.imgChecked.isHidden = false
                            cell.imgChecked.image = UIImage(named: "uncheckbox")
                        }
                        else if (objList.status == PaymentStatus.UnPaid.dispValue() && totalAmount > 0 && type == InvoiceType.UnpaidInvoice.dispValue())
                        {
                            cell.imgChecked.isHidden = false
                            cell.imgChecked.image = UIImage(named: "uncheckbox")
                        }
                        else
                        {
                            cell.imgChecked.isHidden = true
                        }
                        
                    }
                    if arrSelectedSections.contains(1) && type == InvoiceType.PackageRequest.dispValue()
                    {
                        btnCart.isHidden = false
                        btnCart.setImage(UIImage(named: "Package_Request"), for: .normal)
                        btnCart.tag = 101
                    }
                    else if arrSelectedSections.contains(1)
                    {
                        btnCart.isHidden = false
                        btnCart.setImage(UIImage(named: "add-to-cart"), for: .normal)
                        btnCart.tag = 102
                    }
                    else
                    {
                        btnCart.isHidden = true
                        btnCart.tag = 0
                    }
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor(named: "theme_bg_color") //Colors.theme_white_color
                    cell.backgroundColor = UIColor(named: "theme_bg_color") //Colors.theme_white_color
                    cell.imgChecked.image = UIImage(named: "uncheckbox")
                    btnCart.isHidden = true
                }
                //data
                if arrList.count > 0
                {
                    let objList = getInvoiceObject(tag: section)
                    cell.lblDate.text = objList.dateOfInvoice ?? ""
                    cell.lblInvoiceNo.text = objList.hawbNumber ?? ""
                    cell.lblName.text = objList.customerName ?? ""
                    if objList.status == PaymentStatus.InTransit.dispValue()
                    {
                        cell.lblAmount.text = "\(objList.totalTtd ?? "")"
                    }
                    else
                    {
                        if let totalTTD = objList.totalTtd , totalTTD != ""
                        {
                            cell.lblAmount.text = "\(kTTCurrency) \(totalTTD)"
                        }
                        else
                        {
                            cell.lblAmount.text = "\(kTTCurrency) 0.00"
                        }
                    }
                    if objList.status == PaymentStatus.Paid.dispValue()
                    {
                        cell.colorBar.backgroundColor = UIColor(named: "theme_green_color")
                    }
                    else if objList.status == PaymentStatus.UnPaid.dispValue()
                    {
                        if let totalUSD = objList.totalUsd , totalUSD != ""
                        {
                            if Float(totalUSD)! == 0
                            {
                                cell.colorBar.backgroundColor = UIColor(named: "theme_blue_color")
                            }
                            else
                            {
                                if objList.isCreditPayment == 1 {
                                    //Pink
                                    cell.colorBar.backgroundColor = Colors.credit_invoice_bar_color
                                }else{
                                    cell.colorBar.backgroundColor = UIColor(named: "theme_red_color")
                                }
                            }
                        }
                        else
                        {
                            if objList.isCreditPayment == 1 {
                                cell.colorBar.backgroundColor = Colors.credit_invoice_bar_color
                            }else{
                                cell.colorBar.backgroundColor = UIColor(named: "theme_red_color")
                            }
                            
                        }
                    }
                    else if objList.status == PaymentStatus.InTransit.dispValue()
                    {
                        cell.colorBar.backgroundColor = UIColor(named: "theme_yellow_color")
                    }
                    else if objList.status == PaymentStatus.PaidBtNotCollected.dispValue()
                    {
                        cell.colorBar.backgroundColor = UIColor(named: "theme_blue_color")
                    }
                    cell.colorBar.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
                    cell.colorBar.layer.cornerRadius = 2
                    cell.colorBar.layer.masksToBounds = true
                    
                    cell.viewMore.isHidden = false
                    
                    if let arrReceipt = self.arrList[section].documentsArr {
                        print("Receipt count is \(arrReceipt.count)")
                        
                        if arrReceipt.count > 0 {
                            cell.viewReceipt.isHidden = false
                        }else{
                            cell.viewReceipt.isHidden = true
                        }
                    }
                    
                    cell.moreContainer.layer.cornerRadius = cell.moreContainer.frame.size.height/2
                    cell.moreContainer.layer.masksToBounds = true
                    
                    cell.btnMore.isUserInteractionEnabled = true
                    cell.btnMore.tag = section
                    let btntapGestureMore = UITapGestureRecognizer(target: self, action: #selector(btnMoreTapped(sender:)))
                    cell.btnMore.addGestureRecognizer(btntapGestureMore)
                    cell.btnMore.bringSubviewToFront(cell.contentView)
                    
                    cell.btnViewReceipt.isUserInteractionEnabled = true
                    cell.btnViewReceipt.tag = section
                    
                    cell.receiptContainer.backgroundColor = Colors.theme_blue_color
                    let btntapViewReciept = UITapGestureRecognizer(target: self, action: #selector(btnViewReceiptTapped(sender:)))
                    cell.btnViewReceipt.addGestureRecognizer(btntapViewReciept)
                    cell.btnViewReceipt.bringSubviewToFront(cell.contentView)

                }
                return cell.contentView
            }
        }

        return UITableViewCell().contentView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if type == InvoiceType.Upcoming.rawValue {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceListCell", for: indexPath) as? InvoiceListCell {

                if arrMiamiInvoicelist.count > 0
                {
                    let objList = getMiamiInvoiceObject(tag: indexPath.section)
                    cell.lblShipperType.text = objList.shipperName ?? ""
                    cell.lblTrackingNo.text = objList.trackingNumber ?? ""
                    cell.lblWeight.text = "\(objList.weight ?? "0") \(kWeightUnit)"
    
                    cell.lblAmountLabel.text = "Shipping Type"
                    cell.lblAmount.text = objList.shippingType ?? ""
                    cell.lblDesc.text = objList.descriptionField ?? ""

                    cell.contentView.backgroundColor = UIColor(named: "theme_bg_color")
                    cell.backgroundColor = UIColor(named: "theme_bg_color")
                }
                return cell
            }
        }
        else{
            var totalAmount : Float = 0
            if let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceListCell", for: indexPath) as? InvoiceListCell {
                if arrSelectedSections.count > 0
                {
                    if  arrSelectedSections[indexPath.section] == 1
                    {
                        cell.contentView.backgroundColor = UIColor(named:"theme_lightblue_color")
                        cell.backgroundColor =  UIColor(named:"theme_lightblue_color")
                    }
                    else
                    {
                        cell.contentView.backgroundColor = UIColor(named: "theme_bg_color")
                        cell.backgroundColor = UIColor(named: "theme_bg_color")
                    }
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor(named: "theme_bg_color")
                    cell.backgroundColor = UIColor(named: "theme_bg_color")
                }
                if arrList.count > 0
                {
                    let objList = getInvoiceObject(tag: indexPath.section)
                    cell.lblShipperType.text = objList.shipper ?? ""
                    cell.lblTrackingNo.text = objList.trackingNumber ?? ""
                    cell.lblWeight.text = "\(objList.packagesWeight ?? "") \(kWeightUnit)"
                    cell.lblAmountLabel.text = "USD Amount"
                    cell.lblAmount.text = objList.totalUsd ?? "" //objList.totalTtd ?? ""
                    cell.lblDesc.text = objList.descriptionName ?? ""
                    if objList.status == PaymentStatus.UnPaid.dispValue()
                    {
                        //totalAmount = Float((objList.totalUsd ?? "0")!)! //new change apr-2021
                        if let totalUSD = objList.totalUsd,totalUSD != ""
                        {
                            totalAmount = Float(totalUSD)!
                        }
                        
                    }
                }
                //long press gesture
                cell.contentView.tag = indexPath.section
                return cell
            }
        }
        return UITableViewCell()
    }
    //Pagination
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isLoadingList = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (Int(tblInvoiceList.contentOffset.y + tblInvoiceList.frame.size.height) >= Int(tblInvoiceList.contentSize.height))
        {
            if !isLoadingList{
                isLoadingList = true
                if self.totalPages == pageNo
                {
                    return
                }
                pageNo += 1
                getInvoiceList(isShowLoader: true)
            }
        }
    }
    
}
extension MyInvoicesVC
{
    // MARK: - Custom methods
    @objc func btnGenerateTicktClicked(sender:UIButton)
    {
        UIView.setAnimationsEnabled(true)
        var hawb_number : String  = ""
        let objList = getInvoiceObject(tag:sender.tag)
        hawb_number = objList.hawbNumber ?? ""
        if let ticketVC = appDelegate.getViewController("GenerateTicketVC", onStoryboard: "Invoice") as? GenerateTicketVC {
            ticketVC.hawb_number = hawb_number
            ticketVC.modalPresentationStyle = .overFullScreen
            self.present(ticketVC,animated: true, completion: nil)
        }
    }
    @objc func btnDownloadClicked(sender:UIButton)
    {
        var params : [String:Any] = [:]
        self.showHUD()
        let objList = getInvoiceObject(tag: sender.tag)
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"] = objList.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.DownloadInvoice, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                if let filePath = (resultDict as NSDictionary).value(forKey: "filepath") as? String
                {
                    self.downloadFile(InvoiceID:objList.hawbNumber ?? "",pdfFileURL:filePath)
                }
                else
                {
                    self.hideHUD()
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    func callAPIForAddToCart(params:[String:Any])
    {
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.addToCart, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                //new change
                // self.longPressGesture = "Stopped"
                self.clearData()
                //over
                UserDefaults.standard.setValue(true, forKey: kCartItemAvailable) //new change
                //new change
                var flag : Int = 0
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: ViewCartVC.self)
                    {
                        flag = 1
                        self.navigationController?.popToViewController(controller, animated: true)
                        break
                    }
                    
                }
                if flag == 0
                {
                    if let vc = appDelegate.getViewController("ViewCartVC", onStoryboard: "Invoice") as? ViewCartVC {
                        self.objUserInfo = getUserInfo()
                        vc.userID = self.objUserInfo.id ?? ""
                        vc.isFromVC = "MyInvoicesVC"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else
            {
                // appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                self.showAlert(title: "", msg: message, vc: self)
            }
        }
    }
    
    func getInvoiceIDs() -> String
    {
        var strInvoiceID : String  = ""
        for (index,value) in arrSelectedSections.enumerated()
        {
            if value == 1 && arrList.count > 0
            {
                let objList = arrList[index]
                if strInvoiceID != ""
                {
                    strInvoiceID += "," + objList.id
                }
                else
                {
                    strInvoiceID =  objList.id
                }
            }
        }
        return strInvoiceID
    }
    
    // MARK: - IBAction methods
    @IBAction func btnAddCartClicked(_ sender:UIButton)
    {
        var params : [String:Any] = [:]
        if arrSelectedSections.count > 0
        {
            let strInvoiceIDs = getInvoiceIDs()
            if sender.tag == 101 //Package request
            {
                if let pickupSubViewVC = appDelegate.getViewController("PickupSubviewsVC", onStoryboard: "Invoice") as? PickupSubviewsVC {
                    pickupSubViewVC.isFromVC = "Invoice"
                    pickupSubViewVC.strInvoiceIDs = strInvoiceIDs
                    
                    pickupSubViewVC.referenceVC = self
                    pickupSubViewVC.strDriveThruReqURL = self.strDriveThruReqURL
                    
                    pickupSubViewVC.navvc = self.navigationController
                    pickupSubViewVC.view.backgroundColor =  UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.8)
                    pickupSubViewVC.modalPresentationStyle = .overFullScreen
                    self.navigationController?.present(pickupSubViewVC, animated: true, completion: nil)
                }
            }
            else if sender.tag == 102 //Add to cart
            {
                objUserInfo = getUserInfo()
                params["user_id"] = self.objUserInfo.id ?? ""
                params["invoice_id"] = strInvoiceIDs
                callAPIForAddToCart(params:params)
            }
        }
    }
}
//class InvoiceHeaderCell : UITableViewCell
//{
//    @IBOutlet weak var colorBar : UIView!
//    @IBOutlet weak var imgChecked : UIImageView!
//    @IBOutlet weak var imgUpDown : UIImageView!
//    @IBOutlet weak var lblDate : UILabel!
//    @IBOutlet weak var lblInvoiceNo : TapAndCopyLabel!
//    @IBOutlet weak var lblName : TapAndCopyLabel!
//    @IBOutlet weak var viewName : UIView!
//    @IBOutlet weak var lblAmount : UILabel!
//    @IBOutlet weak var longSeperator : UILabel!
//    @IBOutlet weak var shortSeperator : UILabel!
//    @IBOutlet weak var btnMore : UIButton!
//    @IBOutlet weak var moreContainer : UIView!
//    @IBOutlet weak var imgMore : UIImageView!
//    @IBOutlet weak var stackViewHeader : UIStackView!
//}
class InvoiceHeaderCell : UITableViewCell
{
    @IBOutlet weak var colorBar : UIView!
    @IBOutlet weak var imgChecked : UIImageView!
    @IBOutlet weak var imgUpDown : UIImageView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblInvoiceNo : TapAndCopyLabel!
    @IBOutlet weak var lblName : TapAndCopyLabel!
    @IBOutlet weak var viewName : UIView!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var longSeperator : UILabel!
    @IBOutlet weak var shortSeperator : UILabel!
    @IBOutlet weak var btnMore : UIButton!
    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var moreContainer : UIView!
    @IBOutlet weak var imgMore : UIImageView!
    @IBOutlet weak var stackViewHeader : UIStackView!
    
    @IBOutlet weak var receiptContainer : UIView!
    @IBOutlet weak var viewReceipt: UIView!
    @IBOutlet weak var btnViewReceipt : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        receiptContainer.layer.cornerRadius = receiptContainer.frame.size.height/2
        receiptContainer.layer.masksToBounds = true
    }
}
class InvoiceListCell : UITableViewCell
{
    @IBOutlet weak var lblShipperType : UILabel!
    @IBOutlet weak var lblTrackingNo : TapAndCopyLabel!
    @IBOutlet weak var lblWeight : UILabel!
    @IBOutlet weak var lblAmountLabel : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var stackViewUSDAmount : UIStackView!
    
//    @IBOutlet weak var btnGnerateTckt : UIButton!
//    @IBOutlet weak var btnView : UIButton!
//    @IBOutlet weak var btnDownload : UIButton!
//    @IBOutlet weak var btnCart : UIButton!
//    @IBOutlet weak var btnOpenTicket : UIButton!
//    @IBOutlet weak var tcktContainer : UIView!
//    @IBOutlet weak var viewContainer : UIView!
//    @IBOutlet weak var downloadContainer : UIView!
//    @IBOutlet weak var cartContainer : UIView!
//    @IBOutlet weak var openTicketContainer : UIView!
//    @IBOutlet weak var btnMore : UIButton!
//    @IBOutlet weak var moreContainer : UIView!

    override func awakeFromNib() {
//        tcktContainer.layer.cornerRadius = btnGnerateTckt.frame.size.height/2
//        tcktContainer.layer.masksToBounds = true
//        viewContainer.layer.cornerRadius = btnView.frame.size.height/2
//        viewContainer.layer.masksToBounds = true
//        downloadContainer.layer.cornerRadius = btnDownload.frame.size.height/2
//        downloadContainer.layer.masksToBounds = true
//        cartContainer.layer.cornerRadius = btnCart.frame.size.height/2
//        cartContainer.layer.masksToBounds = true
//        openTicketContainer.layer.cornerRadius = btnCart.frame.size.height/2
//        openTicketContainer.layer.masksToBounds = true
        
        //moreContainer.layer.cornerRadius = btnMore.frame.size.height/2
        //moreContainer.layer.masksToBounds = true

    }
}

extension MyInvoicesVC : InvoiceActionsVCDelegate{
    func getSelectedInvoiceAction(rowIndex: Int, invoiceActionIndex : Int, invoiceActionName : String) {
        //print("Selected row index is \(rowIndex)")
        //print("Selected index is \(invoiceActionIndex)")
        //print("Selected action is \(invoiceActionName)")
        
        self.dismiss(animated: true) { [self] in
            
            if invoiceActionName == InvoiceActionsName.GenerateTicket.rawValue {
                generateTicket(index: rowIndex)
            }else if invoiceActionName == InvoiceActionsName.InvoiceDetails.rawValue {
                viewInvoice(index: rowIndex)
            }else if invoiceActionName == InvoiceActionsName.DownloadInvoice.rawValue{
                download(index: rowIndex)
            }else if invoiceActionName == InvoiceActionsName.ViewYourCart.rawValue{
                addToCartClicked(index: rowIndex)
            }else if invoiceActionName == InvoiceActionsName.PackageRequest.rawValue{
                packageRequest(index: rowIndex)
            }
        }
    }
    
    func generateTicket(index : Int){
        UIView.setAnimationsEnabled(true)
        var hawb_number : String  = ""
        let objList = getInvoiceObject(tag:index)
        hawb_number = objList.hawbNumber ?? ""
        if let ticketVC = appDelegate.getViewController("GenerateTicketVC", onStoryboard: "Invoice") as? GenerateTicketVC {
            ticketVC.hawb_number = hawb_number
            ticketVC.modalPresentationStyle = .overFullScreen
            self.present(ticketVC,animated: true, completion: nil)
        }
    }
    
    func viewInvoice(index : Int)
    {
        var params : [String:Any] = [:]
        let objInvoice = getInvoiceObject(tag: index)
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"] = objInvoice.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ViewInvoice, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                let objInvoiceDetail = InvoiceDetail(fromJson: JSON(resultDict))
                if let vc = appDelegate.getViewController("InvoiceDetailVC", onStoryboard: "Invoice") as? InvoiceDetailVC {
                    self.objUserInfo = getUserInfo()
                    vc.userID = self.objUserInfo.id ?? ""
                    vc.isFullRefund = objInvoiceDetail.fullRefund ?? ""
                    vc.objInvoiceDetail = objInvoiceDetail
                    if let status = objInvoiceDetail.paidStatus as? String, status == "0"
                    {
                        vc.isPaid = false
                    }
                    else
                    {
                        vc.isPaid = true
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    func download(index : Int){
        var params : [String:Any] = [:]
        self.showHUD()
        let objList = getInvoiceObject(tag: index)
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["invoice_id"] = objList.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.DownloadInvoice, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                if let filePath = (resultDict as NSDictionary).value(forKey: "filepath") as? String
                {
                    self.downloadFile(InvoiceID:objList.hawbNumber ?? "",pdfFileURL:filePath)
                }
                else
                {
                    self.hideHUD()
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    func packageRequest(index : Int){
        if let vc = appDelegate.getViewController("PickupSubviewsVC", onStoryboard: "Invoice") as? PickupSubviewsVC {
            vc.isFromVC = "Invoice"
            let objInvoice = getInvoiceObject(tag: index)
            vc.strInvoiceIDs = objInvoice.id ?? ""
            
            vc.referenceVC = self
            vc.strDriveThruReqURL = self.strDriveThruReqURL
            
            vc.navvc = self.navigationController
            vc.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
            vc.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    func addToCartClicked(index : Int)
    {
        var params : [String:Any] = [:]
        let objInvoice = getInvoiceObject(tag: index)
        objUserInfo = getUserInfo()
        params["user_id"] = self.objUserInfo.id ?? ""
        params["invoice_id"] = objInvoice.id ?? ""
        callAPIForAddToCart(params:params)
    }

    func showInvoiceActionsVC(index : Int){
        UIView.setAnimationsEnabled(true)
        if let invoiceActionsVC = appDelegate.getViewController("InvoiceActionsVC", onStoryboard: "Invoice") as? InvoiceActionsVC {
            invoiceActionsVC.InvoiceActionsVCDelegate = self
            invoiceActionsVC.invoiceRowIndex = index
            invoiceActionsVC.isShowCartOption = self.isShowCartOption
            invoiceActionsVC.isShowOpenTicketOption = self.isShowOpenTicketOption
            invoiceActionsVC.modalPresentationStyle = .overFullScreen
            self.present(invoiceActionsVC,animated: true, completion: nil)
        }
    }
}

extension MyInvoicesVC {
    func getUpcomingInvoiceData(isShowLoader: Bool)
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["page_id"] = pageNo
        params["search_content"] = strSearch
        params["start_date"] = strStartDate
        params["end_date"] = strEndDate
        params["date_type"] = strDateType
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ReceiptList, showLoader: isShowLoader) { (resultDict, status, message) in
            self.tblInvoiceList.isHidden = false
            if status == true
            {
                print("Upcoming invoice response is \(resultDict)")
                self.isLoadingList = false
                self.refreshControl.endRefreshing()
                if self.pageNo == 1
                {
                    self.arrMiamiInvoicelist.removeAll()
                }
                let objMyInvoiceList = InvoiceAtMiami.init(fromJson: JSON(resultDict))
                if self.arrMiamiInvoicelist.count > 0
                {
                    self.arrMiamiInvoicelist.append(contentsOf: objMyInvoiceList.list)
                    for _ in 0..<objMyInvoiceList.list.count
                    {
                        self.arrMiamiSections.append(0)
                        self.arrMiamiSelectedSections.append(0)
                    }
                }
                else
                {
                    self.arrMiamiInvoicelist = objMyInvoiceList.list
                    self.arrMiamiSections.removeAll()
                    self.arrMiamiSelectedSections.removeAll()
                    for _ in 0..<self.arrMiamiInvoicelist.count
                    {
                        self.arrMiamiSections.append(0)
                        self.arrMiamiSelectedSections.append(0)
                    }
                }
                self.totalRecordsCount = objMyInvoiceList.totalCount ?? 0
                if self.arrMiamiInvoicelist.count > 0
                {
                    self.lblNoRecordFound.isHidden = true
                    self.btnExportReport.isHidden = false
                }
                else
                {
                    self.lblNoRecordFound.isHidden = false
                    self.btnExportReport.isHidden = true
                }
                self.totalPages = objMyInvoiceList.totalPage ?? 0
                DispatchQueue.main.async {
                    self.tblInvoiceList.reloadData()
                }

                if self.totalRecordsCount > 0 {
                    self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
                }else {
                    self.cnstTotalRecordsTitleHeight.constant = 0
                }
                self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrMiamiSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
                self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrMiamiSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PACKAGES)
            }
            else
            {
                self.refreshControl.endRefreshing()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    func getMiamiInvoiceObject(tag:Int) -> InvoiceAtMiamiList
    {
        if arrMiamiInvoicelist.count > 0
        {
            return arrMiamiInvoicelist[tag]
        }
        return InvoiceAtMiamiList()
    }
}
