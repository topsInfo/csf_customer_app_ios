////
////  ReceiptPicturesCell.swift
////  CSFMerchant
////
////  Created by Tops on 04/08/22.
////
//
//import UIKit
//
//class ReceiptPicturesCell: UICollectionViewCell {
//    
//    @IBOutlet weak var viewImgPage: UIView!
//    @IBOutlet weak var imgPage: UIImageView!
//    @IBOutlet weak var imgDoc: UIImageView!
//    
//    override class func awakeFromNib() {
//    }
//
//    override func layoutSubviews() {
//    }
//}

//
//  ReceiptPicturesCell.swift
//  CSFMerchant
//
//  Created by Tops on 04/08/22.
//
import UIKit

class ReceiptPicturesCell: UICollectionViewCell {
    
    @IBOutlet weak var viewImgPage: UIView!
    @IBOutlet weak var imgPage: UIImageView!
    @IBOutlet weak var imgDoc: UIImageView!
    @IBOutlet weak var imgActivityIndicator: UIActivityIndicatorView!

    override class func awakeFromNib() {
    }

    override func layoutSubviews() {
    }
}
