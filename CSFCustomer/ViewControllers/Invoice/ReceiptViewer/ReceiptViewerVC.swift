//
//  ReceiptViewerVC.swift
//  CSFCustomer
//
//  Created by iMac on 04/08/22.
//

import UIKit
class ReceiptViewerVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var clvReceiptViewer : UICollectionView!
    @IBOutlet weak var pageProgress: UIProgressView!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var viewNote: UIView!
    @IBOutlet weak var viewPreview: UIView!
    @IBOutlet weak var btnPreview: UIButton!
    @IBOutlet weak var btnDownloadDocument: UIButton!
    
    // MARK: - Global Variables
    var currentPage : Int = 0
    var numberOfPages : Int = 0
    var scrollViewHeightConstraint: NSLayoutConstraint!
    var intSelectedIndex : Int = 0
    var arrDocuments : [String] = [String]()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        self.btnDownloadDocument.isHidden = false
        self.clvReceiptViewer.delegate = self
        self.clvReceiptViewer.isUserInteractionEnabled = true
        self.clvReceiptViewer.isPagingEnabled = true
        
        viewPreview.layer.cornerRadius = viewPreview.frame.size.height/2
        viewPreview.layer.masksToBounds = true
        
        numberOfPages = arrDocuments.count
        
        DispatchQueue.main.async { [self] in
            clvReceiptViewer.reloadData()
            clvReceiptViewer.scrollToItem(at: IndexPath(row: intSelectedIndex, section: 0), at: .top, animated: false)
            configureUI()
        }
    }
    
    func configureUI(){
        self.lblTitle.text = "\(self.currentPage + 1) of \(numberOfPages)"
        updateUI()
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDownloadDocumentClicked(_ sender: UIButton) {
        self.showHUD()
        let strURL : String = self.arrDocuments[self.currentPage]
        let strFileName : String = strURL.getFileNameWithExtension
        self.downloadFile(InvoiceID: "\(strFileName)",pdfFileURL:strURL,downloadedFileType: "Document")
    }
    
    @IBAction func btnPreviewClicked(_ sender: UIButton) {
        print("btnPreviewClicked")
        
        if let receiptDocViewerVC = appDelegate.getViewController("ReceiptDocViewerVC", onStoryboard: "Invoice") as? ReceiptDocViewerVC {
            receiptDocViewerVC.strDocURL = self.arrDocuments[currentPage]
            self.navigationController?.pushViewController(receiptDocViewerVC, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        self.currentPage = Int(ceil(x/w))// + 1
        print("Current page is \(self.currentPage)")
        configureUI()
    }
    
    func updateUI(){
        
        let progressValue : Float = Float(self.currentPage + 1) / Float(self.numberOfPages)
        
        print("progress is \(progressValue)")
        self.pageProgress.progress = progressValue
        
        let strURL : String = self.arrDocuments[self.currentPage]
        self.lblFileName.text = strURL.getFileNameWithExtension
        
        let strFileExtension : String = strURL.getFileExtension
        if (strFileExtension.lowercased() == "jpeg" || strFileExtension.lowercased() == "jpg" || strFileExtension.lowercased() == "png") {
            self.viewPreview.isUserInteractionEnabled = false
            self.viewPreview.alpha = 0.7
        }else{
            self.viewPreview.isUserInteractionEnabled = true
            self.viewPreview.alpha = 1.0
        }
    }
}

extension ReceiptViewerVC {
    // MARK: - CollectionView Delegate and Datasource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrDocuments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell : ReceiptPicturesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReceiptPicturesCell", for: indexPath) as? ReceiptPicturesCell{
            cell.imgPage.contentMode = .scaleAspectFit
            cell.imgPage.isHidden = false
            
            let strURL : String = self.arrDocuments[indexPath.row]
            let strFileExtension : String = strURL.getFileExtension
            if (strFileExtension.lowercased() == "jpeg" || strFileExtension.lowercased() == "jpg" || strFileExtension.lowercased() == "png") {
                
                cell.imgPage.isHidden = false
                cell.imgDoc.isHidden = true
                
                cell.imgActivityIndicator.isHidden = false
                cell.imgActivityIndicator.startAnimating()
                
                if strURL != "" {
                    cell.imgPage.sd_setImage(with: URL(string:strURL), completed: { (image, error, type, url) in
                        cell.imgActivityIndicator.isHidden = true
                        if image != nil {
                            cell.imgPage.image = image
                        }else {
                            //Image Not found
                        }
                    })
                }
            }else{
                cell.imgPage.isHidden = true
                cell.imgDoc.isHidden = false
                cell.imgActivityIndicator.isHidden = true
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
