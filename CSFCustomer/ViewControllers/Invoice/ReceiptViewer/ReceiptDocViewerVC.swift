//
//  ReceiptDocViewerVC.swift
//  CSFCustomer
//
//  Created by Tops on 04/08/22.
//

import UIKit
import WebKit
import MBProgressHUD
import PDFKit
class ReceiptDocViewerVC: UIViewController,WKNavigationDelegate, UIScrollViewDelegate{
    
    // MARK: - IBOutlets
    @IBOutlet weak var webView : WKWebView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var activityProgressView: UIActivityIndicatorView!
    @IBOutlet weak var viewViewer: UIView!
    
    var strDocURL : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        
        webView.navigationDelegate = self
        webView.scrollView.delegate = self
        
        loadWebView()
    }
    
    func loadWebView(){
        print("mediaUrl is \(String(describing: strDocURL))")
        
        if strDocURL != ""{
            
            createLoader()
            let strFileExtension : String = strDocURL.getFileExtension
            if strFileExtension.lowercased() == "pdf"{
                let strFileMimeType : String = "application/pdf"
                if let link : URL = URL(string:strDocURL) {
                    self.convertToDataAndLoadToWebView(withMimeType: strFileMimeType, ofURL: link)
                }
            }else{
                if let link : URL = URL(string:strDocURL) {
                    let request = URLRequest(url: link)
                    webView.load(request)
                }
            }
        }
    }
    
    func createLoader()
    {
        activityProgressView.startAnimating()
    }
    
    func convertToDataAndLoadToWebView(withMimeType : String, ofURL : URL){
        DispatchQueue.main.async {
            if let data = try? Data(contentsOf: ofURL) {
                self.webView.load(data, mimeType: withMimeType, characterEncodingName: "", baseURL: ofURL)
            }
        }
    }
    // MARK: - IBAction Methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webview delegate methods
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("SHOW PROGRESS")
        activityProgressView.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("HIDE PROGRESS")
        activityProgressView.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("HIDE PROGRESS")
        activityProgressView.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        activityProgressView.stopAnimating()
        if error._code == -1001 { // TIMED OUT:
            print("HIDE PROGRESS")
            // CODE to handle TIMEOUT
        } else if error._code == -1003 { // SERVER CANNOT BE FOUND
            print("HIDE PROGRESS")
            // CODE to handle SERVER not found
        } else if error._code == -1100 { // URL NOT FOUND ON SERVER
            print("HIDE PROGRESS")
            // CODE to handle URL not found
        } else if error._code == -1200 { // URL NOT FOUND ON SERVER
            print("HIDE PROGRESS")
            // CODE to handle SSL Error and Secure Connection
        }
    }
}
