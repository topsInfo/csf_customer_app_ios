//
//  SegmentPageVC.swift
//  CSFCustomer
//
//  Created by Tops on 08/12/20.
//

import UIKit
//import TabPageViewController
import ScrollableSegmentedControl
class SegmentPageVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var segmentedControl: ScrollableSegmentedControl!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var lblCartCounter : UILabel!
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var btnMenu : UIButton!
    @IBOutlet weak var viewSegmentOptions: UIView!

    // MARK: - Global Variable
    lazy var MyInvoicesVC: UIViewController? = {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyInvoicesVC") as! MyInvoicesVC
        return vc
    }()
    var selectedIndex : Int = 0
    var isFromMenu :Bool = false
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(getCartCounter(_:)), name: NSNotification.Name(rawValue: "CartCounterUpdated"), object: nil)
        lblCartCounter.layer.cornerRadius = lblCartCounter.frame.size.height / 2
        lblCartCounter.layer.masksToBounds = true
        lblCartCounter.isHidden = true
        topBarView.topBarBGColor()
        if isFromMenu == true
        {
            btnMenu.isHidden = false
            btnBack.isHidden = true
        }
        else
        {
            btnMenu.isHidden = true
            btnBack.isHidden = false
        }
        
        //Segment Management
        viewSegmentOptions.backgroundColor = Colors.theme_segment_bg_color
        
//        This old code is now replaced by below new code - SegmentOptions
//        let decodedSegments  = UserDefaults.standard.data(forKey: kArrSegmentOptons)
//        if decodedSegments != nil {
//            if let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decodedSegments!) as? [SegmentOptions] {
//                appDelegate.arrOptions = decodedTeams
//                //print("segment count =****= \(appDelegate.arrOptions.count)")
//            }
//        }
        
        if let arrData = UserDefaults.standard.decode(for: [SegmentOptions].self, using: String(describing: SegmentOptions.self)){
            appDelegate.arrOptions = arrData
        }
        
        getSegmentOptons()

        let arrSelectedSegments = appDelegate.arrOptions.filter { $0.isChecked == "1" }
        setSegmentControlNew(arrSegments: arrSelectedSegments)
    }
    @objc func getCartCounter(_ notification:NSNotification)
    {
          let cartCounter = notification.userInfo?["cartCounter"] as? Int ?? 0
          if cartCounter == 0
          {
            lblCartCounter.isHidden = true
          }
          else
          {
            lblCartCounter.isHidden = false
            if cartCounter <= 9
            {
                self.lblCartCounter.text = String(cartCounter)
            }
            else
            {
                self.lblCartCounter.text = "9+"
            }
          }
    }
    @objc func segmentSelected(sender:UISegmentedControl)
    {
        self.MyInvoicesVC!.view.removeFromSuperview()
        self.MyInvoicesVC!.removeFromParent()
        
        if sender.selectedSegmentIndex >= 0 {
            let arrSelectedSegments = appDelegate.arrOptions.filter { $0.isChecked == "1" }

            showController(vcname: self.MyInvoicesVC!,type:arrSelectedSegments[sender.selectedSegmentIndex].refNo)
        }
    }
    func showController(vcname:UIViewController,type:String)
    {
        (vcname as! MyInvoicesVC).type = type
        self.addChild(vcname)
        let topValue : CGFloat = 165
        vcname.didMove(toParent: self)
        vcname.view.frame = CGRect(x: 0, y: topValue, width: self.view.frame.size.width, height: (self.view.frame.size.height)-topValue)
        self.view.addSubview(vcname.view)
        NotificationCenter.default.post(name: Notification.Name("SegmentChanged"), object: nil)
    }
    // MARK: - IBAction methods
    @IBAction func btnCartClicked()
    {
        var flag : Int = 0
        let objUserInfo = getUserInfo()
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ViewCartVC.self)
            {
                flag = 1
                self.navigationController?.popToViewController(controller, animated: true)
                break
            }
            
        }
        if flag == 0
        {
            if let vc = appDelegate.getViewController("ViewCartVC", onStoryboard: "Invoice") as? ViewCartVC{
                let objUserInfo = getUserInfo()
                vc.userID = objUserInfo.id ?? ""
                vc.isFromVC = "MyInvoicesVC"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    @IBAction func btnFilterClicked()
    {
        NotificationCenter.default.post(name: Notification.Name("FilterClicked"), object: nil)
    }
    @IBAction func btnInfoClicked()
    {
        UIView.setAnimationsEnabled(true)
        if let popupVC = appDelegate.getViewController("InvoiceInfoVC", onStoryboard: "Invoice") as? InvoiceInfoVC {
            popupVC.isFrom = "Invoice"
            popupVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
            popupVC.modalPresentationStyle = .overFullScreen
            self.present(popupVC,animated: true, completion: nil)
        }
    }
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnMenuClicked()
    {
        self.revealViewController().revealToggle(animated: true)
    }
}

extension SegmentPageVC : SegmentOptionsVCDelegate {
    @IBAction func btnSegmentOptionsClicked(_ sender: UIButton) {
        //print("Segment Options Clicked")
        showSegmentOptionsVC()
    }
    
    func showSegmentOptionsVC(){
        UIView.setAnimationsEnabled(true)
        if let segmentOptionsVC = appDelegate.getViewController("SegmentOptionsVC", onStoryboard: "Invoice") as? SegmentOptionsVC {
            segmentOptionsVC.segmentOptionsVCDelegate = self
            segmentOptionsVC.modalPresentationStyle = .overFullScreen
            self.present(segmentOptionsVC,animated: true, completion: nil)
        }
    }
    
    // MARK: - getUpdatedSegments
    func getUpdatedSegments(arrSegments: [SegmentOptions]) {
        
        let arrSelectedSegments = arrSegments.filter { $0.isChecked == "1"}

        //print("selected options are \(arrSelectedSegments.map { $0.optionName})")
        //print("selected index are \(arrSelectedSegments.map { $0.index})")
        
        setSegmentControlNew(arrSegments: arrSelectedSegments)
    }
    
    func setSegmentControlNew(arrSegments: [SegmentOptions])
    {
        //First we remove all segments
        while segmentedControl.numberOfSegments != 0 {
            //Nand : crash solution
            segmentedControl.selectedSegmentIndex = 0
            segmentedControl.removeSegment(at: 0)
        }
        segmentedControl.segmentStyle = .textOnly
        
        //Here we will add selected segments only
        var index : Int = 0
        for obj in arrSegments {
            //print("Option is : \(obj.optionName!)")
            segmentedControl.insertSegment(withTitle: obj.optionName!, image: nil, at: index)
            index += 1
        }
        
        segmentedControl.underlineSelected = true
        segmentedControl.tintColor = UIColor(named: "theme_orange_color")//Colors.theme_orange_color
        segmentedControl.addTarget(self, action: #selector(segmentSelected(sender:)), for: .valueChanged)
        
        segmentedControl.backgroundColor = Colors.theme_segment_bg_color
        
        //set font and color
        let largerRedTextAttributes = [NSAttributedString.Key.font: UIFont(name: fontname.openSansRegular, size: 12), NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.8)]
        let largerRedTextSelectAttributes = [NSAttributedString.Key.font: UIFont(name: fontname.openSansBold, size: 12), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        segmentedControl.setTitleTextAttributes(largerRedTextAttributes as [NSAttributedString.Key : Any], for: .normal)
        segmentedControl.setTitleTextAttributes(largerRedTextSelectAttributes as [NSAttributedString.Key : Any], for: .selected)
        // Turn off all segments been fixed/equal width.
        // The width of each segment would be based on the text length and font size.
        segmentedControl.fixedSegmentWidth = true
        segmentedControl.selectedSegmentIndex = selectedIndex
        
        //Updating the segmentcontrol layout
        segmentedControl.layoutSubviews()
        
        selectedIndex = 0
        segmentedControl.selectedSegmentIndex = selectedIndex
    }
}
