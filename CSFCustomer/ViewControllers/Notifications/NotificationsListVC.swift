//
//  NotificationsListVC.swift
//  CSFCustomer
//
//  Created by Tops on 24/02/21.
//

import UIKit
import SwiftyJSON
class NotificationsListVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var tblNotificationList : UITableView!
    
    // MARK: - Global Variable
    var arrNotifications : [NotificationListData] = [NotificationListData]()
    var userID : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        topBarView.topBarBGColor()
        getNotificationList(isShowLoader: true)
        tblNotificationList.dataSource = self
        tblNotificationList.delegate = self
        tblNotificationList.estimatedRowHeight = 45
        tblNotificationList.rowHeight = UITableView.automaticDimension
        tblNotificationList.tableFooterView = UIView()
    }
    
    // MARK: - getNotificationList
    func getNotificationList(isShowLoader : Bool)
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.NotificationList, showLoader:isShowLoader) { (resultDict, status, message) in
            if status == true
            {
                let objNotiList = Notifications(fromJson: JSON(resultDict))
                self.arrNotifications = objNotiList.list ?? []
                DispatchQueue.main.async {
                    self.tblNotificationList.reloadData()
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    /**
        This method is called when the value of a UISwitch in the NotificationsListVC is changed.
    */    
    @objc func notficationValueChanged(_ sender:UISwitch)
    {
        var status : Int = 0
        var type : String = ""
        if sender.tag == 0
        {
            type = "all"
        }
        else
        {
            let objNotification = arrNotifications[sender.tag-1]
            type = objNotification.type ?? ""
        }
        
        status = sender.isOn ? 1 : 0
        
        updateSwitchStatically(type: type, status: status, index: sender.tag)
        callAPIForNotificationUpdate(type:type,status:status,isShowLoader: false)
    }
    
    /**
     Updates the switch statically for a given type, status, and index.     
     */
    func updateSwitchStatically(type : String, status : Int, index : Int){
        if type == "all"{
            arrNotifications.indices.forEach{ arrNotifications[$0].status =  status}
        }else{
            arrNotifications[index - 1].status = status
        }
        self.tblNotificationList.reloadData()
    }
    
    // MARK: - callAPIForNotificationUpdate
    func callAPIForNotificationUpdate(type:String,status:Int,isShowLoader : Bool)
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["type"] = type
        params["status"] = status
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.NotificationSetting, showLoader:isShowLoader) { (resultDict, status, message) in
            if status == true
            {
                self.getNotificationList(isShowLoader: isShowLoader)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotifications.count + 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListCell") as? NotificationListCell {
         
            if indexPath.row == 0
            {
                let attributes = [NSAttributedString.Key.foregroundColor:UIColor(named:"theme_label_blue_color")]
                
                cell.lblNotification.attributedText = NSMutableAttributedString(string: "All Notifications", attributes: attributes as [NSAttributedString.Key : Any])
                
                let arrResult = arrNotifications.filter{$0.status == 0}
                
                if arrResult.count > 0
                {
                    cell.notificationSwitch.isOn = false
                }
                else
                {
                    cell.notificationSwitch.isOn = true
                }
            }
            else
            {
                let objNotification = arrNotifications[indexPath.row - 1]
                
                let attributes = [NSAttributedString.Key.foregroundColor:UIColor(named:"theme_label_black_color")]
                
                cell.lblNotification.attributedText = NSMutableAttributedString(string: "\(objNotification.title ?? "")", attributes: attributes as [NSAttributedString.Key : Any])

                if objNotification.status == 1
                {
                    cell.notificationSwitch.isOn = true
                }
                else
                {
                    cell.notificationSwitch.isOn = false
                }
            }
            cell.notificationSwitch.tag = indexPath.row
            cell.notificationSwitch.addTarget(self, action: #selector(notficationValueChanged(_:)), for: .valueChanged)
            return cell
        }
       return UITableViewCell()
    }
    
}
class NotificationListCell : UITableViewCell
{
    @IBOutlet weak var lblNotification : UILabel!
    @IBOutlet weak var notificationSwitch : UISwitch!
}
