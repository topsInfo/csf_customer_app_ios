//
//  NotificationVC.swift
//  CSFCustomer
//
//  Created by Tops on 04/12/20.
//

import UIKit
import SwiftyJSON
class NotificationVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var viewTotalRecord : TotalRecordView!
    @IBOutlet weak var cnstTotalRecordsTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var tblNotification : UITableView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var topBarView : UIView!
    
    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    var arrNotificationList : [NotificationList] = [NotificationList]()
    var refreshControl = UIRefreshControl()
    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
        getNotifications(isShowLoader: true)
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name("NotificationRead"), object: nil)
    }
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        if  let decoded = UserDefaults.standard.object(forKey: kUserInfo) as? Data
        {
            
            //          This old code is now replaced by below new code
            //          objUserInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserInfo
            
            do {
                if let unarchivedObject = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserInfo.self, from: decoded) {
                    // Handle the unarchived object
                    objUserInfo = unarchivedObject
                } else {
                    // Handle the case where unarchiving failed
                }
            } catch {
                // Handle the error
            }
        }
        topBarView.topBarBGColor()
        self.lblNoRecordFound.isHidden = true
        tblNotification.dataSource = self
        tblNotification.delegate = self
        tblNotification.estimatedRowHeight = 75
        tblNotification.rowHeight = UITableView.automaticDimension
        tblNotification.tableFooterView = UIView()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblNotification.addSubview(refreshControl)
        cnstTotalRecordsTitleHeight.constant = 0
        viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: 0, totalRecords: totalRecordsCount)
        viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: 0, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_NOTIFICATION)
        
    }
    @objc func refresh()
    {
        getNotifications(isShowLoader: false)
    }
    
    @objc func willEnterForeground() {
        DispatchQueue.main.async {
            self.tblNotification.reloadData()
        }
    }
    @objc func btnRowClicked(sender:UIButton)
    {
        self.arrNotificationList[sender.tag].isRead = "1"
        tblNotification.reloadData()
        
        let objNotificationList = arrNotificationList[sender.tag]
        let type  = objNotificationList.type ?? ""
        //type 1 = receipt web view,type 2 = invoice details,type 3 = service guide
        if type == "1"
        {
            if let vc = appDelegate.getViewController("PDFViewerVC", onStoryboard: "Invoice") as? PDFViewerVC {
                if let redirect = objNotificationList.redirect, redirect != ""
                {
                    vc.pdfFile = objNotificationList.redirect
                    vc.strTitle = "Receipt Detail"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        else if type == "2"
        {
            if let vc = appDelegate.getViewController("InvoiceDetailVC", onStoryboard: "Invoice") as? InvoiceDetailVC {
                if let invoiceID = objNotificationList.redirect, invoiceID != ""
                {
                    vc.invoiceID = invoiceID
                    vc.isFrom = "PushNotification"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        else if type == "3"
        {
            if let vc = appDelegate.getViewController("ServiceGuideVC", onStoryboard: "Home") as? ServiceGuideVC {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if type == "4"
        {
            if let vc = appDelegate.getViewController("RewardsVC", onStoryboard: "Referral") as? RewardsVC {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if type == "5" || type == "6"
        {
            if isShowDriveThruOption {
                if type == "6" {
                    if let redirect = objNotificationList.redirect, redirect != ""{
                        if let vc = appDelegate.getViewController("RequestDetailVC", onStoryboard: "DriveThru") as? RequestDetailVC {
                            vc.requestId = redirect
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        if let vc = appDelegate.getViewController("WalletVC", onStoryboard: "Referral") as? WalletVC {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
                if type == "5" {
                    if let vc = appDelegate.getViewController("WalletVC", onStoryboard: "Referral") as? WalletVC {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }else{
                if let vc = appDelegate.getViewController("WalletVC", onStoryboard: "Referral") as? WalletVC {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    // MARK: - getNotifications
    func getNotifications(isShowLoader: Bool)
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.getNotification, showLoader:isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            if status == true
            {
                let objNotification = CSFNotification(fromJson: JSON(resultDict))
                self.arrNotificationList = objNotification.list
                
                self.totalRecordsCount = self.arrNotificationList.count
                if self.arrNotificationList.count > 0
                {
                    self.lblNoRecordFound.isHidden = true
                    self.tblNotification.isHidden = false
                }
                else
                {
                    self.lblNoRecordFound.isHidden = false
                    self.tblNotification.isHidden = true
                }
                DispatchQueue.main.async
                {
                    self.tblNotification.reloadData()
                }
                if self.totalRecordsCount > 0 {
                    self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
                }else {
                    self.cnstTotalRecordsTitleHeight.constant = 0
                }
                self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: 0, totalRecords: self.totalRecordsCount)
                self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: 0, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_NOTIFICATION)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotificationList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as? NotificationCell {

            let objNotification = arrNotificationList[indexPath.row]
            cell.lblDate.text = objNotification.createdAt ?? ""
            cell.lblUserName.text = " Dear \(objUserInfo.firstName ?? "")"
            if let mesg = objNotification.message ,mesg != ""
            {
                if (UserDefaults.standard.value(forKey: kIsDarkModeEnabled) as? Bool) == true{
                    cell.txtDesc.attributedText = mesg.htmlToAttributedString(size: 15, hexStringColor: "#FFFFFF", fontFamily: fontname.openSansRegular)
                }
                
                if (UserDefaults.standard.value(forKey: kIsLightModeEnabled) as? Bool) == true{
                    cell.txtDesc.attributedText = mesg.htmlToAttributedString(size: 15, hexStringColor: "#000000", fontFamily: fontname.openSansRegular)
                }
                
                if (UserDefaults.standard.value(forKey: kIsSystemModeModeEnabled) as? Bool) == true{
                    if isDarkTheme() {
                        cell.txtDesc.attributedText = mesg.htmlToAttributedString(size: 15, hexStringColor: "#FFFFFF", fontFamily: fontname.openSansRegular)
                    }else{
                        cell.txtDesc.attributedText = mesg.htmlToAttributedString(size: 15, hexStringColor: "#000000", fontFamily: fontname.openSansRegular)
                    }
                }
            }
            if let readStatus = objNotification.isRead  , readStatus == "0"
            {
                cell.backgroundColor = UIColor(named: "theme_noti_unread_color")//Colors.theme_lightorange_color
                cell.contentView.backgroundColor = UIColor(named: "theme_noti_unread_color")//Colors.theme_lightorange_color
            }
            else
            {
                cell.backgroundColor = Colors.clearColor
                cell.contentView.backgroundColor =  Colors.clearColor
            }
            cell.btnRow.tag = indexPath.row
            cell.btnRow.addTarget(self, action: #selector(btnRowClicked(sender:)), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: - traitCollectionDidChange
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Do sonthing
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                reloadVisibleCellsOnThemeChange()
            }
        }else{
            reloadVisibleCellsOnThemeChange()
        }
    }
    
    // MARK: - reloadVisibleCellsOnThemeChange
    func reloadVisibleCellsOnThemeChange(){
        //https://stackoverflow.com/questions/57706671/detecting-ios-dark-mode-change
        DispatchQueue.main.async { [self] in
            guard let visibleRows = tblNotification.indexPathsForVisibleRows else { return }
            tblNotification.beginUpdates()
            tblNotification.reloadRows(at: visibleRows, with: .none)
            tblNotification.endUpdates()
        }
    }

}
class NotificationCell : UITableViewCell
{
    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var txtDesc : UITextView!
    @IBOutlet weak var btnRow : UIButton!
    override func awakeFromNib() {
        txtDesc.isEditable = false
        txtDesc.isUserInteractionEnabled = true
    }
}
