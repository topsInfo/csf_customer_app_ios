//
//  UserListVC.swift
//  CSFCustomer
//
//  Created by Tops on 22/01/21.
//

import UIKit
import SwiftyJSON
class PaymentRefundVC : UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var viewTotalRecord : TotalRecordView!
    @IBOutlet weak var cnstTotalRecordsTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var tblPaymentRefund : UITableView!
    @IBOutlet weak var btnClearFilter : UIButton!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var btnExportReport : UIButton!
    @IBOutlet weak var btnClearFilterHeightConstant: NSLayoutConstraint!
    
    // MARK: - Global Variable
    var arrSelectedSections : [Int] = [Int]()
    var arrSections : [Int] = [Int]()
    var arrPaymentRefundList :[RefundList] = [RefundList]()
    var userID : String = ""
    var pageNo : Int = 1
    var isLoadingList : Bool = false
    var totalPages : Int = 0
    var objUserInfo = UserInfo()
    var objPaymentRefund = PaymentRefund()
    var strStartDate : String = ""
    var strEndDate : String = ""
    var strSearch : String = ""
    var refreshControl = UIRefreshControl()
    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(getFilterRecords(_:)), name: NSNotification.Name(rawValue: "PaymentFilterApplied"), object: nil)
        objUserInfo = getUserInfo()
        topBarView.topBarBGColor()
        getPaymentRefundList(isShowLoader: true)
        tblPaymentRefund.dataSource = self
        tblPaymentRefund.delegate = self
        tblPaymentRefund.estimatedRowHeight = 114
        tblPaymentRefund.rowHeight = UITableView.automaticDimension
        tblPaymentRefund.estimatedSectionHeaderHeight = 75
        tblPaymentRefund.sectionHeaderHeight = UITableView.automaticDimension
        tblPaymentRefund.tableFooterView? = UIView()
        btnClearFilterHeightConstant.constant = 0
        btnClearFilter.layer.cornerRadius = 2.0
        btnClearFilter.layer.masksToBounds = true
        btnClearFilter.isHidden = true
        lblNoRecordFound.isHidden = true
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tblPaymentRefund.addSubview(refreshControl)
        
        cnstTotalRecordsTitleHeight.constant = 0
        viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount)
        viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_INVOICE)
    }
    @objc func refresh(_ sender: AnyObject)
    {
        self.pageNo = 1
        getPaymentRefundList(isShowLoader: false)
    }
    
    // MARK: - getPaymentRefundList
    func getPaymentRefundList(isShowLoader: Bool)
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["search_text"] = strSearch
        params["start_date"] = strStartDate
        params["end_date"] = strEndDate
        params["page_id"] = pageNo
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.PaymentRefund, showLoader:isShowLoader) { [self] (resultDict, status, message) in
            self.tblPaymentRefund.isHidden = false
            if status == true
            {
                self.refreshControl.endRefreshing()
                self.isLoadingList = false
                if pageNo == 1
                {
                    self.arrPaymentRefundList.removeAll()
                }
                self.objPaymentRefund = PaymentRefund(fromJson: JSON(resultDict))
                self.totalPages = self.objPaymentRefund.totalPage ?? 1
                if self.arrPaymentRefundList.count > 0
                {
                    self.arrPaymentRefundList.append(contentsOf: objPaymentRefund.list)
                    for _ in 0..<objPaymentRefund.list.count
                    {
                        self.arrSections.append(0)
                    }
                }
                else
                {
                    self.arrPaymentRefundList = objPaymentRefund.list
                    self.arrSections.removeAll()
                    for _ in 0..<self.arrPaymentRefundList.count
                    {
                        self.arrSections.append(0)
                        
                    }
                }
                self.totalRecordsCount = objPaymentRefund.totalCount ?? 0
                if self.arrPaymentRefundList.count > 0
                {
                    self.lblNoRecordFound.isHidden = true
                    self.btnExportReport.isHidden = false
                }
                else
                {
                    self.lblNoRecordFound.isHidden = false
                    self.btnExportReport.isHidden = true
                }
                
                DispatchQueue.main.async {
                    self.tblPaymentRefund.reloadData()
                }
                
                if self.totalRecordsCount > 0 {
                    self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
                } else {
                    self.cnstTotalRecordsTitleHeight.constant = 0
                }
                self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
                self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_INVOICE)
            } else {
                self.refreshControl.endRefreshing()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    @objc func getFilterRecords(_ notification: NSNotification)
    {
        self.view.layoutIfNeeded()
        pageNo = 1
        strStartDate = notification.userInfo?["start_date"] as? String ?? ""
        strEndDate = notification.userInfo?["end_date"] as? String ?? ""
        strSearch = notification.userInfo?["search_text"] as? String ?? ""
        if strStartDate == "" && strEndDate == "" && strSearch == ""
        {
            btnClearFilterHeightConstant.constant = 0
            btnClearFilter.isHidden = true
        }
        else
        {
            btnClearFilterHeightConstant.constant = 45
            btnClearFilter.isHidden = false
        }
        getPaymentRefundList(isShowLoader: true)
    }
    func clearData()
    {
        strStartDate = ""
        strEndDate = ""
        strSearch =  ""
        pageNo = 1
        btnClearFilterHeightConstant.constant = 0
        self.btnClearFilter.isHidden = true
        tblPaymentRefund.layoutIfNeeded()
        getPaymentRefundList(isShowLoader: true)
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnClearFilterClicked()
    {
        clearData()
    }
    @IBAction func btnSearchClicked()
    {
        if let vc = appDelegate.getViewController("SearchUserVC", onStoryboard: "Profile") as?  SearchUserVC {
            vc.strSearch = self.strSearch
            tblPaymentRefund.layoutIfNeeded()
            UIView.setAnimationsEnabled(true)
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc,animated: true, completion: nil)
        }
    }
    @IBAction func btnFilterClicked()
    {
        tblPaymentRefund.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        if let refundFilterVC = appDelegate.getViewController("PaymentRefundFilterVC", onStoryboard: "Invoice") as? PaymentRefundFilterVC {
            refundFilterVC.strStartDt = self.strStartDate
            refundFilterVC.strEndDt = self.strEndDate
            refundFilterVC.strSearch = self.strSearch
            refundFilterVC.modalPresentationStyle = .overFullScreen
            self.present(refundFilterVC,animated: true, completion: nil)
        }
    }
    
    @IBAction func btnExportReportClicked(_ sender: UIButton) {
        print("btnExportReportClicked - PaymentRefundVC")
        
        var params : [String:Any] = [:]
        self.showHUD()
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["search_text"] = strSearch
        params["start_date"] = strStartDate
        params["end_date"] = strEndDate
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.exportRefundInvoices, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                if let filePath = (resultDict as NSDictionary).value(forKey: "link") as? String
                {
                    self.downloadFile(InvoiceID:"" ,pdfFileURL:filePath, downloadedFileType: "Report")
                }
                else
                {
                    self.hideHUD()
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }  
}
extension PaymentRefundVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrPaymentRefundList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrSections.count > 0
        {
            if arrSections[indexPath.section] == 1
            {
                return UITableView.automaticDimension
            }
        }
        else
        {
            return 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RefundHeaderCell") as? RefundHeaderCell {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(sectionTapped(sender:)))
            cell.contentView.addGestureRecognizer(tapGesture)
            cell.contentView.tag = section
            cell.contentView.isUserInteractionEnabled = true
            
            if arrPaymentRefundList.count > 0
            {
                let objPaymentRefund = arrPaymentRefundList[section]
                cell.lblDateOfRefund.text = objPaymentRefund.refundDate ?? ""
                cell.lblRefundAmount.text = "\(kCurrencySymbol) \(objPaymentRefund.refundAmount ?? "0")"
                cell.lblHAWNo.text = objPaymentRefund.hawbNumber ?? ""
                cell.lblTotalUSD.text = "\(kCurrencySymbol) \(objPaymentRefund.totalUsd ?? "0")"
                if arrSections.count > 0
                {
                    if arrSections[section] == 1
                    {
                        cell.imgUpDown.image = UIImage(named: "up")
                        cell.longSeperator.isHidden = true
                        cell.shortSeperator.isHidden = false
                    }
                    else
                    {
                        cell.imgUpDown.image = UIImage(named: "down")
                        cell.longSeperator.isHidden = false
                        cell.shortSeperator.isHidden = true
                    }
                }
                else
                {
                    cell.imgUpDown.image = UIImage(named: "down")
                    cell.longSeperator.isHidden = false
                    cell.shortSeperator.isHidden = true
                }
            }
            return cell.contentView
        }
        return UITableViewCell().contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RefundCell", for: indexPath) as? RefundCell {
            if self.arrPaymentRefundList.count > 0
            {
                let objPaymentRefund = arrPaymentRefundList[indexPath.section]
                cell.lblCustomerName.text = objPaymentRefund.customerName ?? ""
                cell.lblRefundMethod.text = objPaymentRefund.refundMethod ?? ""
                cell.lblRefundType.text = objPaymentRefund.refundType ?? ""
                cell.lblInvoiceDate.text = objPaymentRefund.dateOfInvoice ?? ""
                cell.btnView.tag = indexPath.section
                cell.btnView.addTarget(self, action: #selector(btnViewClicked(sender:)), for: .touchUpInside)
            }
            return cell
        }
        return UITableViewCell()
    }
    //Pagination
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isLoadingList = false
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (Int(tblPaymentRefund.contentOffset.y + tblPaymentRefund.frame.size.height) >= Int(tblPaymentRefund.contentSize.height))
        {
            if !isLoadingList{
                isLoadingList = true
                if self.totalPages == pageNo
                {
                    return
                }
                pageNo += 1
                getPaymentRefundList(isShowLoader: true)
            }
        }
    }
    
    // MARK: - Custom methods
    @objc func btnViewClicked(sender:UIButton)
    {
        var params : [String:Any] = [:]
        if self.arrPaymentRefundList.count > 0 {
            let objRefundList =  self.arrPaymentRefundList[sender.tag]
            objUserInfo = getUserInfo()
            params["user_id"] = objUserInfo.id ?? ""
            params["invoice_id"] = objRefundList.id ?? ""
            URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ViewInvoice, showLoader:true) { (resultDict, status, message) in
                if status == true
                {
                    let objInvoiceDetail = InvoiceDetail(fromJson: JSON(resultDict))
                    if let vc = appDelegate.getViewController("InvoiceDetailVC", onStoryboard: "Invoice") as? InvoiceDetailVC {
                        vc.userID = self.objUserInfo.id ?? ""
                        vc.objInvoiceDetail = objInvoiceDetail
                        if let status = objInvoiceDetail.paidStatus, status == "0"
                        {
                            vc.isPaid = false
                        }
                        else
                        {
                            vc.isPaid = true
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }

        }
    }
    
    @objc func sectionTapped(sender:UITapGestureRecognizer) //new change
    {   let tag = sender.view!.tag
        if arrSections.count > 0
        {
            if arrSections[tag] == 0
            {
                arrSections[tag] = 1
            }
            else
            {
                arrSections[tag] = 0
            }
        }
        DispatchQueue.main.async {
            let section = NSIndexSet(index: tag)
            self.tblPaymentRefund.reloadSections(section as IndexSet, with: .none)
            self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
            self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_INVOICE)

        }
    }
    
}
class RefundHeaderCell : UITableViewCell
{
    @IBOutlet weak var imgChecked : UIImageView!
    @IBOutlet weak var imgUpDown : UIImageView!
    @IBOutlet weak var lblDateOfRefund : UILabel!
    @IBOutlet weak var lblRefundAmount : UILabel!
    @IBOutlet weak var lblHAWNo : TapAndCopyLabel!
    @IBOutlet weak var lblTotalUSD : UILabel!
    @IBOutlet weak var longSeperator : UILabel!
    @IBOutlet weak var shortSeperator : UILabel!
}
class RefundCell :UITableViewCell
{
    @IBOutlet weak var lblCustomerName : TapAndCopyLabel!
    @IBOutlet weak var lblRefundMethod : UILabel!
    @IBOutlet weak var lblRefundType: UILabel!
    @IBOutlet weak var lblInvoiceDate: UILabel!
    @IBOutlet weak var btnView : UIButton!
    @IBOutlet weak var viewContainerView : UIView!
    
    override func awakeFromNib() {
        viewContainerView.layer.cornerRadius = viewContainerView.frame.size.height / 2
    }
}
