//
//  ForgotPasswordVC.swift
//  CSFCustomer
//
//  Created by Tops on 04/12/20.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var txtEmail : UITextField!
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    // MARK: - custom methods
    func configureControls()
    {
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor //Colors.theme_lightgray_color.cgColor
        configureTextField(textField:txtEmail, placeHolder: "Enter your email address", font: fontname.openSansRegular, fontSize: 14,
                           textColor:UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor:UIColor(named: "theme_textfield_bgcolor")!)
    }
    /**
     Displays an alert with a given title and message on a specified view controller.
     */
    func passwordAlert(title:String,msg:String,vc:UIViewController)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okaction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }
        okaction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(okaction)
        vc.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtEmail.text!)
        {
            self.showAlert(title: kAppName, msg: Messsages.msg_email, vc: self)
            return false
        }
        else if !isValidEmail(testStr: txtEmail.text!)
        {
            self.showAlert(title: kAppName, msg: Messsages.msg_invalid_email, vc: self)
            return false
        }
        return true
    }
    
    // MARK: - IBAction methods
    @IBAction func btnSendClicked()
    {
        self.view.endEditing(true)
        if isValidData()
        {
            var params : [String:Any] = [:]
            params["email_address"] = txtEmail.text ?? ""
            URLManager.shared.URLCall(method: .post, parameters: params, header: false, url: APICall.ForgotPassword, showLoader: true) { (resultDict, status, message) in
                if status == true
                {
                    self.passwordAlert(title: kAppName, msg: message, vc: self)
                }
                else
                {
                    self.showAlert(title: kAppName, msg: message, vc: self)
                }
            }
        }
    }
    @IBAction func btnLoginClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }    
}
