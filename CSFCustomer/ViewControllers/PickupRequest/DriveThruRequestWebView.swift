//
//  DriveThruRequestWebView.swift
//  CSFCustomer
//
//  Created by Tops on 09/01/24.
//

import UIKit
import WebKit
class DriveThruRequestWebView: UIViewController,WKNavigationDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var dimmerView : UIView!
    @IBOutlet weak var webView : WKWebView!
    @IBOutlet weak var lblTitle : UILabel!
    
    // MARK: - Global Variable
    var strURL : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        
        if !strURL.isEmpty{
            loadWebView(strURL: strURL)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideHUDFromView(view: dimmerView)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func clearWebviewCache(){
        WKWebsiteDataStore.default().removeData(ofTypes: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache], modifiedSince: Date(timeIntervalSince1970: 0), completionHandler:{ })
    }
    
    // MARK: - Custom Methods
    func loadWebView(strURL:String)
    {
        if let url: URL = URL(string: strURL)
        {
            webView.load(URLRequest(url: url))
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webview delegate methods
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("SHOW PROGRESS")
        self.showHUDOnView(view: dimmerView)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("HIDE PROGRESS")
        self.hideHUDFromView(view: dimmerView)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("HIDE PROGRESS")
        self.hideHUDFromView(view: dimmerView)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        self.hideHUDFromView(view: dimmerView)
        if error._code == -1001 { // TIMED OUT:
            print("HIDE PROGRESS")
            // CODE to handle TIMEOUT
        } else if error._code == -1003 { // SERVER CANNOT BE FOUND
            print("HIDE PROGRESS")
            // CODE to handle SERVER not found
        } else if error._code == -1100 { // URL NOT FOUND ON SERVER
            print("HIDE PROGRESS")
            // CODE to handle URL not found
        } else if error._code == -1200 { // URL NOT FOUND ON SERVER
            print("HIDE PROGRESS")
            // CODE to handle SSL Error and Secure Connection
        }
    }
}
