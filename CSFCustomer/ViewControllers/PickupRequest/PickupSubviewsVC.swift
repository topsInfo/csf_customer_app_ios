//
//  PickupSubviewsVC.swift
//  CSFCustomer
//
//  Created by Tops on 01/04/21.
//

import UIKit
import SwiftyJSON
enum PickupOptions : String
{
    case Pickup = "1"
    case CurbPickup = "2"
    case Delivery = "3"
}
class PickupSubviewsVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var dimmerView : UIView!
    @IBOutlet weak var tblCurb : UITableView!
    @IBOutlet weak var curbPickupView : UIView!
    @IBOutlet weak var deliveryRequestView : UIView!
    @IBOutlet weak var pickupOptionsView : UIView!
    @IBOutlet weak var topConst : NSLayoutConstraint!

    @IBOutlet weak var txtPickupDate : UITextField!
    @IBOutlet weak var txtPickupTime : UITextField!
    @IBOutlet weak var txtVehicleDesc : UITextView!
    @IBOutlet weak var txtAdditionalNotes : UITextView!
    
    @IBOutlet weak var imgPickup : UIImageView!
    @IBOutlet weak var imgDriveThruReq : UIImageView!
    @IBOutlet weak var imgDelivery : UIImageView!
    
    @IBOutlet weak var addressCollectionView : UICollectionView!
    @IBOutlet weak var lblNoAddressFound : UILabel!
    @IBOutlet weak var pageControl : UIPageControl!
    
    // MARK: - Global Variables
    var requestType : String = ""
    var curbPickupHeight : CGFloat = 420
    var deliveryRequestHeight : CGFloat = 445
    var objUserInfo = UserInfo()
    var arrTimeList : [String] = [String]()
    var strInvoiceIDs : String = ""
    var navvc : UINavigationController?
    var picker : UIPickerView?
    var strPickupDate : String  = ""
    var strPickupTime : String  = ""
    var strVehicleDesc : String = ""
    var strAdditionalNotes : String = ""
    var selectedAddressID : String = ""
    var selectedRowID : Int = -1
    var arrAddressList :[AddressList] = [AddressList]()
    var objMyAddress = Address()
    var isFromVC : String = ""
    var referenceVC : UIViewController?
    var strDriveThruReqURL : String = ""
    
    lazy var targetFormatter : DateFormatter =
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
    lazy  var sourceFormatter : DateFormatter =
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            return formatter
        }()
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestAddressDetail(_:)), name: Notification.Name("AddressUpdated"), object: nil)
        addressCollectionView.delegate = self
        addressCollectionView.dataSource = self
        tblCurb.isScrollEnabled = false
        tblCurb.alwaysBounceVertical = false
        objUserInfo = getUserInfo()
        txtPickupDate.paddingView(xvalue: 10)
        txtPickupTime.paddingView(xvalue: 10)
        txtVehicleDesc.delegate = self
        txtAdditionalNotes.delegate = self
        
        txtPickupDate.addInputViewDatePicker(target: self,selector: #selector(doneButtonPressed),selectorChange:#selector(dateValueChanged),formatType:DateFormatType.Day.dispValue(),from:"PickupRequest")
        picker = txtPickupTime.addCustomPicker(target: self, selector: #selector(btnDonePressed))
        picker?.selectRow(0, inComponent: 0, animated: true)
        picker?.delegate = self
        
        dimmerView.backgroundColor =  UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.8)
        pickupOptionsView.center = CGPoint(x: self.view.frame.size.width  / 2,
                                           y: self.view.frame.size.height / 2)
        self.view.addSubview(pickupOptionsView)
        self.getAddressDetails()
    }
    func redirectToDriveThruList(){
        if let vc = appDelegate.getViewController("DriveThruVC", onStoryboard: "DriveThru") as? DriveThruVC{
            
            if let refVC = referenceVC {
                refVC.navigationController?.pushViewController(vc, animated: true)
            }else{
                if let objNavigationController  = appDelegate.topNavigation(){
                    objNavigationController.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnCancelClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnPickupOptionsClicked(_ sender:UIButton)
    {
        self.requestType = ""
        self.strPickupDate  = ""
        self.strPickupTime  = ""
        self.strVehicleDesc = ""
        self.strAdditionalNotes = ""
        imgPickup.image = UIImage(named: "radio_btn2")
        imgDriveThruReq.image = UIImage(named: "radio_btn2")
        imgDelivery.image = UIImage(named: "radio_btn2")
        if sender.tag == 101
        {
            imgPickup.image = UIImage(named: "radio_btn")
            requestType = PickupOptions.Pickup.rawValue
            self.popupAlert(title: "", message: Messsages.msg_pickup_request_confirmation, actionTitles: ["Confirm","Cancel"], actions:[{action1 in
                self.callAPIForPickupRequest()
            },{ action2 in
                
            }])
        }
        else if sender.tag == 102
        {
            print("Drive Thru Request Clicked")
            pickupOptionsView.removeFromSuperview()
            self.dismiss(animated: true) { [self] in
                redirectToDriveThruList()
            }
        }
        else if sender.tag == 103
        {
            requestType = PickupOptions.Delivery.rawValue
            self.pickupOptionsView.removeFromSuperview()
            if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
               let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
            {
                let topHeight = safeAreaHeight - deliveryRequestHeight
                topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
                dimmerView.backgroundColor = UIColor.white
                deliveryRequestView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: deliveryRequestHeight + safeAreaHeight)
                UIView.transition(with: self.dimmerView, duration: 0.3, options: .transitionCrossDissolve, animations: { [self] in
                    self.dimmerView.addSubview(self.deliveryRequestView)
                    
                }, completion: nil)
            }
        }
    }
    
    @IBAction func btnAddAddressClicked()
    {
        if let vc = appDelegate.getViewController("AddressVC", onStoryboard: "Profile") as? AddressVC {
            vc.type = "FromDeliveryOptions"
            vc.addTitle = "Add New Address"
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.addChild(vc)
                self.view.addSubview(vc.view)
                vc.didMove(toParent: self)
            }, completion: nil)
        }
    }
    @IBAction func btnCurbSubmitClicked()
    {
        if isCurbValidData()
        {
            let strPickupDt = txtPickupDate.text
            let dt = sourceFormatter.date(from: strPickupDt!)
            if dt != nil
            {
                strPickupDate = destinationFormatter().string(from: dt!)
            }
            strPickupTime = txtPickupTime.text!
            strVehicleDesc = txtVehicleDesc.text!
            callAPIForPickupRequest()
        }
    }
    @IBAction func btnDeliverySubmitClicked()
    {
        if isDeliveryValidData()
        {
            strAdditionalNotes = txtAdditionalNotes.text!
            callAPIForPickupRequest()
        }
    }
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSubmitCurbRequestClicked()
    {
        if isValidData()
        {
            
        }
    }
    // MARK: - Custom methods
    @objc func doneButtonPressed()
    {
        self.view.endEditing(true)
        if let  datePicker = txtPickupDate.inputView as? UIDatePicker {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            txtPickupDate.text = formatter.string(from: datePicker.date)
            txtPickupTime.text = ""
            callAPIToFetchPickupTime()
        }
    }
    @objc func btnDonePressed()
    {
        self.view.endEditing(true)
        let row = picker!.selectedRow(inComponent: 0)
        if arrTimeList.count > 0 {
            txtPickupTime.text = arrTimeList[row]
        }
        
    }
    @objc func getLatestAddressDetail(_ notification:NSNotification)
    {
        getAddressDetails()
    }
    @objc func dateValueChanged()
    {
        
    }
    
    // MARK: - isCurbValidData
    func isCurbValidData() -> Bool
    {
        if isCheckNull(strText: txtPickupDate.text!)
        {
            appDelegate.showToast(message: Messsages.msg_select_date, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtPickupTime.text!)
        {
            appDelegate.showToast(message: Messsages.msg_select_time, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtVehicleDesc.text!)
        {
            appDelegate.showToast(message: Messsages.msg_vehicle_description, bottomValue: getSafeAreaValue())
            return false
        }
        
        return true
    }
    
    // MARK: - isDeliveryValidData
    func isDeliveryValidData() -> Bool
    {
        if isCheckNull(strText: txtAdditionalNotes.text!)
        {
            appDelegate.showToast(message: Messsages.msg_additional_notes, bottomValue: getSafeAreaValue())
            return false
        }
        else if selectedAddressID == ""
        {
            appDelegate.showToast(message: Messsages.msg_select_delivery_address, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    
    // MARK: - callAPIForPickupRequest
    func callAPIForPickupRequest()
    {
        self.showHUD()
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["request_type"] = requestType
        params["invoice_id"] = strInvoiceIDs
        params["description"] = strVehicleDesc
        params["pickup_date"] = strPickupDate
        params["pickup_time"] = strPickupTime
        params["additional_notes"] = strAdditionalNotes
        params["address_id"] = selectedAddressID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.PickupRequest, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()
                if self.isFromVC == "Invoice"
                {
                    NotificationCenter.default.post(name: Notification.Name("PickupRequestCompleted"), object: nil)
                }
                else
                {
                    NotificationCenter.default.post(name: Notification.Name("RefreshInvoiceList"), object: nil)
                }
                self.popupAlert(title: "", message: message, actionTitles: ["Ok"], actions:[{action1 in
                    self.dismiss(animated: true, completion: nil)
                }])
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - callAPIToFetchPickupTime
    func callAPIToFetchPickupTime()
    {
        self.showHUD()
        var targetDt : String = ""
        let sourceDt = sourceFormatter.date(from: txtPickupDate.text!)
        if sourceDt != nil
        {
            targetDt = targetFormatter.string(from: sourceDt!)
        }
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["date"] = targetDt
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.FetchPickupTime, showLoader:false) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()
                if let arrTime = (resultDict as NSDictionary).value(forKey: "list") as? [NSDictionary]
                {
                    self.arrTimeList = arrTime.map({$0.value(forKey: "time") as! String})
                }
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtPickupDate.text!)
        {
            showAlert(title: "", msg: Messsages.msg_select_date, vc: self)
            return false
        }
        return true
    }
    
    // MARK: - getAddressDetails
    func getAddressDetails()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.MyAddress, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.objMyAddress = Address(fromJson:JSON(resultDict))
                self.arrAddressList = self.objMyAddress.list ?? []
                self.arrAddressList = self.arrAddressList.filter({$0.officeId == "0"})
                if self.arrAddressList.count > 0
                {
                    DispatchQueue.main.async {
                        self.addressCollectionView.isHidden = false
                        self.lblNoAddressFound.isHidden = true
                        if self.arrAddressList.count > 0
                        {
                            self.pageControl.isHidden = false
                            self.pageControl.numberOfPages = self.arrAddressList.count
                            self.selectedRowID = self.arrAddressList.firstIndex(where: {  $0.isDefault == "1"}) ?? 0
                            self.selectedAddressID = self.arrAddressList[self.selectedRowID].id ?? ""
                        }
                        else
                        {
                            self.pageControl.isHidden = true
                        }
                        self.addressCollectionView.reloadData()
                    }
                }
                else
                {
                    self.addressCollectionView.isHidden = true
                    self.lblNoAddressFound.isHidden = false
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}
extension PickupSubviewsVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAddressList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"DeliveryAddressCell", for: indexPath) as? DeliveryAddressCell {
            if arrAddressList.count > 0
            {
                let objAddressList = arrAddressList[indexPath.row]
                cell.lblAddTitle.text = objAddressList.title ?? ""
                cell.lblAddress.text = objAddressList.address ?? ""
                if objAddressList.isDefault == "1"
                {
                    cell.imgCheck.image = UIImage(named: "select")
                }
                else
                {
                    cell.imgCheck.image = UIImage(named: "unselect")
                }
                if selectedRowID == indexPath.row
                {
                    //Colors.theme_orange_color
                    cell.btnSelect.setTitleColor(UIColor(named: "theme_orange_color"), for: .normal)
                    cell.btnSelect.setTitle("Selected", for: .normal)
                }
                else
                {   //Colors.theme_blue_color
                    cell.btnSelect.setTitleColor(UIColor(named: "theme_label_blue_color"), for: .normal)
                    cell.btnSelect.setTitle("Select", for: .normal)
                }
                cell.btnSelect.tag = indexPath.row
                cell.btnSelect.addTarget(self, action: #selector(btnSelectClicked(sender:)), for: .touchUpInside)
            }
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //print("addressCollectionView width : \(addressCollectionView.frame.size.width)")
        return CGSize(width: addressCollectionView.frame.size.width, height: 155)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }
    // MARK: - Scrollview delegate methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let roundedIndex = scrollView.contentOffset.x / scrollView.frame.size.width
        self.pageControl.currentPage = Int(roundedIndex)
    }
    // MARK: - Custom methods
    func getHeightForLable(labelWidth: CGFloat, numberOfLines: Int = 0, labelText: String, labelFont: UIFont) -> CGFloat {
        let tempLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = numberOfLines
        tempLabel.text = labelText
        tempLabel.font = labelFont
        tempLabel.sizeToFit()
        return tempLabel.frame.height
    }
    @objc func btnSelectClicked(sender:UIButton)
    {
        let objAddressList = arrAddressList[sender.tag]
        selectedAddressID = objAddressList.id ?? ""
        selectedRowID = sender.tag
        DispatchQueue.main.async {
            self.addressCollectionView.reloadData()
        }
    }
}
extension PickupSubviewsVC : UIPickerViewDataSource,UIPickerViewDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrTimeList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrTimeList[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrTimeList.count > 0 {
            txtPickupTime.text =  arrTimeList[row]
        }
        
    }
}
extension PickupSubviewsVC : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return newText.count <= 200
    }
}
class DeliveryAddressCell : UICollectionViewCell
{
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var containerStackView : UIStackView!
    @IBOutlet weak var lblAddTitle : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var btnSelect : UIButton!
    @IBOutlet weak var imgCheck : UIImageView!
    @IBOutlet weak var btnDefaultAddress : UIButton!
    
    override func awakeFromNib() {
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
    }
}

