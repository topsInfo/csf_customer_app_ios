//
//  PickupRequestVC.swift
//  CSFCustomer
//
//  Created by Tops on 01/04/21.
//

import UIKit
import SwiftyJSON
class PickupRequestVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var viewTotalRecord : TotalRecordView!
    @IBOutlet weak var cnstTotalRecordsTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var tblInvoiceList : UITableView!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var btnClearFilter : UIButton!
    @IBOutlet weak var btnRequest : UIButton!
    
    // MARK: - Global Variables
    var strInvoiceIDs : String = ""
    var objUserInfo = UserInfo()
    var isLoadingList : Bool = false
    var pageNo : Int = 1
    var totalPages : Int = 0
    var arrList : [InvoiceList] = [InvoiceList]()
    var arrMemberList : [MemberList] = [MemberList]()
    var isShowLoader : Bool = true
    var filename : String  = ""
    var arrSelectedSections : [Int] = [Int]()
    //filter vars
    var strMemberID : String = ""
    var strMemberName  : String = ""
    var strStartDate : String  = ""
    var strEndDate : String  = ""
    var strDateType :String = ""
    var strSearch : String = ""
    var refreshControl = UIRefreshControl()
    let totalRecordsTitleHeight : CGFloat = 60
    var totalRecordsCount : Int = 0
    var strDriveThruReqUrl : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(getFilterRecords(_:)), name: NSNotification.Name(rawValue: "UnderliverdListFilter"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords(_:)), name: NSNotification.Name(rawValue: "RefreshInvoiceList"), object: nil)
        objUserInfo = getUserInfo()
        tblInvoiceList.estimatedRowHeight = 150
        tblInvoiceList.rowHeight = UITableView.automaticDimension
        tblInvoiceList.dataSource = self
        tblInvoiceList.delegate = self
        tblInvoiceList.tableFooterView = UIView()
        lblNoRecordFound.isHidden = true
        btnClearFilter.isHidden = true
        btnRequest.isHidden = true
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblInvoiceList.addSubview(refreshControl)

        cnstTotalRecordsTitleHeight.constant = 0
        viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount)
        viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PACKAGES)

        getInvoiceList(isShowLoader: true)
    }
    
    @objc func refresh()
    {
        pageNo = 1
        getInvoiceList(isShowLoader: false)
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnRequestClicked()
    {
        if isValidData()
        {
            if let vc = appDelegate.getViewController("PickupSubviewsVC", onStoryboard: "Invoice") as? PickupSubviewsVC {
                vc.isFromVC = "PickupRequest"
                vc.strInvoiceIDs = strInvoiceIDs
                
                vc.referenceVC = self
                vc.strDriveThruReqURL = self.strDriveThruReqUrl
                
                vc.navvc = self.navigationController
                vc.view.backgroundColor =  UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.5)
                vc.modalPresentationStyle = .overFullScreen
                self.navigationController?.present(vc, animated: true, completion: nil)
                // self.present(vc,animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnFilterClicked()
    {
        UIView.setAnimationsEnabled(true)
        if let filterVC = appDelegate.getViewController("InvoiceFilterVC", onStoryboard: "Invoice") as? InvoiceFilterVC {
            //        filterVC.arrShippingMethods = self.arrShippingMethods
            filterVC.isFrom = "PickupRequest"
            filterVC.arrMemberList = arrMemberList
            filterVC.strMemberID = self.strMemberID
            filterVC.strStartDt = self.strStartDate
            filterVC.strEndDt = self.strEndDate
            filterVC.dateType = self.strDateType
            filterVC.strSearch = self.strSearch
            filterVC.strMemberName = self.strMemberName
            filterVC.modalPresentationStyle = .overFullScreen
            self.present(filterVC,animated: true, completion: nil)
            
        }
    }
    @IBAction func btnClearFilterClicked()
    {
        clearData()
    }
    // MARK: - Custom methods
    @objc func getLatestRecords(_ notification: NSNotification)
    {
        print("**** v1.7.6_PickupRequestList ****")
        clearData()
    }
    func  getInvoiceList(isShowLoader : Bool)
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["page_id"] = pageNo
        params["search_text"] = strSearch
        params["member_id"] = strMemberID
        params["start_date"] = strStartDate
        params["end_date"] = strEndDate
        params["date_type"] =  strDateType
        params["is_drive_thru"] =  "0"
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.UndeliveredInvoiceList, showLoader: isShowLoader) { [self] (resultDict, status, message) in
            self.tblInvoiceList.isHidden = false
            self.refreshControl.endRefreshing()

            if status == true
            {
                self.isLoadingList = false
                if self.pageNo == 1
                {
                    self.arrList.removeAll()
                }
                let objMyInvoiceList = Invoice.init(fromJson: JSON(resultDict))
                self.strDriveThruReqUrl = objMyInvoiceList.driveThruReqUrl ?? ""
                
                if self.arrList.count > 0
                {
                    self.arrList.append(contentsOf: objMyInvoiceList.list)
                    for _ in 0..<objMyInvoiceList.list.count
                    {
                        self.arrSelectedSections.append(0)
                    }
                }
                else
                {
                    self.arrList = objMyInvoiceList.list
                    self.arrSelectedSections.removeAll()
                    for _ in 0..<self.arrList.count
                    {
                        self.arrSelectedSections.append(0)
                    }
                }
                self.totalRecordsCount = objMyInvoiceList.totalCount ?? 0
                if self.arrList.count > 0
                {
                    self.lblNoRecordFound.isHidden = true
                    self.btnRequest.isHidden = false
                    self.arrMemberList = objMyInvoiceList.memberList
                }
                else
                {
                    self.lblNoRecordFound.isHidden = false
                    self.btnRequest.isHidden = true
                }
                self.totalPages = objMyInvoiceList.totalPage ?? 0
                //new change
                DispatchQueue.main.async {
                    self.tblInvoiceList.reloadData()
                }
                
                if self.totalRecordsCount > 0 {
                    self.cnstTotalRecordsTitleHeight.constant = self.totalRecordsTitleHeight
                }else {
                    self.cnstTotalRecordsTitleHeight.constant = 0
                }
                self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
                self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PACKAGES)

            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        strInvoiceIDs = getInvoiceIDs()
        if strInvoiceIDs == ""
        {
            showAlert(title: "", msg: Messsages.msg_select_invoice_pickup_request, vc: self)
            return false
        }
        return true
    }
    func getInvoiceIDs() -> String
    {
        var strInvoiceID : String  = ""
        for (index,value) in arrSelectedSections.enumerated()
        {
            if value == 1 && arrList.count > 0
            {
                let objList = getInvoiceObject(tag: index)
                if let invoiceID = objList.id
                {
                    let invoiceID = String(invoiceID)
                    if strInvoiceID != ""
                    {
                        strInvoiceID = strInvoiceID + "," + invoiceID
                    }
                    else
                    {
                        strInvoiceID = invoiceID
                    }
                }
            }
        }
        return strInvoiceID
    }
    @objc func getFilterRecords(_ notification: NSNotification)
    {
        pageNo = 1
     //   self.arrList.removeAll()
        strMemberID = notification.userInfo?["member_id"] as? String ?? ""
        strMemberName = notification.userInfo?["member_name"] as? String ?? ""
        strStartDate = notification.userInfo?["start_date"] as? String ?? ""
        strEndDate = notification.userInfo?["end_date"] as? String ?? ""
        strDateType = notification.userInfo?["date_type"] as? String ?? ""
        strSearch = notification.userInfo?["search_text"] as? String ?? ""
        btnClearFilter.isHidden = false
        getInvoiceList(isShowLoader: true)
    }
    func clearData()
    {
        strMemberID =  ""
        strStartDate = ""
        strEndDate = ""
        strDateType = ""
        strSearch =  ""
        strMemberName = ""
        pageNo = 1
       // self.arrList.removeAll()
        self.btnClearFilter.isHidden = true
        getInvoiceList(isShowLoader: true)
    }
    // MARK: - Tableview delegte methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UndeliveredCell") as? UndeliveredCell {
            cell.contentView.backgroundColor = UIColor(named: "theme_white_color") // Colors.theme_white_color
            cell.backgroundColor = UIColor(named: "theme_white_color") //Colors.theme_white_color
            let objList = getInvoiceObject(tag: indexPath.row)
            cell.lblDate.text = objList.dateOfInvoice ?? ""
            cell.lblInvoiceNo.text = objList.hawbNumber ?? ""
            cell.lblName.text = objList.customerName ?? ""
            cell.lblAmount.text = "\(kTTCurrency) \(objList.totalTtd ?? "")"
            cell.btnView.tag = indexPath.row
            cell.btnView.addTarget(self, action: #selector(btnViewClicked(sender:)), for: .touchUpInside)
            if arrSelectedSections.count > 0 {
                if arrSelectedSections[indexPath.row] == 1
                {
                    cell.imgChecked.image = UIImage(named: "checkbox")
                    cell.contentView.backgroundColor = UIColor(named: "theme_lightblue_color")//Colors.theme_lightblue_color
                    cell.backgroundColor = UIColor(named: "theme_lightblue_color") //Colors.theme_lightblue_color
                }
                else
                {
                    cell.imgChecked.image = UIImage(named: "uncheckbox")
                    cell.contentView.backgroundColor = UIColor(named: "theme_white_color")//Colors.theme_white_color
                    cell.backgroundColor = UIColor(named: "theme_white_color") //Colors.theme_white_color
                }
            }else{
                cell.imgChecked.image = UIImage(named: "uncheckbox")
                cell.contentView.backgroundColor = UIColor(named: "theme_white_color")//Colors.theme_white_color
                cell.backgroundColor = UIColor(named: "theme_white_color") //Colors.theme_white_color
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tag = indexPath.row
        if arrSelectedSections[tag] == 0
        {
            arrSelectedSections[tag] = 1
        }
        else
        {
            arrSelectedSections[tag] = 0
        }
        DispatchQueue.main.async {
            self.tblInvoiceList.reloadData()
            self.viewTotalRecord.lblTotalRecordViewTitle.text = showTotalRecordsTitle(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount)
            self.viewTotalRecord.lblTotalRecordViewValue.text = showTotalRecordsValue(selectedRecords: self.arrSelectedSections.filter { $0 == 1}.count, totalRecords: self.totalRecordsCount, suffix: SUFFIX_TOTALCOUNT_PACKAGES)
        }
    }
    
    // MARK: - Pagination
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isLoadingList = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (Int(tblInvoiceList.contentOffset.y + tblInvoiceList.frame.size.height) >= Int(tblInvoiceList.contentSize.height))
        {
            if !isLoadingList{
                isLoadingList = true
                if self.totalPages == pageNo
                {
                    return
                }
                pageNo += 1
                getInvoiceList(isShowLoader: true)
            }
        }
    }
    // MARK: - Custom Methods
    @objc func btnViewClicked(sender:UIButton)
    {
        var params : [String:Any] = [:]
        if arrList.count > 0{
            let objInvoice = getInvoiceObject(tag: sender.tag)
            objUserInfo = getUserInfo()
            params["user_id"] = objUserInfo.id ?? ""
            params["invoice_id"] = objInvoice.id ?? ""
            URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ViewInvoice, showLoader:true) { (resultDict, status, message) in
                if status == true
                {
                    let objInvoiceDetail = InvoiceDetail(fromJson: JSON(resultDict))
                    if let vc = appDelegate.getViewController("InvoiceDetailVC", onStoryboard: "Invoice") as? InvoiceDetailVC {
                        vc.userID = self.objUserInfo.id ?? ""
                        vc.isFullRefund = objInvoiceDetail.fullRefund ?? ""
                        vc.objInvoiceDetail = objInvoiceDetail
                        if let status = objInvoiceDetail.paidStatus, status == "0"
                        {
                            vc.isPaid = false
                        }
                        else
                        {
                            vc.isPaid = true
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    func getInvoiceObject(tag:Int) -> InvoiceList
    {
        if arrList.count > 0
        {
            return arrList[tag]
        }
        return InvoiceList()
    }
}
class UndeliveredCell : UITableViewCell
{
    @IBOutlet weak var imgChecked : UIImageView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblName : TapAndCopyLabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblInvoiceNo : TapAndCopyLabel!
    @IBOutlet weak var btnView : UIButton!
}
