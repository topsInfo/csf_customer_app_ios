//
//  HowToUseVC.swift
//  CSFCustomer
//
//  Created by iMac on 24/01/22.
//

import UIKit

class HowToUseVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var clvHowToUse : UICollectionView!
    @IBOutlet weak var pageProgress: UIProgressView!
    @IBOutlet weak var lblStepNumber: UILabel!
    @IBOutlet weak var tvDescription: UITextView!
    @IBOutlet weak var viewNote: UIView!
    
    // MARK: - Global Variables
    var currentPage : Int = 0
    var numberOfPages : Int = 0
    var scrollViewHeightConstraint: NSLayoutConstraint!
    var moduleData : Module = Module()
    var arrPages : [Page] = [Page]()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        self.lblTitle.text = "How to Use"
        self.tvDescription.isEditable = false
        
        let padding = self.tvDescription.textContainer.lineFragmentPadding
        self.tvDescription.textContainerInset =  UIEdgeInsets(top: 0, left: -padding, bottom: 0, right: -padding)
        
        self.tvDescription.textAlignment = .left
        self.tvDescription.isSelectable = true
        self.tvDescription.isEditable = false
        self.tvDescription.dataDetectorTypes = [.link,.phoneNumber]

        self.tvDescription.text = ""
        self.clvHowToUse.delegate = self
        self.clvHowToUse.isUserInteractionEnabled = true
        self.clvHowToUse.isPagingEnabled = true
        
        if moduleData.pages != nil {
            arrPages = moduleData.pages
            numberOfPages = arrPages.count
        }
        
        DispatchQueue.main.async { [self] in
            clvHowToUse.reloadData()
            configureNote()
        }
    }
    
    // MARK: - configureNote
    func configureNote(){
        self.lblTitle.text = "\(self.currentPage + 1) of \(numberOfPages)"
        let note : String = moduleData.pages[self.currentPage].descriptionField ?? ""
        updateUI(note: note)
    }
    
    // MARK: - IBAction methods
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Scroll View Methods
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        self.currentPage = Int(ceil(x/w))// + 1
        print("Current page is \(self.currentPage)")
        configureNote()        
    }
    
    // MARK: - updateUI
    func updateUI(note : String){
        tvDescription.text = note
        
        let progressValue : Float = Float(self.currentPage + 1) / Float(self.numberOfPages)
        
        print("progress is \(progressValue)")
        self.pageProgress.progress = progressValue
        
        let objPage : Page = arrPages[self.currentPage]
        self.lblStepNumber.text = "\(objPage.title ?? "")"
    }
}

extension HowToUseVC {
    // MARK: - CollectionView Delegate and Datasource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell : HowToUseCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HowToUseCell", for: indexPath) as? HowToUseCell{
            cell.imgPage.image = UIImage(named: "HowToUseImg.png")
            cell.imgPage.contentMode = .scaleAspectFit
            cell.imgPage.isHidden = false
            
            let objPage : Page = arrPages[indexPath.row]
            cell.imgPage.setImage(objPage.forIos, placeHolder: UIImage(named: "HowTousePlaceholder"), isShowLoader: false)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
