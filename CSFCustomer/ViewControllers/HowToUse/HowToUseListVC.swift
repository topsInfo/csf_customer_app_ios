//
//  HowToUseListVC.swift
//  CSFCustomer
//
//  Created by iMac on 25/01/22.
//

import UIKit
import SwiftyJSON

class HowToUseListCell :UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    
    override func awakeFromNib() {
    }
}

class HowToUseListVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tblHowToUseList: UITableView!

    // MARK: - Global Variables
    var objUserInfo = UserInfo()
    var objHowTouseData = HowToUse()
    var arrList : [Module] = [Module]()
    var refreshControl = UIRefreshControl()

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblHowToUseList.addSubview(refreshControl)
        
        objUserInfo = getUserInfo()
        getHowToUseData(isShowLoader: true)
    }
    
    @objc func refresh()
    {
        self.getHowToUseData(isShowLoader: false)
    }

    /**
       Redirects to the HowToUseVC with the specified module.
     */
    func redirectToHowtoUseVC(objModule : Module){
        if let vc = appDelegate.getViewController("HowToUseVC", onStoryboard: "Home") as? HowToUseVC {
            if objModule.pages != nil {
                if objModule.pages.count > 0 {
                    vc.moduleData = objModule
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.showNoDataMsg(objModule: objModule)
                }
            }else{
                self.showNoDataMsg(objModule: objModule)
            }
        }
    }
    
    /**
     Shows a message when there is no data available for the specified module.
     */
    func showNoDataMsg(objModule : Module){
        if let title : String = objModule.title {
            let strMsg : String = "There is no data available to assist you in \(title)."
            appDelegate.showToast(message: strMsg, bottomValue: getSafeAreaValue())
        }
    }

    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension HowToUseListVC {
    /**
     Retrieves the HowToUse data.     
     */
    func getHowToUseData(isShowLoader :Bool)
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? "" //UserInfo.shared.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.HowToUse, showLoader: isShowLoader) { [self] (resultDict, status, message) in
            refreshControl.endRefreshing()
            if status == true
            {   
                objHowTouseData = HowToUse(fromJson: JSON(resultDict))
                arrList = objHowTouseData.modules
                print("Module count is \(arrList.count)")
                
                let desc : String = arrList[0].descriptionField
                print("desc  is \(desc)")
                
                reloadData()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    /**
     Reloads the data for the HowToUseListVC view controller.
     */
    func reloadData(){
        DispatchQueue.main.async { [self] in
            self.tblHowToUseList.reloadData()
        }
    }
}

extension HowToUseListVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "HowToUseListCell", for: indexPath) as? HowToUseListCell {
            let objModule : Module = self.arrList[indexPath.row]
            print("Row is \(indexPath.row)")
            print("Title is \(String(describing: objModule.title))")
            
            cell.lblTitle.text = objModule.title ?? ""
            cell.lblDescription.text = objModule.descriptionField ?? ""
            cell.lblTitle.numberOfLines = 1
            cell.lblDescription.numberOfLines = 0
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objModule : Module = self.arrList[indexPath.row]
        redirectToHowtoUseVC(objModule: objModule)
    }
}
