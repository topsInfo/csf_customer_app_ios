//
//  GenerateTicketVC.swift
//  CSFCustomer
//
//  Created by Tops on 23/12/20.
//

import UIKit
import DropDown
import MobileCoreServices
import Photos

class FeedbackVC: UIViewController,UITextViewDelegate,UITextFieldDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var txtReason : UITextField!
    @IBOutlet weak var txtSubject : UITextField!
    @IBOutlet weak var txtDesc : UITextView!
    @IBOutlet weak var btnSend : UIButton!
    @IBOutlet weak var reasonContainerView : UIView!
    @IBOutlet weak var subjectContainerView : UIView!
    
    // Media
    @IBOutlet weak var clvFeedbackMedia : UICollectionView!
    @IBOutlet weak var cnstClvFeedbackMediaHeight: NSLayoutConstraint!
    @IBOutlet weak var lblUpoadInfo: UILabel!

    // MARK: - Global Variable
    var placeholderText = "Description"
    var objUserInfo = UserInfo()
    var hawb_number : String = ""
    let reasonDropdown = DropDown()
    var selectedReasonID : Int   = -1
    var arrFeedbackReason : [FeedbackReason] = [FeedbackReason]()

    //Media
    var uploadData = Data()
    var mimeType : String = ""
    var arrMedia : [MediaModel] = [MediaModel]()
    var intSelectedIndex : Int = 0
    var uploadDataArray = [Data()]
    var docFileNamesArray : [String] = [String]()
    var mimeTypeArray : [String] = [String]()
    var strDocumentName : String = ""
    var mediaLimit : Int = 5
    var mediaSizeLimit : Int = 2
    var arrDeletedDocuments : [String] = [String]()
    var supportedFileExtensions : [String] = ["pdf","jpg","jpeg","png"]

    var documentUpload : Int = 0
    var clvHeight : CGFloat = 0
    var strUploadInfo : String = ""

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        topBarView.topBarBGColor()
        objUserInfo = getUserInfo()
        configureTextField(textField: txtReason, placeHolder: "Reason", font: fontname.openSansRegular, fontSize: 14, textColor:UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor: UIColor(named:"theme_textfield_bgcolor")!)
        configureTextField(textField: txtSubject, placeHolder: "Subject", font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.clearColor, borderWidth: 0, bgColor: UIColor(named:"theme_textfield_bgcolor")!)
        configureTextView(textField:txtDesc, placeHolder: placeholderText, font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named:"theme_lightgray_color")!, cornerRadius: 0, borderColor: UIColor(named:"theme_lightgray_color")!, borderWidth: 1, bgColor: UIColor(named:"theme_textfield_bgcolor")!)
        txtReason.paddingView(xvalue: 10)
        txtSubject.paddingView(xvalue: 10)
        txtSubject.delegate = self
        
        reasonContainerView.layer.borderWidth = 1
        reasonContainerView.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor
        
        subjectContainerView.layer.borderWidth = 1
        subjectContainerView.layer.borderColor = UIColor(named:"theme_lightgray_color")?.cgColor
        
        txtDesc.textContainerInset = UIEdgeInsets(top: 5, left: 7, bottom: 5, right: 10)
        txtDesc.delegate = self
        
        btnSend.layer.cornerRadius = 2
        btnSend.layer.masksToBounds = true
        setReasonDropDown()
        
        self.strUploadInfo = "jpg, jpeg, png, pdf file allowed and per document size up to \(self.mediaSizeLimit) MB"
        self.lblUpoadInfo.text = self.strUploadInfo
        self.addDataForAddMediaView()
     }
    
    // MARK: - Set Reason DropDown
    /**
     Sets up the reason drop-down menu for providing feedback.
     */     
    func setReasonDropDown()
    {
        reasonDropdown.anchorView = txtReason
        reasonDropdown.bottomOffset = CGPoint(x: 0, y:(txtReason.bounds.size.height))
        reasonDropdown.topOffset = CGPoint(x: 0, y:-(reasonDropdown.anchorView?.plainView.bounds.height)!)
        
        reasonDropdown.width = UIScreen.main.bounds.size.width - 60
        reasonDropdown.shadowRadius = 0
        reasonDropdown.direction = .any
        reasonDropdown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        reasonDropdown.separatorColor =  UIColor(named:"theme_lightgray_color")!
        reasonDropdown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        reasonDropdown.textColor = UIColor(named:"theme_text_color")!
        reasonDropdown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        reasonDropdown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        reasonDropdown.dataSource = arrFeedbackReason.map({$0.title})
        reasonDropdown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                let objReason = self?.arrFeedbackReason[index]
                self?.selectedReasonID  = objReason?.id ?? -1
                self?.txtReason.text = item
                self?.reasonDropdown.hide()
            }
            
        }
    }

    // MARK: - Textfield delegate methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if textField == txtSubject
        {
            return newLength <= 255
        }
        return true
    }
    // MARK: - Textview delegate methods
    func textViewDidBeginEditing(_ textView: UITextView) {
         if textView.text == placeholderText
         {
            textView.text = ""
            textView.textColor = UIColor(named: "theme_black_color")//Colors.theme_black_color
         }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty
        {
            textView.text = placeholderText
            textView.textColor = UIColor(named: "theme_lightgray_color")
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
         let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
         return newText.count < 1000
    }

    // MARK: - isValidData    
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtReason.text!)
        {
            appDelegate.showToast(message: Messsages.msg_select_reason, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtSubject.text!)
        {
            appDelegate.showToast(message: Messsages.msg_subject, bottomValue: getSafeAreaValue())
            return false
        }
        else if txtDesc.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            appDelegate.showToast(message: Messsages.msg_enter_desc, bottomValue: getSafeAreaValue())
            return false
        }
        else if txtDesc.text == placeholderText
        {
            appDelegate.showToast(message: Messsages.msg_enter_desc, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }

    // MARK: - IBAction Methods
    @IBAction func btnReasonClicked()
    {
        self.reasonDropdown.show()
    }
    
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendClicked()
    {
        self.view.endEditing(true)
        
        self.uploadDataArray = self.arrMedia.filter( { $0.type == 2 && $0.isFrom == 0} ).map( {$0.attachmentData} )
        self.docFileNamesArray = (self.arrMedia.filter( { $0.type == 2 && $0.isFrom == 0 } ).map( {$0.docFileNames} ))
        self.mimeTypeArray = (self.arrMedia.filter( { $0.type == 2 && $0.isFrom == 0 } ).map( {$0.mimeType} ))
        
        print("self.uploadDataArray \(self.uploadDataArray)")
        print("self.docFileNamesArray \(self.docFileNamesArray)")
        print("self.mimeTypeArray \(self.mimeTypeArray)")
        
        if isValidData()
        {
            var params : [String:Any] = [:]
            objUserInfo = getUserInfo()
            params["user_id"] = objUserInfo.id ?? ""
            params["reason_id"] = selectedReasonID
            params["subject"] = txtSubject.text ?? ""
            params["description"] = txtDesc.text ?? ""
            params["document[]"] = self.uploadDataArray
            print("Params are \(params)")
            
            URLManager.shared.URLCallMultipleMultipartDataForFeedback(method: .post, parameters: params, header: true, url: APICall.Feedback, showLoader: true, WithName: docFileNamesArray, docFileName: docFileNamesArray, uploadData: uploadDataArray, mimeType: mimeTypeArray) { [self] (resultDict, status, message) in
              
                if status == true
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(),isForSuccess: true)
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                }
            }
        }
    }
    
    /**
     Converts the given object to a JSON string representation.
     */    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}

extension FeedbackVC : UIDocumentPickerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    // MARK: - Choose File
    /**
        Allows the user to choose a file.
    */    
    func chooseFile(){
        self.view.endEditing(true)
        let attributedString = NSAttributedString(string: "Please select option", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15), //your font here
            NSAttributedString.Key.foregroundColor :Colors.theme_gray_color
        ])
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(attributedString, forKey: "attributedTitle")
        
        let takePhotoAction = UIAlertAction(title: "Take a Photo" , style: .default) { (_ action) in
            self.openCamera()
        }
        let photoAction = UIAlertAction(title: "Photo Gallery" , style: .default) { (_ action) in
            let status = PHPhotoLibrary.authorizationStatus()
            print("status is \(status)")
            self.openGallery()
        }
        let browseAction = UIAlertAction(title: "Browse" , style: .default) { (_ action) in
            self.openDocuments()
        }
        let cancelAction = UIAlertAction(title: "Cancel" , style: .cancel) { (_ action) in
            self.dismiss(animated: true, completion: nil)
        }
        takePhotoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        photoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        browseAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(takePhotoAction)
        alert.addAction(photoAction)
        alert.addAction(browseAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Open Camera
    /**
     Opens the camera to capture a photo.
     */     
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            PermissionManager.shared.requestCameraPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.camera
                        imagePicker.allowsEditing = false
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - Open Gallery
    /**
     Opens the gallery to allow the user to select an image.
     */     
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            PermissionManager.shared.requestPhotoPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                        imagePicker.allowsEditing = false                        
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
   
    // MARK: - Open Documents
    /**
     Opens the documents for the current user.
     */     
    func openDocuments()
    {
        let types = [String(kUTTypeJPEG),String(kUTTypePNG),String(kUTTypePDF)] as [String] // You can add more types here as pr your expectation
        let documentPicker = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .formSheet
        present(documentPicker, animated: true, completion: nil)
    }

    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var pickedImageAsset : PHAsset!
        
        if (picker.sourceType != UIImagePickerController.SourceType.camera) {
            //If image picked from photo gallary
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            {
                print("image url is \(url)")
                switch (url.pathExtension).lowercased()
                {
                case "jpg","jpeg":
                    self.mimeType = "image/jpeg"
                    break
                case "png":
                    self.mimeType = "image/png"
                    break
                default:
                    //  print("No file")
                    appDelegate.showToast(message: Messsages.msg_image_type, bottomValue: getSafeAreaValue())
                    return
                }
            }
            
            if let pickedImage = info[.originalImage] as? UIImage
            {
                DispatchQueue.main.async { [self] in
                    let dblSize = pickedImage.getSizeIn(mimeType: self.mimeType, type: .megabyte)
                    if dblSize > 0 {
                        let imgSizeInMb : Double = dblSize
                        print("Image size is \(imgSizeInMb)")
                        print("mediaSizeLimit is \(self.mediaSizeLimit)")
                        if imgSizeInMb > Double(self.mediaSizeLimit) {
                            let msg : String = "Selected attachment should be less than \(self.mediaSizeLimit) mb."
                            appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                            picker.dismiss(animated: true, completion: nil)
                        }else{
                            
                            print("\(self.arrMedia.filter( { $0.type == 2 } ).map( {$0.attachment} ))")
                            let docFileNames = self.arrMedia.filter( { $0.type == 2 } ).map( {$0.attachment} )
                            
                            var isFileTaken : Bool = false
                            for doc in docFileNames {
                                if let tmpFile : UIImage = doc {
                                    if tmpFile.isEqualToImage(pickedImage) {
                                        isFileTaken = true
                                        break
                                    }
                                }
                            }
                            
                            if isFileTaken {
                                appDelegate.showToast(message: Messsages.msg_doc_already_uploaded, bottomValue: getSafeAreaValue())
                                pickedImageAsset = nil
                                picker.dismiss(animated: true, completion: nil)
                            }else{
                                setUpImageData(info: info, pickedImageAsset: pickedImageAsset, isFromCamera: false)
                            }
                        }
                    }
                }
            }
        }else {
            //If image picked from camera
            appDelegate.showLoader()
            var pickedImageAsset : PHAsset!
            pickedImageAsset = nil
            setUpImageData(info: info, pickedImageAsset: pickedImageAsset, isFromCamera: true)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Set Up Image Data
    /**
     Sets up the image data for the feedback view controller.
     */     
    func setUpImageData(info : [UIImagePickerController.InfoKey : Any], pickedImageAsset : PHAsset?, isFromCamera : Bool){
        var fileType : String = ""

        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
        {
            print("image url is \(url)")
            fileType  = url.lastPathComponent.lowercased()
            switch (url.pathExtension).lowercased()
            {
            case "jpg","jpeg":
                self.mimeType = "image/jpeg"
                break
            case "png":
                self.mimeType = "image/png"
                break
            default:
                //  print("No file")
                appDelegate.showToast(message: Messsages.msg_image_type, bottomValue: getSafeAreaValue())
                return
            }
        }
        
        if let pickedImage = info[.originalImage] as? UIImage
        {
            documentUpload = 1
            if fileType != ""
            {
                self.strDocumentName = fileType
            }
            else
            {
                self.mimeType = "image/png"
                let currentTimeStamp = String(Int(NSDate().timeIntervalSince1970))
                self.strDocumentName = "csf-\(currentTimeStamp).png"
            }
            DispatchQueue.global(qos: .background).async {
                
                var finalImage : UIImage = pickedImage
                if isFromCamera {
                    finalImage = pickedImage.resized(withPercentage: 0.1)!
                }

                if self.mimeType == "image/jpeg"
                {
                    self.uploadData = finalImage.jpegData(compressionQuality: 0.3)!
                }
                else if self.mimeType == "image/png"
                {
                    self.uploadData = finalImage.pngData()!
                }
                appDelegate.hideLoader()
                // Add Object For Media View
                let objMedia : MediaModel = MediaModel()
                objMedia.id = self.intSelectedIndex
                objMedia.type = 2
                
                objMedia.docFileNames = self.strDocumentName
                objMedia.attachment = pickedImage
                objMedia.attachmentData = self.uploadData
                objMedia.mimeType = self.mimeType
                objMedia.phAsset = pickedImageAsset
                objMedia.mediaType = 1
                objMedia.isFrom = 0
                self.arrMedia[self.intSelectedIndex] = objMedia
                
                // Add Object For Add Media View
                self.addDataForAddMediaView()
                print("Now total items are \(self.arrMedia.count)")
            }
        }
    }
    // MARK: - Document picker delegate
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let url = urls.first else {
            return
        }
        if !self.supportedFileExtensions.contains((url.pathExtension).lowercased()) {
            appDelegate.showToast(message: self.strUploadInfo, bottomValue: getSafeAreaValue())
            return
        }
        
        print("\(self.arrMedia.filter( { $0.type == 2 } ).map( {$0.docFileNames} ))")
        let docFileNames = self.arrMedia.filter( { $0.type == 2 } ).compactMap( {$0.docFileNames} )
        if docFileNames.contains(url.lastPathComponent.lowercased()) {
            appDelegate.showToast(message: Messsages.msg_doc_already_uploaded, bottomValue: getSafeAreaValue())
            return
        }
        
        self.strDocumentName = url.lastPathComponent.lowercased()
        let pathExtension = url.pathExtension.lowercased()
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                self.mimeType = mimetype as String
            }
        }
        
        if self.mimeType == ""
        {
            switch (url.pathExtension).lowercased()
            {
            case "pdf":
                self.mimeType = "application/pdf"
                break
            case "jpg","jpeg":
                self.mimeType = "image/jpeg"
                break
            case "png":
                self.mimeType = "image/png"
                break
            default:
                print("No file")
            }
        }
        
        DispatchQueue.global(qos: .background).async {
            do{
                let documentData : Data = try Data(contentsOf:url.asURL())
                
                DispatchQueue.main.async {
                    let strSize = documentData.getSizeInFromData(data: documentData, type: .megabyte)
                    if strSize.count > 0 {
                        let documentSizeInMb : Float = Float(strSize)!
                        print("Document size is \(documentSizeInMb)")
                        print("mediaSizeLimit is \(self.mediaSizeLimit)")
                        if documentSizeInMb > Float(self.mediaSizeLimit) {
                            let msg : String = "Selected attachment should be less than \(self.mediaSizeLimit) mb."
                            appDelegate.showToast(message: msg, bottomValue: getSafeAreaValue())
                            return
                        }
                    }
                    
                    self.uploadData = documentData
                    
                    // Add Object For Media View
                    self.documentUpload = 1
                    let objMedia : MediaModel = MediaModel()
                    objMedia.id = self.intSelectedIndex
                    objMedia.type = 2
                    
                    objMedia.docFileNames = self.strDocumentName
                    objMedia.attachmentData = self.uploadData
                    objMedia.mimeType = self.mimeType
                    objMedia.isFrom = 0
                    objMedia.localDocURL = url.absoluteString
                    if self.mimeType == "image/jpeg" || self.mimeType == "image/png"{
                        //Image
                        objMedia.attachment = UIImage(data: self.uploadData)
                        objMedia.mediaType = 1
                    }else {
                        //Doc
                        objMedia.attachment = nil
                        objMedia.mediaType = 2
                    }
                    
                    self.arrMedia[self.intSelectedIndex] = objMedia
                    
                    // Add Object For Add Media View
                    self.addDataForAddMediaView()
                    print("Now total items are \(self.arrMedia.count)")
                }
            }
            catch
            {
                print("Unable to load data: \(error)")
                
            }
        }
    }
    
    // MARK: - Add Data For Add Media View
    func addDataForAddMediaView(){
        // Add Object For Add Media View
        
        //This will limit to add media
        if self.arrMedia.count <= self.mediaLimit - 1 {
            let objNewMedia : MediaModel = MediaModel()
            objNewMedia.id = self.intSelectedIndex + 1
            objNewMedia.type = 1
            self.arrMedia.append(objNewMedia)
        }
        self.reloadFeedbackMedia()
    }
    
    // MARK: - Reload Feedback Media
    func reloadFeedbackMedia(){
        DispatchQueue.main.async {
            self.clvFeedbackMedia.reloadData()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.cnstClvFeedbackMediaHeight.constant = self.clvFeedbackMedia.contentSize.height
            print("clv height is \(self.cnstClvFeedbackMediaHeight.constant)")
        }
    }
}

extension FeedbackVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // MARK: - CollectionView Delegate and Datasource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell : AddPreAlertDocCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPreAlertDocCell", for: indexPath) as? AddPreAlertDocCell {
            cell.btnAddMedia.tag = indexPath.row
            cell.btnAddMedia.addTarget(self, action: #selector(btnAddMediaClicked(sender:)), for: .touchUpInside)

            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteMediaClicked(sender:)), for: .touchUpInside)
            
            let objMedia : MediaModel = self.arrMedia[indexPath.row]
            if objMedia.type == 1 {
                //Add
                cell.viewAddMedia.isHidden = false
                cell.viewMedia.isHidden = true
            }else {
                //Media
                cell.viewAddMedia.isHidden = true
                cell.viewMedia.isHidden = false
                
                if objMedia.mediaType == 1 {
                    //Image
                    if objMedia.isFrom == 0 {
                        //Local
                        cell.activityIndiCatorMedia.isHidden = true

                        if let img : UIImage = objMedia.attachment {
                            cell.imgThumb.image = img
                        }
                    }else if objMedia.isFrom == 1 {
                        //Web
                        if let imgUrl : String = objMedia.attachmentURL {
                            cell.activityIndiCatorMedia.isHidden = false
                            cell.activityIndiCatorMedia.startAnimating()
                            cell.imgThumb.sd_setImage(with: URL(string:imgUrl), completed: { (image, error, type, url) in
                                if image != nil {
                                    cell.imgThumb.image = image
                                    cell.activityIndiCatorMedia.stopAnimating()
                                    cell.activityIndiCatorMedia.isHidden = true
                                }else {
                                    //Image Not found
                                }
                            })
                        }
                    }
                    cell.imgThumb.isHidden = false
                    cell.imgDocThumb.isHidden = true
                }else if objMedia.mediaType == 2 {
                    //Doc
                    cell.imgThumb.isHidden = true
                    cell.imgDocThumb.isHidden = false
                }
                cell.imgThumb.contentMode = .scaleAspectFill
                cell.imgDocThumb.contentMode = .scaleAspectFit
            }
            return cell

        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objMedia : MediaModel = self.arrMedia[indexPath.row]
        if let fileUrl : String = objMedia.attachmentURL {
            print("Medai url is \(fileUrl)")
        }
        
        if let preAlertViewerVC = appDelegate.getViewController("PreAlertViewerVC", onStoryboard: "PreAlert") as? PreAlertViewerVC {
            preAlertViewerVC.arrMedia = self.arrMedia.filter( { $0.type == 2} )
            preAlertViewerVC.intSelectedIndex = indexPath.row
            preAlertViewerVC.currentPage = indexPath.row
            self.navigationController?.pushViewController(preAlertViewerVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width : CGFloat = (clvFeedbackMedia.frame.size.width / 3) - 10
        clvHeight = width
        print("cell width is \(width)")
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    }
    
    @objc func btnAddMediaClicked(sender : UIButton)
    {
        print("Add media cllicked at \(sender.tag)")
        
        self.intSelectedIndex = sender.tag
        self.chooseFile()
    }
    
    @objc func btnDeleteMediaClicked(sender : UIButton)
    {
        print("Delete media cllicked at \(sender.tag)")
        
        self.popupAlert(title: "", message: Messsages.msg_delete_document, actionTitles: ["Confirm","Cancel"], actions:[{action1 in
            
            let objMedia : MediaModel = self.arrMedia[sender.tag]
            if let mediaUrl : String = objMedia.attachmentURL {
                if !mediaUrl.isEmpty {
                    let url : URL = URL(string: mediaUrl)!
                    let deletedFileName : String = url.lastPathComponent
                    self.arrDeletedDocuments.append(deletedFileName)
                }
            }
            
            self.arrMedia.remove(at: sender.tag)
            self.reloadFeedbackMedia()
            
            //If all media limit comes and user delete the last media then we will show add media option via this code.
            if self.arrMedia.count > 0 {
                if let objMedia : MediaModel = self.arrMedia.last {
                    if objMedia.type == 2 {
                        self.addDataForAddMediaView()
                    }
                }
            }
        },{ action2 in
            self.dismiss(animated: true, completion: nil)
        }])
    }
}
