//
//  Addchilduser.swift
//  CSFCustomer
//
//  Created by Tops on 13/01/21.
//

import UIKit
import DropDown
class AddSecondaryUserVC: UIViewController
{
    // MARK: - IBOutlets
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblBOXID : UILabel!
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var imgContainer : UIView!
    @IBOutlet weak var txtFirstname : UITextField!
    @IBOutlet weak var txtMiddlename : UITextField!
    @IBOutlet weak var txtLastname : UITextField!
    @IBOutlet weak var txtCompanyName : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    @IBOutlet weak var txtMobile : UITextField!
    @IBOutlet weak var txtNationalID : UITextField!
    @IBOutlet weak var txtDOB : UITextField!
    @IBOutlet weak var txtDriverPermitNo : UITextField!
    @IBOutlet weak var DOBView : UIView!
    @IBOutlet weak var NationalIDView : UIView!
    @IBOutlet weak var lblPermitNoTitle : UILabel!
    @IBOutlet weak var imgMale : UIImageView!
    @IBOutlet weak var imgFemale : UIImageView!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnSubmit : UIButton!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnRemove : UIButton!
    @IBOutlet weak var topBarView : UIView!
    
    // MARK: - Global Variable
    var isImageRemoved : Int = 0
    lazy var targetFormatter : DateFormatter =
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    lazy  var sourceFormatter : DateFormatter =
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter
    }()
    var arrTextFields : [UITextField] = [UITextField]()
    var arrPlaceHolder = [String]()
    var arrNationalIDs : [NationalIDList] = [NationalIDList]()
    let datePicker = UIDatePicker()
    let dropDown = DropDown()
    var selectedNationalID : Int  = -1
    var mimeType : String  = ""
    var uploadData = Data()
    var objChild  = ChildUserList()
    var objUserInfo = UserInfo()
    var strGender : String = ""
    var docFileName : String = ""
    var type : String = ""
    var childUserID : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2
        self.imgUser.clipsToBounds = true
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
       arrTextFields = [txtFirstname,txtMiddlename,txtLastname,txtCompanyName,txtEmail,txtPhone,txtMobile,txtDOB,txtNationalID,txtDriverPermitNo]
       arrPlaceHolder = ["First Name","Middle Name","Last Name","Company Name","Email","Phone","Mobile","Date of Birth","National ID Type","Permit No"]
        for (index,item) in arrTextFields.enumerated()
        {
                item.layer.cornerRadius = 2
                item.layer.masksToBounds = true
                item.layer.borderWidth = 1
                item.layer.borderColor = Colors.theme_lightgray_color.cgColor
                item.placeholder = arrPlaceHolder[index]
                item.textColor = Colors.theme_black_color
                item.paddingView(xvalue: 10)
        }
        txtDOB.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed),selectorChange:#selector(dateValueChanged),formatType:DateFormatType.Day.dispValue(),from:"AddUser")
       
        configureViews()
        if type == "Edit"
        {
            lblTitle.text = "Edit Secondary User"
            txtEmail.isUserInteractionEnabled = false
            showData()
        }
        else if type == "Add"
        {
            lblTitle.text = "Add Secondary User"
            txtEmail.isUserInteractionEnabled = true
            btnRemove.isHidden = true
        }
        objUserInfo = getUserInfo()
        lblBOXID.text = "BOX ID : \(objUserInfo.boxId ?? "")"
    }
    func showData()
    {
        childUserID = objChild.id ?? ""
        if let imgUserURL = objChild.profileImage, imgUserURL != ""
        {
            imgUser.setImage(imgUserURL,placeHolder: UIImage(named: "default-image"))
            btnEdit.isHidden = true
            btnRemove.isHidden = false
        }
        else
        {
            imgUser.image = UIImage(named: "default-image")
            btnEdit.isHidden = false
            btnRemove.isHidden = true
        }
        txtFirstname.text = objChild.firstName ?? ""
        txtMiddlename.text = objChild.middleName ?? ""
        txtLastname.text = objChild.lastName ?? ""
        txtCompanyName.text = objChild.companyName ?? ""
        txtEmail.text = objChild.emailAddress ?? ""
        txtPhone.text = objChild.phoneNo ?? ""
        txtMobile.text = objChild.mobileNo ?? ""
        txtNationalID.text = objChild.nationalIdTypeText ?? ""
        txtDriverPermitNo.text = objChild.nationalIdNumber ?? ""
        lblPermitNoTitle.text = "\(String(describing: txtNationalID.text!)) No"
        strGender = objChild.gender ?? ""
       if let strNationalIDType = objChild.nationalIdType
       {
            selectedNationalID = Int(strNationalIDType)!
       }
        if let gender = objChild.genderText, gender == "MALE"
        {
            imgMale.image = UIImage(named: "radio_btn")
            imgFemale.image = UIImage(named: "unselect")
        }
        else
        {
            imgFemale.image = UIImage(named: "radio_btn")
            imgMale.image = UIImage(named: "unselect")
        }
        if let dob = objChild.dateOfBirth
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dt = formatter.date(from: dob)
            if dt != nil
            {
                formatter.dateFormat = "dd-MMM-yyyy"
                txtDOB.text = formatter.string(from: dt!)
            }
        }
        
    }
    func configureViews()
    {
        topBarView.topBarBGColor()
        self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2
        self.imgUser.clipsToBounds = true
        
        txtDOB.layer.borderColor = Colors.clearColor.cgColor
        txtDOB.layer.borderWidth = 0
        
        txtNationalID.layer.borderColor = Colors.clearColor.cgColor
        txtNationalID.layer.borderWidth = 0
        
        DOBView.layer.cornerRadius = 2
        DOBView.layer.masksToBounds = true
        DOBView.layer.borderWidth = 1
        DOBView.layer.borderColor = Colors.theme_lightgray_color.cgColor
        
        NationalIDView.layer.cornerRadius = 2
        NationalIDView.layer.masksToBounds = true
        NationalIDView.layer.borderWidth = 1
        NationalIDView.layer.borderColor = Colors.theme_lightgray_color.cgColor
        
        btnCancel.layer.cornerRadius = 2
        btnCancel.layer.masksToBounds = true
        btnCancel.layer.borderWidth = 1
        btnCancel.layer.borderColor = Colors.theme_lightgray_color.cgColor
        
        btnSubmit.layer.cornerRadius = 2
        btnSubmit.layer.masksToBounds = true
        
        txtFirstname.delegate = self
        txtLastname.delegate = self
        txtMiddlename.delegate = self
        txtDriverPermitNo.delegate = self
        txtPhone.delegate = self
        txtMobile.delegate = self
        txtEmail.delegate = self
        setNationalIDDropDown()
    }
    func setNationalIDDropDown()
    {
        dropDown.width = UIScreen.main.bounds.size.width - 40
        dropDown.shadowRadius = 0

        dropDown.direction = .any
        dropDown.anchorView = self.txtNationalID // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x:0 , y:(txtNationalID.bounds.height))
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)

        dropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDown.separatorColor =  UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        dropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDown.textColor = UIColor(named:"theme_text_color")!
        dropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        dropDown.dataSource = arrNationalIDs.map({$0.name})
        dropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                let objNationalID = self?.arrNationalIDs[index]
                self?.selectedNationalID  = objNationalID?.id ?? 0
                self?.lblPermitNoTitle.text = "\(item) No"
                self?.txtNationalID.text = item
                self?.txtDriverPermitNo.text = ""
                self?.txtDriverPermitNo.placeholder = "Enter \(item) no"
                self?.dropDown.hide()
            }
        }
    }
    
    @objc func dateValueChanged(_ datePicker: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        txtDOB.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    @objc func doneButtonPressed()
    {
        if let  datePicker = txtDOB.inputView as? UIDatePicker {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            txtDOB.text = formatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }
    @IBAction func btnGenderClicked(_ sender:UIButton)
    {
        if sender.tag == 101
        {
            imgMale.image = UIImage(named: "radio_btn")
            imgFemale.image = UIImage(named: "unselect")
            strGender = gender.Male.dispValue()
        }
        else
        {
            imgFemale.image = UIImage(named: "radio_btn")
            imgMale.image = UIImage(named: "unselect")
            strGender = gender.Female.dispValue()
        }
    }
    @IBAction func btnNationalIDClicked()
    {
        self.dropDown.show()
    }
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnPhotoEditClicked()
    {
        let attributedString = NSAttributedString(string: "Please select option", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15), //your font here
            NSAttributedString.Key.foregroundColor :Colors.theme_gray_color
        ])
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(attributedString, forKey: "attributedTitle")
      
        let takePhotoAction = UIAlertAction(title: "Take a Photo" , style: .default) { (_ action) in
            self.openCamera()
        }
        let photoAction = UIAlertAction(title: "Photo Gallery" , style: .default) { (_ action) in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel" , style: .cancel) { (_ action) in
            self.dismiss(animated: true, completion: nil)
        }
        takePhotoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        photoAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(takePhotoAction)
        alert.addAction(photoAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnRemoveClicked()
    {
        isImageRemoved = 1
        imgUser.image = UIImage(named: "default-image")
        uploadData = Data()
        self.mimeType = "image/png"
        btnEdit.isHidden = false
        btnRemove.isHidden = true
    }
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            PermissionManager.shared.requestCameraPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.camera
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            PermissionManager.shared.requestPhotoPermission(vc: self) { status, isGranted in
                if isGranted {
                    DispatchQueue.main.async {
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.mediaTypes = ["public.image"]
                        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let alert  = UIAlertController(title: "", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func btnCancelClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitClicked()
    {
        if isValidData()
        {
            callAPIToAddUser()
        }
    }
    func callAPIToAddUser()
    {
        
        let fieldName = "profile_image"
        var date = ""
        let dob = sourceFormatter.date(from: txtDOB.text!)
        if dob != nil
        {
            date = targetFormatter.string(from: dob!)
        }
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["child_user_id"] = childUserID
        params["first_name"] = txtFirstname.text ?? ""
        params["middle_name"] = txtMiddlename.text ?? ""
        params["last_name"] = txtLastname.text ?? ""
        params["company_name"] = txtCompanyName.text ?? ""
        params["gender"] = strGender
        params["date_of_birth"] =  date
        params["mobile_no"] = txtMobile.text ?? ""
        params["phone_no"] = txtPhone.text ?? ""
        params["national_id_type"] = selectedNationalID
        params["national_id_number"] = txtDriverPermitNo.text ?? ""
        params["image_removed"] = isImageRemoved
        params["email_address"] = txtEmail.text ?? ""
        
        URLManager.shared.URLCallMultipartData(method: .post, parameters: params, header: true, url: APICall.SaveUser, showLoader: true, WithName: fieldName, docFileName: self.docFileName,uploadData: uploadData, mimeType: self.mimeType) { (resultDict, status, message) in
           if status == true
           {
                self.showAlertForSaveUser(title: "", msg: message)
           }
           else
           {
              appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
           }
       }
    }
    func showAlertForSaveUser(title:String,msg:String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            NotificationCenter.default.post(name: Notification.Name("ChildUserInfoUpdated"), object: nil)
            self.navigationController?.popViewController(animated: true)
        }
        okAction.setValue(UIColor(named: "theme_black_color"), forKey: "titleTextColor")
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtFirstname.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_firstname, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtLastname.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_lastname, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtCompanyName.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_companyname, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtEmail.text!) && type == "Add"
        {
            appDelegate.showToast(message: Messsages.msg_enter_email, bottomValue: getSafeAreaValue())
            return false
        }
        else if !isValidEmail(testStr: txtEmail.text!) && type == "Add"
        {
            appDelegate.showToast(message: Messsages.msg_invalid_email, bottomValue: getSafeAreaValue())
            return false
        }
        else if !isCheckNull(strText: txtPhone.text!) && txtPhone.text!.count < 10
        {
               appDelegate.showToast(message: Messsages.msg_phone_length, bottomValue: getSafeAreaValue())
               return false
            
        }
//        else if !isValidEmail(testStr: txtEmail.text!)
//        {
//            appDelegate.showToast(message: Messsages.msg_invalid_email, bottomValue: getSafeAreaValue())
//            return false
//        }
        else if isCheckNull(strText: txtMobile.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_mobile, bottomValue: getSafeAreaValue())
            return false
        }
        else if self.txtMobile.text!.count < 10
        {
            appDelegate.showToast(message: Messsages.msg_mobile_length, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtDOB.text!)
        {
            appDelegate.showToast(message: Messsages.msg_select_dob, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtNationalID.text!)
        {
            appDelegate.showToast(message: Messsages.msg_select_nationalId, bottomValue: getSafeAreaValue())
            return false
        }
        else if isCheckNull(strText: txtDriverPermitNo.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_permitno + " " + lblPermitNoTitle.text!, bottomValue: getSafeAreaValue())
            return false
        }
        else if strGender == ""
        {
            appDelegate.showToast(message: Messsages.msg_select_gender, bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
}
extension AddSecondaryUserVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
//        let currentString: NSString = (textField.text ?? "") as NSString
//        let newString: NSString =
//            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == txtDriverPermitNo
        {
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_")
            if newLength <= 50
            {
                if string.rangeOfCharacter(from: set) != nil
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        if textField == txtFirstname || textField == txtMiddlename || textField == txtLastname ||  textField == txtDriverPermitNo
        {
            return newLength <= 50
        }
        else if textField == txtPhone || textField == txtMobile
        {
            return newLength <= 10
        }
        return true
    }
}
extension AddSecondaryUserVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    // MARK: - ImagePicker delegate
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            {
                switch url.pathExtension
                {
                case "jpg","jpeg":
                    self.mimeType = "image/jpeg"
                    break
                case "png":
                    self.mimeType = "image/png"
                    break
                default:
                    showAlert(title: "", msg: Messsages.msg_image_type, vc: self)
                    return
                }
             }
            if let pickedImage = info[.editedImage] as? UIImage
            {
               if self.mimeType == ""
               {
                    self.mimeType = "image/png"
               }
               isImageRemoved = 0
               self.docFileName = "Profile-img.png"
               imgUser.image = pickedImage
               let thumb1 = pickedImage.resized(withPercentage: 0.1)
               btnEdit.isHidden = true
               btnRemove.isHidden = false
               DispatchQueue.global(qos: .background).async {
                    if self.mimeType == "image/jpeg"
                    {
                        self.uploadData = pickedImage.jpegData(compressionQuality: 0.3)!
                    }
                    else if self.mimeType == "image/png"
                    {
                        self.uploadData = (thumb1?.pngData())!
                    }
                }
            }
            picker.dismiss(animated: true, completion: nil)
    }
}
