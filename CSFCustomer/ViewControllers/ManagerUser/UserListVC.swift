//
//  UserListVC.swift
//  CSFCustomer
//
//  Created by Tops on 12/01/21.
//

import UIKit
import SwiftyJSON
class UserListVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tblUserList : UITableView!
    @IBOutlet weak var tblBottomConstant : NSLayoutConstraint!
    @IBOutlet weak var btnClearFilter : UIButton!
    @IBOutlet weak var lblNoRecordFound : UILabel!
    @IBOutlet weak var topBarView : UIView!
    
    // MARK: - Global Variable
    var arrSelectedSections : [Int] = [Int]()
    var arrSections : [Int] = [Int]()
    var longPressGesture : String = "Stopped"
    var userID : String = ""
    var pageNo : Int = 1
    var isLoadingList : Bool = false
    var totalPages : Int = 0
    var objChildUser = ChildUser()
    var arrChildUserList :[ChildUserList] = [ChildUserList]()
    var strSearch : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(getFilterRecords(_:)), name: NSNotification.Name(rawValue: "SearchUser"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatesChildUserList(_:)), name: Notification.Name("ChildUserInfoUpdated"), object: nil)
        topBarView.topBarBGColor()
        getUserList()
        tblUserList.dataSource = self
        tblUserList.delegate = self
        tblUserList.estimatedRowHeight = 114
        tblUserList.rowHeight = UITableView.automaticDimension
        tblUserList.estimatedSectionHeaderHeight = 75
        tblUserList.sectionHeaderHeight = UITableView.automaticDimension
        tblUserList.tableFooterView? = UIView()
        tblBottomConstant.constant = 0
        btnClearFilter.isHidden = true
        lblNoRecordFound.isHidden = true
    }
    func getUserList()
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["page_id"] = pageNo
        params["search_text"] = strSearch
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.MyUsers, showLoader:true) { [self] (resultDict, status, message) in
            if status == true
            {
                self.isLoadingList = false
                if pageNo == 1
                {
                    self.arrChildUserList.removeAll()
                }
                self.objChildUser = ChildUser(fromJson: JSON(resultDict))
                self.totalPages = self.objChildUser.totalPage ?? 1
                if self.arrChildUserList.count > 0
                {
                    self.arrChildUserList.append(contentsOf: objChildUser.list)
                    for _ in 0..<objChildUser.list.count
                    {
                        self.arrSections.append(0)
                    }
                }
                else
                {
                    self.arrChildUserList = objChildUser.list
                    self.arrSections.removeAll()
                    for _ in 0..<self.arrChildUserList.count
                    {
                        self.arrSections.append(0)
                        
                    }
                }
                if self.arrChildUserList.count > 0
                {
                    self.tblUserList.isHidden = false
                    self.lblNoRecordFound.isHidden = true
                }
                else
                {
                    self.tblUserList.isHidden = true
                    self.lblNoRecordFound.isHidden = false
                }
                
                DispatchQueue.main.async {
                    self.tblUserList.reloadData()
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    @objc func getFilterRecords(_ notification:NSNotification)
    {
        tblBottomConstant.constant -= 45
        self.view.layoutIfNeeded()
        pageNo = 1
        strSearch = notification.userInfo?["search_text"] as? String ?? ""
        btnClearFilter.isHidden = false
        getUserList()
    }
    @objc func getLatesChildUserList(_ notification: NSNotification)
    {
        pageNo = 1
        getUserList()
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnClearFilterClicked()
    {
        strSearch =  ""
        pageNo = 1
        self.btnClearFilter.isHidden = true
        tblBottomConstant.constant = 0
        tblUserList.layoutIfNeeded()
        getUserList()
    }
    @IBAction func btnSearchClicked()
    {
        if let vc = appDelegate.getViewController("SearchUserVC", onStoryboard: "Profile") as?  SearchUserVC {
            vc.strSearch = self.strSearch
            tblBottomConstant.constant = 0
            tblUserList.layoutIfNeeded()
            UIView.setAnimationsEnabled(true)
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc,animated: true, completion: nil)
        }
    }
    @IBAction func btnAddClicked()
    {
        if let vc = appDelegate.getViewController("AddSecondaryUserVC", onStoryboard: "Profile") as? AddSecondaryUserVC {
            vc.arrNationalIDs = objChildUser.nationalIdList ?? []
            vc.type = "Add"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
extension UserListVC : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrChildUserList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrSections.count > 0
        {
            if arrSections[indexPath.section] == 1
            {
                return UITableView.automaticDimension
            }
        }
        else
        {
            return 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UserHeaderCell") as? UserHeaderCell {
            cell.btnSection.tag = section
            cell.btnSection.addTarget(self, action: #selector(btnSectionClicked(sender:)), for: .touchUpInside )
            if arrChildUserList.count > 0
            {
                let objChildUser = arrChildUserList[section]
                cell.lblUserID.text = objChildUser.boxId ?? ""
                cell.lblName.text = objChildUser.name ?? ""
                if arrSections.count > 0
                {
                    if arrSections[section] == 1
                    {
                        cell.imgUpDown.image = UIImage(named: "up")
                        cell.longSeperator.isHidden = true
                        cell.shortSeperator.isHidden = false
                    }
                    else
                    {
                        cell.imgUpDown.image = UIImage(named: "down")
                        cell.longSeperator.isHidden = false
                        cell.shortSeperator.isHidden = true
                    }
                }
                else
                {
                    cell.imgUpDown.image = UIImage(named: "down")
                    cell.longSeperator.isHidden = false
                    cell.shortSeperator.isHidden = true
                }
            }
            return cell.contentView
        }
        return UITableViewCell().contentView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UserDataCell", for: indexPath) as? UserDataCell {
            cell.btnEdit.tag = indexPath.section
            cell.btnEdit.addTarget(self, action: #selector(btnEditClicked(sender:)), for: .touchUpInside)
            
            cell.btnDelete.tag = indexPath.section
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteClicked(sender:)), for: .touchUpInside)
            
            cell.btnChangePwd.tag = indexPath.section
            cell.btnChangePwd.addTarget(self, action: #selector(btnChangePwdClicked(sender:)), for: .touchUpInside)
            if arrChildUserList.count > 0
            {
                let objChildUser = arrChildUserList[indexPath.section]
                cell.lblEmail.text = objChildUser.emailAddress ?? ""
                cell.lblMobile.text = objChildUser.mobileNo ?? ""
            }
            return cell
        }
        return UITableViewCell()
    }
    //Pagination
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isLoadingList = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (Int(tblUserList.contentOffset.y + tblUserList.frame.size.height) >= Int(tblUserList.contentSize.height))
        {
            if !isLoadingList{
                isLoadingList = true
                if self.totalPages == pageNo
                {
                    return
                }
                pageNo += 1
                getUserList()
            }
        }
    }
    @objc func btnSectionClicked(sender:UIButton)
    {
        let tag = sender.tag
        if arrSections.count > 0
        {
            if arrSections[tag] == 0
            {
                arrSections[tag] = 1
            }
            else
            {
                arrSections[tag] = 0
            }
        }
        DispatchQueue.main.async {
            UIView.setAnimationsEnabled(false)
            let section = NSIndexSet(index: tag)
            self.tblUserList.reloadSections(section as IndexSet, with: .none)
        }
    }
    @objc func btnEditClicked(sender:UIButton)
    {
        if arrChildUserList.count > 0
        {
            let objChild = arrChildUserList[sender.tag]
            if let vc = appDelegate.getViewController("AddSecondaryUserVC", onStoryboard: "Profile") as? AddSecondaryUserVC {
                vc.type = "Edit"
                vc.arrNationalIDs = objChildUser.nationalIdList ?? []
                vc.objChild = objChild
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    @objc func btnDeleteClicked(sender:UIButton)
    {
        if arrChildUserList.count > 0
        {
            let objChild = arrChildUserList[sender.tag]
            if let popupVC = appDelegate.getViewController("DeletePopupVC", onStoryboard: "PreAlert") as? DeletePopupVC {
                popupVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
                popupVC.userID = userID
                popupVC.isFromVC = "DeleteUser"
                popupVC.childUserID = objChild.id ?? ""
                popupVC.modalPresentationStyle = .overFullScreen
                self.present(popupVC,animated: true, completion: nil)
            }
        }
    }
    @objc func btnChangePwdClicked(sender:UIButton)
    {
        if arrChildUserList.count > 0
        {
            let objChild = arrChildUserList[sender.tag]
            if let vc = appDelegate.getViewController("UserChangePasswordVC", onStoryboard: "Profile") as? UserChangePasswordVC {
                vc.userID = userID
                vc.childUserID = objChild.id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
class UserHeaderCell : UITableViewCell
{
    @IBOutlet weak var imgChecked : UIImageView!
    @IBOutlet weak var imgUpDown : UIImageView!
    @IBOutlet weak var lblUserID : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var btnSection : UIButton!
    @IBOutlet weak var longSeperator : UILabel!
    @IBOutlet weak var shortSeperator : UILabel!
}
class UserDataCell :UITableViewCell
{
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblMobile : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnChangePwd : UIButton!
    @IBOutlet weak var editContainerView : UIView!
    @IBOutlet weak var deleteContainerView : UIView!
    @IBOutlet weak var changePwdContainerView : UIView!
    override func awakeFromNib() {
        editContainerView.layer.cornerRadius = editContainerView.frame.size.height / 2
        deleteContainerView.layer.cornerRadius = deleteContainerView.frame.size.height / 2
        changePwdContainerView.layer.cornerRadius = changePwdContainerView.frame.size.height / 2
    }
}
