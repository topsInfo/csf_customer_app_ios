//
//  SearchUserVC.swift
//  CSFCustomer
//
//  Created by Tops on 13/01/21.
//

import UIKit

class SearchUserVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var btnSearch : UIButton!
    
    // MARK: - Global Variable
    var strSearch :String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        showCard()
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black") //UIColor.black.withAlphaComponent(0.5)
        configureTextField(textField: txtSearch, placeHolder: "Search", font: fontname.openSansRegular, fontSize: 16, textColor: Colors.theme_black_color, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 1, bgColor: Colors.theme_white_color)
        txtSearch.paddingView(xvalue: 10)
        txtSearch.text = strSearch
        btnSearch.layer.cornerRadius = 2.0
        btnSearch.layer.masksToBounds = true
    }
    func showCard()
    {
        self.view.layoutIfNeeded()
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - 270
            topConst.constant = topHeight
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSearchClicked()
    {
        let filterDict :[String: Any] = [
           "search_text" : txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
        ]
        NotificationCenter.default.post(name: Notification.Name("SearchUser"), object: nil,userInfo: filterDict)
        self.dismiss(animated: true, completion: nil)
    }
}
