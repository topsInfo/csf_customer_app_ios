//
//  PreAlertCell.swift
//  CSFCustomer
//
//  Created by Tops on 26/11/20.
//

import UIKit

class PreAlertCell: UITableViewCell,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var lblPreAlertCounter : UILabel!
    @IBOutlet weak var tblPreAlert : UITableView!
    @IBOutlet weak var btnAddPreAlert : UIButton!
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var tblPreAlertHeightConstant : NSLayoutConstraint!
    @IBOutlet weak var lblNoFound : UILabel!
    
    // MARK: - Global Variable
    var arrPreAlertLatest : [String] = [String]()
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = Colors.theme_lightgray_color.cgColor
        tblPreAlert.estimatedRowHeight = 30
        tblPreAlert.rowHeight = UITableView.automaticDimension
        tblPreAlert.dataSource = self
        tblPreAlert.delegate = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    // MARK: - Custom methods
    func setData(arrPreAlertLatest:[String])
    {
        self.arrPreAlertLatest = arrPreAlertLatest
        DispatchQueue.main.async {
            self.tblPreAlert.reloadData()
        }
    }
    
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPreAlertLatest.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PreAlertDataCell", for: indexPath) as? PreAlertDataCell{
            cell.lblAlertDesc.text = arrPreAlertLatest[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
}
class PreAlertDataCell : UITableViewCell
{
    @IBOutlet weak var lblAlertDesc : UILabel!
}
