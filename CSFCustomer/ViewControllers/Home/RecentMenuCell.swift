//
//  RecentMenuCell.swift
//  CSFCustomer
//
//  Created by Tops on 25/11/20.
//

import UIKit
import SWRevealViewController
class RecentMenuCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    // MARK: - IBOutlets
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var lblNoRecentActivity : UILabel!
    
    // MARK: - Global Variable
    var arrTitle : [String] = [String]()
    var arrImages : [UIImage] = [UIImage]()
    var navvc : UINavigationController?
    var objUserInfo = UserInfo()
    var strCartCounter : Int  = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    // MARK: - Custom methods
    func setData()
    {
        objUserInfo = getUserInfo()
        arrImages.removeAll()
        for (_,value) in arrTitle.enumerated()
        {
            switch  value {
            case RecentMenuTitle.PreAlert.dispValue():
                arrImages.append(UIImage(named: "pre_alert")!)
                break
            case RecentMenuTitle.Invoice.dispValue():
                arrImages.append(UIImage(named: "invoice")!)
                break
            case RecentMenuTitle.ViewCart.dispValue():
                arrImages.append(UIImage(named: "2_cart")!)
                break
            case RecentMenuTitle.Refund.dispValue():
                arrImages.append(UIImage(named: "refund")!)
                break
            case RecentMenuTitle.Feedback.dispValue():
                arrImages.append(UIImage(named: "comment")!)
                break
            default:
                print("No Menu")
            }
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    // MARK: - Collectionview delegate methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTitle.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as? MenuCell{
            let lblCartCounter : UILabel?
            
            if arrTitle[indexPath.row] == RecentMenuTitle.ViewCart.dispValue()
            {
                if strCartCounter > 0
                {
                    let xpos = cell.imgMenu.frame.origin.x + cell.imgMenu.frame.width
                    let ypos = 20
                    let lblCartCounter = UILabel(frame: CGRect(x: Int(xpos-10), y: ypos, width: 19, height: 19))
                    lblCartCounter.backgroundColor = Colors.theme_orange_color
                    lblCartCounter.layer.cornerRadius = lblCartCounter.frame.size.height/2
                    lblCartCounter.layer.masksToBounds = true
                    lblCartCounter.textAlignment = .center
                    lblCartCounter.tag = 101
                    if strCartCounter <= 9
                    {
                        lblCartCounter.attributedText = NSAttributedString(string: String(strCartCounter), attributes: [NSAttributedString.Key.font : UIFont(name: fontname.openSansBold, size: 12)!,NSAttributedString.Key.foregroundColor : Colors.theme_white_color])
                    }
                    else
                    {
                        lblCartCounter.attributedText = NSAttributedString(string: "9+", attributes: [NSAttributedString.Key.font : UIFont(name: fontname.openSansBold, size: 12)!,NSAttributedString.Key.foregroundColor : Colors.theme_white_color])
                    }
                    cell.contentView.addSubview(lblCartCounter)
                }
                else
                {
                    for lbl in cell.contentView.subviews {
                        guard let lbl = lbl as? UILabel else {
                            continue
                        }
                        if lbl.tag == 101 {
                            lbl.removeFromSuperview()
                        }
                    }
                }
            }
            else
            {
                for lbl in cell.contentView.subviews {
                    guard let lbl = lbl as? UILabel else {
                        continue
                    }
                    if lbl.tag == 101 {
                        lbl.removeFromSuperview()
                    }
                }
            }
            cell.imgMenu.image = arrImages[indexPath.row]
            cell.lblMenuTitle.text = arrTitle[indexPath.row]
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 111, height: 128)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if arrTitle[indexPath.row] == RecentMenuTitle.PreAlert.dispValue()
        {
            if let vc = appDelegate.getViewController("PreAlertListVC", onStoryboard: "PreAlert") as? PreAlertListVC {
                if navvc != nil
                {
                    navvc?.pushViewController(vc, animated: true)
                }
            }
        }
        else if arrTitle[indexPath.row] == RecentMenuTitle.Invoice.dispValue()
        {
            if let vc = appDelegate.getViewController("SegmentPageVC", onStoryboard: "Invoice") as? SegmentPageVC {
                if navvc != nil
                {
                    navvc?.pushViewController(vc, animated: true)
                }
            }
        }
        else if arrTitle[indexPath.row] == RecentMenuTitle.ViewCart.dispValue()
        {
            if let vc = appDelegate.getViewController("ViewCartVC", onStoryboard: "Invoice") as? ViewCartVC {
                objUserInfo = getUserInfo()
                vc.userID = objUserInfo.id ?? ""
                vc.isFromVC = "Dashboard"
                if navvc != nil
                {
                    navvc?.pushViewController(vc, animated: true)
                }
            }
        }
        else if arrTitle[indexPath.row] == RecentMenuTitle.Refund.dispValue()
        {
            if let vc = appDelegate.getViewController("PaymentRefundVC", onStoryboard: "Invoice") as? PaymentRefundVC {
                if navvc != nil
                {
                    navvc?.pushViewController(vc, animated: true)
                }
            }
        }
        else if arrTitle[indexPath.row] == RecentMenuTitle.Feedback.dispValue()
        {
            if let vc = appDelegate.getViewController("FeedbackVC", onStoryboard: "Home") as? FeedbackVC {
                if navvc != nil
                {
                    navvc?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
class MenuCell : UICollectionViewCell
{
    @IBOutlet weak var imgMenu : UIImageView!
    @IBOutlet weak var lblMenuTitle : UILabel!
    @IBOutlet weak var imgContainerView : UIView!
    override func awakeFromNib() {
        imgContainerView.layer.cornerRadius = 5.0
        imgContainerView.layer.masksToBounds = true
    }
}
