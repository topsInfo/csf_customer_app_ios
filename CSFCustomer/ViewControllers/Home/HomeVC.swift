//
//  HomeVC.swift
//  CSFCustomer
//
//  Created by Tops on 25/11/20.
//

import UIKit
import DropDown
import SwiftyJSON
import LoginWithAmazon
import MarqueeLabel
import Firebase
import FirebaseMessaging

class HomeVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate//,SannerOutputDelegate
{
    // MARK: - IBOutlets  
    @IBOutlet weak var tblDashboard: UITableView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgContainer: UIView!
    @IBOutlet weak var lblUsername: TapAndCopyLabel!
    @IBOutlet weak var lblBoxID: TapAndCopyLabel!
    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var notifyView: UIView!
    @IBOutlet weak var lblNotifyMsg: UILabel!
    @IBOutlet weak var viewHowToUse: UIView!
    @IBOutlet weak var viewBreakingNews: UIView!
    @IBOutlet weak var lblMarqueeText: MarqueeLabel!
    @IBOutlet weak var cnstBreakingNewsViewHeight: NSLayoutConstraint!
    
    //Nand : New Feature Updates Code
    @IBOutlet weak var weekendPrefNotifyViewMain : UIView!
    @IBOutlet weak var weekendPrefNotifyView : UIView!
    @IBOutlet weak var tvWeekendPrefNotifyMsg : UITextView!
    @IBOutlet weak var cnstWeekendPrefNotifViewHeight: NSLayoutConstraint!

    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    var arrSections = ["Dashboard Menu Title"]
    var arrMoreActions = ["","","",""]//,
    var preAlertCellHeight : CGFloat = 0
    var invoiceCellHeight : CGFloat = 0
    var creditCardCellHeight : CGFloat = 0
    var selectedSectionIndex : Int  = 0
    var arrShippingMethod : [NSDictionary] = [NSDictionary]()
    var objDashboard = Dashboard()
    var arrSelectedSections : [Int] = [Int]()
    var isShowLoader : Bool = true
    var refreshControl = UIRefreshControl()
    var arrDashboardTitle : [String] = [String]()
    
    /**
     A lazy-initialized `DateFormatter` used for formatting dates in the format "yyyy-MM-dd".
     
     This formatter is used in the `HomeVC` view controller for converting dates to strings and vice versa.
     */
    lazy var sourceDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
        
    // A lazy-initialized `DateFormatter` used for formatting dates in the format "dd-MMM-yyyy".
    lazy var destinationFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter
    }()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureWeekendPrefData()
        self.viewBreakingNews.isHidden = true
        self.cnstBreakingNewsViewHeight.constant = 0
        self.lblMarqueeText.text = ""
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(getUpdatedUserInfo(_:)), name: Notification.Name("UserInfoUpdated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords(_:)), name: Notification.Name("NewPreAlertAdded"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords(_:)), name: Notification.Name("RecordDeleted"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords(_:)), name: Notification.Name("NotificationRead"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords1(_:)), name: Notification.Name("NotificationRead1"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords1(_:)), name: Notification.Name("CartUpdated"), object: nil) //new change
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords1(_:)), name: Notification.Name("ServiceGuideAgreed"), object: nil) //new change
        NotificationCenter.default.addObserver(self, selector: #selector(getLatestRecords1(_:)), name: Notification.Name("ServiceGuideBackClicked"), object: nil) //new change

        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(openEditProfile(gesture:)))
        imgUser.addGestureRecognizer(longGesture)
        imgUser.isUserInteractionEnabled = true
        configureControls()
    }
    /**
     This method is called when the view's layout is updated. It moves the `refreshControl` to its superview and calls the superclass's `viewDidLayoutSubviews()` method.
     */
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    
    /**
     Deinitializes the HomeVC instance.
     
     This method is called when the HomeVC instance is being deallocated from memory. It removes the HomeVC instance as an observer from the default notification center.
     */
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    /**
     This method is called when the view is about to appear on the screen.
     It updates the status bar appearance, sets the corner radius and masks for the user image and container image views.
     It also checks if there are any recent titles stored in UserDefaults and assigns them to the arrDashboardTitle array.
     If the cart is available and set to true in UserDefaults, it sets the isShowLoader flag to true and calls the getHomeDetails() method.
     
     - Parameter animated: A boolean value indicating whether the appearance update should be animated or not.
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()

        imgUser.layer.cornerRadius = imgUser.frame.size.height / 2
        imgUser.layer.masksToBounds = true
        imgContainer.layer.cornerRadius = imgContainer.frame.size.height / 2
        imgContainer.layer.masksToBounds = true
        
        if let arrRecentStrings = UserDefaults.standard.object(forKey: kRecentTitles) as? [String]
        {
            arrDashboardTitle = arrRecentStrings
        }
        if let isCartAvailable = UserDefaults.standard.value(forKey: kCartItemAvailable) as? Bool,isCartAvailable == true
        {
            isShowLoader = true
            getHomeDetails()
        }
    }
    
    /**
        Returns the preferred status bar style for the view controller.
        
        - Returns: A `UIStatusBarStyle` value indicating the preferred status bar style. In this case, it returns `.lightContent` to set the status bar style to light content.
    */
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        for _ in 0..<5
        {
            self.arrSelectedSections.append(0)
        }
        showUserInfoData()
        topBarView.topBarBGColor()
        isShowLoader = true
        getHomeDetails()
        
        viewHowToUse.layer.cornerRadius = viewHowToUse.frame.size.height/2
        viewHowToUse.layer.masksToBounds = true

        imgUser.layer.cornerRadius = imgUser.frame.size.height/2
        imgUser.layer.masksToBounds = true
        tblDashboard.dataSource = self
        tblDashboard.delegate = self
        tblDashboard.tableFooterView = UIView()
        tblDashboard.estimatedRowHeight = 100
        tblDashboard.rowHeight = UITableView.automaticDimension
        lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.size.height/2
        lblNotificationCount.layer.masksToBounds = true
        lblNotificationCount.isHidden = true
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshDashboard), for: .valueChanged)
        tblDashboard.addSubview(refreshControl)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(gesture:)))
        lblUsername.isUserInteractionEnabled = true
        lblUsername.addGestureRecognizer(tapGesture)
        lblBoxID.isUserInteractionEnabled = true
        lblBoxID.addGestureRecognizer(tapGesture)
        
        //notifyview hidden
        notifyView.isHidden = true
        notifyView.dropShadowForNotifyView()
        
        //WeekendPref NotifView
        weekendPrefNotifyViewMain.isHidden = true
        weekendPrefNotifyView.dropShadowForNewFeature()
    }
    
    // MARK: - Configure Weekend Pref Data
    //Nand : New Feature Updates Code
    /**
     Configures the weekend preference data.
     
     This method sets up the necessary configurations for the weekend preference data in the HomeVC view controller. It adjusts the height of the notification view, sets the text container insets and line fragment padding of the notify message text view, disables scrolling, sets the delegate of the notify message text view, and enables data detection for links.
     */
    func configureWeekendPrefData(){
        self.cnstWeekendPrefNotifViewHeight.constant = 0
        
        self.tvWeekendPrefNotifyMsg.textContainerInset = UIEdgeInsets.zero
        self.tvWeekendPrefNotifyMsg.textContainer.lineFragmentPadding = 0
        self.tvWeekendPrefNotifyMsg.isScrollEnabled = false
        
        self.tvWeekendPrefNotifyMsg.delegate = self
        
        tvWeekendPrefNotifyMsg.isEditable = false
        tvWeekendPrefNotifyMsg.isUserInteractionEnabled = true
        tvWeekendPrefNotifyMsg.dataDetectorTypes = .link
    }
    
    // MARK: - showUserInfoData
    /**
     Shows the user information data.
     
     This method retrieves the user information data from UserDefaults and displays it on the screen. It replaces the old code with new code that uses NSKeyedUnarchiver to unarchive the data and update the UI elements accordingly.
     
     - Note: This method assumes that the user information data is stored in UserDefaults with the key "kUserInfo".
     */
    func showUserInfoData()
    {
        if  let decoded = UserDefaults.standard.object(forKey: kUserInfo) as? Data
        {
            do {
                if let unarchivedObject = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserInfo.self, from: decoded) {
                    // Handle the unarchived object
                    objUserInfo = unarchivedObject
                    lblUsername.text = "Hi \(objUserInfo.firstName ?? "")"
                    lblBoxID.text = "BOX ID : \(String(describing: objUserInfo.boxId ?? ""))"
                    imgUser.setImage(objUserInfo.profileImagePath,placeHolder: UIImage(named: "default-image"))
                    imgUser.contentMode = .scaleAspectFill
                } else {
                    // Handle the case where unarchiving failed
                }
            } catch {
                // Handle the error
            }
        }
    }
    
    /**
     This method is called when the dashboard needs to be refreshed.
     It sets the `isShowLoader` flag to `false` and calls the `getHomeDetails()` method.
     */
    @objc func refreshDashboard()
    {
        isShowLoader = false
        getHomeDetails()
    }

    /**
        This method is called when the user's information is updated.
        It shows the updated user information data.
    */
    @objc func getUpdatedUserInfo(_ notification: NSNotification) {
        showUserInfoData()
    }
    
    /**
     This method is called when a notification is received to get the latest records.
     It sets the `isShowLoader` flag to `true` and calls the `getHomeDetails()` method.
     */
    @objc func getLatestRecords(_ notification: NSNotification) {
        isShowLoader = true
        getHomeDetails()
    }

    /**
     This method is called when a notification is received to get the latest records.
     It sets the `isShowLoader` flag to `false` and calls the `getHomeDetails()` method.
     */
    @objc func getLatestRecords1(_ notification: NSNotification) {
        isShowLoader = false
        getHomeDetails()
    }

    /**
     Handles the tap action when the user taps on the view.
     
     - Parameter gesture: The UITapGestureRecognizer instance that triggered the tap action.
     */
    @objc func tapAction(gesture: UITapGestureRecognizer) {
        if let vc = appDelegate.getViewController("EditProfileVC", onStoryboard: "Profile") as? EditProfileVC {
            objUserInfo = getUserInfo()
            vc.userID = objUserInfo.id ?? ""
            vc.isFromVC = "Home"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    /**
     Opens the Edit Profile view controller when a long press gesture is detected.
     
     - Parameter gesture: The long press gesture recognizer.
     */
    @objc func openEditProfile(gesture: UILongPressGestureRecognizer)
    {
        if (gesture.state == UIGestureRecognizer.State.ended) {
            
        } else if (gesture.state == UIGestureRecognizer.State.began)
        {
            if let editProfileVC = appDelegate.getViewController("EditProfileVC", onStoryboard: "Profile") as? EditProfileVC {
                
                if self.revealViewController()?.frontViewPosition.rawValue == 4 {
                    //If Side menu is open then it will close the side menu
                    self.revealViewController().revealToggle(animated: true)
                }
                
                objUserInfo = getUserInfo()
                editProfileVC.userID = objUserInfo.id ?? ""
                editProfileVC.isFromVC = "Home"
                self.navigationController?.pushViewController(editProfileVC, animated: true)
            }
        }
    }
    
    // MARK: - getHomeDetails
    /**
     This function is used to fetch home details from the server.
     It makes a network call to the specified API endpoint and updates the UI accordingly.
     
     - Note: This function requires the user to be logged in and have a valid user ID.
     
     - Important: This function should be called after the user has successfully logged in.
     
     - Precondition: The `objUserInfo` object must be initialized with the user's information.
     
     - Postcondition: The home details are fetched and displayed on the screen.
     
     - Throws: An error if the network call fails or if there is an issue with unarchiving the user information.
     
     - Returns: None.
     */
    func getHomeDetails()
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.Dashboard, showLoader: isShowLoader) { [self] (resultDict, status, message) in
            if status == true
            {
                self.refreshControl.endRefreshing()
                self.objDashboard = Dashboard(fromJson: JSON(resultDict))
                self.configureMarqueeText()
                if let arrShipping = (resultDict as NSDictionary).value(forKey: "shipping_method") as? [NSDictionary]
                {
                    self.arrShippingMethod = arrShipping
                }
                //new change
                if let notifyMsg = self.objDashboard.noticeMsg, notifyMsg != ""
                {
                    self.notifyView.isHidden = false
                    self.lblNotifyMsg.text = notifyMsg
                }
                else
                {
                    self.notifyView.isHidden = true
                    self.lblNotifyMsg.text = ""
                }
                
                //Nand : New Feature Updates Code
                DispatchQueue.main.async { [self] in
                    showHideNewFeatureUpdates()
                }
                
                //over
                arrFeedbackReason = self.objDashboard.feedbackReason ?? []
                if  let decoded = UserDefaults.standard.object(forKey: kUserInfo) as? Data
                {
                    do {
                        if let unarchivedObject = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserInfo.self, from: decoded) {
                            // Handle the unarchived object
                            self.objUserInfo = unarchivedObject
                            self.objUserInfo.firstName = self.objDashboard.firstName ?? ""
                            self.objUserInfo.profileImagePath = self.objDashboard.profileImagePath ?? ""
                            self.objUserInfo.referralCode = self.objDashboard.referralCode ?? ""
                            
                            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: self.objUserInfo, requiringSecureCoding: true)
                            UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                            
                            self.showUserInfoData()
                        } else {
                            // Handle the case where unarchiving failed
                        }
                    } catch {
                        // Handle the error
                        print("Error info: \(error)")
                    }
                }
                var strNotification = self.objDashboard.notificationCount ?? "0"
                if strNotification == ""
                {
                    strNotification = "0"
                }
                if strNotification == "0"
                {
                    self.lblNotificationCount.isHidden = true
                }
                else
                {
                    self.lblNotificationCount.isHidden = false
                    if Int(strNotification)! <= 9
                    {
                        self.lblNotificationCount.text = strNotification
                    }
                    else
                    {
                        self.lblNotificationCount.text = "9+"
                    }
                }
                UIApplication.shared.applicationIconBadgeNumber = Int(strNotification)!
                cartCounter    = self.objDashboard.cartCount ?? 0 //new cart counter
                referralAmount = self.objDashboard.referralAmount ?? 0 //new change
                openTicketURL  = self.objDashboard.osticketWebUrl ?? ""
                openTicketLogoutURL = self.objDashboard.osticketLogoutUrl ?? ""
                documentSizeLimit = self.objDashboard.documentSizeLimit ?? 2
                documentLimit = self.objDashboard.documentLimit ?? 5
                DispatchQueue.main.async {
                    self.tblDashboard.reloadData()
                }
                //new change
                let objUserInfo = getUserInfo()
                if let isProfileRequired = objUserInfo.profileRequired, isProfileRequired == 1 //1 change to 1
                {
                    self.goToEditProfileVC(userID: objUserInfo.id ?? "")
                }
                else if let serviceGuide = self.objDashboard.serviceGuideAlert as? Int,serviceGuide == 1 //change to 1
                {
                    if let popupvc = self.presentedViewController as? ServiceGuideAlertVC
                    {
                        popupvc.type = 1
                        popupvc.version = self.objDashboard.serviceGuideVersion ?? ""
                        popupvc.htmlString = self.objDashboard.serviceGuideMessage ?? ""
                        popupvc.navvc = self.navigationController
                        NotificationCenter.default.post(name: Notification.Name("ServiceGuidePresented"), object: nil)
                    }
                    else
                    {
                        if let popupVC = appDelegate.getViewController("ServiceGuideAlertVC", onStoryboard: "Home") as? ServiceGuideAlertVC {
                            popupVC.type = 1
                            popupVC.version = self.objDashboard.serviceGuideVersion ?? ""
                            popupVC.htmlString = self.objDashboard.serviceGuideMessage ?? ""
                            popupVC.navvc = self.navigationController
                            popupVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.8)
                            popupVC.modalPresentationStyle = .overFullScreen
                            self.present(popupVC, animated: true, completion: nil)
                        }
                    }
                }
                else if let onlineShopperGuide = self.objDashboard.onlineShopperAlert as? Int,onlineShopperGuide == 1 //change to 1
                {
                    if let popupvc = self.presentedViewController as? ServiceGuideAlertVC
                    {
                        popupvc.type = 2
                        popupvc.version = ""
                        popupvc.htmlString = self.objDashboard.onlineShopperGuideMessage ?? ""
                        popupvc.navvc = self.navigationController
                        NotificationCenter.default.post(name: Notification.Name("ServiceGuidePresented"), object: nil)
                    }
                    else
                    {
                        if let popupVC = appDelegate.getViewController("ServiceGuideAlertVC", onStoryboard: "Home") as? ServiceGuideAlertVC {
                            popupVC.type = 2
                            popupVC.version = ""
                            popupVC.htmlString = self.objDashboard.onlineShopperGuideMessage ?? ""
                            popupVC.navvc = self.navigationController
                            popupVC.view.backgroundColor = UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.8)
                            popupVC.modalPresentationStyle = .overFullScreen
                            self.present(popupVC, animated: true, completion: nil)
                        }
                    }
                }
                else
                {
                    self.isShowBiometricPopup()
                }
            }
            else
            {
                self.refreshControl.endRefreshing()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    //Nand : New Feature Updates Code
    /**
     Shows or hides new feature updates based on certain conditions.
     
     This function checks if the `showNewFeatureUpdates` property of `objDashboard` is equal to 1 and if the `kShowFeatureMsg` key in `UserDefaults` is set to `true`. If both conditions are met, it shows the `weekendPrefNotifyViewMain` view, sets up the weekend preference message, adjusts the height of the `tvWeekendPrefNotifyMsg` text view, and updates the height constraint of the `cnstWeekendPrefNotifViewHeight` constraint. If the height constraint is less than 70, it sets it to 70. Otherwise, it calls the `hideNewFeatureUpdates()` function.
     */
    func showHideNewFeatureUpdates(){
        let isShowFeatureMsg = UserDefaults.standard.value(forKey: kShowFeatureMsg) as? Bool
        
        if self.objDashboard.showNewFeatureUpdates == 1 && isShowFeatureMsg == true
        {
            self.weekendPrefNotifyViewMain.isHidden = false
            self.setUpWeekendPrefMsg(weekendPrefMsg: self.objDashboard.featureUpdateMsg ?? "")
            self.tvWeekendPrefNotifyMsg.adjustUITextViewHeight()
            
            self.cnstWeekendPrefNotifViewHeight.constant = self.tvWeekendPrefNotifyMsg.frame.size.height + 20
            
            if self.cnstWeekendPrefNotifViewHeight.constant < 70 {
                self.cnstWeekendPrefNotifViewHeight.constant = 70
            }
            
        }else{
            hideNewFeatureUpdates()
        }
    }
    
    //Nand : New Feature Updates Code
    /**
     Hides the new feature updates view.
     */
    func hideNewFeatureUpdates(){
        self.weekendPrefNotifyViewMain.isHidden = true
        self.cnstWeekendPrefNotifViewHeight.constant = 0
    }

    // MARK: - Tableview Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return 340            
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardMenuCell", for: indexPath) as? DashboardMenuCell {
                
                if isShowDriveThruOption {
                    cell.viewDriveThru.alpha = 1
                }else{
                    cell.viewDriveThru.alpha = 0
                }

                let strCartCounter = objDashboard.cartCount ?? 0
                if strCartCounter == 0
                {
                    cell.lblCartCounter.isHidden = true
                }
                else
                {
                    cell.lblCartCounter.isHidden = false
                    if strCartCounter <= 9
                    {
                        cell.lblCartCounter.text = String(strCartCounter)
                    }
                    else
                    {
                        cell.lblCartCounter.text = "9+"
                    }
                }
                if  UserDefaults.standard.value(forKey: kCartItemAvailable) != nil
                {
                    UserDefaults.standard.removeObject(forKey: kCartItemAvailable)
                }
                //over
                return cell
            }
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    /**
     Handles the button press event for the sections in the HomeVC.
     
     - Parameter sender: The button that was pressed.
     */
    @objc func btnSectionPressed(sender:UIButton)
    {
        let indexpath = IndexPath(row: 0, section: sender.tag)
        selectedSectionIndex = sender.tag
        sender.isSelected = !sender.isSelected
        if arrSelectedSections[sender.tag] == 0
        {
            if sender.tag == 1
            {
                preAlertCellHeight = 265
            }
            else if sender.tag == 2
            {
                invoiceCellHeight = 175
            }
            else if sender.tag == 3
            {
                creditCardCellHeight = 384
            }
        }
        else
        {
            if sender.tag == 1
            {
                preAlertCellHeight = 0
            }
            else if sender.tag == 2
            {
                invoiceCellHeight = 0
            }
            else if sender.tag == 3
            {
                creditCardCellHeight = 0
            }
        }
        if arrSelectedSections[sender.tag] == 0
        {
            arrSelectedSections[sender.tag] = 1
        }
        else
        {
            arrSelectedSections[sender.tag] = 0
        }
        DispatchQueue.main.async {
            let section = NSIndexSet(index:sender.tag)
            self.tblDashboard.reloadSections(section as IndexSet, with: .none)
            self.tblDashboard.reloadRows(at: [indexpath], with: .none)
        }
    }
    
    /**
     Checks for HomeVC and performs necessary actions.
     
     - Parameters:
        - vc: The view controller to be pushed onto the navigation stack.
     */
    func checkForHomeVC(vc: UIViewController?) {
        if let tabBarVC = self.revealViewController()?.frontViewController as? UITabBarController {
            tabBarVC.selectedIndex = 0
        }
        if let homeNavVC = appDelegate.topNavigation() {
            homeNavVC.pushViewController(vc!, animated: true)
        }
    }
    
    // MARK: - configureMarqueeText
    /**
     Configures the marquee text label.
     
     This method sets various properties of the `lblMarqueeText` label to create a marquee effect. It also adds a tap gesture recognizer to the label. The text of the label is set based on the `breakingNews` property of the `objDashboard` object. If the `breakingNews` is not empty, the label is shown and its height is set to 50. Otherwise, the label is hidden and its height is set to 0.
     */
    func configureMarqueeText(){
        
        self.lblMarqueeText.textAlignment = .left
        self.lblMarqueeText.type = .continuous
        self.lblMarqueeText.speed = .duration(20)
        self.lblMarqueeText.fadeLength = 0.0
        self.lblMarqueeText.leadingBuffer = 0
        self.lblMarqueeText.trailingBuffer = self.lblMarqueeText.frame.size.width
        self.lblMarqueeText.animationCurve = .linear
        self.lblMarqueeText.lineBreakMode = .byTruncatingTail
//        self.lblMarqueeText.forceScrolling = true
        self.lblMarqueeText.isUserInteractionEnabled = true
        self.lblMarqueeText.animationDelay = 0
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(marqueeTapped))
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        self.lblMarqueeText.addGestureRecognizer(tapRecognizer)
        
        if let breakingNews : String = objDashboard.breakingNews {
            if breakingNews.count > 0 {
                self.lblMarqueeText.text = breakingNews
                self.viewBreakingNews.isHidden = false
                self.cnstBreakingNewsViewHeight.constant = 50
            }else {
                self.viewBreakingNews.isHidden = true
                self.cnstBreakingNewsViewHeight.constant = 0
            }
        }else{
            self.viewBreakingNews.isHidden = true
            self.cnstBreakingNewsViewHeight.constant = 0
        }
    }
    
    // MARK: - marqueeTapped
    /**
     Handles the tap gesture on the marquee view.
     
     - Parameter recognizer: The `UIGestureRecognizer` object that triggered the tap gesture.
     */
    @objc func marqueeTapped(_ recognizer: UIGestureRecognizer) {
        print("Marquee Tapped")
        if let breakingNewsUrl : String = objDashboard.breakingNewsLink {
            if let webViewVC = appDelegate.getViewController("PDFViewerVC", onStoryboard: "Invoice") as? PDFViewerVC {
                webViewVC.pdfFile = breakingNewsUrl
                webViewVC.strTitle = "Breaking News"
                self.navigationController?.pushViewController(webViewVC, animated: true)
            }
        }
    }
    
    /**
     Sets up the weekend preference notification view when there is a trait collection change.
     
     This method checks if the `showNewFeatureUpdates` flag is set to 1 in the `objDashboard` object and if the `kShowFeatureMsg` key in `UserDefaults` is set to `true`. If both conditions are met, it calls the `setUpWeekendPrefMsg` method to display the weekend preference message with the content from the `featureUpdateMsg` property of the `objDashboard` object.
     */
    func setUpWeekendPrefNotifViewForTraitCollectionChange(){
        let isShowFeatureMsg = UserDefaults.standard.value(forKey: kShowFeatureMsg) as? Bool
        
        if self.objDashboard.showNewFeatureUpdates == 1 && isShowFeatureMsg == true
        {
            self.setUpWeekendPrefMsg(weekendPrefMsg: self.objDashboard.featureUpdateMsg ?? "")
        }
    }
    
    // MARK: - UITextView Delegate Method
    //Nand : New Feature Updates Code
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        if textView == tvWeekendPrefNotifyMsg {
            self.getWeekendPrefURLData(isShowLoader: true, url: self.objDashboard.newFeatureEndpoint ?? "")
        }
        return true
    }
        
    // MARK: - TraitCollectionDidChange didChange
    /**
     This method is called when the trait collection of the view controller changes.
     
     - Parameter previousTraitCollection: The previous trait collection.
     */
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Do something
        
        //Nand : New Feature Updates Code
        
        super.traitCollectionDidChange(previousTraitCollection)
        
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                DispatchQueue.main.async { [self] in
                    setUpWeekendPrefNotifViewForTraitCollectionChange()
                }
            }
        } else {
            DispatchQueue.main.async { [self] in
                setUpWeekendPrefNotifViewForTraitCollectionChange()
            }
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnHowToUseClicked(_ sender: UIButton) {
        print("How to use clicked")
        if self.revealViewController()?.frontViewPosition.rawValue == 4 {
            //If Side menu is open then it will close the side menu
            self.revealViewController().revealToggle(animated: true)
        }
        if let vc = appDelegate.getViewController("HowToUseListVC", onStoryboard: "Home") as? HowToUseListVC {
            checkForHomeVC(vc:vc)
        }
    }
    
    @IBAction func btnMenuClicked(_ sender:Any)
    {
        UIView.setAnimationsEnabled(true)
        self.revealViewController().revealToggle(animated: true)
    }
    @IBAction func btnWalletClicked()
    {
        if let walletVC = appDelegate.getViewController("WalletVC", onStoryboard: "Referral") as? WalletVC{
            self.navigationController?.pushViewController(walletVC, animated: true)
        }
    }
    
    @IBAction func btnCloseClicked()
    {
        self.notifyView.isHidden = true
    }
    //Nand : New Feature Updates Code
    @IBAction func btnCloseWeekendPrefNotifViewClicked()
    {
        self.hideNewFeatureUpdates()
        UserDefaults.standard.setValue(false, forKey: kShowFeatureMsg)
    }
    @IBAction func btnPreAlertListClicked()
    {
        if self.revealViewController()?.frontViewPosition.rawValue == 4
        {
            //If Side menu is open then it will close the side menu
           self.revealViewController().revealToggle(animated: true)
        }
        if let preAlertListVC = appDelegate.getViewController("PreAlertListVC", onStoryboard: "PreAlert") as? PreAlertListVC {
            self.navigationController?.pushViewController(preAlertListVC, animated: true)
        }
    }
    @IBAction func btnInvoiceClicked()
    {
        if let vc = appDelegate.getViewController("SegmentPageVC", onStoryboard: "Invoice") as? SegmentPageVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnViewCartClicked()
    {
        if let viewCartVC = appDelegate.getViewController("ViewCartVC", onStoryboard: "Invoice") as? ViewCartVC {
            objUserInfo = getUserInfo()
            viewCartVC.userID = objUserInfo.id ?? ""
            viewCartVC.isFromVC = "Dashboard"
            self.navigationController?.pushViewController(viewCartVC, animated: true)
        }
    }
    @IBAction func btnRefundClicked()
    {
        if self.revealViewController()?.frontViewPosition.rawValue == 4
        {
            //If Side menu is open then it will close the side menu
           self.revealViewController().revealToggle(animated: true)
        }
        if let vc = appDelegate.getViewController("PaymentRefundVC", onStoryboard: "Invoice") as? PaymentRefundVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnFeedbackClicked()
    {
        if let vc = appDelegate.getViewController("FeedbackVC", onStoryboard: "Home") as? FeedbackVC {
            vc.arrFeedbackReason = arrFeedbackReason
            vc.mediaSizeLimit = documentSizeLimit
            vc.mediaLimit = documentLimit
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnCalculatorClicked()
    {
        if let vc = appDelegate.getViewController("CalculatorVC", onStoryboard: "Invoice") as? CalculatorVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnLogoutClicked()
    {
        objUserInfo = getUserInfo()
        self.showAlertForLogout(title: "", msg: Messsages.msg_logout,userID:objUserInfo.id ?? "",type:"")
    }
    @IBAction func btnTrackOrderClicked()
    {
        //self.revealViewController().revealToggle(animated: true)
        if self.revealViewController()?.frontViewPosition.rawValue == 4
        {
            //If Side menu is open then it will close the side menu
           self.revealViewController().revealToggle(animated: true)
        }
        if let vc = appDelegate.getViewController("TrackOrderVC", onStoryboard: "Home") as? TrackOrderVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnPickupClicked()
    {
        if let vc = appDelegate.getViewController("PickupRequestVC", onStoryboard: "Invoice") as? PickupRequestVC{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnDriveThruClicked(_ sender: UIButton) {
        print("Drive Thru Clicked")
        
        let driveThruStatus : Int = self.objDashboard.driveThruStatus ?? 0
        if driveThruStatus == 0 {
            self.showAlert(title: "", msg: self.objDashboard.driveThruStatusMsg ?? "", vc: self)
        }else{
            if isShowDriveThruOption {
                if self.revealViewController()?.frontViewPosition.rawValue == 4 {
                    //If Side menu is open then it will close the side menu
                    self.revealViewController().revealToggle(animated: true)
                }
                if let vc = appDelegate.getViewController("DriveThruVC", onStoryboard: "DriveThru") as? DriveThruVC{
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    @IBAction func btnNotificationClicked()
    {
        if let vc = appDelegate.getViewController("NotificationVC", onStoryboard: "Home") as? NotificationVC{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

//    @IBAction func btnPickupKioskClicked(_ sender: UIButton) {
//        print("PickupKiosk Thru Clicked")
//        checkCameraAccess { status in
//            DispatchQueue.main.async {
//                if status {
//                    if let scannerVC = appDelegate.getViewController("ScannerVC", onStoryboard:"DriveThru" ) as? ScannerVC {
//                        scannerVC.modalPresentationStyle = .overFullScreen
//                        scannerVC.scannerOutputDelegte = self
//                        self.present(scannerVC,animated: true, completion: nil)
//                    }
//                }else {
//                    openCameraAccessPopup(title: "", msg: "Camera access is denied.\nPlease enable camera access from Settings.", fromVC: self) { status in
//                        print("Status is \(status)")
//                    }
//                }
//            }
//        }
//    }


//    func getScannerOutput(output: String) {
//        print("Request detail screen Now qrcode output is \(output)")
//        self.driveThruAuthentication(token: output)
//    }
}
//Nand : New Feature Updates Code
extension HomeVC {
    
    // MARK: - setUpWeekendPrefMsg
    /**
     Sets up the weekend preference message with the provided text.
     
     - Parameters:
        - weekendPrefMsg: The text to be displayed as the weekend preference message.
     */
    func setUpWeekendPrefMsg(weekendPrefMsg: String) {
        if (UserDefaults.standard.value(forKey: kIsDarkModeEnabled) as? Bool) == true {
            self.tvWeekendPrefNotifyMsg.attributedText = weekendPrefMsg.htmlToAttributedString(size: 14, hexStringColor: "#FFFFFF", fontFamily: fontname.openSansRegular)
        }
        
        if (UserDefaults.standard.value(forKey: kIsLightModeEnabled) as? Bool) == true {
            self.tvWeekendPrefNotifyMsg.attributedText = weekendPrefMsg.htmlToAttributedString(size: 14, hexStringColor: "#000000", fontFamily: fontname.openSansRegular)
        }
        
        if (UserDefaults.standard.value(forKey: kIsSystemModeModeEnabled) as? Bool) == true {
            if isDarkTheme() {
                self.tvWeekendPrefNotifyMsg.attributedText = weekendPrefMsg.htmlToAttributedString(size: 14, hexStringColor: "#FFFFFF", fontFamily: fontname.openSansRegular)
            } else {
                self.tvWeekendPrefNotifyMsg.attributedText = weekendPrefMsg.htmlToAttributedString(size: 14, hexStringColor: "#000000", fontFamily: fontname.openSansRegular)
            }
        }
    }
    
    // MARK: - getWeekendPrefURLData
    /**
     This method is used to fetch data from a specified URL based on the user's weekend preference.
     
     - Parameters:
        - isShowLoader: A boolean value indicating whether to show a loader while fetching the data.
        - url: The URL to fetch the data from.
     */
    func getWeekendPrefURLData(isShowLoader: Bool, url : String)
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        print("params are \(params)")
        print("url is \(url)")
        
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: url, showLoader:true) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            if status == true
            {
                UserDefaults.standard.setValue(false, forKey: kShowFeatureMsg)

                self.hideNewFeatureUpdates()
                self.redirectToProfileScreen()
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - redirectToProfileScreen
    /**
     Redirects the user to the profile screen.
     
     This function checks if the front view controller of the reveal view controller is a `UITabBarController`.
     If it is, it sets the selected index of the tab bar controller to 3 and sets the `isNeedToOpenMangeAddress` property of the app delegate to `true`.
     */
    func redirectToProfileScreen(){
        if let tabBarVC = self.revealViewController()?.frontViewController as? UITabBarController
        {
            tabBarVC.selectedIndex = 3
            appDelegate.isNeedToOpenMangeAddress = true
        }
    }
}

//extension HomeVC {
//    func driveThruAuthentication(token : String)
//    {
//        let objUserInfo = getUserInfo()
//
//        var params : [String:Any] = [:]
//        params["user_id"] = objUserInfo.id ?? ""
//        params["token"] = token
//
//        print("parameters are \(params)")
//
//        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.driveThruAuth, showLoader: isShowLoader) { [self] (resultDict, status, message) in
//            if status == true
//            {
//                print("Drive thru authenticated....")
//                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(),isForSuccess: true)
//            }
//            else
//            {
//                if message == "" {
//                    appDelegate.showToast(message: "Please show your invoice QR Code.", bottomValue: getSafeAreaValue())
//                }else{
//                    appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
//                }
//            }
//        }
//    }
//}

/// Represents a custom table view cell for the dashboard menu.
class DashboardMenuCell : UITableViewCell
{
    @IBOutlet weak var viewDriveThru: UIView!
    @IBOutlet weak var lblCartCounter : UILabel!

    override func awakeFromNib() {
        lblCartCounter.layer.cornerRadius = lblCartCounter.frame.size.height/2
        lblCartCounter.layer.masksToBounds = true
    }
}
/// A custom cell used for displaying credit card information in a dropdown menu.
class CreditCardCell: DropDownCell
{
    @IBOutlet weak var lblCreditLimit : UILabel!
    @IBOutlet weak var lblRemainingAmount : UILabel!
    @IBOutlet weak var lblOutstandingAmount : UILabel!
    @IBOutlet weak var lblCreditPeriod : UILabel!
    @IBOutlet weak var lblStartDate : UILabel!
    @IBOutlet weak var lblEndDate : UILabel!
    @IBOutlet weak var containerView : UIView!
    override func awakeFromNib() {
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
    }
}
/// Represents a custom table view cell for displaying invoice information.
class InvoiceCell: UITableViewCell
{
    @IBOutlet weak var lblInvoiceAmount : UILabel!
    @IBOutlet weak var lblInvoiceDt : UILabel!
    @IBOutlet weak var containerView : UIView!
    override func awakeFromNib() {
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
    }
}
/// Represents a custom table view cell used for the header section in the HomeVC.
class HomeHeaderCell : UITableViewCell
{
    @IBOutlet weak var imgUpDown : UIImageView!
    @IBOutlet weak var btnSection : UIButton!
    @IBOutlet weak var lblSectionTitle : UILabel!
    @IBOutlet weak var btnMore : UIButton!
    @IBOutlet weak var imgContainer : UIView!
}
