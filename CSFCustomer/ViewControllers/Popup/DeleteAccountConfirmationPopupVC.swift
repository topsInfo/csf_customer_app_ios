//
//  DeleteAccountConfirmationPopupVC.swift
//  CSFCustomer
//
//  Created by iMac on 29/08/22.
//

import UIKit
class DeleteAccountConfirmationPopupVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var ContainerView : UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblUnderstand: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    
    // MARK: - Global Variable
    var refVC : UIViewController!
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        configureText()
    }
    
    // MARK: - configureText
    func configureText(){
        self.lblTitle.text = "You are about to delete your CSF account."
        self.lblUnderstand.text = "I Understand that"
        self.lblInfo.text = "You will lose all your data on deleting the account, please take backup."
    }
    
    // MARK: - IBAction methods
    @IBAction func btnCancleClicked(_ sender: UIButton) {
        print("btnCancleClicked")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnContinueClicked(_ sender: UIButton) {
        print("btnOkClicked")
        self.dismiss(animated: true) {
            if let deleteAccountUserInfoVC = appDelegate.getViewController("DeleteAccountUserInfoVC", onStoryboard: "Home") as? DeleteAccountUserInfoVC {
                //            popupVC.deleteAccountTermsAndConditionDelegate  = self
                deleteAccountUserInfoVC.refVC = self.refVC
                self.refVC.navigationController?.pushViewController(deleteAccountUserInfoVC, animated: true)
            }
        }
    }
}
