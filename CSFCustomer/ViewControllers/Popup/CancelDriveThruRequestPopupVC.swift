//
//  CancelDriveThruRequestPopupVC.swift
//  CSFCustomer
//
//  Created by Tops on 31/03/21.
//

import UIKit
import IQKeyboardManagerSwift
import DropDown

let messageCharacterLimitForCancelRequest : Int = 200

protocol CancelDriveThruRequestPopupVCVCDelegate {
    func getReasonToCancelRequest(reason:String)
}
class CancelDriveThruRequestPopupVC: UIViewController, UITextViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tvDesc : UITextView!
    @IBOutlet weak var viewDesc : UIView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var tblCancelDriveThruRequest : UITableView!
    @IBOutlet weak var txtCancelReason : UITextField!
    @IBOutlet weak var viewCancelReason: UIView!
    @IBOutlet weak var stackViewNotes: UIStackView!
    
    // MARK: - Global Variable
    var cancelDriveThruRequestVCDelegate : CancelDriveThruRequestPopupVCVCDelegate?
    var placeholderText = "Enter Notes"
    var selectReasonPlaceholderText = "Select Reason"
    var cancellationReasonArr : [String]!
    var cancellationReason : String = ""
    let dropDown = DropDown()

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - configureControls
    func configureControls()
    {
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black") //UIColor.black.withAlphaComponent(0.5)

        configureTextView(textField:tvDesc, placeHolder: placeholderText, font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named:"theme_lightgray_color")!, cornerRadius: 5, borderColor: UIColor(named:"theme_lightgray_color")!, borderWidth: 1, bgColor: UIColor(named:"theme_textfield_bgcolor")!)
        tvDesc.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        managePopupHeight(isOther: false)
        tblCancelDriveThruRequest.tableFooterView = UIView()
        
        tvDesc.keyboardDistanceFromTextField = 80
        
        viewCancelReason.layer.borderWidth = 1
        viewCancelReason.layer.borderColor = Colors.theme_lightgray_color.cgColor
        txtCancelReason.paddingView(xvalue: 10)
        tvDesc.isUserInteractionEnabled = false
        configureTextField(textField: txtCancelReason, placeHolder: selectReasonPlaceholderText, font: fontname.openSansRegular, fontSize: 14, textColor: UIColor(named: "theme_text_color")!, cornerRadius: 0, borderColor: Colors.theme_lightgray_color, borderWidth: 0, bgColor: UIColor(named: "theme_textfield_bgcolor")!)

        self.setCancellationReasonDrodown()
    }

    func managePopupHeight(isOther : Bool){
        
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            var topHeight = safeAreaHeight - 250
            
            if isOther {
                topHeight = safeAreaHeight - 380
            }else{
                topHeight = safeAreaHeight - 250
            }
            
            topConst.constant = topHeight
        }
    }
    
    func setCancellationReasonDrodown()
    {
        dropDown.width = UIScreen.main.bounds.size.width - 60
        dropDown.shadowRadius = 0
        dropDown.direction = .any
        dropDown.backgroundColor = UIColor(named:"theme_dropdown_color")!//Colors.theme_white_color
        dropDown.separatorColor = UIColor(named:"theme_lightgray_color")! //Colors.theme_lightgray_color
        dropDown.textFont = UIFont(name: fontname.openSansRegular, size: 14)!
        dropDown.textColor = UIColor(named:"theme_text_color")!
        dropDown.selectionBackgroundColor = Colors.theme_dropdown_selection_back_color
        dropDown.selectedTextColor = Colors.theme_dropdown_selection_text_color
        dropDown.offsetFromWindowBottom = 10
        dropDown.selectionAction = { [weak self] (index, item) in
            if index >= 0
            {
                self?.tvDesc.text = self?.placeholderText
                self?.tvDesc.textColor = UIColor(named: "theme_lightgray_color")
                self?.txtCancelReason.text = self?.selectReasonPlaceholderText
                self?.cancellationReason = ""
                
                print("selected reason is \(self?.cancellationReasonArr[index])")
                if let reason : String = self?.cancellationReasonArr[index] {
                    if reason == "Other" {
                        self?.stackViewNotes.isHidden = false
                        self?.tvDesc.isUserInteractionEnabled = true
                        self?.managePopupHeight(isOther: true)
                    }else{
                        self?.stackViewNotes.isHidden = true
                        self?.tvDesc.isUserInteractionEnabled = false
                        self?.managePopupHeight(isOther: false)
                    }
                    self?.txtCancelReason.text = reason
                    self?.cancellationReason = reason
                }
                
                self?.dropDown.hide()
            }
        }
    }
    
    // MARK: - isValidData
    func isValidData() -> Bool
    {
        if self.cancellationReason == "Other"{
            if ((((self.tvDesc.text) ?? "").isEmpty) || self.tvDesc.text == placeholderText) {
                appDelegate.showToast(message: Messsages.msg_enter_note, bottomValue: getSafeAreaValue())
                return false
            }
        }else{
            if (self.cancellationReason.isEmpty) {
                appDelegate.showToast(message: Messsages.msg_select_reason_cancel_driveThruRequest, bottomValue: getSafeAreaValue())
                return false
            }
        }
        
        return true
    }
    
    // MARK: - IBAction Methods
    @IBAction func btnSubmitClicked()
    {
        self.view.endEditing(true)
        if isValidData() {
            self.dismiss(animated: true) {
                if self.cancellationReason == "Other" {
                    self.cancellationReason = self.tvDesc.text
                }
                self.cancelDriveThruRequestVCDelegate?.getReasonToCancelRequest(reason: self.cancellationReason.trimmingCharacters(in: .whitespacesAndNewlines))
            }
        }
    }
    
    @IBAction func btnCancelClicked()
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
   
    @IBAction func btnCancellationReasonsDropDownClicked(_ sender: UIButton) {
        self.dropDown.bottomOffset = CGPoint(x:0 , y:(self.txtCancelReason.bounds.height))
        self.dropDown.anchorView = self.txtCancelReason // UIView or UIBarButtonItem
        self.dropDown.dataSource = self.cancellationReasonArr
        self.dropDown.reloadAllComponents()
        self.dropDown.show()
    }
    
}

extension CancelDriveThruRequestPopupVC {
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == placeholderText
        {
            textView.text = ""
            textView.textColor = UIColor(named: "theme_black_color")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty
        {
            textView.text = placeholderText
            textView.textColor = UIColor(named: "theme_lightgray_color") //Colors.theme_lightgray_color
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return newText.count <= messageCharacterLimitForCancelRequest
    }
}
