//
//  DeletePopupVC.swift
//  CSFCustomer
//
//  Created by Tops on 01/12/20.
//

import UIKit
import MBProgressHUD
class DeletePopupVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var containerView :UIView!

    // MARK: - Global Variable
    var isFromVC : String = ""
    var userID : String = ""
    var preAlertID : String = ""
    var strInvoiceIDs : String = ""
    var childUserID : String = ""
    var cardID : String = ""
    var addressID : String = ""
    var completionBlock : ((Bool) -> Void)?
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
    }
    // MARK: - IBAction methods
    @IBAction func btnCancelClicked()
    {
        if isFromVC == "DeleteItem"
        {
            completionBlock!(false)
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnDeleteClicked()
    {
        if isFromVC == "PreAlertListVC"
        {
            self.CallAPIForPreAlertDelete()
        }
        else if isFromVC == "ViewCartVC"
        {
            self.callAPIForDeleteCartItem()
        }
        else if isFromVC == "DeleteUser"
        {
            self.callAPIForDeleteUser()
        }
        else if isFromVC == "Checkout" || isFromVC == "ManageCreditCard"
        {
            self.callAPIToDeleteCard()
        }
        else if isFromVC == "DeleteAddress"
        {
            self.callAPIToDeleteAddress()
        }
        else if isFromVC == "DeleteItem"
        {
            completionBlock!(true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    // MARK: - Custom methods
    func callAPIToDeleteAddress()
    {
        self.showHUD()
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["address_id"] = addressID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.deleteAddress, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true)
                NotificationCenter.default.post(name: Notification.Name("AddressUpdated"), object: nil)
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    func  callAPIToDeleteCard()
    {
        self.showHUD()
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["card_id"] = cardID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.DeleteStripeCard, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true)
                NotificationCenter.default.post(name: Notification.Name("CardDeleted"), object: nil)
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    func callAPIForDeleteUser()
    {
        self.showHUD()
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["child_user_id"] = childUserID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.DeleteUser, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true)
                NotificationCenter.default.post(name: Notification.Name("ChildUserInfoUpdated"), object: nil)
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    func callAPIForDeleteCartItem()
    {
        self.showHUD()
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["invoice_id"] = strInvoiceIDs
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.RemoveCart, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(),isForSuccess: true)
                NotificationCenter.default.post(name: Notification.Name("InvoiceDeleted"), object: nil)
                self.dismiss(animated: true, completion: nil)
                
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    func CallAPIForPreAlertDelete()
    {
        self.showHUD()
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["prealert_id"] = preAlertID
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.PreAlertDelete, showLoader: false) { (resultDict, status, message) in
            self.hideHUD()
            if status == true
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(),isForSuccess: true)
                NotificationCenter.default.post(name: Notification.Name("RecordDeleted"), object: nil)
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.hideHUD()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}
