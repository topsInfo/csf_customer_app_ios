//
//  ReportBugSuccessVC.swift
//  CSFCustomer
//
//  Created by Tops on 07/10/21.
//

import UIKit

class ReportBugSuccessPopupVC: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var containerView :UIView!
    @IBOutlet weak var lblSuccessMessage :UILabel!
    
    // MARK: - Global Variable
    var reportVC : ReportVC!
    var successMessage : String = ""
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.8)
        lblSuccessMessage.text = successMessage
    }
    
    // MARK: - IBAction methods
    @IBAction func btnDoneClicked(_ sender:Any)
    {
        reportVC.navigationController?.popViewController(animated: false)

        self.dismiss(animated: true) { [self] in
//            if let tabBarVC = self.revealViewController()?.frontViewController as? UITabBarController
//            {
//                tabBarVC.selectedIndex = 3
//            }
        }
    }    
}
