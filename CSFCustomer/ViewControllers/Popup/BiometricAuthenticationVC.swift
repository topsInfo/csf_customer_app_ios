//
//  BiometricAuthenticationVC.swift
//  CSFCustomer
//
//  Created by Tops on 01/02/21.
//

import UIKit
import LocalAuthentication
import SwiftyJSON
class BiometricAuthenticationVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var dimmerView : UIView!
    @IBOutlet weak var touchIDStackview : UIStackView!
    @IBOutlet weak var faceIDStackview : UIStackView!
    @IBOutlet weak var imgPic : UIImageView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    
    // MARK: - Global Variable
    let laContext = LAContext()
    var authError: NSError?
    var typeOfBiometric : Int = 0
    var userID : String = ""
    var loginUniqueID : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dimmerView.layer.cornerRadius = 10
        dimmerView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner,.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
        
        laContext.localizedFallbackTitle = "Use Passcode"
        imgPic.image = UIImage(named:"")
        faceIDStackview.isHidden = true
        touchIDStackview.isHidden = true
        
        checkForBiometricAvailability()
        
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            let topHeight = safeAreaHeight - 300
            topConst.constant = topHeight
        }
        
    }
    // MARK: - Custom Methods
    func checkForBiometricAvailability()
    {
        if laContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError)
        {
            if authError != nil {
                // handle error
                showAlert(title: "", msg: Messsages.msg_pls_try_again, vc: self)
            }
            else
            {
                //                if #available(iOS 11.0, *) {
                //
                //                }
                do {
                    if laContext.biometryType == .faceID {
                        //localizedReason = "Unlock using Face ID"
                        imgPic.image = UIImage(named: "face_id")
                        touchIDStackview.isHidden = true
                        faceIDStackview.isHidden = false
                        typeOfBiometric = 2
                        configureControls()
                       // print("FaceId support")
                    } else if laContext.biometryType == .touchID {
                        //localizedReason = "Unlock using Touch ID"
                        imgPic.image = UIImage(named:"fingerprint")
                        faceIDStackview.isHidden = true
                        touchIDStackview.isHidden = false
                        typeOfBiometric = 1
                      //  print("TouchId support")
                        configureControls()
                        
                    } else {
                        showAlert(title: "", msg: Messsages.msg_no_biometric, vc: self)
                    }
                } //do over
            } //else over
        }
        else
        {
            // Fallback on earlier versions
            showAlert(title: "", msg: Messsages.msg_biometric_error, vc: self)
        }
    }
    func configureControls()
    {
        let reasonString = "To access the secure data"
        if laContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            laContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                
                if success {
                    DispatchQueue.main.async {
                        self.callAPIForBiometric()
                    }
                } else {
                    // User did not authenticate successfully, look at error and take appropriate action
                    guard let error = evaluateError else {
                        return
                    }
                    DispatchQueue.main.async {
                        self.showAlert(title: "", msg: self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code), vc:self )
                    }
                }
            }
        } else {
            
            guard let error = authError else {
                return
            }
        }        
    }
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
                
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
                
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
                
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts."
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
                
            default:
                message = "Did not find error code on LAError object"
            }
        }
        
        return message
    }
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
        
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.userCancel.rawValue:
            message = "The user cancelled the action"
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        
        return message
    }
    
    // MARK: - callAPIForBiometric
    func callAPIForBiometric()
    {
        var params : [String:Any] = [:]
        params["user_id"] = userID
        params["type"] = typeOfBiometric
        params["device_token"] = UserDefaults.standard.value(forKey: kDeviceToken) ?? ""
        params["device_type"] = kDeviceType
        params["unique_id"] = loginUniqueID
        params["device_name"] = kDeviceName
        params["device_brand"] = kDeviceBrand
        params["os_version"] = kOSVersion
        params["app_version"] = kVersion
        params["api_version"] = apiVersion
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.BioMetricLogin, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                let objUserInfo = UserInfo(fromJson: JSON(resultDict))
                //UserInfo.shared.setData(fromJson: JSON(resultDict))
                UserDefaults.standard.setValue((resultDict as NSDictionary).value(forKey: "token"), forKey: kAuthToken)
                UserDefaults.standard.setValue(true, forKey: kLoggedIn)
                //Nand : New Feature Updates Code
                UserDefaults.standard.setValue(true, forKey: kShowFeatureMsg)
                UserDefaults.standard.setValue(LoginType.AppLogin.rawValue,forKey: kTypeOfLogin)
                
                //                if let isRemember = UserDefaults.standard.value(forKey: kRememberMe) as? Bool,isRemember == true
                //                {
                //                    UserDefaults.standard.setValue(objUserInfo.boxId ?? "", forKey: kUsername)
                //                }
                //                else
                //                {
                //                    UserDefaults.standard.setValue("", forKey: kUsername)
                //                }
                self.saveCoreData(objUserInfo:objUserInfo,TypeOfLogin:LoginType.AppLogin.rawValue)
                
//                This old code is now replaced by below new code
//                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: objUserInfo)
//                UserDefaults.standard.set(encodedData, forKey: kUserInfo)
//                self.goToHomeVC()
                
                do {
                    let encodedData = try NSKeyedArchiver.archivedData(withRootObject: objUserInfo, requiringSecureCoding: true)
                    UserDefaults.standard.set(encodedData, forKey: kUserInfo)
                    self.goToHomeVC()
                } catch {                    
                    print("NSKeyedArchiver Error is : \(error.localizedDescription)")
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnUseAccountClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
}
