//
//  BiometricPopupVC.swift
//  CSFCustomer
//
//  Created by Tops on 27/01/21.
//

import UIKit

class BiometricPopupVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var containerView :UIView!
    @IBOutlet weak var btnYes : UIButton!
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        btnYes.layer.cornerRadius = 5
        btnYes.layer.masksToBounds = true
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")//Colors.theme_black_color.withAlphaComponent(0.8)
        if let typeOfLogin = UserDefaults.standard.value(forKey: kTypeOfLogin) as? String,typeOfLogin == LoginType.AppLogin.rawValue
        {
            btnYes.isHidden = false
        }
        else
        {
            btnYes.isHidden = true
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnSkipClicked()
    {
       self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnYesClicked()
    {
        self.dismiss(animated: true, completion: nil)
        if let vc = appDelegate.getViewController("SecurityCenterVC", onStoryboard: "Home") as? SecurityCenterVC {
            if let objNavigationController  = appDelegate.topNavigation() as? UINavigationController
            {
                objNavigationController.pushViewController(vc, animated: true)
            }
        }
    }
}
