//
//  ServiceGuideAlertVC.swift
//  CSFCustomer
//
//  Created by Tops on 24/03/21.
//

import UIKit

class ServiceGuideAlertVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var ContainerView : UIView!
    @IBOutlet weak var tvDesc: UITextView!
    @IBOutlet weak var cnstTvDescHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnReadMore: UIButton!
    @IBOutlet weak var viewButtonSeperator: UIView!
    @IBOutlet weak var btnAgree: UIButton!
    
    // MARK: - Global Variable
    var htmlString :String = ""
    var version : String = ""
    var navvc : UINavigationController?
    var type : Int = 1 // 1 -> Service guide 2 -> Online shopper guide
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        reloadView()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        tvDesc.isEditable = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(getData(_:)), name: NSNotification.Name(rawValue: "ServiceGuidePresented"), object: nil)
        if type == 1 {
            configureForServiceGuide()
        }else if type == 2 {
            configureForOnlineShopperGuide()
        }
        
        DispatchQueue.main.async { [self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                if self.tvDesc.contentSize.height >= UIScreen.main.bounds.size.height - 250 {
                    self.cnstTvDescHeight.constant = UIScreen.main.bounds.size.height - 250
                    self.tvDesc.isScrollEnabled = true
                }else{
                    self.cnstTvDescHeight.constant = self.tvDesc.contentSize.height
                    self.tvDesc.isScrollEnabled = false
                }
            }
        }
    }
    @objc func getData(_ notification: NSNotification)
    {
        if type == 1 {
            configureForServiceGuide()
        }else if type == 2 {
            configureForOnlineShopperGuide()
        }
        
        DispatchQueue.main.async { [self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
//                print("After delay self.cnstTvDescHeight.constant \(self.cnstTvDescHeight.constant)")
                if self.tvDesc.contentSize.height >= UIScreen.main.bounds.size.height - 250 {
                    self.cnstTvDescHeight.constant = UIScreen.main.bounds.size.height - 250
                    self.tvDesc.isScrollEnabled = true
                }else{
                    self.cnstTvDescHeight.constant = self.tvDesc.contentSize.height
                    self.tvDesc.isScrollEnabled = false
                }
            }
        }
    }
    
    func configureForServiceGuide(){
        lblTitle.text = "Service Guide \(version)"
        btnReadMore.isHidden = false
        viewButtonSeperator.isHidden = false
        btnAgree.isHidden = false
    }
    
    func configureForOnlineShopperGuide(){
        lblTitle.text = "Online Shopper Guide"
        btnReadMore.isHidden = false
        viewButtonSeperator.isHidden = false
        btnAgree.isHidden = false
    }
    
    // MARK: - callAPIForAgree
    func callAPIForAgree()
    {
        var params : [String:Any] = [:]
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["type"] = self.type
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.ServiceGuideAgree, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                NotificationCenter.default.post(name: Notification.Name("ServiceGuideAgreed"), object: nil)
//                self.showAlert(title: "", msg: message, vc: self)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnAgreeClicked()
    {
        self.dismiss(animated: true, completion: nil)
        callAPIForAgree()
    }
    @IBAction func btnReadMoreClicked()
    {
        self.dismiss(animated: true, completion: nil)
        if let serviceGuideVC = appDelegate.getViewController("ServiceGuideVC", onStoryboard: "Home") as? ServiceGuideVC {
            serviceGuideVC.type = self.type
            navvc?.pushViewController(serviceGuideVC, animated: true)
        }
    }
    
    // MARK: - reloadView
    func reloadView(){
//        print("reload views")
        DispatchQueue.main.async { [self] in
            if (UserDefaults.standard.value(forKey: kIsDarkModeEnabled) as? Bool) == true{
                self.tvDesc.attributedText = htmlString.htmlToAttributedString(size: 15, hexStringColor: "#FFFFFF", fontFamily: fontname.openSansRegular)
            }
            
            if (UserDefaults.standard.value(forKey: kIsLightModeEnabled) as? Bool) == true{
                self.tvDesc.attributedText = htmlString.htmlToAttributedString(size: 15, hexStringColor: "#000000", fontFamily: fontname.openSansRegular)
            }
            
            if (UserDefaults.standard.value(forKey: kIsSystemModeModeEnabled) as? Bool) == true{
                if isDarkTheme() {
                    self.tvDesc.attributedText = htmlString.htmlToAttributedString(size: 15, hexStringColor: "#FFFFFF", fontFamily: fontname.openSansRegular)
                }else{
                    self.tvDesc.attributedText = htmlString.htmlToAttributedString(size: 15, hexStringColor: "#000000", fontFamily: fontname.openSansRegular)
                }
            }
        }
    }
   
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Do sonthing
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                reloadView()
            }
        }else{
            reloadView()
        }
    }
}
