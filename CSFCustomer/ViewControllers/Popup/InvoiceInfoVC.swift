//
//  InvoiceInfoVC.swift
//  CSFCustomer
//
//  Created by Tops on 29/01/21.
//

import UIKit

class InvoiceInfoVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var tblInfo : UIView!
    @IBOutlet weak var lblDesc : UILabel!

    // MARK: - Global Variable
    var isFrom : String = ""
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFrom == "Invoice"
        {
            lblDesc.text = "Long press gesture is used to add multiple invoices to the cart."
        }
        else if isFrom == "ViewCart"
        {
            lblDesc.text = "Long press any invoice to delete from the cart."
        }
        else if isFrom == "Wallet"
        {
            lblDesc.text = "Your referral amount will be displayed as blocked amount. It will be transferred to wallet once you received your first order."
        }
        tblInfo.layer.cornerRadius = 10
        tblInfo.layer.masksToBounds = true
    }
    
    // MARK: - IBAction Methods
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
}
