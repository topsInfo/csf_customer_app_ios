//
//  PreAlertDisclaimerVC.swift
//  CSFCustomer
//
//  Created by Tops on 28/01/21.
//

import UIKit

class PreAlertDisclaimerVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblDisclaimer : UILabel!
    @IBOutlet weak var tblDisclaimer : UITableView!
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        tblDisclaimer.layer.cornerRadius = 10
        tblDisclaimer.layer.masksToBounds = true
        lblDisclaimer.text = "Customer Service First (CSF) Couriers Ltd Pre-alert strives to assist Members in getting charged the correct Duties and VAT, however there would be circumstances beyond our control such as inconsistencies in package identification markings, Customs & Excise valuations and the processing time of the pre-alerts, which may produce discrepancies in expected and actual charges.\n\nMembers should ensure that the information entered is realistic, that it reflects reality and is not fictional as this would represent an actual Declaration to Trinidad and Tobago's Customs & Excise Division. Incorrect or false Declarations can lead to a Member being requested by Customs & Excise to submit documents for reference or further clarification. CSF will not accept or be held accountable for any liability of incorrect or false Declarations of the information as this represents a breach in National Laws.\n\nAll pre-alerts should be sent at least 24 hours in advance. With that said CSF Couriers Ltd will not be held responsible for the final Duties and Vat charged on items."
    }
    // MARK: - IBAction methods
    @IBAction func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
}
