//
//  ReportBugVC.swift
//  CSFCustomer
//
//  Created by Tops on 06/10/21.
//

import UIKit
class ReportBugVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var dimmerView : UIView!
    @IBOutlet weak var viewReportBug : UIView!
    @IBOutlet weak var imgReportBug : UIImageView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var bottomConst : NSLayoutConstraint!
    
    // MARK: - Global Variables
    var navvc : UINavigationController?

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        dimmerView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner,.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        dimmerView.layer.cornerRadius = 15
        dimmerView.layer.masksToBounds = true
        
        var height : CGFloat = 0.0
        if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
           let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
        {
            DispatchQueue.main.async {
                height = 370
                self.topConst.constant = safeAreaHeight - height
                self.bottomConst.constant = bottomPadding
            }
        }
        dimmerView.backgroundColor = UIColor(named:"theme_bg_color")
    }
    
    // MARK: - IBAction methods
    @IBAction func btnReportABugClicked()
    {
        self.dismiss(animated: true) { [self] in
            showReportDeailVC(type: "1")
        }
    }
    
    @IBAction func btnSuggestionImprovementClicked()
    {
        self.dismiss(animated: true) { [self] in
            showReportDeailVC(type: "2")
        }
    }
    
    @IBAction func btnCancelClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showReportDeailVC(type : String){
        
        DispatchQueue.main.asyncAfter(deadline: .now()) { [self] in
            if let vc = appDelegate.getViewController("ReportVC", onStoryboard: "Profile") as? ReportVC {
                vc.type = type
                
                let transition = CATransition()
                transition.duration = 0.5
                transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
                transition.type = .moveIn
                transition.subtype = .fromTop
                navvc?.view.layer.add(transition, forKey: nil)
                navvc?.pushViewController(vc, animated: false)
            }
        }
    }
}
