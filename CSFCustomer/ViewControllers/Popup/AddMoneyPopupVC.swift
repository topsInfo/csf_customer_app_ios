//
//  AddMoneyPopupVC.swift
//  CSFCustomer
//
//  Created by Tops on 31/03/21.
//

import UIKit
import PassKit
import Stripe
protocol AddMoneyPopupVCDelegate {
    func showController(type:String)
}
class AddMoneyPopupVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblUSDAmount : UILabel!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnAddMoney : UIButton!
    
    // MARK: - Global Variable
    var PaymentType : String = ""
    var USDAmount : String  = "0"
    var addMoneyVCDelegate : AddMoneyPopupVCDelegate?
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    // MARK: - configureControls
    func configureControls()
    {
        btnCancel.layer.borderWidth = 1
        btnCancel.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor
        btnAddMoney.layer.borderWidth = 1
        btnAddMoney.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor
        lblUSDAmount.text = "\(kCurrencySymbol)\(USDAmount)"
    }
    // MARK: - IBAction Methods
    @IBAction func btnCancelClicked()
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnAddMoneyClicked()
    {
       self.dismiss(animated: true, completion: {
        if self.PaymentType == "PayPal"
            {
                self.addMoneyVCDelegate?.showController(type: "PayPal")
            }
            else if self.PaymentType == "ApplePay"
            {
                self.addMoneyVCDelegate?.showController(type: "ApplePay")
            }
            else if self.PaymentType == "CardPayment"
            {
                self.addMoneyVCDelegate?.showController(type: "CardPayment")
            }
            
        })
    }
}
