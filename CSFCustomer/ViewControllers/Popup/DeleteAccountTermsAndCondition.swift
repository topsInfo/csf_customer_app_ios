//
//  DeleteAccountTermsAndCondition.swift
//  CSFCustomer
//
//  Created by iMac on 29/08/22.
//

import UIKit
import WebKit

protocol DeleteAccountTermsAndConditionProtocol {
    func deleteAccountTermsAndCondition(isAgree : Bool)
}

class DeleteAccountTermsAndCondition: UIViewController, WKNavigationDelegate {
    // MARK: - IBOutlets
    @IBOutlet weak var ContainerView : UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var termsConditionWebView: WKWebView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewHeaderSeperator: UIView!
    @IBOutlet weak var viewFooterSeperator: UIView!
    @IBOutlet weak var activityIndiactor: UIActivityIndicatorView!
    
    // MARK: - Global Variable
    var deleteAccountTermsAndConditionDelegate : DeleteAccountTermsAndConditionProtocol!
    var refVC : UIViewController!
    var objDeleteAccountInfoModel = DeleteAccountInfoModel()

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        configureText()
        self.viewHeaderSeperator.dropShadow()
        self.viewFooterSeperator.dropShadow()
        self.termsConditionWebView.isHidden = true
        self.termsConditionWebView.navigationDelegate = self
        if let url : String = objDeleteAccountInfoModel.termsDocument {
            if !url.isEmpty {
                let urlRequest : URLRequest = URLRequest(url: URL(string: url)!)
                termsConditionWebView.load(urlRequest)
            }
        }
    }
    
    // MARK: - configureText
    func configureText(){
        self.lblTitle.text = "Terms & Conditions."
        self.lblSubTitle.text = "CSF Couriers Limited"
    }
    
    // MARK: - IBAction methods
    @IBAction func btnCancleClicked(_ sender: UIButton) {
        print("btnCancleClicked")
        self.dismiss(animated: true) { [self] in
            deleteAccountTermsAndConditionDelegate.deleteAccountTermsAndCondition(isAgree: false)
        }
    }
    
    @IBAction func btnContinueClicked(_ sender: UIButton) {
        print("btnOkClicked")
        self.dismiss(animated: true) { [self] in
            deleteAccountTermsAndConditionDelegate.deleteAccountTermsAndCondition(isAgree: true)
        }
    }
    
    // MARK: - WebView Delegate Methods
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.termsConditionWebView.isHidden = false
        print("Start Request")
        self.activityIndiactor.isHidden = false
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.termsConditionWebView.isHidden = false
        print("Failed Request")
        self.activityIndiactor.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.termsConditionWebView.isHidden = false
        print("Finished Request")
        self.activityIndiactor.isHidden = true
    }
}
