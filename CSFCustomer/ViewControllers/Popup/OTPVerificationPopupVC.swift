//
//  OTPVerificationPopupVC.swift
//  CSFCustomer
//
//  Created by iMac on 29/08/22.
//

import UIKit
import IQKeyboardManagerSwift

let timeLimit : Int = 30

class OTPTextField: UITextField {
    
    weak var previousTextField: OTPTextField?
    weak var nextTextField: OTPTextField?
    
    override public func deleteBackward(){
        text = ""
        previousTextField?.becomeFirstResponder()
    }
}

class OTPVerificationPopupVC: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var ContainerView : UIView!
    @IBOutlet weak var lblOTPHeaderMsg: UILabel!
    @IBOutlet weak var lblOTPTitleMsg: UILabel!
    @IBOutlet weak var txtFirstNumber : OTPTextField!
    @IBOutlet weak var txtSecondNumber : OTPTextField!
    @IBOutlet weak var txtThirdNumber : OTPTextField!
    @IBOutlet weak var txtFourthNumber : OTPTextField!
    @IBOutlet weak var txtFifthNumber : OTPTextField!
    @IBOutlet weak var txtSixthNumber : OTPTextField!
    @IBOutlet weak var viewAttemptsLeft: UIView!
    @IBOutlet weak var lblAttemptLeft: UILabel!
    @IBOutlet weak var lblResendOTP: UILabel!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var cnstContainerViewY : NSLayoutConstraint!
    
    // MARK: - Global Variable
    var refVC : UIViewController!
    var resendOTPAttemptCounterLeft : Int = 3
    var timer : Timer?
    var timerCount : Int = 0
    var otpId : Int = 0
    var reasonId : Int = 0
    var reasonNote : String = ""
    var textFieldsCollection: [OTPTextField] = []
    var remainingStrStack: [String] = []

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        textFieldsCollection.append(txtFirstNumber)
        txtFirstNumber.nextTextField = txtSecondNumber
        
        textFieldsCollection.append(txtSecondNumber)
        txtSecondNumber.nextTextField = txtThirdNumber
        txtSecondNumber.previousTextField = txtFirstNumber
        
        textFieldsCollection.append(txtThirdNumber)
        txtThirdNumber.nextTextField = txtFourthNumber
        txtThirdNumber.previousTextField = txtSecondNumber
        
        textFieldsCollection.append(txtFourthNumber)
        txtFourthNumber.nextTextField = txtFifthNumber
        txtFourthNumber.previousTextField = txtThirdNumber
        
        textFieldsCollection.append(txtFifthNumber)
        txtFifthNumber.nextTextField = txtSixthNumber
        txtFifthNumber.previousTextField = txtFourthNumber
        
        textFieldsCollection.append(txtSixthNumber)
        txtSixthNumber.previousTextField = txtFifthNumber

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        let objUserInfo = getUserInfo()
        self.lblOTPHeaderMsg.text = "Email One Time Password (OTP) Verification"

        self.lblOTPTitleMsg.text = "\nFor verification, an OTP has been sent to \(objUserInfo.emailAddress ?? "")\n\nTo proceed with deletion, enter it below."
  
        self.lblResendOTP.text = ""

        sendDeleteOtp(isForResend: false)
        txtFirstNumber.delegate = self
        txtSecondNumber.delegate = self
        txtThirdNumber.delegate = self
        txtFourthNumber.delegate = self
        txtFifthNumber.delegate = self
        txtSixthNumber.delegate = self
        
        self.txtFirstNumber.textContentType = .oneTimeCode
        self.txtSecondNumber.textContentType = .oneTimeCode
        self.txtThirdNumber.textContentType = .oneTimeCode
        self.txtFourthNumber.textContentType = .oneTimeCode
        self.txtFifthNumber.textContentType = .oneTimeCode
        self.txtSixthNumber.textContentType = .oneTimeCode

        enableDisableVeifyButton()
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        print("keyboard will show")
        
        self.cnstContainerViewY.constant = -150
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }

    @objc func keyboardWillHide(notification: Notification) {
        print("keyboard will hide")
        self.cnstContainerViewY.constant =  0
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func btnCancleClicked(_ sender: UIButton) {
        print("btnCancleClicked")
        self.view.endEditing(true)
        
        self.popupAlert(title: "", message: Messsages.msg_cancel_delete_account, actionTitles: ["Confirm","Cancel"], actions:[{action1 in
            self.dismiss(animated: true) { [self] in
                if let navVC = refVC.navigationController {
                    for viewController in navVC.viewControllers {
                        if viewController.isKind(of: ProfileVC.self) {
                            refVC.navigationController?.popToViewController(viewController, animated: true)
                            break
                        }
                        if viewController.isKind(of: HomeVC.self) {
                            refVC.navigationController?.popToViewController(viewController, animated: true)
                            break
                        }
                    }
                }
            }
        },{ action2 in
        }])
    }
    
    @IBAction func btnVerifyClicked(_ sender: UIButton) {
        print("btnOkClicked")
        self.view.endEditing(true)
        verifyDeleteOtp()
    }
    
    @IBAction func btnResendOTPClicked(_ sender: UIButton) {
        print("btnResendOTPClicked")
        self.view.endEditing(true)
        resetOTPFields()
        self.enableDisableVeifyButton()
        sendDeleteOtp(isForResend: true)
    }
    
    func resetOTPFields(){
        self.txtFirstNumber.text = ""
        self.txtSecondNumber.text = ""
        self.txtThirdNumber.text = ""
        self.txtFourthNumber.text = ""
        self.txtFifthNumber.text = ""
        self.txtSixthNumber.text = ""
        remainingStrStack.removeAll()
    }
    
    func showHideAttemptLeftCount(){
        if resendOTPAttemptCounterLeft > 0 {
            self.viewAttemptsLeft.isHidden = false
            self.lblAttemptLeft.text = "\(resendOTPAttemptCounterLeft) Attempt(s) Left"
            self.btnResendOTP.isUserInteractionEnabled = true
        }
        else {
            self.viewAttemptsLeft.isHidden = true
            self.lblAttemptLeft.text = ""
        }
    }
    
    func manageTimer(){
        timerCount = timeLimit
        timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(OTPVerificationPopupVC.timerEventCalled), userInfo: nil, repeats: true)
    }
    
    @objc func timerEventCalled()
    {
        if(timerCount > 0){
            showTimerValue()
            timerCount -= 1
        }else {
            timer?.invalidate()
            self.lblResendOTP.text = "Resend OTP?"
            self.lblResendOTP.textColor = Colors.theme_orange_color
            self.btnResendOTP.isUserInteractionEnabled = true
            
            if resendOTPAttemptCounterLeft == 0 {
                self.viewAttemptsLeft.isHidden = true
                self.lblAttemptLeft.text = ""
                self.lblResendOTP.textColor = Colors.theme_dele_ac_disable_button_text_color
                self.btnResendOTP.isUserInteractionEnabled = false
            }
        }
    }
    
    func showTimerValue(){
        
        let minutes = String(format: "%02d",((timerCount % 3600) / 60), 0)
        let seconds = String(format: "%02d",((timerCount % 3600) % 60), 0)
        
        self.lblResendOTP.text = minutes + ":" + seconds
        self.btnResendOTP.isUserInteractionEnabled = false
        self.lblResendOTP.textColor = Colors.theme_label_black_color
    }
    
    func isValidData() -> Bool{
        if txtFirstNumber.text!.isEmpty || txtSecondNumber.text!.isEmpty
            || txtThirdNumber.text!.isEmpty || txtFourthNumber.text!.isEmpty ||
            txtFifthNumber.text!.isEmpty || txtSixthNumber.text!.isEmpty {
            return false
        }
        return true
    }
    
    func enableDisableVeifyButton(){
        if isValidData() {
            enableVerifyButton()
        }else{
            disableVerifyButton()
        }
    }
    
    func enableVerifyButton(){
        self.btnVerify.backgroundColor = Colors.theme_blue_color
        self.btnVerify.setTitleColor(UIColor.white, for: .normal)
        self.btnVerify.isUserInteractionEnabled = true
    }
    
    func disableVerifyButton(){
        self.btnVerify.backgroundColor = Colors.theme_dele_ac_disable_button_back_color
        self.btnVerify.setTitleColor(Colors.theme_dele_ac_disable_button_text_color, for: .normal)
        self.btnVerify.isUserInteractionEnabled = false
    }
}

extension OTPVerificationPopupVC: UITextFieldDelegate {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        //This will dispble copy paste in otp textfileds.
        OperationQueue.main.addOperation {
            UIMenuController.shared.setMenuVisible(false, animated: false)
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.activeNextTextField(self.view)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing")
        self.enableDisableVeifyButton()
    }

    func textFieldDidChangeSelection(_ textField: UITextField) {
        print("textFieldDidChangeSelection")
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("textFieldShouldClear")
        return true
    }
    
    func keyboardInputShouldDelete(_ textField: UITextField) -> Bool {
        print("keyboardInputShouldDelete")
        return true
    }

    //switches between OTPTextfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange,
                   replacementString string: String) -> Bool {
        guard let textField = textField as? OTPTextField else { return true }
        if (string == " ") {
            return false
        }
        if string.count > 1 {
            textField.resignFirstResponder()
            autoFillTextField(with: string)
            enableDisableVeifyButton()
            return false
        } else {
            if (range.length == 0 && string == "") {
                enableDisableVeifyButton()
                return false
            } else if (range.length == 0){
                if textField.nextTextField == nil {
                    textField.text? = string
                    textField.resignFirstResponder()
                }else{
                    textField.text? = string
                    textField.nextTextField?.becomeFirstResponder()
                }
                enableDisableVeifyButton()
                return false
            }
            return true
        }
    }

    func autoFillTextField(with string: String) {
        remainingStrStack = string.reversed().compactMap{String($0)}
        for textField in textFieldsCollection {
            if let charToAdd = remainingStrStack.popLast() {
                textField.text = String(charToAdd)
            } else {
                break
            }
        }
        remainingStrStack = []
    }
}

extension OTPVerificationPopupVC {
    
    func deleteUserAccount() {
        
        let objUserInfo = getUserInfo()
        
        if let loginType = UserDefaults.standard.value(forKey: kTypeOfLogin) as? String,loginType == LoginType.AppleLogin.rawValue {
            print("Apple delete account")
            
            if KeychainItem.currentUserToken != "" && KeychainItem.clientSecrete != ""{
                URLManager.shared.appleAuthTokenRevoke { status, errMsg in
                    
                    if errMsg != "" {
                        appDelegate.showToast(message: errMsg, bottomValue: getSafeAreaValue())
                    }
                    
                    if status == true {
                        print("Box id is \(String(describing: objUserInfo.boxId))")
                        
                        KeychainItem.currentUserIdentifier = ""
                        KeychainItem.currentUserEmail = ""
                        KeychainItem.currentUserFirstName = ""
                        KeychainItem.currentUserLastName = ""
                        KeychainItem.currentUserToken = ""
                        KeychainItem.clientSecrete = ""
                        self.removeUserDataAfterDeleteAccount()
                    }
                }
            }
        }else {
            print("Normal delete account")
            self.removeUserDataAfterDeleteAccount()
        }
    }
    
    func removeUserDataAfterDeleteAccount(){
        DispatchQueue.main.async {
            self.dismiss(animated: true) { [self] in
                let objUserInfo = getUserInfo()
                self.removeRecordFromDB(boxID:objUserInfo.boxId)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.PostLogoutAction(fromVC: self.refVC)
                }
            }
        }
    }
}

extension OTPVerificationPopupVC {
    func sendDeleteOtp(isForResend : Bool)
    {
        self.view.endEditing(true)
        
        var params : [String:Any] = [:]
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        self.showHUD()
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.sendDeleteOtp, showLoader: false) { (resultDict, status, message) in
            self.hideHUD()
            if status == true
            {
                if let otpId = resultDict["otp_id"] as? String{
                    self.otpId = Int(otpId)!
                }
                self.lblResendOTP.textColor = Colors.theme_label_black_color
                self.lblResendOTP.text = "00:\(timeLimit)"

                self.manageTimer()
                self.resendOTPAttemptCounterLeft -= 1
                self.showHideAttemptLeftCount()
                print("sendDeleteOtp is \(resultDict)")
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true, duration: 40)
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    func verifyDeleteOtp()
    {
        let otpNumber = "\(self.txtFirstNumber.text!)\(self.txtSecondNumber.text!)\(self.txtThirdNumber.text!)\(self.txtFourthNumber.text!)\(self.txtFifthNumber.text!)\(self.txtSixthNumber.text!)"
        print("otpNumber is : \(otpNumber)")
        var params : [String:Any] = [:]
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["otp_id"] = "\(self.otpId)"
        params["otp"] = otpNumber
        params["reason_id"] = self.reasonId
        params["reason_notes"] = self.reasonNote
        print("verification dele otp params are \(params)")

        self.showHUD()
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.verifyDeleteOtp, showLoader: false) { (resultDict, status, message) in
            self.hideHUD()
            if status == true
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue(), isForSuccess: true, duration: 40)
                    self.deleteUserAccount()
            }
            else
            {
                self.resetOTPFields()
                self.enableDisableVeifyButton()
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}
