//
//  OpenATicketVC.swift
//  CSFCustomer
//
//  Created by Tops on 26/08/21.
//

import UIKit
import WebKit
import MBProgressHUD
class OpenATicketVC: UIViewController,WKNavigationDelegate, UIScrollViewDelegate{
    
    // MARK: - IBOutlets
    @IBOutlet weak var webView : WKWebView!
    
    // MARK: - Global Variables
    var timer : Timer?
    var Indicator : MBProgressHUD?
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        if openTicketURL != ""
        {
            createLoader()
            loadURL(openTicketURL: openTicketURL)
            webView.isMultipleTouchEnabled = false
            webView.scrollView.bouncesZoom = false
            webView.resizeWebContent()
            webView.navigationDelegate = self
            webView.scrollView.delegate = self
        }
        else
        {
            showAlert(title: "", msg: Messsages.msg_no_osticket_url_found, vc: self)
        }
    }
    /**
     Creates a loader for displaying a loading indicator.     
     */    
    func createLoader()
    {
        Indicator = MBProgressHUD.showAdded(to: self.webView, animated: true)
        Indicator!.mode = MBProgressHUDMode.indeterminate
        Indicator!.bezelView.color = Colors.clearColor
    }

    /**
     Loads a URL for opening a ticket.
     */    
    func loadURL(openTicketURL:String)
    {
        let link = URL(string:openTicketURL)!
        let request = URLRequest(url: link)
        webView.load(request)
    }
    // MARK: - IBAction Methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
   // MARK: - Webview delegate methods
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
         scrollView.pinchGestureRecognizer?.isEnabled = false
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        Indicator!.show(animated: true)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Indicator!.hide(animated: true)
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Indicator!.hide(animated: true)
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url =  navigationAction.request.url!
        guard URLComponents(string: url.absoluteString) != nil else { return  }
        if navigationAction.request.url?.path == URL(string:openTicketLogoutURL)?.path
        {
            goToHomeVC()
        }
        decisionHandler(.allow)
    }
}
extension WKWebView {
    ///Method to fit content of webview inside webview according to different screen size
    func resizeWebContent() {
        let contentSize = self.scrollView.contentSize
        let viewSize = self.bounds.size
        let zoomScale = viewSize.width/contentSize.width
        self.scrollView.minimumZoomScale = zoomScale
        self.scrollView.maximumZoomScale = zoomScale
        self.scrollView.zoomScale = zoomScale
    }
}
