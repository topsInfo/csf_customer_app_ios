//
//  SecurityCenterVC.swift
//  CSFCustomer
//
//  Created by Tops on 01/02/21.
//

import UIKit

class SecurityCenterVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var btnMenu : UIButton!
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var bioSwitch : UISwitch!
    @IBOutlet weak var bioMetricOptionView : UIView!
    @IBOutlet weak var viewTheme: UIView!
    @IBOutlet weak var viewChangePasswordOption : UIView!

    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        objUserInfo = getUserInfo()
        btnMenu.isHidden = true
        btnBack.isHidden = false
        bioMetricOptionView.isHidden = true
        if checkLoginType(boxID:objUserInfo.boxId ?? "")
        {
            if let loginType = UserDefaults.standard.value(forKey: kTypeOfLogin) as? String,loginType == LoginType.AppLogin.rawValue
            {
                bioMetricOptionView.isHidden = false
                if let isBiometricEnabled = UserDefaults.standard.value(forKey: kIsBiometricEnabled) as? Bool, isBiometricEnabled == true
                {
                    bioSwitch.isOn = true
                }
                else
                {
                    bioSwitch.isOn = false
                }
            }
        }
        
        if #available(iOS 13.0, *) {
            viewTheme.isHidden = false
        }else {
            viewTheme.isHidden = true
        }
    }
    
    /**
     Shows an alert with a given title and message for biometric authentication.
     */
    func showAlertForBiometric(title:String,msg:String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            self.goToHomeVC()
        }
        okAction.setValue(UIColor(named:"theme_black_color"), forKey: "titleTextColor")
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - IBAction methods
    @IBAction func biometricValueChanged(_ sender:UISwitch)
    {
        let objManageCoreData = ManageCoreData()
        if sender.isOn == true
        {
            objManageCoreData.updateStatusofBiometric(entityName: kSwitchUserEntity, boxID: objUserInfo.boxId ?? "", fieldName: "isBiometricEnabled", value: true) { (status) in
                if status == true
                {
                    UserDefaults.standard.setValue(true, forKey: kIsBiometricEnabled)
                }
            }
            self.showAlertForBiometric(title: "", msg: "Biometric Authentication is successfully enabled on this device.You can now login with ease using your Face ID or Touch ID")
        }
        else
        {
            objManageCoreData.updateStatusofBiometric(entityName: kSwitchUserEntity, boxID: objUserInfo.boxId ?? "", fieldName: "isBiometricEnabled", value: false) { (status) in
                if status == true
                {
                    UserDefaults.standard.setValue(false, forKey: kIsBiometricEnabled)
                }
            }
            self.showAlertForBiometric(title: "", msg: "Biometric Authentication is disabled on this device")
        }
    }
    
    @IBAction func btnThemeClicked(_ sender: UIButton) {
        if let vc = appDelegate.getViewController("LightDarkModeVC", onStoryboard: "Home") as? LightDarkModeVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    // MARK: - IBAction methods
       @IBAction func darkModeValueChanged(_ sender:UISwitch)
    {
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                let appDel = UIApplication.shared.windows.first
                if sender.isOn {
                    UserDefaults.standard.setValue(true, forKey: kIsDarkModeEnabled)
                    appDel?.overrideUserInterfaceStyle = .dark
                    return
                }
                UserDefaults.standard.setValue(false, forKey: kIsDarkModeEnabled)
                appDel?.overrideUserInterfaceStyle = .light
                return
            }
        }
    }
    
    @IBAction func btnChangePasswordClicked()
    {
        if let vc = appDelegate.getViewController("ChangePasswordVC", onStoryboard: "Profile") as? ChangePasswordVC {
            objUserInfo = getUserInfo()
            vc.userID = objUserInfo.id ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnNotificationsClicked()
    {
        if let vc = appDelegate.getViewController("NotificationsListVC", onStoryboard: "Home") as? NotificationsListVC {
            objUserInfo = getUserInfo()
            vc.userID = objUserInfo.id ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
}
