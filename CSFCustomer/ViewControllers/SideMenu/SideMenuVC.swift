//
//  SideMenuVC.swift
//  CSFCustomer
//
//  Created by Tops on 25/11/20.
//

import UIKit
import CoreData
class SideMenuVC: UIViewController,UIScrollViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var imgCart : UIImageView!
    @IBOutlet weak var lblCartCounter : UILabel!
    @IBOutlet weak var lblUsername : UILabel!
    @IBOutlet weak var lblBoxID :  UILabel!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var logoutView : UIView!
    @IBOutlet weak var viewReport : UIView!
    @IBOutlet weak var viewAccountClosure : UIView!
    @IBOutlet weak var cnstViewReportHeight : NSLayoutConstraint!
    @IBOutlet weak var lblAccountClosureNew: UILabel!
    @IBOutlet weak var lblVideoPlayerNew: UILabel!
    @IBOutlet weak var viewVideoTutorial : UIView!
    @IBOutlet weak var lblVersionNumber: UILabel!
    
    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    override func viewWillAppear(_ animated: Bool) {
        UIView.setAnimationsEnabled(true)
        self.objUserInfo = getUserInfo()
        lblUsername.text = "Hi \(objUserInfo.firstName ?? "")"
        lblBoxID.text = "BOX ID : \(String(describing: objUserInfo.boxId ?? ""))"
        imgUser.setImage(objUserInfo.profileImagePath,placeHolder: UIImage(named: "default-image"))
        showCartCounter()
        //new chage core data
        let objManageCoreData = ManageCoreData()
        let activeUsers : Int = objManageCoreData.countActiveUsers(entityName:kSwitchUserEntity)
        if activeUsers > 1
        {
            logoutView.isHidden = false
        }
        else
        {
            logoutView.isHidden = true
        }
        self.scrollView.isDirectionalLockEnabled = true
        self.showAccountClosureNewTag()
        self.showVideoPlayerNewTag()
    }
    
    /**
        Deinitializes the SideMenuVC instance.
    */
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Custom Methods
    func configureControls()
    {
        lblVersionNumber.text = "v\(kVersion)"
        viewVideoTutorial.isHidden = false
        imgUser.layer.cornerRadius = imgUser.frame.size.height/2
        imgUser.layer.masksToBounds = true
        self.scrollView.delegate = self
        self.scrollView.isDirectionalLockEnabled = true
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(willEnterForegroundAppDelegate),
                                               name: UIApplication.willEnterForegroundNotification, object: nil)

    }

    @objc func willEnterForegroundAppDelegate() {
        print("willEnterForegroundAppDelegate called")
        self.showAccountClosureNewTag()
        self.showVideoPlayerNewTag()
    }
    
    // MARK: - showAccountClosureNewTag
    func showAccountClosureNewTag(){
        self.lblAccountClosureNew.isHidden = true
        if let isAccountClosureShown : Bool = UserDefaults.standard.value(forKey: kIsAccountClosureShown) as? Bool {
            if isAccountClosureShown {
                self.lblAccountClosureNew.isHidden = true
            }else {
                self.lblAccountClosureNew.isHidden = false
            }
        }else {
            self.lblAccountClosureNew.isHidden = false
        }

        self.lblAccountClosureNew.alpha = 1
        self.lblAccountClosureNew.startBlink()
    }
    
    // MARK: - showVideoPlayerNewTag
    func showVideoPlayerNewTag(){
        self.lblVideoPlayerNew.isHidden = true
        if let isVideoPlayerShown : Bool = UserDefaults.standard.value(forKey: kIsVideoTutorialShown) as? Bool {
            if isVideoPlayerShown {
                self.lblVideoPlayerNew.isHidden = true
            }else {
                self.lblVideoPlayerNew.isHidden = false
            }
        }else {
            self.lblVideoPlayerNew.isHidden = false
        }

        self.lblVideoPlayerNew.alpha = 1
        self.lblVideoPlayerNew.startBlink()
    }

    // MARK: - showCartCounter
    func showCartCounter()
    {
        if cartCounter > 0
        {
            lblCartCounter.layer.cornerRadius = lblCartCounter.frame.size.height/2
            lblCartCounter.layer.masksToBounds = true
            lblCartCounter.textAlignment = .center
            if cartCounter <= 9
            {
                lblCartCounter.attributedText = NSAttributedString(string: String(cartCounter), attributes: [NSAttributedString.Key.font : UIFont(name: fontname.openSansBold, size: 12)!,NSAttributedString.Key.foregroundColor : Colors.theme_white_color])
            }
            else
            {
                lblCartCounter.attributedText = NSAttributedString(string: "9+", attributes: [NSAttributedString.Key.font : UIFont(name: fontname.openSansBold, size: 12)!,NSAttributedString.Key.foregroundColor : Colors.theme_white_color])
            }
            lblCartCounter.isHidden = false
        }
        else
        {
            lblCartCounter.isHidden = true
        }
    }
    /**
        Checks if the given view controller is the home view controller.
    */
    func checkForHomeVC(vc : UIViewController?)
    {
        if let tabBarVC = self.revealViewController()?.frontViewController as? UITabBarController
        {
            tabBarVC.selectedIndex = 0
        }
        if let homeNavVC = appDelegate.topNavigation()
        {
            homeNavVC.pushViewController(vc!, animated: true)
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnYourProfileClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let tabBarVC = self.revealViewController()?.frontViewController as? UITabBarController
        {
            tabBarVC.selectedIndex = 3
        }
    }
    @IBAction func btnPreAlertClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("PreAlertListVC", onStoryboard: "PreAlert") as? PreAlertListVC{
            //  vc.isFromMenu = true
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnInvoiceClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("SegmentPageVC", onStoryboard: "Invoice") as? SegmentPageVC {
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnViewCartClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        let objUserInfo = getUserInfo()
        if let vc = appDelegate.getViewController("ViewCartVC", onStoryboard: "Invoice") as? ViewCartVC {
            vc.userID = objUserInfo.id ?? ""
            vc.isFromVC = "Dashboard"
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnRefundClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("PaymentRefundVC", onStoryboard: "Invoice") as? PaymentRefundVC {
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnCalculatorClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("CalculatorVC", onStoryboard: "Invoice") as? CalculatorVC {
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnTrackOrderClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("TrackOrderVC", onStoryboard: "Home") as? TrackOrderVC{
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnCommentsClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("FeedbackVC", onStoryboard: "Home") as? FeedbackVC {
            vc.arrFeedbackReason = arrFeedbackReason
            vc.mediaSizeLimit = documentSizeLimit
            vc.mediaLimit = documentLimit
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnSecurityCenterClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("SecurityCenterVC", onStoryboard: "Home") as? SecurityCenterVC {
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnServiceGuideClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("ServiceGuideVC", onStoryboard: "Home") as? ServiceGuideVC {
            vc.type = 1
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnOnlineShopperClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("ServiceGuideVC", onStoryboard: "Home") as? ServiceGuideVC {
            vc.type = 2
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnReferClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("RefernEarnVC", onStoryboard: "Referral") as? RefernEarnVC {
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnRewardClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("RewardsVC", onStoryboard: "Referral") as? RewardsVC {
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnOpenTicketClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("OpenATicketVC", onStoryboard: "Home") as? OpenATicketVC {
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnReportABugClicked()
    {
        self.revealViewController().revealToggle(animated: true)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let vc = appDelegate.getViewController("ReportBugVC", onStoryboard:"Profile" ) as? ReportBugVC {
                vc.navvc = appDelegate.topNavigation()
                vc.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc,animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnHowToUseApplicationClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("HowToUseListVC", onStoryboard: "Home") as? HowToUseListVC {
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnVideoTutorialClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("VideoTutorialVC", onStoryboard: "Home") as? VideoTutorialVC{
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnAccountClouserClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let vc = appDelegate.getViewController("DeleteAccountUserInfoVC", onStoryboard: "Home") as? DeleteAccountUserInfoVC {
            checkForHomeVC(vc:vc)
        }
    }
    @IBAction func btnLogoutAllClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        objUserInfo = getUserInfo()
        self.showAlertForLogout(title: "", msg: Messsages.msg_logout_all,userID:objUserInfo.id ?? "",type:"LogoutAll")
    }
    
    @IBAction func btnSupportClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        if let driverContactVC = appDelegate.getViewController("DriverContactDetailVC", onStoryboard: "Home") as? DriverContactDetailVC {
            driverContactVC.screenTitle = "Staff Support"
            checkForHomeVC(vc:driverContactVC)
        }
    }
    
    @IBAction func btnLogoutClicked()
    {
        self.revealViewController().revealToggle(animated: true)
        objUserInfo = getUserInfo()
        self.showAlertForLogout(title: "", msg: Messsages.msg_logout,userID:objUserInfo.id ?? "",type:"")
    }
    // MARK: - Scrollview method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x <= 0 || scrollView.contentOffset.x > 0{
            scrollView.contentOffset.x = 0
        }
    }
}
