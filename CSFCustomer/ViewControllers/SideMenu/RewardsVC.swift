//
//  RewardsVC.swift
//  CSFCustomer
//
//  Created by Tops on 05/05/21.
//

import UIKit
import SwiftyJSON
class RewardsVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var tblRewardList : UITableView!
    @IBOutlet weak var lblTotalCredit : UILabel!
    @IBOutlet weak var lblTotalPending : UILabel!
    @IBOutlet weak var lblNoRewardFound : UILabel!
    
    // MARK: - Global Variable
    var objUserInfo = UserInfo()
    var arrRewardList : [RewardList] = [RewardList]()
    var refreshControl = UIRefreshControl()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }

    // MARK: - Custom Methods
    func configureControls()
    {
        lblTotalCredit.text = "-"
        lblTotalPending.text = "-"
        lblNoRewardFound.isHidden = true
        objUserInfo = getUserInfo()
        getRewardList(isShowLoader: true)
        tblRewardList.dataSource = self
        tblRewardList.delegate = self
        tblRewardList.tableFooterView = UIView()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblRewardList.addSubview(refreshControl)
    }
    /**
     Refreshes the view controller.
     */
    @objc func refresh()
    {
        getRewardList(isShowLoader: false)
    }
    
    // MARK: - getRewardList
    func getRewardList(isShowLoader : Bool)
    {
        var params : [String:Any] = [:]
        objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.RewardList, showLoader: isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            self.tblRewardList.isHidden = false
            if status == true
            {
                let objReward = Reward(fromJson: JSON(resultDict))
                self.arrRewardList = objReward.list ?? []
                self.lblTotalCredit.text = "\(kCurrency) \(kCurrencySymbol)\(objReward.totalCreditedUsd ?? "0.00")"
                self.lblTotalPending.text = "\(kCurrency) \(kCurrencySymbol)\(objReward.totalPendingUsd ?? "0.00")"
                if self.arrRewardList.count > 0
                {
                    self.lblNoRewardFound.isHidden = true
                    DispatchQueue.main.async {
                        self.tblRewardList.reloadData()
                    }
                }
                else
                {
                    self.lblNoRewardFound.isHidden = false
                }
               
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Tableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRewardList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RewardCell", for: indexPath) as? RewardCell {
            let objReward = arrRewardList[indexPath.row]
            if let imgUserURL = objReward.profileImage, imgUserURL != ""
            {
                cell.imgUser.setImage(imgUserURL,placeHolder: UIImage(named: "default-image"))
            }
            cell.lblDate.text = objReward.createdAt ?? ""
            cell.lblBoxID.text = objReward.boxId ?? ""
            cell.lblName.text = objReward.name ?? ""
            cell.lblAmount.text = "\(kCurrencySymbol)\(objReward.amountUsd ?? "0.00")"
            if let status = objReward.status, status == 1
            {
                cell.lblStatus.text = "Credited to"
                cell.lblAmount.textColor = UIColor(named: "theme_green_color")//Colors.theme_green_color
                cell.imgStatus.image = UIImage(named: "wallet")
            }
            else
            {
                cell.lblStatus.text = "Pending"
                cell.lblAmount.textColor = UIColor(named: "theme_orange_color") //Colors.theme_orange_color
                cell.imgStatus.image = UIImage(named: "Pending")
            }
            return cell
        }
        return UITableViewCell()
    }
   
}
class RewardCell : UITableViewCell
{
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblBoxID : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var imgStatus : UIImageView!
    override func awakeFromNib() {
        imgUser.layer.cornerRadius = imgUser.frame.size.height/2
        imgUser.clipsToBounds = true
    }
}
