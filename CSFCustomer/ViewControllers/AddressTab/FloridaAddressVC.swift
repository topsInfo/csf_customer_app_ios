//
//  AddressListVC.swift
//  CSFCustomer
//
//  Created by Tops on 27/01/21.
//

import UIKit
import SwiftyJSON

class FloridaAddressVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var tblAddressList :UITableView!
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var viewInfo: UIView!
    
    // MARK: - Global Variable
    var arrFloridaAddress : [FloridaAddList] = [FloridaAddList]()
    var arrOfficeList : [OfficeList] = [OfficeList]()
    var refreshControl = UIRefreshControl()
    
    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        refreshControl.didMoveToSuperview()
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        viewInfo.isHidden = false
        topBarView.topBarBGColor()
        tblAddressList.dataSource = self
        tblAddressList.delegate = self
        tblAddressList.estimatedRowHeight = 400
        tblAddressList.rowHeight = UITableView.automaticDimension
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblAddressList.addSubview(refreshControl)
        
        getAddressList(isShowLoader: true)
    }
    
    @objc func refresh()
    {
        getAddressList(isShowLoader: false)
    }
    
    // MARK: - getAddressList
    /**
     Retrieves the list of addresses.
     */
    func getAddressList(isShowLoader: Bool)
    {
        var params : [String:Any] = [:]
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.FloridaAddress, showLoader:isShowLoader) { (resultDict, status, message) in
            self.refreshControl.endRefreshing()
            if status == true
            {
                let objFloridaAddress = FloridaAddress(fromJson: JSON(resultDict))
                self.arrFloridaAddress = objFloridaAddress.list ?? []
                self.arrOfficeList = objFloridaAddress.officeList ?? []
                
                DispatchQueue.main.async {
                    self.tblAddressList.reloadData()
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
    
    // MARK: - IBAction methods
    @IBAction func btnMenuClicked()
    {
        self.revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func btnInfoClicked(_ sender: UIButton) {
        self.showAddressMapVC()
    }
    
    // MARK: - Tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFloridaAddress.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FloridaAddCell", for: indexPath) as? FloridaAddCell {
            let objFloridaAddCell = arrFloridaAddress[indexPath.row]
            cell.lblTitle.text      = objFloridaAddCell.title ?? ""
            cell.lblName.text       = objFloridaAddCell.name ?? ""
            cell.lblAddress1.text   = objFloridaAddCell.address1 ?? ""
            cell.lblAddress2.text   = objFloridaAddCell.address2 ?? ""
            cell.lblCity.text       = objFloridaAddCell.city ?? ""
            cell.lblState.text      = objFloridaAddCell.state ?? ""
            cell.lblZipcode.text    = objFloridaAddCell.zipcode ?? ""
            
            let objUserInfo = getUserInfo()
            cell.lblMobile.text     = objUserInfo.mobileNo ?? ""
            if let strNotes = objFloridaAddCell.notes,strNotes != ""
            {
                cell.lblNotes.text = "\(strNotes)"
                cell.lblNotes.isHidden = false
            }
            else
            {
                cell.lblNotes.isHidden = true
            }
            cell.btnCopyAddress.tag = indexPath.row
            cell.btnCopyAddress.addTarget(self, action: #selector(btnCopyAddressClicked(sender:)), for: .touchUpInside )
            
            cell.btnCall.tag = indexPath.row
            cell.btnCall.addTarget(self, action: #selector(btnPhoneCallClicked(sender:)), for: .touchUpInside )
            
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: - getFullAddress
    func getFullAddress(index : Int) -> String{
        var strFullAddress : String = ""
        let objFloridaAddress = arrFloridaAddress[index]
        let strAdd1 = objFloridaAddress.address1 ?? ""
        let strAdd2 = objFloridaAddress.address2 ?? ""
        let strCity = objFloridaAddress.city ?? ""
        let strState = objFloridaAddress.state ?? ""
        let strZipcode = objFloridaAddress.zipcode ?? ""
        let objUserInfo = getUserInfo()
        let strMobileNumber = objUserInfo.mobileNo ?? ""
        if strAdd1 != ""
        {
            strFullAddress = strAdd1
        }
        if strFullAddress != "" && strAdd2 != ""
        {
            strFullAddress += "," + strAdd2
        }
        else
        {
            strFullAddress = strAdd2
        }
        if strFullAddress != "" && strCity != ""
        {
            strFullAddress += "," + strCity
        }
        else
        {
            strFullAddress = strCity
        }
        if strFullAddress != "" && strState != ""
        {
            strFullAddress += "," + strState
        }
        else
        {
            strFullAddress = strState
        }
        if strFullAddress != "" && strZipcode != ""
        {
            strFullAddress += "," + strZipcode
        }
        else
        {
            strFullAddress = strZipcode
        }
        strFullAddress += "\n\nMobile Number: " + strMobileNumber
        return strFullAddress
    }
    
    // MARK: - showAddressMapVC
    func showAddressMapVC(){
        if self.arrOfficeList.count > 0 {
            UIView.setAnimationsEnabled(true)
            if let addressMapVC = appDelegate.getViewController("AddressMapVC", onStoryboard: "Home") as? AddressMapVC {
                addressMapVC.arrOfficeList = self.arrOfficeList
                addressMapVC.modalPresentationStyle = .overFullScreen
                self.present(addressMapVC,animated: true, completion: nil)
            }
        }
    }
    
    
    /**
        Handles the button click event when the "Copy Address" button is clicked.
    */    
    @objc func btnCopyAddressClicked(sender:UIButton)
    {
        UIPasteboard.general.string = self.getFullAddress(index: sender.tag)
        appDelegate.showToast(message: Messsages.msg_address_copied, bottomValue: getSafeAreaValue(), isForSuccess: true)
    }

    /**
     Handles the button click event for making a phone call.
     */    
    @objc func btnPhoneCallClicked(sender:UIButton)
    {
        let objFloridaAddress = arrFloridaAddress[sender.tag]
        let mobileValue = objFloridaAddress.mobile ?? ""
        let mobile = mobileValue.replacingOccurrences(of: " ", with: "")

        if let url = URL(string: "tel://+1\(mobile)"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
}
class FloridaAddCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblAddress1 : UILabel!
    @IBOutlet weak var lblAddress2 : UILabel!
    @IBOutlet weak var lblCity : UILabel!
    @IBOutlet weak var lblState : UILabel!
    @IBOutlet weak var lblZipcode : UILabel!
    @IBOutlet weak var lblMobile : UILabel!
    @IBOutlet weak var btnCopyAddress : UIButton!
    @IBOutlet weak var btnCall : UIButton!
    @IBOutlet weak var lblNotes : UILabel!
    @IBOutlet weak var mainContainerView : UIView!
    @IBOutlet weak var copyAddContainerView : UIView!
    @IBOutlet weak var callContainerView : UIView!
    override func awakeFromNib() {
        copyAddContainerView.layer.cornerRadius = 20
        copyAddContainerView.layer.masksToBounds = true
        callContainerView.layer.cornerRadius = 20
        callContainerView.layer.masksToBounds = true
        mainContainerView.layer.borderWidth = 1
        mainContainerView.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor
        mainContainerView.layer.cornerRadius = 5
        mainContainerView.layer.masksToBounds = true
    }
}
