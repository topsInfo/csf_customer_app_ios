//
//  AddressMapCell.swift
//  CSFCustomer
//
//  Created by Tops on 10/11/22.

import UIKit

// MARK: - InvoiceActionsCell
class AddressMapCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var mainContainerView : UIView!
    @IBOutlet weak var btnViewLocation: UIButton!
    
    override func layoutSubviews() {
    }
    
    override func awakeFromNib() {
        mainContainerView.layer.borderWidth = 1
        mainContainerView.layer.borderColor = UIColor(named: "theme_lightgray_color")?.cgColor//Colors.theme_lightgray_color.cgColor
        mainContainerView.layer.cornerRadius = 5
        mainContainerView.layer.masksToBounds = true
    }
}
