//
//  AddressMapVC.swift
//  CSFCustomer
//
//  Created by Tops on 10/11/22.
//

import UIKit
import MapKit

let addressPaddingHeight : CGFloat = 30

class AddressMapVC: UIViewController
{
    // MARK: - IB-Outlets
    @IBOutlet weak var tblAddress : UITableView!
    @IBOutlet weak var topConst : NSLayoutConstraint!
    @IBOutlet weak var dimmerView: UIView!
    @IBOutlet weak var viewHeader: UIView!
    
    // MARK: - Global Varialbes
    var invoiceRowIndex : Int = 0
    var arrOfficeList : [OfficeList] = [OfficeList]()
    
    // MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
        showData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom methods
    func configureControls()
    {
        self.view.backgroundColor = UIColor(named: "theme_popup_bg_black")
        self.tblAddress.backgroundColor = UIColor(named: "theme_white_color")
    }
    
    // MARK: - showData
    func showData()
    {
        self.tblAddress.isHidden = true
        self.viewHeader.isHidden = true
        self.tblAddress.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            if let safeAreaHeight = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame.size.height as? CGFloat,
               let bottomPadding = UIApplication.shared.windows.first?.safeAreaInsets.bottom as? CGFloat
            {
                self.tblAddress.isHidden = false
                self.viewHeader.isHidden = false
                
                let topHeight = CGFloat(safeAreaHeight) - (self.tblAddress.contentSize.height + (2 * addressPaddingHeight))
                
                if topHeight > 0 {
                    self.topConst.constant = topHeight //(safeAreaHeight + bottomPadding)/3.5 //topHeight
                }else{
                    self.topConst.constant = 0 //(safeAreaHeight + bottomPadding)/3.5 //topHeight
                }
            }
            self.tblAddress.delegate = self
            self.tblAddress.dataSource = self
            self.tblAddress.isScrollEnabled = true
            self.tblAddress.reloadData()
        }
    }
    
    // MARK: - IB-Action Methods
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddressMapVC : UITableViewDataSource,UITableViewDelegate {
    // MARK: - Tableview delegte methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfficeList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AddressMapCell") as? AddressMapCell {
            cell.contentView.backgroundColor = UIColor(named: "theme_white_color")
            cell.backgroundColor = UIColor(named: "theme_white_color")
            
            if arrOfficeList.count > 0 {
                let objOfficeList : OfficeList = self.arrOfficeList[indexPath.row]
                cell.lblTitle.text = objOfficeList.title
                cell.lblAddress.text = objOfficeList.address
                cell.lblAddress.numberOfLines = 0
                cell.btnViewLocation.tag = indexPath.row
                cell.btnViewLocation.addTarget(self, action: #selector(btnViewLocationClicked(sender:)), for: .touchUpInside )
            }
            
            return cell
        }
        return UITableViewCell()
    }
    @objc func btnViewLocationClicked(sender:UIButton)
    {
        let objOfficeList : OfficeList = self.arrOfficeList[sender.tag]
        let addressTitle : String = "\(objOfficeList.title ?? "")\n\(objOfficeList.address ?? "")"
        let address : String = "\(objOfficeList.address ?? "")"
        
        self.chooseOption(latitude: Double(objOfficeList.lat) ?? 0, longitude: Double(objOfficeList.longField) ?? 0,locationFullAddress: addressTitle,locationAddress : address, objOfficeList: objOfficeList)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension AddressMapVC {
    
    // MARK: - chooseOption - Google / Apple Map
    func chooseOption(latitude : Double, longitude : Double, locationFullAddress : String, locationAddress : String, objOfficeList : OfficeList){
        self.view.endEditing(true)
        
        //Only redirect to that location
        let appleURL = "https://maps.apple.com/?ll=\(latitude),\(longitude)"
//        let googleURL = "https://maps.google.com/maps?center=\(latitude),\(longitude)&zoom=17&q=\(latitude),\(longitude)"
        let googleURL = "https://maps.google.com/maps?center=\(latitude),\(longitude)&zoom=17&q=\(locationAddress.replacingOccurrences(of: "&", with: "and"))"
        print("googleURL is \(googleURL)")

        var installedNavigationApps = [("Apple Map", URL(string:appleURL)!)]
        let googleItem = ("Google Map", URL(string:googleURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!)
        
        if UIApplication.shared.canOpenURL(googleItem.1) {
            installedNavigationApps.append(googleItem)
        }
        
        let attributedString = NSAttributedString(string: "Select Navigation App", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15), //your font here
            NSAttributedString.Key.foregroundColor :Colors.theme_gray_color
        ])
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(attributedString, forKey: "attributedTitle")
        
        for app in installedNavigationApps {
            print("\(app.0)")
            let button = UIAlertAction(title: app.0, style: .default, handler: { _ in
                if app.0 == "Apple Map" {
                    self.openAppleMap(latitude: Double(objOfficeList.iosLat) ?? 0, longitude: Double(objOfficeList.iosLong) ?? 0,locationAddress: locationFullAddress)
                }else{
                    UIApplication.shared.open(app.1, options: [:], completionHandler: nil)
                }
                
            })
            alert.addAction(button)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancel)
        present(alert, animated: true)
    }
    
    // MARK: - openAppleMap
    func openAppleMap(latitude : Double, longitude : Double, locationAddress : String) {
        
        let latitude:CLLocationDegrees = latitude
        let longitude:CLLocationDegrees = longitude
        
        let regionDistance:CLLocationDistance = 100
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = locationAddress
        mapItem.openInMaps(launchOptions: options)
    }
}