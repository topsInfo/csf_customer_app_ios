//
//  AddMoneyVC.swift
//  CSFCustomer
//
//  Created by Tops on 16/03/21.
//

import UIKit
import PassKit
import Stripe
class AddMoneyVC: UIViewController,UITextFieldDelegate,AddMoneyPopupVCDelegate
{
    // MARK: - IBOutlets
    @IBOutlet weak var topBarView : UIView!
    @IBOutlet weak var txtAmount : UITextField!
    @IBOutlet weak var lblDesc : UILabel!
    
    // MARK: - Global Variable
    let minAmount : Float = 100
    let maxAmount : Float = 1000
    var type : String = ""
    var stripeToken : String = ""
    var ttdAmount : String  = "0"
    var USDAmount : String = "0"

    // MARK: - Viewcontroller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Custom Methods
    func configureControls()
    {
        topBarView.topBarBGColor()
        lblDesc.text = "Add money to your wallet from account. This amount can be used while you checkout to pay for your invoices."
        txtAmount.paddingView(xvalue: 10)
        txtAmount.delegate = self
    }

    // MARK: - IBAction methods
    @IBAction func btnBackClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnPayPalClicked()
    {
        if isValidData()
        {
            callAPIForTTDToUSDConversion(type: "PayPal")
        }
    }
    @IBAction func btnStripeClicked()
    {
        if isValidData()
        {
            callAPIForTTDToUSDConversion(type: "CardPayment")
        }
    }
    @IBAction func btnApplePayClicked()
    {
        if isValidData()
        {
            callAPIForTTDToUSDConversion(type: "ApplePay")
        }
    }
    // MARK: - Custom methods
    func isValidData() -> Bool
    {
        if isCheckNull(strText: txtAmount.text!)
        {
            appDelegate.showToast(message: Messsages.msg_enter_amount, bottomValue: getSafeAreaValue())
            return false
        }
        //Nand : Payment detail changes
        else if(Float(txtAmount.text!)! < minAmount){
            appDelegate.showToast(message: Messsages.msg_add_min_max_ttd_amount_into_wallet, bottomValue: getSafeAreaValue() + 40, height: 110)
            return false
        }else if (Float(txtAmount.text!)! > maxAmount){
            appDelegate.showToast(message: Messsages.msg_max_ttd_amount_for_wallet , bottomValue: getSafeAreaValue())
            return false
        }
        return true
    }
    
    // MARK: - callAPIForTTDToUSDConversion
    /**
     Calls the API for TTD to USD conversion.     
     */
    func callAPIForTTDToUSDConversion(type:String){
        var params : [String:Any] = [:]
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["amount"] = txtAmount.text ?? ""
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.TTDTOUSDConversion, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                if let USDAmount = (resultDict as NSDictionary).value(forKey: "usd_amount") as? String
                {
                    self.USDAmount = USDAmount
                    if let popupVC = appDelegate.getViewController("AddMoneyPopupVC", onStoryboard: "Referral") as? AddMoneyPopupVC {
                        popupVC.PaymentType = type
                        popupVC.USDAmount = USDAmount
                        popupVC.addMoneyVCDelegate = self
                        popupVC.view.backgroundColor = Colors.theme_black_color.withAlphaComponent(0.8)
                        popupVC.modalPresentationStyle = .overFullScreen
                        self.present(popupVC,animated: true, completion: nil)
                    }
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
                
            }
        }
    }
    
    // MARK: - showApplePayView
    /**
     Shows the Apple Pay view for adding money to the wallet.     
     */
    func showApplePayView(USDAmount:String)
    {
        //        let paymentNetworks = [PKPaymentNetwork.amex, .discover, .masterCard, .visa]
        //        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks)
        //        {
        let request = PKPaymentRequest()
        request.merchantIdentifier = "merchant.com.csfcustomer"
        request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
        request.merchantCapabilities = PKMerchantCapability.capability3DS
        request.countryCode = "US"
        request.currencyCode = "USD"
        //  request.requiredShippingAddressFields = PKAddressField.all
        //"Wallet Amount in USD"
        let USDamount = Decimal(string: USDAmount)
        if USDamount! > 0
        {
            request.paymentSummaryItems = [
                PKPaymentSummaryItem(label: "CSF Couriers Ltd.", amount:USDamount! as NSDecimalNumber)
            ]
            
            let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
            applePayController?.delegate = self
            self.present(applePayController!, animated: true, completion: nil)
        }
        else
        {
            showAlert(title: "", msg: Messsages.msg_invalid_usdamount, vc: self)
        }
    }
    
    // MARK: - showController
    func showController(type: String) {
        if type == "ApplePay"
        {
            showApplePayView(USDAmount:USDAmount)
        }
        else if type == "PayPal"
        {
            if let vc = appDelegate.getViewController("PaypalPaymentVC", onStoryboard: "Referral") as? PaypalPaymentVC {
                vc.isFromVC = "AddMoney"
                vc.TTDAmount = self.txtAmount.text!
                vc.USDAmount = self.USDAmount
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if type == "CardPayment"
        {
            if let vc = appDelegate.getViewController("CardPaymentVC", onStoryboard: "Referral") as? CardPaymentVC {
                vc.isFromVC = "AddMoney"
                vc.type = "1"
                vc.totalTTDAmt = self.txtAmount.text!
                vc.totalUSDAmt = self.USDAmount
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    // MARK: - Textfield delegate methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if textField == txtAmount
        {
            return newLength <= 4
        }
        return true
    }
}
/**
 The extension of `AddMoneyVC` that conforms to the `PKPaymentAuthorizationViewControllerDelegate` protocol.
 This extension provides the necessary delegate methods for handling payment authorization view controller events. 
 */
extension AddMoneyVC: PKPaymentAuthorizationViewControllerDelegate {
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        self.dismiss(animated: true, completion: nil)
        
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        // Send the payment object info to Stripe to get the token
        STPAPIClient.shared.createToken(with: payment) { (token, error) in
            if error != nil {
                self.showAlert(title: "", msg: "error", vc: self)
               // print("There is an error")
            }
            else {
               // print(token)
                self.stripeToken = token!.tokenId
                self.type = "3"
                self.callAPIForApplePayToWalletTransfer()
            }
        }
        completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.success, errors: []))
    }
    // MARK: - Custom Methods
    /**
     Calls the API to transfer money from Apple Pay to the user's wallet.     
     */
    func callAPIForApplePayToWalletTransfer()
    {
        var params : [String:Any] = [:]
        let objUserInfo = getUserInfo()
        params["user_id"] = objUserInfo.id ?? ""
        params["token"] = self.stripeToken
        params["total_ttd"]  = self.txtAmount.text!
        params["type"] = self.type
        URLManager.shared.URLCall(method: .post, parameters: params, header: true, url: APICall.StripeToWalletTransfer, showLoader:true) { (resultDict, status, message) in
            if status == true
            {
                NotificationCenter.default.post(name: Notification.Name("WalletUpdated"), object: nil,userInfo: nil)
                if let vc = appDelegate.getViewController("PaymentSuccessVC", onStoryboard: "Invoice") as? PaymentSuccessVC {
                    vc.isFrom = "AddMoney"
                    vc.TTDAmount = self.USDAmount
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                appDelegate.showToast(message: message, bottomValue: getSafeAreaValue())
            }
        }
    }
}
